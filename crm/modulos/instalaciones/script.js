$(document).ready(function(){ 
  $("#eventDialog").load("../../libs/templates/eventdialog.php"); 
  updateIndexActive("li_navBar_soporte");
  getEventsInstall();
});

//funcion que consigue las listas de eventos de ctipo soporte/instalación para su despliegue
function getEventsInstall(){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getEventInstall"},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo eventos");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#div_eventsInstall").html(resp[1]);      
        $("#tbl_eventsInstall").DataTable({
          language: datatableespaniol,
        });
      }
      else{
        $("#div_eventsInstall").html(resp[1]);
      } 
    }  
  });
}

function openInsReqNew(orgId,orgType,id){ 
  $("#hdn_eveIdIns").val(id);
  $("#hdn_orgId").val(orgId);
  $("#hdn_orgType").val(orgType);
  $("#mod_installRequest").modal("show");
  getContactList(9);
}

function getInstallReqFormat(){
  var con = $('#div_insReqConSel input:radio:checked').val();
  window.open("installrequest.php?eveId='"+$("#hdn_eveIdIns").val()+"&con="+con,'_blank');
}
