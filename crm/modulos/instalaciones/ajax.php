<?php

session_start();
set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

include_once "../../libs/db/common.php";

if (!verifySession($_SESSION)) {
  logoutTimeout();
} else {
  $gruposArr  = $_SESSION['grupos'];
  $usuarioId  = $_SESSION['usrId'];
  $usuario    = $_SESSION['usuario'];
  $nombre     = $_SESSION['usrNombre'];

  $db_modulos   = condb_modulos();
  $db_usuarios  = condb_usuarios();
  $db_catalogos = condb_catalogos();

  $modId = 9;

  if ("getEventInstall" == $_POST["action"]) {

    $query = "SELECT cal.*, sta.estatus FROM " . $cfgTableNameMod . ".calendario AS cal, " . $cfgTableNameCat . ".cat_evento_estatus AS sta WHERE cal.estatus_id=sta.evento_estatus_id AND cal.tipo_evento_id>=5 AND cal.tipo_evento_id<=8";

    $result = mysqli_query($db_modulos, $query);

    if (!$result) {
      $salida = "ERROR||Ocurrió un problema al obtener los datos de eventos";
    } else {
      if (0 == mysqli_num_rows($result)) {
        $salida .= "OK||No existen eventos asociados al departamento de instalaciones. Podrá agregar uno desde el modulo de prospectos, clientes, o desde un sitio.";
      } else {
        $fechaNow = date('Y-m-d H:i:s');
        $salida .= "OK||<div class=\"table-responsive\"><table id=\"tbl_eventsInstall\" class=\"table table-striped table-condensed\">\n" .
          "  <thead>\n" .
          "    <tr>\n" .
          "      <th>Origen</th>\n" .
          "      <th>Título</th>\n" .
          "      <th>Por:</th>\n" .
          "      <th>Programado en:</th>\n" .
          "      <th>Programado para:</th>\n" .
          "      <th>Estatus</th>\n" .
          "      <th>Descripción</th>\n" .
          "      <th>Acciones</th>\n" .
          "    </tr>\n" .
          "  </thead>\n" .
          "  <tbody>\n";
        while ($row = mysqli_fetch_assoc($result)) {

          $fechaLim = strtotime($row['fecha_fin']);

          $fechaAlta = strtotime($row['fecha_alta']);

          $tiempoRes = round(($fechaLim - strtotime($fechaNow)) / 3600);

          $tiempoDeAlta = round((strtotime($fechaNow) - $fechaAlta) / 3600);

          $nombreAlta = get_userRealName($row['usuario_id']);
          $salida .=  "    <tr>\n";
          if (0 == $row['tipo_origen'])
            $salida .=  "      <td>Prospecto</td>\n";
          else if (1 == $row['tipo_origen'])
            $salida .=  "      <td>Cliente</td>\n";
          else if (2 == $row['tipo_origen'])
            $salida .=  "      <td>Sitio</td>\n";
          else
            $salida .=  "      <td><i>Sin origen específico</i></td>\n";


          $salida .=  "      <td>" . $row['titulo'] . "</td>\n" .
            "      <td><a href=/crm/modulos/perfil/index.php?id='" . encrypt($row['usuario_id']) . "'>" . $nombreAlta . "</a></td>\n";

          $salida .=  "      <td>" . $row["fecha_alta"] . "</td>\n";

          if ('0000-00-00 00:00:00' == $row["fecha_inicio"]) {
            if (24 <= $tiempoDeAlta) {
              $salida .=  "      <td><i>Sin agendar</i><span class=\"glyphicon glyphicon-exclamation-sign\" style=\"color:red\" title=\"Tiene más de un día sin agendarse\"></span></td>\n";
            } elseif (12 <= $tiempoDeAlta) {
              $salida .=  "      <td><i>Sin agendar</i><span class=\"glyphicon glyphicon-exclamation-sign\" style=\"color:orange\" title=\"Tiene más de 12 horas sin agendarse\"></span></td>\n";
            } elseif (6 <= $tiempoDeAlta) {
              $salida .=  "      <td><i>Sin agendar</i><span class=\"glyphicon glyphicon-exclamation-sign\" style=\"color:yellow\" title=\"Tiene más de seis horas sin agendarse\"></span></td>\n";
            } else {
              $salida .=  "      <td><i>Sin agendar</i></td>\n";
            }
          } else {
            if (0 >= $tiempoRes && 3 > $row['estatus_id']) {
              $salida .=  "      <td>" . $row["fecha_inicio"] . " <span class=\"glyphicon glyphicon-alert\" style=\"color:red\" title=\"Transcurrió la fecha límite y no se ha actualizado como terminado\"></span></td>\n";
            } else {
              $salida .=  "      <td>" . $row["fecha_inicio"] . "</td>\n";
            }
          }
          $salida .= "      <td>" . $row['estatus'] . "</td>\n" .
            "      <td title=\"" . $row["descripcion"] . "\" onClick=\"alert('" . $row["descripcion"] . "')\">" .
            "<a href=\"#\">" . substr($row["descripcion"], 0, 30) . "...</a></td>\n";

          $salida .= "      <td>";

          $salida .= "      <button type=\"button\" title=\"Ir a Calendario\" class=\"btn btn-xs btn-default btn-responsive\" onclick=\"location.href = '../calendario';\"><span class=\"glyphicon glyphicon-calendar\"></span></button>";

          if (hasPermission($modId, 'r') || (hasPermission($modId, 'l') && $row['usuario_id'] == $usuarioId)) {
            $salida .= "      <button type=\"button\" id=\"btn_eventComments_" . $row['id'] . "\" title=\"Ver Comentarios\" class=\"btn btn-xs btn-info btn-responsive\" onClick=\"getComments(" . $row['id'] . ",3);\"><span class=\"glyphicon glyphicon-comment\"></span></button>";
          }
          if (hasPermission($modId, 'e') || (hasPermission($modId, 'n') && $row['usuario_id'] == $usuarioId)) {
            $salida .= "  <button type=\"button\" id=\"btn_eventEdit_" . $row['id'] . "\" title=\"Editar Evento\" " .
              "class=\"btn btn-xs btn-success btn-responsive\" onClick=\"editEvent(" . $row['id'] . ");\">" .
              "<span class=\"glyphicon glyphicon-edit\"></span></button>";
          }
          if (hasPermission($modId, 'd')) {
            $salida .= "  <button type=\"button\" id=\"btn_eventDelete_" . $row['id'] . "\" title=\"Borrar Evento\" " .
              "class=\"btn btn-xs btn-danger btn-responsive\" onClick=\"deleteEvent(" . $row['id'] . ");\">" .
              "<span class=\"glyphicon glyphicon-remove\"></span></button>";
          }
          if ('0000-00-00 00:00:00' == $row["fecha_inicio"]) {
            $salida .= "      <button type=\"button\" title=\"Generar Solicitud\" class=\"btn btn-xs btn-default btn-responsive disabled\" onclick=\"openInsReqNew(" . encrypt($row['origen_id']) . "," . $row['tipo_origen'] . "); disabled\"><span class=\"glyphicon glyphicon-print\"></span></button>";
          } else {
            $salida .= "      <button type=\"button\" title=\"Generar Solicitud\" class=\"btn btn-xs btn-default btn-responsive\" onclick=\"openInsReqNew('" . encrypt($row['origen_id']) . "'," . $row['tipo_origen'] . ",'" . encrypt($row['id']) . "');\"><span class=\"glyphicon glyphicon-print\"></span></button>";
          }

          $salida .= "      </td>\n" .
            "    </tr>\n";
        }
        $salida .= "  </tbody>\n" .
          "</table>\n</div>";
      }
    }
    mysqli_close($db_usuarios);
    mysqli_close($db_modulos);
    mysqli_close($db_catalogos);
    echo $salida;
  }
}
?>
