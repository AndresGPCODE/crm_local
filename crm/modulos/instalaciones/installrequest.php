<?php
session_start();
set_time_limit(0);

include_once "../../libs/db/common.php";
include_once "../../libs/db/encrypt.php";
include_once "../../libs/fpdf/fpdf.php";

header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

$usuario = $_SESSION['usuario'];
$usuarioId = $_SESSION['usrId'];
$gruposArr  = $_SESSION['grupos'];

$eveId  = isset($_GET['eveId']) ? decrypt($_GET['eveId']) : -1;
$conId = isset($_GET['con']) ? decrypt($_GET['con']) : -1;

class PDF extends FPDF
{
  function putHeader($headtext){
    $this->SetFillColor(0);
    $this->SetTextColor(255,255,255);
    $this->SetFont('Arial','B',14); 
    $this->Cell(0,6, utf8_decode(strtoupper($headtext)),'LRT',2,'C',true);
  }
  
  function putFieldName($fieldName,$wideSize){
    //$this->SetFillColor(255,255,255);
    $this->SetTextColor(0);
    $this->SetFont('Arial','B',10);
    $this->Cell($wideSize,6,utf8_decode($fieldName),'LRTB',0,'L',false);
  }
  
  function putFilledField($fieldValue,$wideSize){
    //$this->SetFillColor(255,255,255);
    $this->SetTextColor(0);
    $this->SetFont('Arial','I',10);
    $this->Cell($wideSize,6,utf8_decode($fieldValue),'LRTB',0,'L',false);
  }
  
  function putMultiCellField($fieldValue,$heightsize){
    //$this->SetFillColor(255,255,255);
    $this->SetTextColor(0);
    $this->SetFont('Arial','I',9);
    $this->MultiCell(0,$heightsize,utf8_decode($fieldValue),'LRTB','C',false);
  }
}

$db_modulos = condb_modulos();
$db_catalogos = condb_catalogos();
$db_usuarios = condb_usuarios();

if(-1==$eveId){
  die("ERROR||No se pudo obtener los datos de entrada para generar el documento.");
  goto a;
}
else{
  $queryCal = "SELECT * FROM calendario WHERE id=".$eveId; 
  
  $resultCal = mysqli_query($db_modulos,$queryCal);
  
  if(!$resultCal){
    die("No se pudo obtener los datos de evento para generar el documento.");
  }
  else{
    $fechaNow = date("Y-m-d H:i:s");
    
    $rowCal = mysqli_fetch_assoc($resultCal);
    
    $orgType = $rowCal['tipo_origen'];
    $orgId   = $rowCal['origen_id'];
    $eveType = $rowCal['tipo_evento_id'];
    
    $orgIdField = "";
    $orgTable = "";
    
    switch($orgType){
      case 0:
        $queryOrg = "SELECT pro.*, usr.nombre, usr.apellidos, con.nombre AS connom, con.apellidos AS conape, con.telefono, con.tel_movil, con.email, cal.descripcion AS evedesc, cal.fecha_inicio ".
                    "FROM ".$cfgTableNameMod.".prospectos AS pro, ".
                            $cfgTableNameUsr.".usuarios AS usr, ".
                            $cfgTableNameMod.".calendario AS cal,  ".       
                            $cfgTableNameMod.".rel_origen_contacto AS rel, ".
                            $cfgTableNameMod.".contactos AS con ". 
                    "WHERE cal.tipo_origen=".$orgType." ".         
                    "AND cal.origen_id=pro.prospecto_id ".
                    "AND rel.tipo_origen=".$orgType." ". 
                    "AND rel.origen_id=pro.prospecto_id ".
                    "AND rel.contacto_id=con.contacto_id ".
                    "AND usr.usuario_id=pro.usuario_alta ".
                    "AND cal.id=".$eveId." ".
                    "AND con.contacto_id=".$conId;                   
      break;
      case 1:
        $queryOrg = "SELECT cli.razon_social, cli.nombre_comercial, cli.domicilio_fiscal AS domicilio, cli.colonia_fiscal AS colonia, cli.municipio_fiscal AS municipio, cli.cp_fiscal AS cp, usr.nombre, usr.apellidos, con.nombre AS connom, con.apellidos AS conape, con.telefono, con.tel_movil, con.email, cal.descripcion AS evedesc, cal.fecha_inicio ".
                    "FROM ".$cfgTableNameMod.".clientes AS cli, ".
                            $cfgTableNameUsr.".usuarios AS usr, ".
                            $cfgTableNameMod.".calendario AS cal,  ".       
                            $cfgTableNameMod.".rel_origen_contacto AS rel, ".
                            $cfgTableNameMod.".contactos AS con ".
                    "WHERE cal.tipo_origen=".$orgType." ".         
                    "AND cal.origen_id=cli.cliente_id ".
                    "AND rel.tipo_origen=".$orgType." ". 
                    "AND rel.origen_id=cli.cliente_id ".
                    "AND rel.contacto_id=con.contacto_id ".
                    "AND usr.usuario_id=cli.usuario_alta ".
                    "AND cal.id=".$eveId." ".
                    "AND con.contacto_id=".$conId;
      break;
      case 2:  
        $querySit = "SELECT sit.sitio_id, sit.origen_id, sit.tipo_origen ".
                    "FROM sitios AS sit, ".
                         "calendario AS cal ".
                    "WHERE cal.tipo_origen=2 ".
                    "AND cal.origen_id=sit.sitio_id ".
                    "AND cal.id=".$eveId;
                 
        $resultSit = mysqli_query($db_modulos,$querySit);
         
        if(!$resultSit){
          die("Ocurrió un problema al obtenerse el id de sitio asociado a este evento.");
        }
        else{
          if(0==mysqli_num_rows($resultSit)){
            die("No se encontraron sitios asociados a este evento.");
          }
          else{ 
            $rowSit = mysqli_fetch_assoc($resultSit);
            
            $sitId = $rowSit['sitio_id'];
            $sitOrgId = $rowSit['origen_id'];
            $sitOrgType = $rowSit['tipo_origen'];
            
            switch($sitOrgType){
              case 0:
                $orgTableName = "prospectos";
                $orgIdFieldname = "prospecto_id";
              break;
              case 1:
                $orgTableName = "clientes";
                $orgIdFieldname = "cliente_id";
              break;
            }
            
            $queryOrg = "SELECT sit.*, org.nombre_comercial, org.razon_social, org.".$orgIdFieldname.", ".
                        "usr.nombre, usr.apellidos, con.nombre AS connom, con.apellidos AS conape, con.telefono, ".
                        "con.tel_movil, con.email, cal.descripcion AS evedesc, cal.fecha_inicio ".
                        "FROM ".$cfgTableNameMod.".sitios AS sit, ".
                                $cfgTableNameMod.".".$orgTableName." AS org, ".
                                $cfgTableNameMod.".calendario AS cal, ".
                                $cfgTableNameUsr.".usuarios AS usr, ".
                                $cfgTableNameMod.".rel_origen_contacto AS rel, ".
                                $cfgTableNameMod.".contactos AS con ".
                        "WHERE sit.sitio_id=".$sitId." ".
                        "AND sit.origen_id=org.".$orgIdFieldname." ". 
                        "AND sit.tipo_origen=".$sitOrgType." ".
                        "AND rel.tipo_origen=".$orgType." ".
                        "AND rel.origen_id=".$sitId." ".
                        "AND rel.contacto_id=con.contacto_id ".                        
                        "AND cal.id=".$eveId." ".
                        "AND con.contacto_id=".$conId." "; 
            if(8!=$eveType){
              $queryOrg .= "AND usr.usuario_id=org.usuario_alta";
            }
            else{
              $queryOrg .= "AND usr.usuario_id=cal.usuario_id";
            }                                    
          }     
        } 
      break;
    } 
    
    $resultOrg = mysqli_query($db_modulos,$queryOrg);
    
    if(!$resultOrg){
      die("ERROR: Ocurrió un problema al obtenerse los datos generales del registro");
    }
    else{
      if(0==mysqli_num_rows($resultOrg)){
        die("ERROR: No existen registros para el evento con este id.");
      }
      else{
        $rowOrg = mysqli_fetch_assoc($resultOrg);
        
        $formTitle = "";
        switch($eveType){
          case 8:
            $formTitle = "Mantenimiento preventivo y/o correctivo";
            $formDepName = "Tecnicos:";
            $formAddrTitle = "Domicilio Nuevo";
            $formValTitle = "Validacion del Servicio";
          break;
          case 6:
            $formTitle = "Cambio de Domicilio";
            $formDepName = "Ventas:";
            $formAddrTitle = "Domicilio Anterior";
            $formValTitle = "Validacion del Servicio";
          break;
          case 7:
           $formTitle = "Instalacion";
           $formDepName = "Ejecutivo:";
           $formAddrTitle = "Domicilio";
           $formValTitle = "Validacion del Servicio (cliente)";
          break;
          case 5:
            $formTitle = "Estudio de Factibilidad";
            $formDepName = "Ejecutivo:";
            $formAddrTitle = "Domicilio";
            $formValTitle = "Validacion del Servicio (cliente)";
          break;       
        }
        
        $pdf = new PDF();   
        $pdf->AddPage();
        $pdf->SetXY(10, 10);                
        $pdf->putHeader($formTitle);
        $pdf->putFieldName("Fecha de Solicitud:",35);
        //$pdf->putFilledField($fechaNow,40);
        $pdf->putFilledField($rowOrg['fecha_inicio'],40);
        $pdf->putFieldName($formDepName,20);
        $pdf->putFilledField($rowOrg['nombre']." ".$rowOrg['apellidos'],95);
        $pdf->Ln(12);
        $pdf->putHeader("datos del cliente");
        $pdf->putFieldName("Nombre Comercial:",35);
        $pdf->putFilledField($rowOrg['nombre_comercial'],155);
        $pdf->Ln(6);
        $pdf->putFieldName("Razón Social:",35);
        $pdf->putFilledField($rowOrg['razon_social'],155);
        //$pdf->Ln(12);
        $pdf->Ln(6);
        $pdf->putHeader($formAddrTitle);
        $pdf->putFieldName("Calle:",20);
        $pdf->putFilledField($rowOrg['domicilio'],170);
        $pdf->Ln(6);
        $pdf->putFieldName("Colonia:",20);
        $pdf->putFilledField($rowOrg['colonia'],90);
        $pdf->putFieldName("C.P.",10);
        $pdf->putFilledField($rowOrg['cp'],13);
        $pdf->putFieldName("Municipio:",20);
        $pdf->putFilledField($rowOrg['municipio'],37);
        $pdf->Ln(6);
        $pdf->putFieldName("Cruza con:",20);
        if(isset($rowOrg['calle1'])&&isset($rowOrg['calle2']))
          $pdf->putFilledField($rowOrg['calle1']." y ".$rowOrg['calle2'],170);
        else
          $pdf->putFilledField("",170);
        $pdf->Ln(6);
        $pdf->putFieldName("Referencias del Sitio:",40);
        // $pdf->putFilledField($rowOrg['sitio_referencias'],150);
        $pdf->putFilledField('aqui va el que no estaba definido',150);
        //$pdf->Ln(12);
        $pdf->Ln(6);
        $pdf->putHeader("contacto en sitio");
        $pdf->putFieldName("Contacto:",20);
        $pdf->putFilledField($rowOrg['connom']." ".$rowOrg['conape'],91);
        $pdf->putFieldName("Telefono:",18);
        $pdf->putFilledField($rowOrg['telefono'],23);
        $pdf->putFieldName("Celular:",15);
        $pdf->putFilledField($rowOrg['tel_movil'],23);
        $pdf->Ln(6);
        $pdf->putFieldName("Correo Electrónico:",35);
        $pdf->putFilledField($rowOrg['email'],80);
        $pdf->putFieldName("Otro:",12);
        $pdf->putFilledField("",63);
        $pdf->Ln(12);
        
        if(8==$eveType){
          $pdf->putHeader("autorizacion infraestructura y red");
          $pdf->putFieldName("Ing. que autoriza:",35);
          $pdf->putFilledField("",80);
          $pdf->putFieldName("Firma:",15);
          $pdf->putFilledField("",60);
          //$pdf->Ln(12);
          $pdf->Ln(6);
          $pdf->putHeader("reporte");
          $pdf->putMultiCellField($rowOrg['evedesc'],30);
        }
        if(6==$eveType){
          $pdf->putHeader("domicilio nuevo");
          $pdf->putFieldName("Calle:",20);
          $pdf->putFilledField("",170);
          $pdf->Ln(6);
          $pdf->putFieldName("Colonia:",20);
          $pdf->putFilledField("",90);
          $pdf->putFieldName("C.P.",10);
          $pdf->putFilledField("",13);
          $pdf->putFieldName("Municipio:",20);
          $pdf->putFilledField("",37);
          $pdf->Ln(6);
          $pdf->putFieldName("Cruza con:",20);
          $pdf->putFilledField("",170);
          $pdf->Ln(6);
          $pdf->putFieldName("Referencias del Sitio:",40);
          $pdf->putFilledField("",150);
          $pdf->Ln(6);
          $pdf->putHeader("observaciones");
          $pdf->putMultiCellField($rowOrg['evedesc'],30);         
        }
        if(7==$eveType||5==$eveType){          
          $pdf->putHeader("Servicio Solicitado");
          $pdf->putMultiCellField($rowOrg['evedesc'],30);
          //$pdf->Ln(6);
          $pdf->putHeader("autorizacion infraestructura y red");
          if(5==$eveType){
            $pdf->putFieldName("Estatus:",20);
            $pdf->putFilledField("",170);
            $pdf->Ln(6);
          }          
          $pdf->putFieldName("Ing. que autoriza:",35);
          $pdf->putFilledField("",80);
          $pdf->putFieldName("Firma:",15);
          $pdf->putFilledField("",60);
          $pdf->Ln(6);
          $pdf->putFieldName("Equipo a instalar:",35);
          $pdf->putFilledField("",155);
          //$pdf->Ln(12);
          $pdf->Ln(6);
          $pdf->putHeader("configuracion antena");
          $pdf->putFieldName("RadioBase1",23);
          $pdf->putFilledField("",50);
          $pdf->putFieldName("Km",8);
          $pdf->putFilledField("",11);
          $pdf->putFieldName("Dbm's",13);
          $pdf->putFilledField("",11);
          $pdf->putFieldName("CCQ",10);
          $pdf->putFilledField("",11);
          $pdf->putFieldName("Quality",14);
          $pdf->putFilledField("",11);
          $pdf->putFieldName("Capacity",17);
          $pdf->putFilledField("",11);
          $pdf->Ln(6);
          $pdf->putFieldName("SpeedTest1",63.33);
          $pdf->putFieldName("SpeedTest2",63.33);
          $pdf->putFieldName("SpeedTest3",63.33);
          $pdf->Ln(6);
          $pdf->putFieldName("Bajada",14);
          $pdf->putFilledField("",17.66);
          $pdf->putFieldName("Subida",14);
          $pdf->putFilledField("",17.66);
          $pdf->putFieldName("Bajada",14);
          $pdf->putFilledField("",17.66);
          $pdf->putFieldName("Subida",14);
          $pdf->putFilledField("",17.66);
          $pdf->putFieldName("Bajada",14);
          $pdf->putFilledField("",17.66);
          $pdf->putFieldName("Subida",14);
          $pdf->putFilledField("",17.66);
          $pdf->Ln(6);
          $pdf->putFieldName("Comentarios:",26);
          $pdf->putFilledField("",164);
          $pdf->Ln(6);
          $pdf->putFieldName("Mts. de cable",26);
          $pdf->putFilledField("",11);
          $pdf->putFieldName("¿Existe torre?",40);
          $pdf->putFilledField("",11);
          $pdf->putFieldName("¿Se necesita torre?",40);
          $pdf->putFilledField("",11);
          $pdf->putFieldName("¿Cuantos Mts.?",40);
          $pdf->putFilledField("",11);
          $pdf->Ln(6);
          $pdf->putFieldName("Antena de prueba",37);
          $pdf->putFilledField("",58);
          $pdf->putFieldName("Herramienta especial",40);
          $pdf->putFilledField("",55);
          $pdf->Ln(6);
          $pdf->putFieldName("Material especial",33);
          $pdf->putFilledField("",55);
          $pdf->putFieldName("Lugar para montar antena",47);
          $pdf->putFilledField("",55);
          $pdf->Ln(6);
          $pdf->putFieldName("Tiempo estimado de instalación",55);
          $pdf->putFilledField("",35);
          $pdf->putFieldName("Personas requeridas para instalación",65);
          $pdf->putFilledField("",35);
          $pdf->Ln(6);
          $pdf->putFieldName("Técnicos instaladores",40);
          $pdf->putFilledField("",150);
          $pdf->Ln(6);
          $pdf->putHeader("observaciones/pendientes");
          $pdf->putMultiCellField("",30);
        }
        
        $pdf->Ln(6);
        $pdf->putHeader($formValTitle);
        $pdf->putFieldName("Nombre:",20);
        $pdf->putFilledField("",95);
        // $pdf->putMultiCellField("", 30);
        $pdf->putFieldName("Firma:",15);
        $pdf->putFilledField("",60);
        // $pdf->putMultiCellField("", 30);
      }
      $pdf->Output(); 
    }    
  }    
}


a:
mysqli_close($db_modulos);
mysqli_close($db_catalogos);
mysqli_close($db_usuarios);
?>
