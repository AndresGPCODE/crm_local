<?php
session_start();

set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

//include_once "../../libs/db/dbcommon.php";
include_once "../../libs/db/common.php";

$gruposArr  = $_SESSION['grupos'];
$usuarioId  = $_SESSION['usrId'];
$usuario    = $_SESSION['usuario'];
$nombre     = $_SESSION['usrNombre'];

if(!verifySession($_SESSION)&&!isset($_POST["remFlag"])){
  logoutTimeout();
}else{

$cfgLogService = "../../log/ticketlog";

$db_catalogos = condb_catalogos();
$db_modulos   = condb_modulos();
$db_usuarios  = condb_usuarios();

$salida = "";

$modId = 6;

//obtiene lista de tickets y los ordena por topic
if("getTicketsListByTopic" == $_POST['action']){

  $query1 = "SELECT cate.*, top.* FROM cat_ticket_categorias AS cate, cat_ticket_topic AS top WHERE cate.ticket_categoria_id=top.ticket_categoria_id AND cate.ticket_categoria_id>0";
  
  $query2 = "SELECT * FROM cat_ticket_estatus";

  $result1 = mysqli_query($db_catalogos,$query1);
  $result2 = mysqli_query($db_catalogos,$query2);
  
  if(!$result1||!$result2)
    $salida = "ERROR||No se pudo obtener los datos de los topics";
  else{
    $salida = "OK||";
    $statArr = array();
    
    $salida .= "<div class=\"table-responsive\"><table id=\"tbl_ticketStatTopic\" class=\"table table-striped table-condensed\">\n".
              "  <thead>\n".
              "    <tr>\n".
              "      <td><b>Topic</b></td>\n";
              
    while($row2 = mysqli_fetch_assoc($result2)){
      $statArr[] = $row2['ticket_estatus_id'];
      $salida .= "      <td align=\"center\"><b>".$row2['estatus']."</b></td>\n";
    }
    $salida .= "      <td align=\"center\"><b>Total</b></td>\n";
    $salida .= "    </tr>\n".
               "  </thead>\n".
               "  <tbody>\n";
  
    while($row1 = mysqli_fetch_assoc($result1)){
      $total = 0;
      $salida .= "    <tr>\n".
                 "      <td><b><i>".$row1['categoria']."</i> - ".$row1['ticket_subcategoria']."</b></td>\n";
      foreach($statArr as $key => $value){
        
        $query3 = "SELECT COUNT(ticket_id) FROM tickets WHERE ticket_topic_id = ".$row1['ticket_topic_id']." AND estatus = ".$value;
        
        $result3 = mysqli_query($db_modulos,$query3);
        
        if(!$result3){
          $salida = "ERROR||Ocurrió un problema al obtener la cantidad de registros de los topics";
          goto ticketListByTopica;
        }
        else{          
          while($row3 = mysqli_fetch_row($result3)){
            $salida .= "      <td align=\"center\">".$row3[0]."</td>\n";
            $total += (int)$row3[0];
          }          
        }        
      }
      $salida .= "      <td align=\"center\">".$total."</td>\n";
      $salida .= "    </tr>\n";     
    }    
    $salida .= "  </tbody>\n".
               "</table></div>\n";
  }
  ticketListByTopica:
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida; 
}

//obtiene la lista de tickets y las ordena por el usuario que las agregó o le han sido asignados
if("getTicketsListByUser" == $_POST['action']){
    
  $query1 = "SELECT DISTINCT usr.nombre, usr.apellidos, usr.usuario_id ".
            "FROM ".$cfgTableNameUsr.".usuarios AS usr,".
            "".$cfgTableNameCat.".cat_ticket_departamento AS dep, ".
            "".$cfgTableNameCat.".cat_ticket_staff AS stf, ".
            "".$cfgTableNameUsr.".rel_usuario_grupo AS rel ".
            "WHERE (rel.grupo_id=dep.grupo_id OR stf.usuario_id=usr.usuario_id)".
            "AND rel.usuario_id=usr.usuario_id";
  
  $query2 = "SELECT * FROM cat_ticket_estatus";
  
  $result1 = mysqli_query($db_usuarios,$query1);
  $result2 = mysqli_query($db_catalogos,$query2);
  
  if(!$result1||!$result2){
    $salida = "ERROR||Ocurrió un problema al obtener los datos del usuario ".$query1;
  }
  else{
    $salida = "OK||";
    $statArr = array();
    
    $salida .= "<div class=\"table-responsive\"><table id=\"tbl_ticketStatUser\" class=\"table table-striped table-condensed\">\n".
              "  <thead>\n".
              "    <tr>\n".
              "      <td><b>Usuario</b></td>\n";
              
    while($row2 = mysqli_fetch_assoc($result2)){
      $statArr[] = $row2['ticket_estatus_id'];
      $salida .= "      <td align=\"center\"><b>".$row2['estatus']."</b></td>\n";
    }
    $salida .= "      <td align=\"center\"><b>Total</b></td>\n";
    $salida .= "    </tr>\n".
               "  </thead>\n".
               "  <tbody>";

    while($row1 = mysqli_fetch_assoc($result1)){
      $total = 0;
      $salida .= "    <tr>\n".
                 "      <td><b>".$row1['nombre']." ".$row1['apellidos']."</b></td>\n";
      foreach($statArr as $key => $value){
        
        $query3 = "SELECT COUNT(ticket_id) FROM tickets WHERE asignadoa = '".$row1['usuario_id']."' AND estatus = ".$value;
        
        $result3 = mysqli_query($db_modulos,$query3);
        
        if(!$result3){
          $salida = "ERROR||Ocurrió un problema al obtener la cantidad de registros de los topics ".$query3;
          goto ticketListByUsera;
        }
        else{          
          while($row3 = mysqli_fetch_row($result3)){
            $salida .= "      <td align=\"center\">".$row3[0]."</td>\n";
            $total += (int)$row3[0];
          }          
        }        
      }
      $salida .= "      <td align=\"center\">".$total."</td>\n";
      $salida .= "    </tr>\n";     
    }
    $salida .= "  </tbody>\n".
               "</table></div>\n";
  }
  ticketListByUsera:
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida; 
}

/*3 Asignado AD9600*/
//obtiene la lista de tickets y la ordena por estatus
if("getTicketsLists" == $_POST['action']){
  $option = isset($_POST['option']) ? $_POST['option'] : -1;
  
  if(1<$option){
    $query = "SELECT tic.*, cli.nombre_comercial, pri.ticket_prioridad, sla.ticket_sla, sla.sla_horas ".
             "FROM ".$cfgTableNameMod.".tickets AS tic, ".$cfgTableNameMod.".clientes AS cli, ".$cfgTableNameCat.".cat_ticket_prioridad AS pri, ".$cfgTableNameCat.".cat_ticket_sla AS sla ".
             "WHERE tic.estatus=".$option." AND cli.cliente_id=tic.cliente_id AND sla.ticket_sla_id=tic.ticket_sla_id AND pri.ticket_prioridad_id=tic.prioridad_id";
  }
  
  if(1==$option){//obtiene tickets abiertos o reabiertos
    $query = "SELECT tic.*, cli.nombre_comercial, pri.ticket_prioridad, sla.ticket_sla, sla.sla_horas ".
             "FROM ".$cfgTableNameMod.".tickets AS tic, ".$cfgTableNameMod.".clientes AS cli, ".$cfgTableNameCat.".cat_ticket_prioridad AS pri, ".$cfgTableNameCat.".cat_ticket_sla AS sla ".
             "WHERE (tic.estatus=".$option." OR tic.estatus=6) AND cli.cliente_id=tic.cliente_id AND sla.ticket_sla_id=tic.ticket_sla_id AND pri.ticket_prioridad_id=tic.prioridad_id";
  }
  
  if(0==$option){//obtiene tickets asignados a cierto usuario
    $query = "SELECT tic.*, cli.nombre_comercial, pri.ticket_prioridad, sla.ticket_sla, sla.sla_horas ".
             "FROM ".$cfgTableNameMod.".tickets AS tic, ".$cfgTableNameMod.".clientes AS cli, ".$cfgTableNameCat.".cat_ticket_prioridad AS pri, ".$cfgTableNameCat.".cat_ticket_sla AS sla ".
             "WHERE tic.asignadoa=".$usuarioId." AND cli.cliente_id=tic.cliente_id AND sla.ticket_sla_id=tic.ticket_sla_id AND pri.ticket_prioridad_id=tic.prioridad_id";
  }  
  
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $salida = "ERROR||Ocurrió un problema al obtener los datos de los tickets";
  }
  else{  
    $salida = "OK||<div class=\"table-responsive\"><table id=\"tbl_ticketTick_".$option."\" class=\"table table-striped table-condensed\">\n".
              "  <thead>\n".
              "    <tr>\n".
              "      <td class=\"hidden-xs\"><b>Ticket</b></td>\n".
              "      <td><b>Fecha Limite</b></td>\n".
              "      <td><b>Asunto</b></td>\n".
              "      <td><b>SLA</b></td>\n".
              "      <td><b>De</b></td>\n".
              "      <td><b>Prioridad</b></td>\n".
              "      <td><b>Acciones</b></td>\n".
              "    </tr>\n".
              "  </thead>\n".
              "  <tbody>";
    while($row = mysqli_fetch_assoc($result)){
      $salida .= "  <tr id=\"tr_tick_".encrypt($row['ticket_id'])."\">\n".
                 "    <td class=\"hidden-xs\">".$row['ticket_id']."</td>\n"; 
                 
      $fechaLim = strtotime($row['fecha_limite']);     
      $fechaNow = date("Y-m-d H:i:s");
      
      $tiempoRes = round(($fechaLim - strtotime($fechaNow))/3600);
      
      $interUnit = (int)$row['sla_horas']/3;
      
      if((0>=$tiempoRes)&&(5!=$row['estatus'])){
        $salida .= "    <td>".$row['fecha_limite']."<span class=\"glyphicon glyphicon-alert\" style=\"color:red\" title=\"Atrasado\"></span></td>\n";
      }
      else if(($interUnit>=$tiempoRes)&&(0<$tiempoRes)&&(5!=$row['estatus'])){
        $salida .= "    <td>".$row['fecha_limite']."<span class=\"glyphicon glyphicon-alert\" style=\"color:orange\" title=\"Próximo a atrasarse\"></span></td>\n";
      }
      else if(5==$row['estatus']){
        $salida .= "    <td>".$row['fecha_limite']."<span class=\"glyphicon glyphicon-ok\" style=\"color:green\" title=\"Ticket cerrado\"></span></td>\n";
      }
      else{
        $salida .= "    <td>".$row['fecha_limite']."</td>\n";
      }
      
      $salida .= "    <td>".$row['titulo']."</td>\n".
                 "    <td>".$row['ticket_sla']."</td>\n".
                 "    <td>".$row['nombre_comercial']."</td>\n".  
                 "    <td>".$row['ticket_prioridad']."</td>\n".
                 "    <td>".                
                 //"        <button type=\"button\" id=\"btn_ticketDetails_".$row['ticket_id']."\" title=\"Ver Detalles\" class=\"btn btn-xs btn-primary btn-responsive\" onClick=\"getTicketDetails('".encrypt($row['ticket_id'])."'); getTicketJournal('".encrypt($row['ticket_id'])."');\"><span class=\"glyphicon glyphicon-list\"></span></button>".
                 "        <button type=\"button\" id=\"btn_ticketDetails_".$row['ticket_id']."\" title=\"Ver Detalles\" class=\"btn btn-xs btn-primary btn-responsive\" onClick=\"goToTicketDetails('".encrypt($row['ticket_id'])."');\"><span class=\"glyphicon glyphicon-list\"></span></button>";
     if(hasPermission($modId,'d')){     
       $salida.= "        <button type=\"button\" id=\"btn_ticketDelete_".$row['ticket_id']."\" title=\"Eliminar Ticket\" class=\"btn btn-xs btn-danger btn-responsive\" onClick=\"deleteTicket('".encrypt($row['ticket_id'])."',6);\"><span class=\"glyphicon glyphicon-remove\"></span></button>";
     }
     $salida.= "    </td>\n".            
               "  </tr>\n";
    }
    $salida .= "  </tbody>\n".
               "</table></div>\n";
  }
  
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida; 
}

//obtiene la cantidad de tickets que tiene una categoría
if("getTicketNumbers" == $_POST['action']){
  
  $option = isset($_POST['option']) ? $_POST['option'] : -1;
  
  if(0!=$option&&1!=$option)
    $query = "SELECT COUNT(ticket_id) FROM tickets WHERE estatus=".$option;
  if(1==$option)
    $query = "SELECT COUNT(ticket_id) FROM tickets WHERE (estatus=".$option." OR estatus=6)";
  if(0==$option)
    $query = "SELECT COUNT(ticket_id) FROM tickets WHERE asignadoa=".$usuarioId;
    
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $salida = "ERROR||Ocurrió un problema al obtenerse la cantidad de tickets para una categoría";
  }
  else{
    $row = mysqli_fetch_row($result);
    
    $salida = "OK||".$row[0];
  }

  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida; 
}

//obtiene los datos para desplegarse en el dashboard y los acomoda en un arreglo JSON para que la librería del dashboard lo despliegue
if("getTicketDash" == $_POST['action']){

  //$filterId = isset($_POST['filterId']) ? $_POST['filterId'] : -1;
  //if(hasPermission($orgType,'r')||(hasPermission($orgType,'l')&&$row['usuario_id']==$usuarioId)){
  if(hasPermission($modId,'r')||hasPermission($modId,'l')){
    
    $fecFin = isset($_POST['fecFin']) ? date($_POST['fecFin']) : date("Y-m-d");
    $fecIni = isset($_POST['fecIni']) ? date($_POST['fecIni']) : date("Y-m-d",strtotime($fecFin." -1 month"));
     
    /*$fecFin = date("Y-m-d");  
    $fecIni = date("Y-m-d",strtotime($fecFin." -1 month"));*/
    
    $queryStat = "SELECT * FROM cat_ticket_estatus";
    
    $resultStat = mysqli_query($db_catalogos,$queryStat);
    
    $dataDash = array();
    $idx = 0;

    while($rowStat = mysqli_fetch_assoc($resultStat)){
      $dataDash[$rowStat['ticket_estatus_id']] = array("label" => $rowStat['estatus'],
                                                       "color" => "#".$rowStat['ticket_estatus_color'],
                                                       "idx"   => $rowStat['ticket_estatus_id'],
                                                       "data"  => "");
    
      $query = "SELECT est.*, dash.fecha, dash.estatus, dash.cantidad FROM ".$cfgTableNameCat.".cat_ticket_estatus AS est, ".$cfgTableNameMod.".ticket_dashboard_data AS dash WHERE dash.estatus=est.ticket_estatus_id AND dash.estatus=".$rowStat['ticket_estatus_id'].
               " AND dash.fecha>='".$fecIni."' AND dash.fecha<='".$fecFin."' ORDER BY dash.fecha ASC";
      
      $result = mysqli_query($db_modulos,$query);
      
      if(!$result){
        $salida = "ERROR||Ocurrió un problema al obtenerse los datos de los tickets para su despliegue en el dashboard";
      }
      else{ 
        $data = array();
        while($row = mysqli_fetch_assoc($result)){
          $data[] = array((((int)strtotime($row['fecha']))*1000),(int)$row['cantidad']);     
        }
        $dataDash[$rowStat['ticket_estatus_id']]['data'] = $data;
      }
      $idx++;  
    }    
    $salida = "OK||".json_encode($dataDash);
  }
  else{
    $salida = "ERROR||No cuenta con permisos para acceder a estos datos";
  } 
  
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}

//obtiene la lista de estatus y los despliega como filtros (asignado, cerrado, atrasado, etc)
if("getTicketFilters"==$_POST['action']){
  if(hasPermission($modId,'r')||hasPermission($modId,'l')){
    $query = "SELECT * FROM cat_ticket_estatus";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
      $salida = "ERROR||Ocurrió un problema al obtener los estatus de tickets para su filtrado";  
    }
    else{
      while($row=mysqli_fetch_assoc($result)){
        if(0!=$row['ticket_estatus_id']){
          $salida .= "<span class=\"spn_filtercolor\" style=\"background-color:#".$row['ticket_estatus_color']."\">&nbsp; &nbsp; &nbsp; </span>&nbsp; ".
                     "<label class=\"checkbox-inline\">".
                     "  <input type=\"checkbox\" class=\"chk_filtersTick\" id=\"chk_ticketType_".$row['ticket_estatus_id']."\" value=".$row['ticket_estatus_id']." onchange=\"plotFilter()\">".$row['estatus'].
                     "</label>&nbsp;&nbsp;&nbsp;";
        }     
      }
      //$salida .="<br>";
    }
  }
  else{
    $salida = "<b>ERROR:</b> No cuenta con permisos para acceder a estos datos";
  }  
  mysqli_close($db_catalogos);
  echo $salida;
}


if("getTicketDetails"==$_POST['action']){
  $tickId = isset($_POST['ticketId']) ? decrypt($_POST['ticketId']) : -1; 
  $option = isset($_POST['option']) ? $_POST['option'] : -1;
               
  $query = "SELECT tick.*, sit.nombre, sit.telefono, cte.nombre_comercial, grp.grupo, sla.ticket_sla, sla.sla_horas, sta.ticket_estatus_id, sta.estatus, top.ticket_subcategoria, top.es_otro, org.ticket_origen, pri.ticket_prioridad, cat.categoria ".
           "FROM ".$cfgTableNameMod.".tickets AS tick, ".
           $cfgTableNameMod.".clientes AS cte, ".
           $cfgTableNameMod.".sitios AS sit, ".
           $cfgTableNameUsr.".grupos AS grp, ".
           $cfgTableNameCat.".cat_ticket_sla AS sla, ".
           $cfgTableNameCat.".cat_ticket_estatus AS sta, ".
           $cfgTableNameCat.".cat_ticket_topic AS top, ".
           $cfgTableNameCat.".cat_ticket_origen AS org, ".
           $cfgTableNameCat.".cat_ticket_prioridad AS pri, ".
           $cfgTableNameCat.".cat_ticket_categorias AS cat, ".
           $cfgTableNameCat.".cat_ticket_departamento AS dep ".
           "WHERE tick.ticket_id=".$tickId." ".
           "AND sit.sitio_id=tick.sitio_id ".
           "AND cte.cliente_id=tick.cliente_id ".
           "AND grp.grupo_id=dep.grupo_id ".
           "AND tick.departamento_id=dep.ticket_departamento_id ".
           "AND dep.grupo_id=grp.grupo_id ".
           "AND sla.ticket_sla_id=tick.ticket_sla_id ".
           "AND sta.ticket_estatus_id=tick.estatus ".
           "AND tick.ticket_topic_id=top.ticket_topic_id ".
           "AND top.ticket_categoria_id=cat.ticket_categoria_id ".
           "AND tick.ticket_origen_id=org.ticket_origen_id ".
           "AND tick.prioridad_id=pri.ticket_prioridad_id";          
  
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $salida = "<b>Error||</b> Ocurrió un problema al obtenerse los datos del ticket ";
  }
  else{
    if(0==mysqli_num_rows($result)){
      $salida = "<div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-remove-sign\" style=\"color:red\" title=\"Atrasado\"></span> <b>Error:</b> No se encontró el ticket con id: $tickId</div>";
    }
    else{
      $row = mysqli_fetch_assoc($result);
      
      if(0==$option){
        //$salida = "OK||";
        $fechaLim = strtotime($row['fecha_limite']);     
        $fechaNow = date("Y-m-d H:i:s");
        
        $tiempoRes = round(($fechaLim - strtotime($fechaNow))/3600);
        
        $interUnit = (int)$row['sla_horas']/3;
        
        $salida .= "<div class=\"col col-xs-12 col-lg-12\">".
                  "  <h4><b>Ticket #".$tickId."</b></h4>".
                  "</div><br>";    
                  
        $salida .= "<div class=\"col col-xs-12 col-lg-12\" align=\"right\">";                  
        if(hasPermission($modId,'e')||(hasPermission($modId,'n')&&$row['asignadoa']==$usuarioId)){
          if(5!=$row['ticket_estatus_id']){           
            $salida .= "  <button type=\"button\" id=\"btn_ticketEdit_".$tickId."\" title=\"Editar Ticket\" class=\"btn btn-xs btn-success\" onClick=\"ticketEdit(".$tickId.");\"><span class=\"glyphicon glyphicon-edit\"> Editar</span></button>";
              $salida .= "  <button type=\"button\" id=\"btn_ticketClose_".$tickId."\" title=\"Cerrar Ticket\" class=\"btn btn-xs btn-default\" onClick=\"openTicketClose();\"><span class=\"glyphicon glyphicon-record\"> Cerrar</span></button>";
          }
          else{
            $salida .= "  <button type=\"button\" id=\"btn_ticketEdit_".$tickId."\" title=\"Editar Ticket\" class=\"btn btn-xs btn-success\" onClick=\"\" disabled><span class=\"glyphicon glyphicon-edit\" > Editar</span></button>";
              $salida .= "  <button type=\"button\" id=\"btn_ticketReopen_".$tickId."\" title=\"Reabrir Ticket\" class=\"btn btn-xs btn-default\" onClick=\"openTicketReopen();\"><span class=\"glyphicon glyphicon-record\"> Reabrir</span></button>";
          }
        }
        
        if(hasPermission($modId,'d')){     
           $salida.= "        <button type=\"button\" id=\"btn_ticketDelete_".$tickId."\" title=\"Eliminar Ticket\" class=\"btn btn-xs btn-danger btn-responsive\" onClick=\"deleteTicket('".encrypt($tickId)."',6);\"><span class=\"glyphicon glyphicon-remove\"> Eliminar</span></button>";
        }
        if(7!=$row['ticket_estatus_id']){      
        $salida .= "  <button type=\"button\" id=\"btn_ticketAddReply_".$tickId."\" title=\"Responder\" class=\"btn btn-xs btn-info\" onClick=\"ticketAddReply();\"><span class=\"glyphicon glyphicon-share\"> Responder</span></button>".
                   "  <button type=\"button\" id=\"btn_ticketAddNote_".$tickId."\" title=\"Agregar Nota Interna\" class=\"btn btn-xs btn-primary\" onClick=\"ticketAddIntNote();\"><span class=\"glyphicon glyphicon-edit\"> Nota Interna</span></button>";                 
        }
        else{
          $salida .= "  <button type=\"button\" id=\"btn_ticketSetSite_".$tickId."\" title=\"Asignar a Sitio\" class=\"btn btn-xs btn-default\" onClick=\"displayOrgSites(".$tickId.",6,0);\"><span class=\"glyphicon glyphicon-tower\"><span class=\"glyphicon glyphicon-arrow-left\"> Asignar a Sitio</span></span></button>";
        }
        $salida .= "</div><br><hr>";
        
        if((0>=$tiempoRes)&&(5!=$row['ticket_estatus_id'])){
          $salida .= "<div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-alert\" style=\"color:red\" title=\"Atrasado\"></span> <b>Atención:</b> ¡Este ticket tiene retraso!</div>";
        }
        else if(($interUnit>=$tiempoRes)&&(5!=$row['ticket_estatus_id'])){
          $salida .= "<div class=\"alert alert-warning\"><span class=\"glyphicon glyphicon-alert\" style=\"color:orange\" title=\"Próximo a atrasarse\"> <b>Atención:</b> Este ticket está próximo a atrasarse</div>";
        }
        else if(5==$row['ticket_estatus_id']){
          $salida .= "<div class=\"alert alert-success\"><span class=\"glyphicon glyphicon-ok\" style=\"color:green\" title=\"Próximo a atrasarse\"> <b>OK:</b> Este ticket se encuentra cerrado</div>";
        }
            
        $salida .= "<div class=\"col col-xs-12 col-lg-12\">".
                   "  <div class=\"col col-xs-6 col-lg-2 div_tickBgr1\"><b>Estatus:</b></div>".
                   "  <div class=\"col col-xs-6 col-lg-4\">".$row['estatus']."</div>".
                   "  <div class=\"col col-xs-6 col-lg-2 div_tickBgr1\"><b>Prioridad:</b></div>".
                   "  <div class=\"col col-xs-6 col-lg-4\">".$row['ticket_prioridad']."</div>".
                   "</div><br>".
                   "<div class=\"col col-xs-12 col-lg-12\">".
                   "  <div class=\"col col-xs-6 col-lg-2 div_tickBgr1\"><b>Departamento:</b></div>".
                   "  <div class=\"col col-xs-6 col-lg-4\">".$row['grupo']."</div>".
                   "  <div class=\"col col-xs-6 col-lg-2 div_tickBgr1\"><b>Fecha Alta:</b></div>".
                   "  <div class=\"col col-xs-6 col-lg-4\">".$row['fecha_alta']."</div>".
                   "</div><br>".
                   "<div class=\"col col-xs-12 col-lg-12\">".
                   "  <div class=\"col col-xs-6 col-lg-2 div_tickBgr1\"><b>SLA:</b></div>".
                   "  <div class=\"col col-xs-6 col-lg-4\">".$row['ticket_sla']."</div>".
                   "   <div class=\"col col-xs-6 col-lg-2 div_tickBgr1\"><b>Sitio:</b></div>".
                   "  <div class=\"col col-xs-6 col-lg-4\">".$row['nombre']."</div>".
                   "</div><br>";
                  
        if(5==$row['ticket_estatus_id']){ //si está cerrado el ticket   
          $closeUsr = get_userRealName($row['ultima_act_usuario']);
          $salida .= "<div class=\"col col-xs-12 col-lg-12\">".
                     "  <div class=\"col col-xs-6 col-lg-2 div_tickBgr1\"><b>Cerrado por:</b></div>".
                     "  <div class=\"col col-xs-6 col-lg-4\">".$closeUsr."</div>".
                     "  <div class=\"col col-xs-6 col-lg-2 div_tickBgr1\"><b>Fecha de Cierre:</b></div>".
                     "  <div class=\"col col-xs-6 col-lg-4\">".$row['ultima_act']."</div>".
                     "</div><br>";
        } 
        else{ //si el ticket sigue abierto o con otro estatus
          $assignUsr = get_userRealName($row['asignadoa']);
          $salida .= "<div class=\"col col-xs-12 col-lg-12\">".
                     "  <div class=\"col col-xs-6 col-lg-2 div_tickBgr1\"><b>Asignado a:</b></div>".      
                     "  <div class=\"col col-xs-6 col-lg-4\">".$assignUsr."</div>".
                     "  <div class=\"col col-xs-6 col-lg-2 div_tickBgr1\"><b>Fecha Límite:</b></div>".
                     "  <div class=\"col col-xs-6 col-lg-4\">".$row['fecha_limite']."</div>".
                     "</div><br>";         
        }                    
          $salida .= "<div class=\"col col-xs-12 col-lg-12\">". 
                     "  <div class=\"col col-xs-6 col-lg-2 div_tickBgr1\"><b>Cliente:</b></div>".
                     "  <div class=\"col col-xs-6 col-lg-4\">".$row['nombre_comercial']."</div>".
                     "  <div class=\"col col-xs-6 col-lg-2 div_tickBgr1\"><b>Teléfono:</b></div>".
                     "  <div class=\"col col-xs-6 col-lg-4\">".$row['telefono']."</div>".
                     "</div><br>".
                     "<div class=\"col col-xs-12 col-lg-12\">".
                     "  <div class=\"col col-xs-6 col-lg-2 div_tickBgr1\"><b>Fuente:</b></div>";
        if(3==$row['ticket_origen_id']){
          $salida .= "  <div class=\"col col-xs-6 col-lg-4\">".$row['ticket_origen_otro']."</div>";
        }
        else{
          $salida .= "  <div class=\"col col-xs-6 col-lg-4\">".$row['ticket_origen']."</div>";
        }
          
          $salida .= "  <div class=\"col col-xs-6 col-lg-2 div_tickBgr1\"><b>Topic:</b></div>";
          
        if(0==$row['es_otro']){
          $salida .= "  <div class=\"col col-xs-6 col-lg-4\">".$row['categoria']." - ".$row['ticket_subcategoria']."</div>";
        }
        else{
          $salida .= "  <div class=\"col col-xs-6 col-lg-4\">".$row['categoria']." - ".$row['ticket_topic_otro']."</div>";
        }
          $salida .= "</div><br>";
                     /*"  <div class=\"col col-xs-6 col-lg-3\"><b>Último Mensaje:</b></div>".
                     "  <div class=\"col col-xs-6 col-lg-3\">".$row['']."</div>".
                     "  <div class=\"col col-xs-6 col-lg-3\"><b>Última Respuesta:</b></div>".
                     "  <div class=\"col col-xs-6 col-lg-3\">".$row['']."</div>".*/
                    // "</div>";   
      }
      if(1==$option){
        $salida = "OK||".
                  $row['ticket_origen_id']."||".
                  $row['ticket_topic_id']."||".
                  $row['departamento_id']."||".
                  $row['ticket_sla_id']."||".
                  $row['asignadoa']."||".
                  $row['titulo']."||".
                  $row['detalles']."||".
                  $row['prioridad_id']."||".
                  $row['ticket_origen_otro']."||".
                  $row['ticket_topic_otro'];
      } 
    }
              
  } 
  tickDetailsa:
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}


//obtiene la bitácora de un ticket
if("getTicketJournal"==$_POST['action']){
  $tickId = isset($_POST['ticketId']) ? decrypt($_POST['ticketId']) : -1;
               
  $query = "SELECT bit.*, tip.* FROM ".$cfgTableNameMod.".ticket_bitacora_posts AS bit, ".$cfgTableNameCat.".cat_ticket_tipo_post AS tip WHERE ticket_id=".$tickId." AND tip.tipo_post_id=bit.bitacora_post_tipo ORDER BY bit.fecha_alta DESC";
  
  /*$query = "SELECT bit.*, tip.* ".
           "FROM ".$cfgTableNameMod.".ticket_bitacora_posts AS bit, ".
            $cfgTableNameCat.".cat_ticket_tipo_post AS tip, ".
            $cfgTableNameMod.".ticket_adjuntos AS att ".
            "WHERE ticket_id=".$tickId." ".
            "AND tip.tipo_post_id=bit.bitacora_post_tipo ".
            "AND att.posy_id=bit.post_id ".
            "ORDER BY bit.fecha_alta DESC";*/
   
  $result = mysqli_query($db_modulos,$query);  
  
  if(!$result){
    $salida = "<b>ERROR:</b> Ocurrió un problema al obtenerse los datos de la bitácora para este ticket ".$query;
    goto ticketJournala;
  }
  else{
    //$salida = "OK||";
    $numRows = mysqli_num_rows($result);
    $salida .= "<div class=\"col col-xs-12 col-lg-12\"><br><h4><b>Hilo de este ticket (".$numRows." replicas)</b></h4></div><br><hr>";
    while($row = mysqli_fetch_assoc($result)){
    
      if('c'==$row['agregado_por'][0]){//un cliente lo agregó
        $query2 = "SELECT nombre FROM sitios WHERE sitio_id=".substr($row['agregado_por'],1);
        $result2 = mysqli_query($db_modulos,$query2);
        if(!$result2){
          $salida = "<b>ERROR:</b> Ocurrió un problema al obtenerse los datos de la bitácora para este ticket";
          goto ticketJournala;
        }
        else{
          $row2 = mysqli_fetch_assoc($result2);
          $agrego = $row2['nombre'];
        }
      }
      if('u'==$row['agregado_por'][0]){//un usuario lo agregó
        $agrego = get_userRealName(substr($row['agregado_por'],1));
      }
      
      $query3 = "SELECT * FROM ticket_adjuntos WHERE post_id =".$row['post_id'];
      
      $result3 = mysqli_query($db_modulos,$query3);
    
      $salida .= "<div class=\"panel small col col-xs-12 col-lg-12\">".
                 "  <div class=\"panel-heading\" style=\"background-color:#".$row['post_color']."; border:1px solid;\">".
                 "    <b>".$row['fecha_alta']."</b>&nbsp;&nbsp;".
                 "    <i>".$row['asunto']."</i>".
                 "    <span class=\"pull-right\">".$agrego."</span>".
                 "  </div>".
                 "  <div class=\"panel-body\" style=\"border-color:#000; border:1px solid;\">".$row['mensaje'];
                 
       if(0<mysqli_num_rows($result3)){ 
        $salida .= "<hr> Adjuntos: <br><i class=\"fa fa-paperclip\"></i> "; 
               
        while($row3 = mysqli_fetch_assoc($result3)){
          $filePathArr = explode('/',$row3['adjunto_ruta']);
          $fileNamePos = (count($filePathArr))-1;
          $filename = $filePathArr[$fileNamePos];
          $salida .= "<a href=\"".$row3['adjunto_ruta']."\" target=\"_blank\">".$filename."</a><br>";
        } 
      }               
      $salida .= "</div>";
      
      $salida .= "</div>";   
                 
           
    }
  }
  ticketJournala:
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}

//llamada de ajax. cierra o reabre el ticket y escribe el comentario de quien cerró en la bitácora (si lo puso)
if("closeTicket"==$_POST['action']){
  $tickId = isset($_POST['ticketId']) ? decrypt($_POST['ticketId']) : -1;
  $tickCom = isset($_POST['ticketCloseComment']) ? $_POST['ticketCloseComment'] : "";
  $tickAction = isset($_POST['ticketCloseAction']) ? $_POST['ticketCloseAction'] : "";
  
  $fecha = date("Y-m-d H:i:s");
  
  if(0==$tickAction){
    $estatus = 5;
    $sta = "cerrado";
  }
  if(1==$tickAction){
    $estatus = 6;
    $sta = "reabierto";
  }
  $query = "UPDATE tickets SET estatus=".$estatus.", ultima_act='".$fecha."', ultima_act_usuario=".$usuarioId." WHERE ticket_id=".$tickId;

  $result = mysqli_query($db_modulos,$query);
  $usrCierre = get_userRealName($usuarioId);
  
  if(!$result){
    $salida = "ERROR||Ocurrió un problema al cerrarse el ticket";
  }
  else{
    $salida = "OK||El ticket se ha cerrado con éxito";
    if(0<sizeof($tickCom)){
      $comment = str_replace("'","''",$tickCom);
    }
    else{
      $comment = "Sin Comentarios";
    }
    $tickJour = insertTicketJournal("Ticket ".$sta,$tickId,"El Ticket ha sido ".$sta." por: ".$usrCierre." (<i>".$comment."</i>)",'u'.$usuarioId,4);
    if(false!==moveImgsToFtp($tickId,$tickJour,$tickCom,3)){
      sendTicketEmail($tickJour,7,"");
    }
    else{
      $salida = "ERROR||No se trasladó el archivo al servidor remoto, no se pudo enviar la notificación por correo";
    }       
  }
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}

if("saveInterNote"==$_POST['action']){
  log_write("DEBUG: SAVE-INTER-NOTE: Se va a agregar una nota interna",8);

  $tickId = isset($_POST['ticketId']) ? decrypt($_POST['ticketId']) : -1;
  $tickNot = isset($_POST['ticketNote']) ? $_POST['ticketNote'] : "";
  $tickAction = isset($_POST['ticketNoteAction']) ? $_POST['ticketNoteAction'] : "";
  
  $comment = str_replace("'","''",$tickNot);
  $comment = str_replace("<p>","",$tickNot);
  $comment = str_replace("</p>","",$tickNot);

  $tickJour = insertTicketJournal('Nota Interna',$tickId,$comment,'u'.$usuarioId,3);
       
  if(!$tickJour){
    log_write("ERROR: SAVE-INTER-NOTE: Ocurrió un problema al agregarse la nota interna para este ticket",8);
    $salida = "ERROR||Ocurrió un problema al agregarse la nota interna para este ticket";
  }
  else{
    log_write("OK: SAVE-INTER-NOTE: Se ha agregado la nota interna con éxito Nota id: [".$tickJour."]",8);
    log_write("OK: SAVE-INTER-NOTE: Se va a enviar un correo",8);
    
    if(false!==moveImgsToFtp($tickId,$tickJour,$tickNot,2)){
      sendTicketEmail($tickJour,5,"");
      $salida = "OK||Se ha agregado la nota interna con éxito";      
    }
    else{
      $salida = "ERROR||No se trasladó el archivo al servidor remoto, no se pudo enviar la notificación por correo";
    }  
  }
  
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}

if("getTicketAssoc"==$_POST['action']){
  $tickId = isset($_POST['ticketId']) ? decrypt($_POST['ticketId']) : -1;
  $option = isset($_POST['option']) ? $_POST['option'] : -1;
  
  $query = "SELECT * FROM ticket_asociados WHERE ticket_id=".$tickId;
  
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $salida = "ERROR||ocurrió un problema en la obtención de los datos de asociados al ticket";
  }
  else{
    if(0==$option){
      $salida = "OK||<label>Enviar a asociados:</label><br><br>\n";
    }
    else{
      $salida = "<br><br><label>Asociados a este ticket:</label><br>\n".
                "  <button type=\"button\" id=\"btn_ticketAssocNew_".$tickId."\" title=\"Nuevo Asociado\" class=\"btn btn-sm btn-default\" onClick=\"insertTicketAssoc(".$tickId.");\"><span class=\"glyphicon glyphicon-asterisk\">Nuevo Asociado</span></button><br><br>";
    }
    
    if(0>=mysqli_num_rows($result)){
      if(0==$option){
        $salida = "OK||";
      }
      $salida .= "<i>Sin asociados</i>";
    }
    else{
      $salida .= "<div class=\"table-responsive\"><table id=\"tbl_ticketAssocList_$option\" class=\"table table-striped table-condensed\">\n".
                 "  <thead>\n".
                 "    <tr>\n".
                 "      <th>Nombre</th>\n".
                 "      <th>E-mail</th>\n".
                 "      <th>Acciones</th>\n".
                 "    </tr>\n".
                 "  </thead>\n".
                 "  <tbody>\n";                
      while($row = mysqli_fetch_assoc($result)){
        $salida .= "  <tr>\n".
                   "    <td>".$row['asociado_nombre']."</td>\n".
                   "    <td>".$row['asociado_email']."</td>\n".
                   "    <td align=\"center\">\n";
        if(0==$option){
          $salida .= "      <input type=\"checkbox\" id=\"chk_ticketAssoc_".$row['ticket_asociado_id']."\" class=\"chk_filtersTick\"  name=\"chk_ticketAssoc\" value=".$row['ticket_asociado_id']."/>\n";
        } 
        if(1==$option&&hasPermission($modId,'e')){
          $salida .= "      <button type=\"button\" id=\"btn_ticketAssocEdit_".$row['ticket_asociado_id']."\" title=\"Editar Asociado\" class=\"btn btn-xs btn-success\" onClick=\"ticketAssocEdit(".$row['ticket_asociado_id'].");\"><span class=\"glyphicon glyphicon-edit\"></span></button>";
        }          
        if(1==$option&&hasPermission($modId,'d')){
          $salida .= "      <button type=\"button\" id=\"btn_ticketAssocDelete_".$row['ticket_asociado_id']."\" title=\"Eliminar Asociado\" class=\"btn btn-xs btn-danger\" onClick=\"ticketAssocDelete(".$row['ticket_asociado_id'].");\"><span class=\"glyphicon glyphicon-remove\"></span></button>";
        }
        $salida .= "    </td>\n".
                   
                   "  </tr>\n";      
      }
      $salida .= "  </tbody>\n".
                 "</table></div>\n";
    }
  }    
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}

//obtiene los detalles de un asociado para su edición
if("getTicketAssocDetails"==$_POST['action']){
  $option = isset($_POST['option']) ? $_POST['option'] : -1;
  $assocId = isset($_POST['assocId']) ? $_POST['assocId'] : -1;
  
  log_write("DEBUG: GET-TICKET-ASSOC-DETAILS: Asociado ID"."<pre>".$assocId,8);
  
  $query = "SELECT asociado_nombre, asociado_email FROM ticket_asociados WHERE ticket_asociado_id=".$assocId;
  
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    log_write("ERROR: GET-TICKET-ASSOC-DETAILS: Ocurrió un problema al obtenerse el asociado de ticket para su edición",8);
    $salida = "ERROR||Ocurrió un problema al obtenerse el asociado de ticket para su edición";
  }
  else{
    $row = mysqli_fetch_assoc($result);
    $salida = "OK||".$row['asociado_nombre']."||".$row['asociado_email'];
  }
  
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}

if("saveAssocNew"==$_POST['action']){

  $tickId = isset($_POST['ticketId']) ? decrypt($_POST['ticketId']) : -1;
  $assocName = isset($_POST['tickAssocName']) ? $_POST['tickAssocName'] : "";
  $assocEmail = isset($_POST['tickAssocEmail']) ? $_POST['tickAssocEmail'] : "";
  $assocId = isset($_POST['tickAssocId']) ? $_POST['tickAssocId'] : -1;
  
  log_write("ERROR: SAVE-ASSOC-NEW: Se guardará/editará asociado",8);
  
  if(-1==$assocId||0==$assocId){
    $res = insertTicketAssociate($assocName,$assocEmail,$tickId,0);
    if(false===$res)
     $salida = "ERROR|| Ocurrió un error al insertarse los datos del asociado";
    else{
      $salida = "OK|| El usuario se insertó con éxito";
    }
  }  
  else{
    $query = "UPDATE ticket_asociados SET asociado_nombre='".$assocName."', asociado_email='".$assocEmail."' WHERE ticket_asociado_id=".$assocId;    
    $result = mysqli_query($db_modulos,$query);
    
    log_write("ERROR: SAVE-ASSOC-NEW: UpdateQuery: ".$query,8);
    
    if(!$result){
      log_write("ERROR: SAVE-ASSOC-NEW: Ocurrió un error al actualizarse los datos del asociado ".mysqli_error($db_modulos),8);
      $salida = "ERROR|| Ocurrió un error al actualizarse los datos del asociado";
    }
    else{
      log_write("OK: SAVE-ASSOC-NEW: Se actualizaron los datos del asociado",8);
      $salida = "OK|| Se actualizaron los datos del asociado";
    }
  }
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}

if("deleteTicketAssoc"==$_POST['action']){
  $assocId = isset($_POST['assocId']) ? $_POST['assocId'] : -1;
  
  $query = "DELETE FROM ticket_asociados WHERE ticket_asociado_id=".$assocId;
  
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    log_write("ERROR: DELETE-TICKET-ASSOC: Ocurrió un error al eliminarse los datos del asociado",8);
    $salida = "ERROR|| Ocurrió un error al eliminarse los datos del asociado";
  }
  else{
    log_write("OK: DELETE-TICKET-ASSOC: Se eliminaron los datos del asociado",8);
    $salida = "OK|| Se eliminaron los datos del asociado";
  }
  
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}

//guarda una respuesta
if("saveReply"==$_POST['action']){

  log_write("DEBUG: SAVE-REPLY: Se va a agregar una respuesta",8);
   
  $tickId     = isset($_POST['ticketId'])          ? decrypt($_POST['ticketId']) : -1;
  $tickRep    = isset($_POST['ticketReply'])       ? $_POST['ticketReply']       : "";
  $tickRepOrg = isset($_POST['ticketRepOrg'])      ? $_POST['ticketRepOrg'] : 2;//si se manda via crm por empleado o por cte email
  $assocArr   = isset($_POST['assocArray'])        ? json_decode($_POST['assocArray'],true) : "";
  
  log_write("DEBUG: SAVE-REPLY: Lista de asociados"."<pre>".print_r($assocArr,true)."</pre>",8);
  
  $comment = str_replace("'","''",$tickRep);
  $comment = str_replace("<p>","",$tickRep);
  $comment = str_replace("</p>","",$tickRep);
  
  $query = "SELECT titulo, cliente_id FROM tickets WHERE ticket_id=".$tickId;
  
  $result = mysqli_query($db_modulos,$query);
  
  log_write("DEBUG: SAVE-REPLY: ".$query,8);
  
  if(!$result){
    log_write("ERROR: SAVE-REPLY: Ocurrió un problema al obtenerse el asunto de ticket para su respuesta",8);
    $salida = "ERROR||Ocurrió un problema al obtenerse el asunto de ticket para su respuesta";
  }
  else{
    log_write("DEBUG: SAVE-REPLY: Se va a guardar en la bitácora la respuesta",8);
    $row = mysqli_fetch_assoc($result);
    
    if(isset($_POST["remFlag"]))
      $tickJour = insertTicketJournal('Re: '.$row['titulo'].' [#'.$tickId.']',$tickId,$tickRep,'c'.$row['cliente_id'],$tickRepOrg);
    else
      $tickJour = insertTicketJournal('Re: '.$row['titulo'].' [#'.$tickId.']',$tickId,$tickRep,'u'.$usuarioId,$tickRepOrg);
         
    if(!$tickJour){
      log_write("ERROR: SAVE-REPLY: Ocurrió un problema al guardarse la respuesta para este ticket ".$tickJour,8);
      $salida = "ERROR||Ocurrió un problema al guardarse la respuesta para este ticket";
    }
    else{
      log_write("OK: SAVE-REPLY: Se ha agregado la respuesta con éxito",8);      
      if(false!==moveImgsToFtp($tickId,$tickJour,$tickRep,1)){
        sendTicketEmail($tickJour,8,$_POST['assocArray']);
        sendTicketEmail($tickJour,9,"");
        $salida = "OK||Se ha agregado respuesta con éxito";
      }
      else{
        $salida = "ERROR||No se trasladó el archivo al servidor remoto, no se pudo enviar la notificación por correo";
      }      
    }  
  }
   
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}

//llamada de ajax. Asigna un ticket agregado via email a un sitio previamente insertado
if("assignTicketToSite" == $_POST["action"]){
  $db_modulos   = condb_modulos();
  
  log_write("DEBUG: ASSIGN-TICKET-TO_SITE: Se va a asignar un ticket a un sitio",8);
  
  $siteId   = isset($_POST['siteId']) ? decrypt($_POST['siteId']) : -1;
  $clientId = isset($_POST['cliId']) ? decrypt($_POST['cliId']) : -1;
  $ticketId = isset($_POST['tickId']) ? decrypt($_POST['tickId']) : -1;
  
  $query  = "UPDATE tickets SET cliente_id=".$clientId.", sitio_id=".$siteId.", usuario_alta=".$usuarioId.", ".
                               "ultima_act_usuario=".$usuarioId.", estatus=1 ".
            "WHERE ticket_id=".$ticketId;
  $query2 = "UPDATE ticket_bitacora_posts SET agregado_por='c".$clientId."' WHERE ticket_id=".$ticketId; 
  
  $result  = mysqli_query($db_modulos,$query);
  $result2 = mysqli_query($db_modulos,$query2);
  
  if(!$result||!$result2){
    log_write("ERROR: ASSIGN-TICKET-TO-SITE: Ocurrió un problema al asignar el ticket al sitio seleccionado",8);
    $salida = "ERROR||Ocurrió un problema al asignar el ticket al sitio seleccionado";
  }
  else{
    log_write("OK: ASSIGN-TICKET-TO-SITE: Se ha asignado el ticket al sitio seleccionado",8);
    writeOnJournal($usuarioId,"Ha Asignado el ticket con Id [".$ticketId."] al sitio Id [".$siteId."]");
    $tickJour = insertTicketJournal('Asignacion de ticket a sitio',$ticketId,'Se ha asignado el ticket al sitio','u'.$usuarioId,4);
    
    if(!$tickJour){
      log_write("ERROR: ASSIGN-TICKET-TO-SITE: Ocurrió un problema el mensaje de asignación para este ticket ".$tickJour,8);
      $salida = "ERROR||Ocurrió un problema al guardarse el mensaje de asignación para este ticket";
    }
    else{
      sendTicketEmail($tickJour,1,"");
    }    
    $salida = "OK|| Se ha asignado el ticket al sitio seleccionado";
  }
  
  mysqli_close($db_modulos);
  echo $salida;
}

}
?>
