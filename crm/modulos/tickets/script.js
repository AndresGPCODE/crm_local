$(document).ready(function(){
  $("#ticketDialog").load("../../libs/templates/ticketdialog.php");
  $("#siteDialog").load("../../libs/templates/sitedialog.php");
  
  updateIndexActive("li_navBar_soporte");
  
  if(0<$("#hdn_tickId").val().length){
    $('#ul_ticketDivs a[href="#li_ticketTick"]').tab('show');
    getTicketDetails();
    getTicketAssocs();
    getTicketJournal();
    getTicketNumbers(0);
    getTicketNumbers(1);
    getTicketNumbers(2);
    getTicketNumbers(4);
    getTicketNumbers(5);   
    getTicketNumbers(7); 
  }
  else{
    $('#ul_ticketDivs a[href="#li_ticketDash"]').tab('show');
    $('#ul_ticketStatDivs a[href="#li_ticketStatTopic"]').tab('show');
    $('#ul_ticketTickDivs a[href="#li_ticketTickOpen"]').tab('show');
    
    $("#div_ticketTickDivsTabs").show();   
    $("#div_ticketTickDivsDetails").hide();
  }
  getTicketsListByTopic(); 
  getTicketFilters();   
  
  $('a[href="#li_ticketDash"]').on('show.bs.tab',function(){
    $("#div_ticketDash").empty();
    filterByDate();     
  });  
  $('a[href="#li_ticketStat"]').on('show.bs.tab',function(){
    $('#ul_ticketStatDivs a[href="#li_ticketStatTopic"]').tab('show');
    $('#ul_ticketTickDivs a[href="#li_ticketTickOpen"]').tab('show');  
  });  
  $('a[href="#li_ticketStatTopic"]').on('show.bs.tab',function(){
    getTicketsListByTopic();    
  });  
  $('a[href="#li_ticketStatUser"]').on('show.bs.tab',function(){
    getTicketsListByUser();   
  });  
  $('a[href="#li_ticketTick"]').on('show.bs.tab',function(){
    getTicketsLists(1);
    getTicketNumbers(0); 
    getTicketNumbers(1);
    getTicketNumbers(2);
    getTicketNumbers(4);
    getTicketNumbers(5);  
    getTicketNumbers(7); 
  });
  $('a[href="#li_ticketTickMyti"]').on('show.bs.tab',function(){
    getTicketsLists(0);      
  });  
  $('a[href="#li_ticketTickOpen"]').on('show.bs.tab',function(){
    getTicketsLists(1);      
  });
  $('a[href="#li_ticketTickAnsw"]').on('show.bs.tab',function(){
    getTicketsLists(2);    
  });
  $('a[href="#li_ticketTickOver"]').on('show.bs.tab',function(){
    getTicketsLists(4);    
  });
  $('a[href="#li_ticketTickClos"]').on('show.bs.tab',function(){
    getTicketsLists(5);   
  }); 
  $('a[href="#li_ticketTickRece"]').on('show.bs.tab',function(){
    getTicketsLists(7);   
  });  
 
  window.onresize = function() {
    filterByDate();
  }
  
  $("<div id='tooltip'></div>").css({
    position: "absolute",
    display: "none",
    border: "1px solid #fdd",
    padding: "2px",
      "background-color": "#fee",
      opacity: 0.80
  }).appendTo("body");   
   
  $("#div_ticketDash").bind("plothover",function(event,pos,item){
    if(item) {
      var y=item.datapoint[1];
      $("#tooltip").html(item.series.label+"s: "+y).css({top:item.pageY+5,left:item.pageX+5}).fadeIn(200);
    }
    else {
      $("#tooltip").hide();
    }
  }); 
  
  var hoy = new Date();
  
  var hoyd = ("0" +(hoy.getDate()+1)).substr(-2);
  var hoym = ("0" +(hoy.getMonth()+1)).substr(-2);
  var hoya = hoy.getFullYear();
  
  $("#txt_fecFiltIni").datepicker({dateFormat:'yy-mm-dd'});
  $("#txt_fecFiltFin").datepicker({dateFormat:'yy-mm-dd'});
  
  $("#txt_fecFiltFin").val(hoya+"-"+hoym+"-"+hoyd);  
  $("#txt_fecFiltIni").val(hoya+"-"+hoym+"-01");
  
  filterByDate();
     
});

var data = "";
var plot = "";
  
var options = {
  grid: {
     hoverable: true,
     borderWidth: 0,
     aboveData: true
  },
  legend: {
    show: false
  },
  series: {
    lines: { 
      show: true,
      fill: true 
    },
    points: { 
      show: true, 
      fill: true 
    },          
  },
  xaxis: {
    show: true,
    mode: "time",
    timeformat: "%Y/%m/%d",
    timezone:"browser",
    minTickSize: [1,"day"],
    TickSize: [1,"day"],
    alignTicksWithAxis: 1
  }  
};

//obtiene la lista de filtros para el despliegue u ocultación de series
function getTicketFilters(){  
  $.ajax({
    type: "POST",
    url: "ajax.php",
    async: false,
    data: {"action": "getTicketFilters"},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo filtros de eventos");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      $("#div_ticketDashFilter").html(msg); 
      $(".chk_filtersTick").prop('checked',true);      
    }  
  });
}

//valida el rango de fechas antes de filtar la gráfica
function validateDateFilter(){
  var fecFin = parseDatetime($("#txt_fecFiltFin").val()+" 00:00:00");  
  var fecIni = parseDatetime($("#txt_fecFiltIni").val()+" 00:00:00");

  if(fecFin.getTime() <= fecIni.getTime()){
    return false;     
  }
  else {
    return true;    
  }
}

//aplica el filtro de los datos a ser desplegados en la gráfica de acuerso al rango de fecha seleccionado
function filterByDate(){
  if(false==validateDateFilter()){
    printErrorMsg("div_msgAlert","El rango de fechas no es válido");    
  }
  else{
    getDashData();
    plotFilter();
  }
}

//obtiene los datos para su despliegue en la gráfica  
function getDashData(){
  var fecFin = $("#txt_fecFiltFin").val();  
  var fecIni = $("#txt_fecFiltIni").val();
  $.ajax({
    type: "POST",
    async: false,
    url: "ajax.php",
    data: {"action": "getTicketDash", "fecFin":fecFin, "fecIni":fecIni},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo estadísticas gráficas de tickets");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      //$("#li_ticketDash").html(msg); 
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        data = JSON.parse(resp[1]);
        plot = $("#div_ticketDash").plot(data,options).data("plot");
        //$("#div_ticketDash").html(msg);
      }
      else{ 
        //printErrorMsg("div_msgAlert",resp[1]);
        $("#div_ticketDash").html("<b>Error:</b>"+resp[1]);               
      }  
    }  
  });
}

//agrega y quita series de la gráfica dependiendo de los checkboxes seleccionados necesita que primero se ejecute getDashData() al menos una vez
function plotFilter(){
  var dataFiltered = new Array();
  
  $("#div_ticketDashFilter input[type=checkbox]").each(function(){     
    if($(this).is(":checked")){
      var key = parseInt($(this).attr("value"));
      if(typeof data[key] !== 'undefined'){        
        dataFiltered.push(data[key]);
      }
    }
  });  
  if(dataFiltered.length > 0) {
    plot = $("#div_ticketDash").plot(dataFiltered,options);  
  }
}

//obtiene la lista de tickets ordenada por topics
function getTicketsListByTopic(){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getTicketsListByTopic"},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo lista de tickets");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#li_ticketStatTopic").html("<br>"+resp[1]);
      }
      else{ 
        printErrorMsg("div_msgAlert",resp[1]);                 
      }   
    }  
  });
}

//obtiene la lista de tickets ordenadas por usuario
function getTicketsListByUser(){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getTicketsListByUser"},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo lista de tickets");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#li_ticketStatUser").html("<br>"+resp[1]);  
        $("#tbl_ticketStatUser").DataTable({
          language: datatableespaniol,
        });
      }
      else{ 
        printErrorMsg("div_msgAlert",resp[1]);               
      }   
    }  
  });
}

//obtiene los registros de tickets por estatus, que se despliegan al seleccionar cada pestaña
function getTicketsLists(option){
  $("#div_ticketTickDivsTabs").show();   
  $("#div_ticketTickDivsDetails").hide();
  $("#div_ticketTickDivsAssocs").hide();
  $("#hdn_tickId").val("");
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getTicketsLists", "option":option},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo lista de tickets");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#div_ticketTickDivsDetails").hide();
        $("#div_ticketTickDivsJournal").hide();     
        switch(option){
          case 0:
            $("#li_ticketTickMyti").html("<br>"+resp[1]);  
          break;
          case 1:
            $("#li_ticketTickOpen").html("<br>"+resp[1]);  
          break;
          case 2:
            $("#li_ticketTickAnsw").html("<br>"+resp[1]);          
          break;
          case 4:
            $("#li_ticketTickOver").html("<br>"+resp[1]);  
          break;
          case 5:
            $("#li_ticketTickClos").html("<br>"+resp[1]);  
          break; 
          case 7:
            $("#li_ticketTickRece").html("<br>"+resp[1]);  
          break;    
        } 
        $("#tbl_ticketTick_"+option).DataTable({
          language: datatableespaniol,
        });
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);                  
      }   
    }  
  });
}

//cuenta cuantos tickets existen con cada estatus y los depliega en las pestañas
function getTicketNumbers(option){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getTicketNumbers", "option":option},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo cantidad de tickets");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        switch(option){
          case 0:
            $("#spn_ticketTickMyti").html("("+resp[1]+")");
          break;
          case 1:
            $("#spn_ticketTickOpen").html("("+resp[1]+")");
          break;
          case 2:
            $("#spn_ticketTickAnsw").html("("+resp[1]+")");
          break;
          case 4:
            $("#spn_ticketTickOver").html("("+resp[1]+")");
          break;
          case 5:
            $("#spn_ticketTickClos").html("("+resp[1]+")"); 
          break;
          case 7:
            $("#spn_ticketTickRece").html("("+resp[1]+")"); 
          break;
        }
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);                
      }   
    }  
  });
}

//obtiene los detalles del ticket
function getTicketDetails(){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getTicketDetails", "ticketId": $("#hdn_tickId").val(), "option":0},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo datos del ticket");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){ 
      //$("#div_ticketTickDivsTabs").hide();   
      $("#div_ticketTickDivsDetails").show();     
      $("#div_ticketTickDivsDetails").html(msg);          
    }  
  });
}

//obtiene la bitácora de un ticket
function getTicketJournal(){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getTicketJournal", "ticketId": $("#hdn_tickId").val()},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo bitácora de ticket");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){  
      $("#div_ticketTickDivsJournal").show();     
      $("#div_ticketTickDivsJournal").html(msg);          
    }  
  });
}

function getTicketAssocs(){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getTicketAssoc", "ticketId": $("#hdn_tickId").val(), "option":1},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo Asociados");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){     
      $("#div_ticketTickDivsAssocs").html(msg);
      $("#tbl_ticketAssocList_1").DataTable({
          language: datatableespaniol,
      });          
    }  
  });
}

//abre el cuadro de diálogo para cerrar el ticket
function openTicketClose(){
  $("#div_msgAlertTicketClose").html("");
  $("#hdn_tickCloseAction").val(0);
  $("#div_tickCloseComments").summernote({
    lang: 'es-ES',
    height: 300, 
    minHeight: 100,
    maxHeight: 600,
    onImageUpload: function(files) {
      sendFile(files[0],$(this),'div_msgAlertTicketClose');
    },
    onMediaDelete : function(target) {
      removeFile(target[0],$(this),'div_msgAlertTicketClose');
    }  
  }); 
  $("#h4_ticketCloseTitle").html("Cerrar Ticket");
  $("#div_tickCloseComments").code("");  
  $("#mod_ticketClose").modal("show");
}

//abre el cuadro de diálogo para cerrar el ticket
function openTicketReopen(){
  $("#div_msgAlertTicketClose").html("");
  $("#hdn_tickCloseAction").val(1);
  $("#div_tickCloseComments").summernote({
    lang: 'es-ES',
    height: 300, 
    minHeight: 100,
    maxHeight: 600,
    onImageUpload: function(files) {
      sendFile(files[0],$(this),'div_msgAlertTicketClose');
    },
    onMediaDelete : function(target) {
      removeFile(target[0],$(this),'div_msgAlertTicketClose');
    }  
  }); 
  $("#h4_ticketCloseTitle").html("Reabrir Ticket");
  $("#div_tickCloseComments").code("");  
  $("#mod_ticketClose").modal("show");
}

//asigna un sitio recibido via email a un sitio
function assignTicketToSite(siteId,cliId){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action":"assignTicketToSite", "siteId":siteId, "cliId":cliId, "tickId":$("#hdn_tickId").val()},
    beforeSend: function(){
      setLoadDialog(0,"Asignando ticket a sitio");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = msg.trim().split("||");      
      if("OK" == resp[0]){
        getTicketDetails();
        getTicketJournal();
        getTicketNumbers(1);
        getTicketNumbers(7);
        printSuccessMsg("div_msgAlert",resp[1]);
        $("#mod_sites").modal("hide");
      }
      else{
        printErrorMsg("div_msgAlertsSites",resp[1]);   
      }
    }  
  });
}

//llena los campos del formulario de tickets para su edición y abre el cuadro de diálogo
function ticketEdit(){
  $("#div_msgAlertTicket").html("");
  $("#hdn_ticketInsertAction").val(1);
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getTicketDetails", "ticketId": $("#hdn_tickId").val(), "option":1},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo datos del ticket");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){ 
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#sel_ticketOrg_error").html("");
        $("#sel_ticketTopic_error").html("");
        $("#sel_ticketDepartment_error").html("");
        $("#sel_ticketSla_error").html("");
        $("#sel_tickAssign_error").html("");
        $("#txt_tickTitle_error").html("");
        $("#div_tickDetails_error").html("");
        $("#sel_ticketPrior_error").html("");
        $("#lbl_ticketReassign").show();
        $("#chk_ticketReassign").val(0);
        $("#chk_ticketReassign").attr('checked',false);
        $("#chk_ticketClientNotify").attr('checked',false);
        $("#chk_ticketClientNotify").attr('disabled',true);
        $("#div_assocData").hide();
        
        $("#div_tickDetailsDiv").hide();
        /*$("#div_tickDetails").summernote({
          lang: 'es-ES',
          height: 300, 
          minHeight: 100,
          maxHeight: 600,
          onImageUpload: function(files) {
            sendFile(files[0],$(this),'div_msgAlertTicket');
          },
          onMediaDelete : function(target) {
            removeFile(target[0],$(this),'div_msgAlertTicket');
          }  
        }); */
        
        getTicketPriorityList(resp[8]);
        getTicketDepartmentList(resp[3]);
        getTicketSlaList(resp[4]);
        getTicketTopicList(resp[2]);
        getTicketOriginList(resp[1]);
        getTicketAssignToList(resp[5]); 
        $("#txt_tickTitle").val(resp[6]);
        $("#div_tickDetails").code(resp[7]);
        
        if((5==resp[2])||(6==resp[2])||(14==resp[2])||(17==resp[2])){
          $("#txt_tickTopicOther").val(resp[10]);
          $("#div_tickTopicOther").show();
        }
        else{
          $("#txt_tickTopicOther").val("");
          $("#div_tickTopicOther").hide();
        }

        if(3==resp[1]){
          $("#txt_tickOrgOther").val(resp[9]);
          $("#div_tickOrgOther").show();
        }
        else{
          $("#txt_tickOrgOther").val("");
          $("#div_tickOrgOther").hide();
        }  
  
        $("#mod_ticketInsert").modal("show");
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);
      }                
    }  
  });
}

//cierra o reabre el ticket (si ya está cerrado)
function ticketClose(){
  if(confirm("¿Desea cerrar/reabrir este ticket?")){
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: {"action": "closeTicket", "ticketId": $("#hdn_tickId").val(), "ticketCloseAction":$("#hdn_tickCloseAction").val(),"ticketCloseComment":$("#div_tickCloseComments").code()},
      beforeSend: function(){
        setLoadDialog(0,"Cerrando/reabriendo ticket");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){ 
        var resp = msg.trim().split("||");
        if("OK" == resp[0]){
          $("#mod_ticketClose").modal("hide");
          printSuccessMsg("div_msgAlert",resp[1]);
          getTicketDetails();
          getTicketJournal();
        }
        else{
          printErrorMsg("div_msgAlertTicketClose",resp[1]);          
        }       
      }  
    });
  }
}

//abre el formulario para agregar una nota interna nueva
function ticketAddIntNote(){ 
  $("#div_msgAlertTicketNote").html("");
  $("#hdn_tickNoteAction").val(0);
  $("#div_tickNote").summernote({
    lang: 'es-ES',
    height: 300, 
    minHeight: 100,
    maxHeight: 600,
    onImageUpload: function(files) {
      sendFile(files[0],$(this),'div_msgAlertTicketNote');
    },
    onMediaDelete : function(target) {
      removeFile(target[0],$(this),'div_msgAlertTicketNote');
    }
  });   
  $("#div_tickNote").code("");  
  $("#mod_ticketNote").modal("show");
}

//abre el formulario para agregar una respuesta nueva que se enviará al cliente
function ticketAddReply(){
  $("#div_msgAlertTicketReply").html(""); 
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getTicketAssoc", "ticketId": $("#hdn_tickId").val(), "option":0},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo asociados");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){ 
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#div_tickAssoc").html(resp[1]);
        $("#tbl_ticketAssocList_0").DataTable({
          language: datatableespaniol,
        }); 
        $("#hdn_tickReplyAction").val(0);
        $("#div_tickReply").summernote({
          lang: 'es-ES',
          height: 300, 
          minHeight: 100,
          maxHeight: 600,
          onImageUpload: function(files) {
            sendFile(files[0],$(this),'div_msgAlertTicketReply');
          },
          onMediaDelete : function(target) {
            removeFile(target[0],$(this),'div_msgAlertTicketReply');
          }            
        });   
        $("#div_tickReply").code("");  
        $("#mod_ticketReply").modal("show");
      }
      else{      
        printErrorMsg("div_msgAlertTicketReply",resp[1]);        
      }       
    }  
  });
}

//guarda y envía una respuesta de ticket
function saveTickReplyNew(){
  var assocArr = Array();
  
  //obtiene qué asociados están seleccionados
  $("#div_tickAssoc input[type=checkbox]").each(function(){     
    if($(this).is(":checked")){
      var key = parseInt($(this).attr("value"));      
      assocArr.push(key);
    }
  });
  
  var assocArray = JSON.stringify(assocArr);

  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "saveReply", "ticketId": $("#hdn_tickId").val(), "ticketRepOrg":2, "ticketReply":$("#div_tickReply").code(), "assocArray":assocArray},
    beforeSend: function(){
      setLoadDialog(0,"Agregando Nota Interna");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){ 
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#mod_ticketReply").modal("hide");
        printSuccessMsg("div_msgAlert",resp[1]);
        getTicketJournal();
      }
      else{
        printErrorMsg("div_msgAlertTicketReply",resp[1]);        
      }       
    }  
  });
}

//guarda una nota interna
function saveTickNoteNew(){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "saveInterNote", "ticketId":$("#hdn_tickId").val(), "tickNoteAction":$("#hdn_tickNoteAction").val(), "ticketNote":$("#div_tickNote").code()},
    beforeSend: function(){
      setLoadDialog(0,"Agregando Nota Interna");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){ 
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#mod_ticketNote").modal("hide");
        printSuccessMsg("div_msgAlert",resp[1]);
        getTicketJournal();
      }
      else{
        printErrorMsg("div_msgAlertTicketNote",resp[1]);        
      }       
    }  
  });
}

/*Abre el cuadro de diálogo de insertar un asociado nuevo*/
function insertTicketAssoc(){
  $("#hdn_tickAssocId").val(0);
  $("#div_msgAlertTicketAssoc").html("");
  $("#txt_tickAssocEmail").val("");
  $("#txt_tickAssocEmail_error").html("");
  $("#txt_tickAssocName").val("");
  $("#txt_tickAssocName_error").html("");
  $("#mod_assocInsert").modal("show");
}

/*Obtiene los datos del asociado en cuestión para su edición*/
function ticketAssocEdit(id){
  $("#hdn_tickAssocId").val(id);
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getTicketAssocDetails", "assocId":id},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo Asociado");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){ 
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#txt_tickAssocEmail").val(resp[2]);
        $("#txt_tickAssocName").val(resp[1]);
      }
      else{
        printErrorMsg("div_msgAlertTicketAssoc",resp[1]);        
      } 
      $("#mod_assocInsert").modal("show");      
    }  
  });
}

/*Manda a guardar un asociado nuevo*/
function saveAssocNew(){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "saveAssocNew", "ticketId":$("#hdn_tickId").val(), "tickAssocName":$("#txt_tickAssocName").val(), "tickAssocEmail":$("#txt_tickAssocEmail").val(), "tickAssocId":$("#hdn_tickAssocId").val()},
    beforeSend: function(){
      setLoadDialog(0,"Agregando Asociado");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){ 
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#mod_assocInsert").modal("hide");
        printSuccessMsg("div_msgAlert",resp[1]);
        getTicketAssocs();
      }
      else{
        printErrorMsg("div_msgAlertTicketAssoc",resp[1]);        
      }       
    }  
  });
}

/*Obtiene los datos del asociado en cuestión para su edición*/
function ticketAssocDelete(id){
  if(confirm("¿Está seguro de eliminar este asociado? Esta acción no se podrá deshacer.")){
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: {"action": "deleteTicketAssoc", "assocId":id},
      beforeSend: function(){
        setLoadDialog(0,"Obteniendo Asociado");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){ 
        var resp = msg.trim().split("||");
        if("OK" == resp[0]){
          printSuccessMsg("div_msgAlert",resp[1]);
          getTicketAssocs();
        }
        else{
          printErrorMsg("div_msgAlertTicketAssoc",resp[1]);        
        }   
      }  
    });
  } 
}
  
