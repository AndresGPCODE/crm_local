<?php 
session_start();
set_time_limit(0);
include_once "../../libs/db/common.php";
include_once "../../libs/db/encrypt.php";

if(!verifySession($_SESSION)){
  logoutTimeout();
}else{
  header('Content-Type: text/html; charset=UTF-8');
  date_default_timezone_set('America/Mexico_City');

  $title = "Tickets de Soporte";
  $usuario = $_SESSION['usuario'];
  $gruposArr  = $_SESSION['grupos'];
   
  $tickId = $_GET['id']; 
?>

<!DOCTYPE html>
<html>
  <head>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/AdminLTE2/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
    <link href="../../libs/js/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css"/>  
    <link href="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.css" rel="stylesheet" type="text/css"/>      
    <link href="../../libs/bootstrap/summernote-0.6.6-dist/summernote.css" rel="stylesheet" type="text/css"/> 
    <link href="../../libs/bootstrap/font-awesome-4.3.0/css/font-awesome.css" rel="stylesheet" type="text/css"/>
    
    <script src="../../libs/AdminLTE2/plugins/jQuery/jQuery-2.1.3.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/bootstrap/js/bootstrap.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/dist/js/app.js" type="text/javascript"></script>   
    <script src="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/flot/jquery.flot.js" type="text/javascript"></script>  
    <script src="../../libs/AdminLTE2/plugins/flot/jquery.flot.time.js" type="text/javascript"></script>  
    <script src="../../libs/js/jqueryValidate/dist/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.form.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.uploadPreview.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery-ui-sliderAccess.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
    <script src="../../libs/bootstrap/summernote-0.6.6-dist/summernote.js" type="text/javascript"></script>
    <script src="../../libs/bootstrap/summernote-0.6.6-dist/lang/summernote-es-ES.js" type="text/javascript"></script>
    <script src="../../libs/js/common.js" type="text/javascript"></script>
    <script src="script.js" type="text/javascript"></script>
    
    <link rel="icon" type="image/png" href="../../img/favicon-16x16.png" sizes="16x16">
    
    <title>Coeficiente CRM - Tickets</title>
    <style type="text/css"> 
      .container {
        margin-top: 10px;
      }
      #div_ticketDash{
        width: 100%;
        height:300px;
      }
      .div_tickBgr1{
        background-color: #ddd;
      }
     /* #li_ticketDash{
        width:100%;
      }*/
    </style>
  </head>
  <body>
    <!--Div que contiene el encabezado-->
    <div class="container" id="header"><?php include("../../libs/templates/header.php")?></div>
    
    <div class="container-fluid">
      <h2 class="page-header" id="tituloPag">
        <span class="glyphicon glyphicon-alert"></span> <?php echo $title ?>
      </h2>
    </div><!-- /container -->
    
    <div class="container">
      <div class="row">
        <div id=div_msgAlert></div>
      </div>
    </div> <!-- /container -->

<?php if(hasPermission(6,'r')||hasPermission(6,'l')){?>   
    <div class="container"> 
      <div class="row">
        <ul class="nav nav-tabs" id="ul_ticketDivs">
          <li><a href="#li_ticketDash" data-toggle="tab"><i class="fa fa-area-chart" style="color:black"></i> Dashboard</a></li>
          <li><a href="#li_ticketStat" data-toggle="tab"><i class="fa fa-table" style="color:black"></i> Estadísticas</a></li>
          <li><a href="#li_ticketTick" data-toggle="tab"><i class="fa fa-ticket" style="color:black"></i> Tickets</a></li>
        </ul>
        <br>
        <div class="tab-content" id="div_ticketDivs">
          <div class="tab-pane fade in" role="tabpanel" id="li_ticketDash"><!--dashboard-->
            <div id="div_ticketDashDate">
              <label>Desde:
                <input type="text" id="txt_fecFiltIni" name="txt_fecFiltIni" class="form-control" placeholder="" maxlength="12" size="12" readonly>
              </label>
              <label>Hasta:
                <input type="text" id="txt_fecFiltFin" name="txt_fecFiltFin" class="form-control" placeholder="" maxlength="12" size="12" readonly>
              </label>
              <input type="button" class="btn btn-primary" id="btn_filtraFecha" value="Filtrar" onClick="filterByDate();">
            </div><br>
            <div id="div_ticketDashFilter"></div>
            <div id="div_ticketDash"></div>
          </div><!--/dashboard-->
          <div class="tab-pane fade in" role="tabpanel" id="li_ticketStat"><!--estadisticas-->
            <ul class="nav nav-tabs" id="ul_ticketStatDivs">
              <li><a href="#li_ticketStatTopic" data-toggle="tab"><i class="fa fa-tasks" style="color:black"></i> Topic</a></li>
              <li><a href="#li_ticketStatUser" data-toggle="tab"><i class="fa fa-user" style="color:black"></i> Usuario</a></li>
            </ul> 
            <div class="tab-content" id="div_ticketStatDivs">
              <div class="tab-pane fade in" role="tabpanel" id="li_ticketStatTopic">Stats Topic here</div>
              <div class="tab-pane fade in" role="tabpanel" id="li_ticketStatUser">Stats User here</div> 
            </div>     
          </div><!--/estadisticas-->
          <div class="tab-pane fade in" role="tabpanel" id="li_ticketTick"> <!--tickets-->        
            <div id="div_ticketTickDivsTabs"><!--despliega los tabuladores de las listas de tickets-->
              <ul class="nav nav-tabs" id="ul_ticketTickDivs">
                <li><a href="#li_ticketTickOpen" data-toggle="tab"><i class="fa fa-file" style="color:black"></i> Abiertos <span id="spn_ticketTickOpen">()</span></a></li>
                <li><a href="#li_ticketTickAnsw" data-toggle="tab"><i class="fa fa-file-o" style="color:blue"></i> Respondidos <span id="spn_ticketTickAnsw">()</span></a></li>
                <li><a href="#li_ticketTickOver" data-toggle="tab"><i class="fa fa-exclamation-triangle" style="color:red"></i> Atrasados <span id="spn_ticketTickOver">()</span></a></li>
                <li><a href="#li_ticketTickClos" data-toggle="tab"><i class="fa fa-check" style="color:green"></i> Cerrados <span id="spn_ticketTickClos">()</span></a></li>
                <li><a href="#li_ticketTickMyti" data-toggle="tab"><i class="fa fa-user" style="color:orange"></i> Mis Tickets <span id="spn_ticketTickMyti">()</span></a></li>
<?php if(hasPermission(6,'a')){  ?>                 
                <li><a href="#li_ticketTickRece" data-toggle="tab"><i class="fa fa-user" style="color:blue"></i> Recibidos <span id="spn_ticketTickRece">()</span></a></li>
<?php } ?>                
              </ul> 
              <div class="tab-content" id="div_ticketTickDivs"><!--aquí aparecen las listas-->
                <div class="tab-pane fade in" role="tabpanel" id="li_ticketTickOpen"></div>
                <div class="tab-pane fade in" role="tabpanel" id="li_ticketTickAnsw"></div>
                <div class="tab-pane fade in" role="tabpanel" id="li_ticketTickOver"></div>
                <div class="tab-pane fade in" role="tabpanel" id="li_ticketTickClos"></div>
                <div class="tab-pane fade in" role="tabpanel" id="li_ticketTickMyti"></div>
<?php if(hasPermission(6,'a')){ ?>                
                <div class="tab-pane fade in" role="tabpanel" id="li_ticketTickRece"></div>
<?php } ?>                 
              </div> 
            </div><!--/div_ticketTickDivsTabs-->
            <br>
            <div id="div_ticketTickDivsDetails" class="col col-xs-12 col-lg-12"></div><br><br><!--despliega los detalles de ticket cuando se selecciona uno-->
            <div id="div_ticketTickDivsAssocs" class="col col-xs-12 col-lg-12"></div><!--despliega los asociados de ticket cuando se selecciona uno-->
            <div id="div_ticketTickDivsJournal" class="col col-xs-12 col-lg-12"></div><!--despliega la bitácora de ticket cuando se selecciona uno-->
          </div><!-- /tickets -->
        </div>
      </div>    
      <input type="hidden" id="hdn_tickId" value="<?php echo $tickId;?>"></input>
    </div> <!-- /container -->     
<?php
  }
  else{
    $salida = "<b>ERROR:</b> No cuenta con permisos para acceder a estos datos";
  } 
?>    
    <div class="container" id="siteDialog"></div><!--cuadro de dialogo para gestión de sitios--> 
    
    <div class="container-fluid" id="footer"><?php include("../../libs/templates/footer.php")?></div> 
    
    <div class="container-fluid" id="ticketDialog"></div> 
    
  </body>
</html>  

<?php } //fin de else?>
