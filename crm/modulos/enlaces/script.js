$(document).ready(function(){ 
  $("#siteDialog").load("../../libs/templates/sitedialog.php");
  $("#eventDialog").load("../../libs/templates/eventdialog.php");
  $("#siteConnDialog").load("../../libs/templates/siteconndialog.php");
  $("#serviceDialog").load("../../libs/templates/servicedialog.php");
  getSiteConnectionDetails();
  getServiceList();
  
});

//obtiene la lista de todos los servicios asociados a un enlace
function getServiceList(){
  var siteConId = $("#hdn_siteConId").val();
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getServices", "siteConId":siteConId},
    success: function(msg){
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#div_services").html(resp[1]);
        $("#tbl_services").DataTable({
          language: datatableespaniol,
        });
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);         
      }
    }  
  });
}

//obtiene los datos detallados de un enlace de sitio
function getSiteConnectionDetails(){  
  var siteConId = $("#hdn_siteConId").val(); 
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getSiteConnectionDetails", "siteConId":siteConId, "option":0},
    success: function(msg){
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#div_siteConnectionDetails").html(resp[1]);
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);         
      }
    }  
  });
}

function openNewService(){
  $("#hdn_serviceAction").val(0);
  
  getService(1);
  serFieldSet("serDownload",1);
  serFieldSet("serUpload",1);
  serFieldSet("serCantIp",0);
  serFieldSet("serLinAna",0);
  serFieldSet("serCantDid",0);
  serFieldSet("serCantCan",0);
  serFieldSet("serCantExt",0);
  serFieldSet("serDomVpbx",0);
  serFieldSet("serIpVpbx",0);
  serFieldSet("serCosto",1);
  
  $("#div_msgAlertService").html("");
  $("#mod_newService").modal("show"); 
}


