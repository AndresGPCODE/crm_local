<?php
session_start();

set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

//include_once "../../libs/db/dbcommon.php";
include_once "../../libs/db/common.php";

$gruposArr  = $_SESSION['grupos'];
$usuarioId  = $_SESSION['usrId'];
$usuario    = $_SESSION['usuario'];
$nombre     = $_SESSION['usrNombre'];

if(!verifySession($_SESSION)){
  logoutTimeout();
}else{

$db_catalogos = condb_catalogos();
$db_modulos   = condb_modulos();
$db_usuarios  = condb_usuarios();

$salida = "";

if("getServices" == $_POST['action']){
  
  $siteConId = isset($_POST['siteConId']) ? decrypt($_POST['siteConId']) : -1;
  
  //$query = "SELECT ser.* FROM servicios AS ser, rel_enlace_servicio AS rel WHERE ser.servicio_id=rel.servicio_id AND rel.enlace_id=".$siteConId;
  
  $query = "SELECT * FROM servicios WHERE enlace_id=".$siteConId;
  
  $query2 = "SELECT usuario_alta FROM enlaces WHERE enlace_id=".$siteConId;
  
  $result = mysqli_query($db_modulos,$query);
  $result2 = mysqli_query($db_modulos,$query2);
  
  if(!$result||!$result2){
    $salida = "ERROR||No se pudo obtener los datos de los servicios para este enlace";
  }
  else{
    $salida = "OK||";
    $row2 = mysqli_fetch_assoc($result2);
    if(hasPermission(5,'w')||(hasPermission(5,'m')&&$row2['usuario_alta']==$usuarioId)){  
      $salida .= "<div class=\"col container-fluid\">".
                 "  <button type=\"button\" id=\"btn_servicesNew\" title=\"Nuevo Servicio\" class=\"btn btn-default\" onClick=\"openNewService();\"><span class=\"glyphicon glyphicon-asterisk\"></span> Nuevo Servicio</button><br>".
                 "</div><br>";
    }
    if(0==mysqli_num_rows($result)){
      $salida .= "No existen registros disponibles haga click en <strong>Nuevo Servicio</strong> para agregar uno nuevo";
    }
    else{
      $salida .= "<div class=\"table-responsive\"><table id=\"tbl_services\" class=\"table table-striped table-condensed\">\n".
                 "  <thead>\n".
                 "    <tr>\n".
                 "      <th>Id</th>\n".
                 "      <th>Tipo de Servicio</th>\n".
                /*"      <th>BW download</th>\n".
                 "      <th>Cantidad IP públicas</th>\n".
                 "      <th>Línea analógica</th>\n".
                 "      <th>Cantidad Dids</th>\n".*/
                 /*"      <th>BW upload</th>\n".
                 "      <th>Cantidad canales</th>\n".
                 "      <th>Cantidad extensiones</th>\n".
                 "      <th>Dominio VPBX</th>\n".
                 "      <th>IP VPBX</th>\n".*/
                 "      <th>Costo</th>\n".
                 "      <th>Acciones</th>\n".
                 "    </tr>\n".
                 "  </thead>\n".
                 "  <tbody>\n"; 
                 
       while($row = mysqli_fetch_assoc($result)){
         $tipoServicio = get_serviceType($row['tipo_servicio']);
         $salida .= "    <tr>".
                    "      <td>".$row['servicio_id']."</td>\n".
                    "      <td>".$tipoServicio."</td>\n".
                    /*"      <td>".$row['bw_bajada']." Mbps</td>\n".
                    "      <td>".$row['cant_ip_publica']."</td>\n".
                    "      <td>".$row['linea_analogica']."</td>\n".
                    "      <td>".$row['cant_did']."</td>\n".*/
                    /*"      <td>".$row['bw_subida']." Mbps</td>\n".
                    "      <td>".$row['cant_canales']."</td>\n".
                    "      <td>".$row['cant_extensiones']."</td>\n".
                    "      <td>".$row['dom_vpbx']."</td>\n".
                    "      <td>".$row['ip_vpbx']."</td>\n".*/
                    "      <td>$".number_format($row['costo'], 2, '.', ',')."</td>".
                    "      <td>";
         if(hasPermission(5,'r')||(hasPermission(5,'l')&&$row['usuario_alta']==$usuarioId)){ 
           $salida .= "        <button type=\"button\" id=\"btn_serviceDetails_".$row['servicio_id']."\" title=\"Ver Detalles\" class=\"btn btn-xs btn-primary btn-responsive\" onClick=\"getServiceDetails(".$row['servicio_id'].");\"><span class=\"glyphicon glyphicon-list\"></span></button>".    
                      "        <button type=\"button\" id=\"btn_serviceEquipment_".$row['servicio_id']."\" title=\"Ver Equipos\" class=\"btn btn-xs btn-default btn-responsive\" onClick=\"getServiceEquipmentList(".$row['servicio_id'].");\"><span class=\"glyphicon glyphicon-phone\"></span></button>";    
         }         
         if(hasPermission(5,'e')||(hasPermission(5,'n')&&$row['usuario_alta']==$usuarioId)){  
           $salida .= "        <button type=\"button\" id=\"btn_serviceEdit_".$row['servicio_id']."\" title=\"Editar\" class=\"btn btn-xs btn-success btn-responsive\" onClick=\"serviceEdit(".$row['servicio_id'].");\"><span class=\"glyphicon glyphicon-edit\"></span></button>";
         }           
         if(hasPermission(5,'d')){      
           $salida .= "        <button type=\"button\" id=\"btn_serviceDelete_".$row['servicio_id']."\" title=\"Eliminar Servicio\" class=\"btn btn-xs btn-danger btn-responsive\" onClick=\"deleteService(".$row['servicio_id'].");\"><span class=\"glyphicon glyphicon-remove\"></span></button>";
         }                               
         $salida .= "      </td>\n".
                    "    </tr>\n"; 
       } 
        $salida .= "  </tbody>\n".
                 "</table>\n</div>"; 
    }
  }
  
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}



}
?>
