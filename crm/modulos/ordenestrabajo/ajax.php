<?php

session_start();
set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

include_once "../../libs/db/common.php";

if(!verifySession($_SESSION)){
  logoutTimeout();
}
	else{
	$gruposArr  = $_SESSION['grupos'];
	$usuarioId  = $_SESSION['usrId'];
	$usuario    = $_SESSION['usuario'];
	$nombre     = $_SESSION['usrNombre'];

	$db_modulos   = condb_modulos();
	$db_usuarios  = condb_usuarios();
	$db_catalogos = condb_catalogos();

	$modId = 9;

	if("getWorkOrdersListQuery" == $_POST['action']){				 
						 
	  $query = "SELECT ordTra.*, ordTraTyp.*, ordTraSta.estatus, cot.tipo_origen, cot.origen_id, ordTraTip.tipo_orden, relCotPro.sitio_asignado, relCotPro.orden_trabajo_id ".
						 "FROM ".$cfgTableNameMod.".ordenes_trabajo AS ordTra, ".
                     $cfgTableNameMod.".cotizaciones AS cot, ".
                     $cfgTableNameMod.".rel_cotizacion_producto AS relCotPro, ".
										 $cfgTableNameCat.".cat_orden_trabajo_tipo AS ordTraTyp, ".
										 $cfgTableNameCat.".cat_orden_trabajo_estatus AS ordTraSta, ".
                     $cfgTableNameCat.".cat_orden_trabajo_tipo AS ordTraTip ".
						 "WHERE ordTraSta.estatus_id = ordTra.estatus ".
						 "AND ordTra.tipo_orden_id=ordTraTyp.tipo_orden_id ".
             "AND relCotPro.relacion_id = ordTra.rel_cotizacion_producto ".
             "AND relCotPro.cotizacion_id = cot.cotizacion_id ".
             "AND ordTraTip.tipo_orden_id = ordTra.tipo_orden_id ";
             $txt_resp = "";
    if(isset($_POST['condicion']) && $_POST['condicion'] == 0){
      $query .= "AND ordTra.estatus != 3 AND ordTra.estatus != 5 ";
    }elseif(isset($_POST['condicion']) && $_POST['condicion'] == 1){
      $query .= "AND ordTra.estatus = 3 ";
    }elseif(isset($_POST['condicion']) && $_POST['condicion'] == 2){
      $query .= "AND ordTra.estatus = 5 ";
      $txt_resp = " pendientes de autorización";
    }
    $query .= "ORDER BY ordTra.fecha_alta DESC";
    $result = mysqli_query($db_modulos,$query);
    // var_dump($query);
		if(!$result){
			$salida = "ERROR: Ocurrió un problema al consultar las órdenes de trabajo ";	
		}
		else{
			if(0>=mysqli_num_rows($result)){
				$salida = "No existen órdenes de trabajo". $txt_resp .".";	
			}
			else{
				
				$fechaNow = date("Y-m-d H:i:s");  

				$salida .= " <div class=\"table-responsive\"><table id=\"tbl_workOrders\" class=\"table table-striped table-condensed\">\n".
									 "  <thead>\n".
									 "    <tr>\n".
									 "      <th>Origen</th>\n".
									 "      <th>Nombre del Cliente</th>\n".
									 "      <th>Por:</th>\n".
									 "      <th>Fecha de Alta:</th>\n".
									 "      <th>Estatus</th>\n".
									 "      <th>Tipo de Orden</th>\n".
									 "      <th>Acciones</th>\n".
									 "    </tr>\n".
									 "  </thead>\n".
									 "  <tbody>\n";
										 
				while($row = mysqli_fetch_assoc($result)){

					$fechaLim = date("Y-m-d H:i:s", strtotime(date($row['fecha_alta'].' +3 hours')));
					
					$fechaAlta = strtotime(date($row['fecha_alta']));
					
					$tiempoRes = round((strtotime($fechaLim) - strtotime($fechaNow))/3600);
					
					$tiempoDeAlta = round((strtotime($fechaNow) - $fechaAlta)/3600);
					
					$nombreAlta = get_userRealName($row['usuario_alta']);
					$salida .=  "    <tr>\n";
					if(0==$row['tipo_origen'])
						$salida .=  "      <td>Prospecto</td>\n";
					else if(1==$row['tipo_origen'])
						$salida .=  "      <td>Cliente</td>\n";
					else if(2==$row['tipo_origen'])
						$salida .=  "      <td>Sitio</td>\n";
					else
						$salida .=  "      <td><i>Sin origen específico</i></td>\n";
            
          switch($row['tipo_origen']){
            case 0:
              $tableName = "prospectos";
              $fieldId = "prospecto_id";
            break;
            case 1:
              $tableName = "clientes";
              $fieldId = "cliente_id";
            break;
            default:
              $tableName = "";
            break; 
          }
            
          $queryCli = "SELECT * FROM ".$tableName." WHERE ".$fieldId." = ".$row['origen_id'];
          
          $resultCli = mysqli_query($db_modulos,$queryCli);

          if(!$resultCli){
            $salida = "Ocurrió un error al obtener los datos de los ".$tableName;
          }
          else{
            $rowCli = mysqli_fetch_assoc($resultCli);
            if(""!=$rowCli['razon_social'])
              $nomCom = $rowCli['razon_social'];
            else
              $nomCom = $rowCli['nombre_comercial'];
              
            $salida .= "      <td>".$nomCom."</td>\n".
											 "      <td><a href=/crm/modulos/perfil/index.php?id='".encrypt($row['usuario_alta'])."'>".$nombreAlta."</a></td>\n";
					
            $salida .=  "      <td>".$row['fecha_alta']."</td>\n";
            
            $indicator = "";
                        
            if(24<=$tiempoDeAlta){
              $indicator =  "      <span class=\"glyphicon glyphicon-exclamation-sign\" style=\"color:red\" title=\"Tiene más de un día sin agendarse\"></span>\n";
            }
            elseif(12<=$tiempoDeAlta){
              $indicator .=  "      <span class=\"glyphicon glyphicon-exclamation-sign\" style=\"color:orange\" title=\"Tiene más de 12 horas sin agendarse\"></span>\n";
            }
            elseif(6<=$tiempoDeAlta){
              $indicator .=  "      <span class=\"glyphicon glyphicon-exclamation-sign\" style=\"color:yellow\" title=\"Tiene más de seis horas sin agendarse\"></span>\n";
            }          
            else{
              $indicator .=  "      <i>Sin agendar</i>\n";
            }
            /*else{
              if(0>=$tiempoRes&&3>$row['estatus']){
                $indicator .=        $row["fecha_inicio"]."<span class=\"glyphicon glyphicon-alert\" style=\"color:red\" title=\"Transcurrió la fecha límite y no se ha actualizado como terminado\"></span>\n";
              }
              else{
                $indicator .=       $row["fecha_inicio"]."\n";
              }          
            }*/
            $salida .= "      <td>".$row['estatus']."</td>\n".
                       "      <td>".$row['tipo_orden']."</td>\n";
            //         "      <td title=\"".$row["descripcion"]."\" onClick=\"alert('".$row["descripcion"]."')\">".
            //         "      <a href=\"#\">".substr($row["descripcion"],0,30)."...</a></td>\n";
            $salida .= "      <td>";          
                                     
            if(hasPermission($modId,'e')){  
              if(0!=$row['sitio_asignado'])
                $sitAsig = $row['sitio_asignado'];
              else{
                $queryAdecSit = "SELECT relCotPro.sitio_asignado ".
                                "FROM ".$cfgTableNameMod.".rel_cotizacion_producto AS relCotPro, ".
                                        $cfgTableNameMod.".ordenes_trabajo AS ordTra ".
                                "WHERE ordTra.orden_id = ".$row['orden_trabajo_id']." ".
                                "AND relCotPro.relacion_id = ordTra.rel_cotizacion_producto";
                $resultAdecSit = mysqli_query($db_modulos,$queryAdecSit);
                if(!$resultAdecSit){
                  $salida = "Ocurrió un problema al obtenerse los datos de sitio de la orden de trabajo";
                }
                else{
                  $rowAdecSit = mysqli_fetch_assoc($resultAdecSit);
                  $sitAsig = $rowAdecSit['sitio_asignado'];
                }
              }  
              $salida .= "      <button type=\"button\" id=\"btn_workOrdTask_".$row['orden_id']."\" title=\"Detalles de Órden\" class=\"btn btn-xs btn-default\" onClick=\"getWorkOrderDetails('".encrypt($row['orden_id'])."',0);\"><span class=\"glyphicon glyphicon-th-list\"></span></button>";
              $salida .= "        <button type=\"button\" id=\"btn_siteDetails_".$sitAsig."\" title=\"Ver Sitio\" class=\"btn btn-xs btn-primary btn-responsive\" onClick=\"goToSiteDetails('".encrypt($sitAsig)."','".encrypt($row['tipo_origen'])."');\"><span class=\"glyphicon glyphicon-list\"></span></button>";
              
            }          				  
            /*if(hasPermission($modId,'d')){ 
              $salida .= "  <button type=\"button\" id=\"btn_eventDelete_".$row['id']."\" title=\"Borrar Evento\" ".
                                    "class=\"btn btn-xs btn-danger btn-responsive\" onClick=\"deleteEvent(".$row['id'].");\">".
                                    "<span class=\"glyphicon glyphicon-remove\"></span></button>";
            }*/
            $salida .= "      </td>\n".
                       "    </tr>\n"; 
          }
				}
				 $salida .= "  </tbody>\n".
                    "  <tfoot>".
                    "    <tr>".
                    "      <td></td>".
                    "      <td></td>".
                    "      <td></td>".
                    "      <td></td>".
                    "      <td></td>".
                    "      <td></td>".
                    "      <td></td>".
                    "    </tr>".
                    "  </tfoot>\n".
										"</table>\n</div>";
			}	
		}
		mysqli_close($db_modulos);
		mysqli_close($db_usuarios);
		mysqli_close($db_catalogos);
		echo $salida;
	}

}
?>
