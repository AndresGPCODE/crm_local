//document.ready ejecuta continuamente lo que contenga dentro de la funcion
$(document).ready(function(){ 
  $("#workOrderDialog").load("../../libs/templates/workorderdialog.php"); 
  updateIndexActive("li_navBar_soporte");
  getWorkOrdersMod(0);
});

//funcion que consigue las listas de eventos de ctipo soporte/instalación para su despliegue
function getWorkOrdersMod(terminado){
  if(terminado == 0){
    terminado = {
      "action": "getWorkOrdersListQuery",
      "condicion": 0
    }
  } else if(terminado == 1){
    terminado = {
      "action": "getWorkOrdersListQuery",
      "condicion": 1
    }
  }else{
    terminado = {
      "action": "getWorkOrdersListQuery",
      "condicion": 2
    }
  }
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: terminado,
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo órdenes de trabajo");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = msg.trim().split("||");
			$("#div_workOrders").html(resp);      
			$("#tbl_workOrders").DataTable({
				language: datatableespaniol,
        initComplete: function () {
          var cont = 0;
          this.api().columns().every( function () {
            cont++;
            var column = this;
            if (cont != 7){
              var select = $('<select><option value=""></option></select>').appendTo($(column.footer()).empty()).on('change', function () {
                var val = $.fn.dataTable.util.escapeRegex($(this).val());
                column.search(val ? '^' + val + '$' : '', true, false).draw();
              });
            }
            // console.log(cont);
            column.data().unique().sort().each(function(d,j){
              if (cont == 3) {
                // si es la columna de los vendedores, se separa el string en partes para solo colocar el nombre para el select y no toda la etiqueta
                d = d.split('>');
                d = d[1].split('<');
                select.append('<option value="' + d[0] + '">' + d[0] + '</option>');
                // console.log(d[0]);
              }else if(cont != 7) {
                select.append('<option value="' + d + '">' + d + '</option>');
              }
            });
          });
        }
			});
    }  
  });
}