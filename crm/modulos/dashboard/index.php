<?php 
session_start();
set_time_limit(0);
include_once "../../libs/db/common.php";

if(!verifySession($_SESSION)){
  logoutTimeout();
}else{

header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

$title = "Dashboard";

?>

<!DOCTYPE html>
<html>
  <head>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/AdminLTE2/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
    <link href="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <link href="../../libs/bootstrap/font-awesome-4.3.0/css/font-awesome.css" rel="stylesheet" type="text/css"/> 
    <link href="../../libs/AdminLTE2/plugins/fullcalendar-2.3.0/fullcalendar.css" rel="stylesheet" type="text/css"/> 
    
    <script src="../../libs/AdminLTE2/plugins/jQuery/jQuery-2.1.3.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../libs/js/jqueryValidate/dist/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/fullcalendar-2.3.0/lib/moment.min.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/fullcalendar-2.3.0/fullcalendar.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/fullcalendar-2.3.0/lang/es.js" type="text/javascript"></script> 
    <script src="../../libs/AdminLTE2/dist/js/app.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/chartjs/Chart.min.js" type="text/javascript"></script>
    <script src="../../libs/js/common.js" type="text/javascript"></script>
    <script src="script.js" type="text/javascript"></script>
    
    <link rel="icon" type="image/png" href="../../img/favicon-16x16.png" sizes="16x16">
    <style type="text/css">

      #li_calendar_div {
          width: 400px;
          margin: 0 auto;
          font-size: 10px;
          overflow:hidden;
      }
      .fc-header-title h2 {
          font-size: .9em;
          white-space: normal !important;
      }
      .fc-view-month .fc-event, .fc-view-agendaWeek .fc-event {
          font-size: 0;
          overflow: hidden;
          height: 2px;
      }
      .fc-view-agendaWeek .fc-event-vert {
          font-size: 0;
          overflow: hidden;
          width: 2px !important;
      }
      .fc-agenda-axis {
          width: 20px !important;
          font-size: .7em;
      }

      .fc-button-content {
          padding: 0;
      }
      
      .hiddenEvent{display: none;}
      .fc-other-month .fc-day-number { display:none;}

      td.fc-other-month .fc-day-number {
          visibility: hidden;
      }
    </style>

    <title>Coeficiente CRM</title>
  </head>  
  
  <body>
    <div class="container" id="header"><?php include("../../libs/templates/header.php")?></div>
    
    <div class="container-fluid">
      <h2 class="page-header" id="tituloPag">
        <span class="glyphicon glyphicon-dashboard"></span> <?php echo $title ?>
      </h2>
    </div>
    <div class="container">
      <div class="row">
        <div id=div_msgAlert></div>
      </div>
    </div> <!-- /container -->
    
    <!--<div class="jumbotron">
      <div class="container">
        <h2>Bienvenido:</h2>
        <p>COECRM</p>
      </div>
    </div> -->
    <div class="row row-eq-height">
      <div class="col-lg-8 col-md-8 col-xs-12">
        <div>
          <label for="weekProsChart">Estatus actuales de los prospectos.<br>
          <div class="col-md-8 col-xs-8" id="div_weekProsChart"><canvas id="weekProsChart" width="150" height="150"></canvas></div>
          <div class="col-md-4 col-xs-4 small" id="div_weekProsChart_acot"></div></label>  
          <label for="weekCliChart">Origen de los clientes.<br>
          <div class="col-md-8 col-xs-8" id="div_weekCliChart"><canvas id="weekCliChart" width="150" height="150"></canvas></div>
          <div class="col-md-4 col-xs-4 small" id="div_weekCliChart_acot"></div></label>
          <label for="monthSellChart">Venta meta del mes.<br>
          <div class="col-md-8 col-xs-8" id="div_monthSellChart"><canvas id="monthSellChart" width="150" height="150"></canvas></div>
          <div class="col-md-4 col-xs-4 small" id="div_monthSellChart_acot"></div></label>
        </div>
        <div id="div_tableDataSell"></div>
      </div> 
        
      <div class="col-lg-4 col-md-4 col-xs-12 form-signing-round">
        <div class="tab-pane fade in" role="tabpanel" id="li_mainCalendarTabs">
          <div id="div_calTabs" align="center">
            <ul class="nav nav-tabs" id="ul_mainCalendarTabs">
              <li class="nav-item active" id="li_calendar">
                <a href="#li_calendar_div" data-toggle="tab" class="nav-link" role="tab">Calendario</a>
              </li>
              <li class="nav-item" id="li_promoGdl">
                <a href="#li_promoGdl_div" data-toggle="tab" class="nav-link" role="tab">Promo Gdl</a>
              </li>
              <li class="nav-item" id="li_promoMzn">
                <a href="#li_promoMzn_div" data-toggle="tab" class="nav-link" role="tab">Promo Mzn</a>
              </li>            
            </ul>
          </div>            
          <br>
          <div class="tab-content" id="div_calDivs">
            <div class="tab-pane active" role="tabpanel" id="li_calendar_div"></div>
            <div class="tab-pane" role="tabpanel" id="li_promoGdl_div">test</div>
            <div class="tab-pane" role="tabpanel" id="li_promoMzn_div">test</div>
          </div>
        </div>
      </div>
    </div>
    <hr>
    <!--_Cuadro de dialogo de eventos de prospecto -->
    <div id="mod_eventDetails" class="modal fade" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Detalles de Evento</h4>
          </div>
          <div class="container-fluid">
            <div id=div_msgAlertEventDetails></div>
          </div>
          <div class="modal-body" id=div_eventDetails></div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->  
    
    <div class="container" id="commentEventDialog"></div><!--cuadro de dialogo para gestión de comentarios--> 
    <div class="container-fluid" id="footer"><?php include("../../libs/templates/footer.php")?></div>
  </body>
</html>

<?php
}//fin else sesión
?>
