$(document).ready(function(){ 
  
  getPieData("weekProsChart","weekProspects");
  getPieData("weekCliChart","weekClients");
  //getPieData("monthSellChart","getPieChartData"); 
  
  updateIndexActive("li_navBar_dashboard");
  
  regPromo("promoGdl",1);
  regPromo("promoMzn",2);
  getDashSellData();
  //getEventJson();
  
  //inicializa el calendario para su despliegue inicial y su gestión en todas las funciones
  calendar = $('#li_calendar_div').fullCalendar({
    editable: false,
    aspectRatio: 1.9,
    contentHeight: "auto",
    header: {
      right: '',
      left: 'prev,next',
      center: 'title'
    },   
    events: {
      type: 'POST',
      url: 'ajax.php',
      data: {"action": "getCalendar"}  
    }, 
    eventRender: function(event, element, view) {
      eventRender(event,element);
    },
    eventMouseover: function(event, jsEvent, view) {
        if (view.name !== 'agendaDay') {
            $(jsEvent.target).attr('title', event.title);
        }
    } 
  }); 
});

//solo para debuggeo. despliega en vez del calendario el json de todos los eventos
function getEventJson(){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getCalendar"},
    success: function(msg){
      var arr = JSON.parse(msg.trim());
      //$("#li_calendar_div'").html(arr); 
    }  
  });
}

var pieCharOptions = {
  //Boolean - Show a backdrop to the scale label
  scaleShowLabelBackdrop: true,
  //String - The colour of the label backdrop
  scaleBackdropColor: "rgba(0,0,0,0.75)",
  // Boolean - Whether the scale should begin at zero
  scaleBeginAtZero: true,
  //Number - The percentage of the chart that we cut out of the middle
  percentageInnerCutout : 30, // This is 0 for Pie charts
  //Number - The backdrop padding above & below the label in pixels
  scaleBackdropPaddingY: 2,
  //Number - The backdrop padding to the side of the label in pixels
  scaleBackdropPaddingX: 2,
  //Boolean - Show line for each value in the scale
  scaleShowLine: true,
  //Boolean - Stroke a line around each segment in the chart
  segmentShowStroke: true,
  //String - The colour of the stroke on each segement.
  segmentStrokeColor: "#fff",
  //Number - The width of the stroke value in pixels
  segmentStrokeWidth: 2,
  //Number - Amount of animation steps
  animationSteps: 100,
  //String - Animation easing effect.
  animationEasing: "easeOutBounce",
  //Boolean - Whether to animate the rotation of the chart
  animateRotate: true,
  //Boolean - Whether to animate scaling the chart from the centre
  animateScale: false,
  //String - A legend template
  legendTemplate: "  <% for (var i=0; i<segments.length; i++){%>"+
                  "    <span style=\"background-color:<%=segments[i].fillColor%>\">&nbsp;&nbsp;&nbsp;</span>"+
                  "    <%if(segments[i].label){%><%=segments[i].label%><%}%>"+
                  "    <br>"+
                  "  <%}%>"
}

function getPieData(divId,source){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getPieChartData", "source":source},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo gráfica");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        var data = JSON.parse(resp[1]);
        var ctx = $("#"+divId).get(0).getContext("2d");
        chart = new Chart(ctx).Pie(data,pieCharOptions);
        var legend = chart.generateLegend();
        $("#div_"+divId+"_acot").html(legend);          
      }
      else{
        $("#div_"+divId).html(resp[1]);  
      }      
    }
  });
}

function getDashSellData(){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getTableDataSell"},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo datos a graficar");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      $("#div_tableDataSell").append(msg);
      /*var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#div_dashData").append(resp[1]);
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);       
      }*/   
    }  
  });
}

function regPromo(div,regId){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getPromo", "regId":regId},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo promoción");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = msg.trim();
      $("#li_"+div+"_div").html(resp);
    }
  });
}
