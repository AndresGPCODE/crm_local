<?php
session_start();
set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

include_once "../../libs/db/common.php";

$gruposArr  = $_SESSION['grupos'];
$usuarioId  = $_SESSION['usrId'];
$usuario    = $_SESSION['usuario'];
$nombre     = $_SESSION['usrNombre'];
$usrUbica   = $_SESSION['usrUbica'];

if(!verifySession($_SESSION)){
  logoutTimeout();
}else{

$db_catalogos = condb_catalogos();
$db_modulos   = condb_modulos();
$db_usuarios  = condb_usuarios();

$salida = "";

function getPieData($query){
  global $db_modulos;
           
  $result = mysqli_query($db_modulos,$query); 
  
  if(!$result){
    $salida = "ERROR|| No se pudo consultar de los datos de la gŕafica en la DB ";
  }
  else{
    if(0==mysqli_num_rows($result)){
      $salida = "ERROR||No hay datos disponibles."; 
      goto weekdata;
    }
    else{
      $dataArr = array();
      while($row = mysqli_fetch_assoc($result)){
        $r = dechex(rand(0x11, 0x77));
        $g = dechex(rand(0x11, 0x11));
        $b = dechex(rand(0x11, 0x33));
      
        $label = $row['label'];
        
        $dataArr[] = array("value" => $row['cnt']*1,
                           "label" => $label,
                           "color" => "#".$r.$g.$b,
                           "highlight" => "#".$b.$g.$r);    
      }    
    }
    $salida = "OK||".json_encode($dataArr);
  }
  weekdata: 
  return $salida;       
}

function getWeekClients(){
  global $db_modulos;
  global $cfgTableNameMod;
  global $cfgTableNameUsr;
  global $usrUbica;
  
  $monday = date('Y-m-d', strtotime('monday this week'));
  $friday = date('Y-m-d', strtotime('friday this week'));
  
  if((hasPermission(1,'r'))&&(hasPermission(1,'f'))){
    $query = "SELECT cli.nombre_comercial, cli.origen, cli.estatus, cli.usuario_alta, cli.razon_social ".
             "FROM ".$cfgTableNameMod.".clientes as cli WHERE cli.fecha_alta BETWEEN '$monday 00:00:00' AND '$friday 23:59:59' ";
  }
  if((hasPermission(1,'r'))&&(hasPermission(1,'c'))){
    $query = "SELECT cli.nombre_comercial, cli.origen, cli.razon_social, cli.usuario_alta ".
             "FROM ".$cfgTableNameMod.".clientes AS cli, ".$cfgTableNameUsr.".usuarios AS usr ". 
             "WHERE cli.usuario_alta=usr.usuario_id AND usr.ubicacion_id=".$usrUbica.
             " AND cli.fecha_alta BETWEEN '$monday 00:00:00' AND '$friday 23:59:59' ";
  }
  if(hasPermission(1,'l')){
     $query = "SELECT cli.nombre_comercial, cli.origen, cli.estatus, cli.usuario_alta ".
              "FROM ".$cfgTableNameMod.".clientes WHERE (cli.usuario_alta=".$usuarioId." OR cli.asignadoa=".$usuarioId.") ".
              "cli.fecha_alta BETWEEN '$monday 00:00:00' AND '$friday 23:59:59' ";    
  }
  
  $result = mysqli_query($db_modulos,$query);

  if(!$result){
    $salida = "ERROR|| No se pudo consultar de los datos de clientes en la DB ".$query;
  }
  else{
    $salida = "<div class=\"col-md-4 col-sm-12 col-lg-4 col-xs-12 col-no-padding\">\n".
              "<p class=\"tab\">Clientes</p> nuevos en esta semana<br>\n";   
    if(0==mysqli_num_rows($result)){
      $salida .= "No se han dado de alta clientes nuevos en esta semana";
    }
    else{ 
        
      $salida .= "<div class=\"stat-table-div\" id=\"div_weekCli\">\n".
                 "<table id=\"tbl_clients\" class=\"table table-hover table-striped\">\n".
                 "  <thead>\n".
                 "    <tr>\n".
                 "      <th>Nombre Comercial</th>\n".
                 "      <th>Razon Social</th>\n".
                 "      <th>Agregado por:</th>\n".
                 "    </tr>\n".
                 "  </thead>\n".
                 "  <tbody>\n";
      while($row = mysqli_fetch_assoc($result)){
        $nombre = get_userRealName($row['usuario_alta']);
        
        $salida .= "    <tr>\n".
                   "      <td>".$row["nombre_comercial"]."</td>\n".
                   "      <td>".$row["razon_social"]."</td>\n".
                   "      <td><a href=/crm/modulos/perfil/index.php?id='".encrypt($row['usuario_alta'])."'>".$nombre."</a></td>\n".
                   "    </tr>\n";                 
      }
      $salida .= "  </tbody>\n".
                 "</table>\n";                     
    } 
    $salida .= "</div></div>";   
  }   
  return $salida;
}

function getWeekProspect(){
  global $db_modulos;
  global $cfgTableNameMod;
  global $cfgTableNameUsr;
  global $usrUbica;

  $monday = date('Y-m-d', strtotime('monday this week'));
  $friday = date('Y-m-d', strtotime('friday this week'));

  if((hasPermission(0,'r'))&&(hasPermission(0,'f'))){
    $query = "SELECT nombre_comercial, origen, asignadoa, estatus, usuario_alta ".
             "FROM ".$cfgTableNameMod.".prospectos WHERE fecha_alta BETWEEN '$monday 00:00:00' AND '$friday 00:00:00' ";
  }
  if((hasPermission(0,'r'))&&(hasPermission(0,'c'))){
    $query = "SELECT pr.nombre_comercial, pr.origen, pr.estatus, pr.asignadoa, pr.usuario_alta ".
             "FROM ".$cfgTableNameMod.".prospectos AS pr, ".$cfgTableNameUsr.".usuarios AS usr ". 
             "WHERE pr.usuario_alta=usr.usuario_id AND usr.ubicacion_id=".$usrUbica.
             " AND fecha_alta BETWEEN '$monday 00:00:00' AND '$friday 00:00:00' ";
  }
  if(hasPermission(0,'l')){
     $query = "SELECT pr.nombre_comercial, pr.origen, pr.estatus, pr.asignadoa, pr.usuario_alta ".
              "FROM ".$cfgTableNameMod.".prospectos WHERE (usuario_alta=".$usuarioId." OR asignadoa=".$usuarioId.") ".
              "fecha_alta BETWEEN '$monday 00:00:00' AND '$friday 00:00:00' ";    
  }
  
  $result = mysqli_query($db_modulos,$query);

  if(!$result){
    $salida = "ERROR|| No se pudo consultar de los datos de prospectos en la DB";
  }
  else{
    $salida = "<div class=\"col-md-4 col-sm-12 col-lg-4 col-xs-12 col-no-padding\">\n".
              "<p class=\"tab\">Prospectos</p> nuevos en esta semana<br>\n";      
    if(0==mysqli_num_rows($result)){
      $salida .= "No se han dado de alta prospectos nuevos en esta semana";
    }
    else{     
      $salida .= "<div class=\"stat-table-div\" id=\"div_weekPro\">\n".
                 "<table id=\"tbl_prospects\" class=\"table table-hover table-striped\">\n".
                 "  <thead>\n".
                 "    <tr>\n".
                 "      <th>Nombre Comercial</th>\n".
                 "      <th>Origen Prospecto</th>\n".
                 "      <th>Estatus</th>\n".
                 "      <th>Asignado a:</th>\n".
                 "    </tr>\n".
                 "  </thead>\n".
                 "  <tbody>\n";
      while($row = mysqli_fetch_assoc($result)){
        $estatus = get_sellStat($row['estatus']);
        $nombre = get_userRealName($row['asignadoa']);
        $origen = get_sellOrigin($row['origen']);
        
        $salida .= "    <tr>\n".
                   "      <td>".$row["nombre_comercial"]."</td>\n".
                   "      <td>".$origen."</td>\n".
                   "      <td>".$estatus."</td>\n".
                   "      <td><a href=/crm/modulos/perfil/index.php?id='".encrypt($row['asignadoa'])."'>".$nombre."</a></td>\n".
                   "    </tr>\n";                 
      }
      $salida .= "  </tbody>\n".
                 "</table>\n";                    
    }
    $salida .= "</div></div>";    
  }
  return $salida;
}

function weekSeller($option){
  global $db_modulos;
  global $cfgTableNameMod;
  global $cfgTableNameUsr;
  global $usrUbica;
    
  $monday = date('Y-m-d', strtotime('monday this week'));
  $friday = date('Y-m-d', strtotime('friday this week'));
  
  switch($option){
    case "prospect":
      $name = "prospectos";
      if((hasPermission(0,'r'))&&(hasPermission(0,'f'))){
        $query = "SELECT COUNT(pro.prospecto_id) AS conteo, usr.usuario_id ".
                 "FROM ".$cfgTableNameMod.".prospectos AS pro, ".
                 $cfgTableNameUsr.".usuarios AS usr ".
                 "WHERE pro.fecha_alta BETWEEN '$monday 00:00:00' AND '$friday 00:00:00' ".
                 "AND pro.asignadoa = usr.usuario_id ".
                 "GROUP BY usr.usuario_id ORDER BY pro.estatus DESC";
      }
      elseif((hasPermission(0,'l'))&&(hasPermission(0,'c'))){
        $query = "SELECT COUNT(pro.prospecto_id) AS conteo, usr.usuario_id ".
                 "FROM ".$cfgTableNameMod.".prospectos AS pro, ".
                 $cfgTableNameUsr.".usuarios AS usr ".
                 "WHERE pro.fecha_alta BETWEEN '$monday 00:00:00' AND '$friday 00:00:00' ".
                 "AND pro.asignadoa = usr.usuario_id ".
                 "AND usr.ubicacion_id=".$usrUbica.
                 " GROUP BY usr.usuario_id ORDER BY pro.estatus DESC"; 
      }
    break;
    case "client":
      $name = "clientes";
      if((hasPermission(0,'r'))&&(hasPermission(0,'f'))){
        $query = "SELECT COUNT(cli.cliente_id) AS conteo, usr.usuario_id ".
                 "FROM ".$cfgTableNameMod.".clientes AS cli, ".
                 $cfgTableNameUsr.".usuarios AS usr ".
                 "WHERE cli.fecha_alta BETWEEN '$monday 00:00:00' AND '$friday 00:00:00' ".
                 "AND cli.usuario_alta = usr.usuario_id ".
                 "GROUP BY usr.usuario_id";
      }
      elseif((hasPermission(0,'l'))&&(hasPermission(0,'c'))){
        $query = "SELECT COUNT(cli.cliente_id) AS conteo, usr.usuario_id ".
                 "FROM ".$cfgTableNameMod.".clientes AS cli, ".
                 $cfgTableNameUsr.".usuarios AS usr ".
                 "WHERE cli.fecha_alta BETWEEN '$monday 00:00:00' AND '$friday 00:00:00' ".
                 "AND cli.usuario_alta = usr.usuario_id ".
                 "AND usr.ubicacion_id=".$usrUbica.
                 " GROUP BY usr.usuario_id";
      }
    break;
    case "appointment":
      $name = "citas";
      if((hasPermission(0,'r'))&&(hasPermission(0,'f'))){
        $query = "SELECT COUNT(eve.id) AS conteo, usr.usuario_id ".
                 "FROM ".$cfgTableNameMod.".calendario AS eve, ".
                 $cfgTableNameUsr.".usuarios AS usr ".
                 "WHERE eve.fecha_alta BETWEEN '$monday 00:00:00' AND '$friday 00:00:00' ".
                 "AND eve.usuario_id = usr.usuario_id ".
                 " GROUP BY usr.usuario_id"; 
      }
      elseif((hasPermission(0,'l'))&&(hasPermission(0,'c'))){
        $query = "SELECT COUNT(eve.id) AS conteo, usr.usuario_id ".
                 "FROM ".$cfgTableNameMod.".calendario AS eve, ".
                 $cfgTableNameUsr.".usuarios AS usr ".
                 "WHERE eve.fecha_alta BETWEEN '$monday 00:00:00' AND '$friday 00:00:00' ".
                 "AND eve.usuario_id = usr.usuario_id ".
                 "AND usr.ubicacion_id=".$usrUbica.
                 " GROUP BY usr.usuario_id";
      }         
    break;
  
  
  }
  
  $result = mysqli_query($db_modulos,$query);

  if(!$result){
    $salida = "ERROR|| No se pudo consultar de los datos de prospectos en la DB ".$query;
  }
  else{
    $salida = "<div class=\"col-md-4 col-sm-12 col-lg-4 col-xs-12 col-no-padding\">\n".
              "<p class=\"tab\">Ranking</p> por ".$name." nuevos en la semana<br>\n";      
    if(0==mysqli_num_rows($result)){
      $salida .= "No se han dado de alta ".$name." nuevos en esta semana";
    }
    else{
      $rnkCont = 1;
      $salida .= "<div class=\"stat-table-div\" id=\"div_weekPro\">\n".
                 "<table id=\"tbl_".$name."_user\" class=\"table table-hover table-striped\">\n".
                 "  <thead>\n".
                 "    <tr>\n".
                 "      <th>Lugar</th>\n".
                 "      <th>Usuario</th>\n".
                 "      <th>".$name."</th>\n".
                 "    </tr>\n".
                 "  </thead>\n".
                 "  <tbody>\n";
      while($row = mysqli_fetch_assoc($result)){
        $nombre = get_userRealName($row['usuario_id']);
        
        $salida .= "    <tr>\n";        
        if(1==$rnkCont)
          $salida .= "      <td><i class=\"fa fa-trophy\"></i></td>\n";
        else
          $salida .= "      <td><i class=\"fa fa-star-o\"></i></td>\n";
        
        $salida .= "      <td><a href=/crm/modulos/perfil/index.php?id='".encrypt($row['usuario_id'])."'>".$nombre."</a></td>\n".
                   "      <td>".$row["conteo"]."</td>\n".
                   "    </tr>\n";    
        $rnkCont++;                        
      }
      $salida .= "  </tbody>\n".
                 "</table>\n";
                     
    }
    $salida .= "</div></div>";
  }
  return $salida;  
}

if("getTableDataSell" == $_POST["action"]){

  /*$salida .= getWeekProspect();
  $salida .= getWeekClients();*/
  $salida .= weekSeller("prospect");
  $salida .= weekSeller("client");
  $salida .= weekSeller("appointment");

  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}

if("getPieChartData" == $_POST["action"]){
  
  $monday = date('Y-m-d', strtotime('monday this week'));
  $friday = date('Y-m-d', strtotime('friday this week'));
  
  switch($_POST["source"]){
    
    case "weekProspects":
      /*$query = "SELECT count(pro.prospecto_id) AS cnt, cat.estatus AS label ".
               "FROM ".$cfgTableNameMod.".prospectos AS pro, ".
               $cfgTableNameCat.".cat_venta_estatus AS cat ".
               "WHERE pro.fecha_alta BETWEEN '$monday 00:00:00' AND '$friday 23:59:59' ".
               "AND cat.venta_estatus_id = pro.estatus GROUP BY pro.estatus";*/
      if(hasPermission(0,'c')&&hasPermission(0,'r')){
        $query = "SELECT count(pro.prospecto_id) AS cnt, cat.estatus AS label ".
                 "FROM ".$cfgTableNameMod.".prospectos AS pro, ".
                 $cfgTableNameUsr.".usuarios AS usr, ".
                 $cfgTableNameCat.".cat_venta_estatus AS cat ".
                 "WHERE pro.asignadoa=usr.usuario_id ".
                 "AND cat.venta_estatus_id = pro.estatus ".
                 "AND usr.ubicacion_id=".$usrUbica." ".
                 "GROUP BY pro.estatus ";
      }
      elseif(hasPermission(0,'f')&&hasPermission(0,'r')){
        $query = "SELECT count(pro.prospecto_id) AS cnt, cat.estatus AS label ".
                 "FROM ".$cfgTableNameMod.".prospectos AS pro, ".
                 $cfgTableNameCat.".cat_venta_estatus AS cat ".
                 "WHERE cat.venta_estatus_id = pro.estatus GROUP BY pro.estatus";
      } 
      elseif(hasPermission(0,'l')){
        $query = "SELECT count(pro.prospecto_id) AS cnt, cat.estatus AS label ".
                 "FROM ".$cfgTableNameMod.".prospectos AS pro, ".
                 $cfgTableNameCat.".cat_venta_estatus AS cat ".
                 "WHERE cat.venta_estatus_id = pro.estatus GROUP BY pro.estatus ".
                 "AND pro.asignadoa=".$usuarioId;
      }
      else
        $query = "ERROR||No se determinó el tipo de usuario";       
      
      
    break;
    case "weekClients":
      if(hasPermission(0,'c')&&hasPermission(0,'r')){  
        $query = "SELECT count(cli.cliente_id) AS cnt, cat.venta AS label ".
                 "FROM ".$cfgTableNameMod.".clientes AS cli, ".
                 $cfgTableNameCat.".cat_venta_origen AS cat, ".
                 $cfgTableNameUsr.".usuarios AS usr ".
                 "WHERE cat.venta_origen_id = cli.origen ".
                 "AND cli.usuario_alta = usr.usuario_id ".
                 "AND usr.ubicacion_id=".$usrUbica." ". 
                 "GROUP BY cli.origen ";
      }
      elseif(hasPermission(0,'f')&&hasPermission(0,'r')){
        $query = "SELECT count(cli.cliente_id) AS cnt, cat.venta AS label ".
                 "FROM ".$cfgTableNameMod.".clientes AS cli, ".
                 $cfgTableNameCat.".cat_venta_origen AS cat ".
                 "WHERE cat.venta_origen_id = cli.origen ".
                 "GROUP BY cli.origen";
      }
      elseif(hasPermission(0,'l')){
        $query = "SELECT count(cli.cliente_id) AS cnt, cat.venta AS label ".
                 "FROM ".$cfgTableNameMod.".clientes AS cli, ".
                 $cfgTableNameCat.".cat_venta_origen AS cat ".
                 "WHERE cat.venta_origen_id = cli.origen GROUP BY cli.origen ".
                 "AND cli.usuario_alta=".$usuarioId;
      } 
      else
        $query = "ERROR||No se determinó el tipo de usuario";      
      /*$query = "SELECT count(cli.cliente_id) AS cnt, cat.venta AS label ".
               "FROM ".$cfgTableNameMod.".clientes AS cli, ".
               $cfgTableNameCat.".cat_venta_origen AS cat ".
               "WHERE cli.fecha_alta BETWEEN '$monday 00:00:00' AND '$friday 23:59:59' ".
               "AND cat.venta_origen_id = cli.origen GROUP BY cli.origen";*/
      
    break;
    case "SellMilestone":
      $mes = datetime("Y-m");
      if(hasPermission(0,'c')&&hasPermission(0,'r')){
        $query = "SELECT pr.estatus,pr.asignadoa,pr.fecha_alta,pr.fecha_actualizacion, ".
                 "cat.porcentaje,cat.orden,cat.tiempo_limite".
                 "FROM ".$cfgTableNameMod.".prospectos AS pr, ".
                         $cfgTableNameCat.".cat_venta_estatus AS cat, ".
                         $cfgTableNameUsr.".usuarios AS usr ".
                 "WHERE pr.fecha_alta BETWEEN '".$mes."-01 00:00:00' AND '".$mes."-31 23:59:59 ".
                 "AND pr.usuario_alta = usr.usuario_id ".
                 "AND usr.ubicacion_id=".$usrUbica." ". 
                 "AND cat.venta_estatus_id=pr.esatus ORDER BY cat.orden";
      }
      elseif(hasPermission(0,'f')&&hasPermission(0,'r')){
        $query = "SELECT pr.estatus,pr.asignadoa,pr.fecha_alta,pr.fecha_actualizacion, ".
                 "cat.porcentaje,cat.orden,cat.tiempo_limite".
                 "FROM ".$cfgTableNameMod.".prospectos AS pr, ".
                         $cfgTableNameCat.".cat_venta_estatus AS cat ".
                 "WHERE pr.fecha_alta BETWEEN '".$mes."-01 00:00:00' AND '".$mes."-31 23:59:59 ".
                 "AND cat.venta_estatus_id=pr.esatus ORDER BY cat.orden";
      }
      elseif(hasPermission(0,'l')){
        $query = "SELECT pr.estatus,pr.asignadoa,pr.fecha_alta,pr.fecha_actualizacion, ".
                 "cat.porcentaje,cat.orden,cat.tiempo_limite".
                 "FROM ".$cfgTableNameMod.".prospectos AS pr, ".
                         $cfgTableNameCat.".cat_venta_estatus AS cat ".
                 "WHERE pr.fecha_alta BETWEEN '".$mes."-01 00:00:00' AND '".$mes."-31 23:59:59 ".
                 "AND (pr.usuario_alta=".$usuarioId." OR pr.asignadoa=".$usuarioId.") ".
                 "AND cat.venta_estatus_id=pr.esatus ORDER BY cat.orden";
      }
      else
        $query = "ERROR||No se determinó el tipo de usuario"; 
      
    break;
    default:
      $salida = "ERROR||Ocurrió un problema al obtener el tipo de origen para los datos de este gráfico";
      goto gpcda;
    break;
  }
  $salida = getPieData($query);
  //$salida = "ERROR||".$query; 
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios); 
  gpcda: 
  echo $salida;
}

if("getPromo" == $_POST["action"]){
  $regId   = isset($_POST['regId']) ? $_POST['regId'] : -1;
  
  $query = "SELECT * FROM cat_promociones WHERE promo_region=".$regId." AND promocion_estatus = 1";
  
  $result = mysqli_query($db_catalogos,$query);
  
  if(!$result){
    $salida = "ERROR: No se pudo obtener el listado de promociones ".$query;
  }
  else{
    if(0>=mysqli_num_rows($result)){
      $salida = "Aún no contamos con promociones vigentes";  
    }
    else{
      $salida = "<ul class=\"ul-custom1\">";
      while($row=mysqli_fetch_assoc($result)){
        $salida .= "  <li>".$row['promocion']."</li>";
      }
      $salida .= "</ul>";
    }
  }
  
  echo $salida;
}

if("getCalendar" == $_POST["action"]){
  
  $salida = "";
  $query = "SELECT cal.*, typ.color, sta.estatus FROM ".$cfgTableNameMod.".calendario AS cal, ".$cfgTableNameCat.".cat_tipo_eventos AS typ, ".$cfgTableNameCat.".cat_evento_estatus AS sta WHERE cal.tipo_evento_id=typ.tipo_evento_id AND cal.estatus_id=sta.evento_estatus_id";

  $result = mysqli_query($db_modulos,$query);

  if(!$result){
    $salida = "ERROR||No se pudo recabar los eventos del calendario";
  }
  else{
    $exArr = array();
    while ($row = mysqli_fetch_assoc($result)){
    
    $start = explode(" ",$row['fecha_inicio']);      
    $end = explode(" ",$row['fecha_fin']);
    
    $exArr[] = array("id"              => $row['id'],
                     "typeId"          => $row['tipo_evento_id'],
                     "title"           => $row['titulo'],
                     "start"           => $start[0].' '.$start[1],
                     "end"             => $end[0].' '.$end[1],
                     "modify"          => $modify,
                     "delete"          => $delete,
                     "user"            => get_userRealName($row['usuario_id']),
                     "tip"             => $row['descripcion'],
                     "description"     => $row['descripcion'],
                     "backgroundColor" => "#".$row['color'],
                     "status"          => $row['estatus'],
                     "allDay"          => intval($row['todo_dia']));
    }
    $salida = json_encode($exArr,false);
  }  
  echo $salida;
}


}//fin else sesion
?>
