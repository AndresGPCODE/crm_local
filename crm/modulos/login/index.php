<?php
session_start();

include_once "../../libs/db/session.php";

if (verifySession($_SESSION)) {
  header('Location: ../main/');
} else {
  if(isset($_GET['sub'])){
    $sub = $_GET['sub'];
  }else{
    $sub = "";
  }
  
  ?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Coeficiente CRM Login</title>
  <!-- Bootstrap core CSS -->
  <link href="../../libs/AdminLTE2/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.css" rel="stylesheet" type="text/css" />
  <script src="../../libs/AdminLTE2/plugins/jQuery/jQuery-2.1.3.js" type="text/javascript"></script>
  <script src="../../libs/AdminLTE2/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="../../libs/AdminLTE2/dist/js/app.js" type="text/javascript"></script>
  <script src="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.js" type="text/javascript"></script>
  <script src="script.js" type="text/javascript"></script>
  <link rel="icon" type="image/png" href="../../img/favicon-16x16.png" sizes="16x16">
  <style type="text/css">
    body {
      background-image: url("../../img/interface-01-01.jpg");
      background-size: 100%;
      padding-top: 10%;
      padding-bottom: 40px;
      /*background-color: #fff;*/
      background-repeat: no-repeat;
      background-attachment: fixed;
    }

    .footer {
      padding-top: 10px;
      position: absolute;
      bottom: 0;
      left: 0;
      width: 100%;
      height: 45px;
      background-color: #f5f5f5;
    }

    .form-signin {
      max-width: 330px;
      padding: 15px;
      margin: 0 auto;
    }

    .form-signin .form-signin-heading,
    .form-signin .checkbox {
      margin-bottom: 10px;
    }

    .form-signin .checkbox {
      font-weight: normal;
    }

    .form-signin .form-control {
      position: relative;
      height: auto;
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
      padding: 10px;
      font-size: 16px;
    }

    .form-signin .form-control:focus {
      z-index: 2;
    }

    .form-signin input[type="email"] {
      margin-bottom: 10px;
      border-bottom-right-radius: 0;
      border-bottom-left-radius: 0;
      background-color: #eee;
    }

    .form-signin input[type="password"] {
      margin-bottom: 10px;
      border-top-left-radius: 0;
      border-top-right-radius: 0;
      background-color: #eee;
    }

    .btn {
      color: #ffffff;
      background: -webkit-linear-gradient(#7d1317, #b10511);
      /* Safari */
      background: -o-linear-gradient(#7d1317, #b10511);
      /* Opera */
      background: -moz-linear-gradient(#7d1317, #b10511);
      /* Firefox */
      background: linear-gradient(#7d1317, #b10511);
      /* Chrome IE */
    }

    .form-signing-round {
      /*#ebebeb,#d1d2d7*/
      border: 1px solid #d1d2d7;
      border-radius: 25px;
      padding: 30px;
      padding-left: 30px;
      padding-right: 30px;
      background: -webkit-linear-gradient(#ffffff, #eee);
      /* Safari */
      background: -o-linear-gradient(#ffffff, #eee);
      /* Opera */
      background: -moz-linear-gradient(#ffffff, #eee);
      /* Firefox */
      background: linear-gradient(#ffffff, #eee);
      /* Chrome IE */
    }
  </style>
</head>
<body>
  <div class="container">
    <div class="row">
      <p align="center"><img alt="Coeficiente" src="/crm/crm/img/logo_coe.png" class="img-responsive"></img></p>
    </div>
  </div> <!-- /container -->
  <div class="container">
    <div class="row">
      <div id="div_msgAlert"></div>
    </div>
  </div> <!-- /container -->
  <div class="container">
    <form class="form-signin form-signing-round">
      <label for="inputUser" class="sr-only">Usuario</label>
      <input type="text" id="inputUser" class="form-control" placeholder="Nombre de usuario" autofocus><br>
      <label for="inputPassword" class="sr-only">Contraseña</label>
      <input type="password" id="inputPassword" class="form-control" placeholder="Contraseña">
      <p align="center"><button type="button" class="btn btn-lg btn-block" id="btn_entrar">Ingresar</button></p>
      <span align="center"><a href="resetpass.php">Olvidé mi contraseña</a></span>
      <input type="hidden" id="hdn_sub" name="hdn_sub" value=<?php echo $sub ?>>
    </form>
  </div> <!-- /container -->
  <footer class="footer">
    <div class="container">
      <p class="text-muted" align="center">Powered by <a href="http://www.ctclat.com/">CTCLat</a> © 2015-2016 Todos los derechos reservados. COECRM versión 0.1.26 Beta</p>
    </div>
  </footer>
</body>
</html>
<?php
}
?>