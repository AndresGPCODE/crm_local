$(document).ready(function(){  
  $("#inputUser").keypress(function(e){
    if(e.which === 13){
      login();
    }  
  });
  
  $("#inputPassword").keypress(function(e){
    if(e.which === 13){
      login();
    }  
  });
  
  $(document).on('click','#btn_entrar',function(){
    login();
  }); 
  
  if('timeout'==$("#hdn_sub").val()){
    $("#div_msgAlert").html("<div class=\"alert alert-danger alert-dismissable\">"+
                            "  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>"+
                            "  <strong>Error:</strong> No ha iniciado sesión, ó su tiempo de sesión ha finalizado. Por favor, inicie sesión."+
                            "</div>");
  } 
  else if('logout'==$("#hdn_sub").val()){
    $("#div_msgAlert").html("<div class=\"alert alert-success alert-dismissable\">"+
                            "  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>"+
                            "  <strong>Ok:</strong> Ha cerrado correctamente su sesión."+
                            "</div>");
  }
  else if('pwdChg'==$("#hdn_sub").val()){
    $("#div_msgAlert").html("<div class=\"alert alert-success alert-dismissable\">"+
                            "  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>"+
                            "  <strong>Ok:</strong> Ha cambiado correctamente su contraseña. por favor inicie sesión."+
                            "</div>");
  }
});  

function login(){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {'accion':"login",'username':$("#inputUser").val(),'password':$("#inputPassword").val()},
    success: function(msg){
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        window.location="../main/";        
      }
      else{
        if(undefined == msg||undefined == resp[1]){
          msg = "Ocurrió un error desconocido, consulte con su administrador";
        }
        $("#div_msgAlert").html("<div class=\"alert alert-danger alert-dismissable\">"+
                                "  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>"+
                                "  <strong>Error:</strong> "+resp[1]+"."+
                                "</div>");
        $("#inputPassword").val("");
      }   
    }
  });
}
