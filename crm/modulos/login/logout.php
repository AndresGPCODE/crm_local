<?php
session_start();
include_once('../../libs/db/dbcommon.php');
include_once('../../libs/db/log.php');
$db_usuarios  = condb_usuarios();
$cfgLogService = "../../log/loginlog";
$sub = $_GET['sub'];
//$query = "DELETE FROM sesiones WHERE sesion_hash='".session_id()."' AND usuario_id=".$_SESSION['usrId'];
$query = "DELETE FROM sesiones WHERE usuario_id=".$_SESSION['usrId'];
$result = mysqli_query($db_usuarios,$query);
//log_write("DEBUG: LOGIN: ".$query,5);
if(!$result){
  log_write("ERROR: LOGIN: No se pudo eliminar los datos de sesión obsoletos",5);
  //die("ERROR AL ELIMINAR LOS DATOS OBSOLETOS DE SESIÓN");
}
else{
  writeOnJournal($_SESSION['usrId'],"Cerró sesión");
  log_write("DEBUG: LOGIN: El usuario ".$_SESSION['usuario']." se ha deslogueado",5);
}
mysqli_close($db_usuarios);
session_destroy();
printf("<script>location.href='../login/index.php?sub=".$sub."'</script>");
//header('Location: ../login');
?>
