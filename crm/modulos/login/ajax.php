<?php
session_start();
set_time_limit(0);
include_once('../../libs/db/dbcommon.php');
include_once('../../libs/db/encrypt.php');
include_once('../../libs/db/log.php');

$db_usuarios  = condb_usuarios();
$salida = "";
$cfgLogService = "../../log/loginlog";

if($_POST['accion']=="login"){
  
  $usuario  = (isset($_POST['username'])) ? $_POST['username'] : "";
  $pass     = (isset($_POST['password'])) ? encrypt($_POST['password']) : "";
  
  $query = "SELECT usuario_id,estatus,nombre,apellidos,ubicacion_id,email,nIDExtension,nIDUsuario FROM usuarios WHERE BINARY username=? AND BINARY contrasena=?";  
  
  /*Se crea un objeto stmt ó "prepared statement" con el query a ejecutar y que contiene datos insertados por el usuario como parámetros de búsqueda. El stmt sirve como blindaje contra ataques de inyección SQL ya que impide al usuario malicioso agregar consultas mediante el formulario pues la consulta ya fue definida con anterioridad*/
  
  $stmt1 = mysqli_prepare($db_usuarios,$query);
  
  mysqli_stmt_bind_param($stmt1,"ss",$usuario,$pass);//se le agrega o quita otra s (string) o d (decimal) dependiendo de cuantos parámetros sean
  
  $result = mysqli_stmt_execute($stmt1);  

  if (!$result) {
    log_write("ERROR: LOGIN: Ocurrió un problema al consultar los datos del usuario",5);
    $salida = "ERROR|| Ocurrió un problema al iniciar sesión";
  }
  else{ 
    mysqli_stmt_store_result($stmt1);   
    if(0==mysqli_stmt_num_rows($stmt1)){
      log_write("ERROR: LOGIN: el usuario [".$usuario."] intentó sin exito iniciar sesión",5);
      $salida = "ERROR|| Los datos de inicio de sesión son incorrectos";
    }  
    else{
      mysqli_stmt_bind_result($stmt1,$usrId,$usrStat,$usrName,$usrLastName,$usrLocation,$usrEmail,$usrIDExt,$usrIDUsr);
      mysqli_stmt_fetch($stmt1);
      if(0!=$usrStat){
        log_write("ERROR: LOGIN: El usuario [".$usuario."] intentó logearse pero se encuentra desactivado",5);
        $salida = "ERROR|| Su cuenta de usuario se encuentra desactivada. Consulte con su administrador";
      }
      else{              
        $query2 = "SELECT rel.grupo_id, gr.grupo, per.modulo_id, per.permiso FROM rel_usuario_grupo AS rel, grupos AS gr, rel_grupo_permisos AS per WHERE rel.grupo_id=gr.grupo_id AND per.grupo_id=gr.grupo_id AND rel.usuario_id=?";
        
        $stmt2 = mysqli_prepare($db_usuarios,$query2);
  
        mysqli_stmt_bind_param($stmt2,"s",$usrId);
        
        $result2 = mysqli_stmt_execute($stmt2);
        
        if(!$result2){
          log_write("ERROR: LOGIN: No se encontraron datos de grupo para ese usuario",5);
          $salida = "ERROR|| No se encontraron datos de grupo para este usuario. Consulte con su administrador";
        }
        else{
          $groupArr = array();
          
          mysqli_stmt_bind_result($stmt2,$gpoId,$gpo,$modId,$permission);
          
          while(mysqli_stmt_fetch($stmt2)){                            
            $groupArr[$gpoId][$modId] = $permission;    
//Ejemplo: groupArr[2][0]=>rwedaf donde 2=grupo Jefe ventas 0=modulo Prospectos rwedaf= clave de permisos, consultar catalogo de permisos en la DB para ver lo que significan       
          }
          log_write("DEBUG: LOGIN: Arreglo de permisos: <pre>".print_r($groupArr,true)."</pre>",5);
                    
          log_write("DEBUG: LOGIN: Se asignaron los valores de sesión",5);
          
          $_SESSION['grupos']    = $groupArr;
          $_SESSION['usrId']     = $usrId;
          $_SESSION['usuario']   = $usuario;
          $_SESSION['usrNombre'] = $usrName." ".$usrLastName;
          $_SESSION['usrUbica']  = $usrLocation;
          $_SESSION['usrEmail']  = $usrEmail;
          $_SESSION['numUnreadMsgs'] = 0;
          $_SESSION['nIDExtension'] = $usrIDExt;
          $_SESSION['nIDUsuario'] = $usrIDUsr;
          
          if(!insertSessionData($_SESSION)){
            $salida = "ERROR||Ocurrió un problema al guardarse los datos de sesión";
            session_destroy();
          }
          else
            $salida = "OK||La sesión inició con éxito";  
        }                 
      }                
    }
  }
    echo $salida; 
    mysqli_stmt_close($stmt1); 
    mysqli_stmt_close($stmt2); 
    mysqli_close($db_usuarios);
}

function insertSessionData($session){
  global $db_usuarios;
  global $cfgLogService;
  
  $fecha  = date("Y-m-d H:i:s");
  
  $_SESSION['loginFecha'] = $fecha;
  
  $query = "INSERT INTO sesiones(usuario_id,sesion_hash,fecha_hora,online) VALUES(".$session['usrId'].",'".session_id()."','".$fecha."',1)";
   
  $result = mysqli_query($db_usuarios,$query);
  
  if(!$result){
    log_write("ERROR: LOGIN: No se escribieron los valores de sesión en la DB",5);
    $salida = "ERROR|| No se pudo crear los datos para la sesión actual";
    return false;
  }
  else{ 
    writeOnJournal($session['usrId'],"Inició Sesión");
    log_write("OK: LOGIN: Se escribieron los valores de sesión en la DB",5);
    return true;
  }
}

?>
