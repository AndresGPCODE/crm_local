<?php
  session_start();
  include_once "../../libs/db/common.php";
  include_once "../../libs/db/encrypt.php";
  
  header('Content-Type: text/html; charset=UTF-8');
  date_default_timezone_set('America/Mexico_City');
  
  $action = isset($_POST['action'])?$_POST['action']:"default";
  
  if("resetPassSend"==$action){
    $usuario = isset($_POST['usr'])?$_POST['usr']:"";
    $temppass = encrypt(openssl_random_pseudo_bytes(4));
    $query = "SELECT email FROM usuarios WHERE BINARY username = ?";
    $stmt1 = mysqli_prepare($db_usuarios,$query);
    mysqli_stmt_bind_param($stmt1,"s",$usuario);
    $result = mysqli_stmt_execute($stmt1);  
    mysqli_stmt_store_result($stmt1);  
    if(0==mysqli_stmt_num_rows($stmt1)){
      $salida = "ERROR||Nombre de usuario no encontrado. Verifique que sea correcto.";
    }
    else{
      mysqli_stmt_bind_result($stmt1,$usrEmail); 
      mysqli_stmt_fetch($stmt1);
      
      
    }
  }
  elseif("confirmPassChange"==$action){
    // $
  }
  else{
  
  }
?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Coeficiente CRM Login</title>

    <!-- Bootstrap core CSS -->  
    <link href="../../libs/AdminLTE2/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.css" rel="stylesheet" type="text/css"/>  
    
    <script src="../../libs/AdminLTE2/plugins/jQuery/jQuery-2.1.3.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/dist/js/app.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.js" type="text/javascript"></script>
    <script src="script.js" type="text/javascript"></script>
    
    <link rel="icon" type="image/png" href="../../img/favicon-16x16.png" sizes="16x16">
    
    <style type="text/css">
    </style>
    <script type="text/javascript">
      function resetPass(){
        var usr = $('#inputUser').val();
        
        $.ajax({
          type: 'POST',
          url: 'resetpass.php',
          data: {"action":"resetPassSend", "usr":usr},
          beforeSend: function(){
            setLoadDialog(0,"Restaurando contraseña");
          },
          complete: function(){
            setLoadDialog(1,"");
          },
          success: function(msg){
            $("#div_alertMsg").html(msg);     
            window.open("../../libs/db/contractpdf3.php?sub=pwdChg",'_self');
          } 
        });
      }
    </script>
  </head>

  <body>
    <div class="container">
      <div class="row">
        <p align="center"><img alt="Coeficiente" src="/crm/img/logo_coe.png" class="img-responsive"></img></p>
      </div>
    </div> <!-- /container -->
    <div class="container">
      <div class="row">
        <div id="div_msgAlert"></div>
      </div>
    </div> <!-- /container -->
    <div class="container">
      <form class="form-signin form-signing-round">
        <label>Introduzca su correo electrónico, se le enviará una contraseña nueva</label>
        <label for="inputUser" class="sr-only">Correo Electrónico</label>
        <input type="text" id="inputUser" class="form-control" placeholder="Nombre de usuario" autofocus><br>
        <p align="center"><button type="button" class="btn btn-lg btn-block" id="btn_resetPass" onClick="resetPass();">Enviar</button></p>
        <input type="hidden" id="hdn_sub" name="hdn_sub" value=<?php echo $sub?>>
      </form>
    </div> <!-- /container --> 
    <footer class="footer">
     <div class="container">
      <p class="text-muted" align="center">Powered by <a href="http://www.ctclat.com/">CTCLat</a> © 2015-2016 Todos los derechos reservados. COECRM versión 0.1.26 Beta</p>
     </div>
    </footer>   
  </body>
</html>
