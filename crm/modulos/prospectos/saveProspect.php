<?php  //-*-mode: php-*-
session_start();
set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

//include_once "../../libs/db/dbcommon.php";
include_once "../../libs/db/common.php";

$gruposArr  = $_SESSION['grupos'];
$usuarioId  = $_SESSION['usrId'];
$usuario    = $_SESSION['usuario'];
$nombre     = $_SESSION['usrNombre'];
$ubicacion  = $_SESSION['usrUbica'];

$modId = 0; //id del módulo (consultar el catálogo de módulos en la DB)

if(!verifySession($_SESSION)){
  logoutTimeout();
}else{

$cfgLogService = "../../log/prospectlog";

$db_catalogos = condb_catalogos();
$db_modulos   = condb_modulos();
$db_usuarios  = condb_usuarios();

$salida = "";

$prosId      = isset($_POST['hdn_prosId'])      ? $_POST['hdn_prosId'] : -1;

$fecha       = date("Y-m-d H:i:s");
$razonSoc    = isset($_POST['txt_razonSoc'])    ? $_POST['txt_razonSoc'] : "";
$rfc         = isset($_POST['txt_rfc'])         ? $_POST['txt_rfc'] : "";
$tipPer      = isset($_POST['rad_persona'])     ? $_POST['rad_persona'] : "";
$nomCom      = isset($_POST['txt_nomCom'])      ? $_POST['txt_nomCom'] : "";
$tel         = isset($_POST['txt_tel'])         ? $_POST['txt_tel'] : "";
$telExt      = isset($_POST['txt_ext'])         ? $_POST['txt_ext'] : "";
$cell        = isset($_POST['txt_cell'])        ? $_POST['txt_cell'] : "";
$dom         = isset($_POST['txt_dom'])         ? $_POST['txt_dom'] : "";
$col         = isset($_POST['txt_col'])         ? $_POST['txt_col'] : "";
$cp          = (isset($_POST['txt_cp'])&&$_POST['txt_cp']!="") ? $_POST['txt_cp'] : 0;
$mun         = isset($_POST['txt_mun'])         ? $_POST['txt_mun'] : "";
$state       = isset($_POST['sel_state'])       ? $_POST['sel_state'] : 0;
$email       = isset($_POST['txt_email'])       ? $_POST['txt_email'] : "";
$origin      = isset($_POST['sel_origin'])      ? $_POST['sel_origin'] : 0;
$originOth   = isset($_POST['txt_origin'])      ? $_POST['txt_origin'] : "";
$status      = isset($_POST['sel_status'])      ? $_POST['sel_status'] : 0;
$assignUsers = isset($_POST['sel_assignUsers']) ? $_POST['sel_assignUsers'] : 0;
$desc        = isset($_POST['txt_desc'])        ? $_POST['txt_desc'] : "";
$hdnAction   = isset($_POST['hdn_action'])      ? $_POST['hdn_action'] : -1;
$contArr     = isset($_POST['hdn_contArr'])     ? json_decode($_POST['hdn_contArr']) : "";
$giroCom     = isset($_POST['sel_giroCom'])     ? $_POST['sel_giroCom'] : 0;
//$reprArr     = isset($_POST['manArr'])          ? json_decode($_POST['manArr']) : "";
$remLogo     = isset($_POST['chk_removeLogo'])  ? $_POST['chk_removeLogo'] : 0;
$numPod      = isset($_POST['txt_prosNumPod'])  ? $_POST['txt_prosNumPod'] : "";
$notario     = isset($_POST['txt_prosNotario']) ? $_POST['txt_prosNotario'] : "";
$folio       = isset($_POST['txt_prosFolio'])   ? $_POST['txt_prosFolio'] : "";
$fecPod      = isset($_POST['txt_prosFecPod'])  ? $_POST['txt_prosFecPod'] : "";


$tel2        = isset($_POST['txt_tel2'])        ? $_POST['txt_tel2'] : "";
$tel3        = isset($_POST['txt_tel3'])        ? $_POST['txt_tel3'] : "";
$numExt      = isset($_POST['txt_numExt'])      ? $_POST['txt_numExt'] : "";
$numInt      = isset($_POST['txt_numInt'])      ? $_POST['txt_numInt'] : "S/N";
$calle1      = isset($_POST['txt_calle1'])      ? $_POST['txt_calle1'] : "";
$calle2      = isset($_POST['txt_calle2'])      ? $_POST['txt_calle2'] : "";
$webPage     = isset($_POST['txt_webSite'])     ? $_POST['txt_webSite'] : "";
$pais        = isset($_POST['sel_country'])     ? $_POST['sel_country'] : "";
$latitud     = isset($_POST['hdn_proslatitud']) ? $_POST['hdn_proslatitud'] : -1;
$longitud    = isset($_POST['hdn_proslongitud'])? $_POST['hdn_proslongitud'] : -1; 
$facebook    = isset($_POST['txt_face'])        ? $_POST['txt_face'] : ""; 
$twitter     = isset($_POST['txt_twit'])        ? $_POST['txt_twit'] : ""; 
$instagram   = isset($_POST['txt_insta'])       ? $_POST['txt_insta'] : ""; 
$logo        = isset($_FILES['fl_logo'])        ? $_FILES['fl_logo'] : "";

if(0==$assignUsers)
  $assignUsers = $usuarioId;

$campos = array("prospecto_id"     => $prosId,
                "razon_social"     => $razonSoc,
                "rfc"              => $rfc,
                "tipo_persona"     => $tipPer,
                "nombre_comercial" => $nomCom,
                "telefono"         => $tel,
                "tel_movil"        => $cell,
                "domicilio"        => $dom,
                "colonia"          => $col,
                "cp"               => $cp,
                "municipio"        => $mun,
                "estado"           => $state,
                "email"            => $email,
                "origen"           => $origin,
                "origen_otro"      => $originOth,
                "estatus"          => $status,
                "asignadoa"        => $assignUsers,
                "descripcion"      => $desc,
                "giro_id"          => $giroCom,
                "facebook"         => $facebook,
                "twitter"          => $twitter,
                "instagram"        => $instagram,
                "num_poder"        => $numPod,
                "fecha_poder"      => $fecPod,
                "notario"          => $notario,
                "folio_mercantil"  => $folio);

$campos2 = array("telefono2"    => $tel2,
                 "telefono3"    => $tel3,
                 "num_exterior" => $numExt,
                 "num_interior" => $numInt,
                 "calle1"       => $calle1,
                 "calle2"       => $calle2,
                 "sitio_web"    => $webPage,
                 "pais"         => $pais,
                 "latitud"      => $latitud,
                 "longitud"     => $longitud);
                
function checkIfRfcExists($value,$orgId,$orgType){
  $db_modulos   = condb_modulos();
  global $salida;
  
  if($value=='XAXX-010101-YYY'||$value=='XAX-010101-YYY'){
    return true;
  }
  else{
    log_write("DEBUG: CHECK-IF-RFC-EXISTS: Se checará si ya existe el prospecto",0);
    
    $query = "SELECT prospecto_id FROM prospectos WHERE rfc='".$value."'";
    
    $result = mysqli_query($db_modulos,$query);
    
    log_write("DEBUG: CHECK-IF-RFC-EXISTS: ".$query,0);
    
    if(!$result){
      log_write("ERROR: CHECK-IF-RFC-EXISTS: No se pudo obtener si ya existe el prospecto",0);
      $salida = "ERROR|| Ocurrió un problema en consulta de datos previos";
      return false;
    }
    else{
       if(0!=mysqli_num_rows($result)){
        log_write("OK: CHECK-IF-RFC-EXISTS: El prospecto ya existe",0);
        $salida = "ERROR||Ya existe un registro con el dato ".$value;
        return false;
      }
      else{
        log_write("OK: CHECK-IF-RFC-EXISTS: El prospecto no existe aún, se procederá a guardar",0);
        //$salida = "ERROR||No existe un registro con el dato ".$value." se puede guardar";
        return true;
      }
    }
  }
  mysqli_close($db_modulos);
}

if(""==$rfc&&1==$tipPer){//fisica
  $rfc = 'XAXX-010101-YYY';
}
if(""==$rfc&&2==$tipPer){//moral
  $rfc = 'XAX-010101-YYY';
}                

if(0==$hdnAction){ //si se manda a guardar prospecto nuevo
  log_write("DEBUG NEW-PROSPECT: Se insertará un prospecto nuevo:",0);
  
  if(!checkIfRfcExists($rfc,$orgId,$orgType)){
    //if((!array_key_exists(1, $gruposArr))&&(!array_key_exists(2, $gruposArr))){
    /*if(!hasPermission($modId,'a')){
      $assignUsers = $usuarioId;
    }*/
    $salida = "ERROR||Ya existe un registro con ese RFC";
    goto insPros;
  }
  else{
    $query = "INSERT INTO prospectos ( razon_social, ".
                                      "nombre_comercial, ".
                                      "rfc, ".
                                      "tipo_persona, ".
                                      "telefono, ".
                                      "extension, ".
                                      "tel_movil, ".
                                      "domicilio, ".
                                      "colonia, ".
                                      "cp, ".
                                      "municipio, ".
                                      "estado, ".
                                      "email, ".
                                      "origen, ".
                                      "origen_otro, ".
                                      "estatus, ".
                                      "asignadoa, ".
                                      "fecha_alta, ".
                                      "usuario_alta, ".
                                      "descripcion, ".
                                      "giro_id, ".
                                      "representante_id, ".
                                      "fecha_actualizacion, ".
                                      "admin_auth,".
                                      "facebook, ".
                                      "twitter, ".
                                      "instagram, ".
                                      "num_poder, ".
                                      "fecha_poder, ".
                                      "notario, ".
                                      "folio_mercantil) ".
                                      
             "VALUES ( '".$razonSoc."', ".
                      "'".$nomCom."', ".
                      "'".$rfc."', ".
                          $tipPer.", ".
                      "'".$tel."', ".
                      "'".$telExt."', ".
                      "'".$cell."', ".
                      "'".$dom."', ".
                      "'".$col."', ".
                          $cp.", ".
                      "'".$mun."', ".
                          $state.", ".
                      "'".$email."', ".
                          $origin.", ".
                      "'".$originOth."', ".
                         // $status.", ".
                          "1,".
                          $assignUsers.", ".
                      "'".$fecha."', ".
                          $usuarioId.", ".
                      "'".$desc."',".
                          $giroCom.", ".
                          "0, ".
                      "'".$fecha."', ".
                          "0, ".
                      "'".$facebook."', ".
                      "'".$twitter."', ".
                      "'".$instagram."', ".
                      "'".$numPod."', ".
                      "'".$fecPod."', ".
                      "'".$notario."', ".
                      "'".$folio."')";
                      
    $result = mysqli_query($db_modulos,$query); 
    
    log_write("DEBUG NEW-PROSPECT: ".$query,0);
    
    if(!$result){
      log_write("ERROR: NEW-PROSPECT: No se pudo insertar en la DB",0);
      $salida = "ERROR|| Ocurrió un problema en la inserción a la base de datos";
    }
    else{
      log_write("OK: NEW-PROSPECT: Los datos generales del prospecto se insertaron con éxito",0);
      $newProsId = mysqli_insert_id($db_modulos);
        // ***************************************************************************
        // **************** email de notificacion de alta de prospcto ****************
        // ***************************************************************************
        $mailData = array(
          'dest' => "",
          'cc' => "",
          'cco' => "",
          'asunto' => "Alta de Prospecto CRM",
          'msg' => "",
          'adjunto1' => "",
          'adjunto2' => "",
          'adjunto3' => "",
          'adjunto4' => "",
          'adjunto5' => "",
          'adjunto6' => "",
          'adjunto7' => "",
          'adjunto8' => "",
          'adjunto9' => "",
          'adjunto10' => "",
          'adjunto11' => "",
          'adjunto12' => "",
          'adjunto13' => "",
          'adjunto14' => "",
          'adjunto15' => "",
          'estatus' => "NO ENVIADO",
          'fecha_envio' => "0000-00-00 00:00:00",
          'nIDCorreo' => 1,
          'folio' => "0",
          'firma' => "",
          'observaciones' => "",
          'bestado' => ""
        );
        // $mailData['msg'] = $msgMail . $msgMail4 . $msgMailEnd;
        $mailData['msg'] = "Hola, el usuario ". $_SESSION['usrNombre'] ." ha agregrado un nuevo prospecto con id: ". $newProsId .".";
        sendEmailMsgDepartment(2, 1, $mailData); //(areaId, regionId, mailData)
        // el mensaje se envia a hector, pablo, y a aldrin, son quienes estan en el area 2.
        // ***************************************************************************
        // ***************************************************************************
      log_write("OK: NEW-PROSPECT: Se van a insertar los detalles de dirección",0);
      
      if(""!=$logo){
				$docPath = "../../docs/scan/prospectdocs/".$newProsId ."/";  //ruta donde se va a guardar el archivo						
				$filename = $newProsId."_logo".".".pathinfo($logo["name"], PATHINFO_EXTENSION);							
				if(!$result = uploadfile($logo,$docPath,$filename)){
					log_write("ERROR: UPDATE-PROSPECT: No se pudo subir la imagen de logo",0);
					$salida = "ERROR|| Ocurrió un problema al cargarse la imagen para logo";
					$finalpath = "";
				}
				else{
					$finalpath = $docPath.$filename;					
					$querylogoupd = "UPDATE prospectos SET logo_ruta='".$finalpath."' WHERE prospecto_id = ".$newProsId;
					$resultlogoupd = mysqli_query($db_modulos,$querylogoupd);
					if(!$resultlogoupd){
						log_write("ERROR: UPDATE-PROSPECT: No se pudo actualizar la ruta de imagen de logo",0);
					}
					else{
						log_write("OK: UPDATE-PROSPECT: Se actualizó la ruta de imagen de logo",0);
					}
				}
			}
      //$queryidcte = "INSERT INTO tbl_nidclientes (origen_id,tipo_origen) VALUES (".$newProsId.",0)";
      $query4 = "INSERT INTO direccion_detalles (origen_id,".
                                                "tipo_origen,".
                                                "telefono2,".
                                                "telefono3,".
                                                "num_exterior,".
                                                "num_interior,".
                                                "calle1,".
                                                "calle2,".
                                                "sitio_web,".
                                                "pais,".
                                                "latitud,".
                                                "longitud) ".   
              "VALUES (".$newProsId.",".
                     "0,".
                     "'".$tel2."',".
                     "'".$tel3."',".
                     "'".$numExt."',".
                     "'".$numInt."',".
                     "'".$calle1."',".
                     "'".$calle2."',".
                     "'".$webPage."',".
                     "'".$pais."',".
                     "'".$latitud."',".
                     "'".$longitud."')";
                     
      log_write("DEBUG NEW-PROSPECT: ".$query4,0);    
                 
      $result4 = mysqli_query($db_modulos,$query4);  
      //$resultidcte = mysqli_query($db_modulos,$queryidcte);
         
      if(!$result4){
        log_write("ERROR: NEW-PROSPECT: Fallo al insertar los detalles de dirección",0);
        $salida = "ERROR|| Fallo al insertar los detalles de dirección";
        goto insPros;
      }
      /*elseif(!$resultidcte){
        log_write("ERROR: NEW-PROSPECT: Fallo al insertar el id de prospecto para el comandador",0);
        $salida = "ERROR|| Los datos has sido guardados, pero falló al insertar el id de prospecto para el comandador. Comuniquese con su administrador";
        //goto insPros;
      }*/
      else{
        log_write("DEBUG: NEW-PROSPECT: Se van a insertar los contactos",0);
        foreach($contArr as $key => $value){
          if(!insertContact($db_modulos,$value,0,$newProsId)){
            log_write("ERROR: NEW-PROSPECT: No se insertaron los datos de contacto",0);
            $salida = "ERROR|| Ocurrió un problema al insertar los datos de contacto para el este prospecto"; 
            goto insPros;
          }
          else{
            log_write("OK: NEW-PROSPECT: Se insertaron los datos de contacto",0);
            $newConId = mysqli_insert_id($db_modulos);
            writeOnJournal($usuarioId,"Ha agregado un contacto nuevo con el id [".$newConId."] al prospecto con id [".$newProsId."]");
          }  
        }
        log_write("DEBUG: NEW-PROSPECT: Se insertaron todos los datos del procpecto exitosamente",0);
        writeOnJournal($usuarioId,"Ha insertado un prospecto nuevo con el id ".$newProsId);
        $salida = "OK|| Los datos se han guardado con éxito";     
      } 
    }
  }
  insPros:
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);               
  echo $salida;          
}

//si se manda a guardar prospecto modificado
else if(1==$hdnAction){ 
  log_write("DEBUG: UPDATE-PROSPECT: Se va a actualizar los datos del prospecto con id [".$prosId."]",0);
  $query2 = "SELECT razon_social,nombre_comercial,rfc,tipo_persona,telefono,tel_movil,domicilio,".
                   "colonia,cp,municipio,estado,email,origen,origen_otro,estatus,asignadoa,descripcion,logo_ruta ".
            "FROM prospectos WHERE prospecto_id=".$prosId;
  $query3 = "SELECT rel.*, cont.* FROM rel_origen_contacto AS rel, contactos AS cont ".
            "WHERE rel.tipo_origen=0 AND rel.origen_id=".$prosId." AND rel.contacto_id=cont.contacto_id";
  $query4 = "SELECT telefono2,telefono3,num_exterior,num_interior,calle1,calle2,sitio_web,pais ".
            "FROM direccion_detalles WHERE tipo_origen=0 AND origen_id=".$prosId;
  $result2 = mysqli_query($db_modulos,$query2);
  $result4 = mysqli_query($db_modulos,$query4);
  log_write("DEBUG: UPDATE-PROSPECT: ".$query2,0);
  if(!$result2||!$result4){
    log_write("ERROR: UPDATE-PROSPECT: No se pudo obtener los datos del prospecto",0);
    $salida = "ERROR|| Ocurrió un problema en la consulta de los datos para su actualización";
  }
  else{
    log_write("DEBUG: UPDATE-PROSPECT: Se obtuvieron los datos de prospecto. Listo para su actualización",0);
    //if((!array_key_exists(1, $gruposArr))&&(!array_key_exists(2, $gruposArr))){
    if(!hasPermission($modId,'a')){
      $campos['asignadoa'] = $usuarioId;
    }
    $row = mysqli_fetch_assoc($result2);
    $row4 = mysqli_fetch_assoc($result4);
    $diff = array_diff_assoc($row,$campos);
    $docPath = "../../docs/scan/prospectdocs/".$prosId ."/";  //ruta donde se va a guardar el archivo		
    if(""!=$logo){
			$filename = $prosId."_logo".".".pathinfo($logo["name"], PATHINFO_EXTENSION);							
			if(!$result = uploadfile($logo,$docPath,$filename)){
				log_write("ERROR: UPDATE-PROSPECT: No se pudo subir la imagen de logo",0);
        $salida = "ERROR|| Ocurrió un problema al cargarse la imagen para logo";
        $finalpath = "";
			}
			else{
        unlink($campos['logo_ruta']);
        $finalpath = ", logo_ruta = '".$docPath.$filename."' ";
			}
		}
    else{
      $finalpath = "";
    }
    if(1==$remLogo){
      $finalpath = ", logo_ruta = '' ";
      unlink($row['logo_ruta']);
    }
    if(hasPermission($modId,'a')){                             
      $queryStaPic =  "estatus = ".$status.", ";
    }
    else{
      $queryStaPic = "";
    }
    log_write("DEBUG: UPDATE-PROSPECT: remove logo".$remLogo,0);
    $query = "UPDATE prospectos SET razon_social = '".$razonSoc."', ".
                                   "nombre_comercial = '".$nomCom."', ".
                                   "rfc = '".$rfc."', ".
                                   "tipo_persona = '".$tipPer."', ".
                                   "telefono = '".$tel."', ".
                                   "extension = '".$telExt."', ".
                                   "tel_movil = '".$cell."', ".
                                   "domicilio = '".$dom."', ".
                                   "colonia = '".$col."', ".
                                   "cp = ".$cp.", ".
                                   "municipio = '".$mun."', ".
                                   "estado = ".$state.", ".
                                   "email = '".$email."', ".
                                   "origen = ".$origin.", ".
                                   "origen_otro = '".$originOth."', ".                              
                                   //"estatus = ".$status.", ";
                                   $queryStaPic.
                                   "asignadoa = ".$assignUsers.", ".
                                   "descripcion = '".$desc."', ".
                                   "giro_id = ".$giroCom.", ".
                                   "facebook = '".$facebook."', ".
                                   "twitter = '".$twitter."', ".
                                   "instagram = '".$instagram."', ".
                                   "num_poder = '".$numPod."', ".
                                   "fecha_poder = '".$fecPod."', ".
                                   "notario = '".$notario."', ".
                                   "folio_mercantil = '".$folio."'".
                                   $finalpath.
                                   "WHERE prospecto_id = ".$prosId;
    $result = mysqli_query($db_modulos,$query); 
    log_write("DEBUG: UPDATE-PROSPECT: ".$query,0);
    if(!$result){
      log_write("ERROR: UPDATE-PROSPECT: No se actualizaron los datos del prospecto",0);
      $salida = "ERROR|| Ocurrió un problema en la actualización de los datos";
    }
    else{
      log_write("DEBUG: UPDATE-PROSPECT: Se actualizaron los datos del prospecto",0);
      foreach($diff as $key => $value){
        if($key != 'logo_ruta'){
            writeOnJournal($usuarioId, "Ha modificado el campo \"" . $key . "\" de [" . $value . "] a [" . $campos[$key] . "]" . " para prospecto id [" . $prosId . "]");
        }
      }
      if(0>=mysqli_num_rows($result4)){
        $query5 = "INSERT INTO direccion_detalles (origen_id,".
                                                "tipo_origen,".
                                                "telefono2,".
                                                "telefono3,".
                                                "num_exterior,".
                                                "num_interior,".
                                                "calle1,".
                                                "calle2,".
                                                "sitio_web,".
                                                "pais,".
                                                "latitud,".
                                                "longitud) ".   
              "VALUES (".$prosId.",".
                     "0,".
                     "'".$tel2."',".
                     "'".$tel3."',".
                     "'".$numExt."',".
                     "'".$numInt."',".
                     "'".$calle1."',".
                     "'".$calle2."',".
                     "'".$webPage."',".
                     "'".$pais."',".
                     "'".$latitud."',".
                     "'".$longitud."')";
      }
      else{
        $query5 = "UPDATE direccion_detalles SET telefono2='".$tel2."', ".
                                              "telefono3='".$tel3."', ".
                                              "num_exterior='".$numExt."', ".
                                              "num_interior='".$numInt."', ".
                                              "calle1='".$calle1."', ".
                                              "calle2='".$calle2."', ".
                                              "sitio_web='".$webPage."', ".
                                              "pais=".$pais.", ".
                                              "latitud='".$latitud."', ".
                                              "longitud='".$longitud."' ". 
                 "WHERE origen_id=".$prosId." AND tipo_origen=0";
      }
      log_write("DEBUG NEW-PROSPECT: ".$query5,0);   
      $result5 = mysqli_query($db_modulos,$query5);
      if(!$result5){
        log_write("ERROR: UPDATE-PROSPECT: No se actualizaron los detalles de dirección",0);
      }
      else{
        if(""!=$queryStaPic){
          $estatusNom = get_sellStat($status);
          $msg = "Hola.<br>".
                 $nombre." Ha cambiado directamente la fase del prospecto ".$razonSoc." a ".$estatusNom." por lo que podría haberse omitido parte del proceso de seguimiento de éste. Por favor comunícate con el gerente de ventas o director comercial para aclaración de dudas o motivos de esta acción.";
          sendPrivMsgDepartment(0,2,$ubicacion,"Cambio directo de estatus de prospecto",$msg,0,1);
          sendPrivMsgDepartment(0,11,0,"Cambio directo de estatus de prospecto",$msg,0,1);
        }
        $diff2 = array_diff_assoc($row,$campos2);
        foreach($diff as $key => $value){
          if($key != 'logo_ruta'){
            writeOnJournal($usuarioId,"Ha modificado el campo \"".$key."\" de [".$value."] a [".$campos[$key]."] para prospecto id [".$prosId."]");
          }
        }
        log_write("OK: UPDATE-PROSPECT: Se actualizaron los detalles de dirección",0);
        if(!$updtcon = updateContactsByArray($contArr,$query3,$salida,$prosId,0)){ 
          log_write("ERROR: UPDATE-PROSPECT: No se actualizaron los contactos de este prospecto",0);
          $salida = "ERROR|| Ocurrió un problema al actualizarse la lista de los contactos";
        }
        else{
          log_write("DEBUG: UPDATE-PROSPECT: Los contactos para este prospecto fueron actualizados",0);
          $salida = "OK|| Los datos se han actualizado con éxito";
        }
      }
    }
  } 
  //saveProspecta:  
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);               
  echo $salida;          
}
else{
  log_write("DEBUG: UPDATE-PROSPECT: Ocurrió un problema al determinar el tipo de guardado o no cuenta con los permisos necesarios",0);
  $salida = "ERROR|| Ocurrió un problema al determinar el tipo de guardado o no cuenta con los permisos necesarios";
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);               
  echo $salida; 
}
}//fin else sesión
?>
