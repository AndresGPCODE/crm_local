<?php
session_start();
set_time_limit(0);
include_once "../../libs/db/common.php";

if (!verifySession($_SESSION)) {
  logoutTimeout();
} else {

  header('Content-Type: text/html; charset=UTF-8');
  date_default_timezone_set('America/Mexico_City');

  $title = "Prospectos";
  $usuario = $_SESSION['usuario'];
  $gruposArr  = $_SESSION['grupos'];

  $modId = 0;
  ?>

  <!DOCTYPE html>
  <html>

  <head>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/AdminLTE2/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/AdminLTE2/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/js/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/bootstrap/summernote-0.6.6-dist/summernote.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/bootstrap/font-awesome-4.3.0/css/font-awesome.css" rel="stylesheet" type="text/css" />

    <script src="../../libs/AdminLTE2/plugins/jQuery/jQuery-2.1.3.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/bootstrap/js/bootstrap.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/dist/js/app.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../libs/js/jqueryValidate/dist/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.form.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery-ui-sliderAccess.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
    <script src="../../libs/bootstrap/summernote-0.6.6-dist/summernote.js" type="text/javascript"></script>
    <script src="../../libs/bootstrap/summernote-0.6.6-dist/lang/summernote-es-ES.js" type="text/javascript"></script>
    <script src="../../libs/js/common.js" type="text/javascript"></script>
    <script src="script.js" type="text/javascript"></script>
    <link rel="icon" type="image/png" href="../../img/favicon-16x16.png" sizes="16x16">
    <title>Coeficiente CRM - Prospectos</title>
    <style type="text/css">
      #detalle_comentario {
        height: 190px;
        overflow-y: auto
      }
    </style>
  </head>

  <body>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyArJv8_3uHXiTrD0FvohwxCdA-TmYTmB7Y" async defer></script>
    <!--Div que contiene el encabezado-->
    <div class="container" id="header"><?php include("../../libs/templates/header.php") ?></div>
    <div class="container-fluid">
      <h2 class="page-header" id="tituloPag">
        <span class="glyphicon glyphicon-list"></span> <?php echo $title ?>
      </h2>
    </div><!-- /container -->
    <div class="container">
      <div class="row">
        <div id="div_msgAlert"></div>
      </div>
    </div> <!-- /container -->
    <?php
      if (hasPermission($modId, 'w') && (hasPermission($modId, 'r') || hasPermission($modId, 'l'))) { ?>
      <div class="container">
        <div class="row">
          <div><button type="button" id="btn_prospectNew" title="Nuevo prospecto" class="btn btn-default" onClick="openNewProspect();"><span class="glyphicon glyphicon-asterisk"></span> Nuevo Prospecto</button></div>
        </div>
        <br>
      </div> <!-- /container -->
    <?php } ?>
    <div class="container">
      <div class="row">
        <div id="div_prospectTabs"></div>
      </div>
      <div class="row">
        <div id="div_prospects"></div>
        <input type="hidden" id="hdn_orgType" name="hdn_orgType" class="form-control" value=0>
        <input type="hidden" id="hdn_orgId" name="hdn_orgId" class="form-control" value=-1>
      </div>
    </div> <!-- /container -->
    <div class="container-fluid" id="footer"><?php include("../../libs/templates/footer.php") ?></div>
    <!--_Cuadro de dialogo de detalles de prospecto -->
    <div id="mod_prospectDetails" class="modal fade" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Detalles de Prospecto</h4>
          </div>
          <div class="modal-body row">
            <div class="col container-fluid" id="div_prospectDetails"></div>
            <div id="div_prospectMap" class="map"></div>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!--_Cuadro de dialogo de insertar prospecto -->
    <div id="mod_prospectInsert" class="modal fade" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Nuevo Prospecto</h4>
          </div>
          <div class="container-fluid">
            <div id="div_msgAlertProspect"></div>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" id="frm_newProspect" name="frm_newProspect" role="form" action="saveProspect.php" method="post" enctype="multipart/form-data">
              <div class="row">
                <div class="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div>
                    <label>Nombre Comercial(*):
                      <input type="text" id="txt_nomCom" name="txt_nomCom" class="form-control" placeholder="Nombre Comercial" maxlength="120" size="40" required>
                    </label>
                    <span id="spn_txt_nomCom"></span>
                  </div>
                  <div>
                    <label id="txt_nomCom_error" class="div-errorVal" />
                  </div>
                  <div>
                    <label>Razón Social:
                      <input type="text" id="txt_razonSoc" name="txt_razonSoc" class="form-control" placeholder="Razón Social" maxlength="120" size="40">
                    </label>
                    <span id="spn_txt_razonSoc"></span>
                  </div>
                  <div>
                    <label id="txt_razonSoc_error" class="div-errorVal" />
                  </div>
                  <div>
                    <label>Tipo de persona:
                      <input type="radio" name="rad_persona" id="rad_fisica" class="rad_responsive" value=1 checked> Física
                      <input type="radio" name="rad_persona" id="rad_moral" class="rad_responsive" value=2> Moral
                    </label>
                  </div>
                  <div>
                    <label>RFC:
                      <input type="text" id="txt_rfc" name="txt_rfc" class="form-control" placeholder="XXXX-000000-YYY" maxlength="16" size="35">
                    </label>
                    <span id="spn_txt_rfc"></span>
                  </div>
                  <div>
                    <label id="txt_rfc_error" class="div-errorVal" />
                  </div>
                  <div>
                    <label>Giro Comercial:<span id="spn_giroCom"></span></label>
                  </div>
                  <div>
                    <label id="sel_giroCom_error" class="div-errorVal" />
                  </div>
                  <div>
                    <label>Teléfono(*):
                      <input type="tel" id="txt_tel" name="txt_tel" class="form-control" placeholder="10 dígitos" maxlength="10" size="10" required>
                    </label>
                    <label>Extensión:
                      <input type="tel" id="txt_ext" name="txt_ext" class="form-control" placeholder="5 dígitos" maxlength="5" size="5">
                    </label>
                    <label>Teléfono2:
                      <input type="tel2" id="txt_tel2" name="txt_tel2" class="form-control" placeholder="10 dígitos" maxlength="10" size="10">
                    </label>
                    <label>Teléfono3 (WhatsApp):
                      <input type="tel3" id="txt_tel3" name="txt_tel3" class="form-control" placeholder="10 dígitos" maxlength="10" size="10">
                    </label>
                  </div>
                  <div>
                    <label id="txt_tel_error" class="div-errorVal"></label>
                    <label id="txt_tel2_error" class="div-errorVal"></label>
                    <label id="txt_tel3_error" class="div-errorVal"></label>
                  </div>
                  <div>
                    <label>Calle(*):
                      <input type="text" id="txt_dom" name="txt_dom" class="form-control" placeholder="Calle" maxlength="120" size="40" required>
                    </label>
                  </div>
                  <div>
                    <label id="txt_dom_error" class="div-errorVal" />
                  </div>
                  <div>
                    <label>Num. Ext(*):
                      <input type="text" id="txt_numExt" name="txt_numExt" class="form-control" placeholder="Ext" maxlength="6" size="6" required>
                    </label>
                    <label>Num. Int:
                      <input type="text" id="txt_numInt" name="txt_numInt" class="form-control" placeholder="Int" maxlength="4" size="4">
                    </label>
                    <input type="button" class="btn btn-primary" id="btn_localizarMapa" value="Localizar en mapa" onClick="getCodeAddress('txt_dom','txt_numExt','sel_state','sel_country','txt_col','txt_mun','hdn_proslatitud','hdn_proslongitud');">
                  </div>
                  <div>
                    <label id="txt_numExt_error" class="div-errorVal" />
                    <label id="txt_numInt_error" class="div-errorVal" />
                  </div>
                  <div>
                    <label>Colonia(*):
                      <input type="text" id="txt_col" name="txt_col" class="form-control" placeholder="Colonia" maxlength="80" size="40" required>
                    </label>
                  </div>
                  <div>
                    <label id="txt_col_error" class="div-errorVal" />
                  </div>
                  <div>
                    <label>Entre Calle(*):
                      <input type="text" id="txt_calle1" name="txt_calle1" class="form-control" placeholder="Calle 1" maxlength="120" size="40" required>
                    </label>
                    <span id="spn_txt_calle1"></span>
                  </div>
                  <div>
                    <label id="txt_calle1_error" class="div-errorVal" />
                  </div>

                  <div>
                    <label>Y Calle(*):
                      <input type="text" id="txt_calle2" name="txt_calle2" class="form-control" placeholder="Calle 2" maxlength="120" size="40" required>
                    </label>
                    <span id="spn_txt_calle2"></span>
                  </div>
                  <div>
                    <label id="txt_calle2_error" class="div-errorVal" />
                  </div>
                  <div>
                    <label>C.P.:
                      <input type="text" id="txt_cp" name="txt_cp" class="form-control" placeholder="C.P." maxlength="5" size="5">
                    </label>
                    <label>Municipio(*):
                      <input type="text" id="txt_mun" name="txt_mun" class="form-control" placeholder="Municipio" maxlength="80" size="30" required>
                    </label>
                  </div>
                  <div>
                    <label id="txt_cp_error" class="div-errorVal"></label>
                    <label id="txt_mun_error" class="div-errorVal"></label>
                  </div>
                  <div>
                    <label>Estado(*):<span id="spn_edo"></span></label>
                  </div>
                  <div>
                    <label id="sel_state_error" class="div-errorVal" />
                  </div>
                  <div>
                    <label>País(*):<span id="spn_pais"></span></label>
                  </div>
                  <div>
                    <label id="sel_pais_error" class="div-errorVal" />
                  </div>
                  <div>
                    <label>Página Web:
                      <input type="url" id="txt_webSite" name="txt_webSite" class="form-control" placeholder="http://ejemplo.com" maxlength="80" size="20">
                    </label>
                  </div>
                  <div>
                    <label id="txt_webSite_error" class="div-errorVal" />
                  </div>
                  <div>
                    <label>Logo:
                      <input type="file" name="fl_logo" id="fl_logo" />
                    </label>
                    <label><input type="checkbox" id="chk_removeLogo" name="chk_removeLogo" value="0">Remover Imagen</label>
                    <div id="div_logo"></div>
                  </div>
                </div>
                <div class="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <label>Contactos:<span id="spn_contacto"></span></label>
                  <div>
                    <input type="button" class="btn btn-primary" id="btn_insertarContacto" value="Agregar Contacto" onClick="openNewContact(0);">
                    <br><br>
                  </div>
                  <div>
                    <div class="table-responsive">
                      <table id="tbl_selectedContact0" class="table table-hover table-striped tbl_selectedContact">
                        <thead>
                          <tr>
                            <th>Nombre</th>
                            <th>Apellidos</th>
                            <th>Puesto</th>
                            <th>Teléfono</th>
                            <th>Teléfono Móvil</th>
                            <th>Correo Electŕonico</th>
                            <th>Departamento</th>
                            <th>Acciones</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div>
                    <label id="hdn_contArr_error" class="div-errorVal" />
                  </div>

                  <div>
                    <label>Origen de Prospecto(*):<span id="spn_orgn"></span></label>
                  </div>
                  <div>
                    <label id="sel_origin_error" class="div-errorVal" />
                  </div>
                  <div id="div_other_origin" name="div_other_origin">
                    <div>
                      <label>Otro origen:
                        <input type="text" id="txt_origin" name="txt_origin" class="form-control" placeholder="Especifique Origen" maxlength="255" size="40">
                      </label>
                    </div>
                    <div>
                      <label id="txt_origin_error" class="div-errorVal" />
                    </div>
                  </div>
                  <?php
                    if (hasPermission($modId, 'a')) { ?>
                    <div>
                      <label>Estatus:<span id="spn_estat"></span></label>
                    </div>
                    <div>
                      <label id="sel_status_error" class="div-errorVal" />
                    </div>
                    <div>
                      <label>Asignar a:<span id="spn_asign"></span></label>
                    </div>
                    <div>
                      <label id="sel_assignUsers_error" class="div-errorVal" />
                    </div>
                  <?php } ?>
                  <div>
                    <label>Observaciones(*):
                      <textarea id="txt_desc" name="txt_desc" class="form-control" placeholder="Agregue una breve descripción acerca del prospecto." maxlength="255" cols="40" rows="5" required></textarea>
                    </label>
                    <div>
                      <label id="txt_desc_error" class="div-errorVal" />
                    </div>
                  </div>
                  <div>
                    <label>Facebook:
                      <input type="url" id="txt_face" name="txt_face" class="form-control" placeholder="Direccion de perfil" maxlength="120" size="40">
                    </label>
                  </div>
                  <div>
                    <label id="txt_face_error" class="div-errorVal" />
                  </div>
                  <div>
                    <label>Twitter:
                      <input type="url" id="txt_twit" name="txt_twit" class="form-control" placeholder="Direccion de perfil" maxlength="120" size="40">
                    </label>
                  </div>
                  <div>
                    <label id="txt_twit_error" class="div-errorVal" />
                  </div>
                  <div>
                    <label>Instagram:
                      <input type="url" id="txt_insta" name="txt_insta" class="form-control" placeholder="Direccion de perfil" maxlength="120" size="40">
                    </label>
                  </div>
                  <div>
                    <label id="txt_insta_error" class="div-errorVal" />
                  </div>
                  <div>
                    <div id="div_prosMap" class="map"></div>
                  </div>
                </div>
              </div>
              <div id="div_prospectManagerFields">
                <hr>
                <h4><b>Datos Legales:<b></h4>
                <div>
                  <label>Número de poder:
                    <input type="text" id="txt_prosNumPod" name="txt_prosNumPod" class="form-control" placeholder="Número de poder" maxlength="40" size="40">
                  </label>
                </div>
                <div>
                  <label id="txt_prosNumPod_error" class="div-errorVal" />
                </div>
                <div>
                  <label>Notario:
                    <input type="text" id="txt_prosNotario" name="txt_prosNotario" class="form-control" placeholder="Nombre de Notario" maxlength="30" size="20">
                  </label>
                  <label>Folio Mercantil:
                    <input type="text" id="txt_prosFolio" name="txt_prosFolio" class="form-control" placeholder="Folio Mercantil" maxlength="30" size="20">
                  </label>
                </div>
                <div>
                  <label id="txt_prosNotario_error" class="div-errorVal" />
                  <label id="txt_prosFolio_error" class="div-errorVal" />
                </div>
                <div>
                  <label>Fecha de poder:
                    <input type="text" id="txt_prosFecPod" name="txt_prosFecPod" class="form-control" placeholder="Fecha de poder" maxlength="20" size="40" readonly>
                  </label>
                </div>
                <div>
                  <label id="txt_prosFecPod_error" class="div-errorVal" />
                </div>
              </div>
              <input type="hidden" id="hdn_proslatitud" name="hdn_proslatitud" class="form-control" value=-1>
              <input type="hidden" id="hdn_proslongitud" name="hdn_proslongitud" class="form-control" value=-1>
              <input type="hidden" id="hdn_action" name="hdn_action" class="form-control" value=-1>
              <input type="hidden" id="hdn_prosId" name="hdn_prosId" class="form-control" value=-1>
              <input type="hidden" id="hdn_contArr" name="hdn_contArr" class="form-control" value="">
          </div>
          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <input type="submit" class="btn btn-primary" id="cargaCamp" value="Guardar Cambios" onClick="saveProspectNew();">
          </div>
          </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <div class="container" id="managerDialog"></div>
    <!--cuadro de dialogo para representante legal-->

    <div class="container" id="documentDialog"></div>
    <!--cuadro de dialogo para gestión de documentos-->

    <div class="container" id="eventDialog"></div>
    <!--cuadro de dialogo para inserción de evento-->

    <div class="container" id="whatsDialog"></div>
    <!--cuadro de dialogo para mensajería whatsapp-->

    <div class="container" id="pricesDialog"></div>
    <!--cuadro de dialogo para gestión de cotización-->

    <div class="container" id="contractDialog"></div>
    <!--cuadro de dialogo para gestión de contratos-->

    <div class="container" id="workOrderDialog"></div>
    <!--cuadro de dialogo para gestión de ordenes de trabajo-->

    <div class="container" id="commentProspectDialog"></div>
    <!--cuadro de dialogo para gestión de comentarios-->

    <div class="container" id="contactProspectDialog"></div>
    <!--cuadro de dialogo para gestión de contactos-->

    <div class="container" id="siteDialog"></div>
    <!--cuadro de dialogo para gestión de sitios-->

    <div class="container" id="commentTaskDialog"></div>
    <!--cuadro de dialogo para gestión de comentarios-->
     <!-- Modal -->
  <div class="modal fade" id="adminAuthRazon" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Razon o motivo.</h4>
        </div>
        <div class="modal-body">
          <input type="text" name="razon" id="razon" style="width:100%" placeholder="Motivo por el cual se autorizo la cotización.">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-default" id="sendRazon">Enviar</button>
        </div>
      </div>
      
    </div>
  </div>
    <!--cuadro de dialogo para autorizacion por diorectivo-->

  </body>

  </html>

<?php
} //fin else sesión
?>