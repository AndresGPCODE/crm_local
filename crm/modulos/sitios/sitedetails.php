<?php 
session_start();
set_time_limit(0);
include_once "../../libs/db/common.php";
include_once "../../libs/db/encrypt.php";

if(!verifySession($_SESSION)){
  logoutTimeout();
}else{
  header('Content-Type: text/html; charset=UTF-8');
  date_default_timezone_set('America/Mexico_City');

  $title = "Detalles de Sitio";
  $usuario = $_SESSION['usuario'];
  $gruposArr = $_SESSION['grupos'];
  $siteId = decrypt($_GET['id']); 
  $orgType = decrypt($_GET['orgType']);
  $det = $_GET['det'];
?>

<!DOCTYPE html>
<html>
  <head>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/AdminLTE2/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
    <link href="../../libs/js/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css"/>  
    <link href="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.css" rel="stylesheet" type="text/css"/>      
    <link href="../../libs/bootstrap/summernote-0.6.6-dist/summernote.css" rel="stylesheet" type="text/css"/> 
    <link href="../../libs/bootstrap/font-awesome-4.3.0/css/font-awesome.css" rel="stylesheet" type="text/css"/>
    
    <script src="../../libs/AdminLTE2/plugins/jQuery/jQuery-2.1.3.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/bootstrap/js/bootstrap.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/dist/js/app.js" type="text/javascript"></script>   
    <script src="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>   
    <script src="../../libs/js/jqueryValidate/dist/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.form.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.uploadPreview.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery-ui-sliderAccess.js" type="text/javascript"></script>
    <script src="../../libs/bootstrap/summernote-0.6.6-dist/summernote.js" type="text/javascript"></script>
    <script src="../../libs/bootstrap/summernote-0.6.6-dist/lang/summernote-es-ES.js" type="text/javascript"></script>
    <script src="../../libs/js/common.js" type="text/javascript"></script>
    <script src="script.js" type="text/javascript"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyArJv8_3uHXiTrD0FvohwxCdA-TmYTmB7Y"></script>
        
    <link rel="icon" type="image/png" href="../../img/favicon-16x16.png" sizes="16x16">
    
    <title>Coeficiente CRM</title>
    <style type="text/css"> 
    </style>
    <script type="text/javascript">    
    </script>
  </head>
  <body>
    <!--Div que contiene el encabezado-->
    <div class="container" id="header"><?php include("../../libs/templates/header.php")?></div>
    
    <div class="container-fluid">
      <h2 class="page-header" id="tituloPag">
        <?php echo $title ?>
      </h2>
    </div><!-- /container -->
    
    <div class="container">
      <div class="row">
        <div id="div_msgAlert"></div>
      </div>
    </div> <!-- /container -->
    
    <div class="container">
      <div class="row">
        <div class="col col-xs-12 col-lg-12">
          <h4><b>Datos del Sitio:</b></h4><br>
          <div id="div_siteDetails"></div>
        </div>
        <div id="div_siteMapSet" class="map"></div>       
        <input type="hidden" name="hdn_orgId" id="hdn_orgId" value=<?php echo $siteId;?> />
        <input type="hidden" name="hdn_orgSiteType" id="hdn_orgSiteType" value=<?php echo $orgType;?> />
        <input type="hidden" name="hdn_orgType" id="hdn_orgType" value=2 />
        <input type="hidden" name="hdn_det" id="hdn_det" value=<?php echo $det;?> />
      </div>
      <hr>
      <div class="row">
        <h4><b>Eventos programados:</b></h4><br>
        <div class="col col-xs-12 col-lg-12">
          <div id="div_siteEvents"></div>
        </div>
      </div>
      <hr>
      <hr>
      <div class="row">
        <h4><b>Enlaces:</b></h4><br>
        <div class="col col-xs-12 col-lg-12">
          <div id="div_siteConnections"></div>
        </div>
      </div>
    </div> <!-- /container -->
    
    <div class="container-fluid" id="footer"><?php include("../../libs/templates/footer.php")?></div> 
        
    <div class="container" id="eventDialog"></div><!--cuadro de dialogo para inserción de evento--> 
    
    <div class="container" id="siteDialog"></div><!--cuadro de dialogo para gestión de sitio-->  
        
    <div class="container" id="commentSiteDialog"></div><!--cuadro de dialogo para gestión de comentarios-->
    
    <div class="container" id="documentDialog"></div><!--cuadro de dialogo para gestión de documentos--> 
    
    <div class="container" id="siteConnDialog"></div><!--cuadro de dialogo para gestión de enlaces de sitio-->      
  </body>   
</html> 

<?php } //fin de else?>
