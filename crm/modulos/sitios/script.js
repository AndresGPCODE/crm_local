$(document).ready(function(){ 
  $("#siteDialog").load("../../libs/templates/sitedialog.php");
  $("#eventDialog").load("../../libs/templates/eventdialog.php");
  $("#siteConnDialog").load("../../libs/templates/siteconndialog.php");
  $("#ticketDialog").load("../../libs/templates/ticketdialog.php");
  //$("#pricesDialog").load("../../libs/templates/pricesdialog.php");
  $("#documentDialog").load("../../libs/templates/documentdialog.php");
  
  updateIndexActive("li_navBar_sitios");
  
  if(undefined!==$("#hdn_orgId").val()) {
    getSiteDetails();
    getSiteEvents();
    getSiteConnections();
    //getPrices($("#hdn_orgId").val(),2);
  }
  else
    getSiteRegs();  
});

function getSiteRegs(){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getSiteTable"},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo lista de sitios");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#div_sites").html(resp[1]);  
        $("#tbl_sites").DataTable({
          language: datatableespaniol,
        });
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);        
      }   
    }  
  });
}

//obtiene todos los datos detallados de un sitio
function getSiteDetails(){
  var orgId = $("#hdn_orgId").val(); 
  var orgType = $("#hdn_orgSiteType").val();  
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getSiteDetails", "orgId":orgId, "orgType":orgType, "option":"0"},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo datos del sitio");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = msg.trim().split("||");
      $("#div_siteDetails").html(resp[0]);
      initMap(resp[1],resp[2],'div_siteMapSet','hdn_siteLatitud','hdn_siteLongitud');
      //setModalResponsive('mod_siteDetails'); 
      //$("#mod_siteDetails").modal("show"); 
    }  
  });
}

//funcion que consigue las listas de eventos de cierto origen para su despliegue
function getSiteEvents(){
  var orgId = $("#hdn_orgId").val();
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getEvents", "orgId":orgId, "orgType":2},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo eventos");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){      
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#div_siteEvents").html(resp[1]);
      
        $("#tbl_Events").DataTable({
            language: datatableespaniol,
        });
      }
      else{
        $("#div_siteEvents").html(resp[1]);
      }
    }  
  });
}

//funcion que consigue las listas de enlaces de cualquier sitio para su despliegue
function getSiteConnections(){
  var orgId = $("#hdn_orgId").val();
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getSiteConnections", "orgId":orgId},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo enlaces de sitio");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      $("#div_siteConnections").html(msg);
      
      $("#tbl_siteConnections").DataTable({
          language: datatableespaniol,
      }); 
    }  
  });
}

//abre el formulario para insetar un enlace de sitio nuevo
function openNewSiteConnection(){  
  clearSiteConnectionFields();
  $("#txt_fecDesSitCon").prop("disabled",true);
  $("#chk_fecDesSitCon").prop("checked",false);
  
  $("#txt_fecInsSitCon").datetimepicker(datetimeOptions);
  $("#txt_fecDesSitCon").datetimepicker(datetimeOptions);
  
  $("#hdn_siteConnOption").val(0);
  
  contactoArray.length = 0;
  getConnectionTopology(0);
  getConnectionActive(0);
  $("#mod_siteConnectionInsert").modal("show");
}

//redirige a la página que muestra los detalles del enlace de 
function goToSiteConnectionDetails(id){
  window.location.href = "../enlaces/index.php?id="+id;
}

//función que obtiene los tickets asociados al sitio
function getSiteTickets(id,cli){
  $("#hdn_siteId").val(id);
  $("#hdn_cliId").val(cli);
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getTicketList", "orgId":id, "orgType":2},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo tickets asociados al sitio");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){    
     var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#div_siteTickets").html(resp[1]);
        
        $("#tbl_ticketList").DataTable({
          language: datatableespaniol,
        });
        setModalResponsive('mod_siteTickets'); 
        $("#mod_siteTickets").modal("show");
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);        
      }      
    }  
  });
}

//abre un cuadro de dialogo para la inserción de un ticket nuevo
function openNewTicket(){
  $("#div_msgAlertTicket").html("");
  $("#hdn_ticketInsertAction").val(0);
  $("#div_tickTopicOther").hide();
  $("#div_tickOrgOther").hide();
  $("#sel_ticketOrg_error").html("");
  $("#sel_ticketTopic_error").html("");
  $("#sel_ticketDepartment_error").html("");
  $("#sel_ticketSla_error").html("");
  $("#sel_tickAssign_error").html("");
  $("#txt_tickTitle_error").html("");
  $("#div_tickDetails_error").html("");
  $("#sel_ticketPrior_error").html("");  
  $("#txt_tickOrgOther").val("");
  $("#txt_tickTopicOther").val("");
  $("#chk_ticketClientNotify").attr('disabled',false);
  
  $("#div_tickDetails").summernote({
    lang: 'es-ES',
    height: 300, 
    minHeight: 100,
    maxHeight: 600,
    onImageUpload: function(files) {
      sendFile(files[0],$(this),'div_msgAlertTicket');
    },
    onMediaDelete : function(target) {
      removeFile(target[0],$(this),'div_msgAlertTicket');
    }  
  });
  
  $("#div_tickDetailsDiv").show(); 
  
  getTicketPriorityList(0);
  getTicketDepartmentList(0);
  getTicketSlaList(0);
  getTicketTopicList(0);
  getTicketOriginList(0);
  getTicketAssignToList(0); 
  $("#lbl_ticketReassign").hide();
  $("#chk_ticketReassign").val(0);
  $("#chk_ticketClientNotify").attr('checked',false);
  $("#chk_ticketReassign").attr('checked',false);
  $("#div_tickDetails").code("");
  $("#txt_tickTitle").val("");
  $("#mod_ticketInsert").modal("show");
}

