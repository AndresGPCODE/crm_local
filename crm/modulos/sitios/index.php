<?php 
session_start();
set_time_limit(0);
include_once "../../libs/db/common.php";
include_once "../../libs/db/encrypt.php";

if(!verifySession($_SESSION)){
  logoutTimeout();
}else{
  header('Content-Type: text/html; charset=UTF-8');
  date_default_timezone_set('America/Mexico_City');

  $title = "Sitios";
  $usuario = $_SESSION['usuario'];
  $gruposArr  = $_SESSION['grupos']; 
  
?>

<!DOCTYPE html>
<html>
  <head>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/AdminLTE2/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
    <link href="../../libs/js/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css"/>  
    <link href="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.css" rel="stylesheet" type="text/css"/>      
    <link href="../../libs/bootstrap/summernote-0.6.6-dist/summernote.css" rel="stylesheet" type="text/css"/> 
    <link href="../../libs/bootstrap/font-awesome-4.3.0/css/font-awesome.css" rel="stylesheet" type="text/css"/>
    
    <script src="../../libs/AdminLTE2/plugins/jQuery/jQuery-2.1.3.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/bootstrap/js/bootstrap.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/dist/js/app.js" type="text/javascript"></script>   
    <script src="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>   
    <script src="../../libs/js/jqueryValidate/dist/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.form.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.uploadPreview.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery-ui-sliderAccess.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
    <script src="../../libs/bootstrap/summernote-0.6.6-dist/summernote.js" type="text/javascript"></script>
    <script src="../../libs/bootstrap/summernote-0.6.6-dist/lang/summernote-es-ES.js" type="text/javascript"></script>
    <script src="../../libs/js/common.js" type="text/javascript"></script>
    <script src="script.js" type="text/javascript"></script>
    
    <link rel="icon" type="image/png" href="../../img/favicon-16x16.png" sizes="16x16">
    
    <title>Coeficiente CRM - Sitios</title>
    <style type="text/css"> 
    </style>
  </head>
  <body>
    <!--Div que contiene el encabezado-->
    <div class="container" id="header"><?php include("../../libs/templates/header.php")?></div>
    
    <div class="container-fluid">
      <h2 class="page-header" id="tituloPag">
        <span class="glyphicon glyphicon-tower"></span> <?php echo $title ?>
      </h2>
    </div><!-- /container -->
    
    <div class="container">
      <div class="row">
        <div id=div_msgAlert></div>
      </div>
    </div> <!-- /container -->
    
    <div class="container">
      <div class="row">
        <div id="div_sites"></div>
      </div>
    </div> <!-- /container -->
    
    <div class="container-fluid" id="footer"><?php include("../../libs/templates/footer.php")?></div>  
    
    <div class="container" id="eventDialog"></div><!--cuadro de dialogo para inserción de evento--> 
     
    <div class="container" id="commentSiteDialog"></div><!--cuadro de dialogo para gestión de comentarios-->
    
    <div class="container" id="siteDialog"></div><!--cuadro de dialogo para gestión de cotización-->  
    
    
    <!--_Cuadro de dialogo de tickets de sitio -->
    <div id="mod_siteTickets" class="modal fade" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Tickets de Sitio</h4>
          </div>          
          <div class="container-fluid">
            <div id=div_msgAlertSiteTickets></div>
            <input type="hidden" id="hdn_cliId" name="hdn_cliId" class="form-control" value=-1 />
            <!--<input type="hidden" id="hdn_orgId" name="hdn_orgId" class="form-control" value=-1 />-->
          </div>
          <div class="modal-body row">  
            <div class="col container-fluid">
    <?php   
      if(hasPermission(6,'w')){ ?>         
              <button type="button" id="btn_ticketNew" title="Nuevo Ticket" class="btn btn-default" onClick="openNewTicket();"><span class="glyphicon glyphicon-asterisk"></span> Agregar Ticket</button><br>
    <?php } ?>          
            </div><br>
            <div class="col container-fluid" id="div_siteTickets"></div> 
          </div>          
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal --> 
    
    <div class="container" id="ticketDialog"></div><!--cuadro de dialogo para gestión de tickets--> 
    
  </body>
</html>  

<?php } //fin de else?>
