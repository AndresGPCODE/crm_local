<?php
session_start();

set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

//include_once "../../libs/db/dbcommon.php";
include_once "../../libs/db/common.php";

$gruposArr  = $_SESSION['grupos'];
$usuarioId  = $_SESSION['usrId'];
$usuario    = $_SESSION['usuario'];
$nombre     = $_SESSION['usrNombre'];

if(!verifySession($_SESSION)){
  logoutTiemout();
}else{

$cfgLogService = "../../log/sitelog";

$db_catalogos = condb_catalogos();
$db_modulos   = condb_modulos();
$db_usuarios  = condb_usuarios();

$salida = "";

$modId = 2;

if("getSiteTable" == $_POST['action']){ 
  //$query = "SELECT * FROM sitios";  
  
  /*if((hasPermission(2,'r'))||(hasPermission(2,'l'))){
    $condCli = "AND cli.usuario_alta =".$usuarioId;
    $condPro = "AND sit.asignadoa =".$usuarioId;    
  }*/
  /*if(hasPermission(8,'r')||(hasPermission(8,'l')&&$row['usuario_alta']==$usuarioId)){
  
  } */
   if(hasPermission($modId,'c')&&hasPermission($modId,'r')){
      $query = "SELECT sit.*, cli.nombre_comercial, cli.cliente_id " .
        "FROM sitios AS sit, clientes AS cli " .
        "WHERE sit.origen_id=cli.cliente_id AND sit.tipo_origen=1 " .
        "AND sit.sitio_id>0 " .
        "UNION " .
        "SELECT sit.*, pro.nombre_comercial, pro.prospecto_id " .
        "FROM sitios AS sit, prospectos AS pro " .
        "WHERE sit.origen_id=pro.prospecto_id AND sit.tipo_origen=0 " .
        "AND sit.sitio_id>0";
   }
   elseif(hasPermission($modId,'f')&&hasPermission($modId,'r')){
     $query = "SELECT sit.*, cli.nombre_comercial, cli.cliente_id ".
               "FROM sitios AS sit, clientes AS cli ".
               "WHERE sit.origen_id=cli.cliente_id AND sit.tipo_origen=1 ".
               "AND sit.sitio_id>0 ".
               "UNION ".           
               "SELECT sit.*, pro.nombre_comercial, pro.prospecto_id ".
               "FROM sitios AS sit, prospectos AS pro ".
               "WHERE sit.origen_id=pro.prospecto_id AND sit.tipo_origen=0 ".
               "AND sit.sitio_id>0";
   }
   elseif(hasPermission($modId,'l')){     
   }
  /*$query = "SELECT sit.*, cli.nombre_comercial, cli.cliente_id ".
           "FROM sitios AS sit, clientes AS cli ".
           "WHERE sit.origen_id=cli.cliente_id AND sit.tipo_origen=1 ".
           "AND sit.sitio_id>0 ".
           "UNION ".           
           "SELECT sit.*, pro.nombre_comercial, pro.prospecto_id ".
           "FROM sitios AS sit, prospectos AS pro ".
           "WHERE sit.origen_id=pro.prospecto_id AND sit.tipo_origen=0 ".
           "AND sit.sitio_id>0";*/
  
  $result = mysqli_query($db_modulos,$query);
  if(!$result){
        $salida = "ERROR||Ocurrió un problema al consultar los datos de sitios en la DB";
  }
  else{
    if(0==mysqli_num_rows($result)){
      $salida = "OK||No existen registros disponibles. Podrá dar de alta un sitio nuevo desde la opción <b>Nuevo Sitio</b> en la página clientes o prospectos";
    }
    else{
      $salida = "OK||<div class=\"table-responsive\"><table id=\"tbl_sites\" class=\"table table-striped\">\n".
                "  <thead>\n".
                "    <tr>\n".
                "      <th class=\"hidden-xs\">Id</th>\n".
                "      <th>Nombre (sucursal)</th>\n".
                "      <th>Nombre Comercial</th>\n".
                "      <th>Cruces</th>\n".
                "      <th>Referencias</th>\n".
                "      <th>Acciones</th>\n".
                "    </tr>\n".
                "  </thead>\n".
                "  <tbody>\n"; 
      while($row = mysqli_fetch_assoc($result)){
        $salida .= "    <tr>\n".
                   "      <td class=\"hidden-xs\">".$row['sitio_id']."</td>\n".
                   "      <td>".$row['nombre']."</td>\n".
                   "      <td>".$row['nombre_comercial']."</td>\n".
                   "      <td>".$row['calle1']." y ".$row['calle2']."</td>\n".
                   "      <td>".$row['sitio_referencias']."</td>\n".
                   "      <td>".
                   "        <button type=\"button\" id=\"btn_siteDetails_".$row['sitio_id']."\" title=\"Ver Detalles\" class=\"btn btn-xs btn-primary btn-responsive\" onClick=\"goToSiteDetails('".encrypt($row['sitio_id'])."','".encrypt($row['tipo_origen'])."');\" ><span class=\"glyphicon glyphicon-list\"></span></button>";
        if(hasPermission(6,'r')||hasPermission(6,'w')){  //si tiene permisos para leer datos de tickets                  
          $salida .= "        <button type=\"button\" id=\"btn_siteTickets_".$row['sitio_id']."\" title=\"Ver Tickets\" class=\"btn btn-xs btn-info btn-responsive\" onClick=\"getSiteTickets(".$row['sitio_id'].",".$row['cliente_id'].");\" disabled='true'><i class=\"fa fa-ticket\"></i></span></button>";
        }
                 
       /* if(hasPermission($modId,'w')||hasPermission($modId,'m')||hasPermission($modId,'r')||hasPermission($modId,'l')){           
          $salida .= "        <button type=\"button\" id=\"btn_siteComments_".$row['sitio_id']."\" title=\"Ver Comentarios\" class=\"btn btn-xs btn-info btn-responsive\" onClick=\"getComments(".$row['sitio_id'].",2);\"><span class=\"glyphicon glyphicon-comment\"></span></button>";
        }    
        if(hasPermission(3,'r')||hasPermission(3,'l')){
          $salida .= "        <button type=\"button\" id=\"btn_siteEvents_".$row['sitio_id']."\" title=\"Ver Eventos\" class=\"btn btn-xs btn-warning btn-responsive\" onClick=\"getEvents(".$row['sitio_id'].",2);\"><span class=\"glyphicon glyphicon-calendar\"></span></button>";
        } 
        if(hasPermission($modId,'e')||(hasPermission($modId,'n')&&$row['usuario_alta']==$usuarioId)){
          $salida .= "        <button type=\"button\" id=\"btn_siteEdit_".$row['sitio_id']."\" title=\"Editar Registro\" class=\"btn btn-xs btn-success btn-responsive\" onClick=\"editSite(".$row['sitio_id'].");\"><span class=\"glyphicon glyphicon-edit\"></span></button>";                       
        }*/                                      

        if(hasPermission($modId,'d')){      
          $salida .= "        <button type=\"button\" id=\"btn_siteDelete_".$row['sitio_id']."\" title=\"Eliminar Cliente\" class=\"btn btn-xs btn-danger btn-responsive\" onClick=\"deleteSite(".$row['sitio_id'].");\" disabled='true'><span class=\"glyphicon glyphicon-remove\"></span></button>";
        }                                    
        $salida .= "      </td>\n".
                   "    </tr>\n";
      } 
      $salida .= "  </tbody>\n";      
      $salida .= "</table>\n";   
      $salida .= "</div>\n";             
    }    
  }
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios); 
  echo $salida;
}

//obtiene la lista de todos los enlaces asociados al sitio en cuestión
if("getSiteConnections" == $_POST['action']){

  $sitioId = isset($_POST['orgId']) ? $_POST['orgId'] : -1;
  
  $query = "SELECT * FROM enlaces WHERE sitio_id=".$sitioId;
  
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $salida = "ERROR||Ocurrió un problema al consultar los datos de enlaces para este sitio en la DB";
  }
  else{
    $salida = "";
    if(hasPermission(8,'w')||(hasPermission(8,'m')&&$row['usuario_alta']==$usuarioId)){  
      $salida .= "<div class=\"col container-fluid\">".
                 "  <button type=\"button\" id=\"btn_siteConnectionNew\" title=\"Nuevo Enlace\" class=\"btn btn-default\" onClick=\"openNewSiteConnection();\"><span class=\"glyphicon glyphicon-asterisk\"></span> Nuevo Enlace</button><br>".
                 "</div><br>";
    }
    if(0==mysqli_num_rows($result)){
      $salida .= "No existen registros disponibles haga click en <strong>Nuevo Enlace</strong> para agregar uno nuevo";
    }
    else{
      $salida .= "<div class=\"table-responsive\"><table id=\"tbl_siteConnections\" class=\"table table-striped table-condensed\">\n".
                 "  <thead>\n".
                 "    <tr>\n".
                 "      <th>Id</th>\n".
                 "      <th>Nodo</th>\n".
                 "      <th>Fecha de Instalación</th>\n".
                 "      <th>Comentarios</th>\n".
                 "      <th>Acciones</th>\n".
                 "    </tr>\n".
                 "  </thead>\n".
                 "  <tbody>\n";                
      while($row = mysqli_fetch_assoc($result)){
        $salida .= "    <tr>".
                   "      <td>".$row['idenlace']."</td>".
                   "      <td>".$row['nodo']."</td>".
                   "      <td>".$row['fecha_instalacion']."</td>".
                   "      <td title=\"".$row["comentarios"]."\" onClick=\"alert('".$row["comentarios"]."')\">".
                   "        <a href=\"#\">".substr($row["comentarios"],0,30)."...</a></td>\n".
                   "      <td>";
        if(hasPermission(8,'r')||(hasPermission(8,'l')&&$row['usuario_alta']==$usuarioId)){           
          $salida .= "        <button type=\"button\" id=\"btn_siteConnectionDetails_".$row['enlace_id']."\" title=\"Ver Detalles\" class=\"btn btn-xs btn-primary btn-responsive\" onClick=\"goToSiteConnectionDetails('".encrypt($row['enlace_id'])."');\"><span class=\"glyphicon glyphicon-list\"></span></button>";
        }  
        if(hasPermission(8,'e')||(hasPermission(8,'n')&&$row['usuario_alta']==$usuarioId)){  
          $salida .= "        <button type=\"button\" id=\"btn_siteConnectionEdit_".$row['enlace_id']."\" title=\"Editar\" class=\"btn btn-xs btn-success btn-responsive\" onClick=\"siteConnectionEdit('".encrypt($row['enlace_id'])."');\"><span class=\"glyphicon glyphicon-edit\"></span></button>";
        }           
        if(hasPermission($modId,'d')){      
          $salida .= "        <button type=\"button\" id=\"btn_siteConnectionDelete_".$row['enlace_id']."\" title=\"Eliminar Enlace\" class=\"btn btn-xs btn-danger btn-responsive\" onClick=\"deleteSiteConnection('".encrypt($row['enlace_id'])."');\"><span class=\"glyphicon glyphicon-remove\"></span></button>";
        } 
                   "      </td>";
        $salida .= "    </tr>";
      
      }
      $salida .= "  </tbody>\n".
                 "</table>\n</div>";
    }
  }
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios); 
  echo $salida;
}

//elimina los datos de un enlace de sitio
if("deleteSiteConnection" == $_POST['action']){
  $sitConId = isset($_POST['siteConId']) ? $_POST['siteConId'] : -1;
  
  log_write("DEBUG: DELETE-SITECONNECTIONS: Se va a eliminar un enlace de sitio",2); 
  
  $query = "DELETE FROM enlaces WHERE enlace_id=".$sitConId;
  
  $result = mysqli_query($db_modulos,$query);
  
  log_write("ERROR: DELETE-SITECONNECTIONS: ".$query,2); 
  
  if(!$result){
    log_write("ERROR: DELETE-SITECONNECTIONS: No se pudo borrar los datos del enlace de sitio",2); 
    $salida = "ERROR||Ocurrió un error al eliminarse los datos del enlace de sitio";
  }
  else{
    log_write("OK: DELETE-SITECONNECTIONS: Se eliminaron los datos del enlace de sitio",2); 
    $salida = "OK||Se han eliminado los datos del enlace";
  }
  
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios); 
  echo $salida;
}

}
?>
