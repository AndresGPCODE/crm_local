$(document).ready(function(){ 
  $("#serviceDialog").load("../../libs/templates/servicedialog.php");
  updateIndexActive("li_navBar_soporte");
  getServicesRegs();    
});

function getServicesRegs(){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getServiceTable"},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo lista de servicios");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#div_services").html(resp[1]);  
        $("#tbl_services").DataTable({
          language: datatableespaniol,
        });
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);        
      }   
    }  
  });
}
