<?php
session_start();
set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

//include_once "../../libs/db/dbcommon.php";
include_once "../../libs/db/common.php";
include_once "../../libs/db/log.php";

if(!verifySession($_SESSION)){
  logoutTimeout();
}
else{
$gruposArr  = $_SESSION['grupos'];
$usuarioId  = $_SESSION['usrId'];
$usuario    = $_SESSION['usuario'];
$nombre     = $_SESSION['usrNombre'];

$db_modulos = condb_modulos();
$db_usuarios  = condb_usuarios();

if("getUserData" == $_POST["action"]){

  $id = isset($_POST['id']) ? decrypt($_POST['id']) : 0;
  
  $picFile = glob("../../img/profilepics/$id.*");
  
  $picExt = pathinfo("../../img/profilepics/".$picFile[0], PATHINFO_EXTENSION);
  
  if(!file_exists("../../img/profilepics/$id.".$picExt)){
    $picUrl = "../../img/profilepics/default.jpg";
  }
  else{
    $picUrl = "../../img/profilepics/$id.".$picExt;    
  }  
 
  $query = "SELECT * FROM usuarios WHERE usuario_id=".$id;
  
  $result = mysqli_query($db_usuarios,$query);
  
  if(!$result){
    $salida = "ERROR: No se pudo obtener los datos de este usuario";
  }
  else{
    $row = mysqli_fetch_assoc($result);
    
    $salida = "<div class=\"col col-xs-2 col-md-2 sidebar-profile1\" align=\"center\" id=\"div_profilePic\"><br><img class=\"img-responsive\" src=\"".$picUrl."\" alt=\"".$row['username']."\" width=\"85\">";
    
    if(array_key_exists(1,$gruposArr)||($id==$usuarioId)){
      $salida .=     "<button type=\"button\" class=\"btn btn-xs btn-default\" style=\"display: none; top:0;\" onClick=\"openEditProfilePic();\">".
                    "<span class=\"glyphicon glyphicon-edit\">Editar</span>".
                  "</button>";
    }            
    $salida .= "</div>".//barra azul de foto de perfil
              "<div class=\"col col-xs-8 col-md-10\" align=\"left\">". //datos del usuario
              "  <h2>".$row['username']."</h2>&nbsp;";
              
    $salida .= "<button type=\"button\" id=\"btn_sendPrivMsg\" title=\"Enviar Mensaje\" class=\"btn btn-sm btn-info\" onClick=\"openPrivateMsg()\"><span class=\"glyphicon glyphicon-envelope\"> Enviar msg</span></button>";                         
    $salida .= "  <h4>".$row['nombre']." ".$row['apellidos']."</h4>".//más datos de usuario
               "  <b>Puesto:</b>&nbsp;".$row['puesto']."<br>".
               "  <b>Departamento:</b>&nbsp;".$row['departamento']."<br>".
               "  <b>Extensión:</b>";
    if(($usuarioId==$id)||array_key_exists(1,$gruposArr)){
      $salida .= "  <button type=\"button\" id=\"btn_editExtension\" title=\"Editar\" class=\"btn btn-xs btn-default\" onClick=\"openEditPerInfo('extension',0)\"><span class=\"glyphicon glyphicon-edit\"></span></button>".
                 "<input type=\"hidden\" id=\"hdn_extension\" value=\"".$row['extension']."\"/>";
    }
    $salida .= "&nbsp;<span id=\"spn_extension\">".$row['extension']."</span><br>".
               "  <b>Tel. Móvil:</b>";
    if(($usuarioId==$id)||array_key_exists(1,$gruposArr)){
      $salida .= "  <button type=\"button\" id=\"btn_editTelMovil\" title=\"Editar\" class=\"btn btn-xs btn-default\" onClick=\"openEditPerInfo('tel_movil',0)\"><span class=\"glyphicon glyphicon-edit\"></span></button>".
                 "<input type=\"hidden\" id=\"hdn_tel_movil\" value=\"".$row['tel_movil']."\"/>";
    }
    $salida .= "&nbsp;<span id=\"spn_tel_movil\">".$row['tel_movil']."</span><br>".
               "  <b>E-mail:</b>&nbsp;".$row['email']."<br>".
               "  <b>Acerca de mí:</b>";
    if(($usuarioId==$id)||array_key_exists(1,$gruposArr)){
      $salida .= "  <button type=\"button\" id=\"btn_editSobreMi\" title=\"Editar\" class=\"btn btn-xs btn-default\" onClick=\"openEditPerInfo('sobre_mi',1)\"><span class=\"glyphicon glyphicon-edit\"></span></button>".
                 "<input type=\"hidden\" id=\"hdn_sobre_mi\" value=\"".$row['sobre_mi']."\"/>";
    }
    $salida .= "&nbsp;<i><span id=\"spn_sobre_mi\">".$row['sobre_mi']."</span></i><br>";
    if(($usuarioId==$id)||array_key_exists(1,$gruposArr)){
      $salida .= "  <b>Contraseña:</b>&nbsp;<i>".str_repeat("*",strlen(decrypt($row['contrasena'])))."</i>".
                 "  <button type=\"button\" id=\"btn_editPass\" title=\"Cambiar Contraseña\" class=\"btn btn-xs btn-default\" onClick=\"openEditPass()\"><span class=\"glyphicon glyphicon-edit\"></span></button>&nbsp;";
               }
    $salida .= "<br></span></div>";
  }
  mysqli_close($db_usuarios);
  mysqli_close($db_modulos);
  echo $salida;
}

if("getUserActivity" == $_POST["action"]){

  $id = isset($_POST['id']) ? decrypt($_POST['id']) : 0;
  $option = isset($_POST['option']) ? $_POST['option'] : -1;
  
  if(0==$option){
    $query = "SELECT * FROM bitacora WHERE usuario_id=".$id." ORDER BY fecha_hora DESC LIMIT 20";
  }
  if(1==$option){
    $query = "SELECT * FROM bitacora WHERE usuario_id=".$id;
  }
    
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $salida = "ERROR: No se pudo obtener la actividad de este usuario";
  }
  else{
    if(0==$option){
      if(($usuarioId==$id)||array_key_exists(1,$gruposArr)){
        $salida = "<button type=\"button\" id=\"btn_allActivity\" title=\"Ver toda la actividad\" class=\"btn btn-sm btn-primary btn-responsive\" onClick=\"getAllActivity('".encrypt($id)."');\">Ver toda la actividad</button><br>";
      }
      while($row = mysqli_fetch_assoc($result)){
        $salida .= "<span class=\"actTime\"><i><b>".$row['fecha_hora']."</i></b>&nbsp;&nbsp;&nbsp;</span>".$row['descripcion']."<br>";
      }       
    }
           
    if(1==$option){
      $salida = "<div class=\"table-responsive\"><table id=\"tbl_userActivity\" class=\"table table-hover table-striped wrap\">\n".
                "  <thead>\n".
                "    <tr>\n".
                "      <th>Fecha y hora</th>\n".
                "      <th>Actividad</th>\n".
                "    </tr>\n".
                "  </thead>\n".
                "  <tbody>\n";
      while($row = mysqli_fetch_assoc($result)){
        $salida .= "    <tr>\n".
                   "      <td><b>".$row['fecha_hora']."</b></td>\n".
                   "      <td>".$row['descripcion']."</td>\n".
                   "    </tr>\n";
      }
      $salida .= "  </tbody>\n".
                 "</table></div>\n";
    }    
  }
  mysqli_close($db_usuarios);
  mysqli_close($db_modulos);
  echo $salida;
}

if("getUserEvents" == $_POST["action"]){

  $id = isset($_POST['id']) ? decrypt($_POST['id']) : 0;
  
  $fechaHora = date("Y-m-d H:i:s");
  
  //$query = "SELECT cal.*, tipeve.* FROM ".$cfgTableNameMod.".calendario AS cal, ".$cfgTableNameCat.".cat_tipo_eventos AS tipeve WHERE cal.tipo_evento_id=tipeve.tipo_evento_id ORDER BY cal.fecha_inicio DESC LIMIT 10";
  $query = "SELECT cal.*, tipeve.* FROM ".$cfgTableNameMod.".calendario AS cal, ".$cfgTableNameCat.".cat_tipo_eventos AS tipeve WHERE usuario_id=".$id." AND cal.tipo_evento_id=tipeve.tipo_evento_id AND cal.fecha_inicio >='".$fechaHora."' ORDER BY cal.fecha_inicio ASC LIMIT 10";
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $salida = "ERROR: No se pudo obtener los eventos para este usuario";
  }
  else{
    if(0==mysqli_num_rows($result)){
      $salida = "Este usuario no tiene eventos próximos programados";
    }
    else{
      $salida = "<div class=\"table-responsive table-striped small\"\n>".
                "  <table id=\"tbl_profileEvents\" class=\"table table-striped\">\n".
                "    <thead>".
                "     <tr>".
                "       <th>Fecha/Hora</th>".
                "       <th>Tipo de evento</th>".
                "       <th>Título</th>".
                "       <th>Descripción</th>".                
                "     </tr>".
                "    </thead>".
                "    <tbody>";
      while($row = mysqli_fetch_assoc($result)){
        $salida .= "      <tr>\n".
                   "        <td class=\"actTime\">".$row['fecha_inicio']."</td>\n".
                   "        <td style=\"color:#".$row['color']."\">".$row['tipo_evento']."</td>\n".
                   "        <td style=\"color:#333\">".$row['titulo']."</td>\n".
                   "        <td style=\"color:#333\" title=\"".$row['descripcion']."\" onclick=\"alert('".$row['descripcion']."')\">".
                   "          <a href=\"#\">".substr($row['descripcion'],0,30)."...</a></td>\n".
                   "      </tr>\n";
        //$salida .= "<span class=\"actTime\"><i><b>".$row['fecha_inicio']."</i></b>&nbsp;&nbsp;&nbsp;</span>".$row['descripcion']."&nbsp;&nbsp;&nbsp;<span style='color:#".$row['color']."'><b>".$row['tipo_evento']."</b></span><br>";
      }
      $salida .= "    </tbody>\n".
                 "  </table>\n".
                 "</div>\n"; 
    } 
  } 
  mysqli_close($db_usuarios);
  mysqli_close($db_modulos);
  echo $salida;
}

if("sendPrivMsg" == $_POST["action"]){  
  $asunto = isset($_POST['msgSubject']) ? $_POST['msgSubject'] : "Sin Asunto";
  $mensaje = isset($_POST['msgContent']) ? $_POST['msgContent'] : "";
  $destId  = isset($_POST['id']) ? decrypt($_POST['id']) : -1;
  
  $fechaHora = date("Y-m-d H:i:s");
  
  if(100000<strlen($mensaje)){
    $salida = "ERROR||El tamaño del mensaje es demasiado grande. Quizá deba remover algunas imágenes";
    goto sendPrivMsga;
  }
  if(-1==$destId){
    $salida = "ERROR||Ocurrió un problema al obtener el id del destinatario";
    goto sendPrivMsga;
  }
  
  $query = "INSERT INTO mensajes (remitente_id,destinatario_id,fecha_hora,asunto,mensaje,leido,tipo_mensaje) VALUES(".$usuarioId.",".$destId.",'".$fechaHora."','".$asunto."','".$mensaje."',0,1)";
  
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $salida = "ERROR||No se pudo enviar el mensaje";
  }
  else{
    $salida = "OK||El mensaje ha sido enviado con éxito";
  }
  
  sendPrivMsga:
  mysqli_close($db_usuarios);
  mysqli_close($db_modulos);
  echo $salida;
}

if("saveNewPass" == $_POST['action']){
  $oldPass = isset($_POST['usrOldPass']) ? $_POST['usrOldPass'] : "";
  $newPass = isset($_POST['usrNewPass']) ? $_POST['usrNewPass'] : "";
  $newPass2 = isset($_POST['usrNewPass2']) ? $_POST['usrNewPass2'] : "";
  $usrId = isset($_POST['usrId'])?decrypt($_POST['usrId']):-1;
  
  if(""==$oldPass||""==$newPass||""==$newPass2)
    $salida = "ERROR||Por favor, verifique su información. Uno o más campos requeridos están vacíos";
  elseif($newPass!=$newPass2)
    $salida = "ERROR||Las contraseñas anteriores no coinciden";
  else{
    if(array_key_exists(1,$gruposArr))//verifica y valida si es administrador. Si lo es podrá cambiar la contraseña de cualquier usuario con su pass de dios
      $query1 = "SELECT usr.usuario_id ".
                "FROM usuarios AS usr, ".
                "rel_usuario_grupo AS relgrp ".
                "WHERE usr.usuario_id=".$usuarioId." ".
                "AND usr.contrasena='".encrypt($oldPass)."' ".
                "AND relgrp.usuario_id=usr.usuario_id ".
                "AND relgrp.grupo_id=1";
    else //usuario normal, cambia su contraseña con su contraseña anterior
      $query1 = "SELECT usuario_id FROM usuarios WHERE usuario_id=".$usuarioId." AND contrasena='".encrypt($oldPass)."'";
    
    $result1 = mysqli_query($db_usuarios,$query1);
    
    if(!$result1||0>=mysqli_num_rows($result1))
      $salida = "ERROR||Los datos de sesión son incorrectos, verifique su contraseña anterior ".$query1; 
    else{
      $query2 = "UPDATE usuarios SET contrasena='".encrypt($newPass)."' WHERE usuario_id=".$usrId;
      $result2 = mysqli_query($db_usuarios,$query2);
     
      if(!$result2)
        $salida = "ERROR||Ocurrió un error al realizar el cambio de contraseña ".$query2;
      else{
        //writeOnJournal($usuarioId,"Ha cambiado su contraseña");
        $salida = "OK||La contraseña ha sido cambiada correctamente. Terminando sesión";
      }
    }
  }
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}

if("savePersonalInfo" == $_POST['action']){
  $info = isset($_POST['fieldValue'])?$_POST['fieldValue']:"";
  $fieldName = isset($_POST['fieldName'])?$_POST['fieldName']:"";
  
  $query = "UPDATE usuarios SET ".$fieldName."='".$info."' WHERE usuario_id=".$usuarioId;
 
  $result = mysqli_query($db_usuarios,$query);
  
  if(!$result)
    $salida = "ERROR||No se pudo cambiar la información personal ";
  else 
    $salida = "OK||Se ha guardado la información personal";
  
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}

if("saveProfilePic" == $_POST['action']){
  if(isset($_FILES["fl_usrNewProfilePic"])) {
    log_write("DEBUG: UPLOAD-NEW-DOCUMENT: Si existe un archivo ".$_FILES["fl_usrNewProfilePic"]["name"],4);
    log_write("DEBUG: UPLOAD-NEW-DOCUMENT: Tamaño de archivo ".$_FILES["fl_usrNewProfilePic"]["size"],4);
    log_write("DEBUG: UPLOAD-NEW-DOCUMENT: Tipo de archivo ".$_FILES["fl_usrNewProfilePic"]["type"],4);
    
    $docPath = "../../img/profilepics/";
    
    $type = array('image/png',
                  'image/jpeg',
                  'image/gif',
                  'image/bmp',
                  'image/tiff'
                  );
    
    if(in_array($_FILES["fl_usrNewProfilePic"]["type"],$type)) {
      if ($_FILES["fl_usrNewProfilePic"]["error"] > 0) {
        $salida = "ERROR|| Error al leer el archivo ".$_FILES["file"]["error"];
      }
      else if(0 == $_FILES["fl_usrNewProfilePic"]["size"]) {
        $salida = "ERROR|| El archivo está vacío.";
      }
      else if($_FILES["fl_usrNewProfilePic"]["size"] > $maxDocSize) {
        $salida = "ERROR|| El tamaño del archivo excede el máximo permitido: ".$maxDocSize;
      } 
      else{ 
        //renombra el archivo y lo carga        
        $filename = $usuarioId.".".pathinfo($_FILES["fl_usrNewProfilePic"]["name"], PATHINFO_EXTENSION);
        
        $filePat = $docPath.$usuarioId.".*"; //si ya existe otra imagen la borra
        array_map("unlink", glob($filePat));

        $result = move_uploaded_file($_FILES["fl_usrNewProfilePic"]["tmp_name"],$docPath.$filename); 
        
        if(!$result){
          $Salida = "ERROR|| No pudo cargarse el archivo";
          //log_write("ERROR: UPLOAD-NEW-DOCUMENT: No se pudo mover el archivo ".$_FILES["fl_usrNewProfilePic"]["name"]." a su carpeta ".$docPath.$filename,4);
        }
        else{
          chmod($docPath.$filename,0775);
          $salida = "OK|| La foto de perfil se ha cambiado con éxito. Puede tomar alrededor de media hora para que el cambio surta efecto";
        }
      }
    }
    else{
      $salida = "ERROR|| Tipo de archivo no soportado. Recuerde cargar un archivo de formato de imagen. Tipo de archivo enviado ".$_FILES["fl_usrNewProfilePic"]["type"];
    }
  }
  else{
    $salida = "ERROR|| El archivo no pudo subirse.";
  }
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}

if("markAsRead" == $_POST['action']){//aki
  $msgId = isset($_POST['msgId'])?json_decode($_POST['msgId']):"";
  $cont = 1;
  $query2 = "UPDATE mensajes SET leido=1 ";
  
  foreach($msgId as $value){
    if(1==$cont)
      $query2 .= "WHERE "; 
    else
      $query2 .= " OR ";
      
    $query2 .= "mensaje_id=".decrypt($value);
    $cont++;
  }
  
  $result2 = mysqli_query($db_modulos,$query2);
  
  if(!$result2){
    $salida = "ERROR||Ocurrió un problema al intentar marcar los mensajes como leídos";
  }
  else{
    $salida = "OK||El mensaje se marcó como leído";
  }
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}

if("deleteMsg" == $_POST['action']){
  $msgId = isset($_POST['msgId'])?json_decode($_POST['msgId']):"";
  $cont = 1;
  $query = "DELETE FROM mensajes ";"WHERE mensaje_id=".$msgId;
  
  foreach($msgId as $value){
    if(1==$cont)
      $query .= "WHERE "; 
    else
      $query .= " OR ";
      
    $query .= "mensaje_id=".decrypt($value);
    $cont++;
  }
  
  $result = mysqli_query($db_modulos,$query);
  if(!$result){
    $salida = "ERROR||ocurrió un problema al intentar eliminar los mensajes";
  }
  else{
    $salida = "OK||Se han eliminado los mensajes con éxito";
  }
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}

}

?>
