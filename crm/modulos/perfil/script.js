$(document).ready(function(){  
  getLastActivity($("#hdn_usrId").val());
  getUserEvents($("#hdn_usrId").val());
  getUserData($("#hdn_usrId").val());
  
  updateIndexActive("li_navBar_perfil");
  
  $("#div_usrMsgContent").summernote({
    lang: 'es-ES',
    height: 300, 
    minHeight: 100,
    maxHeight: 600 
  }); 
  
  //preview de imagenes en el formulario de foto de perfil
  $.uploadPreview({
    input_field: "#fl_usrNewProfilePic",   // Default: .image-upload
    preview_box: "#div_usrNewProfilePicPrev",  // Default: .image-preview
    label_field: "#lbl_usrNewProfilePicLabel",    // Default: .image-label
    label_default: "Elija un archivo (tamaño máximo 5Mb)",
    label_selected: "Cambiar archivo",
    no_label: false                 // Default: false
  }); 
  
});

function getLastActivity(id){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getUserActivity", "id":id, "option":0},
    success: function(msg){
      $("#div_activity").html(msg); 
    }  
  });
}

function getAllActivity(id){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getUserActivity", "id":id, "option":1},
    success: function(msg){
      $("#div_userActivity").html(msg);
      $("#tbl_userActivity").DataTable({
        language: datatableespaniol,
      });
      setModalResponsive('mod_userActivity');
      $("#mod_userActivity").modal("show"); 
    }  
  });
}

function getUserEvents(id){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getUserEvents", "id":id},
    success: function(msg){
      $("#div_eventosUsr").html(msg); 
    }  
  });
}

function getUserData(id){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getUserData", "id":id},
    success: function(msg){
      $("#div_mainData").html(msg); 
      $("#div_profilePic").mouseenter(function(){
        $(this).find(":button").show();
      });
      $("#div_profilePic").mouseleave(function(){
        $(this).find(":button").hide();
      });
    }  
  });
}

function openPrivateMsg(){
  $("#div_msgAlertUsrMsg").html("");
  $("#div_usrMsgContent").code("");
  $("#txt_usrMsgSubject").val("");
  $("#mod_userNewMsg").modal("show");
}

function sendPrivateMsg(){
  if(false==validatePrivateMsgData()){   
    alert("Por favor verifique los datos");
    return;
  }
  else{
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: {"action": "sendPrivMsg", "id":$("#hdn_destId").val(), "msgSubject":$("#txt_usrMsgSubject").val() ,"msgContent":$("#div_usrMsgContent").code().trim()},
      beforeSend: function(){
        setLoadDialog(0,"Enviando mensaje privado");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){
        var resp = msg.split('||');
        var errCod = resp[0].trim();
        if("OK"== errCod){
          printSuccessMsg("div_msgAlert",resp[1]); 
          $("#mod_userNewMsg").modal("hide");
        }
        else{
          printErrorMsg("div_msgAlertUsrMsg",resp[1]);                                                  
        } 
      }  
    });
  }
}

//valida los datos del mensaje privado antes de ser enviado al destinatario
function validatePrivateMsgData(){	 
  if(0>=$("#div_usrMsgContent").code().length){
    printErrorMsg("div_msgAlertUsrMsg","Por favor incluya un mensaje");
    return false; 
  }  
  if(0>=$("#txt_usrMsgSubject").val().length){
    printErrorMsg("div_msgAlertUsrMsg","Por favor incluya un asunto");
    return false; 
  }
}

//obtiene los mensajes nuevos y leídos del usuario
function getUserMsgs(){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getUserMsgs"},
    success: function(msg){
      var resp = msg.trim().split('||');
      var errCod = resp[0];
      if("OK"== errCod){
        $("#div_usrMsgs").html(resp[1]);
        $("#tbl_inboxMsgs").DataTable({
          language: datatableespaniol,
          bSort : false,
          stateSave: true
        });
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);                                                  
      } 
    } 
  });
}

function openEditPerInfo(fieldName,tipoCampo){
  var currInfo = $("#hdn_"+fieldName).val();
  if(0==tipoCampo)
    $("#spn_"+fieldName).html("<input type=\"text\" id=\"txt_"+fieldName+"\" maxlength=\"60\" size=\"20\" value=\""+currInfo+"\"/>");
  if(1==tipoCampo)
    $("#spn_"+fieldName).html("<textarea id=\"txt_"+fieldName+"\" maxlength=\"255\" cols=\"40\" rows=\"5\">"+currInfo+"</textarea>");
 
  $("#txt_"+fieldName).focus();
  $("#txt_"+fieldName).select();
  $("#txt_"+fieldName).keyup(function (e) {
    var key = e.which;
    if(key == 13)  // the enter key code
    {
      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: {"action": "savePersonalInfo", "fieldValue":$("#txt_"+fieldName).val(), "fieldName":fieldName},
        success: function(msg){
          var resp = msg.split('||');
          var errCod = resp[0].trim();
          if("OK"== errCod){
            $("#spn_"+fieldName).html($("#txt_"+fieldName).val());
          }
          else{
            printErrorMsg("div_msgAlert",resp[1]);                                                  
          }
        }  
      });
    }
    if(key == 27){
      $("#spn_"+fieldName).html($("#hdn_"+fieldName).val());
    } 
  });
}

function openEditPass(){
  $("#txt_usrNewPass").val("");
  $("#txt_usrNewPass2").val("");
  $("#txt_usrOldPass").val("");
  $("#mod_userChangePass").modal("show");
}

function saveNewPass(){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "saveNewPass","usrNewPass":$("#txt_usrNewPass").val(),"usrNewPass2":$("#txt_usrNewPass2").val(),"usrOldPass":$("#txt_usrOldPass").val(),"usrId":$("#hdn_usrId").val()},
    beforeSend: function(){
      setLoadDialog(0,"Guardando información");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = msg.split('||');
      var errCod = resp[0].trim();
      if("OK"== errCod){
        $("#mod_userChangePass").modal("hide");
        printSuccessMsg("div_msgAlert",resp[1]); 
        window.location.replace("../login/logout.php?sub=pwdChg");
      }
      else{
        printErrorMsg("div_msgAlertUsrChgPass",resp[1]);                                                  
      } 
    }  
  });
}

function openEditProfilePic(){
  $("#fl_usrNewProfilePic").replaceWith($("#fl_usrNewProfilePic").val("").clone(true));
  $("#lbl_usrNewProfilePicLabel").html("Elija un archivo (tamaño máximo 5Mb)");
  $("#div_usrNewProfilePicPrev").css("background-image","none");
  $("#mod_userUploadImg").modal("show");
}

//guarda el documento y sus datos
function saveProfilePic(){ 
  var options = {
    contentType: 'multipart/form-data',
    url: "ajax.php",
    data: {"action": "saveProfilePic"},
    beforeSend: function(){
      setLoadDialog(0,"Cambiando Foto de Perfil");
    },
    complete: function(response){
      setLoadDialog(1,"");
      var resp = (response.responseText).trim().split('||');
      if("OK" == resp[0]){
        //printSuccessMsg("div_msgAlert",resp[1]);
        $("#mod_userUploadImg").modal("hide");
        alert(resp[1]);
        window.location.reload();
      }
      else{  
        printErrorMsg("div_msgAlertUsrChgProfilePic",resp[1]);                
      }          
    },
    error: function(){
      printErrorMsg("div_msgAlertDocumentNew","Ocurrió un error al enviarse los datos de documento para su guardado");  
    }      
  };   
  $("#frm_newProfilePic").ajaxForm(options);
}

/*function markAsReadMsg(msg){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "markAsRead", "msgId":msg},
    beforeSend: function(){
      setLoadDialog(0,"Marcando mensaje como leído");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){    
      var resp = (msg).trim().split("||");
      if("OK" == resp[0]){
        //$("#div_msgBody").html(resp[1]);
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);                   
      }       
    }  
  });
}*/

function markAsReadMsg(){
  arrMsgToMark = new Array();
  
  $("input[name=chk_inboxMsgs]:checked").each(function(){
    arrMsgToMark.push($(this).val());
  });
  
  var arrMsgToMarkJson = JSON.stringify(arrMsgToMark);
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "markAsRead", "msgId":arrMsgToMarkJson},
    beforeSend: function(){
      setLoadDialog(0,"Marcando mensaje como leído");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){    
      var resp = (msg).trim().split("||");
      if("OK" == resp[0]){
        printSuccessMsg("div_msgAlert",resp[1]);
        //getUserMsgs();
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);                   
      }       
    }  
  });
}

function deleteMsgs(){
  arrMsgToDel = new Array(); 
  $("input[name=chk_inboxMsgs]:checked").each(function(){
    arrMsgToDel.push($(this).val());
  });
  var arrMsgToDelJson = JSON.stringify(arrMsgToDel);

  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "deleteMsg", "msgId":arrMsgToDelJson},
    beforeSend: function(){
      setLoadDialog(0,"Eliminado mensaje");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){    
      var resp = (msg).trim().split("||");
      if("OK" == resp[0]){
        printSuccessMsg("div_msgAlert",resp[1]);
        //getUserMsgs();
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);                   
      }       
    }  
  });
}
