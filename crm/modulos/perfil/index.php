<?php
session_start();
set_time_limit(0);
include_once "../../libs/db/common.php";

if(!verifySession($_SESSION)){
  logoutTimeout();
}

header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

$title = "Perfil";
$usuario = $_SESSION['usuario'];
$gruposArr  = $_SESSION['grupos'];
$usuarioId = $_SESSION['usrId'];
$usuarioNom = get_userRealName($usuarioId);

$usrProfileId = decrypt($_GET['id']);

?>


<!DOCTYPE html>
<html>
  <head>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/AdminLTE2/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
    <link href="../../libs/js/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css"/>  
    <link href="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.css" rel="stylesheet" type="text/css"/>      
    <link href="../../libs/bootstrap/summernote-0.6.6-dist/summernote.css" rel="stylesheet" type="text/css"/> 
    <link href="../../libs/bootstrap/font-awesome-4.3.0/css/font-awesome.css" rel="stylesheet" type="text/css"/>
    
    <script src="../../libs/AdminLTE2/plugins/jQuery/jQuery-2.1.3.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/bootstrap/js/bootstrap.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/dist/js/app.js" type="text/javascript"></script>   
    <script src="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>   
    <script src="../../libs/js/jqueryValidate/dist/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.form.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.uploadPreview.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery-ui-sliderAccess.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
    <script src="../../libs/bootstrap/summernote-0.6.6-dist/summernote.js" type="text/javascript"></script>
    <script src="../../libs/bootstrap/summernote-0.6.6-dist/lang/summernote-es-ES.js" type="text/javascript"></script>
    <script src="../../libs/js/common.js" type="text/javascript"></script>
    <script src="script.js" type="text/javascript"></script>
    
    <link rel="icon" type="image/png" href="../../img/favicon-16x16.png" sizes="16x16">
    
    <title>Coeficiente CRM - Perfil de <?php echo $usuarioNom?></title>
    <style type="text/css">
      .actTime{
        color:#757185;
      }      
    </style>
  </head>  
  
  <body>
    <!--Div que contiene el encabezado-->
    <div class="container" id="header"><?php include("../../libs/templates/header.php")?></div>
    
    <div class="container-fluid">
      <h2 class="page-header" id="tituloPag">
        <span class="glyphicon glyphicon-user"></span> <?php echo $title ?>
      </h2>
    </div><!-- /container -->
    
    <div class="container">
      <div class="row">
        <div id="div_msgAlert"></div>
      </div>
    </div> <!-- /container -->
          
    <div class="container">
      <!--1 fila-->
      <div class="row">
        <div class="col col-xs-12 col-lg-8">
          <div id="div_mainData"></div>
        </div>
        
        <div class="col col-xs-12 col-lg-4 sidebar-profile1">                             
          <h4 align="center">Sus eventos próximos
            <button type="button" title="Ir a Calendario" class="btn btn-xs btn-default" onclick="location.href = '../calendario';">
              <span class="glyphicon glyphicon-calendar"></span>
            </button> 
          </h4>
          <div id="div_eventosUsr"></div>               
        </div>
        
      </div>      
      <hr>
      <!--2 fila-->
      <div class="row">
        <?php if(array_key_exists(1,$gruposArr)||(decrypt($_GET['id'])==$usuarioId)){?>
        <div class="col col-xs-12 col-lg-12 panelCoe1">
          <h4 class="page-header" id="tituloPag">Última Actividad</h4>
          <div class="container-fluid" id="div_activity"></div>
        </div>    
        <?php }?>     
      </div>
      
      <input type="hidden" id="hdn_usrId" name="hdn_usrId" class="form-control" value=<?php echo $_GET['id'];?>>
    </div> <!-- /container -->
    
    <div class="container-fluid" id="footer"><?php include("../../libs/templates/footer.php")?></div>
       
    <!--_Cuadro de dialogo de visualización de los detalles de toda actividad del usuario-->
    <div id="mod_userActivity" class="modal fade" data-backdrop="static" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Actividad del usuario</h4>
          </div>
          <div class="modal-body">         
            <div class="col container-fluid" id="div_userActivity"></div>  
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <!--_Cuadro de dialogo de creación de un mensaje privado-->
    <div id="mod_userNewMsg" class="modal fade" data-backdrop="static" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Enviar un mensaje a <?php echo get_userRealName(decrypt($_GET['id'])) ?></h4>
          </div>
          <div class="container-fluid">
            <div id="div_msgAlertUsrMsg"></div>
          </div>
          <div class="modal-body">
            <div>Asunto: 
              <input type="text" id="txt_usrMsgSubject" name="txt_usrMsgSubject" class="form-control" placeholder="" maxlength="60" size="60" required>
            </div>   
            <br>                 
            <div id="div_usrMsgContent"></div>
          </div>
          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <input type="submit" class="btn btn-primary" id="sendPrivateMsg" value="Enviar" onClick="sendPrivateMsg();">
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <!--_Cuadro de dialogo de cambio de contraseña-->
    <div id="mod_userChangePass" class="modal fade" data-backdrop="static" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Cambiar Contraseña de <?php echo $usuarioNom ?></h4>
          </div>
          <div class="container-fluid">
            <div id=div_msgAlertUsrChgPass></div>
          </div>
          <div class="modal-body">
              <label>Confirmar Contraseña:
                <input type="password" id="txt_usrOldPass" name="txt_usrOldPass" class="form-control" placeholder="" maxlength="60" size="60" required>
              </label>
              <label>Nueva Contraseña:
                <input type="password" id="txt_usrNewPass" name="txt_usrNewPass" class="form-control" placeholder="" maxlength="60" size="60" required>
              </label>
              <label>Confirmar Contraseña Nueva:
                <input type="password" id="txt_usrNewPass2" name="txt_usrNewPass2" class="form-control" placeholder="" maxlength="60" size="60" required>
              </label>
            <br>                 
          </div>
          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <input type="submit" class="btn btn-primary" id="sendNewPass" value="Guardar" onClick="saveNewPass();">
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <!--_Cuadro de dialogo de creación de un mensaje privado-->
    <div id="mod_userUploadImg" class="modal fade" data-backdrop="static" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Subir foto de Perfil</h4>
          </div>
          <div class="container-fluid">
            <div id=div_msgAlertUsrChgProfilePic></div>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" id="frm_newProfilePic" name="frm_newProfilePic" role="form" method="post" enctype="multipart/form-data">
              <div id="div_usrNewProfilePicPrev">
                <label for="fl_usrNewProfilePic" id="lbl_usrNewProfilePicLabel">Elija un archivo (tamaño máximo 5Mb)</label>
                <input type="file" name="fl_usrNewProfilePic" id="fl_usrNewProfilePic"/>
              </div>
            <br>                 
          </div>
          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <input type="submit" class="btn btn-primary" id="snedProfilePic" value="Guardar" onClick="saveProfilePic();">
            </form>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <input type="hidden" id="hdn_destId" name="hdn_destId" class="form-control" value="<?php echo $_GET['id']?>">
  </body>
</html>  
