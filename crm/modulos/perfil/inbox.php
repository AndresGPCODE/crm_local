<?php
session_start();
set_time_limit(0);
include_once "../../libs/db/common.php";

if(!verifySession($_SESSION)){
  logoutTimeout();
}

header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

$salida = "";
$title = "Bandeja de Entrada";
$usuario = $_SESSION['usuario'];
$gruposArr  = $_SESSION['grupos'];
$usuarioId = $_SESSION['usrId'];
$usuarioNom = get_userRealName($usuarioId);

$usrProfileId = decrypt($_GET['msgType']);

$db_modulos = condb_modulos();
$db_catalogos  = condb_catalogos();

$query = "SELECT msg.*".
				 "FROM ".$cfgTableNameMod.".mensajes AS msg ".
				 "WHERE destinatario_id=".$usuarioId." ".
				 "ORDER BY fecha_hora DESC, tipo_mensaje ASC";
				 
$result = mysqli_query($db_modulos,$query);

if(!$result){
	$salida = "ERROR||Ocurrió un problema al obtener los datos de la bandeja de entrada";
}
else{
	$salida =  "<div class=\"container\">".
						 "  <button type=\"button\" class=\"btn btn-sm btn-danger\" id=\"btn_deleteMsgs\" onClick=\"deleteMsgs();\"><span class=\"glyphicon glyphicon-remove\"> Eliminar</span></button>".
						 "  <button type=\"button\" class=\"btn btn-sm btn-default\" id=\"btn_markAsReadMsgs\" onClick=\"markAsReadMsg();\"><span class=\"glyphicon glyphicon-ok\"> Marcar como Leído</span></button>\n".
						 "<hr></div>\n";
	$salida .= "<div class=\"container\">".
							"<table id=\"tbl_inboxMsgs\" class=\"table table-striped no-footer table-hover table-email\">".
							"  <thead>\n".
							"    <tr>\n".
							"      <th></th>\n".
							"      <th></th>\n".
							"      <th>Remitente</th>\n".
							"      <th>Asunto</th>\n".
							"      <th>Fecha/Hora</th>\n".
							"      <th></th>\n".
							"    </tr>\n".
							"  </thead>\n".
							"  <tbody>\n";
	while($row = mysqli_fetch_assoc($result)){
	
		$salida .= "    <tr class=\"clickable-row\" data-href=\"#\">\n".
								"      <td><input type=\"checkbox\" name=\"chk_inboxMsgs\" id=\"chk_inboxMsgs_".encrypt($row['mensaje_id'])."\" value=\"".encrypt($row['mensaje_id'])."\"></td>\n". 
								"      <td>";
		if(0==$row['leido'])
			$salida .= "      <span class=\"glyphicon glyphicon-exclamation-sign\" id=\"spn_glyphReadSign_".encrypt($row['mensaje_id'])."\" style=\"color:red\" title=\"No leído\"></span>\n";
			
		$salida .= "        <span class=\"glyphicon ".$row['icono_tipo_mensaje']."\" style=\"color:".$row['color_tipo_mensaje']."\" title=\"".$row['etiqueta_tipo_mensaje']."\"></span>".
								"      </td>".
								"      <td>".get_userRealName($row['remitente_id'])."</td>\n".
								"      <td>".substr($row['asunto'],0,60)."</td>\n".
								"      <td>".$row['fecha_hora']."</td>\n".
								"      <td>".
								"  <button type=\"button\" id=\"btn_msgSee_".encrypt($row['mensaje_id'])."\" title=\"Leer Mensaje\" ".
																	"class=\"btn btn-xs btn-info btn-responsive\" onClick=\"readMsg('".encrypt($row['mensaje_id'])."');\">".
																	"<span class=\"glyphicon glyphicon-eye-open\"></span></button>". 							
								"</td>";
		
		$salida .= "    </tr>\n"; 
 
	}
	$salida .= "  </tbody>\n".
							"</table>".
							"</div>"; 
						 
}
//$salida = $query;
mysqli_close($db_modulos);
mysqli_close($db_catalogos);

?>

<!DOCTYPE html>
<html>
  <head>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/AdminLTE2/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
    <link href="../../libs/js/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css"/>  
    <link href="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.css" rel="stylesheet" type="text/css"/>      
    <link href="../../libs/bootstrap/summernote-0.6.6-dist/summernote.css" rel="stylesheet" type="text/css"/> 
    <link href="../../libs/bootstrap/font-awesome-4.3.0/css/font-awesome.css" rel="stylesheet" type="text/css"/>
    
    <script src="../../libs/AdminLTE2/plugins/jQuery/jQuery-2.1.3.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/bootstrap/js/bootstrap.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/dist/js/app.js" type="text/javascript"></script>   
    <script src="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>   
    <script src="../../libs/js/jqueryValidate/dist/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.form.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.uploadPreview.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery-ui-sliderAccess.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
    <script src="../../libs/bootstrap/summernote-0.6.6-dist/summernote.js" type="text/javascript"></script>
    <script src="../../libs/bootstrap/summernote-0.6.6-dist/lang/summernote-es-ES.js" type="text/javascript"></script>
    <script src="../../libs/js/common.js" type="text/javascript"></script>
    <script src="script.js" type="text/javascript"></script>
    
    <link rel="icon" type="image/png" href="../../img/favicon-16x16.png" sizes="16x16">
    
    <title>Coeficiente CRM - Perfil de <?php echo $usuarioNom?></title>
    <style type="text/css">
      .actTime{
        color:#757185;
      }      
    </style>
    <script type="text/javascript">
      $(document).ready(function(){
        //getUserMsgs();
        $("#tbl_inboxMsgs").DataTable({
          language: datatableespaniol,
          bSort : false,
          stateSave: true
        });
      }); 
    </script>
  </head>  
  
  <body>
      <!--Div que contiene el encabezado-->
    <div class="container" id="header"><?php include("../../libs/templates/header.php")?></div>
    
    <div class="container-fluid">
      <h2 class="page-header" id="tituloPag">
        <span class="glyphicon glyphicon-envelope"></span> <?php echo $title ?>
      </h2>
    </div><!-- /container -->
    
    <div class="container">
      <div class="row">
        <div id=div_msgAlert></div>
      </div>
    </div> <!-- /container -->
    <div id="div_usrMsgs">
			  <?php echo $salida;?>
    </div>
    
    <!--_Cuadro de dialogo de visualización de mensajes-->
    
    <div class="container-fluid" id="footer"><?php include("../../libs/templates/footer.php")?></div>
  </body>
  
</html>
