<?php

/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('America/Mexico_City');
$hoy = date("Y-m-d H:i:s");
$nombredoc = "Reporte Cotizaciones - " . $hoy;
if (PHP_SAPI == 'cli')
    die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Reporte Ventas")
    ->setLastModifiedBy("Reporte Cotizaciones")
    ->setTitle("Cotizaciones")
    ->setDescription("Reporte de los prospectos atrasados en su seguimiento.");
// ->setSubject("Office 2007 XLSX Test Document")
// ->setKeywords("office 2007 openxml php")
// ->setCategory("Test result file");


// Add some data
// encabezados de las columnas
$objPHPExcel->setActiveSheetIndex(0);
$encabezados = ['nombre', 'ubicacion_nombre', 'nombre_comercial', 'total', 'notas', 'estatus', 'status_pros', 'servicio', 'fecha_actualizacion', 'prospecto_estado_vigencia'];
$index = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
$index2 = [];
$index2 = $index;
for ($i = 0; $i < count($index); $i++) {
    for ($j = 0; $j < count($index); $j++) {
        array_push($index2, $index[$i] . $index[$j]);
    }
}
$cont = 1;
$encabezados2 = ['Ejecutivo o Asignado a', 'Region', 'Nombre de cliente', 'Total', 'Notas', 'Estado Cotizacion', 'Estatus Prospecto', 'Servicios', 'Ultima actualizacion', 'Estado del prospecto'];

for ($i = 0; $i < count($encabezados2); $i++) {
    $colt = $i;
    // echo $index2[$i] . $cont, " ".$encabezados[$i] . "<br>";
    $objPHPExcel->getActiveSheet()->setCellValue($index2[$i] . $cont, $encabezados2[$i])->getStyle($index2[$i] . $cont)->getFont()->setBold(true)->setSize(14);
    $objPHPExcel->getActiveSheet()->getStyle($index2[$i] . $cont)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('b3b3b3');

}
$cont++;
//fin de los encabezados

$cfgTableNameModP = 'coecrm_modulos';
$cfgTableNameUsrP = 'coecrm_usuarios';
$cfgTableNameCatP = 'coecrm_catalogos';
$cfgTableNameOmnP = 'bdOMNI';
$cfgDbServerP['location'] = '10.1.0.109';
$cfgDbServerP['user'] = 'supervisor';
$cfgDbServerP['pass'] = 'Kb.204.h32017';
//Opciones de la conexión base de pruebas
$opcionesP = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
try {
    $conP = new PDO('mysql:host=' . $cfgDbServerP['location'] . ';dbname=' . $cfgTableNameUsrP, $cfgDbServerP['user'], $cfgDbServerP['pass'], $opcionesP);
    /****************************************************************************************/

    /****************************************************************************************/
    $query = "SELECT usr.nombre, usr.apellidos, cest.estatus,ubi.ubicacion_nombre, pros.nombre_comercial, cot.subtotal, cot.total, cot.notas, cot.cotizacion_id, cvs.estatus as status_pros, pros.fecha_alta, pros.fecha_actualizacion, cvs.tiempo_limite, pros.activo " .
        "FROM coecrm_modulos.prospectos as pros, " .
        "coecrm_modulos.cotizaciones as cot, " .
        "coecrm_usuarios.usuarios as usr, " .
        "coecrm_catalogos.cat_cotizacion_estatus as cest, " .
        "coecrm_catalogos.cat_venta_estatus AS cvs, " .
        "coecrm_catalogos.cat_coe_ubicacion as ubi " .
        "WHERE pros.prospecto_id = cot.origen_id " .
        "AND cot.region_id = ubi.ubicacion_id " .
        "AND cot.cotizacion_estatus_id = cest.cotizacion_estatus_id " .
        "AND cot.usuario_alta = usr.usuario_id " .
        "AND subtotal > 0 " .
        "AND cvs.venta_estatus_id = pros.estatus " .
        "AND region_id > 0 ".
        "order by usr.nombre;";
    // echo $query;
    $stmt = $conP->prepare($query);
    $stmt->execute();

    setlocale(LC_MONETARY, 'en_US');
    foreach ($stmt as $row) {

        $row['fecha_alta'] = explode(' ', $row['fecha_alta']);
        $row['fecha_alta'] = $row['fecha_alta'][0];
        $row['fecha_actualizacion'] = explode(' ', $row['fecha_actualizacion']);
        $row['fecha_actualizacion'] = $row['fecha_actualizacion'][0];
        $fecNow = date('Y-m-d H:i:s');
        $fecLim = date('Y-m-d H:i:s', strtotime($row['fecha_actualizacion'] . "+" . $row['tiempo_limite'] . " hours"));
        $fecDif = round((strtotime($fecLim) - strtotime($fecNow)) / 3600);
        $fecThi = (int) $row['tiempo_limite'] / 3;



        if (1 == $row['activo']) {
            // si el prospecto esta liminado
        } else {
            if ($fecDif <= 0) {
                // atrasado
                $row['prospecto_estado_vigencia'] = "Atrasado";
            } elseif ($fecDif <= $fecThi * 2) {
                // si esta proximo a atrasarce
                $row['prospecto_estado_vigencia'] = "Proximo a atrasarse";
            } else {
                // si esta al corriente
                $row['prospecto_estado_vigencia'] = "Al corriente";
            }

            //select cotp.producto_id, cotp.cotizacion_id, serv.servicio from coecrm_modulos.rel_cotizacion_producto as cotp, coecrm_catalogos.cat_venta_servicios as serv where serv.servicio_id = cotp.producto_id and cotizacion_id = 869;
            if ($row['status_pros'] == "Cierre (100%)") {
                $query2 = "select cotp.producto_id, cotp.cotizacion_id, serv.servicio " .
                    "FROM coecrm_modulos.rel_cotizacion_producto as cotp, " .
                    "coecrm_catalogos.cat_venta_servicios as serv " .
                    "WHERE serv.servicio_id = cotp.producto_id " .
                    "AND cotizacion_id = " . $row['cotizacion_id'];
                // echo $query;
                $stmt2 = $conP->prepare($query2);
                $stmt2->execute();
                $serv = "";
                foreach ($stmt2 as $row2) {
                    $serv .= $row2['servicio'] . ", ";
                }
                $row['servicio'] = $serv;
                $row['subtotal'] =  money_format('%.2n', $row['subtotal']);
                $row['total'] =  money_format('%.2n', $row['total']);
                $row['nombre'] = $row['nombre'] . " " . $row['apellidos'];
                for ($i = 0; $i < count($encabezados); $i++) {
                    $objPHPExcel->getActiveSheet()->setCellValue($index2[$i] . $cont, $row[$encabezados[$i]]);
                }
                $cont++;
            }

        }

        
    }
    //fin del foreach




    // Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle('Cotizaciones');
    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);
    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $nombredoc . '.xls"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
    exit;
} catch (PDOException $e) {
    print "¡Error!: " . $e->getMessage() . "<br/>";
    die();
}
