<?php

/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
    die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");


// Add some data

// Miscellaneous glyphs, UTF-8
$objPHPExcel->setActiveSheetIndex(0);
$encabezados = ['prospecto_id', 'nombre', 'ubicacion_nombre', 'nombre_comercial', 'fecha_alta', 'fecha_actualizacion', 'venta', 'descripcion', 'estatus'];

$index = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
$index2 = [];
$index2 = $index;
for ($i = 0; $i < count($index); $i++) {
    for ($j = 0; $j < count($index); $j++) {
        array_push($index2, $index[$i] . $index[$j]);
    }
}
$cont = 1;
$encabezados2 = ['ID', 'Ejecutivo o Asignado a', 'Region', 'Nombre de cliente', 'fecha de captura', 'fecha de ultimo cambio', 'Origen prospecto', 'Observaciones', 'Estatus y Etapa', 'Notas'];
for ($i = 0; $i < count($encabezados2); $i++) {
    $colt = $i;
    // echo $index2[$i] . $cont, " ".$encabezados[$i] . "<br>";
    $objPHPExcel->getActiveSheet()->setCellValue($index2[$i] . $cont, $encabezados2[$i])->getStyle($index2[$i] . $cont)->getFont()->setBold(true)->setSize(14);
    // $objPHPExcel->getActiveSheet()->getStyle($index2[$i] . $cont)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
    
}
$cont++;







$cfgTableNameModP = 'coecrm_modulos';
$cfgTableNameUsrP = 'coecrm_usuarios';
$cfgTableNameCatP = 'coecrm_catalogos';
$cfgTableNameOmnP = 'bdOMNI';
$cfgDbServerP['location'] = '10.1.0.109';
$cfgDbServerP['user'] = 'supervisor';
$cfgDbServerP['pass'] = 'Kb.204.h32017';
//Opciones de la conexión base de pruebas
$opcionesP = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
try {
    $conP = new PDO('mysql:host=' . $cfgDbServerP['location'] . ';dbname=' . $cfgTableNameUsrP, $cfgDbServerP['user'], $cfgDbServerP['pass'], $opcionesP);
    /****************************************************************************************/

    /****************************************************************************************/
    $query = "SELECT pr.prospecto_id, pr.razon_social, pr.admin_auth, pr.municipio, pr.representante_id, pr.estatus AS estatus_id, pr.origen, pr.origen_otro, pr.asignadoa, usr.nombre, usr.apellidos, pr.descripcion, pr.nombre_comercial,pr.telefono,pr.fecha_alta, pr.fecha_actualizacion, pr.facebook, pr.twitter, pr.instagram, pr.activo, vSt.*, dir.telefono3, ubi.ubicacion_nombre, cvs.estatus AS staName, cvo.venta " .
        "FROM " . $cfgTableNameModP . ".prospectos AS pr, " .
        $cfgTableNameModP . ".direccion_detalles AS dir, " .
        $cfgTableNameCatP . ".cat_venta_estatus AS vSt, " .
        $cfgTableNameUsrP . ".usuarios AS usr, " .
        $cfgTableNameCatP . ".cat_coe_ubicacion AS ubi, " .
        $cfgTableNameCatP . ".cat_venta_estatus AS cvs, " .
        $cfgTableNameCatP . ".cat_venta_origen AS cvo " .
        "WHERE pr.estatus >= 1" .
        " AND vSt.venta_estatus_id = pr.estatus" .
        " AND dir.origen_id = pr.prospecto_id" .
        " AND dir.tipo_origen = 0" .
        " AND usr.usuario_id = pr.asignadoa" .
        " AND ubi.ubicacion_id = usr.ubicacion_id" .
        " AND cvs.venta_estatus_id = pr.estatus" .
        " AND pr.estatus > 0" .
        " AND cvo.venta_origen_id = pr.origen order by pr.prospecto_id ";
    // echo $query;
    $stmt = $conP->prepare($query);
    $stmt->execute();
    foreach ($stmt as $row) {

        $razSoc = ($row["razon_social"] != "") ? $row["razon_social"] : "<i>Sin Dato</i>";
        $fecNow = date('Y-m-d H:i:s');
        $fecLim = date('Y-m-d H:i:s', strtotime($row['fecha_actualizacion'] . "+" . $row['tiempo_limite'] . " hours"));
        $fecDif = round((strtotime($fecLim) - strtotime($fecNow)) / 3600);
        $fecThi = (int) $row['tiempo_limite'] / 3;
        $row['nombre'] = $row['nombre'] . " " . $row['apellidos'];

        $row['fecha_alta'] = explode(' ', $row['fecha_alta']);
        $row['fecha_alta'] = $row['fecha_alta'][0];

        $row['fecha_actualizacion'] = explode(' ', $row['fecha_actualizacion']);
        $row['fecha_actualizacion'] = $row['fecha_actualizacion'][0];

        if (1 != $row['activo']) {

            if ($fecDif <= 0) {
                for ($i = 0; $i < count($encabezados); $i++) {
                    $col = $i;
                    $prosId = $row['prospecto_id'];

                    $objPHPExcel->getActiveSheet()->setCellValue($index2[$i] . $cont, $row[$encabezados[$i]]);
                }
                // buscamos los comentarios de este prospecto
                $querycomment = "SELECT * FROM coecrm_modulos.comentarios_prospectos where prospecto_id = $prosId";
                $stmtc = $conP->prepare($querycomment);
                $stmtc->execute();;
                $comment = "";
                if ($stmtc->rowCount() > 0) {
                    foreach ($stmtc as $rowc) {
                        $comment .= $rowc['comentario'] . ", ||";
                    }
                    $objPHPExcel->getActiveSheet()->setCellValue($index2[$col + 1] . $cont, $comment);
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue($index2[$col + 1] . $cont, 'No tiene ningun comentario');
                }
                // cambiamos de esta fila a la siguiente
                $cont++;
            }
        }
    }
    //fin del foreac










// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Simple');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="01simple.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
} catch (PDOException $e) {
    print "¡Error!: " . $e->getMessage() . "<br/>";
    die();
}