$(document).ready(function () {
    $("#contractDialog").load("../../libs/templates/contractdialog.php");
    $("#pricesDialog").load("../../libs/templates/pricesdialog.php");
    $("#workOrderDialog").load("../../libs/templates/workorderdialog.php");
    $("#siteDialog").load("../../libs/templates/sitedialog.php");
    $("#documentDialog").load("../../libs/templates/documentdialog.php");
    $("#whatsDialog").load("../../libs/templates/whatsappdialog.php");
    $("#eventDialog").load("../../libs/templates/eventdialog.php");
    $("#managerDialog").load("../../libs/templates/managerdialog.php");
    // getStatusTabs(0);
    $("#div_other_origin").hide();
    $("#txt_rfc").blur(function () {
        formatRfc("txt_rfc", "mod_prospectInsert");
    });
    $("#txt_rfc").focus(function () {
        deformatRfc("txt_rfc");
    });
    $("#rad_fisica").click(function () {
        $("#txt_rfc").attr('placeholder', 'XXXX-000000-YYY');
        deformatRfc("txt_rfc");
        formatRfc("txt_rfc", "mod_prospectInsert");
    });
    $("#rad_moral").click(function () {
        $("#txt_rfc").attr('placeholder', 'XXX-000000-YYY');
        deformatRfc("txt_rfc");
        formatRfc("txt_rfc", "mod_prospectInsert");
    });
    updateIndexActive("li_navBar_soporte");
});


//función que obtiene la lista de todos los prospectos existentes con retrasos
function getProspectRegsAtr() {
    // setLoadDialog(0, "Obteniendo lista de prospectos");
    $.ajax({
        type: "POST",
        url: "ajax.php",
        data: {
            "action": "getProspectTableAtr"
        },
        beforeSend: function () {
            setLoadDialog(0, "Obteniendo lista de prospectos atrasados.");
        },
        complete: function () {
            setLoadDialog(1, "");
        },
        success: function (msg) {
            setLoadDialog(1, "");
            var resp = msg.trim().split("||");
            if ("OK" == resp[0]) {
                $("#ProspectAtr").html(resp[1]);
                $("#tbl_prospects_atr").DataTable({
                    stateSave: true,
                    language: datatableespaniol,
                });
                // if (4 == status) {
                    // $("input[name='sel_giroCom']").rules("add", "isSomethingSelected");
                // }
                // console.log(resp[1]);
            } else {
                printErrorMsg("div_msgAlert", resp[1]);
            }
        }
    });
}
getProspectRegsAtr();

function generarXcelProspectos(){
    $(location).attr('href', 'PHPExcel/Prospectos.php');
}

function generarXcelCotizacion(){
    // alert("modulo de reporte en construccion");
    $(location).attr('href', 'Excel/Cotizaciones.php');
}

function generarXcelVenta(){
    alert("modulo de reporte en construccion");
    // $(location).attr('href', 'Excel/Reporte Prospectos.php');
}

//función que obtiene los datos completos de un prospecto seleccionado
function getProspectDetails(id) {
    $("#hdn_orgId").val(id);
    $.ajax({
        type: "POST",
        url: "ajax.php",
        data: {
            "action": "getProspectDetails",
            "prosId": id,
            "option": "0"
        },
        beforeSend: function () {
            setLoadDialog(0, "Obteniendo detalles del prospecto");
        },
        complete: function () {
            setLoadDialog(1, "");
        },
        success: function (msg) {
            var resp = msg.trim().split("||");
            $("#div_prospectDetails").html(resp[0]);
            console.log(resp[0]);
            initMap(resp[1], resp[2], 'div_prospectMap', 'hdn_proslatitud', 'hdn_proslongitud');

            if (undefined != resp[3]) {
                var sitLocArr = JSON.parse(resp[3]);
                $.each(sitLocArr, function (key, value) { //arreglo de sitios
                    addMapPointer(value);
                });
            }
            setModalResponsive('mod_prospectDetails');
            $("#mod_prospectDetails").modal("show");
        }
    });
}

//abre el cuadro de diálogo para insertar un prospecto
function openNewProspect() {
    $("#contactProspectDialog").load("../../libs/templates/contactdialog.php"); //carga cuadro de dialogo de insertar contacto en el modulo
    $("#hdn_action").val(0);
    $("#hdn_prosId").val(-1);
    getStatesList(0, "spn_edo", "");
    getCountryList(151, "spn_pais", "");
    getOriginList(0);
    getStatusList(0);
    getAssigntoList(0);
    getBusinessList(0);
    clear_fields();
    setModalResponsive('mod_prospectInsert');
    $("#mod_prospectInsert").modal("show");
    $("#div_prospectManagerFields").hide();
    initMap(20.683, -103.378, 'div_prosMap', 'hdn_proslatitud', 'hdn_proslongitud');
}

//abre el cuadro de diálogo para editar un prospecto
function editProspect(id) {
    $("#contactProspectDialog").load("../../libs/templates/contactdialog.php"); //carga cuadro de dialogo de insertar contacto en el modulo
    $("#hdn_action").val(1);
    $("#hdn_prosId").val(id);
    fill_fields(id);
}

//llena los campos del formulario de prospecto cuando se elige editar
function fill_fields(id) {
    clear_fields();
    $.ajax({
        type: "POST",
        url: "ajax.php",
        beforeSend: function () {
            setLoadDialog(0, "Obteniendo detalles del prospecto para su edición");
        },
        complete: function () {
            setLoadDialog(1, "");
        },
        data: {
            "action": "getProspectDetails",
            "prosId": id,
            "option": "1"
        },
        success: function (msg) {
            var resp = msg.trim().split("||");
            if ("OK" == resp[0]) {
                printErrorMsg("div_msgAlertProspect", resp[1]);

                getStatesList(resp[11], "spn_edo", "");
                getCountryList(resp[27], "spn_pais", "");
                getOriginList(resp[13]);
                getStatusList(resp[14]);
                getAssigntoList(resp[15]);
                getBusinessList(resp[28]);

                $("#div_msgAlertProspect").html("");
                $("#txt_razonSoc").val(resp[1]);
                $("#txt_nomCom").val(resp[2]);
                $("#txt_rfc").val(resp[3]);

                if (1 == resp[4]) {
                    $("#rad_fisica").prop('checked', true);
                    $("#rad_moral").prop('checked', false);
                } else if (2 == resp[4]) {
                    $("#rad_fisica").prop('checked', false);
                    $("#rad_moral").prop('checked', true);
                }

                $("#txt_tel").val(resp[5]);
                $("#txt_ext").val(resp[39]);
                $("#txt_cell").val(resp[6]);
                $("#txt_dom").val(resp[7]);
                $("#txt_col").val(resp[8]);
                $("#txt_cp").val(resp[9]);
                $("#txt_mun").val(resp[10]);
                $("#txt_email").val(resp[12]);
                $("#txt_desc").val(resp[18]);

                if (8 == resp[13]) {
                    $("#txt_origin").val(resp[19]);
                    $("#div_other_origin").show();
                } else {
                    $("#txt_origin").val("");
                    $("#div_other_origin").hide();
                }

                $("#txt_tel2").val(resp[20]);
                $("#txt_tel3").val(resp[21]);
                $("#txt_numExt").val(resp[22]);
                $("#txt_numInt").val(resp[23]);
                $("#txt_calle1").val(resp[24]);
                $("#txt_calle2").val(resp[25]);
                $("#txt_webSite").val(resp[26]);
                $("#txt_face").val(resp[31]);
                $("#txt_twit").val(resp[32]);
                $("#txt_insta").val(resp[33]);
                if ("" != resp[34] && undefined != resp[34])
                    $("#div_logo").html("<img src='" + resp[34] + "' alt='logo' title='logo' width='200' class='img-responsive'/>");
                else
                    $("#div_logo").html("<img src='../../img/img_no_disponible.png' alt='logo' title='logo' width='200' class='img-responsive'/>");

                if (4 == resp[14] || 7 <= resp[14] && 9 >= resp[14]) {
                    $("#txt_prosNumPod").val(resp[35]);
                    $("#txt_prosFecPod").val(resp[36]);
                    $("#txt_prosNotario").val(resp[37]);
                    $("#txt_prosFolio").val(resp[38]);

                    $("#div_prospectManagerFields").show();
                } else
                    $("#div_prospectManagerFields").hide();

                var contArr = JSON.parse(resp[40]);

                for (var x = 0; x < contArr.length; x++) {
                    insertContactOnForm(1, contArr[x], 0);
                }
                setModalResponsive('mod_prospectInsert');
                $("#mod_prospectInsert").modal("show");

                initMap(resp[29], resp[30], 'div_prosMap', 'hdn_proslatitud', 'hdn_proslongitud');
                $("#hdn_proslatitud").val(resp[29]);
                $("#hdn_proslongitud").val(resp[30]);
            } else {
                printErrorMsg("div_msgAlert", resp[1]);
            }
        }
    });
}

//Limpia todos los campos del formulario de prospecto
function clear_fields() {
    $("#txt_nomCom").val("");
    $("#txt_rfc").val("");
    $("#rad_fisica").prop('checked', true);
    $("#rad_moral").prop('checked', false);
    $("#rad_persona").val(1);
    $("#txt_tel").val("");
    $("#txt_ext").val("");
    $("#txt_tel2").val("");
    $("#txt_tel3").val("");
    $("#txt_razonSoc").val("");
    $("#txt_dom").val("");
    $("#txt_calle1").val("");
    $("#txt_calle2").val("");
    $("#txt_col").val("");
    $("#txt_cp").val("");
    $("#txt_mun").val("");
    $("#txt_numExt").val("");
    $("#txt_numInt").val("");
    $("#sel_state").val(0);
    $("#sel_country").val(0);
    $("#txt_webSite").val("");
    $("#sel_origin").val(0);
    $("#txt_origin").val("");
    $("#sel_status").val(0);
    $("#sel_assignUsers").val(0);
    $("#txt_desc").val("");
    $("#hdn_latitud").val(-1);
    $("#hdn_longitud").val(-1);
    $("#div_logo").html("");
    $("#fl_logo").replaceWith($("#fl_logo").val("").clone(true));
    $("#txt_prosNumPod").val("");
    $("#txt_prosFecPod").val("");
    $("#txt_prosFolio").val("");
    $("#txt_prosNotario").val("");
    $("#txt_prosFecPod").datetimepicker(datetimeOptions);

    contactoArray.length = 0;
    $("#tbl_selectedContact0").find("tr:gt(0)").remove();
    $("#hdn_contactNewAction").val(0);
    $("#btn_contInsert").attr('onclick', "insertContactOnForm(0,'');");

    $("#div_msgAlertProspect").html("");
    $("#txt_razonSoc_error").html("");
    $("#txt_rfc_error").html("");
    $("#txt_nomCom_error").html("");
    $("#txt_tel_error").html("");
    $("#txt_ext_error").html("");
    $("#txt_tel2_error").html("");
    $("#txt_tel3_error").html("");
    $("#txt_dom_error").html("");
    $("#txt_calle1_error").html("");
    $("#txt_calle2_error").html("");
    $("#txt_col_error").html("");
    $("#txt_cp_error").html("");
    $("#txt_mun_error").html("");
    $("#txt_numExt_error").html("");
    $("#txt_numInt_error").html("");
    $("#sel_state_error").html("");
    $("#txt_webSite_error").html("");
    $("#sel_origin_error").html("");
    $("#txt_origin_error").html("");
    $("#sel_status_error").html("");
    $("#sel_assignUsers_error").html("");
    $("#txt_desc_error").html("");
    $("#hdn_contArr_error").html("");
    $("#spn_txt_razonSoc").html("");
    $("#spn_txt_nomCom").html("");
    $("#sel_pais_error").html("");
    $("#sel_giroCom_error").html("");
    $("#txt_prosNumPod_error").html("");
    $("#txt_prosFecPod_error").html("");
    $("#txt_prosFolio_error").html("");
    $("#txt_prosNotario_error").html("");

}

function valProspectDocs(val, id) {
    var option = 0;

    if ($("#chk_prospVerif_" + val + "_" + id).is(":checked"))
        option = 1;
    else
        option = 0;

    $.ajax({
        type: "POST",
        url: "ajax.php",
        data: {
            "action": "valProspectDoc",
            "prosId": id,
            "option": option,
            "stat": val
        },
        beforeSend: function () {
            setLoadDialog(0, "Actualizando estatus de autorización");
        },
        complete: function () {
            setLoadDialog(1, "");
        },
        success: function (msg) {
            var resp = msg.trim().split("||");
            if ("OK" == resp[0]) {
                printSuccessMsg("div_msgAlert", resp[1]);
                if (3 == val && 1 == option)
                    getPrices(id, 0, 1);
            } else
                printErrorMsg("div_msgAlert", resp[1]);
        }
    });
}

//función que manda a guardar los datos de un prospecto nuevo o modificar los de un prospecto existente
function saveProspectNew() {
    $("#hdn_contArr").val(JSON.stringify(contactoArray));
    //var repJson = JSON.stringify(representanteArray);
    //$("#hdn_manArr").val(JSON.stringify(representanteArray));
    if (false == validate_data()) {
        printErrorMsg("div_msgAlertProspect", "Existen datos no válidos, favor de corregirlos");
    } else {
        var options = {
            contentType: 'multipart/form-data; charset=utf-8',
            //data:{"repArray": repJson},
            beforeSend: function () {
                setLoadDialog(0, "Guardando prospecto");
            },
            complete: function (response) {
                setLoadDialog(1, "");
                var resp = (response.responseText).trim().split('||');
                var errCod = resp[0];
                var errMsg = resp[1];
                if ("OK" == errCod) {
                    printSuccessMsg("div_msgAlert", errMsg);
                    $("#mod_prospectInsert").modal("hide");
                    location.reload();
                } else {
                    printErrorMsg("div_msgAlertProspect", errMsg);
                }
            },
            error: function () {
                printErrorMsg("div_msgAlertProspect", "Ocurrió un error al enviarse los datos para su guardado");
            }
        };
        $("#frm_newProspect").ajaxForm(options);
    }
}

//función que manda a eliminar un prospecto
function deleteProspect(id, opt) {
    if (confirm("¿Está seguro/a de eliminar este prospecto? Ésta acción no se podrá deshacer.")) {
        $.ajax({
            type: "POST",
            url: "ajax.php",
            data: {
                "action": "deleteProspect",
                "prosId": id,
                "option": opt
            },
            beforeSend: function () {
                setLoadDialog(0, "Eliminando prospecto");
            },
            complete: function () {
                setLoadDialog(1, "");
            },
            success: function (msg) {
                var resp = msg.trim().split("||");
                if ("OK" == resp[0]) {
                    printSuccessMsg("div_msgAlert", resp[1]);
                    //getProspectRegs();
                } else {
                    printErrorMsg("div_msgAlert", resp[1]);
                }
            }
        });
    }
}

//función que valida la integridad de todos los datos del formulario de prospecto nuevo/editar. Devuelve true si todos los campos son válidos
function validate_data() {
    addValidationRules(0);
    jQuery.validator.addMethod("isOtherOriginSpecified", function (value, element) {
        if ((0 >= (value).length) && ("8" == $("#sel_origin").val()))
            return false;
        else
            return true;
    }, "Debe especificar otro tipo de origen");

    return $("#frm_newProspect").validate({
        ignore: ':hidden:not(#hdn_contArr)',
        rules: {
            txt_tel: {
                number: true,
                isPhoneNumber: true
            },
            txt_ext: {
                number: true
            },
            txt_tel2: {
                isPhoneNumber: true
            },
            txt_tel3: {
                isPhoneNumber: true
            },
            txt_rfc: {
                rfc: true
            },
            txt_cp: {
                number: true
            },
            txt_numExt: {
                number: true
            },
            txt_origin: {
                isOtherOriginSpecified: true
            },
            sel_state: {
                isSomethingSelected: true
            },
            sel_status: {
                isSomethingSelected: true
            },
            sel_origin: {
                isSomethingSelected: true
            },
            hdn_contArr: {
                hasContacts: true
            },
            txt_prosFecPod: {
                isDateTime: true
            }
        },
        errorPlacement: function (error, element) {
            error.appendTo($('#' + element.attr("id") + '_error'));
        }
    }).form();
}

function convertToClient(id) {
    if (confirm("Los datos de este prospecto, así como datos de cotización, comentarios, y eventos serán transferidos a los datos de clientes y no estarán disponibles para este modulo ¿Está seguro/a de migrar los datos de este prospecto?")) {
        window.location.href = "../clientes/index.php?pId=" + id;
    }
}
