<?php
session_start();
set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

//include_once "../../libs/db/dbcommon.php";
include_once "../../libs/db/common.php";

$gruposArr  = $_SESSION['grupos'];
$usuarioId  = $_SESSION['usrId'];
$usuario    = $_SESSION['usuario'];
$nombre     = $_SESSION['usrNombre'];
$usrUbica   = $_SESSION['usrUbica'];
$modId = 0;

if(!verifySession($_SESSION)){
  logoutTimeout();
}else{

$db_catalogos = condb_catalogos();
$db_modulos   = condb_modulos();
$db_usuarios  = condb_usuarios();

$salida = "";


function checkIfRfcExists($value,$orgId,$orgType){
  $db_modulos   = condb_modulos();
  global $salida;
  
  if($value=='XAXX-010101-YYY'||$value=='XAX-010101-YYY'){
    return true;
  }
  else{
    log_write("DEBUG: CHECK-IF-RFC-EXISTS: Se checará si ya existe el prospecto",0);
    
    $query = "SELECT prospecto_id FROM prospectos WHERE rfc='".$value."'";
    
    $result = mysqli_query($db_modulos,$query);
    
    log_write("DEBUG: CHECK-IF-RFC-EXISTS: ".$query,0);
    
    if(!$result){
      log_write("ERROR: CHECK-IF-RFC-EXISTS: No se pudo obtener si ya existe el prospecto",0);
      $salida = "ERROR|| Ocurrió un problema en consulta de datos previos";
      return false;
    }
    else{
       if(0!=mysqli_num_rows($result)){
        log_write("OK: CHECK-IF-RFC-EXISTS: El prospecto ya existe",0);
        $salida = "ERROR||Ya existe un registro con el dato ".$value;
        return false;
      }
      else{
        log_write("OK: CHECK-IF-RFC-EXISTS: El prospecto no existe aún, se procederá a guardar",0);
        //$salida = "ERROR||No existe un registro con el dato ".$value." se puede guardar";
        return true;
      }
    }
  }
  mysqli_close($db_modulos);
}

if("getProspectTable" == $_POST["action"]){
   $stat = 1;
  //  $stat = isset($_POST['status'])?$_POST['status']:-1;
  //if((array_key_exists(1, $gruposArr))||(array_key_exists(2, $gruposArr))){}
  if(hasPermission($modId,'t')){
    $queryAct = "";
  }
  else{
    $queryAct =" AND pr.activo = 0";
  }
    if (hasPermission($modId, 'f') && hasPermission($modId, 'r')) {
      $query = "SELECT pr.prospecto_id, pr.razon_social, pr.admin_auth, pr.representante_id, pr.estatus AS estatus_id, pr.origen, pr.origen_otro, pr.asignadoa, pr.descripcion, pr.nombre_comercial,pr.telefono,pr.fecha_alta, pr.fecha_actualizacion, pr.facebook, pr.twitter, pr.instagram, pr.activo, vSt.*, dir.telefono3, ubi.ubicacion_nombre, cvs.estatus AS staName, cvo.venta " .
        "FROM " . $cfgTableNameMod . ".prospectos AS pr, " .
        $cfgTableNameMod . ".direccion_detalles AS dir, " .
        $cfgTableNameCat . ".cat_venta_estatus AS vSt, " .
        $cfgTableNameUsr . ".usuarios AS usr, " .
        $cfgTableNameCat . ".cat_coe_ubicacion AS ubi, " .
        $cfgTableNameCat . ".cat_venta_estatus AS cvs, " .
        $cfgTableNameCat . ".cat_venta_origen AS cvo " .
        "WHERE pr.estatus = " . $stat .
        " AND vSt.venta_estatus_id = pr.estatus" .
        " AND dir.origen_id = pr.prospecto_id" .
        " AND dir.tipo_origen = 0" .
        " AND usr.usuario_id = pr.usuario_alta" .
        " AND ubi.ubicacion_id = usr.ubicacion_id" .
        " AND cvs.venta_estatus_id = pr.estatus" .
        " AND pr.estatus > 0" .
        " AND cvo.venta_origen_id = pr.origen" .
        $queryAct;
      $msgresp = "entro primero";
    }
  elseif(hasPermission($modId,'c')&&hasPermission($modId,'r')){
    $query = "SELECT pr.prospecto_id, pr.razon_social, pr.admin_auth, pr.estatus AS estatus_id, pr.origen, pr.origen_otro, pr.asignadoa, pr.descripcion, pr.nombre_comercial,pr.telefono,pr.fecha_alta, pr.fecha_actualizacion, pr.facebook, pr.twitter, pr.instagram, vSt.*, dir.telefono3, pr.activo, cvs.estatus AS staName, cvo.venta ".
             "FROM ".$cfgTableNameMod.".prospectos AS pr, ".
                     $cfgTableNameMod.".direccion_detalles AS dir, ".
                     $cfgTableNameUsr.".usuarios AS usr, ".
                     $cfgTableNameCat.".cat_venta_estatus AS vSt, ".
                     $cfgTableNameCat.".cat_venta_estatus AS cvs, ".
                     $cfgTableNameCat.".cat_venta_origen AS cvo ".
             "WHERE pr.usuario_alta=usr.usuario_id AND usr.ubicacion_id=".$usrUbica." AND pr.estatus=".$stat.
             " AND pr.estatus=vSt.venta_estatus_id ".
             " AND dir.origen_id = pr.prospecto_id".
             " AND dir.tipo_origen = 0".
             " AND pr.estatus > 0".
             " AND cvs.venta_estatus_id = pr.estatus".
             " AND cvo.venta_origen_id = pr.origen".
             $queryAct;
      $msgresp = "entro segundo";
  }
  elseif(hasPermission($modId,'l')){
    $query = "SELECT pr.prospecto_id, pr.razon_social, pr.admin_auth, pr.representante_id, pr.estatus AS estatus_id, pr.origen, pr.origen_otro, pr.asignadoa, pr.descripcion, pr.nombre_comercial,pr.telefono,pr.fecha_alta, pr.fecha_actualizacion, pr.facebook, pr.twitter, pr.instagram, vSt.*, dir.telefono3, pr.activo, cvs.estatus AS staName, cvo.venta ".
             "FROM ".$cfgTableNameMod.".prospectos AS pr, ".
             $cfgTableNameMod.".direccion_detalles AS dir, ".
             $cfgTableNameCat.".cat_venta_estatus AS vSt, ".
             $cfgTableNameCat.".cat_venta_estatus AS cvs, ".
             $cfgTableNameCat.".cat_venta_origen AS cvo ".
             "WHERE (pr.usuario_alta=".$usuarioId." OR pr.asignadoa=".$usuarioId.") ".
             "AND pr.estatus=".$stat.
             " AND pr.estatus=vSt.venta_estatus_id".
             " AND dir.origen_id = pr.prospecto_id".
             " AND dir.tipo_origen = 0".
             " AND pr.estatus > 0".
             " AND cvs.venta_estatus_id = pr.estatus".
             " AND cvo.venta_origen_id = pr.origen".
             $queryAct;
    $msgresp = "entro tercero";
  }
  else{
    $salida = "<b>ERROR:</b> No cuenta con permisos para ver esta información";
    goto prosTablea;
  }
 
  error_log("query pros ".$query);
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $salida = "ERROR|| No se pudo consultar de los datos de prospectos en la DB ";
  }
  else{
    $salida = "OK||<table id=\"tbl_prospects_$stat\" class=\"table table-hover table-striped\">\n".
              "  <thead>\n".
              "    <tr>\n".
              "      <th class=\"hidden-xs\">ID</th>\n".
              "      <th class=\"hidden-xs\">Estatus</th>\n".
              "      <th>Nombre Comercial</th>\n".
              "      <th>Fecha Límite Estatus</th>\n".
              "      <th class=\"hidden-xs\">Origen Prospecto</th>\n";
    if(hasPermission($modId,'f'))
      $salida .="      <th>Región</th>\n";          
    $salida .="      <th>Teléfono</th>\n".
              "      <th>Asignado a:</th>\n".
              "      <th class=\"hidden-xs\">Observación</th>\n".
              "      <th>Acciones</th>\n";
    $salida .= "      <th>Autorización</th>\n";
                        
    $salida .= "    </tr>\n".
               "  </thead>\n".
               "  <tbody>\n";
    while($row = mysqli_fetch_assoc($result)){
    
      //$estatus = get_sellStat($row['estatus']);
      $estatus = $row['staName'];
      
      if(8!=$row['origen'])
        //$origen = get_sellOrigin($row['origen']);
        $origen = $row['venta'];
      else
        $origen = $row['origen_otro'];
        
      $nombre = get_userRealName($row['asignadoa']);
      $razSoc = ($row["razon_social"]!="")?$row["razon_social"]:"<i>Sin Dato</i>";
      
      $fecNow = date('Y-m-d H:i:s');
      $fecLim = date('Y-m-d H:i:s',strtotime($row['fecha_actualizacion']."+".$row['tiempo_limite']." hours"));
      
      $fecDif = round((strtotime($fecLim) - strtotime($fecNow))/3600);
      
      $fecThi = (int)$row['tiempo_limite']/3;
      
      if(1==$row['activo']){
        $icon = "<span style=\"font-size:0\"> eliminado</span><span class=\"glyphicon glyphicon-remove\" style=\"color:red\" title=\"Eliminado\"></span>";
      }
      else{
        if($fecDif<=0){
          $icon = "<span style=\"font-size:0\">3 atrasado retrasado</span><span class=\"glyphicon glyphicon-alert\" style=\"color:red\" title=\"Presenta un retraso de ".abs($fecDif)." horas\"></span>";
        } 
        elseif($fecDif<=$fecThi*2){
          $icon = "<span style=\"font-size:0\">2 proximo por atrasarse</span><span class=\"glyphicon glyphicon-alert\" style=\"color:orange\" title=\"Próximo a atrasarse en ".$fecDif." horas\"></span>";
        }
        else{
          $icon = "<span style=\"font-size:0\">1 ok nuevo al corriente</span><span class=\"glyphicon glyphicon-ok\" style=\"color:green\" title=\"Próximo a atrasarse en ".$fecDif." horas\"></span>";
        }
      }
      
      $salida .= "    <tr>\n".
                 "      <td class=\"hidden-xs\">".$row["prospecto_id"]."</td>\n".
                 "      <td class=\"hidden-xs\">".$icon."</td>\n".
                 "      <td>".$row["nombre_comercial"]."</td>\n".
                 "      <td>".$fecLim."</td>\n".
                 "      <td class=\"hidden-xs\">".$origen."</td>\n";
     if(hasPermission($modId,'f'))
      $salida .="      <td>".$row["ubicacion_nombre"]."</td>\n";            
     $salida .= "      <td>".
                "        <a href='#' onclick=\"callMarcatron('".$row["telefono"]."','TELEFONO')\">".$row["telefono"]."</a>";
                 //"        ".$row["telefono"]."</a>";
      
      /*if(""!=$row["telefono3"]&&isset($row["telefono3"]))
        $salida .= "&nbsp;<button type=\"button\" id=\"btn_prospectWhats\" title=\"WhatsApp\" class=\"btn btn-xs btn-success btn-responsive\" onClick=\"openWhatsMsg('".$row["telefono3"]."');\"><i class=\"fa fa-whatsapp\"></i></button></div>";*/
         
      /*if(0<sizeof($row['facebook'])&&""!=$row['facebook']){
         $salida .= "&nbsp;<i class=\"fa fa-facebook-square\"></i>";
      } 
      if(0<sizeof($row['twitter'])&&""!=$row['twitter']){
         $salida .= "&nbsp;<i class=\"fa fa-twitter\"></i>";
      } 
      if(0<sizeof($row['instagram'])&&""!=$row['instagram']){
         $salida .= "&nbsp;<i class=\"fa fa-instagram\"></i>";
      }  */       
                 
      $salida .= "      </td>\n".
                 "      <td><a href=/crm/modulos/perfil/index.php?id='".encrypt($row['asignadoa'])."'>".$nombre."</a></td>\n";
                 if(30<strlen($row["descripcion"]))
                   $salida .= "      <td class=\"hidden-xs\" title=\"".$row["descripcion"]."\">".substr($row["descripcion"],0,30)."...</td>\n"; 
                 else
                   $salida .= "      <td class=\"hidden-xs\">".$row["descripcion"]."</td>\n";             
      $salida .= "      <td>".
                 "        <button type=\"button\" id=\"btn_prospectDetails_".$row['prospecto_id']."\" title=\"Ver Detalles\" class=\"btn btn-xs btn-primary btn-responsive\" onClick=\"getProspectDetails(".$row['prospecto_id'].");\"><span class=\"glyphicon glyphicon-list\"></span></button>";
      if(hasPermission($modId,'w')||(hasPermission($modId,'l')&&$row['asignadoa']==$usuarioId)){           
        $salida .= "        <button type=\"button\" id=\"btn_prospectComments_".$row['prospecto_id']."\" title=\"Ver Comentarios\" class=\"btn btn-xs btn-info btn-responsive\" onClick=\"getComments(".$row['prospecto_id'].",0);\"><span class=\"glyphicon glyphicon-comment\"></span></button>";
      }
      if(hasPermission(3,'r')||(hasPermission(3,'l')&&$row['asignadoa']==$usuarioId)){
        $salida .= "        <button type=\"button\" id=\"btn_prospectEvents_".$row['prospecto_id']."\" title=\"Ver Eventos\" class=\"btn btn-xs btn-warning btn-responsive\" onClick=\"getEvents(".$row['prospecto_id'].",0);\"><span class=\"glyphicon glyphicon-calendar\"></span></button>";
      }
      if(hasPermission(4,'r')||hasPermission(4,'w')){ //tiene permiso para leer datos de cotizaciones
        $salida .= "        <button type=\"button\" id=\"btn_prospectPrices_".$row['prospecto_id']."\" title=\"Ver Cotizaciones\" class=\"btn btn-xs btn-default btn-responsive\" onClick=\"getPrices(".$row['prospecto_id'].",0,0);\"><span class=\"glyphicon glyphicon-usd\"></span></button>";
      }
      if((hasPermission(2,'r')||hasPermission(2,'w'))&&1<=$row['orden']){  //si tiene permisos para leer datos de sitios                  
        $salida .= "        <button type=\"button\" id=\"btn_ProspectSites_".$row['prospecto_id']."\" title=\"Ver Sitios\" class=\"btn btn-xs btn-default btn-responsive\" onClick=\"displayOrgSites(".$row['prospecto_id'].",0,0);\"><span class=\"glyphicon glyphicon-map-marker\"></span></button>";
      }
      if((4==$stat)&&(hasPermission($modId,'w')||(hasPermission($modId,'l')&&$row['asignadoa']==$usuarioId))){
        $salida .= "        <button type=\"button\" id=\"btn_prospecttDocs_".$row['prospecto_id']."\" title=\"Ver Documentación\" class=\"btn btn-xs btn-primary btn-responsive\" onClick=\"displayDocuments(".$row['prospecto_id'].",0);\"><span class=\"glyphicon glyphicon-file\"></span></button>";
      }
                         
      if(hasPermission($modId,'d')){
        if(0==$row['activo']){
          $salida.= "        <button type=\"button\" id=\"btn_prospectDelete_".$row['prospecto_id']."\" title=\"Eliminar Prospecto\" class=\"btn btn-xs btn-danger btn-responsive\" onClick=\"deleteProspect(".$row['prospecto_id'].",1);\"><span class=\"glyphicon glyphicon-remove\"></span></button>";
        }
        else{
          $salida.= "        <button type=\"button\" id=\"btn_prospectDelete_".$row['prospecto_id']."\" title=\"Restaurar Prospecto\" class=\"btn btn-xs btn-secondary btn-responsive\" onClick=\"deleteProspect(".$row['prospecto_id'].",0);\"><span class=\"glyphicon glyphicon-repeat\"></span></button>";
        }       
      }
      if(hasPermission($modId,'e')||(hasPermission($modId,'n')&&$row['asignadoa']==$usuarioId)){
        $salida .= "        <button type=\"button\" id=\"btn_prospectEdit_".$row['prospecto_id']."\" title=\"Editar Registro\" class=\"btn btn-xs btn-success btn-responsive\" onClick=\"editProspect(".$row['prospecto_id'].");\"><span class=\"glyphicon glyphicon-edit\"></span></button>";
      }                             
      $salida.= "      </td>\n";
      
      if(4==$row['estatus_id']){
        $queryDocCons = "SELECT(SELECT COUNT(ctd.tipo_documento_id) FROM ".$cfgTableNameCat.".cat_tipo_documento AS ctd WHERE ctd.tipo_documento_id != 0 AND ctd.tipo_documento_id != 6 ) AS cont_tipos, ".	
                        "(SELECT COUNT(DISTINCT doc.tipo_documento_id) FROM ".$cfgTableNameMod.".documentos AS doc WHERE doc.origen_id=".$row['prospecto_id']." AND doc.tipo_origen=0) AS cont_docsact FROM dual";
                        
        /*$queryDocCons = "SELECT(SELECT COUNT(ctd.tipo_documento_id) FROM ".$cfgTableNameCat.".cat_tipo_documento AS ctd WHERE ctd.tipo_documento_id != 0 AND ctd.tipo_documento_id != 6 ) AS cont_tipos, ".	
                        "(SELECT COUNT(DISTINCT doc.tipo_documento_id) FROM ".$cfgTableNameMod.".documentos AS doc WHERE doc.origen_id=".$row['prospecto_id']." AND doc.tipo_origen=0) AS cont_docsact FROM dual";*/
        $salida .= "      <td>\n";              
        if(!$resultDocCons = mysqli_query($db_modulos,$queryDocCons)){
          $salida .= "      <i>Error al obtener estatus</i>\n";	
        }
        else{
          $rowDocCons = mysqli_fetch_assoc($resultDocCons);
          
          if($rowDocCons['cont_tipos']<=$rowDocCons['cont_docsact']){
            $salida .= "<span style=\"font-size:0\">".$row['admin_auth']."</span>	";
            $salida .= "<span title=\"documentos digitales completados\" class=\"glyphicon glyphicon-ok\" style=\"color:green\"></span>";
            if(hasPermission($modId,'a')){
              
              if(1==$row['admin_auth']||0==$row['admin_auth']){
                $salida .= "<input type=\"checkbox\" title=\"verificar documentos físicos\" class=\"chk_filters\" id=\"chk_prospVerif_2_".encrypt($row['prospecto_id'])."\" value=2  onchange=\"valProspectDocs(2,'".encrypt($row['prospecto_id'])."')\">";
                $salida .= "<input type=\"checkbox\" title=\"confirmar match de documentos verificados\" class=\"chk_filters\" id=\"chk_prospVerif_3_".encrypt($row['prospecto_id'])."\" value=3  onchange=\"valProspectDocs(3,'".encrypt($row['prospecto_id'])."')\" disabled>";
              }
              elseif(3==$row['admin_auth']){
                $salida .= "<input type=\"checkbox\" title=\"verificar documentos físicos\" class=\"chk_filters\" id=\"chk_prospVerif_2_".encrypt($row['prospecto_id'])."\" value=2  onchange=\"valProspectDocs(2,'".encrypt($row['prospecto_id'])."')\" checked=\"checked\" disabled>";
                $salida .= "<input type=\"checkbox\" title=\"confirmar match de documentos verificados\" class=\"chk_filters\" id=\"chk_prospVerif_3_".encrypt($row['prospecto_id'])."\" value=3  onchange=\"valProspectDocs(3,'".encrypt($row['prospecto_id'])."')\" checked=\"checked\">";
              }
              elseif(2==$row['admin_auth']){
                $salida .= "<input type=\"checkbox\" title=\"verificar documentos físicos\" class=\"chk_filters\" id=\"chk_prospVerif_2_".encrypt($row['prospecto_id'])."\" value=2  onchange=\"valProspectDocs(2,'".encrypt($row['prospecto_id'])."')\" checked=\"checked\">";
                $salida .= "<input type=\"checkbox\" title=\"confirmar match de documentos verificados\" class=\"chk_filters\" id=\"chk_prospVerif_3_".encrypt($row['prospecto_id'])."\" value=3  onchange=\"valProspectDocs(3,'".encrypt($row['prospecto_id'])."')\">";
              }
            }
            else{
              /*if(1==$row['admin_auth']||0==$row['admin_auth']){
                $salida .= "<span title=\"documentos digitales completados\" class=\"glyphicon glyphicon-ok\" style=\"color:green\"></span>";
              }*/
              if(3==$row['admin_auth']){
                $salida .= "<span title=\"documentos físicos verificados\" class=\"glyphicon glyphicon-ok\" style=\"color:green\"></span>";
                $salida .= "<span title=\"match de documentos verificados\" class=\"glyphicon glyphicon-ok\" style=\"color:green\"></span>";
              }
              elseif(2==$row['admin_auth']){
                $salida .= "<span title=\"documentos físicos verificados\" class=\"glyphicon glyphicon-ok\" style=\"color:green\"></span>";
              }
            }
          }
          else{
            $salida .= "<span class=\"glyphicon glyphicon-remove\" style=\"color:red\" title=\"Documentos pendientes de agregar\"></span>";
          }
        }
      }
      elseif(10==$row['estatus_id']){
        $salida .= "      <td><span style=\"color:orange\">Proceso terminado</span></td>";
      }
      else{
        $salida .= "      <td><i>Sin acción a realizar</i></td>\n";
      }
      $salida.= "    </tr>\n";      
    }
    $salida .= "  </tbody>\n".
               "</table>\n"; 
  }
  prosTablea:
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
  // echo "<script type=\"text/javascript\">alert(\"$msgresp\");</script>"; 
  // print "<script type='text/javascript'>console.log(".$query.");</script>";
}



//obtener la lista de prospectos 
if("getProspectTableAtr" == $_POST["action"]){
   $stat = 1;
  //  $stat = isset($_POST['status'])?$_POST['status']:-1;
  //if((array_key_exists(1, $gruposArr))||(array_key_exists(2, $gruposArr))){}
  if(hasPermission($modId,'t')){
    $queryAct = "";
  }
  else{
    $queryAct =" AND pr.activo = 0";
  }
    if (hasPermission($modId, 'f') && hasPermission($modId, 'r')) {
      $query = "SELECT pr.prospecto_id, pr.razon_social, pr.admin_auth, pr.representante_id, pr.estatus AS estatus_id, pr.origen, pr.origen_otro, pr.asignadoa, pr.descripcion, pr.nombre_comercial,pr.telefono,pr.fecha_alta, pr.fecha_actualizacion, pr.facebook, pr.twitter, pr.instagram, pr.activo, vSt.*, dir.telefono3, ubi.ubicacion_nombre, cvs.estatus AS staName, cvo.venta " .
        "FROM " . $cfgTableNameMod . ".prospectos AS pr, " .
        $cfgTableNameMod . ".direccion_detalles AS dir, " .
        $cfgTableNameCat . ".cat_venta_estatus AS vSt, " .
        $cfgTableNameUsr . ".usuarios AS usr, " .
        $cfgTableNameCat . ".cat_coe_ubicacion AS ubi, " .
        $cfgTableNameCat . ".cat_venta_estatus AS cvs, " .
        $cfgTableNameCat . ".cat_venta_origen AS cvo " .
        "WHERE pr.estatus >= " . $stat .
        " AND vSt.venta_estatus_id = pr.estatus" .
        " AND dir.origen_id = pr.prospecto_id" .
        " AND dir.tipo_origen = 0" .
        " AND usr.usuario_id = pr.usuario_alta" .
        " AND ubi.ubicacion_id = usr.ubicacion_id" .
        " AND cvs.venta_estatus_id = pr.estatus" .
        " AND pr.estatus > 0" .
        " AND cvo.venta_origen_id = pr.origen" .
        $queryAct;
      $msgresp = $query;
    }
  elseif(hasPermission($modId,'c')&&hasPermission($modId,'r')){
    $query = "SELECT pr.prospecto_id, pr.razon_social, pr.admin_auth, pr.estatus AS estatus_id, pr.origen, pr.origen_otro, pr.asignadoa, pr.descripcion, pr.nombre_comercial,pr.telefono,pr.fecha_alta, pr.fecha_actualizacion, pr.facebook, pr.twitter, pr.instagram, vSt.*, dir.telefono3, pr.activo, cvs.estatus AS staName, cvo.venta ".
             "FROM ".$cfgTableNameMod.".prospectos AS pr, ".
                     $cfgTableNameMod.".direccion_detalles AS dir, ".
                     $cfgTableNameUsr.".usuarios AS usr, ".
                     $cfgTableNameCat.".cat_venta_estatus AS vSt, ".
                     $cfgTableNameCat.".cat_venta_estatus AS cvs, ".
                     $cfgTableNameCat.".cat_venta_origen AS cvo ".
             "WHERE pr.usuario_alta=usr.usuario_id AND usr.ubicacion_id=".$usrUbica." AND pr.estatus >=".$stat.
             " AND pr.estatus=vSt.venta_estatus_id ".
             " AND dir.origen_id = pr.prospecto_id".
             " AND dir.tipo_origen = 0".
             " AND pr.estatus > 0".
             " AND cvs.venta_estatus_id = pr.estatus".
             " AND cvo.venta_origen_id = pr.origen".
             $queryAct;
      $msgresp = "entro segundo";
  }
  elseif(hasPermission($modId,'l')){
    $query = "SELECT pr.prospecto_id, pr.razon_social, pr.admin_auth, pr.representante_id, pr.estatus AS estatus_id, pr.origen, pr.origen_otro, pr.asignadoa, pr.descripcion, pr.nombre_comercial,pr.telefono,pr.fecha_alta, pr.fecha_actualizacion, pr.facebook, pr.twitter, pr.instagram, vSt.*, dir.telefono3, pr.activo, cvs.estatus AS staName, cvo.venta ".
             "FROM ".$cfgTableNameMod.".prospectos AS pr, ".
             $cfgTableNameMod.".direccion_detalles AS dir, ".
             $cfgTableNameCat.".cat_venta_estatus AS vSt, ".
             $cfgTableNameCat.".cat_venta_estatus AS cvs, ".
             $cfgTableNameCat.".cat_venta_origen AS cvo ".
             "WHERE (pr.usuario_alta=".$usuarioId." OR pr.asignadoa=".$usuarioId.") ".
             "AND pr.estatus >=".$stat.
             " AND pr.estatus=vSt.venta_estatus_id".
             " AND dir.origen_id = pr.prospecto_id".
             " AND dir.tipo_origen = 0".
             " AND pr.estatus > 0".
             " AND cvs.venta_estatus_id = pr.estatus".
             " AND cvo.venta_origen_id = pr.origen".
             $queryAct;
    $msgresp = "entro tercero";
  }
  else{
    $salida = "<b>ERROR:</b> No cuenta con permisos para ver esta información";
    goto prosTablea2;
  }
 $cons = $query;
  error_log("query pros ".$query);
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $salida = "ERROR|| No se pudo consultar de los datos de prospectos en la DB ";
  }
  else{
    $salida = "OK||<table id=\"tbl_prospects_atr\" class=\"table table-hover table-striped\">\n".
              "  <thead>\n".
              "    <tr>\n".
              "      <th class=\"hidden-xs\">ID</th>\n".
              "      <th class=\"hidden-xs\">Estatus</th>\n".
              "      <th>Nombre Comercial</th>\n".
              "      <th>Fecha Límite Estatus</th>\n".
              "      <th class=\"hidden-xs\">Origen Prospecto</th>\n";
    if(hasPermission($modId,'f'))
      $salida .="      <th>Región</th>\n";          
    $salida .="      <th>Teléfono</th>\n".
              "      <th>Asignado a:</th>\n".
              "      <th class=\"hidden-xs\">Observación</th>\n".
              "      <th>Acciones</th>\n";
    $salida .= "      <th>Estado Prospecto</th>\n";
                        
    $salida .= "    </tr>\n".
               "  </thead>\n".
               "  <tbody>\n";



    while($row = mysqli_fetch_assoc($result)){
      //$estatus = get_sellStat($row['estatus']);
      $estatus = $row['staName'];
      if(8!=$row['origen'])
        //$origen = get_sellOrigin($row['origen']);
        $origen = $row['venta'];
      else
        $origen = $row['origen_otro'];
      $nombre = get_userRealName($row['asignadoa']);
      $razSoc = ($row["razon_social"]!="")?$row["razon_social"]:"<i>Sin Dato</i>";
      $fecNow = date('Y-m-d H:i:s');
      $fecLim = date('Y-m-d H:i:s',strtotime($row['fecha_actualizacion']."+".$row['tiempo_limite']." hours"));
      $fecDif = round((strtotime($fecLim) - strtotime($fecNow))/3600);
      $fecThi = (int)$row['tiempo_limite']/3;
      if(1==$row['activo']){
        $icon = "<span style=\"font-size:0\"> eliminado</span><span class=\"glyphicon glyphicon-remove\" style=\"color:red\" title=\"Eliminado\"></span>";
      }
      else{
        if($fecDif<=0){
          $icon = "<span style=\"font-size:0\">3 atrasado retrasado</span><span class=\"glyphicon glyphicon-alert\" style=\"color:red\" title=\"Presenta un retraso de ".abs($fecDif)." horas\"></span>";
            $salida .= "    <tr>\n" .
              "      <td class=\"hidden-xs\">" . $row["prospecto_id"] . "</td>\n" .
              "      <td class=\"hidden-xs\">" . $icon . "</td>\n" .
              "      <td>" . $row["nombre_comercial"] . "</td>\n" .
              "      <td>" . $fecLim . "</td>\n" .
              "      <td class=\"hidden-xs\">" . $origen . "</td>\n";
            if (hasPermission($modId, 'f'))
              $salida .= "      <td>" . $row["ubicacion_nombre"] . "</td>\n";
            $salida .= "      <td>" .
              "        <a href='#' onclick=\"callMarcatron('" . $row["telefono"] . "','TELEFONO')\">" . $row["telefono"] . "</a>";
            //"        ".$row["telefono"]."</a>";

                    /*if(""!=$row["telefono3"]&&isset($row["telefono3"]))
                $salida .= "&nbsp;<button type=\"button\" id=\"btn_prospectWhats\" title=\"WhatsApp\" class=\"btn btn-xs btn-success btn-responsive\" onClick=\"openWhatsMsg('".$row["telefono3"]."');\"><i class=\"fa fa-whatsapp\"></i></button></div>";*/

                    /*if(0<sizeof($row['facebook'])&&""!=$row['facebook']){
                $salida .= "&nbsp;<i class=\"fa fa-facebook-square\"></i>";
              } 
              if(0<sizeof($row['twitter'])&&""!=$row['twitter']){
                $salida .= "&nbsp;<i class=\"fa fa-twitter\"></i>";
              } 
              if(0<sizeof($row['instagram'])&&""!=$row['instagram']){
                $salida .= "&nbsp;<i class=\"fa fa-instagram\"></i>";
              }  */

            $salida .= "      </td>\n" .
              "      <td><a href=/crm/modulos/perfil/index.php?id='" . encrypt($row['asignadoa']) . "'>" . $nombre . "</a></td>\n";
            if (30 < strlen($row["descripcion"]))
              $salida .= "      <td class=\"hidden-xs\" title=\"" . $row["descripcion"] . "\">" . substr($row["descripcion"], 0, 30) . "...</td>\n";
            else
              $salida .= "      <td class=\"hidden-xs\">" . $row["descripcion"] . "</td>\n";
            $salida .= "      <td>" .
              "        <button type=\"button\" id=\"btn_prospectDetails_" . $row['prospecto_id'] . "\" title=\"Ver Detalles\" class=\"btn btn-xs btn-primary btn-responsive\" onClick=\"getProspectDetails(" . $row['prospecto_id'] . ");\"><span class=\"glyphicon glyphicon-list\"></span></button>";
            if (hasPermission($modId, 'w') || (hasPermission($modId, 'l') && $row['asignadoa'] == $usuarioId)) {
              // $salida .= "        <button type=\"button\" id=\"btn_prospectComments_" . $row['prospecto_id'] . "\" title=\"Ver Comentarios\" class=\"btn btn-xs btn-info btn-responsive\" onClick=\"getComments(" . $row['prospecto_id'] . ",0);\"><span class=\"glyphicon glyphicon-comment\"></span></button>";
            }
            if (hasPermission(3, 'r') || (hasPermission(3, 'l') && $row['asignadoa'] == $usuarioId)) {
              // $salida .= "        <button type=\"button\" id=\"btn_prospectEvents_" . $row['prospecto_id'] . "\" title=\"Ver Eventos\" class=\"btn btn-xs btn-warning btn-responsive\" onClick=\"getEvents(" . $row['prospecto_id'] . ",0);\"><span class=\"glyphicon glyphicon-calendar\"></span></button>";
            }
            if (hasPermission(4, 'r') || hasPermission(4, 'w')) { //tiene permiso para leer datos de cotizaciones
              // $salida .= "        <button type=\"button\" id=\"btn_prospectPrices_" . $row['prospecto_id'] . "\" title=\"Ver Cotizaciones\" class=\"btn btn-xs btn-default btn-responsive\" onClick=\"getPrices(" . $row['prospecto_id'] . ",0,0);\"><span class=\"glyphicon glyphicon-usd\"></span></button>";
            }
            if ((hasPermission(2, 'r') || hasPermission(2, 'w')) && 1 <= $row['orden']) {  //si tiene permisos para leer datos de sitios                  
              // $salida .= "        <button type=\"button\" id=\"btn_ProspectSites_" . $row['prospecto_id'] . "\" title=\"Ver Sitios\" class=\"btn btn-xs btn-default btn-responsive\" onClick=\"displayOrgSites(" . $row['prospecto_id'] . ",0,0);\"><span class=\"glyphicon glyphicon-map-marker\"></span></button>";
            }
            if ((4 == $stat) && (hasPermission($modId, 'w') || (hasPermission($modId, 'l') && $row['asignadoa'] == $usuarioId))) {
              // $salida .= "        <button type=\"button\" id=\"btn_prospecttDocs_" . $row['prospecto_id'] . "\" title=\"Ver Documentación\" class=\"btn btn-xs btn-primary btn-responsive\" onClick=\"displayDocuments(" . $row['prospecto_id'] . ",0);\"><span class=\"glyphicon glyphicon-file\"></span></button>";
            }
            if (hasPermission($modId, 'd')) {
              if (0 == $row['activo']) {
                // $salida .= "        <button type=\"button\" id=\"btn_prospectDelete_" . $row['prospecto_id'] . "\" title=\"Eliminar Prospecto\" class=\"btn btn-xs btn-danger btn-responsive\" onClick=\"deleteProspect(" . $row['prospecto_id'] . ",1);\"><span class=\"glyphicon glyphicon-remove\"></span></button>";
              } else {
                // $salida .= "        <button type=\"button\" id=\"btn_prospectDelete_" . $row['prospecto_id'] . "\" title=\"Restaurar Prospecto\" class=\"btn btn-xs btn-secondary btn-responsive\" onClick=\"deleteProspect(" . $row['prospecto_id'] . ",0);\"><span class=\"glyphicon glyphicon-repeat\"></span></button>";
              }
            }
            if (hasPermission($modId, 'e') || (hasPermission($modId, 'n') && $row['asignadoa'] == $usuarioId)) {
              // $salida .= "        <button type=\"button\" id=\"btn_prospectEdit_" . $row['prospecto_id'] . "\" title=\"Editar Registro\" class=\"btn btn-xs btn-success btn-responsive\" onClick=\"editProspect(" . $row['prospecto_id'] . ");\"><span class=\"glyphicon glyphicon-edit\"></span></button>";
            }
            $salida .= "      </td>\n";
            /************************ *//************************ */
            // aqui va el status del prospecto 
            /************************ *//************************ */
              $row['staName'] = explode("(", $row['staName']);
              $salida .= "      <td><i> (".$row['staName'][1]." ". $row['staName'][0]."  </i></td>\n";
            $salida .= "    </tr>\n";      
        }
        // fin del if de $fecdif 
        elseif($fecDif<=$fecThi*2){
          // $icon = "<span style=\"font-size:0\">2 proximo por atrasarse</span><span class=\"glyphicon glyphicon-alert\" style=\"color:orange\" title=\"Próximo a atrasarse en ".$fecDif." horas\"></span>";
        }
        else{
          // $icon = "<span style=\"font-size:0\">1 ok nuevo al corriente</span><span class=\"glyphicon glyphicon-ok\" style=\"color:green\" title=\"Próximo a atrasarse en ".$fecDif." horas\"></span>";
        }
      }
    }
    //fin del while de los resultados
    $salida .= "  </tbody>\n".
               "</table>\n"; 
  }
  prosTablea2:
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  // $salida = $query;
  echo $salida;
  // echo "<script type=\"text/javascript\">alert(\"$msgresp\");</script>"; 
  // print "<script type='text/javascript'>console.log(".$query.");</script>";
}







if("valProspectDoc" == $_POST["action"]){
  
  $prosId = isset($_POST['prosId']) ? decrypt($_POST['prosId']) : -1;
  $option = isset($_POST['option']) ? $_POST['option'] : -1;
  $stat = isset($_POST['stat']) ? $_POST['stat'] : -1;
  
  if(!valProspectDoc($prosId,$option,$stat))
    $salida = "ERROR||Ocurrió un problema al activar la verificación de prospecto";
  else
    $salida = "OK||La verificación ha sido activada correctamente ";
  
  echo $salida;
}

//llamada de ajax. obtiene todos los datos de un prospecto. Dependiendo su opción crea una tabla con ellos o envía los campos para llenar el formulario de editar prospecto
if("getProspectDetails" == $_POST["action"]){
  
  $prosId = isset($_POST['prosId']) ? $_POST['prosId'] : -1;
  $option = isset($_POST['option']) ? $_POST['option'] : -1;
  
  //if((array_key_exists(1, $gruposArr))||(array_key_exists(2, $gruposArr))){
  if(hasPermission($modId,'a')||hasPermission($modId,'r')){
  
    $queryPri = "SELECT pr.*, dir.* ".
                "FROM ".$cfgTableNameMod.".prospectos AS pr, ".
                        $cfgTableNameMod.".direccion_detalles AS dir ".
                "WHERE pr.prospecto_id = ".$prosId." ". 
                "AND pr.prospecto_id = dir.origen_id ".
                "AND dir.tipo_origen = 0 ";
  }
  else if(hasPermission($modId,'l')){
    $queryPri = "SELECT pr.*, dir.* ".
                "FROM ".$cfgTableNameMod.".prospectos AS pr, ".
                        $cfgTableNameMod.".direccion_detalles AS dir ".
                "WHERE (usuario_alta=".$usuarioId." OR asignadoa=".$usuarioId.") AND pr.prospecto_id = ".$prosId." ". 
                "AND pr.prospecto_id = dir.origen_id ".
                "AND dir.tipo_origen = 0 ";
  }
  else{
    $salida = "<b>ERROR:</b> No cuenta con permisos para ver esta información";
    goto prosDetailsa;
  }
  
  $query2 = "SELECT rel.*, cont.*, dep.departamento, pue.puesto AS puestoPref ".
            "FROM ".$cfgTableNameMod.".rel_origen_contacto AS rel, ".
                    $cfgTableNameMod.".contactos AS cont, ".
                    $cfgTableNameCat.".cat_departamentos AS dep, ".
                    $cfgTableNameCat.".cat_puestos AS pue ".
            "WHERE rel.tipo_origen=0 AND rel.origen_id=".$prosId.
            " AND rel.contacto_id=cont.contacto_id ".
            "AND cont.puesto_id = pue.puesto_id AND cont.departamento_id=dep.departamento_id ".
            "ORDER BY cont.apellidos ASC";
  
  $querySitLoc = "SELECT nombre,latitud,longitud FROM ".$cfgTableNameMod.".sitios WHERE origen_id=".$prosId." AND tipo_origen=0";
  
  $result = mysqli_query($db_modulos,$queryPri);
  
  $result2 = mysqli_query($db_modulos,$query2);
  
  $result4 = mysqli_query($db_modulos,$querySitLoc);
  
  if(!$result||!$result2||!$result4){
    $salida = "ERROR|| No se pudo cargar los detalles para este prospecto de la DB ";
  }
  else{
    if(0==mysqli_num_rows($result)){
      $salida = "ERROR||No existen registros para el prospecto con este id ";
    }
    else{
      if(0==$option){  
        $row = mysqli_fetch_assoc($result);
        if(hasPermission(1,'w')&&10==$row['estatus_id']){
          $salida = "<button type=\"button\" id=\"btn_migrateProspect_".$prosId."\" title=\"Convertir en Cliente\" class=\"btn btn-xs btn-default btn-responsive\" onClick=\"convertToClient('".encrypt($prosId)."')\"><span class=\"glyphicon glyphicon-arrow-right\">Convertir en Cliente</span></button>".
                    "<br><br>";
        }    
        $salida .= "<div class=\"row\" id=\"div_prospectDetails\">";    
        $estatus = get_sellStat($row['estatus']);
        if(0<strlen($row['origen_otro']))
          $origen = $row['origen_otro'];
        else
          $origen = get_sellOrigin($row['origen']);
        $nombreAlta = get_userRealName($row['usuario_alta']);
        $nombreAsign = get_userRealName($row['asignadoa']);
        $estado = get_state($row['estado']);
        $tipoPer = get_personType($row['tipo_persona']);
        $pais = get_country($row['pais']);
        $giroComercial = get_business($row['giro_id']);
        if($row["rfc"]=='XAXX-010101-YYY'||$row["rfc"]=='XAX-010101-YYY'){
          $rfc = "&nbsp;";
        }
        else{
          $rfc = $row["rfc"];
        }
        /***************************************************************************************/
        /******************************** cosnulta comentario  *********************************/
        /***************************************************************************************/
        $querycomment = "SELECT * FROM coecrm_modulos.comentarios_prospectos where prospecto_id = $prosId";
        $resultcom = mysqli_query($db_modulos, $querycomment);
        $row2 = mysqli_fetch_assoc($resultcom);
        $comment = "";
        foreach($resultcom as $fila){
            $comment .= $fila['comentario'];
            $comment .= "<br>";
            $comment .= "<br>";
        }
        /***************************************************************************************/
        /***************************************************************************************/
        $salida .= "<div class=\"col-lg-6 col-xs-12 col-sm-6\"><table id=\"tbl_prospectdetails\" class=\"table table-striped table-condensed\">\n".
                   "  <tbody>\n".
                   "    <tr>".
                   "      <td>ID</td>".
                   "      <td>".$row['prospecto_id']."</td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td><b>Razón Social</b></td>".
                   "      <td>".$row['razon_social']."</td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td>Nombre Comercial</td>".
                   "      <td>".$row['nombre_comercial']."</td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td>RFC</td>".
                   "      <td>".$rfc."</td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td>Giro Comercial</td>".
                   "      <td>".$giroComercial."</td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td>Teléfono</td>".
                   "      <td><a href='#' onclick=\"callPhone('".$row['telefono']."')\">".$row['telefono']."</a></td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td>Extensión</td>".
                   "      <td>".$row['extension']."</td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td>Teléfono2</td>".
                   "      <td><a href='#' onclick=\"callPhone('".$row['telefono2']."')\">".$row['telefono2']."&nbsp;</a></td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td>Teléfono3</td>".
                   "      <td><a href='#' onclick=\"callPhone('".$row['telefono3']."')\">".$row['telefono3']."&nbsp;</a></td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td>Calle</td>".
                   "      <td>".$row['domicilio']."</td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td>Num Exterior</td>".
                   "      <td>".$row['num_exterior']."</td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td>Num Interior</td>".
                   "      <td>".$row['num_interior']."</td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td>Colonia</td>".
                   "      <td>".$row['colonia']."</td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td>CP</td>".
                   "      <td>".$row['cp']."</td>".
                   "    </tr>".
                   "    <tr>".
                          "<td> Comentarios </td>".
                          "<td>".
                          "<div id='detalle_comentario'>". $comment ."</div>".
                          "</td>".
                   "</tr>\n".
                   "  </tbody>\n". 
                   "</table></div>\n";
                   
        $salida .= "<div class=\"col-lg-6 col-xs-12 col-sm-6\"><table id=\"tbl_prospectdetails\" class=\"table table-striped table-condensed\">\n".
                   "  <tbody>\n".
                   "    <tr>".
                   "      <td>Municipio</td>".
                   "      <td>".$row['municipio']."</td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td>Estado</td>".
                   "      <td>".$estado."</td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td>País</td>".
                   "      <td>".$pais."</td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td>Sitio Web</td>".
                   "      <td>".$row['sitio_web']."</td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td>Estatus</td>".
                   "      <td>".$estatus."</td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td>Asignado a:</td>".
                   "      <td><a href=/crm/modulos/perfil/index.php?id='".encrypt($row['asignadoa'])."'>".$nombreAsign."</a></td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td>Fecha de Alta</td>".
                   "      <td>".$row['fecha_alta']."</td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td>Usuario que lo Agregó</td>".
                   "      <td><a href=/crm/modulos/perfil/index.php?id='".encrypt($row['usuario_alta'])."'>".$nombreAlta."</a></td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td>Descripción</td>".
                   "      <td>".$row['descripcion']."</td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td>Facebook</td>".
                   "      <td>".$row['facebook']."</td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td>Twitter</td>".
                   "      <td>".$row['twitter']."</td>".
                   "    </tr>".
                   "    <tr>".
                   "      <td>Instagram</td>".
                   "      <td>".$row['instagram']."</td>".
                   "    </tr>".
                   "  </tbody>\n". 
                   "</table></div>\n";
      
        $salida .= "<b>Logo</b>\n";
        
        if(""!=$row['logo_ruta'])
          $salida .= "<img src='".$row['logo_ruta']."' alt='logo' title='".$row['razon_social']."' width='200' class='img-responsive'/><br>";     
        else
          $salida .= "<img src='../../img/img_no_disponible.png' alt='logo' title='logo' width='200' class='img-responsive'/><br>";                   
        if((4==$row['estatus']||(7<=$row['estatus']&&9>=$row['estatus']))&&(hasPermission($modId,'w')||(hasPermission($modId,'l')&&$row['asignadoa']==$usuarioId))){
          
          $salida .= "<hr><h4><b>Representante Legal:<b></h4>"; 
          
          if(0>=$row['representante_id']){
            $salida .= "<div><button type=\"button\" id=\"btn_insertarRepresentante_".$row['prospecto_id']."\" title=\"Insertar representante\" class=\"btn btn-sm btn-default btn-responsive\" onClick=\"openNewManager();\"><span class=\"glyphicon glyphicon-asterisk\">Nuevo Representante</span></button></div>";
            $salida .= "<div>\n\n<i>No existe represetante legal para este prospecto. Para agregar uno haz click en <b>Nuevo Representante</b></i><br><br></div>";
          }
          else{
            $salida .= "<div><button type=\"button\" id=\"btn_editarRepresentante_".$row['prospecto_id']."\" title=\"Insertar representante\" class=\"btn btn-sm btn-default btn-success\" onClick=\"editManager('".encrypt($row['representante_id'])."');\"><span class=\"glyphicon glyphicon-edit\">Editar Representante</span></button><br><br></div>";
          
            $queryRep = "SELECT * FROM representante_legal WHERE representante_id=".$row['representante_id'];
          
            $resultRep = mysqli_query($db_modulos,$queryRep);
            
            if(!$resultRep){
              $salida .= "<i>Ocurrió un error al obtenerse los datos del representante legal</i>\n";
            }
            else{
              if(0>=mysqli_num_rows($resultRep)){
                $salida .= "<i>No existe represetante legal para este prospecto</i>\n";
              }
              else{
                $rowRep = mysqli_fetch_assoc($resultRep);
                $estadoRep = get_state($rowRep['estado']);

                $salida .= "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\"><b>ID</b></div>\n".
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\">".$rowRep['representante_id']."&nbsp;</div>\n".
                           "  <div class=\"col-lg-6 col-xs-12 col-sm-6 nodatatablerowdark\">&nbsp;</div>\n".
                           
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowlight\"><b>Nombre</b></div>\n".
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowlight\">".$rowRep['nombre']." ".$rowRep['apellidos']."&nbsp;</div>\n".
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowlight\"><b>CP</b></div>\n".
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowlight\">".$rowRep['cp']."&nbsp;</div>\n".
                           
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\"><b># Notario</b></div>\n".
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\">".$rowRep['notario']."&nbsp;</div>\n".
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\"><b>Folio Mercantil</b></div>\n".
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\">".$rowRep['folio_mercantil']."&nbsp;</div>\n".
                            
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowlight\"><b># Poder</b></div>\n".
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowlight\">".$rowRep['num_poder']."&nbsp;</div>\n".
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowlight\"><b>Municipio</b></div>\n".
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowlight\">".$rowRep['municipio']."&nbsp;</div>\n".
                            
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\"><b>Fecha de Poder</b></div>\n".
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\">".$rowRep['fecha_poder']."&nbsp;</div>\n".
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\"><b>Estado</b></div>\n".
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\">".$estadoRep."&nbsp;</div>\n".
                           
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowlight\"><b>Domicilio</b></div>\n".
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowlight\">".$rowRep['domicilio']."</div>\n".
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowlight\"><b>Teléfono</b></div>\n".
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowlight\">".$rowRep['telefono']."</div>\n".
                           /*"  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowlight\"><b>Ext.</b></div>\n".
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowlight\">".$rowRep['extension']."</div>\n".*/
                            
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\"><b>Colonia</b></div>\n".
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\">".$rowRep['colonia']."&nbsp;</div>\n".
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\"><b>Tel Móvil</b></div>\n".
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\">".$rowRep['tel_movil']."&nbsp;</div>\n".
                           
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowlight\"><b>Email</b></div>\n".
                           "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowlight\">".$rowRep['email']."&nbsp;</div>\n".
                           "  <div class=\"col-lg-6 col-xs-12 col-sm-6 nodatatablerowlight\">&nbsp;</div>\n";
              }
            }
          }
        }
        
        $salida .= "\n\n<h4><b>Contactos:<b></h4>";           
        if(0==mysqli_num_rows($result2)){
          $salida .= "No existen contactos para este prospecto";
        }
        else{          
          $salida .= "<div class=\"table-responsive\"><table id=\"tbl_prospectContacts\" class=\"table table-striped\">\n".
                     "  <thead>\n".
                     "    <tr>\n".
                     "      <th class=\"hidden-xs\">id</th>\n".
                     "      <th>Nombre</th>\n".
                     "      <th>Apellidos</th>\n".
                     "      <th>Puesto</th>\n".
                     "      <th>Teléfono</th>\n".
                     "      <th>Teléfono Móvil</th>\n".
                     "      <th>Correo Electŕonico</th>\n".
                     "    </tr>\n".
                     "  </thead>\n".
                     "  <tbody>\n";   
                             
          while($row2 = mysqli_fetch_assoc($result2)){   
          
            if(6==$row2['puesto_id'])
              $puesto = $row2['puesto'];
            else
              $puesto = $row2['puestoPref'];   
                
            $salida .= "    <tr>\n".
                       "      <td class=\"hidden-xs\">".$row2['contacto_id']."</td>\n".
                       "      <td>".$row2['nombre']."</td>\n".
                       "      <td>".$row2['apellidos']."</td>\n".
                       "      <td>".$puesto."</td>\n".
                       "      <td>".$row2['telefono']."</td>\n".
                       "      <td>".$row2['tel_movil']."</td>\n".
                       "      <td>".$row2['email']."</td>\n".         
                       "    </tr>\n";
          }        
          $salida .= "  </tbody>\n".
                     "</table>\n</div>||".$row['latitud']."||".$row['longitud'];        
        }
        $sitLocArr = array(); 
        while($row4 = mysqli_fetch_assoc($result4)){
          $sitLocArr[] = array($row4['nombre'],$row4['latitud'],$row4['longitud']);
        }
        if(0<sizeOf($sitLocArr)){  
          $salida .="||".json_encode($sitLocArr);
        }
      }
      if(1==$option){
        while($row = mysqli_fetch_assoc($result)){
          $salida = "OK||".
                    $row['razon_social']."||".//1
                    $row['nombre_comercial']."||".//2
                    $row['rfc']."||".//3
                    $row['tipo_persona']."||".//4
                    $row['telefono']."||".//5
                    $row['tel_movil']."||".//6
                    $row['domicilio']."||".//7
                    $row['colonia']."||".//8
                    $row['cp']."||".//9
                    $row['municipio']."||".//10
                    $row['estado']."||".//11
                    $row['email']."||".//12
                    $row['origen']."||".//13
                    $row['estatus']."||".//14
                    $row['asignadoa']."||".//15
                    $row['fecha_alta']."||".//16
                    $row['usuario_alta']."||".//17
                    $row['descripcion']."||".//18
                    $row['origen_otro']."||".//19
                    $row['telefono2']."||".//20
                    $row['telefono3']."||".//21
                    $row['num_exterior']."||".//22
                    $row['num_interior']."||".//23
                    $row['calle1']."||".//24
                    $row['calle2']."||".//25
                    $row['sitio_web']."||".//26
                    $row['pais']."||".//27
                    $row['giro_id']."||".//28
                    $row['latitud']."||".//29
                    $row['longitud']."||".//30
                    $row['facebook']."||".//31
                    $row['twitter']."||".//32
                    $row['instagram']."||".//33
                    $row['logo_ruta']."||".//34
                    $row['num_poder']."||".//35
                    $row['fecha_poder']."||".//36
                    $row['notario']."||".//37
                    $row['folio_mercantil']."||".//38
                    $row['extension']."||"; //39
        }
        $contArr = array();
        while($row2 = mysqli_fetch_assoc($result2)){
          $contArr[] = array($row2['contacto_id'],$row2['nombre'],$row2['apellidos'],$row2['puesto'],$row2['telefono'],$row2['tel_movil'],$row2['email'],$row2['puesto_id'],$row2['departamento_id'],$row2['puestoPref'],$row2['departamento'],$row2['titulo_id']);
        }
        $salida .= json_encode($contArr);//40
      }
      
      if(2==$option){
        while($row = mysqli_fetch_assoc($result)){
          $salida = "OK||".
                    $row['nombre_comercial']."||".
                    $row['telefono']."||".
                    $row['domicilio']." ".$row['num_exterior']." ".$row['num_interior']."||".
                    $row['colonia']."||".
                    $row['cp']."||".
                    $row['municipio']."||".
                    $row['estado']."||".
                    $row['descripcion']."||".
                    $row['calle1']."||".
                    $row['calle2']."||".
                    $row['latitud']."||".
                    $row['longitud']."||"; 
        }
        $contArr = array();
        while($row2 = mysqli_fetch_assoc($result2)){
          $contArr[] = array($row2['contacto_id'],$row2['nombre'],$row2['apellidos'],$row2['puesto'],$row2['telefono'],$row2['tel_movil'],$row2['email'],$row2['puesto_id'],$row2['departamento_id'],$row2['puestoPref'],$row2['departamento'],$row2['titulo_id']);
        }
        $salida .= json_encode($contArr);
      }                            
    }
  }
  prosDetailsa:
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}

//"borra" oculta un prospecto
if("deleteProspect" == $_POST["action"]){
  
  $prosId = isset($_POST['prosId']) ? $_POST['prosId'] : -1;
  $option = isset($_POST['option']) ? $_POST['option'] : -1;
  
  //$query = "DELETE FROM prospectos WHERE prospecto_id = ".$prosId;
  $query = "UPDATE prospectos SET activo = ".$option." WHERE prospecto_id = ".$prosId;
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $salida = "ERROR|| Ocurrio un error al intentar eliminar o restaurar este prospecto";
  }
  else{
    if(1==$option){
      writeOnJournal($usuarioId,"Ha eliminado un prospecto con el id ".$prosId);
      $salida = "OK|| El registro ha sido eliminado exitosamente";
    }
    else if(0==$option){
      writeOnJournal($usuarioId,"Ha restaurado un prospecto previamente eliminado con el id ".$prosId);
      $salida = "OK|| El registro ha sido restaurado exitosamente";
    }
    else{
      $salida = "ERROR|| Ocurrió un problema al recibir la petición";
    }
  }
  
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;  
}

//borra un prospecto
/*if("deleteProspect" == $_POST["action"]){
  
  $prosId = isset($_POST['prosId']) ? $_POST['prosId'] : -1;
  
  $query = "DELETE FROM prospectos WHERE prospecto_id=".$prosId;
  $query2 = "DELETE FROM calendario WHERE origen_id=".$prosId." AND tipo_origen=0";
  $query3 = "DELETE FROM cotizaciones WHERE origen_id=".$prosId." AND tipo_origen=0";
  $query4 = "DELETE FROM documentos WHERE origen_id=".$prosId." AND tipo_origen=0";
  
  $result = mysqli_query($db_modulos,$query);  

  if(!$result){
    $salida = "ERROR|| No se pudo eliminar este prospecto";
  }
  else{
    $result2 = mysqli_query($db_modulos,$query2);
    $result3 = mysqli_query($db_modulos,$query3);
    $result4 = mysqli_query($db_modulos,$query4);
    
    if(!$result2||!$result3||!$result4){
      $salida = "ERROR|| Se eliminó el prospecto, pero ocurrió un problema al eliminarse sus datos asociados";
      writeOnJournal($usuarioId,"Ha eliminado un prospecto con el id ".$prosId." pero no se eliminó sus eventos o cotizaciones");
    }
    else{
      writeOnJournal($usuarioId,"Ha eliminado un prospecto con el id ".$prosId);
      $salida = "OK|| El registro ha sido eliminado exitosamente";
    }
  } 
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;  
}*/

//elimina un comentario
/*if("deleteComment" == $_POST["action"]){
  
  $commId  = isset($_POST['commId']) ? $_POST['commId'] : -1;
  $orgType = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
  $fecha   = date("Y-m-d H:i:s");
  
  case 0:
      $orgName = "prospecto";
      $tableName = "comentarios_prospectos";
      $fieldName = "prospecto_id";
    break;
    case 1:
      $orgName = "cliente";
      $tableName = "comentarios_clientes";
      $fieldName = "cliente_id";
    break;
    case 2:
      $orgName = "sitio";
      $tableName = "comentarios_sitios";
      $fieldName = "sitio_id";
    break;
    case 3:
      $orgName = "evento";
      $tableName = "comentarios_eventos";
      $fieldName = "evento_id";
    break;
  }
  
  $query2 = "SELECT comentario,prospecto_id FROM ".$tableName." WHERE comentario_id=".$commId;
  $result2 = mysqli_query($db_modulos,$query2);
  if(!$result2){
    $salida = "ERROR|| Ocurrió un error al obtener el id del comentario";
  }
  else{
    $comToDelete = mysqli_fetch_assoc($result2);
    
    $query = "DELETE FROM ".$tableName." WHERE comentario_id=".$commId;
  
    $result = mysqli_query($db_modulos,$query);
    
    if(!$result){
      $salida = "ERROR|| Ocurrió un error al borrarse el comentario";
    }
    else{
      writeOnJournal($usuarioId,"Ha borrado el comentario con id ".$comToDelete[$fieldName]." que decía [".$comToDelete['comentario']."]");
      $salida = "OK|| El comentario se ha eliminado con éxito||".$comToDelete[$fieldName];
    }  
  }   
  
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}*/

}//fin else sesión

/*if("saveCommentEdit" == $_POST["action"]){
  
  $commId = isset($_POST['commId']) ? $_POST['commId'] : -1;
  $comment = isset($_POST['comment']) ? $_POST['comment'] : "";
  $fecha  = date("Y-m-d H:i:s");
  
  $query = "SELECT * FROM comentarios_prospectos WHERE comentario_id=".$commId;
  
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $salida = "ERROR|| Error al consultar los datos del comentario original";
  }
  else{
    $row = mysqli_fetch_assoc($result);
    
    $query2 = "INSERT INTO historial_comentarios (comentario_id,tipo_origen,origen_id,fecha_original,comentario_original) VALUES (".$row['comentario_id'].",1, ".$row['prospecto_id'].",'".$row['fecha_alta']."',".$row['comentario'].")";
    
    $result2 = mysqli_query($db_modulos,$query2);
    
    if(!$result2){
      $salida = "ERROR|| No se pudo guardar el comentario";
    }
    else{
      $query3 = "UPDATE comentarios_prospectos SET (prospecto_id,comentario,fecha_alta,usuario_alta) VALUES (".$prosId.",'".$comment."','".$fecha."',".$usuarioId.")";
      
      $result3 = mysqli_query($db_modulos,$query3);
      
      if(!$result3){
        $salida = "ERROR|| Error al guardarse el comentario ";
      }
      else{
        writeOnJournal($usuarioId,"Ha editado el comentario con id [".$commId."]");
        $salida = "OK|| El comentario se ha editado correctamente";
      }   
    }
  }   
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}*/

?>
