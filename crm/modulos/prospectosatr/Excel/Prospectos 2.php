<?php

require __DIR__ . "/vendor/autoload.php";
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

$documento = new Spreadsheet();
    $documento 
        ->getProperties()
        ->setCreator("Softernium")
        ->setDescription("Reporte de C R M")
        ->setTitle("Reporte");

    // $documento->getActiveSheet()->getColumnDimension('A')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('B')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('C')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('D')->setWidth(15);
    //     $documento->getActiveSheet()->getColumnDimension('E')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('F')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('G')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('H')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('I')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('J')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('K')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('L')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('M')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('N')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('O')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('P')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('Q')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('R')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('S')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('T')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('U')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('V')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('W')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('X')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('Y')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('Z')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('AA')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('AB')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('AC')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('AD')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('AE')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('AF')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('AG')->setWidth(25);
    //     $documento->getActiveSheet()->getColumnDimension('AH')->setWidth(25);

$nombreDelDocumento = "Reporte CRM Prospectos y Clientes.xlsx";

$documento->setActiveSheetIndex(0);
$documento->getActiveSheet()->setTitle('CRM Prospectos y Clientes');

/****************************************************************************************/
/****************************************************************************************/
/****************************************************************************************/
// $cfgServerLocationAbs = '/var/www/html/crm/';
// $cfgServerLocation = 'https://crm.coeficiente.mx/';  //el definitivo con dominio
// $cfgServerLocation = "http://10.1.0.109/";
//prubas lap andres
//valores para pruebas locales en las bases de datos de pruebas (comentar estos antes de subir los archivos al server)
// $cfgTableNameModP = 'coecrm_modulos';
// $cfgTableNameUsrP = 'coecrm_usuarios';
// $cfgTableNameCatP = 'coecrm_catalogos';
// $cfgTableNameOmnP = 'bdOMNI';
// $cfgDbServerP['location'] = '10.1.0.20';
// $cfgDbServerP['user'] = 'desarrollo';
// $cfgDbServerP['pass'] = 'Pa55w0rd!crm';
$cfgTableNameModP = 'coecrm_modulos';
$cfgTableNameUsrP = 'coecrm_usuarios';
$cfgTableNameCatP = 'coecrm_catalogos';
$cfgTableNameOmnP = 'bdOMNI';
$cfgDbServerP['location'] = '10.1.0.109';
$cfgDbServerP['user'] = 'supervisor';
$cfgDbServerP['pass'] = 'Kb.204.h32017';
//Opciones de la conexión base de pruebas
$opcionesP = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
try {
    $conP = new PDO('mysql:host=' . $cfgDbServerP['location'] . ';dbname=' . $cfgTableNameCatP, $cfgDbServerP['user'], $cfgDbServerP['pass'], $opcionesP);
    //  echo 'conexion esitosa pruebas';
    //  echo "\n";
    /****************************************************************************************/
    /****************************************************************************************/
    /****************************************************************************************/
    $cont = 2;
    $posicionesC = ['cliente_id', 'razon_social', 'nombre_comercial', 'rfc_curp', 'tipo_persona', 'telefono', 'tel_movil', 'email', 'email_alt', 'estatus', 'origen', 'origen_otro', 'acta_constitutiva', 'fecha_acta', 'representante_id', 'domicilio_fiscal', 'colonia_fiscal', 'cp_fiscal', 'municipio_fiscal', 'estado_fiscal', 'fecha_alta', 'usuario_alta', 'descripcion'];
    $posiciones = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH'];

    for ($i = 0; $i < count($posicionesC); $i++) {
        $documento->getActiveSheet()->setCellValue($posiciones[$i] . $cont, $posicionesC[$i])->getStyle($posiciones[$i] . $cont)->getFont()->setBold(true)->setSize(14);
        $documento->getActiveSheet()->getStyle($posiciones[$i] . $cont)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
    }
    $cont++;
    $clientes = $conP->prepare("SELECT * FROM coecrm_modulos.clientes");
    $clientes->execute();
    foreach ($clientes as $clien) {
        for ($i = 0; $i < count($posicionesC); $i++) {
            $documento->getActiveSheet()->setCellValue($posiciones[$i] . $cont, $clien[$posicionesC[$i]]);
        }
        $cont++;
    }
    //*********************************************************************************/
    //*********************************************************************************/
    $posicionesN = ['prospecto_id', 'razon_social', 'rfc', 'tipo_persona', 'nombre_comercial', 'telefono', 'extension', 'tel_movil', 'domicilio', 'colonia', 'cp', 'municipio', 'estado', 'email', 'origen', 'origen_otro', 'estatus', 'asignadoa', 'fecha_alta', 'usuario_alta', 'descripcion', 'giro_id', 'representante_id', 'fecha_actualizacion', 'admin_auth', 'facebook', 'twitter', 'instagram', 'logo_ruta', 'activo', 'num_poder', 'fecha_poder', 'notario', 'folio_mercantil'];
    $posiciones = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH'];
    $cont += 3;
    for ($i = 0; $i < count($posicionesN); $i++) {
        $documento->getActiveSheet()->setCellValue($posiciones[$i] . $cont, $posicionesN[$i])->getStyle($posiciones[$i] . $cont)->getFont()->setBold(true)->setSize(14);
        $documento->getActiveSheet()->getStyle($posiciones[$i] . $cont)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
    }
    $cont++;
    $prospectos = $conP->prepare("SELECT * from " . $cfgTableNameModP . ".prospectos ");
    $prospectos->execute();
    // $cont += 3;
    foreach ($prospectos as $prosp) {
        if ($prosp['razon_social'] != null && $prosp['nombre_comercial'] != null) {
            for ($i = 0; $i < count($posicionesN); $i++) {
                if ($posicionesN[$i] == 'giro_id') {
                    $giro = $conP->prepare("SELECT giro_descripcion from coecrm_catalogos.cat_giro_comercial WHERE giro_id = " . $prosp[$posicionesN[$i]]);
                    $giro->execute();
                    foreach ($giro as $gir) {
                        // print_r($gir['giro_descripcion']);
                        $documento->getActiveSheet()->setCellValue($posiciones[$i] . $cont, $gir['giro_descripcion']);
                    }
                } elseif ($posicionesN[$i] == 'representante_id') {
                    $documento->getActiveSheet()->setCellValue($posiciones[$i] . $cont, $prosp[$posicionesN[$i]]);
                } else {
                    $documento->getActiveSheet()->setCellValue($posiciones[$i] . $cont, $prosp[$posicionesN[$i]]);
                }
            }
            $cont++;
        } else { }
    }




    
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $nombreDelDocumento . '"');
    header('Cache-Control: max-age=0');
    $writer = IOFactory::createWriter($documento, 'Xlsx');
    $writer->save('php://output');
    exit;

} catch (PDOException $e) {
    print "¡Error!: " . $e->getMessage() . "<br/>";
    die();
}
