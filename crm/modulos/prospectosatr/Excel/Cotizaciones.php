<?php

date_default_timezone_set('America/Mexico_City');
require __DIR__ . "/vendor/autoload.php";

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

$documento = new Spreadsheet();
$documento
    ->getProperties()
    ->setCreator("Softernium")
    ->setDescription("Reporte de C R M")
    ->setTitle("Reporte");
$fechadoc = date('Y-m-d H:i:s');
$nombreDelDocumento = "Reporte CRM Cotizaciones - " . $fechadoc . ".xlsx";
$documento->setActiveSheetIndex(0);
$documento->getActiveSheet()->setTitle('CRM Cotizaciones');


$index = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
$index2 = [];
$index2 = $index;
for ($i = 0; $i < count($index); $i++) {
    for ($j = 0; $j < count($index); $j++) {
        array_push($index2, $index[$i] . $index[$j]);
    }
}
/****************************************************************************************/

/****************************************************************************************/
//prubas lap andres
//valores para pruebas locales en las bases de datos de produccion 
// $cfgTableNameModP = 'coecrm_modulos';
// $cfgTableNameUsrP = 'coecrm_usuarios';
// $cfgTableNameCatP = 'coecrm_catalogos';
// $cfgTableNameOmnP = 'bdOMNI';
// $cfgDbServerP['location'] = '10.1.0.20';
// $cfgDbServerP['user'] = 'desarrollo';
// $cfgDbServerP['pass'] = 'Pa55w0rd!crm';

$cfgTableNameModP = 'coecrm_modulos';
$cfgTableNameUsrP = 'coecrm_usuarios';
$cfgTableNameCatP = 'coecrm_catalogos';
$cfgTableNameOmnP = 'bdOMNI';
$cfgDbServerP['location'] = '10.1.0.109';
$cfgDbServerP['user'] = 'supervisor';
$cfgDbServerP['pass'] = 'Kb.204.h32017';
//Opciones de la conexión base de pruebas
$opcionesP = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
try {
    $conP = new PDO('mysql:host=' . $cfgDbServerP['location'] . ';dbname=' . $cfgTableNameUsrP, $cfgDbServerP['user'], $cfgDbServerP['pass'], $opcionesP);
    /****************************************************************************************/

    /****************************************************************************************/
    $cont = 1;
    //***************************************************************************************/

    //***************************************************************************************/
    // $encabezados2 = ['ID cotizacion','Ejecutivo o Asignado a','Region','Nombre de cliente','cotizacion','Subtotal','total','notas','Estado Cotizacion'];
    $encabezados2 = ['ID cotizacion','Ejecutivo o Asignado a','Region','Nombre de cliente','Subtotal','total','notas','Estado Cotizacion'];

    for ($i = 0; $i < count($encabezados2); $i++) {
        $colt = $i;
        $documento->getActiveSheet()->setCellValue($index2[$i] . $cont, $encabezados2[$i])->getStyle($index2[$i] . $cont)->getFont()->setBold(true)->setSize(14);
        $documento->getActiveSheet()->getStyle($index2[$i] . $cont)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        if($encabezados2[$i] == 'notas'){
            $documento->getActiveSheet()->getColumnDimension($index2[$i])->setWidth(60);
        }else{
            $documento->getActiveSheet()->getColumnDimension($index2[$i])->setAutoSize(true);
        }
        $documento->getActiveSheet()->getStyle($index2[$i] . '1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('b3b3b3');
    }
    $cont++;
    $encabezados = ['cotizacion_id', 'nombre', 'ubicacion_nombre', 'nombre_comercial', 'subtotal', 'total', 'notas', 'estatus'];
        //     $cfgTableNameModP = Base de Datos modulos
        //     $cfgTableNameCatP = Base de Datos catalogos
        //     $cfgTableNameUsrP = Base de Datos usuarios
    $query = "SELECT usr.nombre, usr.apellidos, cest.estatus,ubi.ubicacion_nombre, pros.nombre_comercial, cot.subtotal, cot.total, cot.notas, cot.cotizacion_id ".
    "FROM coecrm_modulos.prospectos as pros, ".
    "coecrm_modulos.cotizaciones as cot, ".
    "coecrm_usuarios.usuarios as usr, ".
    "coecrm_catalogos.cat_cotizacion_estatus as cest, ".
    "coecrm_catalogos.cat_coe_ubicacion as ubi ".
    "WHERE pros.prospecto_id = cot.origen_id ".
    "AND cot.region_id = ubi.ubicacion_id ".
    "AND cot.cotizacion_estatus_id = cest.cotizacion_estatus_id ".
    "AND cot.usuario_alta = usr.usuario_id ".
    "AND subtotal > 0 ".
    "AND region_id > 0;";
    $stmt = $conP->prepare($query);
    $stmt->execute();
    // echo $query;
    foreach ($stmt as $row) {
    // conoces el nombre del atributo
    //     foreach ($row as $atr) {
    //         echo key($row) . '<br />';
    //         next($row);
    //     }
        $row['nombre'] = $row['nombre']. " " . $row['apellidos'];
        for ($i = 0; $i < count($encabezados); $i++) {
            $documento->getActiveSheet()->setCellValue($index2[$i] . $cont, $row[$encabezados[$i]]);
        }
        $cont++;
    }
    //fin del foreach

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $nombreDelDocumento . '"');
    header('Cache-Control: max-age=0');
    $writer = IOFactory::createWriter($documento, 'Xlsx');
    $writer->save('php://output');
    exit;

} catch (PDOException $e) {
    print "¡Error!: " . $e->getMessage() . "<br/>";
    die();
}
