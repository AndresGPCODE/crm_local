<?php
// session_start();
// set_time_limit(0);
// header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');
// include_once "../../../libs/db/common.php";
require __DIR__ . "/vendor/autoload.php";
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
$documento = new Spreadsheet();
    $documento 
        ->getProperties()
        ->setCreator("Softernium")
        ->setDescription("Reporte de C R M")
        ->setTitle("Reporte");
$fechadoc = date('Y-m-d H:i:s');
$nombreDelDocumento = "Reporte CRM Prospectos Atrasados - ". $fechadoc.".xlsx";
$documento->setActiveSheetIndex(0);
$documento->getActiveSheet()->setTitle('CRM Prospectos Atrasados');

// $encabezados = ['prospecto_id', 'nombre', 'municipio', 'razon_social', 'fecha_alta', 'fecha_actualizacion', 'origen', 'descripcion', 'estatus', 'admin_auth', 'representante_id', 'estatus_id', 'origen', 'origen_otro', 'asignadoa',  'nombre_comercial', 'telefono', 'facebook', 'twitter', 'instagram', 'activo', 'venta_estatus_id',  'porcentaje', 'orden', 'tiempo_limite', 'li_label', 'color', 'icono', 'bgcolor', 'visible', 'telefono3', 'ubicacion_nombre', 'staName', 'venta'];
$encabezados = ['prospecto_id', 'nombre', 'ubicacion_nombre', 'nombre_comercial', 'fecha_alta', 'fecha_actualizacion', 'venta', 'descripcion', 'estatus'];

$index = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
$index2 = [];
$index2 = $index;
for ($i = 0; $i < count($index); $i++) {
    for ($j = 0; $j < count($index); $j++) {
        array_push($index2, $index[$i] . $index[$j]);
    }
}
/****************************************************************************************/

/****************************************************************************************/
//prubas lap andres
//valores para pruebas locales en las bases de datos de produccion 
// $cfgTableNameModP = 'coecrm_modulos';
// $cfgTableNameUsrP = 'coecrm_usuarios';
// $cfgTableNameCatP = 'coecrm_catalogos';
// $cfgTableNameOmnP = 'bdOMNI';
// $cfgDbServerP['location'] = '10.1.0.20';
// $cfgDbServerP['user'] = 'desarrollo';
// $cfgDbServerP['pass'] = 'Pa55w0rd!crm';

$cfgTableNameModP = 'coecrm_modulos';
$cfgTableNameUsrP = 'coecrm_usuarios';
$cfgTableNameCatP = 'coecrm_catalogos';
$cfgTableNameOmnP = 'bdOMNI';
$cfgDbServerP['location'] = '10.1.0.109';
$cfgDbServerP['user'] = 'supervisor';
$cfgDbServerP['pass'] = 'Kb.204.h32017';
//Opciones de la conexión base de pruebas
$opcionesP = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
try {
    $conP = new PDO('mysql:host=' . $cfgDbServerP['location'] . ';dbname=' . $cfgTableNameUsrP, $cfgDbServerP['user'], $cfgDbServerP['pass'], $opcionesP);
    /****************************************************************************************/

    /****************************************************************************************/
    $cont = 1;
    //***************************************************************************************/

    //***************************************************************************************/
    // $encabezados2 = ['ID', 'Ejecutivo o Asignado a', 'Municipio', 'Nombre de cliente', 'fecha de captura', 'fecha de ultimo cambio', 'Origen prospecto', 'Observaciones', 'Estatus y Etapa'];
    $encabezados2 = ['ID', 'Ejecutivo o Asignado a', 'Region', 'Nombre de cliente', 'fecha de captura', 'fecha de ultimo cambio', 'Origen prospecto', 'Observaciones', 'Estatus y Etapa', 'Notas'];
    for ($i = 0; $i < count($encabezados2); $i++) {
        $colt = $i;
        // echo $index2[$i] . $cont, " ".$encabezados[$i] . "<br>";
        $documento->getActiveSheet()->setCellValue($index2[$i] . $cont, $encabezados2[$i])->getStyle($index2[$i] . $cont)->getFont()->setBold(true)->setSize(14);
        $documento->getActiveSheet()->getStyle($index2[$i] . $cont)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        // $documento->getActiveSheet()->getColumnDimension($index2[$i])->setWidth(30);
        $documento->getActiveSheet()->getColumnDimension($index2[$i])->setAutoSize(true);
        $documento->getActiveSheet()->getStyle($index2[$i] . '1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('b3b3b3');
    }
    // $cont++;
    // $documento->getActiveSheet()->setCellValue($index2[$colt+1] . $cont, 'Notas')->getStyle($index2[$colt+1] . $cont)->getFont()->setBold(true)->setSize(14);
    // $documento->getActiveSheet()->getStyle($index2[$colt+1] . $cont)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
    $cont++;
    $query = "SELECT pr.prospecto_id, pr.razon_social, pr.admin_auth, pr.municipio, pr.representante_id, pr.estatus AS estatus_id, pr.origen, pr.origen_otro, pr.asignadoa, usr.nombre, usr.apellidos, pr.descripcion, pr.nombre_comercial,pr.telefono,pr.fecha_alta, pr.fecha_actualizacion, pr.facebook, pr.twitter, pr.instagram, pr.activo, vSt.*, dir.telefono3, ubi.ubicacion_nombre, cvs.estatus AS staName, cvo.venta " .
        "FROM " . $cfgTableNameModP . ".prospectos AS pr, " .
        $cfgTableNameModP . ".direccion_detalles AS dir, " .
        $cfgTableNameCatP . ".cat_venta_estatus AS vSt, " .
        $cfgTableNameUsrP . ".usuarios AS usr, " .
        $cfgTableNameCatP . ".cat_coe_ubicacion AS ubi, " .
        $cfgTableNameCatP . ".cat_venta_estatus AS cvs, " .
        $cfgTableNameCatP . ".cat_venta_origen AS cvo " .
        "WHERE pr.estatus >= 1" .
        " AND vSt.venta_estatus_id = pr.estatus" .
        " AND dir.origen_id = pr.prospecto_id" .
        " AND dir.tipo_origen = 0" .
        " AND usr.usuario_id = pr.asignadoa" .
        " AND ubi.ubicacion_id = usr.ubicacion_id" .
        " AND cvs.venta_estatus_id = pr.estatus" .
        " AND pr.estatus > 0" .
        " AND cvo.venta_origen_id = pr.origen order by pr.prospecto_id ";
        // echo $query;
    $stmt = $conP->prepare($query);
    $stmt->execute();
       foreach ($stmt as $row) {
        //conoces el nombre del atributo
        // foreach ($row as $atr) {
        //     echo key($row) . '<br />';
        //     next($row);
        // }

        $razSoc = ($row["razon_social"] != "") ? $row["razon_social"] : "<i>Sin Dato</i>";
        $fecNow = date('Y-m-d H:i:s');
        $fecLim = date('Y-m-d H:i:s', strtotime($row['fecha_actualizacion'] . "+" . $row['tiempo_limite'] . " hours"));
        $fecDif = round((strtotime($fecLim) - strtotime($fecNow)) / 3600);
        $fecThi = (int) $row['tiempo_limite'] / 3;
        $row['nombre'] = $row['nombre'] . " " . $row['apellidos'];

        $row['fecha_alta'] = explode(' ', $row['fecha_alta']);
        $row['fecha_alta'] = $row['fecha_alta'][0]; 

        $row['fecha_actualizacion'] = explode(' ', $row['fecha_actualizacion']);
        $row['fecha_actualizacion'] = $row['fecha_actualizacion'][0];

        if (1 != $row['activo']) {
            // for ($i = 0; $i < count($row)/2; $i++){
            //     echo $row[$i] . " <br>";
            // }
            // var_dump($row);

            if ($fecDif <= 0) {
                for ($i = 0; $i < count($encabezados); $i++) {
                    $col = $i;
                    $prosId = $row['prospecto_id'];
                    // echo $index2[$i] . $cont, $row[$encabezados[$i]]."<br>";
                    // $nombre = get_userRealName($row['asignadoa']);

                    $documento->getActiveSheet()->setCellValue($index2[$i] . $cont, $row[$encabezados[$i]]);
                }
                // buscamos los comentarios de este prospecto
                $querycomment = "SELECT * FROM coecrm_modulos.comentarios_prospectos where prospecto_id = $prosId";
                $stmtc = $conP->prepare($querycomment);
                $stmtc->execute();;
                $comment = "";
                if ($stmtc->rowCount() > 0) {
                    foreach ($stmtc as $rowc) {
                        $comment .= $rowc['comentario'] . ", ||";
                    }
                    $documento->getActiveSheet()->setCellValue($index2[$col + 1] . $cont, $comment);
                } else {
                    $documento->getActiveSheet()->setCellValue($index2[$col + 1] . $cont, 'No tiene ningun comentario');
                }
                // cambiamos de esta fila a la siguiente
                $cont++;
            }
            
        }
         
    }
    //fin del foreach
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    // header('Content-Type: application/xls');
    header('Content-Disposition: attachment;filename="' . $nombreDelDocumento . '"');
    header('Cache-Control: max-age=0');
    $writer = IOFactory::createWriter($documento, 'Xlsx');
    $writer->save('php://output');
    exit;
    } catch (PDOException $e) {
        print "¡Error!: " . $e->getMessage() . "<br/>";
        die();
    }
