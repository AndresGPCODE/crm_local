<?php
require __DIR__."excel/vendor/autoload.php";

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Security\XmlScanner;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;



$documento = new Spreadsheet();
$documento
    ->getProperties()
    ->setCreator("Softernium")
    ->setDescription("Reporte de C R M")
    ->setTitle("Reporte");
$documento->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $documento->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $documento->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $documento->getActiveSheet()->getColumnDimension('D')->setWidth(20);
    $documento->getActiveSheet()->getColumnDimension('E')->setWidth(20);

$nombreDelDocumento = "Reporte CRM Prospectos y Clientes.xlsx";
$documento->setActiveSheetIndex(0);
$documento->getActiveSheet()->setTitle('CRM Prospectos y Clientes');

$encabezados =['prospecto_id','razon_social','admin_auth','representante_id','estatus_id','origen','origen_otro','asignadoa','descripcion','nombre_comercial','telefono','fecha_alta','fecha_actualizacion','facebook','twitter','instagram','activo','venta_estatus_id','estatus','porcentaje','orden','tiempo_limite','li_label','color','icono','bgcolor','visible','telefono3','ubicacion_nombre','staName','venta'];
$index = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
$index2 = [];
$index2 = $index;
for ($i=0; $i<count($index); $i++){
    for ($j = 0; $j < count($index); $j++) { 
        array_push($index2, $index[$i]. $index[$j]);
    }     
}
// for ($i=0; $i<count($index2); $i++){
//     echo $index2[$i] . "<br>";
// }
//prubas lap andres
//valores para pruebas locales en las bases de datos de pruebas (comentar estos antes de subir los archivos al server)
// $cfgTableNameModP = 'coecrm_modulos';
// $cfgTableNameUsrP = 'coecrm_usuarios';
// $cfgTableNameCatP = 'coecrm_catalogos';
// $cfgTableNameOmnP = 'bdOMNI';
// $cfgDbServerP['location'] = '10.1.0.20';
// $cfgDbServerP['user'] = 'desarrollo';
// $cfgDbServerP['pass'] = 'Pa55w0rd!crm';

//conexion base de datos produccion
$cfgTableNameModP = 'coecrm_modulos';
$cfgTableNameUsrP = 'coecrm_usuarios';
$cfgTableNameCatP = 'coecrm_catalogos';
$cfgTableNameOmnP = 'bdOMNI';
$cfgDbServerP['location'] = '10.1.0.109';
$cfgDbServerP['user'] = 'supervisor';
$cfgDbServerP['pass'] = 'Kb.204.h32017';
//Opciones de la conexión base de pruebas
$opcionesP = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
try {
    $conP = new PDO('mysql:host=' . $cfgDbServerP['location'] . ';dbname=' . $cfgTableNameUsrP, $cfgDbServerP['user'], $cfgDbServerP['pass'], $opcionesP);
    $cont = 2;
    for ($i = 0; $i < count($encabezados); $i++) {
        // echo $index2[$i] . $cont, " ".$encabezados[$i] . "<br>";
    $documento->getActiveSheet()->setCellValue($index2[$i] . $cont, $encabezados[$i])->getStyle($index2[$i] . $cont)->getFont()->setBold(true)->setSize(14);
    $documento->getActiveSheet()->getStyle($index2[$i] . $cont)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
    }
    $cont++;
    $query = "SELECT pr.prospecto_id, pr.razon_social, pr.admin_auth, pr.representante_id, pr.estatus AS estatus_id, pr.origen, pr.origen_otro, pr.asignadoa, pr.descripcion, pr.nombre_comercial,pr.telefono,pr.fecha_alta, pr.fecha_actualizacion, pr.facebook, pr.twitter, pr.instagram, pr.activo, vSt.*, dir.telefono3, ubi.ubicacion_nombre, cvs.estatus AS staName, cvo.venta " .
        "FROM " . $cfgTableNameModP . ".prospectos AS pr, " .
        $cfgTableNameModP . ".direccion_detalles AS dir, " .
        $cfgTableNameCatP . ".cat_venta_estatus AS vSt, " .
        $cfgTableNameUsrP . ".usuarios AS usr, " .
        $cfgTableNameCatP . ".cat_coe_ubicacion AS ubi, " .
        $cfgTableNameCatP . ".cat_venta_estatus AS cvs, " .
        $cfgTableNameCatP . ".cat_venta_origen AS cvo " .
        "WHERE pr.estatus >= 1" .
        " AND vSt.venta_estatus_id = pr.estatus" .
        " AND dir.origen_id = pr.prospecto_id" .
        " AND dir.tipo_origen = 0" .
        " AND usr.usuario_id = pr.usuario_alta" .
        " AND ubi.ubicacion_id = usr.ubicacion_id" .
        " AND cvs.venta_estatus_id = pr.estatus" .
        " AND pr.estatus > 0" .
        " AND cvo.venta_origen_id = pr.origen order by pr.prospecto_id limit 1";
    $stmt = $conP->prepare($query);
    $stmt->execute();
    foreach ($stmt as $row) {
        //conoces el nombre del atributo
        // foreach ($row as $atr) {
        //     echo key($row) . '<br />';
        //     next($row);
        // }
        
        for ($i = 0; $i < count($encabezados); $i++) {
            echo $index2[$i] . $cont, $row[$encabezados[$i]]."<br>";
            // $documento->getActiveSheet()->setCellValue($index2[$i] . $cont, $row[$encabezados[$i]]);
        }
        $cont++;
    }
    //fin del foreach


    //*********************************************************************************/
    //*********************************************************************************/

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $nombreDelDocumento . '"');
    header('Cache-Control: max-age=0');
    // $writer = IOFactory::createWriter($documento, 'Xlsx');
    $writer = new Xlsx($documento);
    $writer->save('php://output');
    exit;
} catch (PDOException $e) {
    print "¡Error!: " . $e->getMessage() . "<br/>";
    die();
}


