<?php
session_start();
set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

include_once "../../libs/db/dbcommon.php";
include_once "../../libs/db/common.php";

$gruposArr  = $_SESSION['grupos'];
$usuarioId  = $_SESSION['usrId'];
$usuario    = $_SESSION['usuario'];
$nombre     = $_SESSION['usrNombre'];

$modId = 3;

if(!verifySession($_SESSION)){
  logoutTimeout();
}else{

$db_modulos = condb_modulos();
$db_catalogos = condb_catalogos();
$db_usuarios = condb_usuarios();

$salida = "";

if("getEventFilters"==$_POST['action']){
  if(hasPermission($modId,'r')||hasPermission($modId,'l')){
    $query = "SELECT * FROM cat_tipo_eventos";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
      $salida = "ERROR||Ocurrió un problema al obtener las categorías de eventos para su filtrado";  
    }
    else{
      while($row=mysqli_fetch_assoc($result)){
        if(0!=$row['tipo_evento_id']){
          $salida .= "<span class=\"spn_filtercolor\" style=\"background-color:#".$row['color']."\">&nbsp; &nbsp; &nbsp; </span>&nbsp; ".
                     "<label class=\"checkbox-inline\">".
                     "  <input type=\"checkbox\" class=\"chk_filters\" id=\"chk_eventType_".$row['tipo_evento_id']."\" value=".$row['tipo_evento_id']." onchange=\"execFilter(this,0)\">".$row['tipo_evento'].
                     "</label>&nbsp;&nbsp;&nbsp;<br><br>";
        }     
      }
      $salida .="<br>";
    }
  }
  else{
    $salida = "<b>ERROR:</b> No cuenta con permisos para acceder a estos datos";
  }  
    mysqli_close($db_catalogos);
    echo $salida;
}

if("getEventFiltersByUser"==$_POST['action']){
  if(hasPermission($modId,'r')||hasPermission($modId,'l')){
    $query = "SELECT * FROM usuarios WHERE usuario_id>0";
    
    $result = mysqli_query($db_usuarios,$query);
    
    if(!$result){
      $salida = "ERROR||Ocurrió un problema al obtener las los usuarios para su filtrado";  
    }
    else{
      $salida = "<label class=\"checkbox-inline\" align=\"left\">".
                "  <input type=\"checkbox\" class=\"chk_filtersUser\" id=\"chk_eventSelectAllUsers\" value=\"\" onchange=\"eveSelectAllUsers(this)\">Seleccionar Todos".
                "</label><br><br>";
                
      $salida .= "<table id=\"tbl_userEventFilter\" class=\"table table-striped small\">\n".
                 "  <thead>\n".
                 "    <tr>\n".
                 "      <th>Nombre</th>\n".
                 "      <th>Selección</th>\n".
                 "    </tr>\n".
                 "  </thead>\n".
                 "  <tbody>\n";
      while($row=mysqli_fetch_assoc($result)){
        $salida .= "    <tr>".
                   "      <td><a href=/crm/modulos/perfil/index.php?id='".encrypt($row['usuario_id'])."'>".$row['nombre']." ".$row['apellidos']."</a></td>\n".
                  // "      <td>".$row['nombre']." ".$row['apellidos']."</td>".
                   "      <td>".
                   "        <input type=\"checkbox\" class=\"chk_filtersUser\" id=\"chk_eventUser_".$row['usuario_id']."\" value=".$row['usuario_id']." onchange=\"execFilter(this,1)\" checked>".
                   "      </td>".
                   "    </tr>";    
      }
      $salida .= "  </tbody>\n".
                 "  <tfooter></tfooter>".
                 "</table>";
    }
  }
  else{
    $salida = "<b>ERROR:</b> No cuenta con permisos para acceder a estos datos";
  } 
  mysqli_close($db_usuarios);
  echo $salida;
}
}//fin else sesión
?>
