<?php
session_start();
set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

include_once "../../libs/db/dbcommon.php";
include_once "../../libs/db/common.php";

$gruposArr  = $_SESSION['grupos'];
$usuarioId  = $_SESSION['usrId'];
$usuario    = $_SESSION['usuario'];
$nombre     = $_SESSION['usrNombre'];

$modId = 3;

if(!verifySession($_SESSION)){
  logoutTimeout();
}else{

$db_modulos = condb_modulos();
$db_usuarios = condb_usuarios();
$db_catalogos = condb_modulos();

$salida = "";

$filterId = isset($_GET['filterId']) ? $_GET['filterId'] : -1;
$userId   = isset($_GET['userId']) ? $_GET['userId'] : -1;
$action   = isset($_GET['action']) ? $_GET['action'] : "loadcalendar";

if((hasPermission($modId,'r'))||(hasPermission($modId,'l'))){

  if("loadcalendar" == $action){
    $query = "SELECT cal.*, typ.color, sta.estatus FROM ".$cfgTableNameMod.".calendario AS cal, ".$cfgTableNameCat.".cat_tipo_eventos AS typ, ".$cfgTableNameCat.".cat_evento_estatus AS sta WHERE cal.tipo_evento_id=typ.tipo_evento_id AND cal.estatus_id=sta.evento_estatus_id";
  }
  if("loadfilter" == $action){
    $query = "SELECT cal.*, typ.color, sta.estatus FROM ".$cfgTableNameMod.".calendario AS cal, ".$cfgTableNameCat.".cat_tipo_eventos AS typ, ".$cfgTableNameCat.".cat_evento_estatus AS sta WHERE cal.tipo_evento_id=typ.tipo_evento_id AND cal.estatus_id=sta.evento_estatus_id AND typ.tipo_evento_id=".$filterId;
  } 
  if("loadfilterbyuser" == $action){
    $query = "SELECT cal.*, typ.color, sta.estatus FROM ".$cfgTableNameMod.".calendario AS cal, ".$cfgTableNameCat.".cat_tipo_eventos AS typ, ".$cfgTableNameCat.".cat_evento_estatus AS sta WHERE cal.tipo_evento_id=typ.tipo_evento_id AND cal.estatus_id=sta.evento_estatus_id AND cal.usuario_id=".$filterId;
  }

  $result = mysqli_query($db_modulos,$query);

  if(!$result){
    $salida = "ERROR||No se pudo recabar los eventos del calendario";
  }
  else{
    $exArr = array();
    while ($row = mysqli_fetch_assoc($result)){
    
      $start = explode(" ",$row['fecha_inicio']);      
      $end = explode(" ",$row['fecha_fin']);
      
      //if(($usuarioId == $row['usuario_id'])||(array_key_exists(1, $gruposArr)))
      if(hasPermission($modId,'a')||hasPermission($modId,'e')||(hasPermission($modId,'n')&&$row['usuario_id']==$usuarioId))
        $modify = 1;
      else
        $modify = 0;
      
      //if(array_key_exists(1, $gruposArr))
      if(hasPermission($modId,'t')||hasPermission($modId,'d'))
        $delete = 1;
      else
        $delete = 0;
        
      $invArray = explode(",",substr($row['invitados'],1,-1));      
      $equArray = explode(",",substr($row['equipo'],1,-1));
      
      $invFields = "";
      $invNoConfFields = "";
      $equFields = "";
      $invCancelFields = "";
      
      foreach($invArray as $key1 => $value1){
        $idInv = str_replace("!","",$value1);
        $idInv = str_replace("*","",$idInv);
        
        $queryInv = "SELECT usuario_id,nombre,apellidos FROM ".$cfgTableNameUsr.".usuarios WHERE usuario_id=".$idInv;
        $resultInv = mysqli_query($db_usuarios,$queryInv);
        if(!$resultInv){
          error_log("Error al consultar invitado ".$queryInv);
        }
        else{
          $rowInv = mysqli_fetch_assoc($resultInv);
          
          if(false!==strpos($value1,"!"))
            $invNoConfFields .= $rowInv['nombre']." ".$rowInv['apellidos']."<br>";
          elseif(false!==strpos($value1,"*"))
            $invCancelFields .= $rowInv['nombre']." ".$rowInv['apellidos']."<br>";
          else
            $invFields .= $rowInv['nombre']." ".$rowInv['apellidos']."<br>";
        }
      }
      error_log("equArr: ".print_r($equArray,true));
      
      foreach($equArray as $key2 => $value2){
        $queryEqu = "SELECT equipo_id,categoria_id,descripcion,num_serie FROM ".$cfgTableNameCat.".cat_mobiliario_equipo WHERE equipo_id=".$value2;
        $resultEqu = mysqli_query($db_catalogos,$queryEqu);
        if(!$resultEqu){
          error_log("Error al consultar equipo ".$queryEqu);
        }
        else{
          $rowEqu = mysqli_fetch_assoc($resultEqu);
          $equFields .= $rowEqu['descripcion']." ".$rowEqu['num_serie']."<br>";
        }
      }
                
      $exArr[] = array("id"              => $row['id'],
                       "typeId"          => $row['tipo_evento_id'],
                       "title"           => $row['titulo'],
                       "start"           => $start[0].' '.$start[1],
                       "end"             => $end[0].' '.$end[1],
                       "modify"          => $modify,
                       "delete"          => $delete,
                       "user"            => get_userRealName($row['usuario_id']),
                       "tip"             => $row['descripcion'],
                       "description"     => $row['descripcion'],
                       "backgroundColor" => "#".$row['color'],
                       "status"          => $row['estatus'],
                       "allDay"          => intval($row['todo_dia']),
                       "guests"          => $invFields,
                       "inv"             => $invNoConfFields,
                       "cancel"          => $invCancelFields,
                       "equipment"       => $equFields);
    }
    $salida = json_encode($exArr,false);
  }
}
else{
  $salida = "<b>ERROR:</b> El usuario no cuenta con permisos necesarios";
}
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
   mysqli_close($db_catalogos);

  echo $salida; 
}//fin else sesión
?>
