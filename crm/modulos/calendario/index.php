<?php 
session_start();
set_time_limit(0);
include_once "../../libs/db/common.php";

if(!verifySession($_SESSION)){
 logoutTimeout();
}else{

header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

$title = "Calendario";
$usuario = $_SESSION['usuario'];
$gruposArr  = $_SESSION['grupos'];
$date = isset($_GET['date'])?$_GET['date']:"";

$modId = 3;
?>

<!DOCTYPE html>
<html>
  <head>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/AdminLTE2/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
    <link href="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.css" rel="stylesheet" type="text/css"/>  
    <link href="../../libs/js/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css"/>  
    <link href="../../libs/AdminLTE2/plugins/fullcalendar-2.3.0/fullcalendar.css" rel="stylesheet" type="text/css"/>
    
    <script src="../../libs/AdminLTE2/plugins/jQuery/jQuery-2.1.3.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/dist/js/app.js" type="text/javascript"></script>   
    <script src="../../libs/js/jqueryValidate/dist/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/fullcalendar-2.3.0/lib/moment.min.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/fullcalendar-2.3.0/fullcalendar.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/fullcalendar-2.3.0/lang/es.js" type="text/javascript"></script> 
    <script src="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script> 
    <script src="../../libs/js/jqueryValidate/dist/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.form.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery-ui-sliderAccess.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
    <script src="../../libs/js/common.js" type="text/javascript"></script>
    <script src="script.js" type="text/javascript"></script>
    
    <link rel="icon" type="image/png" href="../../img/favicon-16x16.png" sizes="16x16">
    
    <title>Coeficiente CRM - Calendario</title>
    <style type="text/css">
    </style>
  </head>  
  
  <body>
    <!--Div que contiene el encabezado-->
    <div class="container" id="header"><?php include("../../libs/templates/header.php")?></div>
    
    <div class="container-fluid">
      <h2 class="page-header" id="tituloPag">
        <span class="glyphicon glyphicon-calendar"></span> <?php echo $title ?>
      </h2>
    </div>
    
    <div class="container">
      <div class="row">
        <div id=div_msgAlert></div>
      </div>
    </div> <!-- /container -->
          
    <div class="container">
      <div class="col container-fluid">
      <?php if(hasPermission($modId,'w')){ ?>
          <button type="button" id="btn_eventNew" title="Nuevo Evento" class="btn btn-default" onClick="openNewEvent();"><span class="glyphicon glyphicon-asterisk"></span> Nuevo Evento</button><br>
      <?php } ?>              
      </div><br><br>
      <?php if((hasPermission($modId,'r'))||(hasPermission($modId,'l'))){ ?>       
      <div class="row">             
        <div class="col-lg-9 col-sm-9 col-xs-12" id="div_calendar" align="right"></div> 
        <div class="col-lg-3 col-sm-3 col-xs-12 div_filters" align="left">
          <br>
          <div>
            Filtrado por:<br>
            <label class="radio-inline"><input type="radio" name="rad_filterType" class="rad_filterType rad_responsive" id="rad_filterType_0" value=0>Tipos de eventos</label>
            <label class="radio-inline"><input type="radio" name="rad_filterType" class="rad_filterType rad_responsive" id="rad_filterType_1" value=1>Usuarios</label>
          </div><hr>
          <div id="div_filters"></div>
          <div id="div_filtersByUser"></div>         
        </div> 
        <input type="hidden" id="hdn_specDate" name="hdn_specDate" value="<?php echo $date?>">                
      </div>  
      <?php } ?>       
    </div> <!-- /container -->    
    
    <!--_Cuadro de dialogo de eventos de prospecto -->
    <div id="mod_eventDetails" class="modal fade" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Detalles de Evento</h4>
          </div>
          <div class="container-fluid">
            <div id=div_msgAlertEventDetails></div>
          </div>
          <div class="modal-body" id=div_eventDetails></div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->  
    
    <div class="container-fluid" id="footer"><?php include("../../libs/templates/footer.php")?></div>
    <div class="container" id="eventDialog"></div><!--cuadro de dialogo para inserción de evento-->     
  </body>
</html>

<?php
}//fin else sesión
?>
