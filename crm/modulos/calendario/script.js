var calendar = "";
var eventsFilterArr = new Array();
var eventsFilterUsrArr = new Array();
var tableUsr = "";

$(document).ready(function(){
  $("#eventDialog").load("../../libs/templates/eventdialog.php");
  
  updateIndexActive("li_navBar_calendario");

  //inicializa el calendario para su despliegue inicial y su gestión en todas las funciones
  calendar = $('#div_calendar').fullCalendar({
     editable: false,
     height: 500,
     aspectRatio: 1.9,
     header: {
       left: 'prev,next today',
       center: 'title',
       right: 'month,agendaWeek,agendaDay'
     },   
     /*events: {
       url: 'calendareventfetch.php?dummy=all',
       type: 'GET',
       data: {
         "action": "loadcalendar"
       }  
     },*/
     viewDisplay: function(element){
       //execFilter(0,"loadcalendar");
     },  
     eventRender: function(event, element, view) {
       eventRender(event,element);
     }  
  });
  
  $(".rad_filterType").change(function(){
    var option = $(this).val();
    setFilterType(option);
  })  
  
  //setea por default los filtros por tipo de evento
  setFilterType(0); 
  
  var dateval = $("#hdn_specDate").val().trim().length;
  if(0!=dateval){
    $('#div_calendar').fullCalendar('gotoDate',$("#hdn_specDate").val());       
  }
});

//despliega las listas de los filtros a aplicar. setea los filtros de acuerdo a si se eligió por tipos de eventos (cierre,cotización,llamada,etc) o por usuarios
function setFilterType(option){  
  $("#rad_filterType").val(option);
  $("#rad_filterType_"+option).prop('checked',true);
   
  if(0==option){
    getEventFilters();
    $("#div_filters").show();
    $("#div_filtersByUser").hide();
    eventsFilterUsrArr.forEach(function(item){      
      calendar.fullCalendar('removeEventSource',item);
    }); 
    eventsFilterUsrArr.length = 0;
    $(".chk_filters").prop('checked',true);  
    $('#div_filters input[type=checkbox]').each(function(){
      execFilter(this,0);
    });                        
  }
  else{ 
    getEventFiltersByUser();
    $("#div_filtersByUser").show();
    $("#div_filters").hide();   
    eventsFilterArr.forEach(function(item){      
      calendar.fullCalendar('removeEventSource',item);
    });
    eventsFilterArr.length = 0;
    $("#chk_eventSelectAllUsers").prop('checked',true); 
    eveSelectAllUsers($("#chk_eventSelectAllUsers"));               
  }  
}

//Selecciona todos o ninguno de los usuarios en el filtro
function eveSelectAllUsers(elem){
  var cells = tableUsr.cells().nodes();  
  if($(elem).is(":checked")){   
    $(cells).find(':checkbox').prop('checked',true);
  }
  else{
    $(cells).find(':checkbox').prop('checked',false);  
  }
  $(cells).find(':checkbox').each(function(){
    execFilter(this,1);  
  }); 
}

//Agrega o quita el evento de acuerdo con el filtro seleccionado
function execFilter(elem,option){
  var action = -1;   
    
  if(0==option){
    action = "loadfilter";   
  }
  if(1==option){
    action = "loadfilterbyuser";
  }
  
  var filterId = $(elem).val();      
  var events = {
      url: 'calendareventfetch.php?dummy='+filterId,
      type: 'GET',
      data: {
        "action": action,
        "filterId": filterId
     }                
  }      
  if($(elem).is(":checked")){   
    calendar.fullCalendar('removeEventSource', events);
    calendar.fullCalendar('addEventSource', events);
    if(0==option)
      eventsFilterArr.push(events);
    if(1==option) 
      eventsFilterUsrArr.push(events);      
  }
  else{
    if("loadfilterbyuser"==action){
      $("#chk_eventSelectAllUsers").prop('checked',false);
    }
    calendar.fullCalendar('removeEventSource', events);
    if(0==option)
      unsetEventFromArray(eventsFilterArr,events);
    if(1==option) 
      unsetEventFromArray(eventsFilterUsrArr,events);
  }   
}

//quita el id del evento del arreglo de eventos desplegados
function unsetEventFromArray(arr,id){
  for(var x=0; x<arr.length; x++){
    if(arr[x] == id)
      arr.splice(x,1);
  }
}

//carga los checkboxes con los filtros para todos los tipos de eventos y los carga en el calendario
function getEventFilters(){  
  $.ajax({
    type: "POST",
    url: "ajax.php",
    async: false,
    data: {"action": "getEventFilters"},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo filtros de eventos");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      $("#div_filters").html(msg); 
      $(".chk_filters").prop('checked',true);      
    }  
  });
}

//carga los checkboxes con los filtros para todos los usuarios y los carga en el calendario
function getEventFiltersByUser(){  
  $.ajax({
    type: "POST",
    url: "ajax.php",
    async: false,
    data: {"action": "getEventFiltersByUser"},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo filtros por usuario");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      $("#div_filtersByUser").html(msg); 
      tableUsr = $("#tbl_userEventFilter").DataTable({
          language: datatableespaniol,
      }).on('init.dt',function(){
        $("#chk_eventSelectAllUsers").prop('checked', true); 
      });
    }  
  });
}

//solo para debuggeo. despliega en vez del calendario el json de todos los eventos
function getEventJson(){
  $.ajax({
    type: "POST",
    url: "calendareventfetch.php",
    data: {"action": "getEvent"},
    success: function(msg){
      $("#div_calendar").html(msg); 
    }  
  });
}
