var representanteArray = Array();
var manTempId = 0;

$(document).ready(function(){
  $("#documentDialog").load("../../libs/templates/documentdialog.php");
  $("#eventDialog").load("../../libs/templates/eventdialog.php");
  $("#pricesDialog").load("../../libs/templates/pricesdialog.php");
  $("#siteDialog").load("../../libs/templates/sitedialog.php");
  $("#managerDialog").load("../../libs/templates/managerdialog.php");
  updateIndexActive("li_navBar_clientes");
  getClientRegs();
  $("#div_other_origin").hide();
  
  $("#txt_rfc").blur(function(){
    formatRfc("txt_rfc","mod_clientInsert");
  }); 
  $("#txt_rfc").focus(function(){
    deformatRfc("txt_rfc");
  });
  //$("#txt_repFecPod").datetimepicker(datetimeOptions);
  $("#txt_fecActa").datetimepicker(datetimeOptions);
  $("#txt_fecActi").datetimepicker(datetimeOptions);
  $("#rad_fisica").click(function(){
    $("#txt_rfc").attr('placeholder','XXXX-000000-YYY');
    $("#div_actCons").hide();
    $("#div_fecActCons").hide();
    $("#div_repLegal").hide();
    $("#div_altEmail").hide();
    $("#txt_emailAlt_error").html("");
    $("#txt_actaCons_error").html("");
    $("#txt_fecActa_error").html("");
    $("#hdn_manArr_error").html("");
  });
  $("#rad_moral").click(function(){
    $("#txt_rfc").attr('placeholder','XXX-000000-YYY');
    $("#div_actCons").show();
    $("#div_fecActCons").show();
    $("#div_repLegal").show();
    $("#div_altEmail").show();
  });
  if(0<$("#hdn_isProspect").val()){//checa si llegó un valor con el id del prospecto, si llegó significa que se va a migrar
    openNewClient();
    getProspectData();
  }
  $('#mod_clientInsert').on('hide.bs.modal', function(){
    $("#contactClientDialog").html("");
  });   
});

//función que obtiene los datos del prospecto y llena los campos posibles en el cuadro de diálogo de clientes para su migración al modulo
function getProspectData(){
  $.ajax({
    type: "POST",
    url: "../prospectos/ajax.php",
    data: {"action": "getProspectDetails", "prosId":$("#hdn_isProspect").val(), "option":"1"},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo datos del prospecto para su migración");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){                                    
        getStatesList(resp[11],"spn_edo","");                                
        getOriginList(resp[13]);
        getStatusList(resp[14]);
        getAssigntoList(resp[15]);
        
        $("#div_msgAlertClient").html("");
        $("#txt_razonSoc").val(resp[1]);
        $("#txt_nomCom").val(resp[2]);
        $("#txt_rfc").val(resp[3]);
        
        if(1==resp[4]){
          $("#rad_fisica").prop('checked',true); 
          $("#rad_moral").prop('checked',false);
        }
        else if(2==resp[4]){
          $("#rad_fisica").prop('checked',false); 
          $("#rad_moral").prop('checked',true);    
          $("#txt_rfc").attr('placeholder','XXX-000000-YYY');
          $("#div_actCons").show();
          $("#div_fecActCons").show();
          $("#div_repLegal").show();
          $("#div_altEmail").show(); 
        }
        
        $("#txt_tel").val(resp[5]);
        $("#txt_cell").val(resp[6]);        
        $("#txt_dom").val(resp[7]);
        $("#txt_col").val(resp[8]);
        $("#txt_cp").val(resp[9]);
        $("#txt_mun").val(resp[10]);
        $("#txt_email").val(resp[12]);
        $("#txt_desc").val(resp[18]);       
        
        if(8==resp[13]){
          $("#txt_origin").val(resp[19]);
          $("#div_other_origin").show();          
        }
        else{
          $("#txt_origin").val("");
          $("#div_other_origin").hide();
        }
        
        var contArr = JSON.parse(resp[20]);
        
        for(var x=0;x<contArr.length;x++){
          insertContactOnForm(1,contArr[x],1);
        }                         
        
        $("#hdn_action").val(2); //acción 2 que indica que se trata de una migración, no un cliente desde cero ni edición de cliente
        $("#btn_insertarContacto").prop('disabled',true);                             
        $("#mod_clientInsert").modal("show");
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);
      }         
    }  
  });
}

//función que obtiene la lista de todos los clientes existentes
function getClientRegs(){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getClientTable"},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo lista de clientes");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#div_clients").html(resp[1]);  
        $("#tbl_clients").DataTable({
          language: datatableespaniol,
        });
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);        
      }   
    }  
  });
}

//Limpia todos los campos del formulario de cliente
function clear_fields(){
  $("#contactClientDialog").load("../../libs/templates/contactdialog.php");//carga cuadro de dialogo de contactos en el modulo
  $("#txt_nomCom").val("");
  $("#txt_actaCons").val("");
  $("#txt_fecActa").val("");
  $("#txt_rfc").val("");
  $("#rad_fisica").prop('checked',true); 
  $("#rad_moral").prop('checked',false);  
  $("#rad_persona").val(1);
  $("#txt_tel").val("");
  $("#txt_cell").val("");
  $("#txt_razonSoc").val("");
  $("#txt_dom").val("");
  $("#txt_col").val("");
  $("#txt_cp").val("");
  $("#txt_mun").val("");
  $("#sel_state").val(0);
  $("#txt_email").val("");
  $("#txt_emailAlt").val("");
  $("#sel_origin").val(0);
  $("#txt_origin").val("");
  $("#sel_status").val(0);
  $("#div_other_origin").hide();
  //$("#sel_assignUsers").val(0);
  $("#txt_desc").val("");
  
  contactoArray.length = 0;
  representanteArray.length = 0;
  $("#tbl_selectedContact1").find("tr:gt(0)").remove();
  $("#tbl_selectedManager").find("tr:gt(0)").remove();
  $("#hdn_repActNewAction").val(0);
  $("#btn_repInsert").attr('onclick',"insertContactOnForm(0,'');");
  
  $("#txt_fecActa").prop("readonly","readonly");
  $("#txt_repFecPod").prop("readonly","readonly");
  
  $("#div_msgAlertClient").html("");
  $("#txt_razonSoc_error").html("");
  $("#txt_rfc_error").html("");
  $("#txt_nomCom_error").html("");
  $("#txt_actaCons_error").html("");
  $("#txt_fecActa_error").html("");
  $("#txt_tel_error").html("");
  $("#txt_cell_error").html("");
  $("#txt_dom_error").html("");
  $("#txt_col_error").html("");
  $("#txt_cp_error").html("");
  $("#txt_mun_error").html("");
  $("#sel_state_error").html("");
  $("#txt_email_error").html("");
  $("#txt_emailAlt_error").html("");
  $("#sel_origin_error").html("");
  $("#txt_origin_error").html("");
  $("#sel_status_error").html("");
  $("#txt_desc_error").html(""); 
  $("#hdn_contArr_error").html("");
  $("#hdn_repArr_error").html(""); 
  $("#hdn_manArr_error").html("");
  $("#spn_txt_razonSoc").html(""); 
  $("#spn_txt_nomCom").html("");
}

//abre el cuadro de diálogo para insertar un cliente
function openNewClient(){
  $("#hdn_action").val(0);
  $("#hdn_cliId").val(-1);
  getStatesList(0,"spn_edo","");
  getOriginList(0);
  getStatusList(0);
  getAssigntoList(0);
  clear_fields();
  $("#div_actCons").hide();
  $("#div_fecActCons").hide();
  $("#div_repLegal").hide();
  $("#div_altEmail").hide();
  $("#btn_insertarContacto").prop('disabled',false); 
  $("#btn_insertarRepresentante").prop('disabled',false);
  $("#mod_clientInsert").modal("show");
}

//función que manda a guardar los datos de un cliente nuevo o modificar los de un prospecto existente que se va a migrar
function saveClientNew(){
  $("#hdn_contArr").val(JSON.stringify(contactoArray));
  $("#hdn_manArr").val(JSON.stringify(representanteArray));
  if(false==validate_data()){ 
    printErrorMsg("div_msgAlertClient","Existen datos no válidos, favor de corregirlos"); 
  }
  else{    
    var options = {
      contentType: 'multipart/form-data; charset=utf-8',
      data:{"prosId":$("#hdn_isProspect").val(), "cliId":$("#hdn_cliId").val()}, 
      beforeSend: function(){
        setLoadDialog(0,"Guardando cliente");
      },
      complete: function(response) {
          setLoadDialog(1,"");
          var resp = (response.responseText).trim().split('||');
          var errCod = resp[0];
          var errMsg = resp[1];
          if("OK"== errCod){
            printSuccessMsg("div_msgAlert",errMsg);
            $("#mod_clientInsert").modal("hide");
            getClientRegs();
            clear_fields();
          }
          else{
            printErrorMsg("div_msgAlertClient",errMsg);                                                        
          }          
      },
      error: function() {
          printErrorMsg("div_msgAlertClient","Ocurrió un error al enviarse los datos para su guardado");     
      }      
    }; 
    $("#frm_newClient").ajaxForm(options);  
  }
} 

//función que valida la integridad de todos los datos del formulario de cliente nuevo/editar. Devuelve tru si todos los campos son válidos
function validate_data(){
  addValidationRules(1);
  
  jQuery.validator.addMethod("isOtherOriginSpecified", function(value, element){    
    if((0>=(value).length)&&("8"==$("#sel_origin").val()))
      return false;
    else
      return true;  
  }, "Debe especificar otro tipo de origen");
  
  jQuery.validator.addMethod("hasManager", function(value, element){    
    if(($("#rad_moral").is(':checked'))&&((0>=representanteArray.length)||(0>=value.length)))
      return false;
    else
      return true;  
  }, "Debe agregar un representante legal");

  return $("#frm_newClient").validate({
    ignore: ':hidden:not(#hdn_contArr,#hdn_manArr)',
    rules: {
      txt_tel:
      {
        number: true,
        isPhoneNumber:true
      },
      txt_rfc:
      {
        rfc: true
      },
      txt_cell:
      {
        number: true,
        isPhoneNumber:true
      },
      txt_fecActa:
      {
        isDateTime:true
      },
      txt_cp:
      {
        number: true
      },
      txt_origin:
      {
        isOtherOriginSpecified: true
      },
      sel_status:
      {
        isSomethingSelected: true
      },
      sel_origin:
      {
        isSomethingSelected: true
      },
      hdn_contArr:
      {
        hasContacts:true
      },
      hdn_manArr:
      {
        hasManager:true
      },
      sel_state:
      {
        isSomethingSelected: true
      }
    },
    errorPlacement: function(error, element) {
       	error.appendTo($('#' + element.attr("id") + '_error'));
    }
  }).form(); 
}

//abre el cuadro de diálogo para editar un cliente
function editClient(id){
  clear_fields();
  $("#div_msgAlertClient").html("");
  $("#hdn_action").val(1);
  $("#hdn_cliId").val(id);
  fill_fields(id);
  $("#btn_insertarContacto").prop('disabled',false); 
  $("#mod_clientInsert").modal("show");
  setModalResponsive("mod_clientInsert");
}

//llena los campos del formulario de cliente cuando se elige editar
function fill_fields(id){
  clear_fields();
  $.ajax({
    type: "POST",
    url: "ajax.php",
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo datos del cliente para su edición");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    data: {"action": "getClientDetails", "cliId":id, "option":"1"},
    success: function(msg){
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){           
        $("#h4TitlleClient").html("Modificar Ciente");                             
        getStatesList(resp[19],"spn_edo","");                                
        getOriginList(resp[10]);
        //getStatusList(resp[14]);
        //getAssigntoList(resp[15]);
        $("#div_msgAlertClient").html("");
        $("#txt_razonSoc").val(resp[2]);
        $("#txt_nomCom").val(resp[3]);
        $("#txt_rfc").val(resp[4]);
        if(1==resp[5]){
          $("#rad_fisica").prop('checked',true); 
          $("#rad_moral").prop('checked',false);
          $("#div_actCons").hide();
          $("#div_fecActCons").hide();
          $("#div_repLegal").hide();
          $("#div_altEmail").hide();
        }
        else if(2==resp[5]){
          $("#rad_fisica").prop('checked',false); 
          $("#rad_moral").prop('checked',true);          
          $("#div_actCons").show();
          $("#div_fecActCons").show();
          $("#div_repLegal").show();
          $("#div_altEmail").show();
          var repArr = JSON.parse(resp[24]);
          if(0<repArr.length){
            for(var y=0;y<repArr.length;y++){
              insertManagerOnForm(1,repArr[y]);
            } 
          }
        }
        $("#txt_actaCons").val(resp[12]);
        $("#txt_fecActa").val(resp[13]);
        $("#txt_tel").val(resp[6]);
        $("#txt_cell").val(resp[7]);        
        $("#txt_dom").val(resp[15]);
        $("#txt_col").val(resp[16]);
        $("#txt_cp").val(resp[17]);
        $("#txt_mun").val(resp[18]);
        $("#txt_email").val(resp[8]);
        $("#txt_emailAlt").val(resp[9]);
        $("#txt_desc").val(resp[22]);
        if(8==resp[10]){
          $("#txt_origin").val(resp[11]);
          $("#div_other_origin").show();          
        }
        else{
          $("#txt_origin").val("");
          $("#div_other_origin").hide();
        }
        var contArr = JSON.parse(resp[23]);
        for(var x=0;x<contArr.length;x++){
          insertContactOnForm(1,contArr[x],1);
        }
      }
      else{
        printErrorMsg("div_msgAlertClient",resp[1]);
      }      
    }  
  });
}

//función que manda a eliminar un cliente
function deleteClient(id){
  if(confirm("¿Está seguro/a de eliminar este cliente? Esto borrará también todos sus comentarios asociados, eventos y cotizaciones. Esta acción no se podrá deshacer.")) {
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: {"action": "deleteClient", "cliId":id},
      beforeSend: function(){
        setLoadDialog(0,"Eliminando Cliente");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){
        var resp = msg.trim().split("||");
        if("OK" == resp[0]){
          printSuccessMsg("div_msgAlert",resp[1]);
          getClientRegs();
        }
        else{
          printErrorMsg("div_msgAlert",resp[1]);         
        } 
      }  
    });
  }
}

//función que obtiene los tickets asociados al cliente
function getClientTickets(id){
  $("#hdn_cliId").val(id);
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getTicketList", "orgId":id, "orgType":1},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo tickets asociados al cliente");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){    
     var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#div_clientTickets").html(resp[1]);
        
        $("#tbl_ticketList").DataTable({
          language: datatableespaniol,
        });
        setModalResponsive('mod_clientTickets'); 
        $("#mod_clientTickets").modal("show");
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);        
      }      
    }  
  });
}


