<?php
session_start();
set_time_limit(0);
include_once "../../libs/db/common.php";
include_once "../../libs/db/encrypt.php";

if (!verifySession($_SESSION)) {
  logoutTimeout();
} else {
  header('Content-Type: text/html; charset=UTF-8');
  date_default_timezone_set('America/Mexico_City');

  $title = "Clientes";
  $usuario = $_SESSION['usuario'];
  $gruposArr  = $_SESSION['grupos'];

  $prosId = isset($_GET['pId']) ? decrypt($_GET['pId']) : -1;

  $modId = 1;
  ?>

  <!DOCTYPE html>
  <html>

  <head>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/AdminLTE2/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/js/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/bootstrap/summernote-0.6.6-dist/summernote.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/bootstrap/font-awesome-4.3.0/css/font-awesome.css" rel="stylesheet" type="text/css" />

    <script src="../../libs/AdminLTE2/plugins/jQuery/jQuery-2.1.3.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/bootstrap/js/bootstrap.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/dist/js/app.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../libs/js/jqueryValidate/dist/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.form.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.uploadPreview.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery-ui-sliderAccess.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
    <script src="../../libs/bootstrap/summernote-0.6.6-dist/summernote.js" type="text/javascript"></script>
    <script src="../../libs/bootstrap/summernote-0.6.6-dist/lang/summernote-es-ES.js" type="text/javascript"></script>
    <script src="../../libs/js/common.js" type="text/javascript"></script>
    <script src="script.js" type="text/javascript"></script>

    <link rel="icon" type="image/png" href="../../img/favicon-16x16.png" sizes="16x16">

    <title>Coeficiente CRM - Clientes</title>
    <style type="text/css">
    </style>
  </head>

  <body>
    <!--Div que contiene el encabezado-->
    <div class="container" id="header"><?php include("../../libs/templates/header.php") ?></div>

    <div class="container-fluid">
      <h2 class="page-header" id="tituloPag">
        <span class="glyphicon glyphicon-list-alt"></span> <?php echo $title ?>
      </h2>
    </div><!-- /container -->

    <div class="container">
      <div class="row">
        <div id=div_msgAlert></div>
      </div>
    </div> <!-- /container -->
    <?php if (hasPermission($modId, 'w') && (hasPermission($modId, 'r') || hasPermission($modId, 'l'))) { ?>
      <div class="container">
        <div class="row">
          <div style="display:block;"><button type="button" id="btn_prospectNew" title="Nuevo cliente" class="btn btn-default" onClick="openNewClient();"><span class="glyphicon glyphicon-asterisk"></span> Nuevo Cliente</button></div>
        </div>
        <br>
      </div> <!-- /container -->
    <?php } ?>

    <div class="container">
      <div class="row">
        <div id="div_tabs"></div>
      </div>
      <div class="row">
        <div style="" id="div_clients"></div>
        <input type="hidden" id="hdn_orgType" name="hdn_orgType" class="form-control" value=1>
        <input type="hidden" id="hdn_orgId" name="hdn_orgId" class="form-control" value=-1>
        <input type="hidden" id="hdn_isProspect" name="hdn_isProspect" class="form-control" value=<?php echo $prosId ?>>
      </div>
    </div> <!-- /container -->
    <div class="container-fluid" id="footer"><?php include("../../libs/templates/footer.php") ?></div>

    <!--Cuadro de dialogo de insertar cliente -->
    <div id="mod_clientInsert" class="modal fade" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="h4TitlleClient">Nuevo Cliente</h4>
          </div>
          <div class="container-fluid">
            <div id=div_msgAlertClient></div>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" id="frm_newClient" name="frm_newClient" role="form" action="saveClient.php" method="post" enctype="multipart/form-data">
              <div>
                <label>Tipo de persona:
                  <input type="radio" name="rad_persona" id="rad_fisica" class="rad_responsive" value=1 checked> Física
                  <input type="radio" name="rad_persona" id="rad_moral" class="rad_responsive" value=2> Moral
                </label>
              </div>
              <div>
                <label>RFC:
                  <input type="text" id="txt_rfc" name="txt_rfc" class="form-control" placeholder="XXXX-000000-YYY" maxlength="16" size="35" required>
                </label>
              </div>
              <div>
                <label id="txt_rfc_error" class="div-errorVal" />
              </div>
              <div>
                <label>Razón Social:
                  <input type="text" id="txt_razonSoc" name="txt_razonSoc" class="form-control" placeholder="Razón Social" maxlength="120" size="40" required>
                </label>
                <span id="spn_txt_razonSoc"></span>
              </div>
              <div>
                <label id="txt_razonSoc_error" class="div-errorVal" />
              </div>
              <div>
                <label>Nombre Comercial:
                  <input type="text" id="txt_nomCom" name="txt_nomCom" class="form-control" placeholder="Nombre Comercial" maxlength="120" size="40" required>
                </label>
                <span id="spn_txt_nomCom"></span>
              </div>
              <div>
                <label id="txt_nomCom_error" class="div-errorVal" />
              </div>
              <div id="div_actCons">
                <div>
                  <label>Acta Constitutiva:
                    <input type="text" id="txt_actaCons" name="txt_actaCons" class="form-control" placeholder="Acta Constitutiva" maxlength="40" size="40" required>
                  </label>
                </div>
                <div>
                  <label id="txt_actaCons_error" class="div-errorVal" />
                </div>
              </div>
              <div id="div_fecActCons">
                <div>
                  <label>Fecha de Acta:
                    <input type="text" id="txt_fecActa" name="txt_fecActa" class="form-control" placeholder="Fecha de Acta" maxlength="20" size="40" required>
                  </label>
                </div>
                <div>
                  <label id="txt_fecActa_error" class="div-errorVal" />
                </div>
              </div>
              <div>
                <label>Teléfono:
                  <input type="tel" id="txt_tel" name="txt_tel" class="form-control" placeholder="10 dígitos" maxlength="10" size="10" required>
                </label>
                <label>Teléfono Móvil:
                  <input type="tel" id="txt_cell" name="txt_cell" class="form-control" placeholder="10 dígitos" maxlength="10" size="10" required>
                </label>
              </div>
              <div>
                <label id="txt_tel_error" class="div-errorVal"></label>
                <label id="txt_cell_error" class="div-errorVal"></label>
              </div>
              <div>
                <label>Domicilio Fiscal:
                  <input type="text" id="txt_dom" name="txt_dom" class="form-control" placeholder="Domicilio" maxlength="120" size="40" required>
                </label>
              </div>
              <div>
                <label id="txt_dom_error" class="div-errorVal" />
              </div>
              <div>
                <label>Colonia Fiscal:
                  <input type="text" id="txt_col" name="txt_col" class="form-control" placeholder="Colonia" maxlength="80" size="40" required>
                </label>
              </div>
              <div>
                <label id="txt_col_error" class="div-errorVal" />
              </div>
              <div>
                <label>C.P. Fiscal:
                  <input type="text" id="txt_cp" name="txt_cp" class="form-control" placeholder="C.P." maxlength="5" size="5" required>
                </label>
                <label>Municipio Fiscal:
                  <input type="text" id="txt_mun" name="txt_mun" class="form-control" placeholder="Municipio" maxlength="80" size="30" required>
                </label>
              </div>
              <div>
                <label id="txt_cp_error" class="div-errorVal"></label>
                <label id="txt_mun_error" class="div-errorVal"></label>
              </div>
              <div>
                <label>Estado Fiscal:<span id="spn_edo"></span></label>
              </div>
              <div>
                <label id="sel_state_error" class="div-errorVal" />
              </div>
              <div>
                <label>Correo Electrónico:
                  <input type="email" id="txt_email" name="txt_email" class="form-control" placeholder="Correo Electrónico" maxlength="80" size="20" required>
                </label>
              </div>
              <div>
                <label id="txt_email_error" class="div-errorVal" />
              </div>
              <div id="div_altEmail">
                <div>
                  <label>Correo Electrónico Alterno:
                    <input type="email" id="txt_emailAlt" name="txt_emailAlt" class="form-control" placeholder="Correo Electrónico" maxlength="80" size="20" required>
                  </label>
                </div>
                <div>
                  <label id="txt_emailAlt_error" class="div-errorVal" />
                </div>
              </div>

              <label>Contactos:<span id="spn_contacto"></span></label>
              <div>
                <input type="button" class="btn btn-primary" id="btn_insertarContacto" value="Agregar Contacto" onClick="openNewContact(1);">
              </div>
              <div>
                <div class="table-responsive">
                  <table id="tbl_selectedContact1" class="table table-hover table-striped tbl_selectedContact">
                    <thead>
                      <tr>
                        <th>Nombre</th>
                        <th>Apellidos</th>
                        <th>Puesto</th>
                        <th>Teléfono</th>
                        <th>Teléfono Móvil</th>
                        <th>Correo Electŕonico</th>
                        <th>Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>

              <div>
                <label id="hdn_contArr_error" class="div-errorVal" />
              </div>

              <div id="div_repLegal">
                <label>Representante legal:<span id="spn_representante"></span></label>
                <div>
                  <input type="button" class="btn btn-primary" id="btn_insertarRepresentante" value="Agregar Representante" onClick="openNewManager();">
                </div>
                <div>
                  <div class="table-responsive">
                    <table id="tbl_selectedManager" class="table table-hover table-striped">
                      <thead>
                        <tr>
                          <th>Nombre</th>
                          <th>Apellidos</th>
                          <th># Poder</th>
                          <!-- <th>Fecha Poder</th> 
                          <th>Domicilio</th>
                          <th>Colonia</th>
                          <th>C.P.</th>
                          <th>Municipio</th>
                          <th>Estado</th>-->
                          <th>Teléfono</th>
                          <th>Teléfono Movil</th>
                          <th>Email</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              <div>
                <label id="hdn_manArr_error" class="div-errorVal" />
              </div>
              <div>
                <label>Origen de Cliente:<span id="spn_orgn"></span></label>
              </div>
              <div>
                <label id="sel_origin_error" class="div-errorVal" />
              </div>
              <div id="div_other_origin" name="div_other_origin">
                <div>
                  <label>Otro origen:
                    <input type="text" id="txt_origin" name="txt_origin" class="form-control" placeholder="Especifique Origen" maxlength="255" size="40" required>
                  </label>
                </div>
                <div>
                  <label id="txt_origin_error" class="div-errorVal" />
                </div>
              </div>
              <!--  <div>
                <label>Estatus:<span id="spn_estat"></span></label>
              </div>
              <div>
                <label id="sel_status_error" class="div-errorVal"/>
              </div>  -->
              <div>
                <label>Descripción:
                  <textarea id="txt_desc" name="txt_desc" class="form-control" placeholder="Agregue una breve descripción acerca del cliente." maxlength="255" cols="40" rows="5" required></textarea>
                </label>
                <div>
                  <label id="txt_desc_error" class="div-errorVal" />
                </div>
              </div>
              <input type="hidden" id="hdn_action" name="hdn_action" class="form-control" value=-1>
              <input type="hidden" id="hdn_cliId" name="hdn_cliId" class="form-control" value=-1>
              <input type="hidden" id="hdn_contArr" name="hdn_contArr" class="form-control" value="">
              <input type="hidden" id="hdn_manArr" name="hdn_manArr" class="form-control" value="">
          </div>
          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <input type="submit" class="btn btn-primary" id="cargaCamp" value="Guardar Cambios" onClick="saveClientNew();">
          </div>
          </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="container" id="managerDialog"></div>
    <!--cuadro de dialogo para representante legal-->

    <div class="container" id="eventDialog"></div>
    <!--cuadro de dialogo para inserción de evento-->

    <div class="container" id="pricesDialog"></div>
    <!--cuadro de dialogo para gestión de cotización-->

    <div class="container" id="commentClientDialog"></div>
    <!--cuadro de dialogo para gestión de comentarios-->

    <div class="container" id="documentDialog"></div>
    <!--cuadro de dialogo para gestión de documentos-->

    <div class="container" id="siteDialog"></div>
    <!--cuadro de dialogo para gestión de sitios-->

    <div class="container" id="contactClientDialog"></div>
    <!--cuadro de dialogo para gestión de contactos-->

    <!--_Cuadro de dialogo de tickets de cliente -->
    <div id="mod_clientTickets" class="modal fade" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Tickets de Cliente</h4>
          </div>
          <div class="container-fluid">
            <div id="div_msgAlertClientTickets"></div>
          </div>
          <div class="modal-body row">
            <div class="col container-fluid">
            </div><br>
            <div class="col container-fluid" id="div_clientTickets"></div>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!--_Cuadro de dialogo de tickets de cliente -->
    <div id="mod_clientDetails" class="modal fade" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Dealles del Cliente:</h4>
          </div>
          <div class="container-fluid">
            <div id="div_msgAlertClientDetails"></div>
          </div>
          <div class="modal-body row">
            <div class="col container-fluid">
            </div><br>
            <div class="col container-fluid" id="div_clientDetails"></div>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

  </body>

  </html>

<?php
}
?>