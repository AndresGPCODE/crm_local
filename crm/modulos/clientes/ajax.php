<?php
session_start();

set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

//include_once "../../libs/db/dbcommon.php";
include_once "../../libs/db/common.php";

$gruposArr  = $_SESSION['grupos'];
$usuarioId  = $_SESSION['usrId'];
$usuario    = $_SESSION['usuario'];
$nombre     = $_SESSION['usrNombre'];
$usrUbica   = $_SESSION['usrUbica'];

if(!verifySession($_SESSION)){
  logoutTimeout();
}else{
$db_catalogos = condb_catalogos();
$db_modulos   = condb_modulos();
$db_usuarios  = condb_usuarios();
$salida = "";
$modId = 1;
if("getClientTable" == $_POST["action"]){
  if(hasPermission($modId,'f')&&hasPermission($modId,'r')){
      // devuelve todos los clientes
      $query = "SELECT * FROM clientes";
  }
  elseif(hasPermission($modId,'c')&&hasPermission($modId,'r')){
      // devuelve los clientes de la region del usuario
      $query = "SELECT cl.* FROM " . $cfgTableNameMod . ".clientes AS cl, " . $cfgTableNameUsr . ".usuarios AS usr " .
        "WHERE cl.usuario_alta=usr.usuario_id AND usr.ubicacion_id=" . $usrUbica . " AND cl.cliente_id>0";
  }
  elseif(hasPermission($modId,'l')){
    // devuelve los clientes dados de alta por el usuario
    $query = "SELECT * FROM clientes WHERE usuario_alta=".$usuarioId;
  }
  else{
    $salida = "<b>ERROR:</b> No cuenta con permisos para ver esta información";
    goto cliTablea;
  }
  $result = mysqli_query($db_modulos,$query);
  if(!$result){
    $salida = "ERROR|| No se pudo consultar de los datos de clientes en la DB";
  }
  else{
    if(0==mysqli_num_rows($result)){
      $salida = "OK||No existen registros disponibles haga click en <strong>Nuevo Cliente</strong> para agregar uno nuevo".$queryo;
    }
    else{
      $salida = "OK||<table id=\"tbl_clients\" class=\"table table-hover table-striped\">\n".
                "  <thead>\n".
                "    <tr>\n".
                "      <th class=\"hidden-xs\">ID</th>\n".
                "      <th class=\"hidden-xs\">Razón Social</th>\n".
                "      <th>Nombre Comercial</th>\n".
                "      <th>Teléfono</th>\n".
            /*  "      <th>Teléfono Móvil</th>\n".
                "      <th>Domicilio</th>\n".
                "      <th>Colonia</th>\n".
                "      <th>CP</th>\n".
                "      <th>Municipio</th>\n".
                "      <th>Estado</th>\n". */ 
                "      <th>Correo Electrónico</th>\n".  
                "      <th class=\"hidden-xs\">Origen Cliente</th>\n".
             /* "      <th>Estatus</th>\n".
                "      <th>Asignado a:</th>\n".
                "      <th>Fecha de Alta</th>\n".
                "      <th>Usuario que lo Agregó</th>\n".  */
                "      <th class=\"hidden-xs\">Descripción</th>\n".
                "      <th>Acciones</th>\n".
                "    </tr>\n".
                "  </thead>\n".
                "  <tbody>\n";
      while($row = mysqli_fetch_assoc($result)){
        $estatus = get_sellStat($row['estatus']);
        $origen = get_sellOrigin($row['origen']);
        //$nombre = get_userRealName($row['asignadoa']);
        //$estado = get_state($row['']);
        $salida .= "    <tr>\n".
                   "      <td class=\"hidden-xs\">".$row["cliente_id"]."</td>\n".
                   "      <td class=\"hidden-xs\">".$row["razon_social"]."</td>\n".
                   "      <td>".$row["nombre_comercial"]."</td>\n".
                   "      <td>".$row["telefono"]."</td>\n".
                /* "      <td>".$row["tel_movil"]."</td>\n".
                   "      <td>".$row["domicilio"]."</td>\n".
                   "      <td>".$row["colonia"]."</td>\n".
                   "      <td>".$row["cp"]."</td>\n".
                   "      <td>".$row["municipio"]."</td>\n".
                   "      <td>".$row["estado"]."</td>\n"."]*/
                   "      <td>".$row["email"]."</td>\n".  
                   "      <td class=\"hidden-xs\">".$origen."</td>\n";
                /* "      <td>".$estatus."</td>\n".
                   "      <td>".$nombre."</td>\n";
                   "      <td>".$row["fecha_alta"]."</td>\n".
                   "      <td>".$row["usuario_alta"]."</td>\n".*/
                   if(30<strlen($row["descripcion"]))
                     $salida .= "      <td class=\"hidden-xs\" title=\"".$row["descripcion"]."\">".substr($row["descripcion"],0,30)."...</td>\n"; 
                   else
                     $salida .= "      <td class=\"hidden-xs\">".$row["descripcion"]."</td>\n";             
        $salida .= "      <td>".
                   "        <button type=\"button\" id=\"btn_clientDetails_".$row['cliente_id']."\" title=\"Ver Detalles\" class=\"btn btn-xs btn-primary btn-responsive\" onClick=\"getClientDetails('". encrypt($row['cliente_id']) ."');\"><span class=\"glyphicon glyphicon-list\"></span></button>".
                   "        <button type=\"button\" id=\"btn_clientComments_".$row['cliente_id']."\" title=\"Ver Comentarios\" class=\"btn btn-xs btn-info btn-responsive\" onClick=\"getComments(".$row['cliente_id'].",1);\"><span class=\"glyphicon glyphicon-comment\"></span></button>".
                   "        <button type=\"button\" id=\"btn_clientEvents_".$row['cliente_id']."\" title=\"Ver Eventos\" class=\"btn btn-xs btn-warning btn-responsive\" onClick=\"getEvents(".$row['cliente_id'].",1);\"><span class=\"glyphicon glyphicon-calendar\"></span></button>";
        if(hasPermission(4,'r')||hasPermission(4,'w')){ //tiene permiso para leer datos de cotizaciones
          $salida .= "        <button type=\"button\" id=\"btn_clientPrices_".$row['cliente_id']."\" title=\"Ver Cotizaciones\" class=\"btn btn-xs btn-default btn-responsive\" onClick=\"getPrices(".$row['cliente_id'].",1);\"><span class=\"glyphicon glyphicon-usd\"></span></button>";
        }           
        $salida .= "        <button type=\"button\" id=\"btn_clientDocs_".$row['cliente_id']."\" title=\"Ver Documentación\" class=\"btn btn-xs btn-primary btn-responsive\" onClick=\"displayDocuments(".$row['cliente_id'].",1);\"><span class=\"glyphicon glyphicon-file\"></span></button>"; 
        if(hasPermission(2,'r')||hasPermission(2,'w')){  //si tiene permisos para leer datos de sitios                  
          $salida .= "        <button type=\"button\" id=\"btn_clientSites_".$row['cliente_id']."\" title=\"Ver Sitios\" class=\"btn btn-xs btn-default btn-responsive\" onClick=\"displayOrgSites(".$row['cliente_id'].",1,0);\"><span class=\"glyphicon glyphicon-tower\"></span></button>";
        }
        if(hasPermission($modId,'d')){     
          $salida.= "        <button type=\"button\" id=\"btn_clientDelete_".$row['cliente_id']."\" title=\"Eliminar Cliente\" class=\"btn btn-xs btn-danger btn-responsive\" onClick=\"deleteClient(".$row['cliente_id'].");\"><span class=\"glyphicon glyphicon-remove\"></span></button>";
        }
        if(hasPermission($modId,'e')||(hasPermission($modId,'n')&&$row['usuario_alta']==$usuarioId)){
          $salida .= "        <button type=\"button\" id=\"btn_clientEdit_".$row['cliente_id']."\" title=\"Editar Registro\" class=\"btn btn-xs btn-success btn-responsive\" onClick=\"editClient('".encrypt($row['cliente_id'])."');\"><span class=\"glyphicon glyphicon-edit\"></span></button>";                                            
        } 
        if(hasPermission(6,'r')||hasPermission(6,'w')){  //si tiene permisos para leer datos de sitios                  
          $salida .= "        <button type=\"button\" id=\"btn_clientTickets_".$row['cliente_id']."\" title=\"Ver Tickets\" class=\"btn btn-xs btn-info btn-responsive\" onClick=\"getClientTickets(".$row['cliente_id'].");\"><i class=\"fa fa-ticket\" style=\"color:black;\"></i></span></button>";
        }                            
         $salida.= "      </td>\n".
                   "    </tr>\n";      
      }
      $salida .= "  </tbody>\n".
                 "</table>\n"; 
    }
  }
  cliTablea:
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);  
  echo $salida;
}
//borra un cliente
if("deleteClient" == $_POST["action"]){
  
  $cliId = isset($_POST['cliId']) ? $_POST['cliId'] : -1;
  
  $query = "DELETE FROM clientes WHERE cliente_id=".$cliId;
  $query2 = "DELETE FROM calendario WHERE origen_id=".$cliId." AND tipo_origen=1";
  $query3 = "DELETE FROM cotizaciones WHERE origen_id=".$cliId." AND tipo_origen=1";
  $query4 = "DELETE FROM documentos WHERE origen_id=".$cliId." AND tipo_origen=1";
  $result = mysqli_query($db_modulos,$query);  

  if(!$result){
    $salida = "ERROR|| No se pudo eliminar este cliente";
  }
  else{
    $result2 = mysqli_query($db_modulos,$query2);
    $result3 = mysqli_query($db_modulos,$query3);
    $result4 = mysqli_query($db_modulos,$query4);
    
    if(!$result2||!$result3||!$result4){
      $salida = "ERROR|| Se eliminó el prospecto, pero ocurrió un problema al eliminarse sus datos asociados";
      writeOnJournal($usuarioId,"Ha eliminado un prospecto con el id ".$cliId." pero no se eliminó sus eventos o cotizaciones");
    }
    else{
      writeOnJournal($usuarioId,"Ha eliminado un prospecto con el id ".$cliId);
      $salida = "OK|| El registro ha sido eliminado exitosamente";
    }
  } 
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;  
}
}//fin else sesión 
?>
