<?php  //-*-mode: php-*-
session_start();
set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

//include_once "../../libs/db/dbcommon.php";
include_once "../../libs/db/common.php";

$gruposArr  = $_SESSION['grupos'];
$usuarioId  = $_SESSION['usrId'];
$usuario    = $_SESSION['usuario'];
$nombre     = $_SESSION['usrNombre'];

if(!verifySession($_SESSION)){
  printf("<script>alert('Su tiempo de sesi&oacute;n ha terminado. Por favor, vuelva a iniciar sesi&oacute;n');location.href='../login/'</script>");
  //header('Location: ../login/');
}else{

$cfgLogService = "../../log/clientlog";

$db_catalogos = condb_catalogos();
$db_modulos   = condb_modulos();
$db_usuarios  = condb_usuarios();

$salida = "";

$cliId       = isset($_POST['cliId'])      ? $_POST['cliId'] : -1;                                 

$fecha       = date("Y-m-d H:i:s");
$razonSoc    = isset($_POST['txt_razonSoc'])    ? $_POST['txt_razonSoc'] : "";
$rfc         = isset($_POST['txt_rfc'])         ? $_POST['txt_rfc'] : "";
$tipPer      = isset($_POST['rad_persona'])     ? $_POST['rad_persona'] : "";
$nomCom      = isset($_POST['txt_nomCom'])      ? $_POST['txt_nomCom'] : "";
$actCons     = isset($_POST['txt_actaCons'])    ? $_POST['txt_actaCons'] : "";
$fecAct      = isset($_POST['txt_fecActa'])     ? $_POST['txt_fecActa'] : "";
$tel         = isset($_POST['txt_tel'])         ? $_POST['txt_tel'] : "";
$cell        = isset($_POST['txt_cell'])        ? $_POST['txt_cell'] : "";
$dom         = isset($_POST['txt_dom'])         ? $_POST['txt_dom'] : "";
$col         = isset($_POST['txt_col'])         ? $_POST['txt_col'] : "";
$cp          = (isset($_POST['txt_cp'])&&$_POST['txt_cp']!="") ? $_POST['txt_cp'] : 0;
$mun         = isset($_POST['txt_mun'])         ? $_POST['txt_mun'] : "";
$state       = isset($_POST['sel_state'])       ? $_POST['sel_state'] : 0;
$email       = isset($_POST['txt_email'])       ? $_POST['txt_email'] : "";
$emailAlt    = isset($_POST['txt_emailAlt'])    ? $_POST['txt_emailAlt'] : "";
$origin      = isset($_POST['sel_origin'])      ? $_POST['sel_origin'] : 0;
$originOth   = isset($_POST['txt_origin'])      ? $_POST['txt_origin'] : "";
$status      = isset($_POST['sel_status'])      ? $_POST['sel_status'] : 0;
$desc        = isset($_POST['txt_desc'])        ? $_POST['txt_desc'] : "";
$hdnAction   = isset($_POST['hdn_action'])      ? $_POST['hdn_action'] : -1;
$contArr     = isset($_POST['hdn_contArr'])     ? json_decode($_POST['hdn_contArr']) : "";
$reprArr     = isset($_POST['hdn_manArr'])      ? json_decode($_POST['hdn_manArr']) : "";

$prosId      = isset($_POST['prosId'])          ? $_POST['prosId'] : -1;

$campos = array("cliente_id"       => $cliId,
                "razon_social"     => $razonSoc,
                "rfc"              => $rfc,
                "tipo_persona"     => $tipPer,
                "acta_costitutiva" => $actCons,
                "nombre_comercial" => $nomCom,
                "telefono"         => $tel,
                "tel_movil"        => $cell,
                "domicilio"        => $dom,
                "colonia"          => $col,
                "cp"               => $cp,
                "municipio"        => $mun,
                "estado"           => $state,
                "email"            => $email,
                "origen"           => $origin,
                "origen_otro"      => $originOth,
                "estatus"          => $status,
                "descripcion"      => $desc);
                
function checkIfRfcExists($value){
  global $db_modulos;
  global $salida;
  
  $query = "SELECT * FROM clientes WHERE rfc_curp='".$value."'";
  
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $salida = "ERROR|| Ocurrió un problema en consulta de datos previos";
    return false;
  }
  else{
     if(0!=mysqli_num_rows($result)){
      $salida = "ERROR||Ya existe un registro con el dato ".$value;
      return false;
    }
    else{
      //$salida = "ERROR||No existe un registro con el dato ".$value." se puede guardar";
      return true;
    }
  }
}

if(0==$hdnAction||2==$hdnAction){ //si se manda a guardar cliente nuevo
  if(!checkIfRfcExists($rfc)){  
    goto insCli;
  }
  else{
    if(2==$tipPer){//si es persona moral guardará rep legal
      if(!$newRepId = insertManager($reprArr)){ 
        $salida = "ERROR||Ocurrió un problema al guardarse los datos de representante legal";
        goto insCli;
      }
    } 
    else{
      $newRepId = -1;
    }
     
    $query = "INSERT INTO clientes ( razon_social, ".
                                    "nombre_comercial, ".
                                    "rfc_curp, ".
                                    "tipo_persona, ".
                                    "telefono, ".
                                    "tel_movil, ".
                                    "email, ".
                                    "email_alt, ".
                                    "estatus, ".
                                    "origen, ".
                                    "origen_otro, ".
                                    "acta_constitutiva, ".
                                    "fecha_acta, ".
                                    "representante_id, ".
                                    "domicilio_fiscal, ".
                                    "colonia_fiscal, ".
                                    "cp_fiscal, ".
                                    "municipio_fiscal, ".
                                    "estado_fiscal, ".
                                    "fecha_alta, ".
                                    "usuario_alta, ".
                                    "descripcion ) ".
       "VALUES ( '".$razonSoc."', ".
                "'".$nomCom."', ".
                "'".$rfc."', ".
                    $tipPer.", ".
                "'".$tel."', ".
                "'".$cell."', ".
                "'".$email."', ".
                "'".$emailAlt."', ".
                    $status.", ".
                    $origin.", ".
                "'".$originOth."', ".
                "'".$actCons."', ".
                "'".$fecAct."', ".                     
                    $newRepId.", ".
                "'".$dom."', ".
                "'".$col."', ".
                    $cp.", ".
                "'".$mun."', ".
                    $state.", ".                      
                "'".$fecha."', ".
                    $usuarioId.", ".
                "'".$desc."' )";    
  
    $result = mysqli_query($db_modulos,$query); 
    
    if(!$result){
      $salida = "ERROR|| Ocurrió un problema en la inserción del cliente a la base de datos ".$query;
    }
    else{
      writeOnJournal($usuarioId,"Ha insertado un cliente nuevo con el id ".mysqli_insert_id($db_modulos));
      $newCliId = mysqli_insert_id($db_modulos);
      
      if(0==$hdnAction){ //si es un cliente nuevo
        log_write("DEBUG: INSERT-CLIENT: Se van a insertar contactos nuevos para el cliente id [".$newCliId."]",1);
        foreach($contArr as $key => $value){
          if(!insertContact($db_modulos,$value,1,$newCliId))
          goto insCli;
        } 
      }
      if(2==$hdnAction){ //si se migra de un prospecto
        log_write("DEBUG: INSERT-CLIENT: Se va a migrar los datos asociados al prospecto/cliente [".$newCliId."]",1);
        if(!migrateProspectData($db_modulos,$prosId,$newCliId)){
          goto insCli;
        }
        else{
          writeOnJournal($usuarioId,"Ha migrado con éxito los datos del prospecto id [".$prosId."] al cliente id [".$newCliId."]");
        }
      }
    }      
    $salida = "OK|| Los datos se han guardado con éxito";
  }  
  insCli:
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);               
  echo $salida;          
}

if(1==$hdnAction){  //si se actualiza cliente existente
  if(0>$cliId){
    $salida = "ERROR||No se recibió el id del cliente para su edición";
    log_write("ERROR: UPDATE-CLIENT: No se recibió el id del cliente ".$cliId,0);
    goto editClia;
  }
  log_write("DEBUG: UPDATE-CLIENT: Se va a actualizar los datos del cliente con id [".$cliId."]",1);

  $query2 = "SELECT razon_social,nombre_comercial,rfc_curp,tipo_persona,telefono,tel_movil,email,email_alt,estatus,".
            "origen,origen_otro,acta_constitutiva,fecha_acta,representante_id,domicilio_fiscal,colonia_fiscal,cp_fiscal,".
            "municipio_fiscal,estado_fiscal,descripcion ".
            "FROM clientes WHERE cliente_id=".$cliId;
  
  $query3 = "SELECT rel.*, cont.* FROM rel_origen_contacto AS rel, contactos AS cont WHERE rel.tipo_origen=1 AND rel.origen_id=".$cliId." AND rel.contacto_id=cont.contacto_id";
  
  $result2 = mysqli_query($db_modulos,$query2);
  
  log_write("DEBUG: UPDATE-CLIENT: ".$query2,0);
  
  //$result3 = mysqli_query($db_modulos,$query3);
  
  if(!$result2){
    log_write("ERROR: UPDATE-CLIENT: No se pudo obtener los datos del cliente",1);
    $salida = "ERROR|| Ocurrió un problema en la consulta de los datos para su actualización";
  }
  else{
    log_write("DEBUG: UPDATE-CLIENT: Se obtuvieron los datos de cliente. Listo para su actualización",1);
    
    $row = mysqli_fetch_assoc($result2);
    
    $diff = array_diff_assoc($row,$campos);
    
    if((1==$row['tipo_persona'])&&(2==$tipPer)){//Si cambió de física a moral se inserta representante
      if(!$repId = insertManager($reprArr)){               
        goto insCli;
      }
    }
    if((1==$tipPer)&&(2==$row['tipo_persona'])&&count(0==$repArr)){//si cambió de moral a física se elimina representante
      $repId = $row['representante_id'];
      $query4 = "DELETE FROM representante_legal WHERE representante_id=".$row['representante_id'];
      $repId = -1;
      $fecAct = "";
      $actCons = "";
      $emailAlt = "";
      $result4 = mysqli_query($db_modulos,$query4);
    }
    else{
      $repId = $row['representante_id'];
    } 
  
    $query = "UPDATE clientes SET razon_social = '".$razonSoc."', ".
                                 "nombre_comercial = '".$nomCom."', ".
                                 "rfc_curp = '".$rfc."', ".
                                 "tipo_persona = '".$tipPer."', ".
                                 "acta_constitutiva = '".$actCons."', ".
                                 "fecha_acta = '".$fecAct."', ".
                                 "representante_id = ".$repId.",".
                                 "telefono = '".$tel."', ".
                                 "tel_movil = '".$cell."', ".
                                 "domicilio_fiscal = '".$dom."', ".
                                 "colonia_fiscal = '".$col."', ".
                                 "cp_fiscal = ".$cp.", ".
                                 "municipio_fiscal = '".$mun."', ".
                                 "estado_fiscal = ".$state.", ".
                                 "email = '".$email."', ".
                                 "email_alt = '".$emailAlt."', ".
                                 "origen = ".$origin.", ".
                                 "origen_otro = '".$originOth."', ".
                                 "estatus = ".$status.", ".
                                 "descripcion = '".$desc."' ".
                                 "WHERE cliente_id = ".$cliId;                                     
                                 
    $result = mysqli_query($db_modulos,$query); 
    
    log_write("DEBUG: UPDATE-CLIENT: ".$query,1);
    
    if(!$result){
      log_write("ERROR: UPDATE-CLIENT: No se actualizaron los datos del cliente",1);
      $salida = "ERROR|| Ocurrió un problema en la actualización de los datos";
    }
    else{
      log_write("DEBUG: UPDATE-CLIENT: Se actualizaron los datos del cliente",1);
      foreach($diff as $key => $value){
        writeOnJournal($usuarioId,"Ha modificado el campo \"".$key."\" de [".$value."] a [".$campos[$key]."]"." para cliente id [".$cliId."]");
      }
      
      if(!updateContactsByArray($contArr,$query3,$salida,$cliId,1)){ 
        log_write("DEBUG: UPDATE-CLIENT: No se actualizaron los contactos de este cliente",1);
        $salida = "ERROR|| Ocurrió un problema al actualizarse la lista de los contactos";
      }
      else{  
        log_write("DEBUG: UPDATE-CLIENT: Los contactos para este cliente fueron actualizados",1);
        $salida = "OK|| Los datos se han actualizado con éxito";
      }
    }
  } 
  editClia:  
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);               
  echo $salida;   
}

}//fin else sesión


//función que cambia los ids asociados de los comentarios, cotizaciones y eventos del prospecto a sus datos como cliente, es decir migra los datos.
function migrateProspectData($db_modulos,$prosId,$newCliId){
  global $salida;
  global $usuarioId;
  
  if(0>=$prosId||0>=$newCliId){
    $salida = "Ocurrió un problema al obtener los ids de los registros para la actualización de sus datos adicionales";
    return false;
  }
  
  log_write("DEBUG: MIGRATE-PROSPECT-DATA: Se va a migrar los datos del prospecto id [".$prosId."] al cliente con id [".$newCliId."]",1);
  
  $queryCot = "UPDATE cotizaciones SET tipo_origen=1,origen_id=".$newCliId." WHERE tipo_origen=0 AND origen_id=".$prosId;
  
  $queryEve = "UPDATE calendario SET tipo_origen=1,origen_id=".$newCliId." WHERE tipo_origen=0 AND origen_id=".$prosId;
  
  $queryCon = "UPDATE rel_origen_contacto SET tipo_origen=1,origen_id=".$newCliId." WHERE tipo_origen=0 AND origen_id=".$prosId;
  
  $querySit = "UPDATE sitios SET tipo_origen=1,origen_id=".$newCliId.",usuario_alta=".$usuarioId." WHERE tipo_origen=0 AND origen_id=".$prosId;
  
  $queryDoc = "UPDATE documentos SET tipo_origen=1 WHERE tipo_origen=0 AND origen_id".$prosId;
  
  $queryCom1 = "SELECT * FROM comentarios_prospectos WHERE prospecto_id=".$prosId;
  
  
  
  log_write("DEBUG: MIGRATE-PROSPECT-DATA: ".$queryCot,1);
  log_write("DEBUG: MIGRATE-PROSPECT-DATA: ".$queryEve,1);
  log_write("DEBUG: MIGRATE-PROSPECT-DATA: ".$queryCon,1);
  log_write("DEBUG: MIGRATE-PROSPECT-DATA: ".$querySit,1);
  log_write("DEBUG: MIGRATE-PROSPECT-DATA: ".$queryCom1,1);
  log_write("DEBUG: MIGRATE-PROSPECT-DATA: ".$queryPros,1);
  
  $resultCot = mysqli_query($db_modulos,$queryCot);
  $resultEve = mysqli_query($db_modulos,$queryEve);
  $resultCon = mysqli_query($db_modulos,$queryCon);
  $resultSit = mysqli_query($db_modulos,$querySit);
  $resultCom1 = mysqli_query($db_modulos,$queryCom1);
  $resultDoc = mysqli_query($db_modulos,$queryDoc);
  
  if(!$resultCot){
    log_write("ERROR: MIGRATE-PROSPECT-DATA: Ocurrió un problema al migrar los datos de las cotizaciones",1);
    $salida = "ERROR||Ocurrió un problema al migrar los datos de las cotizaciones";
    return false;
  }
  if(!$resultEve){
    log_write("ERROR: MIGRATE-PROSPECT-DATA: Ocurrió un problema al migrar los datos de los eventos",1);
    $salida = "ERROR||Ocurrió un problema al migrar los datos de los eventos";
    return false;
  }
  if(!$resultCon){
    log_write("ERROR: MIGRATE-PROSPECT-DATA: Ocurrió un problema al migrar los datos existentes de los contactos",1);
    $salida = "ERROR||Ocurrió un problema al migrar los datos existentes de los contactos";
    return false;
  }
  if(!$resultSit){
    log_write("ERROR: MIGRATE-PROSPECT-DATA: Ocurrió un problema al migrar los datos existentes de los sitios",1);
    $salida = "ERROR||Ocurrió un problema al migrar los datos existentes de los sitios";
    return false;
  }
  if(!$resultCom1){
    log_write("ERROR: MIGRATE-PROSPECT-DATA: Ocurrió un problema al consultar los datos de los comentarios para su migración",1);
    $salida = "ERROR||Ocurrió un problema al consultar los datos de los comentarios para su migración";
    return false;
  }
  if(!$resultDoc){
    log_write("ERROR: MIGRATE-PROSPECT-DATA: Ocurrió un problema al migrar los datos existentes de los documentos",1);
    $salida = "ERROR||Ocurrió un problema al migrar los datos existentes de los documentos";
    return false;
  }
  else{
    if(0<mysqli_num_rows($resultCom1)){
      while($rowCom = mysqli_fetch_assoc($resultCom1)){
        $queryCom2 = "INSERT INTO comentarios_clientes(cliente_id,comentario,fecha_alta,usuario_alta) VALUES(".$newCliId.",'".$rowCom['comentario']."','".$rowCom['fecha_alta']."',".$rowCom['usuario_alta'].")";
        
        log_write("DEBUG: MIGRATE-PROSPECT-DATA: ".$queryCom2,1);
        
        $resultCom2 = mysqli_query($db_modulos,$queryCom2);
        
        if(!$resultCom2){
          log_write("ERROR: MIGRATE-PROSPECT-DATA: Ocurrió un problema al transferir los datos de los comentarios",1);
          $salida = "ERROR||Ocurrió un problema al transferir los datos de los comentarios";
          return false;
        }
      }
    }    
  }
  log_write("DEBUG: MIGRATE-PROSPECT-DATA: Datos migrados correctamente, se ocultará prospecto",1); 
  
  //$queryProDel = "DELETE FROM prospectos WHERE prospecto_id=".$prosId;  
  $queryProDel = "UPDATE prospectos SET estatus = 0";
  
  $resultProDel = mysqli_query($db_modulos,$queryProDel);
  
  if(!$resultProDel){
    log_write("ERROR: MIGRATE-PROSPECT-DATA: Ocurrió un problema borrar los datos de prospecto",1);
    $salida = "ERROR||Los datos se migraron correctamente, pero no se pudieron borrar los registros sobrantes del prospecto. Debe borrarlos manualmente desde el módulo de prospectos";
    return false;
  }
  else{
    log_write("OK: MIGRATE-PROSPECT-DATA: Los datos del prospecto migrado id [".$prosId."] ha sido ocultado",1);
  } 
   
  log_write("OK: MIGRATE-PROSPECT-DATA: Los datos se migraron correctamente",1); 
  return true;
}
?>
