$(document).ready(function () {
    // openPromo();
});
var ctrlView = 0;

function openNewService() {
    var d = new Date();
    var dia = d.getDate();
    var mes = ((d.getMonth() + 1));
    var anio = d.getFullYear();

    var fechaActual = anio + "-" + mes + "-" + dia;
    $("#mod_serviceInsert").modal("show");
    // limpiamos todos los campos
    $("#txt_serCve").val("");
    $("#txt_serDesc").val("");
    $("#select_tipoServicio").val("");
    $("#select_tipoCobro").val("");
    $("#txt_serSubida").val("");
    $("#txt_serBajada").val("");
    $("#txt_serDistMax").val("");
    $("#txt_serPrecBase").val("");
    $("#txt_serPrecMedio").val("");
    $("#txt_serPrecLista").val("");
    $("#txt_inscripcion").val("");
    $("#txt_kilometros").val("");
    $("#txt_inicioVigencia").datepicker().val(fechaActual);
    $("#txt_finVigencia").datepicker().val(fechaActual);
    // subsidios
    $("#txt_sub12m").val("");
    $("#txt_sub24m").val("");
    $("#txt_sub36m").val("");
    // precios
    $("#txt_precio12m").val("");
    $("#txt_precio24m").val("");
    $("#txt_precio36m").val("");
    // folios
    $("#txt_folio_ift12m").val("");
    $("#txt_folio_ift24m").val("");
    $("#txt_folio_ift36m").val("");
    // cuota
    $("#txt_cuotaIns12m").val("");
    $("#txt_cuotaIns24m").val("");
    $("#txt_cuotaIns36m").val("");
    // costo
    $("#txt_costo12m").val("");
    $("#txt_costo24m").val("");
    $("#txt_costo36m").val("");
    // adecuaciones cancelacion
    $("#txt_cargoAdecCancel12m").val("");
    $("#txt_cargoAdecCancel24m").val("");
    $("#txt_cargoAdecCancel36m").val("");
    // cargo adecuaciones
    $("#txt_cargoAdec12m").val("");
    $("#txt_cargoAdec24m").val("");
    $("#txt_cargoAdec36m").val("");
    // adecuaciones
    $("#txt_Adec12m").val("");
    $("#txt_Adec24m").val("");
    $("#txt_Adec36m").val("");

    $("#txt_sub6m").val("");
    $("#txt_precio6m").val("");
    $("#txt_folio_ift6m").val("");
    $("#txt_costo6m").val("");
    $("#txt_Adec6m").val("");

    $("#txt_sub18m").val("");
    $("#txt_precio18m").val("");
    $("#txt_folio_ift18m").val("");
    $("#txt_costo18m").val("");
    $("#txt_Adec18m").val("");

    $("#txt_sub30m").val("");
    $("#txt_precio30m").val("");
    $("#txt_folio_ift30m").val("");
    $("#txt_costo30m").val("");
    $("#txt_Adec30m").val("");

    // obtenemos todos las listas qe se necesitan
    getTipoCobro();
    getTipoServicio();
    getRegiones();
    setModalResponsive("mod_serviceInsert");
}
function getTipoCobro(mod = "", idCobro = 0) {
    $.ajax({
        type: "POST",
        url: "xml.php",
        data: {
            "action": "getTipoCobro"
        },
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (msg) {
            var resp = (msg).trim().split("||");
            if ("OK" == resp[0]) {
                $("#select_tipoCobro"+mod).html(resp[1]);
            }
            if(mod == "Mod"){
              $('#select_tipoCobroMod option[value="' + idCobro + '"]').prop('selected', true);
            }

        }
    }); 
}
function getTipoServicio( mod = "", idser = 0){
    $.ajax({
        type: "POST",
        url: "xml.php",
        data: {
            "action": "getTipoServicio"
        },
        beforeSend: function () {},
        complete: function () {},
        success: function (msg) {
            var resp = (msg).trim().split("||");
            if ("OK" == resp[0]) {
                $("#select_tipoServicio"+mod).html(resp[1]);
            }
            if(mod == "Mod"){
              $('#select_tipoServicioMod option[value="' + idser + '"]').prop('selected', true);
            }
        }
    });
    
}
function getRegiones(){
     $.ajax({
         type: "POST",
         url: "xml.php",
         data: {
             "action": "getRegiones"
         },
         beforeSend: function () {},
         complete: function () {},
         success: function (msg) {
             var resp = (msg).trim().split("||");
             if ("OK" == resp[0]) {
                 $("#regiones").html(resp[1]);
             }
         }
     });
}
function getServiceDetails(id) {
    $.ajax({
        type: 'POST',
        url: 'xml.php',
        data: {
            "action": "serviceDetails",
            "serId": id,
            "option": 0
        },
        beforeSend: function () {
            setLoadDialog(0, "Consultando datos de Servicios");
        },
        complete: function () {
            setLoadDialog(1, "");
        },
        success: function (msg) {
            $("#div_serviceDetails").html(msg);
            $("#mod_serviceDetails").modal("show");

            setModalResponsive("mod_serviceDetails");
        }
    });
}
function saveNewService(){
    var regionesId = [];
    $("input[type=checkbox]:checked").each(function () {
        regionesId.push(this.value);
    });
    if (validateDataNewService(regionesId)) {
      $.ajax({
        type: "POST",
        url: "xml.php",
        data: {
          action: "saveServiceNew",
          servicio: $("#txt_serCve").val(),
          descripcion: $("#txt_serDesc").val(),
          tipoServicio: $("#select_tipoServicio").val(),
          tipoCobro: $("#select_tipoCobro").val(),
          regArr: regionesId,
          subida: $("#txt_serSubida").val(),
          bajada: $("#txt_serBajada").val(),
          distanciaMax: $("#txt_serDistMax").val(),
          tiempoInstall: $("#txt_tiempoInstal").val(),
          precioBase: $("#txt_serPrecBase").val(),
          precioMedio: $("#txt_serPrecMedio").val(),
          precioLista: $("#txt_serPrecLista").val(),
          inscripcion: $("#txt_inscripcion").val(),
          kilometros: $("#txt_kilometros").val(),
          inicioVigencia: $("#txt_inicioVigencia").val(),
          finVigencia: $("#txt_finVigencia").val(),
          sub6m: $("#txt_sub6m").val(),
          sub12m: $("#txt_sub12m").val(),
          sub24m: $("#txt_sub24m").val(),
          sub36m: $("#txt_sub36m").val(),
          precio6m: $("#txt_precio6m").val(),
          precio12m: $("#txt_precio12m").val(),
          precio24m: $("#txt_precio24m").val(),
          precio36m: $("#txt_precio36m").val(),
          ift6m: $("#txt_folio_ift6m").val(),
          ift12m: $("#txt_folio_ift12m").val(),
          ift24m: $("#txt_folio_ift24m").val(),
          ift36m: $("#txt_folio_ift36m").val(),
          costo6m: $("#txt_costo6m").val(),
          costo12m: $("#txt_costo12m").val(),
          costo24m: $("#txt_costo24m").val(),
          costo36m: $("#txt_costo36m").val(),
          cargoAdecCancel12m: $("#txt_cargoAdecCancel12m").val(),
          cargoAdecCancel24m: $("#txt_cargoAdecCancel24m").val(),
          cargoAdecCancel36m: $("#txt_cargoAdecCancel36m").val(),
          cargoAdec6m: $("#txt_cargoAdec6m").val(),
          cargoAdec12m: $("#txt_cargoAdec12m").val(),
          cargoAdec24m: $("#txt_cargoAdec24m").val(),
          cargoAdec36m: $("#txt_cargoAdec36m").val(),
          adec6m: $("#txt_Adec6m").val(),
          adec12m: $("#txt_Adec12m").val(),
          adec24m: $("#txt_Adec24m").val(),
          adec36m: $("#txt_Adec36m").val(),
          sub18m: $("#txt_sub18m").val(),
          precio18m: $("#txt_precio18m").val(),
          ift18m: $("#txt_folio_ift18m").val(),
          costo18m: $("#txt_costo18m").val(),
          adec18m: $("#txt_Adec18m").val(),
          sub30m:$("#txt_sub30m").val(),
          precio30m:$("#txt_precio30m").val(),
          ift30m:$("#txt_folio_ift30m").val(),
          costo30m:$("#txt_costo30m").val(),
          adec30m:$("#txt_Adec30m").val(),
          cuotaInstall12:$("#txt_cuotaIns12m").val(),
          cuotaInstall24:$("#txt_cuotaIns24m").val(),
          cuotaInstall36:$("#txt_cuotaIns36m").val()
        },
        beforeSend: function() {
          setLoadDialog(0, "Guardando servicio");
        },
        complete: function() {
          setLoadDialog(1, "");
        },
        success: function(msg) {
          var resp = msg.trim().split("||");
          if ("OK" == resp[0]) {
            printSuccessMsg("msg_validateResp", resp[1]);
          } else {
            printErrorMsg("msg_validateResp", resp[1]);
          }
          setTimeout(function() {
            $("#msg_validateResp").html("");
          }, 5000); //5000ms =5s
        }
      });
    } else {
      setTimeout(function() {
        $("#msg_validateResp").html("");
      }, 18000); // 18000ms = 18s
    }
}
function getListServices(){
    $.ajax({
        type: "POST",
        url: "xml.php",
        data: {
        "action":    "mainLoad"
        },
        beforeSend: function(){
        setLoadDialog(0,"Cargando Servicios...");
        },
        complete: function(){
        setLoadDialog(1,"");
        },
        success: function(msg){
                $("#div_prices").html(msg);
                $("#tbl_pricesTable").DataTable({
                    language: datatableespaniol,
                });
        }  
    });   
}
function validateDataNewService(reg){
    // var regExPrec = /^([0-9]{1,5}(.[0-9]{1,2}))$/;
    var regExPrec = /^([0-9,]{0,9}(.[0-9]{0,2}))$/;
    var regExFolio = /^([0-9]{0,6})$/;
    var regExNum = /^([0-9]{1,4})$/;
    var ExpDesc = /^([a-z0-9 ,.-]{1,250})$/gmi;
    var ExpSer = /^([a-z0-9 ,.-]{1,120})$/gmi;
    var msgAlert = "";
    var boolSer,
      boolDetalles,
      boolprecios,
      bool6m,
      bool12m,
      bool18m,
      bool24m,
      bool30m,
      bool36m;
    // evaluacion del controlador del servicio
    if (reg.length > 0 && ExpSer.test($("#txt_serCve").val()) && ExpDesc.test($("#txt_serDesc").val()) && $("#select_tipoServicio").val() != "" && $("#select_tipoCobro").val() != "") {
        boolSer = true;
    }else{
        boolSer = false;
        msgAlert = msgAlert + "Existen campos no validos en la seccion Servicio y Descripcion <br>";
    }
    // evaluacion del controlador de Detalles
    if (regExNum.test($("#txt_serBajada").val()) && regExNum.test($("#txt_serSubida").val()) && regExNum.test($("#txt_serDistMax").val()) && regExNum.test($("#txt_kilometros").val()) && regExPrec.test($("#txt_serPrecBase").val()) && regExPrec.test($("#txt_serPrecMedio").val()) && regExPrec.test($("#txt_serPrecLista").val()) && regExPrec.test($("#txt_inscripcion").val()) && $("#txt_tiempoInstal").val() != "" && ($("#txt_inicioVigencia").val() <= $("#txt_finVigencia").val())){
        boolDetalles = true;
    }else{
        boolDetalles = false;
        msgAlert = msgAlert + "Existen datos no validos en la seccion Detalles del Servicio <br>";
    }
    // evaluacion del controlador de precios

    //   regExPrec.test($("#txt_cargoAdec12m").val()) &&
    //   regExPrec.test($("#txt_cargoAdec24m").val()) &&
    //   regExPrec.test($("#txt_cargoAdec36m").val()) &&


    //   regExPrec.test($("#txt_cargoAdec6m").val()) &&
    //   regExPrec.test($("#txt_cargoAdec18m").val()) &&
    //   regExPrec.test($("#txt_cargoAdec30m").val()) &&


    if (
      regExPrec.test($("#txt_sub6m").val()) &&
      regExPrec.test($("#txt_precio6m").val()) &&
      regExFolio.test($("#txt_folio_ift6m").val()) &&
      regExPrec.test($("#txt_costo6m").val()) &&
      regExPrec.test($("#txt_Adec6m").val())
    ) {
      bool6m = true;
    } else {
      bool6m = false;
      msgAlert =
        msgAlert + "Existen campos no validos en la seccion Precios y Vigencias para los precios de 6 meses. <br>";
    }

    if (
      regExPrec.test($("#txt_sub12m").val()) &&
      regExPrec.test($("#txt_precio12m").val()) &&
      regExFolio.test($("#txt_folio_ift12m").val()) &&
      regExPrec.test($("#txt_costo12m").val()) &&
      regExPrec.test($("#txt_cargoAdecCancel12m").val()) &&
      regExPrec.test($("#txt_Adec12m").val()) &&
      regExPrec.test($("#txt_cuotaIns12m").val())
    ) {
      bool12m = true;
    } else {
      bool12m = false;
      msgAlert =
        msgAlert +
        "Existen campos no validos en la seccion Precios y Vigencias para los precios de 12 meses. <br>";
    }

    if (
      regExPrec.test($("#txt_sub18m").val()) &&
      regExPrec.test($("#txt_precio18m").val()) &&
      regExFolio.test($("#txt_folio_ift18m").val()) &&
      regExPrec.test($("#txt_costo18m").val()) &&
      regExPrec.test($("#txt_Adec18m").val())
    ) {
      bool18m = true;
    } else {
      bool18m = false;
      msgAlert =
        msgAlert +
        "Existen campos no validos en la seccion Precios y Vigencias para los precios de 18 meses. <br>";
    }

    if (
      regExPrec.test($("#txt_sub24m").val()) &&
      regExPrec.test($("#txt_precio24m").val()) &&
      regExFolio.test($("#txt_folio_ift24m").val()) &&
      regExPrec.test($("#txt_costo24m").val()) &&
      regExPrec.test($("#txt_cargoAdecCancel24m").val()) &&
      regExPrec.test($("#txt_Adec24m").val()) &&
      regExPrec.test($("#txt_cuotaIns24m").val())
    ) {
      bool24m = true;
    } else {
      bool24m = false;
      msgAlert =
        msgAlert +
        "Existen campos no validos en la seccion Precios y Vigencias para los precios de 24 meses. <br>";
    }

    if (
      regExPrec.test($("#txt_sub30m").val()) &&
      regExPrec.test($("#txt_precio30m").val()) &&
      regExFolio.test($("#txt_folio_ift30m").val()) &&
      regExPrec.test($("#txt_costo30m").val()) &&
      regExPrec.test($("#txt_Adec30m").val())
    ) {
      bool30m = true;
    } else {
      bool30m = false;
      msgAlert =
        msgAlert +
        "Existen campos no validos en la seccion Precios y Vigencias para los precios de 30 meses. <br>";
    }

    if (
      regExPrec.test($("#txt_sub36m").val()) &&
      regExPrec.test($("#txt_precio36m").val()) &&
      regExFolio.test($("#txt_folio_ift36m").val()) &&
      regExPrec.test($("#txt_costo36m").val()) &&
      regExPrec.test($("#txt_cargoAdecCancel36m").val()) &&
      regExPrec.test($("#txt_Adec36m").val()) &&
      regExPrec.test($("#txt_cuotaIns36m").val())
    ) {
      bool36m = true;
    } else {
      bool36m = false;
      msgAlert =
        msgAlert +
        "Existen campos no validos en la seccion Precios y Vigencias para los precios de 36 meses. <br>";
    }


    // evaluacion de todos los controladores
    if (
      (boolSer && boolDetalles && bool6m && bool12m && bool18m && bool24m && bool30m && bool36m)
    ) {
      return true;
    } else {
        printErrorMsg("msg_validateResp", "<br>" + msgAlert);
    //   alert(msgAlert);
      return false;
    }
}
function openPromo(){
  // abrir modal para insertar nueva promocion en base a un servicio
  $("#mod_promo").modal("show");
  $("#msg_respPromo").html("");
  cleanPromo();
  // setModalResponsive("mod_promo");
  var date = new Date();
  $('#selectAplicaAnio option[value="'+(date.getFullYear())+'"]').prop('selected', true);
  $('#selectAplicaMes option[value="'+(date.getMonth()+1)+'"]').prop('selected', true);
  getRegionesList();
  $(document.body).on('change', '#selRegionesList', function () {
     getServicesToPromo($(this).val());
     cleanPromo();
   });
  getServicesToPromo(1);
   $(document.body).on('change', '#selectServiceBase', function () {
     getPricesServicesPromo($(this).val());
   });
}
function savePromo(){
  // tomar, validar y enviar datos de la promo para almacenarse en la base de datos
  if(!validateDataPromo()){
    printErrorMsg("msg_respPromo", "El formato de los precios ingresados es incorrecto, NO puede incluir caracteres especiales ni letras <br> El formato debe ser la cantidad (000) seguido de un punto (.) y seguido de dos decimales (00). <br> ejemplo: <br> 900.00 ó 900.50");
  }else{
    var fechaVigenciaIni = new Date($('#selectAplicaAnio').val(),$('#selectAplicaMes').val(),'01');
    var fechaVigenciaFin = new Date($('#selectAplicaAnio').val(),$('#selectAplicaMes').val(),'0');
    fechaVigenciaIni = $('#selectAplicaAnio').val()+"-"+$('#selectAplicaMes').val()+"-01 00:00:00";
    fechaVigenciaFin = $('#selectAplicaAnio').val()+"-"+$('#selectAplicaMes').val()+"-"+fechaVigenciaFin.getDate()+" 23:59:59";
    
    $.ajax({
    type: "POST",
    url: "xml.php",
    data: {
      "action": "saveNewPromo",
      "vigIni": fechaVigenciaIni,
      "vigFin": fechaVigenciaFin,
      "idSerBase": $("#idServiceToPromo").html(),
      "precio12m": $("#pricePromo12m").val(),
      "precio24m": $("#pricePromo24m").val(),
      "precio36m": $("#pricePromo36m").val(),
      "mes": $('#selectAplicaMes').val()
    },
    beforeSend: function () {
      setLoadDialog(0, "Guardando Promocion..");
    },
    complete: function () {
      setLoadDialog(1, "");
    },
    success: function (msg) {
      var resp = msg.trim().split("||");
      if("OK"==resp[0]){
        printSuccessMsg("msg_respPromo", resp[1]);
        
      }else{
        printErrorMsg("msg_respPromo", resp[1]);
      }
       
    }
  });
  }
}
function getServicesToPromo(idRegion){
  $.ajax({
    type: "POST",
    url: "xml.php",
    data: {
      "action": "getServicesToPromo",
      "idReg": idRegion
    },
    beforeSend: function () {
      setLoadDialog(0, "Cargando Lista de Servicios..");
    },
    complete: function () {
      setLoadDialog(1, "");
    },
    success: function (msg) {
      $("#selectServiceBase").html(msg);
    }
  });
}
function getPricesServicesPromo(idPrice){
// obtener los preciospara als vigencias del servicio
//  $("#test").html("Entro a getPricesServicesPromo con Id: " + idPrice);
  $.ajax({
    type: "POST",
    url: "xml.php",
    data: {
      "action": "getPricesServicePromo",
      "id_servicio": idPrice
    },
    beforeSend: function () {
      setLoadDialog(0, "Cargando Lista de Servicios..");
    },
    complete: function () {
      setLoadDialog(1, "");
    },
    success: function (msg) {
      $("#input_prices").html(msg);
    }
  });
}
function validateDataPromo(){
  // vlidar datos en los campos de la promocion
  var regExPrec = /^([0-9,]{0,9}(.[0-9]{0,2}))$/;
  if(regExPrec.test($("#pricePromo12m").val()) &&
     regExPrec.test($("#pricePromo24m").val()) &&
     regExPrec.test($("#pricePromo36m").val())
  ){
    return true;
  }else{
    return false;
  }
}
function getRegionesList(){
     $.ajax({
         type: "POST",
         url: "xml.php",
         data: {
             "action": "getRegionesList"
         },
         beforeSend: function () {},
         complete: function () {},
         success: function (msg) {
             var resp = (msg).trim().split("||");
             if ("OK" == resp[0]) {
                 $("#selRegionesList").html(resp[1]);
             }
         }
     });
}
function cleanPromo(){
  $("#input_prices").html("");
}
function fill_6_18_30(){
  $("#txt_sub6m").val("0.00");
  $("#txt_precio6m").val("0.00");
  $("#txt_folio_ift6m").val("000000");
  $("#txt_costo6m").val("0.00");
  $("#txt_Adec6m").val("0.00");

  $("#txt_sub18m").val("0.00");
  $("#txt_precio18m").val("0.00");
  $("#txt_folio_ift18m").val("000000");
  $("#txt_costo18m").val("0.00");
  $("#txt_Adec18m").val("0.00");

  $("#txt_sub30m").val("0.00");
  $("#txt_precio30m").val("0.00");
  $("#txt_folio_ift30m").val("000000");
  $("#txt_costo30m").val("0.00");
  $("#txt_Adec30m").val("0.00");
}
function editService(idservicio){
  // editar servicio 
  $.ajax({
    type: "POST",
    url: "xml.php",
    data: {
      "action": "getServiceToEdit",
      "idServicio": idservicio
    },
    beforeSend: function () {
      setLoadDialog(0, "Cargando datos del servicio");
    },
    complete: function () {
      setLoadDialog(1, "");
    },
    success: function (msg) {
      var resp = (msg).trim().split("||");
      if ("OK" == resp[0]) {
        resp = JSON.parse(resp[1]);
        $("#mod_serviceMod").modal("show");
        setModalResponsive("mod_serviceMod");
        $("#servicioIdMod").html(resp['servicio_id']);
        $("#txt_serCveMod").val(resp['servicio']);
        $("#txt_serDescMod").val(resp['descripcion']);
        $('#txt_tiempoInstalMod option[value="' + resp['tiempo_instalacion'] + '"]').prop('selected', true);
        getTipoServicio("Mod", resp['tipo_servicio']);
        getTipoCobro("Mod", resp['tipo_cobro']);
        $("#txt_serSubidaMod").val(resp['subida']);
        $("#txt_serBajadaMod").val(resp['bajada']);
        $("#txt_serDistMaxMod").val(resp['distancia_max']);
        $("#txt_serPrecBaseMod").val(resp['precio_base']);
        $("#txt_serPrecMedioMod").val(resp['precio_medio']);
        $("#txt_serPrecListaMod").val(resp['precio_lista']);
        $("#txt_inscripcionMod").val(resp['inscripcion']);
        $("#txt_kilometrosMod").val(resp['kilometros']);
        var fechaInicio = resp['servicio_vigencia_inicio'];
        var fechaFin = resp['servicio_vigencia_fin'];
        fechaInicio = (fechaInicio).trim().split(" ");
        fechaInicio = fechaInicio[0];
        fechaFin = (fechaFin).trim().split(" ");
        fechaFin = fechaFin[0];
        // console.log(fechaInicio, fechaFin);
        $("#txt_inicioVigenciaMod").datepicker().val(fechaInicio);
        $("#txt_finVigenciaMod").datepicker().val(fechaFin);
        $("#txt_sub12mMod").val(resp['sub_12m']);
        $("#txt_sub24mMod").val(resp['sub_24m']);
        $("#txt_sub36mMod").val(resp['sub_36m']);
        $("#txt_precio12mMod").val(resp['precio_12m']);
        $("#txt_precio24mMod").val(resp['precio_24m']);
        $("#txt_precio36mMod").val(resp['precio_36m']);
        $("#txt_folio_ift12mMod").val(resp['folio_ift_12m']);
        $("#txt_folio_ift24mMod").val(resp['folio_ift_24m']);
        $("#txt_folio_ift36mMod").val(resp['folio_ift_36m']);
        $("#txt_cuotaIns12mMod").val(resp['cuota_instalacion_12m']);
        $("#txt_cuotaIns24mMod").val(resp['cuota_instalacion_24m']);
        $("#txt_cuotaIns36mMod").val(resp['cuota_instalacion_36m']);
        $("#txt_costo12mMod").val(resp['costo_12m']);
        $("#txt_costo24mMod").val(resp['costo_24m']);
        $("#txt_costo36mMod").val(resp['costo_36m']);
        $("#txt_cargoAdecCancel12mMod").val(resp['cargo_adecuacion_cancelacion_12m']);
        $("#txt_cargoAdecCancel24mMod").val(resp['cargo_adecuacion_cancelacion_24m']);
        $("#txt_cargoAdecCancel36mMod").val(resp['cargo_adecuacion_cancelacion_36m']);
        $("#txt_cargoAdec12mMod").val(resp['cargo_adecuacion_12m']);
        $("#txt_cargoAdec24mMod").val(resp['cargo_adecuacion_24m']);
        $("#txt_cargoAdec36mMod").val(resp['cargo_adecuacion_36m']);
        $("#txt_Adec12mMod").val(resp['adec_12m']);
        $("#txt_Adec24mMod").val(resp['adec_24m']);
        $("#txt_Adec36mMod").val(resp['adec_36m']);
        $("#txt_sub6mMod").val(resp['sub_6m']);
        $("#txt_precio6mMod").val(resp['precio_6m']);
        $("#txt_folio_ift6mMod").val(resp['folio_ift_6m']);
        $("#txt_costo6mMod").val(resp['costo_6m']);
        $("#txt_Adec6mMod").val(resp['adec_6m']);
        $("#txt_sub18mMod").val(resp['sub_18m']);
        $("#txt_precio18mMod").val(resp['precio_18m']);
        $("#txt_folio_ift18mMod").val(resp['folio_ift_18m']);
        $("#txt_costo18mMod").val(resp['costo_18m']);
        $("#txt_Adec18mMod").val(resp['adec_18m']);
        $("#txt_sub30mMod").val(resp['sub_30m']);
        $("#txt_precio30mMod").val(resp['precio_30m']);
        $("#txt_folio_ift30mMod").val(resp['folio_ift_30m']);
        $("#txt_costo30mMod").val(resp['costo_30m']);
        $("#txt_Adec30mMod").val(resp['adec_30m']);
        // servicio_id, 
        // servicio, 
        // descripcion, 
        // tipo_servicio, 
        // bajada, 
        // subida, 
        // distancia_max, 
        // tiempo_instalacion, 
        // precio_base, 
        // precio_medio, 
        // precio_lista, 
        // inscripcion, 
        // tipo_cobro, 
        // kilometros, 
        // vigente, 
        // servicio_vigencia_inicio, 
        // servicio_vigencia_fin, *
        // promo, *
        // region_id, *
        // precio_6m, *
        // cargo_adecuacion_6m, *
        // costo_6m, 
        // sub_6m, 
        // adec_6m, 
        // folio_ift_6m, 
        // precio_12m, 
        // cuota_instalacion_12m, 
        // cargo_adecuacion_12m, 
        // cargo_adecuacion_cancelacion_12m, 
        // costo_12m, 
        // sub_12m,* 
        // adec_12m, 
        // folio_ift_12m, 
        // precio_18m, 
        // cargo_adecuacion_18m, 
        // costo_18m, 
        // sub_18m, 
        // adec_18m, 
        // folio_ift_18m, 
        // precio_24m, 
        // cuota_instalacion_24m, 
        // cargo_adecuacion_24m, 
        // cargo_adecuacion_cancelacion_24m, 
        // costo_24m, 
        // sub_24m, * 
        // adec_24m, 
        // folio_ift_24m, 
        // precio_30m, 
        // cargo_adecuacion_30m, 
        // costo_30m, 
        // sub_30m, 
        // adec_30m, 
        // folio_ift_30m, 
        // precio_36m, 
        // cuota_instalacion_36m, 
        // cargo_adecuacion_36m, 
        // cargo_adecuacion_cancelacion_36m, 
        // costo_36m, 
        // sub_36m, 
        // adec_36m, 
        // folio_ift_36m
        
      }
    }
  });
}
function saveModService(){
  if(!validateModService()){
    // console.log($("#servicioIdMod").html());
    printErrorMsg("msg_serviceMod", "Exsiten datos NO validos en el los campos del formulario");
    setTimeout(function () {
      $("#msg_serviceMod").html("");
    }, 5000); //5000ms =5s
  }else{
    $.ajax({
      type: "POST",
      url: "xml.php",
      data: {
        "action": "updateService",
        "servicio_id": $("#servicioIdMod").html(),
        "servicio": $("#txt_serCveMod").val(),
        "descripcion": $("#txt_serDescMod").val(),
        "tipo_servicio": $("#select_tipoServicioMod").val(),
        "subida": $("#txt_serSubidaMod").val(),
        "bajada": $("#txt_serBajadaMod").val(),
        "distancia_max": $("#txt_serDistMaxMod").val(),
        "tiempo_instalacion": $('#txt_tiempoInstalMod').val(),
        "precio_base": $("#txt_serPrecBaseMod").val(),
        "precio_medio": $("#txt_serPrecMedioMod").val(),
        "precio_lista": $("#txt_serPrecListaMod").val(),
        "inscripcion": $("#txt_inscripcionMod").val(),
        "tipo_cobro": $("#select_tipoCobroMod").val(),
        "kilometros": $("#txt_kilometrosMod").val(),
        "servicio_vigencia_inicio" : $("#txt_inicioVigenciaMod").val(),
        "servicio_vigencia_fin": $("#txt_finVigenciaMod").val(),
        "sub_12m": $("#txt_sub12mMod").val(),
        "sub_24m": $("#txt_sub24mMod").val(),
        "sub_36m": $("#txt_sub36mMod").val(),
        "precio_12m": $("#txt_precio12mMod").val(),
        "precio_24m": $("#txt_precio24mMod").val(),
        "precio_36m": $("#txt_precio36mMod").val(),
        "folio_ift_12m" : $("#txt_folio_ift12mMod").val(),
        "folio_ift_24m" : $("#txt_folio_ift24mMod").val(),
        "folio_ift_36m" : $("#txt_folio_ift36mMod").val(),
        "cuota_instalacion_12m" : $("#txt_cuotaIns12mMod").val(),
        "cuota_instalacion_24m" : $("#txt_cuotaIns24mMod").val(),
        "cuota_instalacion_36m" : $("#txt_cuotaIns36mMod").val(),
        "costo_12m" : $("#txt_costo12mMod").val(),
        "costo_24m" : $("#txt_costo24mMod").val(),
        "costo_36m" : $("#txt_costo36mMod").val(),
        "cargo_adecuacion_cancelacion_12m" : $("#txt_cargoAdecCancel12mMod").val(),
        "cargo_adecuacion_cancelacion_24m" : $("#txt_cargoAdecCancel24mMod").val(),
        "cargo_adecuacion_cancelacion_36m" : $("#txt_cargoAdecCancel36mMod").val(),
        "adec_12m" : $("#txt_Adec12mMod").val(),
        "adec_24m" : $("#txt_Adec24mMod").val(),
        "adec_36m" : $("#txt_Adec36mMod").val(),
        "sub_6m": $("#txt_sub6mMod").val(),
        "precio_6m": $("#txt_precio6mMod").val(),
        "folio_ift_6m": $("#txt_folio_ift6mMod").val(),
        "costo_6m": $("#txt_costo6mMod").val(),
        "adec_6m": $("#txt_Adec6mMod").val(),
        "sub_18m": $("#txt_sub18mMod").val(),
        "precio_18m": $("#txt_precio18mMod").val(),
        "folio_ift_18m": $("#txt_folio_ift18mMod").val(),
        "costo_18m": $("#txt_costo18mMod").val(),
        "adec_18m": $("#txt_Adec18mMod").val(),
        "sub_30m": $("#txt_sub30mMod").val(),
        "precio_30m": $("#txt_precio30mMod").val(),
        "folio_ift_30m": $("#txt_folio_ift30mMod").val(),
        "costo_30m": $("#txt_costo30mMod").val(),
        "adec_30m": $("#txt_Adec30mMod").val()
      },
      beforeSend: function () {
        setLoadDialog(0, "Actualizando datos del servicio..");
      },
      complete: function () {
        setLoadDialog(1, "");
      },
      success: function (msg) {
        var resp = (msg).trim().split("||");
        if ("OK" == resp[0]) {
          printSuccessMsg("msg_serviceMod",resp[1]);
          setTimeout(function () {
            $("#msg_serviceMod").html("");
          }, 5000); //5000ms =5s
        }
      }
    });
  }
}
function validateModService() {
  // var regExPrec = /^([0-9]{1,5}(.[0-9]{1,2}))$/;
  var regExPrec = /^([0-9,]{0,9}(.[0-9]{0,2}))$/;
  var regExFolio = /^([0-9]{0,6})$/;
  var regExNum = /^([0-9]{1,4})$/;
  var ExpDesc = /^([a-z0-9 ,.-]{1,250})$/gmi;
  var ExpSer = /^([a-z0-9 ,.()-]{1,120})$/gmi;
  var msgAlert = "";
  var boolSer,
    boolDetalles,
    boolprecios,
    bool6m,
    bool12m,
    bool18m,
    bool24m,
    bool30m,
    bool36m;
  // evaluacion del controlador del servicio
  if (ExpSer.test($("#txt_serCveMod").val()) && ExpDesc.test($("#txt_serDescMod").val()) && $("#select_tipoServicioMod").val() != "" && $("#select_tipoCobroMod").val() != "") {
    boolSer = true;
  } else {
    boolSer = false;
    msgAlert = msgAlert + "Existen campos no validos en la seccion Servicio y Descripcion <br>";
  }
  // evaluacion del controlador de Detalles
  if (regExNum.test($("#txt_serBajadaMod").val()) && regExNum.test($("#txt_serSubidaMod").val()) && regExNum.test($("#txt_serDistMaxMod").val()) && regExNum.test($("#txt_kilometrosMod").val()) && regExPrec.test($("#txt_serPrecBaseMod").val()) && regExPrec.test($("#txt_serPrecMedioMod").val()) && regExPrec.test($("#txt_serPrecListaMod").val()) && regExPrec.test($("#txt_inscripcionMod").val()) && $("#txt_tiempoInstalMod").val() != "" && ($("#txt_inicioVigenciaMod").val() <= $("#txt_finVigenciaMod").val())) {
    boolDetalles = true;
  } else {
    boolDetalles = false;
    msgAlert = msgAlert + "Existen datos no validos en la seccion Detalles del Servicio <br>";
  }

  if (
    regExPrec.test($("#txt_sub6mMod").val()) &&
    regExPrec.test($("#txt_precio6mMod").val()) &&
    regExFolio.test($("#txt_folio_ift6mMod").val()) &&
    regExPrec.test($("#txt_costo6mMod").val()) &&
    regExPrec.test($("#txt_Adec6mMod").val())
  ) {
    bool6m = true;
  } else {
    bool6m = false;
    msgAlert =
      msgAlert + "Existen campos no validos en la seccion Precios y Vigencias para los precios de 6 meses. <br>";
  }

  if (
    regExPrec.test($("#txt_sub12mMod").val()) &&
    regExPrec.test($("#txt_precio12mMod").val()) &&
    regExFolio.test($("#txt_folio_ift12mMod").val()) &&
    regExPrec.test($("#txt_costo12mMod").val()) &&
    regExPrec.test($("#txt_cargoAdecCancel12mMod").val()) &&
    regExPrec.test($("#txt_Adec12mMod").val()) &&
    regExPrec.test($("#txt_cuotaIns12mMod").val())
  ) {
    bool12m = true;
  } else {
    bool12m = false;
    msgAlert =
      msgAlert +
      "Existen campos no validos en la seccion Precios y Vigencias para los precios de 12 meses. <br>";
  }

  if (
    regExPrec.test($("#txt_sub18mMod").val()) &&
    regExPrec.test($("#txt_precio18mMod").val()) &&
    regExFolio.test($("#txt_folio_ift18mMod").val()) &&
    regExPrec.test($("#txt_costo18mMod").val()) &&
    regExPrec.test($("#txt_Adec18mMod").val())
  ) {
    bool18m = true;
  } else {
    bool18m = false;
    msgAlert =
      msgAlert +
      "Existen campos no validos en la seccion Precios y Vigencias para los precios de 18 meses. <br>";
  }

  if (
    regExPrec.test($("#txt_sub24mMod").val()) &&
    regExPrec.test($("#txt_precio24mMod").val()) &&
    regExFolio.test($("#txt_folio_ift24mMod").val()) &&
    regExPrec.test($("#txt_costo24mMod").val()) &&
    regExPrec.test($("#txt_cargoAdecCancel24mMod").val()) &&
    regExPrec.test($("#txt_Adec24mMod").val()) &&
    regExPrec.test($("#txt_cuotaIns24mMod").val())
  ) {
    bool24m = true;
  } else {
    bool24m = false;
    msgAlert =
      msgAlert +
      "Existen campos no validos en la seccion Precios y Vigencias para los precios de 24 meses. <br>";
  }

  if (
    regExPrec.test($("#txt_sub30mMod").val()) &&
    regExPrec.test($("#txt_precio30mMod").val()) &&
    regExFolio.test($("#txt_folio_ift30mMod").val()) &&
    regExPrec.test($("#txt_costo30mMod").val()) &&
    regExPrec.test($("#txt_Adec30mMod").val())
  ) {
    bool30m = true;
  } else {
    bool30m = false;
    msgAlert =
      msgAlert +
      "Existen campos no validos en la seccion Precios y Vigencias para los precios de 30 meses. <br>";
  }

  if (
    regExPrec.test($("#txt_sub36mMod").val()) &&
    regExPrec.test($("#txt_precio36mMod").val()) &&
    regExFolio.test($("#txt_folio_ift36mMod").val()) &&
    regExPrec.test($("#txt_costo36mMod").val()) &&
    regExPrec.test($("#txt_cargoAdecCancel36mMod").val()) &&
    regExPrec.test($("#txt_Adec36mMod").val()) &&
    regExPrec.test($("#txt_cuotaIns36mMod").val())
  ) {
    bool36m = true;
  } else {
    bool36m = false;
    msgAlert =
      msgAlert +
      "Existen campos no validos en la seccion Precios y Vigencias para los precios de 36 meses. <br>";
  }
  if (
    (boolSer && boolDetalles && bool6m && bool12m && bool18m && bool24m && bool30m && bool36m)
  ) {
    return true;
  } else {
    printErrorMsg("msg_serviceMod", "<br>" + msgAlert);
    return false;
  }
}
function changeVigencia(idService, status){
  var estatus= {
    0 : "desactivar la vigencia del servicio",
    1 : "activar la vigencia del servicio"
  }
  if(confirm("Deseas "+estatus[status]+"?")){
    $.ajax({
      type: "POST",
      url: "xml.php",
      data: {
        "action": "changeVigencia",
        "id": idService,
        "status": status
      },
      beforeSend: function () {
        setLoadDialog(0, "Actualizando Vigencia del servicio");
      },
      complete: function () {
        setLoadDialog(1, "");
      },
      success: function (msg) {
        var resp = (msg).trim().split("||");
        if ("OK" == resp[0]) {
          printSuccessMsg("msg_global", resp[1]);
          // getListServices(); 
          setTimeout(function () {
            $("#msg_global").html("");
          }, 5000); //5000ms =5s
        } else {
          printErrorMsg("msg_global", "No se pudo actualizar la vigencia del servicio");
          setTimeout(function () {
            $("#msg_global").html("");
          }, 5000); //5000ms =5s
        }
      }
    });
  }else{
    // si cancela la anulacion de el cambio de vigencia se cancela el proceso
  }
}

// **+*********************************************
// funciones para la gestion de usuarios
// **+*********************************************

function getUsers(){
  $.ajax({
    type: "POST",
    url: "xml.php",
    data: {
      "action": "getUsers"
    },
    beforeSend: function () {},
    complete: function () {},
    success: function (msg) {
      var resp = (msg).trim().split("||");
      if ("OK" == resp[0]) {
        $('#div_usuarios').html(resp[1]);
        $("#tbl_usersTable").DataTable({
          language: datatableespaniol,
        });
      }
    }
  });
}

function openNewUser(option) {
  cleanModUser();
  $("#mod_userNew").modal("show");
  
  if(0 == option){
    getRegionesUser(-1);
    var cont = generarPassword(8);
    $("#txt_passwordUsr").attr('type', 'text');
    $("#txt_passwordUsr").val(cont);
    ctrlView = 1;
  }
  
  // $("#txt_passwordUsr").val(cont);
}

function generarPassword(longitud) {
  var caracteres = "abcdefghijkmnpqrtuvwxyzABCDEFGHIJKLMNPQRTUVWXYZ1234567890";
  var contraseña = "";
  for (i = 0; i < longitud; i++) contraseña += caracteres.charAt(Math.floor(Math.random() * caracteres.length));
  return contraseña;
}

function viewPassword(){
  if(ctrlView == 0){
    $("#txt_passwordUsr").attr('type', 'text');
    ctrlView = 1;
  }else{
    $("#txt_passwordUsr").attr('type', 'password');
    ctrlView = 0;
  }
}

function getRegionesUser(id){
     $.ajax({
         type: "POST",
         url: "xml.php",
         data: {
             "action": "getRegionesList",
             "idRegion": id
         },
         beforeSend: function () {},
         complete: function () {},
         success: function (msg) {
             var resp = (msg).trim().split("||");
             if ("OK" == resp[0]) {
                 $("#regiones").html(resp[1]);
             }
         }
     });
}

function saveUser(){
  var idUser = $("#idUsr").html(), option = $("#option").html();
  // console.log(idUser,option);
  if(!validateUserData()){
  }else{
    alert("entro");
    $.ajax({
      type: "POST",
      url: "xml.php",
      data: {
        "action": "saveUser",
        "option": option,
        "id": idUser,
        "pass": $("#txt_passwordUsr").val(),
        "email": $("#txt_emailUsr").val(),
        "nombre": $("#txt_nombreUsr").val(),
        "apellidos": $("#txt_apellidosUsr").val(),
        "puesto": $("#txt_puestoUsr").val(),
        "departamento": $("#txt_departamentoUsr").val(),
        "extension": $("#txt_extUsr").val(),
        "tel_movil": $("#txt_movilUsr").val(),
        "sobre_mi": $("#txt_aboutUsr").val(),
        "ubicacion_id": $("#regiones").val()
      },
      beforeSend: function () {},
      complete: function () {},
      success: function (msg) {
        var resp = (msg).trim().split("||");
        if ("OK" == resp[0]) {
        }
      }
    });
  }
}

function validateUserData(){
  var expEmail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  var expMovil = /^([0-9]{0,10})$/;
  var expExt = /^([0-9]{1,4})$/;
  var expPass = /^([a-z0-9]{1,12})$/gmi;
  var msgAlert = "";
  var bool = false;
  if (expEmail.test($("#txt_emailUsr").val()) && expPass.test($("#txt_passwordUsr").val())){
    if(!bool){
      bool = true;
    }
  }else{
    msgAlert += "ERROR en los datos de los Accesos.";
    bool = false;
  }

  if (expMovil.test($("#txt_movilUsr").val()) && expExt.test($("#txt_extUsr").val())) {
    if (!bool) {
      bool = true;
    }
  } else {
    msgAlert += "ERROR datos incorrecto sde extension o telefono celular.";
    bool = false;
  }
  // $("#txt_nombreUsr").val();
  // $("#txt_apellidosUsr").val();
  // $("#txt_puestoUsr").val();
  if (!bool) {
    printErrorMsg("msg_respUserNew", msgAlert);
  }
  return bool;
}

function editUser(idUser, option){
  $("#txt_passwordUsr").attr('type', 'password');
  ctrlView = 0;
  openNewUser(option);
  $("#idUsr").html(idUser);
  $("#option").html(option);
  $.ajax({
    type: "POST",
    url: "xml.php",
    data: {
      "action": "getUserData",
      "id": idUser
    },
    beforeSend: function () { },
    complete: function () { },
    success: function (msg) {
      var resp = (msg).trim().split("||");
      if ("OK" == resp[0]) {
        row = JSON.parse(resp[1]);
        // alert(row['ubicacion_id']);
        getRegionesUser(row['ubicacion_id']);
        $("#txt_passwordUsr").val(resp[2]);
        $("#txt_emailUsr").val(row['username']);
        $("#txt_nombreUsr").val(row['nombre']);
        $("#txt_apellidosUsr").val(row['apellidos']);
        $("#txt_puestoUsr").val(row['puesto']);
        $("#txt_departamentoUsr").val(row['departamento']);
        $("#txt_extUsr").val(parseInt(row['extension']));
        $("#txt_movilUsr").val(parseInt(row['tel_movil']));
        $("#txt_aboutUsr").val(row['sobre_mi']);
      }
    }
  });
}

function cleanModUser(){
  $("#txt_passwordUsr").val('');
  $("#txt_emailUsr").val('');
  $("#txt_nombreUsr").val('');
  $("#txt_apellidosUsr").val('');
  $("#txt_puestoUsr").val('');
  $("#txt_departamentoUsr").val('');
  $("#txt_extUsr").val('');
  $("#txt_movilUsr").val('');
  $("#txt_aboutUsr").val('');
}