<?php
session_start();
set_time_limit(0);
include_once "../../libs/db/common.php";

if (!verifySession($_SESSION)) {
  logoutTimeout();
} else {

  header('Content-Type: text/html; charset=UTF-8');
  date_default_timezone_set('America/Mexico_City');

  $title = "Herramientas";
  $usuario = $_SESSION['usuario'];
  $gruposArr  = $_SESSION['grupos'];
  ?>

  <!DOCTYPE html>
  <html>

  <head>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/AdminLTE2/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/js/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/bootstrap/summernote-0.6.6-dist/summernote.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/bootstrap/font-awesome-4.3.0/css/font-awesome.css" rel="stylesheet" type="text/css" />

    <script src="../../libs/AdminLTE2/plugins/jQuery/jQuery-2.1.3.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/bootstrap/js/bootstrap.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/dist/js/app.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../libs/js/jqueryValidate/dist/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.form.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery-ui-sliderAccess.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
    <script src="../../libs/bootstrap/summernote-0.6.6-dist/summernote.js" type="text/javascript"></script>
    <script src="../../libs/bootstrap/summernote-0.6.6-dist/lang/summernote-es-ES.js" type="text/javascript"></script>
    <script src="../../libs/js/common.js" type="text/javascript"></script>
    <!-- <script src="script.js" type="text/javascript"></script> -->
    <link rel="icon" type="image/png" href="../../img/favicon-16x16.png" sizes="16x16">

    <title>Coeficiente CRM - Herramientas</title>
    <style type="text/css"></style>
  </head>

  <body>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyArJv8_3uHXiTrD0FvohwxCdA-TmYTmB7Y" async defer></script>
    <!--Div que contiene el encabezado-->
    <div class="container" id="header"><?php include("../../libs/templates/header.php") ?></div>

    <div class="container-fluid">
      <h2 class="page-header" id="tituloPag">
        <span class="glyphicon glyphicon-wrench"></span> <?php echo $title ?>
      </h2>
    </div><!-- /container -->

    <div class="container">
      <div class="row">
        <div id="div_msgAlert"></div>
      </div>
    </div> <!-- /container -->

    <div class="container">
      <div class="row">
        <table id="tbl_herramientas" class="table table-striped">
          <thead>
            <tr>
              <th>Herramienta</th>
              <th>Categoría</th>
              <th>Descripción</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <?php
                if ((array_key_exists(4, $gruposArr)) || hasPermission(4, 'a')) { ?>
                <td><a href="editorprecios.php">Editor de Precios y Promociones</a></td>
                <td>Cotización</td>
                <td>Modifica o agrega precios de servicios y productos para su uso en cotizaciones</td>
              <?php } ?>
            </tr>
            <tr>
              <?php
                if ((array_key_exists(4, $gruposArr)) || hasPermission(4, 'a')) { ?>
                <td><a href="editorUsuarios.php">Gestion de usuarios</a></td>
                <td>Usuarios</td>
                <td>Gestion de usuarios, alta, modificacion y baja de los mismos.</td>
              <?php } ?>
            </tr>
          </tbody>
        </table>
      </div>
      <br><br>
    </div> <!-- /container -->
  </body>

  </html>

<?php
} //fin else sesión
?>