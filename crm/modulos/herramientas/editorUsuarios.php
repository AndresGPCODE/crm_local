<?php
session_start();
set_time_limit(0);
include_once "../../libs/db/common.php";
include_once "../../libs/db/dbcommon.php";
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');
$title = "Herramientas/Editor de Precios";
$usuario = $_SESSION['usuario'];
$gruposArr  = $_SESSION['grupos'];
$usrRegion = $_SESSION['usrUbica'];

?>

<!DOCTYPE HTML>
<html>

<head>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/AdminLTE2/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/js/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/bootstrap/summernote-0.6.6-dist/summernote.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/bootstrap/font-awesome-4.3.0/css/font-awesome.css" rel="stylesheet" type="text/css" />

    <script src="../../libs/AdminLTE2/plugins/jQuery/jQuery-2.1.3.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/bootstrap/js/bootstrap.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/dist/js/app.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../libs/js/jqueryValidate/dist/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.form.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery-ui-sliderAccess.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
    <script src="../../libs/bootstrap/summernote-0.6.6-dist/summernote.js" type="text/javascript"></script>
    <script src="../../libs/bootstrap/summernote-0.6.6-dist/lang/summernote-es-ES.js" type="text/javascript"></script>
    <script src="../../libs/js/common.js" type="text/javascript"></script>
    <script src="script.js" type="text/javascript"></script>
    <link rel="icon" type="image/png" href="../../img/favicon-16x16.png" sizes="16x16">

    <title>Coeficiente CRM - Editor de Precios</title>
    <style type="text/css">
        #tbl_pricesTable_wrapper {
            margin-left: -27px;
        }

        #region {
            margin-top: 10px;
            width: 100%;
        }

        .fullWidth {
            width: 48%;
        }

        .fullWidth2 {
            width: 97%;
        }

        .fullWidthPassword {
            width: 40%
        }

        hr {
            margin-left: 0px;
            width: 96%
        }

        button#btn_viewPassword {
            margin-top: -3px;
            border: 0;
        }

        /* input[type="radio"] {
            margin-left: 10px;
        }

        input[type="radio"]:first-child {
            margin-left: 0px;
        }

        .colum-3 {
            width: 31.5%;
        }

        .colum-4 {
            width: 23.5%;
        }

        #btn_Promo {
            margin-left: 10px;
            float: left;
        }

        #btn_productNew {
            float: left;
        }

        #selServicioBase {
            width: 100%;
        }

        #selRegionesListLabel {
            width: 100%;
        }

        #descripcionPromo {
            font-size: 16px;
        }

        #aplicaMes {
            width: 20%;
        }

        #aplicaAnio {
            width: 15%;
        }

        #tbl_pricesTable_filter label:first-child {
            width: auto;
        }

        #btn_autoFill {
            margin-left: 15px;
        } */
    </style>
</head>

<body>
    <script type="text/javascript">
    </script>
    <div class="container" id="header"><?php include("../../libs/templates/header.php") ?></div>

    <div class="container-fluid">
        <h2 class="page-header" id="tituloPag">
            <span class="glyphicon glyphicon-list"></span> Usuarios
        </h2>
    </div><!-- /container -->

    <div class="container">
        <div class="row">
            <div id="div_msgAlert"></div>
        </div>
    </div> <!-- /container -->

    <?php
    if (((array_key_exists(1, $gruposArr)) || (array_key_exists(2, $gruposArr)) || (array_key_exists(11, $gruposArr))) && $_SESSION['usrId'] == 72) { ?>
        <div class="container">
            <div class="row">
                <div><button type="button" id="btn_userNew" title="Nuevo usuario" class="btn btn-primary" onClick="openNewUser(0);"><span class="glyphicon glyphicon-asterisk"></span>Nuevo usuario</button></div>
            </div>
            <br>
        </div> <!-- /container -->
    <?php } ?>

    <div class="container">
        <div class="row">
            <div id="msg_global"></div>
        </div>
        <div class="row">

            <div id="div_usuarios">
            </div>
        </div>
    </div>
    <!-- /container -->

    <!-- modal para las primociones -->
    <div id="mod_userNew" class="modal fade" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Datos del usuario</h4>
                </div>
                <div class="modal-body">
                    <div id="msg_respUserNew"></div>
                    <div style="display:none;" id="idUsr">0</div>
                    <div style="display:none;" id="option">0</div>
                    <h4>Accesos: </h4>
                    <label class="fullWidth">Email Usuario/Username:
                        <input type="email" id="txt_emailUsr" name="txt_emailUsr" class="form-control" placeholder="ejemplo@coeficiente.mx" size="65" maxlength="80" title="Correo del usuario">
                    </label>
                    <label class="fullWidthPassword">Contraseña:
                        <input type="password" id="txt_passwordUsr" name="txt_passwordUsr" class="form-control" placeholder="" size="65" minlength="8" maxlength="12" title="Contraseña">
                    </label>
                    <button type="button" id="btn_viewPassword" title="Nuevo usuario" class="btn btn-default" onClick="viewPassword();"><span class="glyphicon glyphicon-eye-open"></span></button>
                    <br>
                    <hr>
                    <h4>Datos personales: </h4>
                    <label class="fullWidth">Nombre:
                        <input type="text" id="txt_nombreUsr" name="txt_nombreUsr" class="form-control" placeholder="Nombre del usuario" size="65" maxlength="80" title="Nombre del usuario">
                    </label>
                    <label class="fullWidth">Apellidos:
                        <input type="text" id="txt_apellidosUsr" name="txt_apellidosUsr" class="form-control" placeholder="Apellidos del usuario" size="65" maxlength="80" title="Apellido del usuario">
                    </label>
                    <label class="fullWidth">Puesto:
                        <input type="text" id="txt_puestoUsr" name="txt_puestoUsr" class="form-control" placeholder="Puesto del usuario" size="65" maxlength="80" title="Puesto del usuario">
                    </label>
                    <label class="fullWidth">Departamento:
                        <input type="text" id="txt_departamentoUsr" name="txt_departamentoUsr" class="form-control" placeholder="Departamento del usuario" size="65" maxlength="80" title="Departamento del usuario">
                    </label>
                    <!-- seleccion de las regiones para las que aplica este servicio -->
                    <label id="region"> Region:
                        <select name="regiones" id="regiones">
                        </select>
                    </label>
                    <label class="fullWidth">Extencion:
                        <input type="number" id="txt_extUsr" name="txt_extUsr" class="form-control" placeholder="Ext. de telefono" size="65" maxlength="4" title="Extencion de la linea telefonica del usuario">
                    </label>
                    <label class="fullWidth">Tel-Movil:
                        <input type="number" id="txt_movilUsr" name="txt_movilUsr" class="form-control" placeholder="Telefono Celular del usuario" size="65" maxlength="10" title="numero de telefono celular del usuario">
                    </label>
                    <label class="fullWidth2">Acerca de:
                        <textarea id="txt_aboutUsr" name="txt_aboutUsr" class="form-control" placeholder="Datos relacionados con el usuario" rows="5" cols="65" maxlength="120" title="acerca del usuario"></textarea>
                    </label>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                    <input type="submit" class="btn btn-primary" id="saveUser" value="Guardar Usuario" onClick="saveUser();">
                </div>
            </div>
        </div>
    </div>

    <!-- no eliminar estas lineas de codigo -->
    <div class="container-fluid" id="footer"><?php include("../../libs/templates/footer.php") ?></div>
    <script>
        getUsers();
    </script>
</body>

</html>