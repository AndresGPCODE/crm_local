<?php
session_start();
set_time_limit(0);
include_once "../../libs/db/common.php";
include_once "../../libs/db/dbcommon.php";
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');
$title = "Herramientas/Editor de Precios";
$usuario = $_SESSION['usuario'];
$gruposArr  = $_SESSION['grupos'];
$usrRegion = $_SESSION['usrUbica'];

?>

<!DOCTYPE HTML>
<html>

<head>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link href="../../libs/AdminLTE2/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
  <link href="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css" />
  <link href="../../libs/js/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />
  <link href="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.css" rel="stylesheet" type="text/css" />
  <link href="../../libs/bootstrap/summernote-0.6.6-dist/summernote.css" rel="stylesheet" type="text/css" />
  <link href="../../libs/bootstrap/font-awesome-4.3.0/css/font-awesome.css" rel="stylesheet" type="text/css" />

  <script src="../../libs/AdminLTE2/plugins/jQuery/jQuery-2.1.3.js" type="text/javascript"></script>
  <script src="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.js" type="text/javascript"></script>
  <script src="../../libs/AdminLTE2/bootstrap/js/bootstrap.js" type="text/javascript"></script>
  <script src="../../libs/AdminLTE2/dist/js/app.js" type="text/javascript"></script>
  <script src="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
  <script src="../../libs/js/jqueryValidate/dist/jquery.validate.min.js" type="text/javascript"></script>
  <script src="../../libs/js/jquery.form.js" type="text/javascript"></script>
  <script src="../../libs/js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
  <script src="../../libs/js/jquery-ui-sliderAccess.js" type="text/javascript"></script>
  <script src="../../libs/js/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
  <script src="../../libs/bootstrap/summernote-0.6.6-dist/summernote.js" type="text/javascript"></script>
  <script src="../../libs/bootstrap/summernote-0.6.6-dist/lang/summernote-es-ES.js" type="text/javascript"></script>
  <script src="../../libs/js/common.js" type="text/javascript"></script>
  <script src="script.js" type="text/javascript"></script>
  <link rel="icon" type="image/png" href="../../img/favicon-16x16.png" sizes="16x16">

  <title>Coeficiente CRM - Editor de Precios</title>
  <style type="text/css">
    #tbl_pricesTable_wrapper {
      margin-left: -27px;
    }

    .fullWidth {
      width: 50%;
    }

    .fullWidth2 {
      width: 100%;
    }

    label {
      width: 15%;
      margin-right: 10px;
    }

    #vigencia1 {
      width: 15%;
    }

    #vigencia {
      width: 15%;
    }

    #select_tipoServicio {
      width: 100%;
    }

    #select_tipoServicioMod {
      width: 100%;
    }

    #region {
      width: 100%;
    }

    input[type="radio"] {
      margin-left: 10px;
    }

    input[type="radio"]:first-child {
      margin-left: 0px;
    }

    .colum-3 {
      width: 31.5%;
    }

    .colum-4 {
      width: 23.5%;
    }

    #btn_Promo {
      margin-left: 10px;
      float: left;
    }

    #btn_productNew {
      float: left;
    }

    #selServicioBase {
      width: 100%;
    }

    #selRegionesListLabel {
      width: 100%;
    }

    #descripcionPromo {
      font-size: 16px;
    }

    #aplicaMes {
      width: 20%;
    }

    #aplicaAnio {
      width: 15%;
    }

    #tbl_pricesTable_filter label:first-child {
      width: auto;
    }

    #btn_autoFill {
      margin-left: 15px;
    }
  </style>
</head>

<body>
  <script type="text/javascript">
  </script>
  <div class="container" id="header"><?php include("../../libs/templates/header.php") ?></div>

  <div class="container-fluid">
    <h2 class="page-header" id="tituloPag">
      <span class="glyphicon glyphicon-list"></span> Editor de Servicios
    </h2>
  </div><!-- /container -->

  <div class="container">
    <div class="row">
      <div id="div_msgAlert"></div>
    </div>
  </div> <!-- /container -->

  <?php
  if (((array_key_exists(1, $gruposArr)) || (array_key_exists(2, $gruposArr)) || (array_key_exists(11, $gruposArr))) && $_SESSION['usrId'] == 72) { ?>
    <div class="container">
      <div class="row">
        <div><button type="button" id="btn_productNew" title="Nuevo Servicio" class="btn btn-default" onClick="openNewService();"><span class="glyphicon glyphicon-asterisk"></span>Nuevo Servicio</button></div>
        <div><button type="button" id="btn_Promo" title="Nueva Promocion" class="btn btn-default" onClick="openPromo();"><span class="glyphicon glyphicon-asterisk"></span>Nueva Promocion</button></div>
      </div>
      <br>
    </div> <!-- /container -->
  <?php } ?>

  <div class="container">
    <div class="row">
      <div id="msg_global"></div>
    </div>
    <div class="row">

      <div id="div_prices">
      </div>
    </div>
  </div>
  <!-- /container -->

  <div id="mod_serviceDetails" class="modal fade" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Detalles de Servicio</h4>
        </div>
        <div class="modal-body">
          <div id="div_serviceDetails"></div>
        </div>
        <div class="modal-footer">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
  <!-- modal para la insercion de un servicio -->
  <div id="mod_serviceInsert" class="modal fade" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Nuevo Servicio</h4>

        </div>
        <div class="modal-body">
          <div id="msg_validateResp"></div>
          <!-- nombre del servicio -->
          <h3 style="margin-top: 0px;">Servicio y Descripcion:</h3>
          <label class="fullWidth">Clave de Servicio:
            <input type="text" id="txt_serCve" name="txt_serCve" class="form-control" placeholder="Clave de Servicio" size="65" maxlength="120" title="Nombre del servicio">
          </label>
          <!-- seleccion de tipo de cobro -->
          <label>Tipo de servicio:
            <select name="select" id="select_tipoServicio" title="tipo del servicio">
            </select>
          </label>
          <!-- seleccion de tipo de cobro -->
          <label>Tipo de Cobro:
            <select name="select" id="select_tipoCobro" title="Tipo de cobro">
            </select>
          </label>
          <!-- descripcion del servicio -->
          <label class="fullWidth2">Descripción de Servicio:
            <textarea id="txt_serDesc" name="txt_serDesc" class="form-control" placeholder="Descripción del servicio" maxlength="255" rows="2" cols="64" title="Descripcion del servicio - Max 250 caracteres"></textarea>
          </label>
          <!-- seleccion de las regiones para las que aplica este servicio -->
          <fieldset id="region">
            <label>Regiones para las que aplica:</label>
            <div id="regiones">
            </div>
          </fieldset>
          <hr>

          <h3>Detalles del servicio:</h3>
          <!-- mb de bajada -->
          <label>Bajada: (Mb)
            <input type="text" id="txt_serBajada" name="txt_serBajada" class="form-control" placeholder="0000" size="5" maxlength="4" title="Mb de bajada">
          </label>
          <!-- mb de subida -->
          <label>Subida: (Mb)
            <input type="text" id="txt_serSubida" name="txt_serSubida" class="form-control" placeholder="0000" size="5" maxlength="4" title="Mb de subida">
          </label>
          <!-- Distancia en KM -->
          <label>Distancia Max.: (KM)
            <input type="text" id="txt_serDistMax" name="txt_serDistMax" class="form-control" placeholder="000" size="5" maxlength="3" title="Distancia en KM">
          </label>
          <!-- tiempo de instalacion -->
          <label>Tiempo instalacion:
            <select style="width:100%" name="txt_tiempoInstal" id="txt_tiempoInstal" title="Dias para la instalacion">
              <option value="-1">Validar Proyecto</option>
              <option value="0">2 Dias</option>
              <option value="1" selected>1 Dia</option>
              <option value="2">2 Dias</option>
              <option value="3">3 Dias</option>
              <option value="4">4 Dias</option>
              <option value="5">5 Dias</option>
              <option value="6">6 Dias</option>
              <option value="7">7 Dias</option>
              <option value="8">8 Dias</option>
              <option value="9">9 Dias</option>
            </select>
            <!-- <input title="Tiempo de istalacion en DIAS" type="text" id="txt_tiempoInstal" name="txt_tiempoInstal" class="form-control" placeholder="Dias" size="5" maxlength="5" number> -->
          </label>
          <!-- Precio base -->
          <label>Precio Base:
            <input title="Precio base del servicio" type="text" id="txt_serPrecBase" name="txt_serPrecBase" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <!-- Precio medio -->
          <label>Precio Medio:
            <input title="Precio medio del servicio" type="text" id="txt_serPrecMedio" name="txt_serPrecMedio" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <!-- Precio de lista -->
          <label>Precio Lista:
            <input title="precio de lista del servicio" type="text" id="txt_serPrecLista" name="txt_serPrecLista" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <!-- Precio de instalacion -->
          <label>Precio inscripcón:
            <input title="Costo por inscripción" type="text" id="txt_inscripcion" name="txt_inscripcion" class="form-control" placeholder="000.00" size="6" maxlength="11" number>
          </label>
          <!-- Kilometros de alcance posiblemente -->
          <label>Kilometros: (KM)
            <input title="Alcance en Kilometros" type="text" id="txt_kilometros" name="txt_kilometros" class="form-control" placeholder="000" size="5" maxlength="3" number>
          </label>
          <!-- fecha de inicio de vigencia -->
          <label id="vigencia1"> Inicio de Vigencia
            <input title="Fecha de inicio de la vigencia del servicio" type="date" id="txt_inicioVigencia" name="txt_inicioVigencia" value="2019-01-01" min="2019-01-01" max="2050-12-31">
          </label>
          <!-- fecha de inicio de vigencia -->
          <label id="vigencia"> Fin de Vigencia
            <input title="Fecha de Fin de la vigencia del servicio" type="date" id="txt_finVigencia" name="txt_finVigencia" value="2019-01-01" min="2019-01-01" max="2050-12-31">
          </label>
          <br>
          <hr>

          <h3>Precios y Vigencias:</h3>
          <!-- subsidio del servicio para todas las vigencias -->
          <label class="colum-3">Sub-12m.
            <input title="subsidio para contrato por 12 meses" type="text" id="txt_sub12m" name="txt_sub12m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Sub-24m.
            <input title="subsidio para contrato por 24 meses" type="text" id="txt_sub24m" name="txt_sub24m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Sub-36m.
            <input title="subsidio para contrato por 36 meses" type="text" id="txt_sub36m" name="txt_sub36m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>

          <!-- precio del servicio para todas las vigencias -->
          <label class="colum-3">Precio-12m.
            <input title="precio para contrato por 12 meses" type="text" id="txt_precio12m" name="txt_precio12m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Precio-24m.
            <input title="precio para contrato por 24 meses" type="text" id="txt_precio24m" name="txt_precio24m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Precio-36m.
            <input title="precio para contrato por 36 meses" type="text" id="txt_precio36m" name="txt_precio36m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>

          <!-- cuotaIns del servicio para todas las vigencias -->
          <label class="colum-3">Cuota Instalacion-12m.
            <input title="cuota por instalacion" type="text" id="txt_cuotaIns12m" name="txt_cuotaIns12m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Cuota Instalacion-24m.
            <input title="cuota por instalacion" type="text" id="txt_cuotaIns24m" name="txt_cuotaIns24m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Cuota Instalacion-36m.
            <input title="cuota por instalacion" type="text" id="txt_cuotaIns36m" name="txt_cuotaIns36m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>

          <!-- folio_ift del servicio para todas las vigencias -->
          <label class="colum-3">Folio_ift_12m.
            <input title="folio_ift para contrato por 12 meses" type="text" id="txt_folio_ift12m" name="txt_folio_ift12m" class="form-control" placeholder="000000" size="5" maxlength="6" number>
          </label>
          <label class="colum-3">Folio_ift_24m.
            <input title="folio_ift para contrato por 24 meses" type="text" id="txt_folio_ift24m" name="txt_folio_ift24m" class="form-control" placeholder="000000" size="5" maxlength="6" number>
          </label>
          <label class="colum-3">Folio_ift_36m.
            <input title="folio_ift para contrato por 36 meses" type="text" id="txt_folio_ift36m" name="txt_folio_ift36m" class="form-control" placeholder="000000" size="5" maxlength="6" number>
          </label>

          <!-- costo del servicio para todas las vigencias -->
          <label class="colum-3">Costo-12m.
            <input title="costo para contrato por 12 meses" type="text" id="txt_costo12m" name="txt_costo12m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Costo-24m.
            <input title="costo para contrato por 24 meses" type="text" id="txt_costo24m" name="txt_costo24m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Costo-36m.
            <input title="costo para contrato por 36 meses" type="text" id="txt_costo36m" name="txt_costo36m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>

          <!-- cargo_adecuaciones_cancelacion del servicio para todas las vigencias -->
          <label class="colum-3">Cargo_adecuaciones_cancelacion-12m.
            <input title="cargo de adecuaciones por cancelacion para contrato por 12 meses" type="text" id="txt_cargoAdecCancel12m" name="txt_cargoAdecCancel12m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Cargo_adecuaciones_cancelacion-24m.
            <input title="cargo de adecuaciones por cancelacion para contrato por 24 meses" type="text" id="txt_cargoAdecCancel24m" name="txt_cargoAdecCancel24m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Cargo_adecuaciones_cancelacion-36m.
            <input title="cargo de adecuaciones por cancelacion para contrato por 36 meses" type="text" id="txt_cargoAdecCancel36m" name="txt_cargoAdecCancel36m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>

          <!-- Cargo_adecuaciones del servicio para todas las vigencias -->
          <!-- <label class="colum-3">Cargo_adecuaciones-12m.
            <input title="cargo de adecuaciones para contrato por 12 meses" type="text" id="txt_cargoAdec12m" name="txt_cargoAdec12m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Cargo_adecuaciones-24m.
            <input title="cargo de adecuaciones para contrato por 24 meses" type="text" id="txt_cargoAdec24m" name="txt_cargoAdec24m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Cargo_adecuaciones-36m.
            <input title="cargo de adecuaciones para contrato por 36 meses" type="text" id="txt_cargoAdec36m" name="txt_cargoAdec36m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label> -->

          <!-- Adecuaciones del servicio para todas las vigencias -->
          <label class="colum-3">Adecuaciones-12m.
            <input title="Adecuaciones para contrato por 12 meses" type="text" id="txt_Adec12m" name="txt_Adec12m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Adecuaciones-24m.
            <input title="Adecuaciones para contrato por 24 meses" type="text" id="txt_Adec24m" name="txt_Adec24m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Adecuaciones-36m.
            <input title="Adecuaciones para contrato por 36 meses" type="text" id="txt_Adec36m" name="txt_Adec36m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>

          <!--  -->
          <!--  -->

          <br><br><br>
          <div class="row">
            <div><button type="button" id="btn_autoFill" title="Auto rellenar los campos de estas secciones con valores minimos" class="btn btn-primary" onClick="fill_6_18_30();"><span class="glyphicon glyphicon-asterisk"></span>Auto llenar vigencias 6,18 y 30 Meses</button></div>
          </div><br><br>
          <!-- subsidio del servicio para todas las vigencias -->
          <label class="colum-3">Sub-6m.
            <input title="subsidio para contrato por 6 meses" type="text" id="txt_sub6m" name="txt_sub6m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Sub-18m.
            <input title="subsidio para contrato por 18 meses" type="text" id="txt_sub18m" name="txt_sub18m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Sub-30m.
            <input title="subsidio para contrato por 30 meses" type="text" id="txt_sub30m" name="txt_sub30m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>

          <!-- precio del servicio para todas las vigencias -->
          <label class="colum-3">Precio-6m.
            <input title="precio para contrato por 6 meses" type="text" id="txt_precio6m" name="txt_precio6m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Precio-18m.
            <input title="precio para contrato por 12 meses" type="text" id="txt_precio18m" name="txt_precio18m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Precio-30m.
            <input title="precio para contrato por 30 meses" type="text" id="txt_precio30m" name="txt_precio30m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>

          <!-- folio_ift del servicio para todas las vigencias -->
          <label class="colum-3">Folio_ift_6m.
            <input title="folio_ift para contrato por 6 meses" type="text" id="txt_folio_ift6m" name="txt_folio_ift6m" class="form-control" placeholder="000000" size="5" maxlength="6" number>
          </label>
          <label class="colum-3">Folio_ift_18m.
            <input title="folio_ift para contrato por 18 meses" type="text" id="txt_folio_ift18m" name="txt_folio_ift18m" class="form-control" placeholder="000000" size="5" maxlength="6" number>
          </label>
          <label class="colum-3">Folio_ift_30m.
            <input title="folio_ift para contrato por 30 meses" type="text" id="txt_folio_ift30m" name="txt_folio_ift30m" class="form-control" placeholder="000000" size="5" maxlength="6" number>
          </label>

          <!-- costo del servicio para todas las vigencias -->
          <label class="colum-3">Costo-6m.
            <input title="costo para contrato por 6 meses" type="text" id="txt_costo6m" name="txt_costo6m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Costo-18m.
            <input title="costo para contrato por 18 meses" type="text" id="txt_costo18m" name="txt_costo18m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Costo-30m.
            <input title="costo para contrato por 30 meses" type="text" id="txt_costo30m" name="txt_costo30m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>

          <!-- cargo_adecuaciones_cancelacion del servicio para todas las vigencias -->
          <!-- <label class="colum-3">Cargo_adecuaciones_cancelacion-6m.
            <input title="cargo de adecuaciones por cancelacion para contrato por 6 meses" type="text" id="txt_cargoAdecCancel6m" name="txt_cargoAdecCancel6m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Cargo_adecuaciones_cancelacion-18m.
            <input title="cargo de adecuaciones por cancelacion para contrato por 18 meses" type="text" id="txt_cargoAdecCancel18m" name="txt_cargoAdecCancel18m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Cargo_adecuaciones_cancelacion-30m.
            <input title="cargo de adecuaciones por cancelacion para contrato por 30 meses" type="text" id="txt_cargoAdecCancel30m" name="txt_cargoAdecCancel30m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label> -->

          <!-- Cargo_adecuaciones del servicio para todas las vigencias -->
          <!-- <label class="colum-3">Cargo_adecuaciones-6m.
            <input title="cargo de adecuaciones para contrato por 6 meses" type="text" id="txt_cargoAdec6m" name="txt_cargoAdec6m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Cargo_adecuaciones-18m.
            <input title="cargo de adecuaciones para contrato por 18 meses" type="text" id="txt_cargoAdec18m" name="txt_cargoAdec18m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Cargo_adecuaciones-30m.
            <input title="cargo de adecuaciones para contrato por 30 meses" type="text" id="txt_cargoAdec30m" name="txt_cargoAdec30m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label> -->

          <!-- Adecuaciones del servicio para todas las vigencias -->
          <label class="colum-3">Adecuaciones-6m.
            <input title="Adecuaciones para contrato por 6 meses" type="text" id="txt_Adec6m" name="txt_Adec6m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Adecuaciones-18m.
            <input title="Adecuaciones para contrato por 18 meses" type="text" id="txt_Adec18m" name="txt_Adec18m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Adecuaciones-30m.
            <input title="Adecuaciones para contrato por 30 meses" type="text" id="txt_Adec30m" name="txt_Adec30m" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <!--  -->
          <!--  -->
        </div>
        <!-- fin del modal para insersion de los servicios -->
        <div class="modal-footer">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
          <input type="submit" class="btn btn-primary" id="guardaServicio" value="Guardar Cambios" onClick="saveNewService();">
        </div>
      </div>
    </div>
  </div>
  <!-- modal para las primociones -->
  <div id="mod_promo" class="modal fade" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Promocion</h4>
        </div>
        <div class="modal-body">
          <div id="msg_respPromo"></div>
          <label id="selRegionesListLabel"> Region:
            <select name="selRegionesList" id="selRegionesList">
            </select>
          </label>
          <label id="selServicioBase"> Servicio Base:
            <select name="selectServiceBase" id="selectServiceBase">
            </select>
          </label>
          <label id="aplicaAnio"> Año de Vigencia:
            <select name="selectAplicaAnio" id="selectAplicaAnio">
              <option value="2019">2019</option>
              <option value="2020">2020</option>
              <option value="2021">2021</option>
              <option value="2022">2022</option>
            </select>
          </label>
          <label id="aplicaMes"> Mes de Vigencia:
            <select name="selectAplicaMes" id="selectAplicaMes">
              <option value="1">Enero</option>
              <option value="2">Febrero</option>
              <option value="3">Marzo</option>
              <option value="4">Abril</option>
              <option value="5">Mayo</option>
              <option value="6">Junio</option>
              <option value="7">Julio</option>
              <option value="8">Agosto</option>
              <option value="9">Septiembre</option>
              <option value="10">Octubre</option>
              <option value="11">Noviembre</option>
              <option value="12">Diciembre</option>
            </select>
          </label>
          <div id="test"></div>
          <div id="input_prices">
            <!-- todos los campos de los precios para modificar -->
            <h4>Selecciona una region y el servicio para el cual aplicara la promocion.</h4>
            <!-- <h4>INT-CORP-1G X 1G.</h4>
            <p id="descripcionPromo">Descipcion del servicio Base para la promocion</p>
            <label class="colum-3">Precio 12 Meses.
              <input title="Precio que aplica para la vigencia de 12 meses." type="text" id="pricePromo12m" name="pricePromo12m" class="form-control" size="5" maxlength="8" value="1200.00">
            </label>
            <label class="colum-3">Precio 24 Meses.
              <input title="Precio que aplica para la vigencia de 24 meses." type="text" id="pricePromo24m" name="pricePromo24m" class="form-control" size="5" maxlength="8" value="2400.00">
            </label>
            <label class="colum-3">Precio 36 Meses.
              <input title="Precio que aplica para la vigencia de 36 meses." type="text" id="pricePromo36m" name="pricePromo36m" class="form-control" size="5" maxlength="8" value="3600.00">
            </label> -->
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
          <input type="submit" class="btn btn-primary" id="guardaPromo" value="Guardar Promocion" onClick="savePromo();">
        </div>
      </div>
    </div>
  </div>
  <!-- modal para la insercion de un servicio -->
  <div id="mod_serviceMod" class="modal fade" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Modificar Servicio</h4>

        </div>
        <div class="modal-body">
          <div id="msg_serviceMod"></div>
          <div style="display:none;" id="servicioIdMod"></div>
          <!-- nombre del servicio -->
          <h3 style="margin-top: 0px;">Servicio y Descripcion:</h3>
          <label class="fullWidth">Clave de Servicio:
            <input type="text" id="txt_serCveMod" name="txt_serCveMod" class="form-control" placeholder="Clave de Servicio" size="65" maxlength="120" title="Nombre del servicio">
          </label>
          <!-- seleccion de tipo de cobro -->
          <label>Tipo de servicio:
            <select name="select" id="select_tipoServicioMod" title="tipo del servicio">
            </select>
          </label>
          <!-- seleccion de tipo de cobro -->
          <label>Tipo de Cobro:
            <select name="select" id="select_tipoCobroMod" title="Tipo de cobro">
            </select>
          </label>
          <!-- descripcion del servicio -->
          <label class="fullWidth2">Descripción de Servicio:
            <textarea id="txt_serDescMod" name="txt_serDescMod" class="form-control" placeholder="Descripción del servicio" maxlength="255" rows="2" cols="64" title="Descripcion del servicio - Max 250 caracteres"></textarea>
          </label>
          <!-- seleccion de las regiones para las que aplica este servicio -->
          <fieldset id="regionMod" style="display: none;">
            <label>Regiones para las que aplica:</label>
            <div id="regionesMod">
            </div>
          </fieldset>
          <hr>

          <h3>Detalles del servicio:</h3>
          <!-- mb de bajada -->
          <label>Bajada: (Mb)
            <input type="text" id="txt_serBajadaMod" name="txt_serBajadaMod" class="form-control" placeholder="0000" size="5" maxlength="4" title="Mb de bajada">
          </label>
          <!-- mb de subida -->
          <label>Subida: (Mb)
            <input type="text" id="txt_serSubidaMod" name="txt_serSubidaMod" class="form-control" placeholder="0000" size="5" maxlength="4" title="Mb de subida">
          </label>
          <!-- Distancia en KM -->
          <label>Distancia Max.: (KM)
            <input type="text" id="txt_serDistMaxMod" name="txt_serDistMaxMod" class="form-control" placeholder="000" size="5" maxlength="3" title="Distancia en KM">
          </label>
          <!-- tiempo de instalacion -->
          <label>Tiempo instalacion:
            <select style="width:100%" name="txt_tiempoInstalMod" id="txt_tiempoInstalMod" title="Dias para la instalacion">
              <option value="-1">Validar Proyecto</option>
              <option value="0">0 Dias</option>
              <option value="1">1 Dia</option>
              <option value="2">2 Dias</option>
              <option value="3">3 Dias</option>
              <option value="4">4 Dias</option>
              <option value="5">5 Dias</option>
              <option value="6">6 Dias</option>
              <option value="7">7 Dias</option>
              <option value="8">8 Dias</option>
              <option value="9">9 Dias</option>
            </select>
            <!-- <input title="Tiempo de istalacion en DIAS" type="text" id="txt_tiempoInstal" name="txt_tiempoInstal" class="form-control" placeholder="Dias" size="5" maxlength="5" number> -->
          </label>
          <!-- Precio base -->
          <label>Precio Base:
            <input title="Precio base del servicio" type="text" id="txt_serPrecBaseMod" name="txt_serPrecBaseMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <!-- Precio medio -->
          <label>Precio Medio:
            <input title="Precio medio del servicio" type="text" id="txt_serPrecMedioMod" name="txt_serPrecMedioMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <!-- Precio de lista -->
          <label>Precio Lista:
            <input title="precio de lista del servicio" type="text" id="txt_serPrecListaMod" name="txt_serPrecListaMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <!-- Precio de instalacion -->
          <label>Precio inscripcón:
            <input title="Costo por inscripción" type="text" id="txt_inscripcionMod" name="txt_inscripcionMod" class="form-control" placeholder="000.00" size="6" maxlength="11" number>
          </label>
          <!-- Kilometros de alcance posiblemente -->
          <label>Kilometros: (KM)
            <input title="Alcance en Kilometros" type="text" id="txt_kilometrosMod" name="txt_kilometrosMod" class="form-control" placeholder="000" size="5" maxlength="3" number>
          </label>
          <!-- fecha de inicio de vigencia -->
          <label id="vigencia1"> Inicio de Vigencia
            <input title="Fecha de inicio de la vigencia del servicio" type="date" id="txt_inicioVigenciaMod" name="txt_inicioVigenciaMod" min="2018-01-01" max="2050-12-31">
          </label>
          <!-- fecha de inicio de vigencia -->
          <label id="vigencia"> Fin de Vigencia
            <input title="Fecha de Fin de la vigencia del servicio" type="date" id="txt_finVigenciaMod" name="txt_finVigenciaMod" min="2018-01-01" max="2050-12-31">
          </label>
          <br>
          <hr>

          <h3>Precios y Vigencias:</h3>
          <!-- subsidio del servicio para todas las vigencias -->
          <label class="colum-3">Sub-12m.
            <input title="subsidio para contrato por 12 meses" type="text" id="txt_sub12mMod" name="txt_sub12mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Sub-24m.
            <input title="subsidio para contrato por 24 meses" type="text" id="txt_sub24mMod" name="txt_sub24mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Sub-36m.
            <input title="subsidio para contrato por 36 meses" type="text" id="txt_sub36mMod" name="txt_sub36mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>

          <!-- precio del servicio para todas las vigencias -->
          <label class="colum-3">Precio-12m.
            <input title="precio para contrato por 12 meses" type="text" id="txt_precio12mMod" name="txt_precio12mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Precio-24m.
            <input title="precio para contrato por 24 meses" type="text" id="txt_precio24mMod" name="txt_precio24mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Precio-36m.
            <input title="precio para contrato por 36 meses" type="text" id="txt_precio36mMod" name="txt_precio36mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>

          <!-- cuotaIns del servicio para todas las vigencias -->
          <label class="colum-3">Cuota Instalacion-12m.
            <input title="cuota por instalacion" type="text" id="txt_cuotaIns12mMod" name="txt_cuotaIns12mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Cuota Instalacion-24m.
            <input title="cuota por instalacion" type="text" id="txt_cuotaIns24mMod" name="txt_cuotaIns24mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Cuota Instalacion-36m.
            <input title="cuota por instalacion" type="text" id="txt_cuotaIns36mMod" name="txt_cuotaIns36mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>

          <!-- folio_ift del servicio para todas las vigencias -->
          <label class="colum-3">Folio_ift_12m.
            <input title="folio_ift para contrato por 12 meses" type="text" id="txt_folio_ift12mMod" name="txt_folio_ift12mMod" class="form-control" placeholder="000000" size="5" maxlength="6" number>
          </label>
          <label class="colum-3">Folio_ift_24m.
            <input title="folio_ift para contrato por 24 meses" type="text" id="txt_folio_ift24mMod" name="txt_folio_ift24mMod" class="form-control" placeholder="000000" size="5" maxlength="6" number>
          </label>
          <label class="colum-3">Folio_ift_36m.
            <input title="folio_ift para contrato por 36 meses" type="text" id="txt_folio_ift36mMod" name="txt_folio_ift36mMod" class="form-control" placeholder="000000" size="5" maxlength="6" number>
          </label>

          <!-- costo del servicio para todas las vigencias -->
          <label class="colum-3">Costo-12m.
            <input title="costo para contrato por 12 meses" type="text" id="txt_costo12mMod" name="txt_costo12mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Costo-24m.
            <input title="costo para contrato por 24 meses" type="text" id="txt_costo24mMod" name="txt_costo24mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Costo-36m.
            <input title="costo para contrato por 36 meses" type="text" id="txt_costo36mMod" name="txt_costo36mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>

          <!-- cargo_adecuaciones_cancelacion del servicio para todas las vigencias -->
          <label class="colum-3">Cargo_adecuaciones_cancelacion-12m.
            <input title="cargo de adecuaciones por cancelacion para contrato por 12 meses" type="text" id="txt_cargoAdecCancel12mMod" name="txt_cargoAdecCancel12mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Cargo_adecuaciones_cancelacion-24m.
            <input title="cargo de adecuaciones por cancelacion para contrato por 24 meses" type="text" id="txt_cargoAdecCancel24mMod" name="txt_cargoAdecCancel24mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Cargo_adecuaciones_cancelacion-36m.
            <input title="cargo de adecuaciones por cancelacion para contrato por 36 meses" type="text" id="txt_cargoAdecCancel36mMod" name="txt_cargoAdecCancel36mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>

          <!-- Adecuaciones del servicio para todas las vigencias -->
          <label class="colum-3">Adecuaciones-12m.
            <input title="Adecuaciones para contrato por 12 meses" type="text" id="txt_Adec12mMod" name="txt_Adec12mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Adecuaciones-24m.
            <input title="Adecuaciones para contrato por 24 meses" type="text" id="txt_Adec24mMod" name="txt_Adec24mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Adecuaciones-36m.
            <input title="Adecuaciones para contrato por 36 meses" type="text" id="txt_Adec36mMod" name="txt_Adec36mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>

          <!--  -->
          <!--  -->

          <br><br><br>
          <!-- <div class="row">
            <div><button type="button" id="btn_autoFill" title="Auto rellenar los campos de estas secciones con valores minimos" class="btn btn-primary" onClick="fill_6_18_30Mod();"><span class="glyphicon glyphicon-asterisk"></span>Auto llenar vigencias 6,18 y 30 Meses</button></div>
          </div><br><br> -->
          <!-- subsidio del servicio para todas las vigencias -->
          <label class="colum-3">Sub-6m.
            <input title="subsidio para contrato por 6 meses" type="text" id="txt_sub6mMod" name="txt_sub6mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Sub-18m.
            <input title="subsidio para contrato por 18 meses" type="text" id="txt_sub18mMod" name="txt_sub18mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Sub-30m.
            <input title="subsidio para contrato por 30 meses" type="text" id="txt_sub30mMod" name="txt_sub30mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>

          <!-- precio del servicio para todas las vigencias -->
          <label class="colum-3">Precio-6m.
            <input title="precio para contrato por 6 meses" type="text" id="txt_precio6mMod" name="txt_precio6mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Precio-18m.
            <input title="precio para contrato por 12 meses" type="text" id="txt_precio18mMod" name="txt_precio18mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Precio-30m.
            <input title="precio para contrato por 30 meses" type="text" id="txt_precio30mMod" name="txt_precio30mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>

          <!-- folio_ift del servicio para todas las vigencias -->
          <label class="colum-3">Folio_ift_6m.
            <input title="folio_ift para contrato por 6 meses" type="text" id="txt_folio_ift6mMod" name="txt_folio_ift6mMod" class="form-control" placeholder="000000" size="5" maxlength="6" number>
          </label>
          <label class="colum-3">Folio_ift_18m.
            <input title="folio_ift para contrato por 18 meses" type="text" id="txt_folio_ift18mMod" name="txt_folio_ift18mMod" class="form-control" placeholder="000000" size="5" maxlength="6" number>
          </label>
          <label class="colum-3">Folio_ift_30m.
            <input title="folio_ift para contrato por 30 meses" type="text" id="txt_folio_ift30mMod" name="txt_folio_ift30mMod" class="form-control" placeholder="000000" size="5" maxlength="6" number>
          </label>

          <!-- costo del servicio para todas las vigencias -->
          <label class="colum-3">Costo-6m.
            <input title="costo para contrato por 6 meses" type="text" id="txt_costo6mMod" name="txt_costo6mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Costo-18m.
            <input title="costo para contrato por 18 meses" type="text" id="txt_costo18mMod" name="txt_costo18mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Costo-30m.
            <input title="costo para contrato por 30 meses" type="text" id="txt_costo30mMod" name="txt_costo30mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>

          <!-- Adecuaciones del servicio para todas las vigencias -->
          <label class="colum-3">Adecuaciones-6m.
            <input title="Adecuaciones para contrato por 6 meses" type="text" id="txt_Adec6mMod" name="txt_Adec6mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Adecuaciones-18m.
            <input title="Adecuaciones para contrato por 18 meses" type="text" id="txt_Adec18mMod" name="txt_Adec18mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <label class="colum-3">Adecuaciones-30m.
            <input title="Adecuaciones para contrato por 30 meses" type="text" id="txt_Adec30mMod" name="txt_Adec30mMod" class="form-control" placeholder="000.00" size="5" maxlength="11" number>
          </label>
          <!--  -->
          <!--  -->
        </div>
        <!-- fin del modal para insersion de los servicios -->
        <div class="modal-footer">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
          <input type="submit" class="btn btn-primary" id="guardaServicioMod" value="Guardar Cambios" onClick="saveModService();">
        </div>
      </div>
    </div>
  </div>

  <div class="container-fluid" id="footer"><?php include("../../libs/templates/footer.php") ?></div>
  <script>
    getListServices();
  </script>
</body>

</html>