<?php
session_start();
set_time_limit(0);
include_once "../../libs/db/common.php";
include_once "../../libs/db/dbcommon.php";
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');
$title = "Herramientas/Editor de Precios";
$usuario = $_SESSION['usuario'];
$gruposArr  = $_SESSION['grupos'];
$usrId = $_SESSION['usrId'];
$usrRegion = $_SESSION['usrUbica'];
$salida = "";

if(false) {
    // if (!verifySession($_SESSION)) {
  logoutTimeout();
}else{
    // lista de servicios
    if ("mainLoad" == $_POST['action']) {
        $db_catalogos = condb_catalogos();
        if ((array_key_exists(1, $gruposArr)) || (array_key_exists(2, $gruposArr)) || (array_key_exists(11, $gruposArr))) {
            $query = "SELECT cvs.*, cvts.tipo_servicio AS tipSer, cvts.categoria_id, cvc.categoria, ccu.ubicacion_nombre " .
                "FROM " . $cfgTableNameCat . ".cat_venta_servicios AS cvs, " .
                $cfgTableNameCat . ".cat_venta_tipo_servicios AS cvts, " .
                $cfgTableNameCat . ".cat_venta_categorias AS cvc, " .
                $cfgTableNameCat . ".cat_coe_ubicacion AS ccu " .
                "WHERE cvs.tipo_servicio = cvts.tipo_servicio_id " .
                "AND cvc.categoria_id = cvts.categoria_id " .
                "AND servicio_vigencia_inicio > '2018-11-18 11:59:59' " .
                "AND ccu.ubicacion_id = cvs.region_id " .
                "ORDER BY cvs.vigente ASC, cvts.categoria_id ASC, cvs.tipo_servicio ";
        } else {
            $query = "SELECT cvs.*, cvts.tipo_servicio AS tipSer, cvts.categoria_id, cvc.categoria " .
                "FROM " . $cfgTableNameCat . ".cat_venta_servicios AS cvs, " .
                $cfgTableNameCat . ".cat_venta_tipo_servicios AS cvts, " .
                $cfgTableNameCat . ".cat_venta_categorias AS cvc " .
                "WHERE cvs.region_id = " . $usrRegion . " " .
                "AND cvs.tipo_servicio = cvts.tipo_servicio_id " .
                "AND cvc.categoria_id = cvts.categoria_id " .
                "AND servicio_vigencia_inicio > '2018-11-18 11:59:59' " .
                "ORDER BY cvts.categoria_id ASC, cvs.tipo_servicio ";
        }
        // $query .= "LIMIT 5";
        $result = mysqli_query($db_catalogos, $query);

        if (!$result) {
            $salida = "ERROR: Ocurrió un problema al obtener los datos de productos";
        } else {
            $salida = "<table id=\"tbl_pricesTable\" class=\"table table-striped\">" .
                "  <thead>\n" .
                "    <tr>\n" .
                "      <th>Categoría</th>\n" .
                "      <th>Servicio</th>\n" .
                "      <th>Descripción</th>\n" .
                "      <th>Tipo Servicio</th>\n";
            if ((array_key_exists(1, $gruposArr)) || (array_key_exists(2, $gruposArr)) || (array_key_exists(11, $gruposArr))) {
                $salida .= "      <th>Region</th>\n";
            }
            $salida .= "      <th>Bajada</th>\n" .
                "      <th>Subida</th>\n" .
                "      <th>Precio Base</th>\n" .
                "      <th>Precio Medio</th>\n" .
                "      <th>Precio Lista</th>\n" .
                "      <th>Vigencia Inicio</th>\n" .
                "      <th>Vigencia Fin</th>\n" .
                "      <th>Acciones</th>\n" .
                "    </tr>\n" .
                "  </thead>\n" .
                "  <tbody>\n";

            while ($row = mysqli_fetch_assoc($result)) {
                if ("0000-00-00 00:00:00" == $row['servicio_vigencia_inicio'])
                    $iniVig = "<i>permanente</i>";
                else
                    $iniVig = $row['servicio_vigencia_inicio'];
                if ("0000-00-00 00:00:00" == $row['servicio_vigencia_fin'])
                    $finVig = "<i>permanente</i>";
                else
                    $finVig = $row['servicio_vigencia_fin'];
                $salida .= "    <tr>\n" .
                    "      <td>" . $row['categoria'] . "</td>\n" .
                    "      <td>" . $row['servicio'] . "</td>\n" .
                    "      <td>" . $row['descripcion'] . "</td>\n" .
                    "      <td>" . $row['tipSer'] . "</td>\n";
                if ((array_key_exists(1, $gruposArr)) || (array_key_exists(2, $gruposArr)) || (array_key_exists(11, $gruposArr))) {
                    $salida .= "      <td>" . $row['ubicacion_nombre'] . "</td>\n";
                }
                $salida .= "      <td>" . $row['bajada'] . "</td>\n" .
                    "      <td>" . $row['subida'] . "</td>\n" .
                    "      <td>$" . number_format($row['precio_base'], 2, '.', ',') . "</td>\n" .
                    "      <td>$" . number_format($row['precio_medio'], 2, '.', ',') . "</td>\n" .
                    "      <td>$" . number_format($row['precio_lista'], 2, '.', ',') . "</td>\n" .
                    "      <td>" . $iniVig . "</td>\n" .
                    "      <td>" . $finVig . "</td>\n" .
                    "      <td>";
                $salida .= "        <button type=\"button\" id=\"btn_priceDetails_" . $row['servicio_id'] . "\" title=\"Ver Detalles\" " .
                    "class=\"btn btn-xs btn-primary btn-responsive\" onClick=\"getServiceDetails('" . encrypt($row['servicio_id']) . "');\"><span class=\"glyphicon glyphicon-list\"></span></button>";
                if ((array_key_exists(1, $gruposArr)) || (array_key_exists(2, $gruposArr)) || (array_key_exists(11, $gruposArr))) {
                    $salida .= "    <button type=\"button\" id=\"btn_priceEdit_" . $row['servicio_id'] . "\" title=\"Editar Servicio\" " .
                        "class=\"btn btn-xs btn-success btn-responsive\" onClick=\"editService('" . encrypt($row['servicio_id']) . "');\">" .
                        "<span class=\"glyphicon glyphicon-edit\"></span></button>";
                        if($row['vigente'] == 1){
                        $salida .= "  <button type=\"button\" id=\"btn_priceDelete_" . $row['servicio_id'] . "\" title=\"Desactivar vigencia\" " .
                            "class=\"btn btn-xs btn-danger btn-responsive\" onClick=\"changeVigencia('" . encrypt($row['servicio_id']) . "', 0);\">" .
                            "<span class=\"glyphicon glyphicon-ban-circle\"></span></button>";
                        }else{
                        $salida .= "  <button type=\"button\" id=\"btn_priceDelete_" . $row['servicio_id'] . "\" title=\"Activar vigencia\" " .
                            "class=\"btn btn-xs btn-default btn-responsive\" onClick=\"changeVigencia('" . encrypt($row['servicio_id']) . "', 1);\">" .
                            "<span class=\"glyphicon glyphicon-ok-circle\"></span></button>";
                        }
                    
                }
                $salida .= "</td>\n" .
                    "    </tr>\n";
            }
            $salida .= "  </tbody>\n";
        }
        mysqli_close($db_catalogos);
        echo $salida;
    }
    if ("serviceDetails" == $_POST['action']) {
        $option = isset($_POST['option']) ? $_POST['option'] : -1;
        $serId = isset($_POST['serId']) ? decrypt($_POST['serId']) : -1;
        $db_modulos = condb_modulos();
        $db_catalogos = condb_catalogos();
        $queryRowCheckAdec = "SELECT DISTINCT(COLUMN_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '" . $cfgTableNameCat . "' " .
            "AND TABLE_NAME = 'cat_venta_servicios' AND (COLUMN_NAME LIKE 'sub_%m' OR COLUMN_NAME LIKE 'precio_%m' OR COLUMN_NAME LIKE 'costo_%m' OR COLUMN_NAME LIKE 'adec_%m' OR COLUMN_NAME LIKE 'cargo_adecuacion_%m' OR COLUMN_NAME LIKE 'folio_ift_%m') ORDER BY COLUMN_NAME DESC";
        $query = "SELECT cvs.*, cvts.tipo_servicio, ccu.ubicacion_nombre, cvtc.tipo_cobro " .
            "FROM " . $cfgTableNameCat . ".cat_venta_servicios AS cvs, " .
            $cfgTableNameCat . ".cat_venta_tipo_servicios AS cvts, " .
            $cfgTableNameCat . ".cat_coe_ubicacion AS ccu, " .
            $cfgTableNameCat . ".cat_venta_tipo_cobro AS cvtc " .
            "WHERE cvs.servicio_id = " . $serId . " " .
            "AND cvtc.tipo_cobro_id = cvs.tipo_cobro " .
            "AND ccu.ubicacion_id = cvs.region_id " .
            "AND cvs.tipo_servicio = cvts.tipo_servicio_id";
        $resultRowCheckAdec = mysqli_query($db_catalogos, $queryRowCheckAdec);
        $result = mysqli_query($db_modulos, $query);
        if (!$result || !$resultRowCheckAdec)
            $salida = "ERROR: Ocurrió un error al obtenerse los datos del servicio";
        else {
            $row = mysqli_fetch_assoc($result);
            if (0 == $option) {
                $salida = "<div class=\"table-responsive\"><table id=\"tbl_serDetails\" class=\"table table-striped\">" .
                    "  <tbody>" .
                    "    <tr>" .
                    "      <td colspan=\"3\"><b>Servicio</b></td>" .
                    "      <td colspan=\"4\">" . $row['servicio'] . "</td>" .
                    "      <td colspan=\"3\"><b>Tipo Servicio</b></td>" .
                    "      <td colspan=\"4\">" . $row['tipo_servicio'] . "</td>" .
                    "    </tr>" .
                    "    <tr>" .
                    "      <td colspan=\"3\"><b>Bajada</b></td>" .
                    "      <td colspan=\"4\">" . $row['bajada'] . "</td>" .
                    "      <td colspan=\"3\"><b>Subida</b></td>" .
                    "      <td colspan=\"4\">" . $row['subida'] . "</td>" .
                    "    </tr>" .
                    "    <tr>" .
                    "      <td colspan=\"3\"><b>Region</b></td>" .
                    "      <td colspan=\"4\">" . $row['ubicacion_nombre'] . "</td>" .
                    "      <td colspan=\"3\"><b>Promoción</b></td>";
                if (0 == $row['promo'])
                    $salida .= "      <td colspan=\"4\">No</td>";
                elseif (1 == $row['promo']) {
                    $salida .= "      <td colspan=\"4\">Sí</td>" .
                        "    </tr>" .
                        "    <tr>" .
                        "      <td colspan=\"2\"><b>Inicio Vigencia</b></td>" .
                        "      <td colspan=\"3\">" . $row['servicio_vigencia_inicio'] . "</td>" .
                        "      <td colspan=\"2\"><b>Fin Vigencia</b></td>" .
                        "      <td colspan=\"3\">" . $row['servicio_vigencia_fin'] . "</td>" .
                        "      <td colspan=\"2\"><b>Activo</b></td>";
                    if (0 == $row['vigente'])
                        $salida .= "      <td colspan=\"3\">No</td>";
                    elseif (1 == $row['vigente'])
                        $salida .= "      <td colspan=\"3\">Sí</td>";
                }
                $salida .= "    </tr>" .
                    "    <tr>" .
                    "      <td colspan=\"2\"><b>Precio Base</b></td>" .
                    "      <td colspan=\"3\">" . number_format($row['precio_base'], 2, '.', ',') . "</td>" .
                    "      <td colspan=\"2\"><b>Precio Medio</b></td>" .
                    "      <td colspan=\"3\">" . number_format($row['precio_medio'], 2, '.', ',') . "</td>" .
                    "      <td colspan=\"2\"><b>Precio de Lista</b></td>" .
                    "      <td colspan=\"3\">" . number_format($row['precio_lista'], 2, '.', ',') . "</td>" .
                    "    </tr>" .
                    "    <tr>" .
                    "      <td colspan=\"3\"><b>Descripción</b></td>" .
                    "      <td colspan=\"12\">" . $row['descripcion'] . "</td>" .
                    "    </tr>" .
                    "  </tbody>" .
                    "</table>";
                $salida .= "<div class=\"table-responsive\"><table id=\"tbl_serDetailsSubs\" class=\"table table-striped\">";
                $c = 1;
                while ($rowCheckAdec = mysqli_fetch_assoc($resultRowCheckAdec)) {
                    if (1 == $c)
                        $salida .= "    <tr>";
                    $salida .= "      <td><b>" . $rowCheckAdec['COLUMN_NAME'] . "</b></td>" .
                        "      <td>$ " . number_format($row[$rowCheckAdec['COLUMN_NAME']], 2, '.', ',') . "</td>";
                    if (6 == $c) {
                        $salida .= "    </tr>";
                        $c = 0;
                    }
                    $c++;
                }
                $salida .= "  </tbody>" .
                    "</table></div>";
            }
        }
        mysqli_close($db_modulos);
        echo $salida;
    }
    if("saveServiceNew" == $_POST['action']){
        // validar recepcion de los valores por POST
            $servicio = isset($_POST['servicio']) ? $_POST['servicio'] : "";
            $descripcion = isset($_POST['descripcion']) ? $_POST['descripcion'] : "";
            $tipoServicio = isset($_POST['tipoServicio']) ? $_POST['tipoServicio'] : 1;
            $tipoCobro = isset($_POST['tipoCobro']) ? $_POST['tipoCobro'] : 1;
            $regArr = isset($_POST['regArr']) ? $_POST['regArr'] : [];
            $subida = isset($_POST['subida']) ? $_POST['subida'] : 1;
            $bajada = isset($_POST['bajada']) ? $_POST['bajada'] : 1;
            $distanciaMax = isset($_POST['distanciaMax']) ? $_POST['distanciaMax'] : 0;
            $tiempoInstall = isset($_POST['tiempoInstall']) ? $_POST['tiempoInstall'] : 1;
            $precioBase = isset($_POST['precioBase']) ? $_POST['precioBase'] : 0;
            $precioMedio = isset($_POST['precioMedio']) ? $_POST['precioMedio'] : 0;
            $precioLista = isset($_POST['precioLista']) ? $_POST['precioLista'] : 0;
            $inscripcion = isset($_POST['inscripcion']) ? $_POST['inscripcion'] : 0;
            $kilometros = isset($_POST['kilometros']) ? $_POST['kilometros'] : 0;
            $inicioVigencia = isset($_POST['inicioVigencia']) ? $_POST['inicioVigencia'] : -1;
            $finVigencia = isset($_POST['finVigencia']) ? $_POST['finVigencia'] : -1;
            $sub6m = isset($_POST['sub6m']) ? $_POST['sub6m'] : 0;
            $sub12m = isset($_POST['sub12m']) ? $_POST['sub12m'] : 0;
            $sub24m = isset($_POST['sub24m']) ? $_POST['sub24m'] : 0;
            $sub36m = isset($_POST['sub36m']) ? $_POST['sub36m'] : 0;
            $precio6m = isset($_POST['precio6m']) ? $_POST['precio6m'] : 0;
            $precio12m = isset($_POST['precio12m']) ? $_POST['precio12m'] : 0;
            $precio24m = isset($_POST['precio24m']) ? $_POST['precio24m'] : 0;
            $precio36m = isset($_POST['precio36m']) ? $_POST['precio36m'] : 0;
            $ift6m = isset($_POST['ift6m']) ? $_POST['ift6m'] : 0;
            $ift12m = isset($_POST['ift12m']) ? $_POST['ift12m'] : 0;
            $ift24m = isset($_POST['ift24m']) ? $_POST['ift24m'] : 0;
            $ift36m = isset($_POST['ift36m']) ? $_POST['ift36m'] : 0;
            $costo6m = isset($_POST['costo6m']) ? $_POST['costo6m'] : 0;
            $costo12m = isset($_POST['costo12m']) ? $_POST['costo12m'] : 0;
            $costo24m = isset($_POST['costo24m']) ? $_POST['costo24m'] : 0;
            $costo36m = isset($_POST['costo36m']) ? $_POST['costo36m'] : 0;
            // $cargoAdecCancel6m = isset($_POST['cargoAdecCancel6m']) ? $_POST['cargoAdecCancel6m'] : 0;
            $cargoAdecCancel12m = isset($_POST['cargoAdecCancel12m']) ? $_POST['cargoAdecCancel12m'] : 0;
            $cargoAdecCancel24m = isset($_POST['cargoAdecCancel24m']) ? $_POST['cargoAdecCancel24m'] : 0;
            $cargoAdecCancel36m = isset($_POST['cargoAdecCancel36m']) ? $_POST['cargoAdecCancel36m'] : 0;
            $adec6m = isset($_POST['adec6m']) ? $_POST['adec6m'] : 0;
            $adec12m = isset($_POST['adec12m']) ? $_POST['adec12m'] : 0;
            $adec24m = isset($_POST['adec24m']) ? $_POST['adec24m'] : 0;
            $adec36m = isset($_POST['adec36m']) ? $_POST['adec36m'] : 0;
            $sub18m = isset($_POST['sub18m']) ? $_POST['sub18m'] : 0;
            $precio18m = isset($_POST['precio18m']) ? $_POST['precio18m'] : 0;
            $ift18m = isset($_POST['ift18m']) ? $_POST['ift18m'] : 0;
            $costo18m = isset($_POST['costo18m']) ? $_POST['costo18m'] : 0;
            // $cargoAdecCancel18m = isset($_POST['cargoAdecCancel18m']) ? $_POST['cargoAdecCancel18m'] : 0;
            $adec18m = isset($_POST['adec18m']) ? $_POST['adec18m'] : 0;
            $sub30m = isset($_POST['sub30m']) ? $_POST['sub30m'] : 0;
            $precio30m = isset($_POST['precio30m']) ? $_POST['precio30m'] : 0;
            $ift30m = isset($_POST['ift30m']) ? $_POST['ift30m'] : 0;
            $costo30m = isset($_POST['costo30m']) ? $_POST['costo30m'] : 0;
            // $cargoAdecCancel30m = isset($_POST['cargoAdecCancel30m']) ? $_POST['cargoAdecCancel30m'] : 0;
            $adec30m = isset($_POST['adec30m']) ? $_POST['adec30m'] : 0;
            $cuotaInstall12 = isset($_POST['cuotaInstall12']) ? $_POST['cuotaInstall12'] : 0;
            $cuotaInstall24 = isset($_POST['cuotaInstall24']) ? $_POST['cuotaInstall24'] : 0;
            $cuotaInstall36 = isset($_POST['cuotaInstall36']) ? $_POST['cuotaInstall36'] : 0;
        
            // se cpmentaron por que de momento no se planea que se usen 
            // $cargoAdec6m = isset($_POST['cargoAdec6m']) ? $_POST['cargoAdec6m'] : 0;
            // $cargoAdec12m = isset($_POST['cargoAdec12m']) ? $_POST['cargoAdec12m'] : 0;
            // $cargoAdec24m = isset($_POST['cargoAdec24m']) ? $_POST['cargoAdec24m'] : 0;
            // $cargoAdec36m = isset($_POST['cargoAdec36m']) ? $_POST['cargoAdec36m'] : 0;
            $fechaActual = date("Y-m-d H:i:s");
        // fin de validar recepcion de los valores por POST
        $db_catalogos = condb_catalogos();
        for($i=0;$i<count($regArr);$i++){
        //     var_dump($regArr[$i]);
            $query = "INSERT INTO " . $cfgTableNameCat . ".cat_venta_servicios (servicio, descripcion, tipo_servicio, bajada, subida, distancia_max, tiempo_instalacion, precio_base, precio_medio, precio_lista, inscripcion, tipo_cobro, kilometros, vigente, servicio_vigencia_inicio, servicio_vigencia_fin, promo, region_id, precio_6m, cargo_adecuacion_6m, costo_6m, sub_6m, adec_6m, folio_ift_6m, precio_12m, cuota_instalacion_12m, cargo_adecuacion_12m, cargo_adecuacion_cancelacion_12m, costo_12m, sub_12m, adec_12m, folio_ift_12m, precio_18m, cargo_adecuacion_18m, costo_18m, sub_18m, adec_18m, folio_ift_18m, precio_24m, cuota_instalacion_24m, cargo_adecuacion_24m, cargo_adecuacion_cancelacion_24m, costo_24m, sub_24m, adec_24m, folio_ift_24m, precio_30m, cargo_adecuacion_30m, costo_30m, sub_30m, adec_30m, folio_ift_30m, precio_36m, cuota_instalacion_36m, cargo_adecuacion_36m, cargo_adecuacion_cancelacion_36m, costo_36m, sub_36m, adec_36m, folio_ift_36m, usuario_alta, usuario_modificacion, fecha_creacion, fecha_modificacion) VALUES (' $servicio', '$descripcion', $tipoServicio, $bajada, $subida, $distanciaMax, $tiempoInstall, $precioBase, $precioMedio, $precioLista, $inscripcion, $tipoCobro, $kilometros, 1, '$inicioVigencia 00:00:00', '$finVigencia 23:59:59', 0, $regArr[$i], $precio6m, $adec6m, $costo6m, $sub6m, $adec6m, $ift6m, $precio12m, $cuotaInstall12, $adec12m, $cargoAdecCancel12m, $costo12m, $sub12m, $adec12m, $ift12m, $precio18m, $adec18m, $costo18m, $sub18m, $adec18m, $ift18m, $precio24m, $cuotaInstall24, $adec24m, $cargoAdecCancel24m, $costo24m, $sub24m, $adec24m, $ift24m, $precio30m, $adec30m, $costo30m, $sub30m, $adec30m, $ift30m, $precio36m, $cuotaInstall36, $adec36m, $cargoAdecCancel36m, $costo36m, $sub36m, $adec36m, $ift36m, $usrId, $usrId, '$fechaActual', '$fechaActual') ";
            $insertNewService = mysqli_query($db_catalogos, $query);
        }

        if (!$insertNewService) {
            echo "ERROR||No se pudo agregar el servicio en este momento, por favor intentalo mas tarde! ";
        } else {
            $id = $db_catalogos->insert_id;
            if($id > 0){
                echo "OK|| Se almaceno el servicio ";
            }else{
                echo "ERROR||Ocurrio un problema al almacenar el servicio!! <br> Contacta al Administrador. ";
            }
            
        }
        mysqli_close($db_catalogos);
    }
    if("getTipoCobro" == $_POST['action']){
        $db_catalogos = condb_catalogos();
        $query = "SELECT * FROM ". $cfgTableNameCat.".cat_venta_tipo_cobro;";
        $resultRowTipoCobro = mysqli_query($db_catalogos, $query);
        if(!$resultRowTipoCobro){
            $salida = "ERROR: Ocurrió un error al obtenerse los tipo de cobros";
        }else{
            $salida = "OK||";
            while($row = mysqli_fetch_assoc($resultRowTipoCobro)){
                $salida .= "<option value=" . $row['tipo_cobro_id'] . ">" . $row['tipo_cobro'] . "</option>";
            }
        }
        mysqli_close($db_catalogos);
        echo $salida;
    }
    if ("getTipoServicio" == $_POST['action']) {
        $db_catalogos = condb_catalogos();
        $query = "SELECT * FROM " . $cfgTableNameCat . ".cat_venta_tipo_servicios;";
        $resultRowTipoServicio = mysqli_query($db_catalogos, $query);
        if (!$resultRowTipoServicio) {
            $salida = "ERROR: Ocurrió un error al obtenerse los tipo de cobros";
        } else {
            $salida = "OK||";
            while ($row = mysqli_fetch_assoc($resultRowTipoServicio)) {
                $salida .= "<option value=" . $row['tipo_servicio_id'] . ">" . $row['tipo_servicio'] . "</option>";
            }
        }
        mysqli_close($db_catalogos);
        echo $salida;
    }
    if ("getRegiones" == $_POST['action']) {
        $db_catalogos = condb_catalogos();
        $query = "SELECT * FROM " . $cfgTableNameCat . ".cat_coe_ubicacion;";
        $resultRowTipoServicio = mysqli_query($db_catalogos, $query);
        if (!$resultRowTipoServicio) {
            $salida = "ERROR: Ocurrió un error al obtenerse los tipo de cobros";
        } else {
            $salida = "OK||";
            while ($row = mysqli_fetch_assoc($resultRowTipoServicio)) {
                $salida .= "<label><input name='checkReg' type='checkbox' id='" . $row['ubicacion_id'] . "' value='" . $row['ubicacion_id'] . "'> ". $row['ubicacion_nombre']."</label>" ;
            }
        }
        mysqli_close($db_catalogos);
        echo $salida;
    }
    if("getServicesToPromo" == $_POST['action']){
        // seleccionar los servicios para su despliegue en la lista de servicios pra promocion
        $db_catalogos = condb_catalogos();
        $idReg = isset($_POST['idReg']) ? $_POST['idReg'] : 1;
        $query = "SELECT * 
                FROM coecrm_catalogos.cat_venta_servicios as cvs, 
                coecrm_catalogos.cat_venta_tipo_servicios as cvtc  
                WHERE cvs.promo = 0 
                AND cvs.vigente = 1 
                AND cvs.region_id = $idReg 
                AND cvs.tipo_servicio = cvtc.tipo_servicio_id 
                AND (cvtc.categoria_id = 1 
                OR cvtc.categoria_id = 3 
                OR cvtc.categoria_id = 2) 
                order by servicio ASC";
                // $query = "SELECT * 
                // FROM coecrm_catalogos.cat_venta_servicios as cvs, 
                // coecrm_catalogos.cat_venta_tipo_servicios as cvtc  
                // WHERE cvs.promo = 0 
                // AND cvs.vigente = 1 
                // AND cvs.region_id = $idReg 
                // AND cvs.tipo_servicio = cvtc.tipo_servicio_id 
                // AND (cvtc.categoria_id = 1 
                // OR cvtc.categoria_id = 1 
                // OR cvtc.categoria_id = 1) 
                // order by servicio ASC";
        $result = mysqli_query($db_catalogos, $query);
        if (!$result) {
            $salida = "ERROR: Ocurrió un problema al obtener los datos de productos";
        } else {
            $salida .= "<option id='0' value='0'>selecciona un servicio..</option>";
            while ($row = mysqli_fetch_assoc($result)) {
            $salida .= "<option id=" . encrypt($row['servicio_id']) . " value=" . encrypt($row['servicio_id']) . ">" . $row['servicio'] . "</option>";
            }
        }
        mysqli_close($db_catalogos);
        echo $salida;
    }
    if("getPricesServicePromo" == $_POST['action']){
        // obtencion de los precios para crear una 
        $db_catalogos = condb_catalogos();
        $id = isset($_POST['id_servicio']) ? decrypt($_POST['id_servicio']) : -1;
        $query = "SELECT * 
                FROM coecrm_catalogos.cat_venta_servicios as cvs
                WHERE servicio_id = $id";
        $result = mysqli_query($db_catalogos, $query);
        if (!$result) {
            $salida = "ERROR: Ocurrió un problema al obtener los datos de productos";
        } else {
            $row = mysqli_fetch_assoc($result);
            $salida .= "<h4>".$row['servicio'].".</h4>
                <p id='descripcionPromo'>".$row['descripcion']."</p>
                <span style='display:none;' id='idServiceToPromo'>".encrypt($row['servicio_id'])."</span>
                <label class='colum-4'>Precio 12 Meses.
                <input title='Precio que aplica para la vigencia de 12 meses.' type='text' id='pricePromo12m' name='pricePromo12m' class='form-control' size='5' maxlength='11' value='".number_format($row['precio_12m'], 2, '.', '')."'>
                </label>
                <label class='colum-4'>Precio 24 Meses.
                <input title='Precio que aplica para la vigencia de 24 meses.' type='text' id='pricePromo24m' name='pricePromo24m' class='form-control' size='5' maxlength='11' value='".number_format($row['precio_24m'], 2, '.', '')."'>
                </label>
                <label class='colum-4'>Precio 36 Meses.
                <input title='Precio que aplica para la vigencia de 36 meses.' type='text' id='pricePromo36m' name='pricePromo36m' class='form-control' size='5' maxlength='11' value='".number_format($row['precio_36m'], 2, '.', '')."'>
                </label>";
        }
        mysqli_close($db_catalogos);
        echo $salida;
    }
    if ("getRegionesList_" == $_POST['action']) {
        $db_catalogos = condb_catalogos();
        $query = "SELECT * FROM " . $cfgTableNameCat . ".cat_coe_ubicacion;";
        $resultRowTipoServicio = mysqli_query($db_catalogos, $query);
        if (!$resultRowTipoServicio) {
            $salida = "ERROR: Ocurrió un error al obtenerse los tipo de cobros";
        } else {
            $salida = "OK||";
            while ($row = mysqli_fetch_assoc($resultRowTipoServicio)) {
                $salida .= "<option id='" . $row['ubicacion_id'] . "' value=" . $row['ubicacion_id'] . ">" . $row['ubicacion_nombre'] . "</option>";
            }
        }
        mysqli_close($db_catalogos);
        echo $salida;
    }
    if ("getRegionesList" == $_POST['action']) {
        $db_catalogos = condb_catalogos();
        $id = isset($_POST['idRegion']) ? $_POST['idRegion'] : -1;
        $query = "SELECT * FROM " . $cfgTableNameCat . ".cat_coe_ubicacion";
        $resultRowTipoServicio = mysqli_query($db_catalogos, $query);
        if (!$resultRowTipoServicio) {
            $salida = "ERROR: Ocurrió un error al obtenerse los tipo de cobros";
        } else {
            $salida = "OK||";
            while ($row = mysqli_fetch_assoc($resultRowTipoServicio)) {
                if($row['ubicacion_id'] == $id){
                    $salida .= "<option id='" . $row['ubicacion_id'] . "' value=" . $row['ubicacion_id'] . " selected>" . $row['ubicacion_nombre'] . "</option>";
                }else{
                    $salida .= "<option id='" . $row['ubicacion_id'] . "' value=" . $row['ubicacion_id'] . ">" . $row['ubicacion_nombre'] . "</option>";
                }
                
            }
        }
        mysqli_close($db_catalogos);
        echo $salida;
    }
    if("saveNewPromo" == $_POST['action']){
        $vigIni = isset($_POST['vigIni']) ? $_POST['vigIni'] : -1;
        $vigFin = isset($_POST['vigFin']) ? $_POST['vigFin'] : -1;
        $idSerBase = isset($_POST['idSerBase']) ? decrypt($_POST['idSerBase']) : -1;
        $precio12m = isset($_POST['precio12m']) ? $_POST['precio12m'] : -1;
        $precio24m = isset($_POST['precio24m']) ? $_POST['precio24m'] : -1;
        $precio36m = isset($_POST['precio36m']) ? $_POST['precio36m'] : -1;
        $mes = isset($_POST['mes']) ? $_POST['mes'] : -1;
        $meses = ['','Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
        // seleccionar todos los datos de el servicio en base a su id enviado
        $db_catalogos = condb_catalogos();
        $query = "SELECT * FROM " . $cfgTableNameCat . ".cat_venta_servicios WHERE servicio_id = $idSerBase";
        $result = mysqli_query($db_catalogos, $query);
        $fechaActual = date("Y-m-d H:i:s");
        if (!$result) {
            $salida = "ERROR: Ocurrió un error al obtenerse los datos de el servicio base";
        } else {
            $row = mysqli_fetch_assoc($result);
            // realizar la inserccion de los datos que se optuvieron y de los que se recivieron despues de validarlos
            $queryPromo = "INSERT INTO " . $cfgTableNameCat . ".cat_venta_servicios ".
                "(servicio, descripcion, tipo_servicio, bajada, subida, distancia_max, tiempo_instalacion, precio_base, precio_medio, precio_lista, inscripcion, tipo_cobro, kilometros, vigente, servicio_vigencia_inicio, servicio_vigencia_fin, promo, region_id, precio_6m, cargo_adecuacion_6m, costo_6m, sub_6m, adec_6m, folio_ift_6m, precio_12m, cuota_instalacion_12m, cargo_adecuacion_12m, cargo_adecuacion_cancelacion_12m, costo_12m, sub_12m, adec_12m, folio_ift_12m, precio_18m, cargo_adecuacion_18m, costo_18m, sub_18m, adec_18m, folio_ift_18m, precio_24m, cuota_instalacion_24m, cargo_adecuacion_24m, cargo_adecuacion_cancelacion_24m, costo_24m, sub_24m, adec_24m, folio_ift_24m, precio_30m, cargo_adecuacion_30m, costo_30m, sub_30m, adec_30m, folio_ift_30m, precio_36m, cuota_instalacion_36m, cargo_adecuacion_36m, cargo_adecuacion_cancelacion_36m, costo_36m, sub_36m, adec_36m, folio_ift_36m, usuario_alta, usuario_modificacion, fecha_creacion, fecha_modificacion) ".
            "VALUES ('".$row['servicio']." (promo ".$meses[$mes].")', '".$row['descripcion']."', ".$row['tipo_servicio'].", ".$row['bajada'].", ".$row['subida'].", ".$row['distancia_max'].", ".$row['tiempo_instalacion'].", ".$row['precio_base'].", ".$row['precio_medio'].", ".$row['precio_lista'].", ".$row['inscripcion'].", ".$row['tipo_cobro'].", ".$row['kilometros'].", ".$row['vigente'].", '".$vigIni."', '".$vigFin."',1, ".$row['region_id'].", ".$row['precio_6m'].", ".$row['cargo_adecuacion_6m'].", ".$row['costo_6m'].", ".$row['sub_6m'].", ".$row['adec_6m'].", ".$row['folio_ift_6m'].", ".$precio12m.", ".$row['cuota_instalacion_12m'].", ".$row['cargo_adecuacion_12m'].", ".$row['cargo_adecuacion_cancelacion_12m'].", ".$row['costo_12m'].", ".$row['sub_12m'].", ".$row['adec_12m'].", ".$row['folio_ift_12m'].", ".$row['precio_18m'].", ".$row['cargo_adecuacion_18m'].", ".$row['costo_18m'].", ".$row['sub_18m'].", ".$row['adec_18m'].", ".$row['folio_ift_18m'].", ".$precio24m.", ".$row['cuota_instalacion_24m'].", ".$row['cargo_adecuacion_24m'].", ".$row['cargo_adecuacion_cancelacion_24m'].", ".$row['costo_24m'].", ".$row['sub_24m'].", ".$row['adec_24m'].", ".$row['folio_ift_24m'].", ".$row['precio_30m'].", ".$row['cargo_adecuacion_30m'].", ".$row['costo_30m'].", ".$row['sub_30m'].", ".$row['adec_30m'].", ".$row['folio_ift_30m'].", ".$precio36m.", ".$row['cuota_instalacion_36m'].", ".$row['cargo_adecuacion_36m'].", ".$row['cargo_adecuacion_cancelacion_36m'].", ".$row['costo_36m'].", ".$row['sub_36m'].", ".$row['adec_36m'].", ".$row['folio_ift_36m'].", $usrId, $usrId, '$fechaActual', '$fechaActual')";
            $insertNewPromo = mysqli_query($db_catalogos, $queryPromo);
            if(!$insertNewPromo){
                $salida = "ERROR: Ocurrió un error al insertar la promocion";
            }else{
                $salida = "OK||Se agrago la promocion con exito.";
            }
        }
        mysqli_close($db_catalogos);
        echo $salida;
    }
    if("getServiceToEdit" == $_POST['action']){
        $idServicio = isset($_POST['idServicio']) ? decrypt($_POST['idServicio']) : -1;
        // seleccionar todos los datos de el servicio en base a su id enviado
        $db_catalogos = condb_catalogos();
        $query = "SELECT * FROM " . $cfgTableNameCat . ".cat_venta_servicios WHERE servicio_id = $idServicio";
        $result = mysqli_query($db_catalogos, $query);
        if (!$result) {
            $salida = "ERROR: Ocurrió un error al obtenerse los datos de el servicio base";
        } else {
            $row = mysqli_fetch_assoc($result);
            $row['servicio_id'] = encrypt($row['servicio_id']);
            $row['precio_base'] = number_format($row['precio_base'], 2, '.', '');  
            $row['precio_medio'] = number_format($row['precio_medio'], 2, '.', '');  
            $row['precio_lista'] = number_format($row['precio_lista'], 2, '.', '');  
            $row['inscripcion'] = number_format($row['inscripcion'], 2, '.', '');  
            $row['precio_6m'] = number_format($row['precio_6m'], 2, '.', '');  
            $row['cargo_adecuacion_6m'] = number_format($row['cargo_adecuacion_6m'], 2, '.', '');  
            $row['costo_6m'] = number_format($row['costo_6m'], 2, '.', '');  
            $row['sub_6m'] = number_format($row['sub_6m'], 2, '.', '');  
            $row['adec_6m'] = number_format($row['adec_6m'], 2, '.', '');  
            $row['precio_12m'] = number_format($row['precio_12m'], 2, '.', '');  
            $row['cuota_instalacion_12m'] = number_format($row['cuota_instalacion_12m'], 2, '.', '');  
            $row['cargo_adecuacion_12m'] = number_format($row['cargo_adecuacion_12m'], 2, '.', '');  
            $row['cargo_adecuacion_cancelacion_12m'] = number_format($row['cargo_adecuacion_cancelacion_12m'], 2, '.', '');  
            $row['costo_12m'] = number_format($row['costo_12m'], 2, '.', '');  
            $row['sub_12m'] = number_format($row['sub_12m'], 2, '.', '');  
            $row['adec_12m'] = number_format($row['adec_12m'], 2, '.', '');  
            $row['precio_18m'] = number_format($row['precio_18m'], 2, '.', '');  
            $row['cargo_adecuacion_18m'] = number_format($row['cargo_adecuacion_18m'], 2, '.', '');  
            $row['costo_18m'] = number_format($row['costo_18m'], 2, '.', '');  
            $row['sub_18m'] = number_format($row['sub_18m'], 2, '.', '');  
            $row['adec_18m'] = number_format($row['adec_18m'], 2, '.', '');  
            $row['precio_24m'] = number_format($row['precio_24m'], 2, '.', '');  
            $row['cuota_instalacion_24m'] = number_format($row['cuota_instalacion_24m'], 2, '.', '');  
            $row['cargo_adecuacion_24m'] = number_format($row['cargo_adecuacion_24m'], 2, '.', '');  
            $row['cargo_adecuacion_cancelacion_24m'] = number_format($row['cargo_adecuacion_cancelacion_24m'], 2, '.', '');  
            $row['costo_24m'] = number_format($row['costo_24m'], 2, '.', '');  
            $row['sub_24m'] = number_format($row['sub_24m'], 2, '.', '');  
            $row['adec_24m'] = number_format($row['adec_24m'], 2, '.', '');  
            $row['precio_30m'] = number_format($row['precio_30m'], 2, '.', '');  
            $row['cargo_adecuacion_30m'] = number_format($row['cargo_adecuacion_30m'], 2, '.', '');  
            $row['costo_30m'] = number_format($row['costo_30m'], 2, '.', '');  
            $row['sub_30m'] = number_format($row['sub_30m'], 2, '.', '');  
            $row['adec_30m'] = number_format($row['adec_30m'], 2, '.', '');  
            $row['precio_36m'] = number_format($row['precio_36m'], 2, '.', '');  
            $row['cuota_instalacion_36m'] = number_format($row['cuota_instalacion_36m'], 2, '.', '');  
            $row['cargo_adecuacion_36m'] = number_format($row['cargo_adecuacion_36m'], 2, '.', '');  
            $row['cargo_adecuacion_cancelacion_36m'] = number_format($row['cargo_adecuacion_cancelacion_36m'], 2, '.', '');  
            $row['costo_36m'] = number_format($row['costo_36m'], 2, '.', '');  
            $row['sub_36m'] = number_format($row['sub_36m'], 2, '.', '');  
            $row['adec_36m'] = number_format($row['adec_36m'], 2, '.', '');  
            
            $salida = "OK||".json_encode($row);
        }
        mysqli_close($db_catalogos);
        echo $salida;
    }
    if("updateService" == $_POST['action']){

        $servicio_id = isset($_POST['servicio_id']) ? decrypt($_POST['servicio_id']) : -1;
            $servicio = isset($_POST['servicio']) ? $_POST['servicio'] : -1;
            $descripcion = isset($_POST['descripcion']) ? $_POST['descripcion'] : -1;
            $tipo_servicio = isset($_POST['tipo_servicio']) ? $_POST['tipo_servicio'] : -1;
            $subida = isset($_POST['subida']) ? $_POST['subida'] : -1;
            $bajada = isset($_POST['bajada']) ? $_POST['bajada'] : -1;
            $distancia_max = isset($_POST['distancia_max']) ? $_POST['distancia_max'] : -1;
            $tiempo_instalacion = isset($_POST['tiempo_instalacion']) ? $_POST['tiempo_instalacion'] : -1;
            $precio_base = isset($_POST['precio_base']) ? $_POST['precio_base'] : -1;
            $precio_medio = isset($_POST['precio_medio']) ? $_POST['precio_medio'] : -1;
            $precio_lista = isset($_POST['precio_lista']) ? $_POST['precio_lista'] : -1;
            $inscripcion = isset($_POST['inscripcion']) ? $_POST['inscripcion'] : -1;
            $tipo_cobro = isset($_POST['tipo_cobro']) ? $_POST['tipo_cobro'] : -1;
            $kilometros = isset($_POST['kilometros']) ? $_POST['kilometros'] : -1;
            $servicio_vigencia_inicio = isset($_POST['servicio_vigencia_inicio']) ? $_POST['servicio_vigencia_inicio'] : -1;
            $servicio_vigencia_fin = isset($_POST['servicio_vigencia_fin']) ? $_POST['servicio_vigencia_fin'] : -1;
            $sub_12m = isset($_POST['sub_12m']) ? $_POST['sub_12m'] : -1;
            $sub_24m = isset($_POST['sub_24m']) ? $_POST['sub_24m'] : -1;
            $sub_36m = isset($_POST['sub_36m']) ? $_POST['sub_36m'] : -1;
            $precio_12m = isset($_POST['precio_12m']) ? $_POST['precio_12m'] : -1;
            $precio_24m = isset($_POST['precio_24m']) ? $_POST['precio_24m'] : -1;  
            $precio_36m = isset($_POST['precio_36m']) ? $_POST['precio_36m'] : -1;
            $folio_ift_12m = isset($_POST['folio_ift_12m']) ? $_POST['folio_ift_12m'] : -1;
            $folio_ift_24m = isset($_POST['folio_ift_24m']) ? $_POST['folio_ift_24m'] : -1;
            $folio_ift_36m = isset($_POST['folio_ift_36m']) ? $_POST['folio_ift_36m'] : -1;
            $cuota_instalacion_12m = isset($_POST['cuota_instalacion_12m']) ? $_POST['cuota_instalacion_12m'] : -1;
            $cuota_instalacion_24m = isset($_POST['cuota_instalacion_24m']) ? $_POST['cuota_instalacion_24m'] : -1;
            $cuota_instalacion_36m = isset($_POST['cuota_instalacion_36m']) ? $_POST['cuota_instalacion_36m'] : -1;
            $costo_12m = isset($_POST['costo_12m']) ? $_POST['costo_12m'] : -1;
            $costo_24m = isset($_POST['costo_24m']) ? $_POST['costo_24m'] : -1;
            $costo_36m = isset($_POST['costo_36m']) ? $_POST['costo_36m'] : -1;
            $cargo_adecuacion_cancelacion_12m = isset($_POST['cargo_adecuacion_cancelacion_12m']) ? $_POST['cargo_adecuacion_cancelacion_12m'] : -1;
            $cargo_adecuacion_cancelacion_24m = isset($_POST['cargo_adecuacion_cancelacion_24m']) ? $_POST['cargo_adecuacion_cancelacion_24m'] : -1;
            $cargo_adecuacion_cancelacion_36m = isset($_POST['cargo_adecuacion_cancelacion_36m']) ? $_POST['cargo_adecuacion_cancelacion_36m'] : -1;
            $adec_12m = isset($_POST['adec_12m']) ? $_POST['adec_12m'] : -1;
            $adec_24m = isset($_POST['adec_24m']) ? $_POST['adec_24m'] : -1;
            $adec_36m = isset($_POST['adec_36m']) ? $_POST['adec_36m'] : -1;
            // $cargo_adecuacion_12m = isset($_POST['cargo_adecuacion_12m']) ? $_POST['cargo_adecuacion_12m'] : -1;
            // $cargo_adecuacion_24m = isset($_POST['cargo_adecuacion_24m']) ? $_POST['cargo_adecuacion_24m'] : -1;
            // $cargo_adecuacion_36m = isset($_POST['cargo_adecuacion_36m']) ? $_POST['cargo_adecuacion_36m'] : -1;
            $sub_6m = isset($_POST['sub_6m']) ? $_POST['sub_6m'] : -1;
            $precio_6m = isset($_POST['precio_6m']) ? $_POST['precio_6m'] : -1;
            $folio_ift_6m = isset($_POST['folio_ift_6m']) ? $_POST['folio_ift_6m'] : -1;
            $costo_6m = isset($_POST['costo_6m']) ? $_POST['costo_6m'] : -1;
            $adec_6m = isset($_POST['adec_6m']) ? $_POST['adec_6m'] : -1;
            $sub_18m = isset($_POST['sub_18m']) ? $_POST['sub_18m'] : -1;
            $precio_18m = isset($_POST['precio_18m']) ? $_POST['precio_18m'] : -1;
            $folio_ift_18m = isset($_POST['folio_ift_18m']) ? $_POST['folio_ift_18m'] : -1;
            $costo_18m = isset($_POST['costo_18m']) ? $_POST['costo_18m'] : -1;
            $adec_18m = isset($_POST['adec_18m']) ? $_POST['adec_18m'] : -1;
            $sub_30m = isset($_POST['sub_30m']) ? $_POST['sub_30m'] : -1;
            $precio_30m = isset($_POST['precio_30m']) ? $_POST['precio_30m'] : -1;
            $folio_ift_30m = isset($_POST['folio_ift_30m']) ? $_POST['folio_ift_30m'] : -1;
            $costo_30m = isset($_POST['costo_30m']) ? $_POST['costo_30m'] : -1;
        $adec_30m = isset($_POST['adec_30m']) ? $_POST['adec_30m'] : -1;
        $fechaActual = date("Y-m-d H:i:s");
        if($servicio_id > 0){
                $db_catalogos = condb_catalogos();
                $query = "SELECT * FROM " . $cfgTableNameCat . ".cat_venta_servicios WHERE servicio_id = $servicio_id";
                $result = mysqli_query($db_catalogos, $query);
                if (!$result) {
                    $salida = "ERROR: Ocurrió un error al obtenerse los datos de el servicio base";
                } else {
                    $row = mysqli_fetch_assoc($result);
                    // realizar la inserccion de los datos que se optuvieron y de los que se recivieron despues de validarlos
                    $queryUpdate = "UPDATE coecrm_catalogos.cat_venta_servicios SET servicio = '$servicio', descripcion = '$descripcion', tipo_servicio = $tipo_servicio, bajada = $bajada, subida = $subida, distancia_max = $distancia_max, tiempo_instalacion = $tiempo_instalacion, precio_base = $precio_base, precio_medio = $precio_medio, precio_lista = $precio_lista, inscripcion = $inscripcion, tipo_cobro = $tipo_cobro, kilometros = $kilometros, servicio_vigencia_inicio = '$servicio_vigencia_inicio', servicio_vigencia_fin = '$servicio_vigencia_fin', precio_6m = $precio_6m, costo_6m = $costo_6m, sub_6m = $sub_6m, adec_6m = $adec_6m, folio_ift_6m = $folio_ift_6m, precio_12m = $precio_12m, cuota_instalacion_12m = $cuota_instalacion_12m, cargo_adecuacion_cancelacion_12m = $cargo_adecuacion_cancelacion_12m, costo_12m = $costo_12m, sub_12m = $sub_12m, adec_12m = $adec_12m, folio_ift_12m = $folio_ift_12m, precio_18m = $precio_18m, costo_18m = $costo_18m, sub_18m = $sub_18m, adec_18m = $adec_18m, folio_ift_18m = $folio_ift_18m, precio_24m = $precio_24m, cuota_instalacion_24m = $cuota_instalacion_24m, cargo_adecuacion_cancelacion_24m = $cargo_adecuacion_cancelacion_24m, costo_24m = $costo_24m, sub_24m = $sub_24m, adec_24m = $adec_24m, folio_ift_24m = $folio_ift_24m, precio_30m = $precio_30m, costo_30m = $costo_30m, sub_30m = $sub_30m, adec_30m = $adec_30m, folio_ift_30m = $folio_ift_30m, precio_36m = $precio_36m, cuota_instalacion_36m = $cuota_instalacion_36m, cargo_adecuacion_cancelacion_36m = $cargo_adecuacion_cancelacion_36m, costo_36m = $costo_36m, sub_36m = $sub_36m, adec_36m = $adec_36m, folio_ift_36m = $folio_ift_36m, usuario_modificacion = $usrId, fecha_modificacion = '$fechaActual' WHERE servicio_id = $servicio_id";
                    $updateService = mysqli_query($db_catalogos, $queryUpdate);
                    if (!$updateService) {
                        $salida = "ERROR: Ocurrió un error al actualizar el servicio.";
                    } else {
                        $salida = "OK||Se actualizo el servicio con exito.";
                    }
                }
        }else{
                $salida = "ERROR|| Ocurrió un error al obtenerse los datos de el servicio base";
        }
            mysqli_close($db_catalogos);
            echo $salida;
    }
    if ("changeVigencia" == $_POST['action']){
        $status = isset($_POST['status']) ? $_POST['status'] : -1;
        $id = isset($_POST['id']) ? decrypt($_POST['id']) : -1;
        if($id > 0){
            $db_catalogos = condb_catalogos();
            $query = "UPDATE " . $cfgTableNameCat . ".cat_venta_servicios SET vigente = $status WHERE servicio_id = $id";
            $result = mysqli_query($db_catalogos, $query);
            if (!$result) {
                $salida = "ERROR||OROcurrió un error al obtenerse los datos de el servicio base";
            } else {
                $salida = "OK||Se actualizo la vigencia del servicio";
            }
        }else{
            $salida = "ERROR|| No se puede actualizar el servicio";
        }
        mysqli_close($db_catalogos);
        echo $salida;
    }


    // **************************************
    // funciones para los usuarios
    // **************************************

    if ("getUsers" == $_POST['action']) {
        $db_catalogos = condb_catalogos();
        $db_usuarios = condb_usuarios();
        if ((array_key_exists(1, $gruposArr)) || (array_key_exists(2, $gruposArr)) || (array_key_exists(11, $gruposArr))) {
            $query = "SELECT usr.*,  ubi.ubicacion_nombre " .
                "FROM " . $cfgTableNameUsr . ".usuarios as usr " .
                ", ".$cfgTableNameCat.".cat_coe_ubicacion as ubi ".
                "WHERE usr.ubicacion_id = ubi.ubicacion_id ".
                "ORDER BY usuario_id ASC";
        }
        // $query .= "LIMIT 5";
        $result = mysqli_query($db_usuarios, $query);
        if (!$result) {
            $salida = "ERROR: Ocurrió un problema al obtener los datos de productos".$query;
        } else {
            $salida = "OK||<table id=\"tbl_usersTable\" class=\"table table-striped\">" .
                "  <thead>\n" .
                "    <tr>\n" .
                "      <th>Nombre de usuario</th>\n" .
                "      <th>Nombre</th>\n" .
                "      <th>Apellidos</th>\n" .
                "      <th>Puesto</th>\n";
            if ((array_key_exists(1, $gruposArr)) || (array_key_exists(2, $gruposArr)) || (array_key_exists(11, $gruposArr))) {
                $salida .= "      <th>Region</th>\n";
            }
            $salida .= "      <th>Acerca de</th>\n" .
                "      <th>Acciones</th>\n" .
                "    </tr>\n" .
                "  </thead>\n" .
                "  <tbody>\n";

            while ($row = mysqli_fetch_assoc($result)) {
                $salida .= "    <tr>\n" .
                    "      <td>" . $row['username'] . "</td>\n" .
                    "      <td>" . $row['nombre'] . "</td>\n" .
                    "      <td>" . $row['apellidos'] . "</td>\n" .
                    "      <td>" . $row['puesto'] . "</td>\n";
                if ((array_key_exists(1, $gruposArr)) || (array_key_exists(2, $gruposArr)) || (array_key_exists(11, $gruposArr))) {
                    $salida .= "      <td>" . $row['ubicacion_nombre'] . "</td>\n";
                }
                if (30 < strlen($row["sobre_mi"])){
                    $salida .= "      <td class=\"hidden-xs\" title=\"" . $row["sobre_mi"] . "\">" . substr($row["sobre_mi"], 0, 30) . "...</td>\n";
                }else{
                    $salida .= "      <td class=\"hidden-xs\">" . $row["sobre_mi"] . "</td>\n";
                }
                $salida .= "<td>\n";
                $salida .= "        <button type=\"button\" id=\"btn_userDetails_" . $row['usuario_id'] . "\" title=\"Ver Detalles\" " .
                    "class=\"btn btn-xs btn-primary btn-responsive\" onClick=\"getUserDetails('" . encrypt($row['usuario_id']) . "');\"><span class=\"glyphicon glyphicon-list\"></span></button>";
                    if ((array_key_exists(1, $gruposArr)) || (array_key_exists(2, $gruposArr)) || (array_key_exists(11, $gruposArr))) {
                    $salida .= "    <button type=\"button\" id=\"btn_userEdit_" . $row['usuario_id'] . "\" title=\"Editar Usuario\" " .
                        "class=\"btn btn-xs btn-success btn-responsive\" onClick=\"editUser('" . encrypt($row['usuario_id']) . "',1);\">" .
                        "<span class=\"glyphicon glyphicon-edit\"></span></button>";
                    if ($row['estatus'] == 0) {
                        $salida .= "  <button type=\"button\" id=\"btn_userDelete_" . $row['usuario_id'] . "\" title=\"Desactivar vigencia\" " .
                            "class=\"btn btn-xs btn-danger btn-responsive\" onClick=\"changeVigenciaUser('" . encrypt($row['usuario_id']) . "', 1);\">" .
                            "<span class=\"glyphicon glyphicon-ban-circle\"></span></button>";
                    } else {
                        $salida .= "  <button type=\"button\" id=\"btn_userDelete_" . $row['usuario_id'] . "\" title=\"Activar vigencia\" " .
                            "class=\"btn btn-xs btn-default btn-responsive\" onClick=\"changeVigenciaUser('" . encrypt($row['usuario_id']) . "', 0);\">" .
                            "<span class=\"glyphicon glyphicon-ok-circle\"></span></button>";
                    }
                }
                $salida .= "</td>\n" .
                    "    </tr>\n";
            }
            $salida .= "  </tbody>\n";
        }
        mysqli_close($db_usuarios);
        echo $salida;
    }

    if ("getUserData" == $_POST['action']) {
        $id = isset($_POST['id']) ? decrypt($_POST['id']) : -1;
        $db_usuarios = condb_usuarios();
        $query = "SELECT * FROM usuarios WHERE usuario_id = ".$id;
        $result = mysqli_query($db_usuarios, $query);
        if (!$result) {
            $salida = "ERROR: Ocurrió un problema al obtener los datos de productos" . $query;
        } else {
            $row = mysqli_fetch_assoc($result);
            $salida = "OK||".json_encode($row)."||".decrypt($row['contrasena']);
        }
        mysqli_close($db_usuarios);
        echo $salida;
    }

    if ("getUsers" == $_POST['action']) {

        $db_usuarios = condb_usuarios();
        $id = isset($_POST['id']) ? decrypt($_POST['id']) : -1;
        $option = isset($_POST['option']) ? decrypt($_POST['option']) : -1;
        $pass = isset($_POST['pass']) ? decrypt($_POST['pass']) : "";
        $email = isset($_POST['email']) ? decrypt($_POST['email']) : "";
        $nombre = isset($_POST['nombre']) ? decrypt($_POST['nombre']) : "";
        $apellidos = isset($_POST['apellidos']) ? decrypt($_POST['apellidos']) : "";
        $puesto = isset($_POST['puesto']) ? decrypt($_POST['puesto']) : "";
        $departamento = isset($_POST['departamento']) ? decrypt($_POST['departamento']) : "";
        $extension = isset($_POST['extension']) ? decrypt($_POST['extension']) : -1;
        $tel_movil = isset($_POST['tel_movil']) ? decrypt($_POST['tel_movil']) : -1;
        $sobre_mi = isset($_POST['sobre_mi']) ? decrypt($_POST['sobre_mi']) : "";
        $ubicacion_id = isset($_POST['ubicacion_id']) ? decrypt($_POST['ubicacion_id']) : -1;

        if(0 == $option){
            $query = "INSERT INTO ";
            $result = mysqli_query($db_usuarios, $query);
            if (!$result) {
                $salida = "ERROR: Ocurrió un problema al obtener los datos de productos" . $query;
            } else {
                $row = mysqli_fetch_assoc($result);
            }
        }else{
            $query = "UPDATE";
            $result = mysqli_query($db_usuarios, $query);
            if (!$result) {
                $salida = "ERROR: Ocurrió un problema al obtener los datos de productos" . $query;
            } else {
                $row = mysqli_fetch_assoc($result);
            }
        }

        


        mysqli_close($db_usuarios);
        echo $salida;

    }//fin de saveUser - guardar un usuario

}// FIN DEL ARCHIVO
?>