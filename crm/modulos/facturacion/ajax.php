<?php
session_start();
  set_time_limit(0);
  header('Content-Type: text/html; charset=UTF-8');
  date_default_timezone_set('America/Mexico_City');
  //include_once "../../libs/db/dbcommon.php";
  include_once "../../libs/db/common.php";
  $gruposArr  = $_SESSION['grupos'];
  $usuarioId  = $_SESSION['usrId'];
  $usuario    = $_SESSION['usuario'];
  $nombre     = $_SESSION['usrNombre'];
$usrUbica   = $_SESSION['usrUbica'];
if(!verifySession($_SESSION)){
  logoutTimeout();
}else{
$db_catalogos = condb_catalogos();
$db_modulos   = condb_modulos();
$db_usuarios  = condb_usuarios();
$salida = "";
$modId = 7;
if("getClientTable" == $_POST["action"]){
  if(hasPermission($modId,'f')&&hasPermission($modId,'r')){
      $query = "SELECT DISTINCT cl.cliente_id AS origen_id, 1 AS tipo_origen, cl.razon_social, cl.nombre_comercial,'Ninguno' AS sucursal, " .
        "cl.descripcion, '' AS org_id " .
        "FROM " . $cfgTableNameMod . ".clientes AS cl, " .
        $cfgTableNameMod . ".cotizaciones AS con " .
        "WHERE cl.cliente_id=con.origen_id " .
        "AND con.tipo_origen=1 " .
        "AND cl.cliente_id>0 " .
        "UNION ALL " .
        "SELECT DISTINCT cli.cliente_id AS origen_id, 0 AS tipo_origen, cli.razon_social, cli.nombre_comercial, sit.nombre AS sucursal, " .
        "sit.descripcion, sit.tipo_origen AS org_id " .
        "FROM " . $cfgTableNameMod . ".sitios AS sit, " .
        $cfgTableNameMod . ".clientes AS cli, " .
        $cfgTableNameMod . ".cotizaciones AS con " .
        "WHERE sit.sitio_id=con.origen_id " .
        "AND sit.origen_id=cli.cliente_id " .
        "AND sit.tipo_origen =con.tipo_origen " .
        "AND con.tipo_origen=0 " .
        "AND sit.sitio_id>0";
      // se cambio tabla contratos por cotizaciones
        // $query = "SELECT DISTINCT cl.cliente_id AS origen_id, 1 AS tipo_origen, cl.razon_social, cl.nombre_comercial,'Ninguno' AS sucursal, " .
        //   "cl.descripcion, '' AS org_id " .
        //   "FROM " . $cfgTableNameMod . ".clientes AS cl, " .
        //   $cfgTableNameMod . ".cotizaciones AS con " .
        //   "WHERE cl.cliente_id=con.origen_id " .
        //   "AND con.tipo_origen<=1 " .
        //   "AND cl.cliente_id>0 " .
        //   "UNION ALL " .
        //   "SELECT DISTINCT sit.sitio_id AS origen_id, 2 AS tipo_origen, cli.razon_social, cli.nombre_comercial, sit.nombre AS sucursal, " .
        //   "sit.descripcion, sit.tipo_origen AS org_id " .
        //   "FROM " . $cfgTableNameMod . ".sitios AS sit, " .
        //   $cfgTableNameMod . ".clientes AS cli, " .
        //   $cfgTableNameMod . ".cotizaciones AS con " .
        //   "WHERE sit.sitio_id=con.origen_id " .
        //   "AND sit.origen_id=cli.cliente_id " .
        //   "AND sit.tipo_origen =con.tipo_origen " .
        //   "AND con.tipo_origen=2 " .
      //   "AND sit.sitio_id>0";
  }elseif(hasPermission($modId,'c')&&hasPermission($modId,'r')){
    $query = "SELECT DISTINCT cl.cliente_id AS origen_id, 1 AS tipo_origen, cl.razon_social, cl.nombre_comercial,'Ninguno' AS sucursal, ".
                             "cl.descripcion, '' AS org_id ".
             "FROM ".$cfgTableNameMod.".clientes AS cl, ".
                    $cfgTableNameUsr.".usuarios AS usr, ".
                    $cfgTableNameMod.".contratos as con ".
             "WHERE cl.usuario_alta=usr.usuario_id ".
             "AND usr.ubicacion_id=".$_SESSION['usrUbica']." ".
             "AND cl.cliente_id=con.origen_id ".
             "AND cl.cliente_id>0 ".
             "UNION ALL ".
             "SELECT DISTINCT sit.sitio_id AS origen_id, 2 AS tipo_origen, cli.razon_social, cli.nombre_comercial, sit.nombre AS sucursal, ".
                             "sit.descripcion, sit.tipo_origen AS org_id ".
             "FROM ".$cfgTableNameMod.".sitios AS sit, ".
                     $cfgTableNameMod.".clientes AS cli, ".
                     $cfgTableNameUsr.".usuarios AS usr, ".
                     $cfgTableNameMod.".contratos AS con ".
             "WHERE sit.origen_id=con.origen_id ".
             "AND sit.origen_id=cli.cliente_id ".
             "AND cli.usuario_alta=usr.usuario_id ".
             "AND usr.ubicacion_id=".$_SESSION['usrUbica']." ".
             "AND sit.tipo_origen=1 ".
             "AND sit.sitio_id=con.sitio_instalacion ".
             "AND sit.sitio_id>0";
  }elseif(hasPermission($modId,'l')){
    $query = "SELECT * FROM clientes WHERE usuario_alta=".$usuarioId;
  }else{
    $salida = "<b>ERROR:</b> No cuenta con permisos para ver esta información";
    goto cliTablea;
  }
  $result = mysqli_query($db_modulos,$query);
  if(!$result){
    $salida = "ERROR|| No se pudo consultar de los datos de clientes en la DB";
  }else{
    if(0==mysqli_num_rows($result)){
      $salida = "OK||No existen registros disponibles";
    }else{
      $salida = "OK||<table id=\"tbl_clients\" class=\"table table-hover table-striped\">\n".
                "  <thead>\n".
                "    <tr>\n".
                "      <th class=\"hidden-xs\"></th>\n".
                "      <th class=\"hidden-xs\">Razón Social</th>\n".
                "      <th>Nombre Comercial</th>\n".
                "      <th class=\"hidden-xs\">Sucursal</th>\n".
                "      <th class=\"hidden-xs\">Descripción</th>\n".
                "      <th>Acciones</th>\n".
                "    </tr>\n".
                "  </thead>\n".
                "  <tbody>\n";
      while($row = mysqli_fetch_assoc($result)){
        $salida .= "    <tr>\n";
         if(0<=$row['tipo_origen']){
           $salida .= "      <td class=\"hidden-xs\"><span class=\"glyphicon glyphicon-list-alt\" title=\"Cliente\"></span></td>\n";
         }
         elseif(2==$row['tipo_origen']){
           $salida .= "      <td class=\"hidden-xs\"><span class=\"glyphicon glyphicon-tower\" title=\"Sitio\"></span></td>\n";
         }
        $salida .= "      <td class=\"hidden-xs\">".$row["razon_social"]."</td>\n".
                   "      <td>".$row["nombre_comercial"]."</td>\n".
                   "      <td class=\"hidden-xs\">".$row["sucursal"]."</td>\n";
                   if(30<strlen($row["descripcion"]))
                     $salida .= "      <td class=\"hidden-xs\" title=\"".$row["descripcion"]."\">".substr($row["descripcion"],0,30)."...</td>\n"; 
                   else 
                     $salida .= "      <td class=\"hidden-xs\">".$row["descripcion"]."</td>\n";             
        $salida .= "      <td>";
        if(0<=$row['tipo_origen']){
          $salida .= "        <button type=\"button\" id=\"btn_clientDetails_".$row['origen_id']."_".$row['tipo_origen']."\" title=\"Ver Detalles\" class=\"btn btn-xs btn-primary btn-responsive\" onClick=\"getClientDetails('".encrypt($row['origen_id'])."');\" ><span class=\"glyphicon glyphicon-list\"></span></button>";
          if(hasPermission(4,'r')||hasPermission(4,'w')){ //tiene permiso para leer datos de cotizaciones
            $salida .= "        <button type=\"button\" id=\"btn_clientPrices_".$row['origen_id']."_".$row['tipo_origen']."\" title=\"Ver Cotizaciones\" class=\"btn btn-xs btn-default btn-responsive\" onClick=\"getPrices(".$row['origen_id'].",".$row['tipo_origen'].");\"><span class=\"glyphicon glyphicon-usd\"></span></button>";
          } 
        }
        elseif(2==$row['tipo_origen']){
          $salida .= "        <button type=\"button\" id=\"btn_siteDetails_".$row['sitio_id']."\" title=\"Ver Detalles\" class=\"btn btn-xs btn-primary btn-responsive\" onClick=\"goToSiteDetails('".encrypt($row['origen_id'])."','".encrypt(1)."');\" disabled=\"true\";><span class=\"glyphicon glyphicon-list\"></span></button>";
        }
        if(hasPermission(7,'w')){ //tiene permiso para escribir datos de facturación
          $salida .= "        <button type=\"button\" id=\"btn_newBill_".$row['origen_id']."_".$row['tipo_origen']."\" title=\"Detalles de Facturación\" class=\"btn btn-xs btn-success btn-responsive\" onClick=\"getBillDetails(".$row['origen_id'].",".$row['tipo_origen'].");\"><span class=\"glyphicon glyphicon-folder-close\"></span></button>";
          $salida .= "        <button type=\"button\" id=\"btn_clientDocs_".$row['origen_id']."_".$row['tipo_origen']."\" title=\"Ver Documentación\" class=\"btn btn-xs btn-primary btn-responsive\" onClick=\"displayDocuments(".$row['origen_id'].",".$row['tipo_origen'].");\"><span class=\"glyphicon glyphicon-file\"></span></button>";
        }
         $salida.= "      </td>\n".
                   "    </tr>\n";      
      }
      $salida .= "  </tbody>\n".
                 "</table>\n"; 
    }
  }
  cliTablea:
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);  
  echo $salida;
}
if("getPaymentType" == $_POST["action"]){
  $id = isset($_POST['id']) ? $_POST['id'] : 0;
  $query = "SELECT * FROM cat_tipo_pago";
  $result = mysqli_query($db_catalogos, $query);
  if(!$result){
    $salida = "ERROR: No se pudo cargar lista de tipos de Pago";
  }else{
    $salida = "<select class=\"form-control\" id=\"sel_pymntMth\" name=\"sel_pymntMth\">";
    while($row = mysqli_fetch_assoc($result)){
      if($id==$row['tipo_pago_id'])
        $salida .= "<option value=".$row['tipo_pago_id']." selected>".$row['tipo_pago']."</option>"; 
      else
        $salida .= "<option value=".$row['tipo_pago_id'].">".$row['tipo_pago']."</option>";
    }
    $salida .= "</select>";
  }
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios); 
  echo $salida;
}

if("getDenomList" == $_POST["action"]){
  $id = isset($_POST['id']) ? $_POST['id'] : 0;
  $query = "SELECT * FROM cat_denominacion";
  $result = mysqli_query($db_catalogos, $query);
  if(!$result){
    $salida = "ERROR: No se pudo cargar lista de denominaciones";
  }else{
    $salida = "<select class=\"form-control\" id=\"sel_denom\" name=\"sel_denom\">";
    while($row = mysqli_fetch_assoc($result)){
      if($id==$row['denominacion_id'])
        $salida .= "<option value=".$row['denominacion_id']." selected>".$row['denominacion_abvr']."</option>"; 
      else
        $salida .= "<option value=".$row['denominacion_id'].">".$row['denominacion_abvr']."</option>";
    }
    $salida .= "</select>";
  }
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios); 
  echo $salida;
}

if("getCliStatList" == $_POST["action"]){
  $id = isset($_POST['id']) ? $_POST['id'] : 0;
  $query = "SELECT * FROM cat_cliente_estatus";
  $result = mysqli_query($db_catalogos, $query);
  if(!$result){
    $salida = "ERROR: No se pudo cargar la lista de estatus";
  }else{
    $salida = "<select class=\"form-control\" id=\"sel_cliEstat\" name=\"sel_cliEstat\">";
    while($row = mysqli_fetch_assoc($result)){
      if($id==$row['cliente_estatus_id'])
          $salida .= "<option value=".$row['cliente_estatus_id']." selected>".$row['cliente_estatus']."</option>"; 
      else
          $salida .= "<option value=".$row['cliente_estatus_id'].">".$row['cliente_estatus']."</option>"; 
    }
    $salida .= "</select>";
  }
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios); 
  echo $salida;
}

if("getCliCutDate" == $_POST["action"]){
  $id = isset($_POST['id']) ? $_POST['id'] : 0;
  $query = "SELECT * FROM cat_fechas_corte";
  $result = mysqli_query($db_catalogos, $query);
  if(!$result){
    $salida = "ERROR: No se pudo cargar la lista de fechas de corte";
  }else{
    $salida = "<select class=\"form-control\" id=\"sel_fecCut\" name=\"sel_fecCut\">";
    while($row = mysqli_fetch_assoc($result)){
      if($id==$row['fecha_corte_id'])
          $salida .= "<option value=".$row['fecha_corte_id']." selected>".$row['fecha_corte_dia']."</option>"; 
      else
          $salida .= "<option value=".$row['fecha_corte_id'].">".$row['fecha_corte_dia']."</option>"; 
    }
    $salida .= "</select>";
  }
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios); 
  echo $salida;
}

if("getBillDetails" == $_POST["action"]){
  $orgId = isset($_POST['orgId']) ? $_POST['orgId'] : 0;
  $orgType = isset($_POST['orgType']) ? $_POST['orgType'] : 0;
  $option = isset($_POST['option']) ? $_POST['option'] : -1;
  $query = "SELECT facDat.*, catFecCot.fecha_corte_dia, catTipPag.tipo_pago, catDem.denominacion_abvr, catCteSta.cliente_estatus ".
           "FROM ".$cfgTableNameMod.".facturacion_datos AS facDat, ".
                   $cfgTableNameCat.".cat_fechas_corte AS catFecCot, ".
                   $cfgTableNameCat.".cat_tipo_pago AS catTipPag, ".
                   $cfgTableNameCat.".cat_denominacion AS catDem, ".
                   $cfgTableNameCat.".cat_cliente_estatus AS catCteSta ".
           "WHERE origen_id =".$orgId." AND tipo_origen=".$orgType." AND catFecCot.fecha_corte_id=facDat.fecha_corte_id";        
           
  $result = mysqli_query($db_modulos,$query);
  if(!$result){
    $salida = "ERROR: No se pudo cargar los datos de facturación ";
  }else{
    if(0==mysqli_num_rows($result)){
      if(hasPermission($modId,'w')&&(hasPermission($modId,'r')||hasPermission($modId,'l'))){   
        $salida = "<div class=\"container\">\n".
                  "  <div class=\"row\">\n".
                  "    <div><button type=\"button\" id=\"btn_newBillDetails\" title=\"Agregar Datos\" class=\"btn btn-default\" onClick=\"newBillDetails();\"><span class=\"glyphicon glyphicon-asterisk\"></span> Agregar Datos de Facturación</button></div>\n".
                  "  </div>\n".
                  "  <br><br>\n".
                  "</div>\n\n";
      }    
      $salida .= "No existen datos. Haga click en <b>Agregar Datos de Facturación</b> para añadirlos a este resgistro";    
    }else{
      $row = mysqli_fetch_assoc($result);
      if(0==$option){
        $salida = "<div class=\"container\">\n".
                  "  <div class=\"row\">\n".
                  "    <div><button type=\"button\" id=\"btn_editBillDetails\" title=\"Editar Datos\" class=\"btn btn-success\" onClick=\"editBillDetails(".$orgId.",".$orgType.")\"><span class=\"glyphicon glyphicon-edit\"></span> Editar</button></div>\n".
                  "  </div>\n".
                  "  <br><br>\n".
                  "</div>\n";
        
        $salida .= "<table id=\"tbl_billDetails\" class=\"table table-striped\">\n".
                   "  <thead>\n".
                   "    <tr>\n".
                   "      <th>Campo</th>\n".
                   "      <th>Valor</th>\n".
                   "    </tr>\n".
                   "  </thead>\n".
                   "  <tbody>\n";
         
        $salida .= "    <tr>\n".
                   "      <td><b>Codigo</b></td>\n".
                   "      <td>".$row["codigo"]."</td>\n".
                   "    </tr>\n". 
                   "    <tr>\n".
                   "      <td><b>Fecha de Corte</b></td>\n".
                   "      <td> Los dias ".$row["fecha_corte_dia"]." de cada mes</td>\n".
                   "    </tr>\n". 
                   "    <tr>\n".
                   "      <td><b>Fecha de Vencimiento</b></td>\n".
                   "      <td>".$row["vencimiento"]."</td>\n".
                   "    </tr>\n".
                   "    <tr>\n".
                   "      <td><b>Método de Pago</b></td>\n".
                   "      <td>".$row["tipo_pago"]."</td>\n".
                   "    </tr>\n".
                   "    <tr>\n".
                   "      <td><b>Num Cta. Pago</b></td>\n".
                   "      <td>".$row["num_cta_pago"]."</td>\n".
                   "    </tr>\n".   
                   "    <tr>\n".
                   "      <td><b>Observaciones</b></td>\n".
                   "      <td>".$row["observaciones"]."</td>\n".
                   "    </tr>\n".
                   "    <tr>\n".
                   "      <td><b>Estatus</b></td>\n".
                   "      <td>".$row["cliente_estatus"]."</td>\n".
                   "    </tr>\n". 
                   "    <tr>\n".
                   "      <td><b>Tel. Notificacion</b></td>\n".
                   "      <td>".$row["tel_notificacion"]."</td>\n".
                   "    </tr>\n". 
                   "    <tr>\n".
                   "      <td><b>Fecha de Alta</b></td>\n".
                   "      <td>".$row["fecha_alta"]."</td>\n".
                   "    </tr>\n";     

        $salida .= "  </tbody>\n".
                     "</table>\n<hr>";
      }
      else{
        $salida = "OK||".$row["codigo"]."||".$row["fecha_corte_id"]."||".$row["vencimiento"]."||".$row["tipo_pago"]."||".$row['denominacion']."||".$row["num_cta_pago"]."||".$row["observaciones"]."||".$row["cliente_estatus"]."||".$row["tel_notificacion"]."||".$row['facturacion_id'];
      } 
    }
  }
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios); 
  echo $salida;
}

if("saveBillingNew"==$_POST['action']){
  $orgId = isset($_POST['orgId']) ? $_POST['orgId'] : 0;
  $orgType = isset($_POST['orgType']) ? $_POST['orgType'] : 0;
  $option = isset($_POST['saveAction']) ? $_POST['saveAction'] : -1;
  $facDatId = isset($_POST['facDatId']) ? $_POST['facDatId'] : -1;
  
  $campos = array();
  
  $campos['code'] = isset($_POST['code']) ? $_POST['code'] : "";
  $campos['fecCut'] = isset($_POST['fecCut']) ? $_POST['fecCut'] : -1;
  $campos['payTyp'] = isset($_POST['payType']) ? $_POST['payType'] : -1;
  $campos['denom'] = isset($_POST['denom']) ? $_POST['denom'] : -1;
  $campos['numCta'] = isset($_POST['numCta']) ? $_POST['numCta'] : "";
  $campos['cliEstat'] = isset($_POST['cliEstat']) ? $_POST['cliEstat'] : -1;
  $campos['telNot'] = isset($_POST['telNot']) ? $_POST['telNot'] : -1;
  $campos['obs'] = isset($_POST['obs']) ? $_POST['obs'] : "";
  
  $fecAlta = date('Y-m-d H:i:s');
  $fecVen = $campos['fecCut']+5;
  
  if(1==$orgType){
    $orgName = "Cliente";
  }
  elseif(2==$orgType){
    $orgName = "Sitio";
  }
  
  if(0==$option){
    $query = "INSERT INTO facturacion_datos (origen_id,tipo_origen,codigo,fecha_corte_id,vencimiento,".
                                            "denominacion,metodo_pago_id,num_cta_pago,observaciones,estatus,tel_notificacion,fecha_alta) ".
             "VALUES (".$orgId.",".$orgType.",'".$campos['code']."',".$campos['fecCut'].",".$fecVen.",".$campos['denom'].",".
                        $campos['payTyp'].",'".$campos['numCta']."','".$campos['obs']."',".$campos['cliEstat'].",'".$campos['telNot']."','".$fecAlta."')";
  }
  elseif(1==$option){
    $query2 = "SELECT * FROM facturacion_datos WHERE facturacion_id=".$facDatId;
    
    $result2 = mysqli_query($db_modulos,$query2);
    
    if(!$result2){
      $salida = "ERROR||Ocurrió un problema al obtenerse los datos actuales del registro a editar";
    }
    else{
      $row2 = mysqli_fetch_assoc($result2);
      $diff = array_diff_assoc($row2,$campos);
    }
  }
  else{
    $salida = "ERROR||No se recibió el parametro de opción de guardado";
    goto sabilla;
  }
  
  $query = "UPDATE facturacion_datos SET ".
           "codigo='".$campos['code']."', fecha_corte_id=".$campos['fecCut'].", vencimiento=".$fecVen.", ".
           "denominacion=".$campos['denom'].", metodo_pago_id=".$campos['payTyp'].", num_cta_pago='".$campos['numCta']."', ".
           "observaciones='".$campos['obs']."', tel_notificacion='".$campos['telNot']."', estatus=".$campos['cliEstat'].
           " WHERE facturacion_id=".$facDatId;
 
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $salida = "ERROR||Ocurrió un problema al guardarse los datos de facturación. ".$query;
  }
  else{
    writeOnJournal($usuarioId,"Ha guardado los datos de facturación para el ".$orgName." con id [".$orgId."]");
    $salida = "OK||Se han guardado los datos de facturación con éxito";
    
    if(0==$option){
      foreach($diff as $key => $value){
        writeOnJournal($usuarioId,"Ha modificado el campo \"".$key."\" de [".$value."] a [".$campos[$key]."]"." para cliente id [".$cliId."]");
      }
    }
  }
  sabilla:
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios); 
  echo $salida; 
}

}//fin else sesión 
?>
