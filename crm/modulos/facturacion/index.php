<?php 
session_start();
set_time_limit(0);
include_once "../../libs/db/common.php";
include_once "../../libs/db/encrypt.php";
if(!verifySession($_SESSION)){
 logoutTimeout();
}else{
  header('Content-Type: text/html; charset=UTF-8');
  date_default_timezone_set('America/Mexico_City');
  $title = "Facturación";
  $usuario = $_SESSION['usuario'];
  $gruposArr  = $_SESSION['grupos'];
  $prosId = isset($_GET['pId']) ? decrypt($_GET['pId']) : -1;
  $modId = 7;
?>
<!DOCTYPE html>
<html>
  <head>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/AdminLTE2/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
    <link href="../../libs/js/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css"/>  
    <link href="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.css" rel="stylesheet" type="text/css"/>      
    <link href="../../libs/bootstrap/font-awesome-4.3.0/css/font-awesome.css" rel="stylesheet" type="text/css"/>
    <script src="../../libs/AdminLTE2/plugins/jQuery/jQuery-2.1.3.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/bootstrap/js/bootstrap.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/dist/js/app.js" type="text/javascript"></script>   
    <script src="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>   
    <script src="../../libs/js/jqueryValidate/dist/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.form.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.uploadPreview.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <script src="../../libs/js/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
    <script src="../../libs/bootstrap/summernote-0.6.6-dist/summernote.js" type="text/javascript"></script>
    <script src="../../libs/bootstrap/summernote-0.6.6-dist/lang/summernote-es-ES.js" type="text/javascript"></script>
    <script src="../../libs/js/common.js" type="text/javascript"></script>
    <script src="script.js" type="text/javascript"></script>
    <link rel="icon" type="image/png" href="../../img/favicon-16x16.png" sizes="16x16">
    <title>Coeficiente CRM - Facturación</title>
    <style type="text/css"> 
    </style>
  </head>
  <body>
    <!--Div que contiene el encabezado-->
    <div class="container" id="header"><?php include("../../libs/templates/header.php")?></div>
    <div class="container-fluid">
      <h2 class="page-header" id="tituloPag">
        <span class="glyphicon glyphicon-folder-close"></span> <?php echo $title ?>
      </h2>
    </div><!-- /container -->
    <div class="container">
      <div class="row">
        <div id=div_msgAlert></div>
      </div>
    </div> <!-- /container -->
    <div class="container">
      <div class="row">
        <div id="div_clients"></div>
        <input type="hidden" id="hdn_orgType" name="hdn_orgType" class="form-control" value=1>
        <input type="hidden" id="hdn_orgId" name="hdn_orgId" class="form-control" value=-1>
        <input type="hidden" id="hdn_isProspect" name="hdn_isProspect" class="form-control" value=<?php echo $prosId?>> 
      </div>
    </div> <!-- /container -->
    <div class="container-fluid" id="footer"><?php include("../../libs/templates/footer.php")?></div>
    <div class="container" id="clientDialog"></div><!--Cuadro de dialogo de detalles de cliente--> 
    <!--_Cuadro de dialogo de detalles de facturación -->
    <div id="mod_billDetails" class="modal fade" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Datos de Facturación</h4>
          </div>
          <div class="container-fluid">
            <div id=div_msgAlertBill></div>
          </div>
          <div class="modal-body row">  
            <div class="col container-fluid" id="div_billDetails"></div> 
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->   
    <!--Formulario de inserción/edición de datos de facturación-->
    <div id="mod_billInsert" class="modal fade" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Detalles de Facturación</h4>
          </div>
          <div class="container-fluid">
            <div id=div_msgAlertBillNew></div>
          </div>
          <div class="modal-body"> 
            <form class="form-horizontal" id="frm_newBillDat" name="frm_newBillDat" role="form" method="post" onsubmit="return false;" enctype="multipart/form-data">
            <div>
              <label>Codigo:
                <input type="text" id="txt_code" name="txt_code" class="form-control" placeholder="" maxlength="16" size="30" required>
              </label>    
            </div>
            <div>
              <label id="txt_code_error" class="div-errorVal"/>
            </div> 
            <div>
              <label>Fecha de Corte (dia de cada mes):
                <span id="spn_cutDate"></span>
              </label>
            </div>
            <div>
              <label id="spn_cutDate_error" class="div-errorVal"/>
            </div>
            <div>
              <div>
                <label>Metodo de Pago:
                  <span id="spn_pymntMth"></span>
                </label> 
                <label>Denominacion:
                  <span id="spn_denom"></span>
                </label>
              </div>
            </div>
            <div>
              <label id="txt_pymntMth_error" class="div-errorVal"/>
              <label id="txt_denom_error" class="div-errorVal"></label>     
            </div>
            <div>
              <label>Numero de Cuenta:
                <input type="text" id="txt_numCta" name="txt_numCta" class="form-control" placeholder="" maxlength="20" size="30" required>
              </label>
            </div>
            <div>
              <label id="txt_numCta_error" class="div-errorVal"/>
            </div>
            <div>
              <label>Numero de Teléfono de Notificación:
                <input type="text" id="txt_telNot" name="txt_telNot" class="form-control" placeholder="" maxlength="10" size="10" required number>
              </label>
            </div>
            <div>
              <label id="txt_telNot_error" class="div-errorVal"/>
            </div>
            <div>
              <label>Estatus:
                <span id="spn_estat"></span>
              </label>
            </div>
            <div>
              <label id="sel_estat_error" class="div-errorVal"/>
            </div>
            <div>              
              <label>Observaciones:
                <textarea id="txt_obs" name="txt_obs" class="form-control" placeholder="Agregue una breve observacion." maxlength="255" cols="40" rows="5" required></textarea>
              </label>
              <div>
                <label id="txt_obs_error" class="div-errorVal"/>
              </div>
            </div> 
            <input type="hidden" id="hdn_saveBillAction" name="hdn_saveBillAction" class="form-control" value=-1>
            <input type="hidden" id="hdn_facDatId" name="hdn_facDatId" class="form-control" value=-1>
          </div>
          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <input type="submit" class="btn btn-primary" id="guardaFact" value="Guardar Cambios" onClick="saveBillingNew();">
          </div>
          </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->    
    <div class="container" id="pricesDialog"></div><!--cuadro de dialogo para gestión de cotización--> 
    <div class="container" id="documentDialog"></div><!--cuadro de dialogo para gestión de documentos--> 
    <div class="container" id="siteDialog"></div><!--cuadro de dialogo para gestión de sitios--> 
  </body>
</html>
<?php
}
?>
