$(document).ready(function(){ 
  $("#documentDialog").load("../../libs/templates/documentdialog.php");
  $("#eventDialog").load("../../libs/templates/eventdialog.php");
  $("#pricesDialog").load("../../libs/templates/pricesdialog.php");
  $("#siteDialog").load("../../libs/templates/sitedialog.php");
  $("#clientDialog").load("../../libs/templates/clientdialog.php");
  
  updateIndexActive("li_navBar_facturacion");
   
  getClientRegs(); 
});

function getClientRegs(){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getClientTable"},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo lista de clientes");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#div_clients").html(resp[1]);  
        $("#tbl_clients").DataTable({
          language: datatableespaniol,
        });
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);
      }   
    }  
  });
}

function getDenomList(id){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getDenomList", "id": id},
    success: function(msg){
      $("#spn_denom").html(msg);
    }
  });
}

function getPaymentType(id){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getPaymentType", "id": id},
    success: function(msg){
      $("#spn_pymntMth").html(msg);
    }
  });
}

function getCliStatList(id){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getCliStatList", "id": id},
    success: function(msg){
      $("#spn_estat").html(msg);
    }
  });
}

function getCliCutDate(id){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getCliCutDate", "id": id},
    success: function(msg){
      $("#spn_cutDate").html(msg);
    }
  });
}

function newBillDetails(){
  $("#div_msgAlertBillNew").html("");
  $("#txt_numCta").val("");
  $("#txt_code").val("");
  $("#txt_obs").val("");
  getDenomList(0);
  getPaymentType(0);
  getCliStatList(0);
  getCliCutDate(0);
  $("#hdn_saveBillAction").val(0);
  $("#mod_billInsert").modal("show");
}

function getBillDetails(orgId,orgType){
  $("#hdn_orgType").val(orgType);
  $("#hdn_orgId").val(orgId);
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getBillDetails", "orgId":orgId, "orgType":orgType, "option":0},
    success: function(msg){
      $("#div_billDetails").html(msg);
      $("#mod_billDetails").modal("show");
    }
  });
}

function editBillDetails(orgId,orgType){
  $("#div_msgAlertBillNew").html("");
  $("#hdn_orgType").val(orgType);
  $("#hdn_orgId").val(orgId);
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getBillDetails", "orgId":orgId, "orgType":orgType, "option":1},
    success: function(msg){
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        newBillDetails();
        $("#txt_code").val(resp[1]);
        $("#txt_fecVen").val(resp[3]);
        $("#txt_numCta").val(resp[6]);
        $("#txt_obs").val(resp[7]);
        $("#txt_telNot").val(resp[9]);
        
        getDenomList(resp[5]);
        getPaymentType(resp[4]);
        getCliStatList(resp[8]);
        getCliCutDate(resp[2]);
        $("#hdn_facDatId").val(resp[10]);
        $("#hdn_saveBillAction").val(1);
      }
      else{
        printErrorMsg("div_msgAlertBillNew", resp[1]);
      }
      $("#mod_billInsert").modal("show");
    }
  });
}

function saveBillingNew(){
  if(false==validateBillData()){
    printErrorMsg("div_msgAlertBillNew","Existen datos no válidos, favor de corregirlos");
  }
  else{
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: {"action": "saveBillingNew", "orgId":$("#hdn_orgId").val(), "orgType":$("#hdn_orgType").val(),
             "code":$("#txt_code").val(),"numCta":$("#txt_numCta").val(),"fecCut":$("#sel_fecCut").val(),
             "obs":$("#txt_obs").val(),"telNot":$("#txt_telNot").val(),"saveAction":$("#hdn_saveBillAction").val(),
             "denom":$("#sel_denom").val(),"payType":$("#sel_pymntMth").val(),"cliEstat":$("#sel_cliEstat").val(),
             "facDatId":$("#hdn_facDatId").val()},
      
      beforeSend: function(){
        setLoadDialog(0,"Guardando datos de facturación");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){
        var resp = msg.trim().split("||");
        if("OK" == resp[0]){ 
          getBillDetails($("#hdn_orgId").val(),$("#hdn_orgType").val());
          $("#mod_billInsert").modal("hide");
          $("#hdn_orgType").val(-1);
          $("#hdn_orgId").val(-1);
          printSuccessMsg("div_msgAlertBill",resp[1]);
        }
        else{
          printErrorMsg("div_msgAlertBillNew",resp[1]);
        }
      }
    });
    return false;
  }
}

//valida la integridad de los campos de datos de facturación
function validateBillData(){
  addValidationRules(7);
  
  return $("#frm_newBillDat").validate({
    rules: {
      txt_code:
      {
        required: true
      },
      txt_numCta:
      {
        required: true
      },
      txt_obs:
      {
        required: true
      },
      txt_telNot:
      {
        required: true,
        isPhoneNumber: true
      }
    },
    errorPlacement: function(error, element) {
       	error.appendTo($('#' + element.attr("id") + '_error'));
    }
  }).form();
}
