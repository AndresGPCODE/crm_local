$(document).ready(function(){ 
  updateIndexActive("li_navBar_main");
});

//codigo nuevo
var globar_usuario;

function sendSugerencia(idusuario){
  globar_usuario = idusuario;
  var txtboxvalue = document.getElementById('txt_sugerencia').value.trim();
  var txtbox = document.getElementById('txt_sugerencia');
  // var opr = confirm("Deseas enviar tu sugerencia?");
  if(true){
    if(txtboxvalue){
      txtbox.value = "";
      $.ajax({
        url: 'xml.php',
        data: {
          mensaje: txtboxvalue,
          crud: 'C'
        },
        type: 'POST',
        success: function (json) {
          //se ejecuta cuando se completa la peticion de manera exitosa
          var data = JSON.parse(json);
          if(data[0].status){
            // data.shift();//elimina el primer elemeneto del arreglo
            alert(data[0].msg);
            if (globar_usuario == 72){
              checkSugerencia(globar_usuario);
            }
          }else{
            //si no hubo respuesta correcta de la peticion
            alert('Error: '+data[0].msg);
          }
        },
        error: function (xhr, status) {
          //se ejecuta si hubo algun error en la peticion
          alert('Disculpe, existió un problema');
        },
        complete: function (xhr, status) {
          //se ejecita cuando la peticion se realizo con exito o con error, esta funcione se ejecuta obligatoriamente
        }
      });
    }else{
      alert("Campo vacio, por favor ingresa tu sugerencia antes de enviarla, Gracias!!");
    }
  }
}

function checkSugerencia(idusuario){
  globar_usuario = idusuario;
  $.ajax({
    url: 'xml.php',
    data: {
      usuario: idusuario
    },
    type: 'GET',
    success: function (json) {
      //se ejecuta cuando se completa la peticion de manera exitosa
      var data = JSON.parse(json);
      if (data[0].status){
        $("#listsugerencias").addClass("sugerencia");
        $("#listsugerencias").removeClass("failsugerencia");
        
        //******************************************************/
        data.shift();
        var list = "";
        for(i=0;i<data.length;i++){
          list += "<div>" + data[i].sugerencia + "</div>"+
            "<button type='button' title='Eliminar Sugerencia' class='btn btn-xs btn-danger btn-responsive deletesug' onclick='deleteSug(" + data[i].id_sug+")'><span class='glyphicon glyphicon-remove'></span></button>";
        }
        $("#listsugerencias").html(list);
        console.log(list);
      }else{
        $("#listsugerencias").addClass("failsugerencia");
        $("#listsugerencias").removeClass("sugerencia");
        $("#listsugerencias").html("<div>" + data[0].msg + "</div>");
      }
      
    },
    error: function (xhr, status) {
      //se ejecuta si hubo algun error en la peticion
      alert('Disculpe, Ha ocurrido un problema un problema');
    },
    complete: function (xhr, status) {
      //se ejecita cuando la peticion se realizo con exito o con error, esta funcione se ejecuta obligatoriamente
    }
  });
}

function deleteSug(id){
  // var del = confirm("ESTAS SERGURO QUE DESEAS ELIMINAR ESTA SUGERENCIA?");
  if(true){
    $.ajax({
      url: "xml.php",
      data: {
        id: id,
        crud: "D"
      },
      type: "POST",
      success: function(json) {
        //se ejecuta cuando se completa la peticion de manera exitosa
        var data = JSON.parse(json);
        if (data[0].status) {
          // data.shift();//elimina el primer elemeneto del arreglo
          alert(data[0].msg);
          checkSugerencia(globar_usuario);
        } else {
          //si no hubo respuesta correcta de la peticion
          alert("Error: " + data[0].msg);
        }
      },
      error: function(xhr, status) {
        //se ejecuta si hubo algun error en la peticion
        alert("Disculpe, existió un problema");
      },
      complete: function(xhr, status) {
        //se ejecita cuando la peticion se realizo con exito o con error, esta funcione se ejecuta obligatoriamente
      }
    });
  }
}


function avisos(opt, id){
  if (opt == 1) {
    // $.ajax({
    //   url: "xml.php",
    //   data: {
    //     id: id,
    //     crud: "D"
    //   },
    //   type: "POST",
    //   success: function (json) {
    //     //se ejecuta cuando se completa la peticion de manera exitosa
    //     var data = JSON.parse(json);
    //     if (data[0].status) {
    //       // data.shift();//elimina el primer elemeneto del arreglo
    //       alert(data[0].msg);
    //       checkSugerencia(globar_usuario);
    //     } else {
    //       //si no hubo respuesta correcta de la peticion
    //       alert("Error: " + data[0].msg);
    //     }
    //   },
    //   error: function (xhr, status) {
    //     //se ejecuta si hubo algun error en la peticion
    //     alert("Disculpe, existió un problema");
    //   },
    //   complete: function (xhr, status) {
    //     //se ejecita cuando la peticion se realizo con exito o con error, esta funcione se ejecuta obligatoriamente
    //   }
    // });
  } else if(opt == 0){
    $.ajax({
      url: "ajax.php",
      data: {
        id: id,
        opt: opt,
        action: 'avisos'
      },
      type: "POST",
      success: function (json) {
        //se ejecuta cuando se completa la peticion de manera exitosa
        var data = JSON.parse(json);
        alert(data['msg'], data['rsp']);
      },
      error: function (xhr, status) {
        //se ejecuta si hubo algun error en la peticion
        // alert("Disculpe, existió un problema");
      },
      complete: function (xhr, status) {
        //se ejecita cuando la peticion se realizo con exito o con error, esta funcione se ejecuta obligatoriamente
      }
    });
  }else{
  alert("Error, No se puede realizar la operacion");
  }
}

function getAvisos(){
  $.ajax({
    url: "ajax.php",
    data: {
      action: 'getAvisos'
    },
    type: "POST",
    success: function (json) {
      //se ejecuta cuando se completa la peticion de manera exitosa
      var data = JSON.parse(json);
      alert(data['msg'], data['rsp']);
    },
    error: function (xhr, status) {
      //se ejecuta si hubo algun error en la peticion
      // alert("Disculpe, existió un problema");
    },
    complete: function (xhr, status) {
      //se ejecita cuando la peticion se realizo con exito o con error, esta funcione se ejecuta obligatoriamente
    }
  });
}

