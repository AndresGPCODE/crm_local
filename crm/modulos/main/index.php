<?php
session_start();
set_time_limit(0);
include_once "../../libs/db/common.php";

if (!verifySession($_SESSION)) {
  logoutTimeout();
} else {
  header('Content-Type: text/html; charset=UTF-8');
  date_default_timezone_set('America/Mexico_City');
  $title = "Principal";
  $gruposArr  = $_SESSION['grupos'];
  ?>
  <!DOCTYPE html>
  <html>

  <head>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../libs/AdminLTE2/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/bootstrap/font-awesome-4.3.0/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../../libs/AdminLTE2/plugins/fullcalendar-2.3.0/fullcalendar.css" rel="stylesheet" type="text/css" />

    <script src="../../libs/AdminLTE2/plugins/jQuery/jQuery-2.1.3.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../libs/js/jqueryValidate/dist/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/fullcalendar-2.3.0/lib/moment.min.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/fullcalendar-2.3.0/fullcalendar.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/fullcalendar-2.3.0/lang/es.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/dist/js/app.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../libs/AdminLTE2/plugins/chartjs/Chart.min.js" type="text/javascript"></script>
    <script src="../../libs/js/common.js" type="text/javascript"></script>
    <script src="script.js" type="text/javascript"></script>
    <link rel="icon" type="image/png" href="../../img/favicon-16x16.png" sizes="16x16">
    <style type="text/css">
      #li_calendar_div {
        width: 400px;
        margin: 0 auto;
        font-size: 10px;
        overflow: hidden;
      }

      .fc-header-title h2 {
        font-size: .9em;
        white-space: normal !important;
      }

      .fc-view-month .fc-event,
      .fc-view-agendaWeek .fc-event {
        font-size: 0;
        overflow: hidden;
        height: 2px;
      }

      .fc-view-agendaWeek .fc-event-vert {
        font-size: 0;
        overflow: hidden;
        width: 2px !important;
      }

      .fc-agenda-axis {
        width: 20px !important;
        font-size: .7em;
      }

      .fc-button-content {
        padding: 0;
      }

      .hiddenEvent {
        display: none;
      }

      .fc-other-month .fc-day-number {
        display: none;
      }

      td.fc-other-month .fc-day-number {
        visibility: hidden;
      }

      .notification-item-unread {
        padding: 5px;
      }

      .notification-item {
        padding: 5px;
      }

      .panelCoe2 {
        border: 1px solid #d1d2d7;
        border-radius: 10px;
        padding-left: 30px;
        padding-right: 30px;
        padding-bottom: 15px;
        word-wrap: break-word;
        background: -webkit-linear-gradient(#ffffff, #eee);
        background: -o-linear-gradient(#ffffff, #eee);
        background: -moz-linear-gradient(#ffffff, #eee);
        background: linear-gradient(#ffffff, #eee);
      }

      .listsugerencias {
        margin-top: 10px;
        height: 303px;
        /* descomentar esta propiedad para permitir el desbordamiento de la lista de las sugerencias */
        overflow-y: auto;
      }

      /* estilos para las sugerencias */
      .listsugerencias>div {
        /* border-right: 1px solid #b3b3b3; */
        /* border-bottom: 1px solid #b3b3b3; */
        padding-left: 10px;
        border-radius: 4px;
        float: left;
        width: 90%;
        height: 25px;
        margin-bottom: 10px;
        text-overflow: ellipsis;
        overflow: hidden;
        white-space: nowrap;
      }

      .listsugerencias>div:hover {
        background-color: #c3c3c3;
      }

      /*fin estilos para las sugerencias */
      #btn_check_sugerencias {
        color: #fff;
        background-color: #449d44;
        border-color: #398439;
      }

      .failsugerencia>div {
        background-color: #e06358;
        color: #ffffff;
      }

      .sugerencia>div {
        background-color: #dbdbdb;
        color: #000000;
      }

      .deletesug {
        float: left;
        margin-bottom: 10px;
        margin-left: 10px;
      }

      /********************************************/
      /* The Modal (background) */
      .modalsuge {
        display: none;
        /* Hidden by default */
        position: fixed;
        /* Stay in place */
        z-index: 1;
        /* Sit on top */
        left: 0;
        top: 0;
        width: 100%;
        /* Full width */
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: rgb(0, 0, 0);
        /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4);
        /* Black w/ opacity */
      }

      /* Modal Content/Box */
      .modalsuge-content {
        background-color: #fefefe;
        margin: 15% auto;
        /* 15% from the top and centered */
        padding: 20px;
        border: 1px solid #888;
        width: 80%;
        /* Could be more or less, depending on screen size */
      }

      /* The Close Button */
      .close {
        color: #aaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
      }

      .close:hover,
      .close:focus {
        color: black;
        text-decoration: none;
        cursor: pointer;
      }

      #myBtn {
        /* float:right; */
        width: 140px;
        padding: 5px;
        border-radius: 5px;
        margin-left: 85%;
      }

      #btn_aviso {
        float: right;
        margin-bottom: -15px;
      }
    </style>

    <title>Coeficiente CRM</title>
  </head>

  <body>
    <div class="container" id="header"><?php include("../../libs/templates/header.php") ?></div>

    <div class="container-fluid">
      <h2 class="page-header" id="tituloPag">
        <span class="glyphicon glyphicon-home"></span> <?php echo $title;  ?>
      </h2>
    </div>
    <div class="container">
      <div class="row">
        <div id=div_msgAlert></div>
      </div>
    </div> <!-- /container -->

    <!--<div class="jumbotron">
      <div class="container">
        <h2>Bienvenido:</h2>
        <p>COECRM</p>
      </div>
    </div> -->
    <div class="row row-eq-height">
      <div class="col-lg-8 col-md-8 col-xs-12">
        <div class="row row-eq-height">
          <div class="col-md-8 col-xs-12 col-lg-8 panelCoe1" id="div_avisosgenerales">
            <div class="container-fluid">
              <h3 class="page-header">
                <span class="glyphicon glyphicon-newspaper"></span> Avisos generales</h3>
            </div>
            <div>
              <ul>
                <li>Se les informa que el próximo Lunes 19 de Junio tendremos una junta general en las oficinas de Agustín Yañez a las 11:00 am</li><br>
                <li>Viernes 16 de Junio es último día para firma de nóminas. Favor de ir con Rocío.</li><br>
                <li>Se les invita a participar en la rifa de una cafetera que se llevará a cabo el día 07 de Julio. Costo del boleto $50. Con Isabel Salazar.</li>
                <ul>
            </div>
            <?php if ($_SESSION['usrId'] == 72) { ?>
              <button class="btn btn-secundary" id="btn_aviso" onClick="avisos(0)">Nuevo aviso</button>
            <?php } ?>
          </div>

          <!-- fin div_avisosgenerales  -->
          <div class="col-md-4 col-xs-12 col-lg-4 panelCoe1" id="div_cumpleaños">
            <div class="container-fluid">
              <h3 class="page-header">
                <i class="fa fa-birthday-cake"></i> Cumpleaños</h3>
              <b>En Coeficiente felicitamos a quienes cumplen años este mes:</b> <br><br>
            </div>
            <div>
              <i class="fa fa-star" aria-hidden="true"></i><b> Tania Sánchez</b> <i>11 de Junio</i>
            </div>
          </div>
        </div>
        <div class="row row-eq-height">
          <div class="col-md-6 col-xs-12 col-lg-6 panelCoe1" id="div_avisoDepto">
            <div class="container-fluid">
              <h3 class="page-header">
                <i class="fa fa-flag-o"></i> Aviso por Departamento</h3>
            </div>
            <div>
              <ul>
                <li>Dejaron olvidada una cartera en el baño de la planta baja. Favor de recogerla con Gerardo Guerrero</li><br>
                <li>Les recordamos que por disposición de vialidad, los automóviles no deben obstruir el paso a los peatones</li>
                <ul>
            </div>
          </div>
          <div class="col-md-6 col-xs-12 col-lg-6 panelCoe1" id="div_diasAsueto">
            <div class="container-fluid">
              <h3 class="page-header">
                <i class="fa fa-beer"></i> Próximos Dias Festivos y Vacaciones</h3>
            </div>
            <div>
              <ul>
                <li><b>Sábado 16 Septiembre</b> <i>Día de la Independencia de México</i></li>
                <li><b>Lunes 20 Noviembre</b> <i>Día de la Revolución Mexicana</i></li>
                <li><b>Lunes 25 Diciembre</b> <i>Día de Navidad</i></li>
                <ul>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-4 col-md-4 col-xs-12 panelCoe2">
        <div class="container-fluid">
          <h3 class="page-header">
            <span class="glyphicon glyphicon-send"></span> Sugerencias</h3>
        </div>
        <!--<div class="row">
          <div class="div_filters col-lg-12 col-md-12 col-xs-12 nopadding">
            <div class="col-lg-1 col-md-1 col-xs-1" align="left">
              <img width="20" src="<?php echo $picUrl; ?>"></img>
            </div>
            <div class="col-lg-7 col-md-7 col-xs-7">
              <b><?php echo $nombre; ?>:&nbsp;</b>hello
            </div>
            <div class="col-lg-4 col-md-4 col-xs-4">
              <span align="right"><i>Hace 15 minutos</i></span>
            </div>
            <br>
          </div>
          
          <div class="div_filters col-lg-12 col-md-12 col-xs-12 nopadding">
            <div class="col-lg-1 col-md-1 col-xs-1" align="left">
              <img width="20" src="<?php echo $picUrl; ?>"></img>
            </div>
            <div class="col-lg-7 col-md-7 col-xs-7">
              <b><?php echo $nombre; ?>:&nbsp;</b>Esto es un mensaje de prueba de lo que será el shoutbox, una especie de chat interno para su uso en el crm
            </div>
            <div class="col-lg-4 col-md-4 col-xs-4">
              <span align="right"><i>Hace 10 minutos</i></span>
            </div>
            <br>
          </div>
          <div class="div_filters col-lg-12 col-md-12 col-xs-12 nopadding">
            <div class="col-lg-1 col-md-1 col-xs-1" align="left">
              <img width="20" src="<?php echo $picUrl; ?>"></img>
            </div>
            <div class="col-lg-7 col-md-7 col-xs-7">
              <b><?php echo $nombre; ?>:&nbsp;</b>Otro mensaje de prueba.
            </div>
            <div class="col-lg-4 col-md-4 col-xs-4">
              <span align="right"><i>Hace 3 minutos</i></span>
            </div>
            <br>
          </div>
        </div>-->
        <!-- // |
             // |
             // |
             // |
             // |
             // |
             // |
             // ▽
             // -->
        <div class="row" align="bottom">
          <span>Agregue su Sugerencia<span>
              <textarea id="txt_sugerencia" name="txt_sugerencia" class="form-control" placeholder="Mensaje (max 1024 caracteres)" maxlength="1024" cols="40" rows="3" required></textarea><br>
              <button class="btn btn-primary" id="btn_mandarSugerencia" onClick="sendSugerencia(<?php echo $_SESSION['usrId'] ?>)">Enviar</button>
              <?php if ($_SESSION['usrId'] == 72) { ?>
                <button class="btn btn-secundary" id="btn_check_sugerencias" onClick="checkSugerencia(<?php echo $_SESSION['usrId'] ?>)">Ver sugerencias</button>
                <div class="listsugerencias form-control" id="listsugerencias"></div>
              <?php } ?>
        </div>
      </div>
    </div>
    <hr>
    <div class="container-fluid" id="footer"><?php include("../../libs/templates/footer.php") ?></div>
    <!-- /*********************************************** -->

    <!-- Trigger/Open The Modal -->
    <!-- <button id="myBtn">Open Modal</button> -->

    <!-- The Modal -->
    <!-- <div id="myModal" class="modalsuge"> -->

    <!-- Modal content -->
    <!-- <div class="modalsuge-content">
        <span class="close" onClick="close()">&times;</span>
        <p>Some text in the Modal..</p>
      </div> -->

    <!-- </div> -->

  </body>

  </html>

<?php
}//fin else sesión
