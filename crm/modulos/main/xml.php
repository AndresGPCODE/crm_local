<?php
session_start();
set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

include_once "../../libs/db/common.php";
// include_once "../../libs/db/dbcommon.php";


//prubas compu andres
  //valores para pruebas locales en las bases de datos de pruebas (comentar estos antes de subir los archivos al server)
  $cfgTableNameMod = 'coecrm_modulos';
  $cfgTableNameUsr = 'coecrm_usuarios';
  $cfgTableNameCat = 'coecrm_catalogos';
  $cfgTableNameOmn = 'bdOMNI';
  $cfgDbServer['location'] = '10.1.0.20';
  $cfgDbServer['user'] = 'desarrollo';
  $cfgDbServer['pass'] = 'Pa55w0rd!crm';
  $cfgServerLocationAbs = '/var/www/crm/';
  $cfgServerLocation = "http://localhost/";  //para pruebas locales
  $cfgServerLocation = "http://10.1.0.49/";  //server de pruebas
// variable to store the current time in seconds  

// producion
// $cfgTableNameMod = 'coecrm_modulos';
// $cfgTableNameUsr = 'coecrm_usuarios';
// $cfgTableNameCat = 'coecrm_catalogos';
// $cfgTableNameOmn = 'bdOMNI';
// $cfgDbServer['location'] = '10.1.0.109';
// $cfgDbServer['user'] = 'supervisor';
// $cfgDbServer['pass'] = 'Kb.204.h32017';
// $cfgServerLocationAbs = '/var/www/html/crm/';
// $cfgServerLocation = 'https://crm.coeficiente.mx/';  //el definitivo con dominio

// converts the time in seconds to current date  
// $currentTimeinSeconds = time();
// $currentDate = date('Y-m-d h:i:s', $currentTimeinSeconds);
$currentDate = date('Y-m-d h:i:s'); 
function condb_modulosl()
{
  global $cfgTableNameMod;
  global $cfgDbServer;
  $db_modulos = new PDO('mysql:host='. $cfgDbServer['location'].';dbname='.$cfgTableNameMod. ';charset=utf8', $cfgDbServer['user'], $cfgDbServer['pass']);
  // mysqli_query($db_modulos, "SET NAMES 'utf8'");
  return $db_modulos;
}
//datos de la session
$gruposArr  = $_SESSION['grupos'];
$usuarioId  = $_SESSION['usrId'];
$usuario    = $_SESSION['usuario'];
$nombre     = $_SESSION['usrNombre'];
$usrUbica   = $_SESSION['usrUbica'];

if(!verifySession($_SESSION)){
  logoutTimeout();
  echo 10;
}else{
  if($_SERVER['REQUEST_METHOD'] =='POST'){
    if($_POST['crud'] == 'C'){
      $conexion = condb_modulosl();
      $mensaje = $_POST['mensaje'];
      $insert = $conexion->prepare("INSERT INTO sugerencias (sugerencia, user_id, fecha) VALUES ('$mensaje','$usuarioId','$currentDate')");
      $insert->execute();
      if($conexion->lastInsertId()>0){
        //mesaje de repuesta basico
        $b = [];
        array_push($b, ['status' => true, 'msg' => "Gracias por tu sugerencia, la tomaremos en cuenta."]);
        echo json_encode($b);
      }else{
      $b = [];
      array_push($b, ['status' => fasle, 'msg' => "Lo sentimos, por el momento sufrimos dificultades tecnicas, intentalo mas tarde."]);
      echo json_encode($b);
      }
    } else if ($_POST['crud'] == 'D') {

      $conexion = condb_modulosl();
      $delete = $conexion->prepare("DELETE FROM sugerencias WHERE id_sugerencia = ".$_POST['id']);
      $delete->execute();
      if(0<$delete->rowCount()){
        //mesaje de repuesta basico
        $b = [];
        array_push($b, ['status' => true, 'msg' => "Se elimino la sugerencia con exito"]);
        echo json_encode($b);
      }else{
        //mesaje de repuesta basico
        $b = [];
        array_push($b, ['status' => false, 'msg' => "no se pudo eliminar la sugerencia, intentalo mas tarde, Gracias."]);
        echo json_encode($b);
      }
    }else if($_POST['crud'] == 'U'){
      // UPDATE
      //codigo para una modificacion de sugerencia
      //proximamente
    }else{
      //mesaje de repuesta basico
      $b = [];
      array_push($b, ['status' => false, 'msg' => "Operacion invalida"]);
      echo json_encode($b);
    }
  }else{
    //esto es el GET
    $conexion = condb_modulosl();
    $stmt = $conexion->prepare("SELECT * FROM sugerencias");
    $stmt->execute();
    if (0 < $stmt->rowCount()) {
      $b = [];
      array_push($b, ['status' => true, 'msg' => "Resultados"]);
      foreach ($stmt as $fila) {
        $c = [
          'id_sug'                  => $fila['id_sugerencia'],
          'id_user'                 => $fila['user_id'],
          'sugerencia'              => $fila['sugerencia'],
          'fecha'                   => $fila['fecha'],
        ];
        array_push($b, $c);
      }
      echo json_encode($b);
    } elseif(0 == $stmt->rowCount()){
      $b = [];
      $c = [
        'status'              => false,
        'msg'                 => "No existen sugerencias"
      ];
      array_push($b, $c);
      echo json_encode($b);
    }else{
      $b = [];
      $c = [
        'status'              => false,
        'msg'                 => "No se pudo consultar los datos"
      ];
      array_push($b, $c);
      echo json_encode($b);
    }
  }
}