<?php
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

include_once "../libs/db/dbcommon.php";
include_once "../libs/db/encrypt.php";
include_once "../libs/db/common.php";

if(!verifySession($_SESSION)){
  header("Location: ../../modulos/login/logout.php?sub=authpric");
}

$priceId = isset($_GET['pricId']) ? decrypt($_GET['pricId']) : -1;
$emailHashGet = isset($_GET['hs']) ? decrypt($_GET['hs']) : "";
$action = isset($_GET['action']) ? decrypt($_GET['action']) : -1;

$db_modulos  = condb_modulos();
$db_usuarios = condb_usuarios();

$salida = "";

$query = "SELECT rcp.relacion_id, cvs.*, cvv.cantidad_vigencia, cvv.factor, rcp.cantidad, cvts.categoria_id, ".
         "rcp.producto_precio_especial, rcp.producto_autorizado_por, rcp.sitio_asignado, rcp.vigencia ".
         "FROM ".$cfgTableNameCat.".cat_venta_servicios AS cvs, ".
                 $cfgTableNameCat.".cat_venta_tipo_servicios AS cvts, ".
                 $cfgTableNameMod.".rel_cotizacion_producto AS rcp, ".
                 $cfgTableNameCat.".cat_venta_vigencia AS cvv ".
         "WHERE cvs.servicio_id = rcp.producto_id ".
         "AND cvs.tipo_servicio = cvts.tipo_servicio_id ".
         "AND rcp.vigencia = cvv.vigencia_id ".
         "AND rcp.cotizacion_id = ".$priceId;
                
$result = mysqli_query($db_modulos,$query);

if(!$result){
  $salida = "ERROR|| No se pudo obtener los productos para esta cotización";  
}
else{
  if(0<mysqli_num_rows($result)){
     $msg = "Hola.<br><br>Se ha dado de alta una cotización cuyos precios de productos están pendientes de autorizar: <br>".
            "Para autorizar o rechazar el pecio haz click en cada una de las opciones que se presentan para cada precio:<br><br> ";
           
    if(-1==$row2['producto_autorizado_por']&&-1!=$row2['producto_precio_especial']){
      if(($row2['precio_lista']*$row2['factor'])<$row2['producto_precio_especial']){
        $nom = "";//precio encima de lista
      }
      else{
      
        $msg .= "<b>Producto o servicio:</b>".$rowProductCon['servicio']."<br> ".
                "<b>Precio base:</b>".$precioBase."<br> ".
                "<b>Precio de lista:</b>".$precioLista."<br> ".
                "<b>Precio sugerido por vendedor:</b>".$productArr[3]."&nbsp; ".                
                "<input type=\"button\" class=\"btn btn-xs btn-info\" onClick=\"priceAuth(\'".$newCotIdEnc."\',\'".$newProdIdEnc."\',0,\'".$precAct."\')\" value=\"Autorizar\"></input>&nbsp".
                "<input type=\"button\" class=\"btn btn-xs btn-danger\" onClick=\"priceAuth(\'".$newCotIdEnc."\',\'".$newProdIdEnc."\',1,\'".$precAct."\')\" value=\"Rechazar\"></input><br><br>";
                  
                  
        $nom = "";//precio a autorizar     
      }                     
    }
    else{
      $nom = ""; //precio autorizado ya
    }
  }
}

?>
