<?php
session_start();
set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

include_once "../libs/db/dbcommon.php";
include_once "../libs/db/encrypt.php";
include_once "../libs/db/common.php";

$priceId = isset($_GET['pricId']) ? $_GET['pricId'] : -1;
$emailHashGet = isset($_GET['hs']) ? $_GET['hs'] : "";
$action = isset($_POST['action']) ? $_POST['action'] : -1;

$db_modulos  = condb_modulos();
$db_usuarios = condb_usuarios();

$salida = "";

if (isset($_POST['action']) && (-1 != $action && "acceptTos" == $action)) {
  $priceIdPost = isset($_POST['pricId']) ? decrypt($_POST['pricId']) : -1;
  $emailHashPost = isset($_POST['hs']) ? decrypt($_POST['hs']) : "";
  //error_log("pricePost ".$_POST['pricId']." pricePost decrypt".$priceIdPost);
  $now = date('Y-m-d H:i:s');
  $queryPro = "SELECT pro.prospecto_id, pro.nombre_comercial, pro.asignadoa, pro.estatus, usr.ubicacion_id, cot.fecha_actualizacion, cot.email_hash " .
    "FROM " . $cfgTableNameMod . ".prospectos AS pro, " .
    $cfgTableNameMod . ".cotizaciones AS cot, " .
    $cfgTableNameUsr . ".usuarios AS usr " .
    "WHERE cot.cotizacion_id = " . $priceIdPost . " " .
    "AND cot.origen_id = pro.prospecto_id " .
    "AND pro.asignadoa=usr.usuario_id " .
    "AND cot.tipo_origen = 0";
  $resultPro = mysqli_query($db_modulos, $queryPro);
  if (!$resultPro) {
    error_log("ERROR: CONFIRM-NEGOTIATION-ACCEPTANCE: No se pudo obtener los datos del prospecto");
    $salida = "ERROR||Ocurrió un problema al obtener los datos del prospecto||";
  } else {
    $rowPro = mysqli_fetch_assoc($resultPro);
    $fecAct = new DateTime($rowPro['fecha_actualizacion']);
    $emailHashBD = decrypt($rowPro['email_hash']);
    if(isset($_POST['razon'])){
      $emailHashPost = $emailHashBD;
    }
    if ($emailHashBD !== $emailHashPost)
      $salida = "ERROR||La cotización o sus precios han sufrido cambios desde su envío. Por favor comuníquese con su agente de ventas||";
    else {
      if(isset($_POST['razon'])){
        // *****************************************************************************
        // si un administrador autoriza la cotizacion
        // *****************************************************************************
        $query2 = "UPDATE cotizaciones SET contrato=0, cotizacion_estatus_id=5,autorizado_por_id=" . $_SESSION['usrId'] . " , motivo='" . $_POST['razon'] . "' WHERE cotizacion_id=" . $priceIdPost;
        $result2 = mysqli_query($db_modulos, $query2);
      }else{
        // *****************************************************************************
        // si un cliente autoriza la cotizacion
        // *****************************************************************************
        $query2 = "UPDATE cotizaciones SET contrato=0, cotizacion_estatus_id=5 WHERE cotizacion_id=" . $priceIdPost;
        $result2 = mysqli_query($db_modulos, $query2);
      }
      $query3 = "SELECT usr.usuario_id, usr.nombre, usr.apellidos " .
        "FROM " . $cfgTableNameUsr . ".usuarios AS usr, " .
        $cfgTableNameUsr . ".rel_grupo_permisos AS rel, " .
        $cfgTableNameUsr . ".rel_usuario_grupo AS usrgrp " .
        "WHERE rel.modulo_id=1 " .
        "AND permiso LIKE '%a%' " .
        "AND usrgrp.grupo_id=rel.grupo_id " .
        "AND usrgrp.usuario_id=usr.usuario_id " .
        "AND usr.ubicacion_id=" . $rowPro['ubicacion_id'];
      $result3 = mysqli_query($db_modulos, $query3);
      if (4 != $rowPro['estatus']) {
        // se actualiza el prospecto a cierre al autorizar la cotizacion
        $query = "UPDATE prospectos SET estatus=4, fecha_actualizacion='" . $now . "' WHERE prospecto_id=" . $rowPro['prospecto_id'];
          
        
        
        //**************************************** */
          //**************************************** */
          // estatus al que cambia, y id del prospecto
          sendEstatusChangeEmail(4, $rowPro['prospecto_id']);
          //**************************************** */
          //**************************************** */




        error_log("DEBUG: UPDATE-ORIGIN-STATUS: Query " . $query);
        $result = mysqli_query($db_modulos, $query);
        $result2 = mysqli_query($db_modulos, $query2);

        if (!$result || !$result2 || !$result3) {
          error_log("ERROR: UPDATE-ORIGIN-STATUS: No se pudo actualizar el estatus del prospecto con ID " . $rowPro['prospecto_id'] . " " . $query . " || " . $query2 . " || " . $query3);
          $salida = "ERROR||Ocurrió un problema al enviar la información de su cotización. Por favor, comuníquese con su agente de ventas||";
        } else {
          $msgAccept = "Hola.<br><br>Se notifica que el prospecto " . $rowPro['nombre_comercial'] . " ha aceptado la propuesta enviada con id " . $priceIdPost . " El estatus ha cambiado a la fase de cierre. Por favor ponte en contacto con administración para el proceso de envío de documentos";
          while ($row3 = mysqli_fetch_assoc($result3)) {
            sendPrivMsg(0, $row3['usuario_id'], "Aceptación de Propuesta", $msgAccept, 2);
          }
          error_log("OK: UPDATE-ORIGIN-STATUS: Se actualizó el estatus del prospecto con ID " . $rowPro['prospecto_id']);
          $salida = "OK||Usted ha aceptado los términos del aviso de privacidad||";
        }
      } else {
        $msgAccept = "Hola.<br><br>Se notifica que el prospecto ya en fase de cierre " . $rowPro['nombre_comercial'] . " ha aceptado la propuesta enviada con id " . $priceIdPost . " Por favor ponte en contacto con administración para el proceso de documentación";
        while ($row3 = mysqli_fetch_assoc($result3)) {
          sendPrivMsg(0, $row3['usuario_id'], "Aceptación de Propuesta", $msgAccept, 2);
        }
        error_log("OK: UPDATE-ORIGIN-STATUS: Se actualizó el estatus del prospecto con ID " . $rowPro['prospecto_id']);
        $salida = "ERROR||Ha aceptado las condiciones de servicio||";
      }
    }
  }
  mysqli_close($db_usuarios);
  mysqli_close($db_modulos);
  echo $salida;
}

$aviso =
  "<div itemprop=\"articleBody\">
  <p><span style=\"font-size: 12pt;\">Responsable de la protección de tus datos personales</span></p>
  <p><span style=\"font-size: 12pt;\"><strong>COEFICIENTE COMUNICACIONES, S.A. DE C.V.</strong>, y/o cualquiera de sus subsidiarias y/o afiliadas (COEFICIENTE COMUNICACIONES, S.A. DE C.V), con domicilio en la calle Herrera y Cairo 1959, Colonia Ladrón de Guevara, Guadalajara, Jalisco, C.P. 44650, México, y portal de internet http://coeficiente.mx/, en cumplimiento a lo establecido por la Ley Federal de Protección de Datos personales en Posesión de los Particulares (“Ley”) y con la finalidad de garantizar la privacidad y el derecho a la autodeterminación informativa de sus clientes y usuarios, hace de su conocimiento la política de privacidad y manejo de datos personales.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(1)</sup>El uso o navegación por parte de cualquier persona del sitio antes mencionado le concede la calidad de Usuario y/o Cliente.</span></p>
  <p><span style=\"font-size: 12pt;\"><strong>A.- Recolección de datos.</strong></span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(2)</sup> Personales.- En la prestación de servicios, COEFICIENTE COMUNICACIONES, S.A. DE C.V requerirá a sus clientes sólo de datos personales, ya sea de identificación y/o financieros y/o profesionales proporcionados por usted de manera directa o por cualquier medio de contacto y/o foro público de conexión en línea relacionados con los servicios que presta COEFICIENTE COMUNICACIONES, S.A. DE C.V, destacando los siguientes:</span></p>
  <p><span style=\"font-size: 12pt;\">Nombre</span></p>
  <p><span style=\"font-size: 12pt;\">Registro Federal de Contribuyentes (RFC)</span></p>
  <p><span style=\"font-size: 12pt;\">Domicilio</span></p>
  <p><span style=\"font-size: 12pt;\">Teléfono particular</span></p>
  <p><span style=\"font-size: 12pt;\">Correo electrónico</span></p>
  <p><span style=\"font-size: 12pt;\">Datos de identificación</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(3)</sup> Con los fines señalados en este aviso de privacidad, podemos recabar sus datos personales de distintas formas: durante el proceso de registro, inscripción a alguna promoción o programa o cuando nos lo proporcione directamente.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(4)</sup> Es decir toda información personal que el usuario haya proporcionado a COEFICIENTE COMUNICACIONES, S.A. DE C.V de manera directa por medio de este sitio, aquella información personal proporcionada por el usuario COEFICIENTE COMUNICACIONES, S.A. DE C.V al momento de utilizar nuestros servicios en línea o cuando COEFICIENTE COMUNICACIONES, S.A. DE C.V obtiene la información personal del usuario lícitamente por cualquier otro medio o fuentes permitidas por la ley, serán utilizados con las siguientes finalidades: Proveer servicios y productos requeridos en el sitio, informar sobre nuevos productos o servicios que estén relacionados con el contratado o adquirido por el usuario, dar cumplimiento con obligaciones contraídas con el usuario, realizar estudios internos sobre hábitos de consumo, promover los productos y servicios que COEFICIENTE COMUNICACIONES, S.A. DE C.V ofrece, informarle sobre cambios en los mismos, para fines publicitarios, promocionales, telemarketing, operaciones, administración del sitio web de COEFICIENTE COMUNICACIONES, S.A. DE C.V, administración de los servicios de COEFICIENTE COMUNICACIONES, S.A. DE C.V, desarrollo de nuevos productos y servicios, encuestas de calidad del servicio o de producto contratados por el usuario y satisfacción del usuario, análisis de uso de productos, servicios y sitio web, para el envío de avisos acerca de productos y servicios operados por COEFICIENTE COMUNICACIONES, S.A. DE C.V y/o por sus afiliadas, subsidiarias y/o por sus socios de negocio; cuando la ley o alguna autoridad lo requiera, para solicitarle actualización de sus datos y documentos de identificación, y en general para hacer cumplir nuestros términos, condiciones y la operación, funcionamiento y administración de nuestros negocios.</span></p>
  <p><span style=\"font-size: 12pt;\">De igual manera el usuario o cliente otorga su consentimiento expreso para que COEFICIENTE COMUNICACIONES, S.A. DE C.V pueda referir su razón social, nombre comercial, logotipos y/o marcas registradas para efectos curriculares o de referenciación mercantil.</span></p>
  <p><span style=\"font-size: 12pt;\">&nbsp;</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(5)</sup> El concepto de “datos personales” en este aviso de privacidad se refiere a toda aquella información de carácter personal que pueda ser usada para identificación del Usuario.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(6)</sup> COEFICIENTE COMUNICACIONES, S.A. DE C.V no recaba ni trata datos personales sensibles.</span></p>
  <p><span style=\"font-size: 12pt;\"><strong>B.- Uso de cookies, scripts y web beacons.</strong></span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(7)</sup> Los datos personales que recabamos cuando visitas nuestro sitio de Internet o utilizas nuestros servicios en línea son la Dirección IP y Cookies.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(8)</sup> COEFICIENTE COMUNICACIONES, S.A. DE C.V podrá recabar datos personales por el uso de cookies y Web beacons. La página de nuestro sitio Web utiliza cookies, los cuales son pequeños ficheros de datos que se descargan automáticamente y almacenados en la computadora del Usuario, las cuales graban sus datos personales cuando se conecta al sitio, entre ellos sus preferencias para la visualización de las páginas de este servidor, modificándose al abandonar el sitio y las cuales permiten obtener la información siguiente:</span></p>
  <p><span style=\"font-size: 12pt;\">Navegador y sistema operativo del usuario.</span></p>
  <p><span style=\"font-size: 12pt;\">Páginas de internet que visita el usuario.</span></p>
  <p><span style=\"font-size: 12pt;\">La fecha y hora de la última vez que el usuario visitó nuestro sitio Web.</span></p>
  <p><span style=\"font-size: 12pt;\">El diseño de contenidos o preferencias que el usuario escogió en su primera visita a nuestro sitio web.</span></p>
  <p><span style=\"font-size: 12pt;\">Elementos de seguridad que intervienen en el control de acceso a las áreas restringidas.</span></p>
  <p><span style=\"font-size: 12pt;\">Reconocer a los usuarios.</span></p>
  <p><span style=\"font-size: 12pt;\">Detectar su ancho de banda.</span></p>
  <p><span style=\"font-size: 12pt;\">Medir parámetros de tráfico.</span></p>
  <p><span style=\"font-size: 12pt;\">Su tipo de navegador y sistema operativo.</span></p>
  <p><span style=\"font-size: 12pt;\">Las páginas de Internet que visitas.</span></p>
  <p><span style=\"font-size: 12pt;\">Los vínculos que sigues.</span></p>
  <p><span style=\"font-size: 12pt;\">Su dirección IP.</span></p>
  <p><span style=\"font-size: 12pt;\">El sitio web que lo dirigió al nuestro.</span></p>
  <p><span style=\"font-size: 12pt;\">La resolución de su monitor.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(9)</sup> Los cookies son anónimos. El acceso a la información por medio de los cookies, permite ofrecer al Usuario un servicio personalizado, ya que almacenan no sólo sus datos personales sino también la frecuencia de utilización del servicio y las secciones de la red visitadas, reflejando así sus hábitos y preferencias. Las redes publicitarias que insertan avisos en nuestras páginas pueden también utilizar sus propios cookies.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(10)</sup> El Usuario tiene la opción de impedir la generación de cookies, mediante la selección de la correspondiente opción en la configuración de su navegador de Internet. Sin embargo COEFICIENTE COMUNICACIONES, S.A. DE C.V no se responsabiliza de que la desactivación de los mismos, ya que impedirían el buen funcionamiento del sitio de Internet.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(11)</sup> Los cookies son archivos de texto descargados automáticamente y almacenados en el disco duro del equipo de cómputo del usuario al navegar en una página de Internet específica, que permiten recordar al servidor de Internet algunos datos sobre este usuario, entre ellos, sus preferencias para la visualización de las páginas en ese servidor, nombre y contraseña.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(12)</sup> Así mismo las scripts son piezas de código que nos permiten obtener información como la dirección IP del usuario, duración del tiempo de interacción en una página y el tipo de navegador utilizado, entre otros.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(13)</sup> Por su parte, las web beacons son imágenes insertadas en una página de Internet o correo electrónico, que puede ser utilizado para monitorear el comportamiento de un visitante y las características de su navegación en nuestro sitio web.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(14)</sup> Estas cookies y otras tecnologías pueden ser deshabilitadas. Para conocer como hacerlo, consulte la documentación de su navegador.</span></p>
  <p><span style=\"font-size: 12pt;\"><strong>C.- eMail Marketing.</strong></span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(15)</sup> En este apartado se especifican los términos y condiciones del uso del servicio de Mailing o eMail Marketing en el cual se exige la correcta aplicación de la herramienta, para su excelente funcionamiento y obtención de los mejores beneficios del servicio.</span></p>
  <p><span style=\"font-size: 12pt;\"><strong>D.- Uso general de servicio de COEFICIENTE COMUNICACIONES, S.A. DE C.V Mailing. </strong></span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(16)</sup> Es su deber tener el consentimiento o permiso de los contactos de su lista de envío, para agregarlos al Mailing, ya que sin ellos todo envío, notificación o correo eMail Marketing, será reportado como spam.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(17)</sup> Únicamente se podrá agregar como contacto a aquellas personas o clientes que hayan aceptado, de manera explícita, escrita o por correo electrónico el Mailing de COEFICIENTE COMUNICACIONES, S.A. DE C.V que manda desde su red. Y solo para los temas en los que específicamente están de acuerdo.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(18)</sup> Siga los siguientes lineamientos para que su destinatario, cliente o suscriptor esté en la categoría de “Uso Aceptado” y evite la cancelación inmediata de su cuenta.</span></p>
  <p><span style=\"font-size: 12pt;\"><strong>E.- Uso Aceptado. </strong></span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(19)</sup> El usuario, cliente o suscriptor acepta explícitamente recibir correos electrónicos.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(20)</sup> Cada contacto se ha suscrito a una de sus listas en línea y consintió claramente obtener uno o más tipos de email al dar clic a uno o más rubros.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(21)</sup> Se admite como válido cuando el registro se hizo en un sitio distinto al de Ud., siempre y cuando se tengan dos opciones para suscripción para cada lista.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(22)</sup> Si por 6 meses no se ha hecho algún envío a los suscriptores del Mailing de COEFICIENTE COMUNICACIONES, S.A. DE C.V, para evitar fraudes, malentendidos o confusión; es importante realizar un envío de reintroducción para informar qué tipo de información se estará recibiendo y con qué periodicidad.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(23)</sup> El contacto fuera de línea admitió conscientemente recibir correos electrónicos.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(24)</sup> Completó una encuesta o participó en un concurso fuera de línea, en la cual otorga la autorización “por escrito” que quiere recibir dicha información.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(25)</sup> El contacto otorgó su información o tarjeta de presentación en un evento.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(26)</sup> El usuario al dar su información, a prueba contundentemente ser agregado a una lista con un tema particular de Mailing. Es obligación de quien recibe dicha información, enviar un correo introductorio en el que se explique a qué está suscrito, por qué y adjuntar los datos generales de la empresa.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(27)</sup> El contacto se suscribió solo a un tema en específico</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(28)</sup> El contacto o suscriptor deberá tener la certeza que obtendrá el correo de eMail Marketing que haya autorizado o que esté en el contrato.</span></p>
  <p><span style=\"font-size: 12pt;\"><strong>F.- Uso No Aceptado.</strong></span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(29)</sup> Obtener el correo de un contacto por Internet u otra fuente y agregarlo al Mailing sin su consentimiento.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(30)</sup> Se considera como spam al envió del Mailing que ha suspendido por más de 2 años y se vuelve a reactivar, aunque se cuente con el permiso explícito del usuario, ya que en ese tiempo pudo olvidar la suscripción.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(31)</sup> Obtener contactos de terceras personas, ya sea producto de la renta, compra o préstamo de una lista, a pesar de que los contactos se hayan suscrito legalmente, pero no dieron permiso para recibir el Mailing de su sitio específicamente.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(32)</sup> Enviar el correo de eMail Marketing a un contacto que es su cliente pero no se ha suscrito.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(33)</sup> Suscribir y enviar el Mailing a un contacto con una cuenta genérica como: “<span id=\"cloak8deeca49d1bec72ef59eaf1e2bf22c1b\"><a href=\"mailto:ventas@dominio.com\">ventas@dominio.com</a></span><script type=\"text/javascript\">
          document.getElementById('cloak8deeca49d1bec72ef59eaf1e2bf22c1b').innerHTML = '';
          var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
          var path = 'hr' + 'ef' + '=';
          var addy8deeca49d1bec72ef59eaf1e2bf22c1b = 'v&#101;nt&#97;s' + '&#64;';
          addy8deeca49d1bec72ef59eaf1e2bf22c1b = addy8deeca49d1bec72ef59eaf1e2bf22c1b + 'd&#111;m&#105;n&#105;&#111;' + '&#46;' + 'c&#111;m';
          var addy_text8deeca49d1bec72ef59eaf1e2bf22c1b = 'v&#101;nt&#97;s' + '&#64;' + 'd&#111;m&#105;n&#105;&#111;' + '&#46;' + 'c&#111;m';document.getElementById('cloak8deeca49d1bec72ef59eaf1e2bf22c1b').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy8deeca49d1bec72ef59eaf1e2bf22c1b + '\'>'+addy_text8deeca49d1bec72ef59eaf1e2bf22c1b+'<\/a>';
      </script>” o “<span id=\"cloakadf198286037ff84d894f96e72687387\"><a href=\"mailto:contacto@dominio.com\">contacto@dominio.com</a></span><script type=\"text/javascript\">
          document.getElementById('cloakadf198286037ff84d894f96e72687387').innerHTML = '';
          var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
          var path = 'hr' + 'ef' + '=';
          var addyadf198286037ff84d894f96e72687387 = 'c&#111;nt&#97;ct&#111;' + '&#64;';
          addyadf198286037ff84d894f96e72687387 = addyadf198286037ff84d894f96e72687387 + 'd&#111;m&#105;n&#105;&#111;' + '&#46;' + 'c&#111;m';
          var addy_textadf198286037ff84d894f96e72687387 = 'c&#111;nt&#97;ct&#111;' + '&#64;' + 'd&#111;m&#105;n&#105;&#111;' + '&#46;' + 'c&#111;m';document.getElementById('cloakadf198286037ff84d894f96e72687387').innerHTML += '<a ' + path + '\'' + prefix + ':' + addyadf198286037ff84d894f96e72687387 + '\'>'+addy_textadf198286037ff84d894f96e72687387+'<\/a>';
      </script>”, ya que regularmente cambian de usuario y no se podría dirigir el envió a alguien en específico.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(34)</sup> Mandar el Mailing a un correo que es una lista de distribución, ya que ése correo manda el eMail Marketing a más de una persona. Lo que es imposible saber sí éstos contactos dieron su autorización explícita para recibir correos.</span></p>
  <p><span style=\"font-size: 12pt;\"><strong>G.- Contenido Obligatorio. </strong></span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(35)</sup> Cada correo que envíes con COEFICIENTE COMUNICACIONES, S.A. DE C.V Mailing deberá incluir lo siguiente:</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(36)</sup> La opción directa de eliminar la suscripción al Mailing hecha en tan solo un clic.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(37)</sup> El nombre y la dirección física de quien envía el correo (Persona o empresa).</span></p>
  <p><span style=\"font-size: 12pt;\"><strong>H.- Finalidad del tratamiento de datos.</strong></span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(38)</sup> Los datos personales que el cliente nos proporcione con fines comerciales para contactarlo y/o enviarle información respecto de los servicios contratados, así como con fines estadísticos sujetos a un proceso de disociación.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(39)</sup> Los datos personales también son utilizados para realizar estudios internos sobre los datos demográficos, intereses y comportamiento de los usuarios; con la finalidad de proporcionarles productos, servicios, contenidos y publicidad acordes a sus necesidades, así como proporcionar notificaciones e información de manera confidencial sobre su servicio y contactar a los usuarios cuando sea necesario, por teléfono o correo electrónico en caso de que se requieran datos adicionales para completar alguna transacción.</span></p>
  <p><span style=\"font-size: 12pt;\"><strong>I.- Transferencia de datos.</strong></span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(40)</sup> COEFICIENTE COMUNICACIONES, S.A. DE C.V podrá tratar los datos personales o los podrá poner a disposición de terceros dentro o fuera del país, en este sentido, su información puede ser compartida a otros tercero a consecuencia de una relación contractual o bien en aquellos casos en que la divulgación sea necesaria para la eficaz operación de los servicios proporcionados.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(41)</sup> Si usted no manifiesta oposición alguna por escrito para que sus datos sean tratados y/o transferidos a terceros, se entenderá que ha otorgado su consentimiento para ello una vez que haya otorgado la aceptación electrónica y/o al ingresar sus datos dentro del contenido y funciones del sitio.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(42)</sup> COEFICIENTE COMUNICACIONES, S.A. DE C.V podrá transmitir lo datos que recabe de sus clientes a cualquiera de las subsidiarias y/o afiliadas y/o socios de negocios y/o terceros, quienes quedarán obligadas a resguardar y utilizar la información en términos de este Aviso de Privacidad y por cualquiera de las razones previstas en Ley, COEFICIENTE COMUNICACIONES, S.A. DE C.V se reserva el derecho de transmitir los datos de sus clientes.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(43)</sup> En caso de vender o traspasar la totalidad o parte de su negocio y/o activos, COEFICIENTE COMUNICACIONES, S.A. DE C.V le comunicará la obligación al siguiente propietario para que utilice los datos de los clientes con apego a esté Aviso de Privacidad, así mismo le informará al titular que pretende transferir los datos a terceros. Si el titular no desea que sus datos sean transferidos al tercero deberá manifestarlo al nuevo propietario.</span></p>
  <p><span style=\"font-size: 12pt;\"><strong>J.- Medios para ejercer los derechos de acceso, rectificación, cancelación u oposición.</strong></span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(44)</sup> Usted tiene derecho a conocer qué datos personales tenemos suyos, para qué los utilizamos y las condiciones del uso que les damos (Acceso). Asimismo, es su derecho solicitar la corrección de su información personal en caso de que esté desactualizada, sea inexacta o incompleta (Rectificación); que la eliminemos de nuestros registros o bases de datos cuando considere que la misma no está siendo utilizada adecuadamente (Cancelación); así como oponerse al uso de sus datos personales para fines específicos (Oposición). Estos derechos se conocen como derechos ARCO.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(45)</sup> Todo cliente de COEFICIENTE COMUNICACIONES, S.A. DE C.V interesado en ejecutar los derechos ARCO previstos en la Ley, podrá hacerlo poniéndose en contacto nuestro Departamento de Protección de Datos Personales, en el domicilio ubicado en Calle Herrera y Cairo 1959, Col. Ladrón de Guevara, Guadalajara, Jalisco, CP 44650, México, o bien, se comunique al teléfono 01 (33) 2282 8180 o vía correo electrónico <span id=\"cloak77e7ca9bc1dc1758831e16f2793c74d1\"><a href=\"mailto:contacto@coeficiente.mx\">contacto@coeficiente.mx</a></span><script type=\"text/javascript\">
          document.getElementById('cloak77e7ca9bc1dc1758831e16f2793c74d1').innerHTML = '';
          var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
          var path = 'hr' + 'ef' + '=';
          var addy77e7ca9bc1dc1758831e16f2793c74d1 = 'c&#111;nt&#97;ct&#111;' + '&#64;';
          addy77e7ca9bc1dc1758831e16f2793c74d1 = addy77e7ca9bc1dc1758831e16f2793c74d1 + 'c&#111;&#101;f&#105;c&#105;&#101;nt&#101;' + '&#46;' + 'mx';
          var addy_text77e7ca9bc1dc1758831e16f2793c74d1 = 'c&#111;nt&#97;ct&#111;' + '&#64;' + 'c&#111;&#101;f&#105;c&#105;&#101;nt&#101;' + '&#46;' + 'mx';document.getElementById('cloak77e7ca9bc1dc1758831e16f2793c74d1').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy77e7ca9bc1dc1758831e16f2793c74d1 + '\'>'+addy_text77e7ca9bc1dc1758831e16f2793c74d1+'<\/a>';
      </script>, el cual solicitamos confirme vía telefónica para garantizar su correcta recepción.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(46)</sup> Con relación al procedimiento y requisitos para el ejercicio de sus derechos ARCO, le informamos lo siguiente:</span></p>
  <ol>
  <li><span style=\"font-size: 12pt;\">a) A través de qué medios pueden acreditar su identidad el titular y, en su caso, su representante, así como la personalidad este último? Con el original y copia de identificación oficial vigente; en el caso de personas morales, se debe agregar original y copia del instrumento notarial en el que consten sus facultades.</span></li>
  <li><span style=\"font-size: 12pt;\">b) Qué información y/o documentación deberá contener la solicitud? Los pormenores respecto de los derechos ARCO, y la precisión o intención que busca el titular, narrando con amplitud los hechos en que basa su pedido.</span></li>
  <li><span style=\"font-size: 12pt;\">c) En cuántos días le daremos respuesta a su solicitud? En diez días hábiles.</span></li>
  <li><span style=\"font-size: 12pt;\">d) Por qué medio le comunicaremos la respuesta a su solicitud? Por correo electrónico.</span></li>
  </ol>
  <p><span style=\"font-size: 12pt;\"><sup>(47)</sup> Usted puede revocar el consentimiento que, en su caso, nos haya otorgado para el tratamiento de sus datos personales. Sin embargo, es importante que tenga en cuenta que no en todos los casos podremos atender su solicitud o concluir el uso de forma inmediata, ya que es posible que por alguna obligación legal requiramos seguir tratando sus datos personales. Asimismo, usted deberá considerar que para ciertos fines, la revocación de su consentimiento implicará que no le podamos seguir prestando el servicio que nos solicitó, o la conclusión de su relación con nosotros.</span></p>
  <p><span style=\"font-size: 12pt;\"><strong>K.- Cambios al Aviso de Privacidad</strong></span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(48)</sup> El presente aviso de privacidad puede sufrir modificaciones, cambios o actualizaciones derivadas de nuevos requerimientos legales; de nuestras propias necesidades por los productos o servicios que ofrecemos; de nuestras prácticas de privacidad; de cambios en nuestro modelo de negocio, o por otras causas.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(49)</sup> Cualquier cambio a este Aviso de Privacidad se hará del conocimiento de los clientes de COEFICIENTE COMUNICACIONES, S.A. DE C.V que marquen al 01 (33) 2282 8180 o manden correo electrónico a la siguiente dirección: <span id=\"cloaka1c0c7828646a67bced6f26a03356f60\"><a href=\"mailto:contacto@coeficiente.mx\">contacto@coeficiente.mx</a></span><script type=\"text/javascript\">
          document.getElementById('cloaka1c0c7828646a67bced6f26a03356f60').innerHTML = '';
          var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
          var path = 'hr' + 'ef' + '=';
          var addya1c0c7828646a67bced6f26a03356f60 = 'c&#111;nt&#97;ct&#111;' + '&#64;';
          addya1c0c7828646a67bced6f26a03356f60 = addya1c0c7828646a67bced6f26a03356f60 + 'c&#111;&#101;f&#105;c&#105;&#101;nt&#101;' + '&#46;' + 'mx';
          var addy_texta1c0c7828646a67bced6f26a03356f60 = 'c&#111;nt&#97;ct&#111;' + '&#64;' + 'c&#111;&#101;f&#105;c&#105;&#101;nt&#101;' + '&#46;' + 'mx';document.getElementById('cloaka1c0c7828646a67bced6f26a03356f60').innerHTML += '<a ' + path + '\'' + prefix + ':' + addya1c0c7828646a67bced6f26a03356f60 + '\'>'+addy_texta1c0c7828646a67bced6f26a03356f60+'<\/a>';
      </script> además de publicarlo en nuestro portal de internet.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(50)</sup> Este aviso de privacidad, describe la Política de Privacidad de COEFICIENTE COMUNICACIONES, S.A. DE C.V, el cual constituye un acuerdo válido entre el Usuario y COEFICIENTE COMUNICACIONES, S.A. DE C.V; si el Usuario utiliza los servicios de COEFICIENTE COMUNICACIONES, S.A. DE C.V significa que ha leído, entendido, aceptado y consecuentemente acordado con COEFICIENTE COMUNICACIONES, S.A. DE C.V los términos del aviso de privacidad antes expuestos. En caso de no estar de acuerdo con ellos, el Usuario NO deberá proporcionar ninguna información personal, ni utilizar este servicio o cualquier información relacionada con el sitio.</span></p>
  <p><span style=\"font-size: 12pt;\"><sup>(51)</sup> Última actualización: Enero del 2016.</span></p>
  <p><span style=\"font-size: 12pt;\">&nbsp;</span></p>
  <p><span style=\"font-size: 12pt;\">&nbsp;</span></p> 	</div>";
// salto de linea   --  https://crm.coeficiente.mx/crm/servicios/confirmnegociationacceptance.php?pricId=Qk5TZld0c2ZIZFpPTkJNQVN4ZDhwQT09&hs=Yis5dWpiQUJWdGhWSnlQUHA2VnU4dz09
// $aviso = $priceId . " - " . $emailHashGet . " - " . $action;
?>

<html>

<head>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link href="../libs/AdminLTE2/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
  <link href="../libs/AdminLTE2/plugins/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css" />
  <link href="../libs/js/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />
  <link href="../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.css" rel="stylesheet" type="text/css" />

  <script src="../libs/AdminLTE2/plugins/jQuery/jQuery-2.1.3.js" type="text/javascript"></script>
  <script src="../libs/AdminLTE2/plugins/jQueryUI/jquery-ui.js" type="text/javascript"></script>
  <script src="../libs/AdminLTE2/bootstrap/js/bootstrap.js" type="text/javascript"></script>
  <script src="../libs/AdminLTE2/dist/js/app.js" type="text/javascript"></script>
  <script src="../libs/js/jquery.form.js" type="text/javascript"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $("#chk_tosAccept").prop('disabled', true);
      $("#btn_tosAccept").prop('disabled', true);

      $("#div_tos").scroll(function() {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
          $("#chk_tosAccept").prop('disabled', false);
        }
      });

      $("#chk_tosAccept").change(function() {
        if ($(this).is(':checked')) {
          $("#btn_tosAccept").prop('disabled', false);
        } else {
          $("#btn_tosAccept").prop('disabled', true);
        }
      })
    });

    function tosAccept() {
      var pricId = $("#hdn_pricId").val();
      var hs = $("#hdn_hs").val();
      var orgId = $("#hdn_orgId").val();
      
      $.ajax({
        type: "POST",
        url: "confirmnegociationacceptance.php",
        data: {
          "action": "acceptTos",
          "pricId": pricId,
          "orgId": orgId,
          "hs": hs
        },
        beforeSend: function() {
          //setLoadDialog(0,"Enviando Información");
        },
        complete: function() {
          //setLoadDialog(1,"");
        },
        success: function(msg) {
          var resp = msg.trim().split("||");
          alert(resp[1]);
        }
      });
    }
    
  </script>
  <title>Coeficiente CRM - Aviso de Privacidad</title>
  <style type="text/css">
    .conf {
      height: 50%;
      overflow-y: auto;
    }
  </style>
</head>

<body>

  <div>
    <div class="container-fluid">
      <h2 class="page-header" id="tituloPag">
        <span class="glyphicon glyphicon-pen"></span>Políticas de Privacidad
      </h2>
    </div>
    <div id="div_tos" class="conf">
      <?php echo $aviso ?>
    </div>
    <div>
      <br>
      <input type="checkbox" id="chk_tosAccept">Acepto los términos y políticas de privacidad.</input>
      <br><br>
    </div>
    <div>
      <button type="button" id="btn_tosAccept" title="He leído y acepto Condiciones de Servicio" class="btn btn-success" onClick="tosAccept();"><span class="glyphicon glyphicon-ok"></span> Aceptar</button>
    </div>
    <input type="hidden" id="hdn_pricId" name="hdn_pricId" class="form-control" value=<?php echo $priceId; ?>>
    <input type="hidden" id="hdn_hs" name="hdn_hs" class="form-control" value=<?php echo $emailHashGet; ?>>
  </div>
</body>

</html>