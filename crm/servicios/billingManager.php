<?php
error_reporting(0);
ini_set('display_errors', 1);
set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');
include_once "../libs/db/dbcommon.php";
include_once "../libs/db/common.php";
$cfgLogService = "../log/billingManager";
$db_catalogos = condb_catalogos();
$db_modulos   = condb_modulos();
$db_usuarios  = condb_usuarios();
$diaNow = date('d');
$queryFecCor = "SELECT fecha_corte_id, fecha_corte_dia, fecha_corte_venc ".
               "FROM ".$cfgTableNameCat.".cat_fechas_corte ".
               "WHERE fecha_corte_dia=(SELECT MAX(fecha_corte_dia) FROM ".
                      $cfgTableNameCat.".cat_fechas_corte WHERE fecha_corte_dia<=$diaNow)";
               
log_write("DEBUG: SAVE-BILLING-MANAGER: fecha de corte: ".$queryFecCor,9);                 
               
$resultFecCor = mysqli_query($db_catalogos,$queryFecCor);

if(!$resultFecCor){
  log_write("ERROR: SAVE-BILLING-MANAGER: No se pudo consultar las fechas de corte",9);
}
else{
  $rowFecCor = mysqli_fetch_assoc($resultFecCor);
  
  log_write("DEBUG: SAVE-BILLING-MANAGER: Fecha de corte id : ".$rowFecCor['fecha_corte_id'],9);
  
  $queryFacDat = "SELECT facdat.* ".                                                          
                 "FROM ".$cfgTableNameMod.".facturacion_datos AS facdat ".
                 "WHERE fecha_corte_id = ".$rowFecCor['fecha_corte_id'];
                 
  log_write("DEBUG: SAVE-BILLING-MANAGER: datos de facturacion query: ".$queryFacDat,9);
           
  $resultFacDat = mysqli_query($db_modulos,$queryFacDat);

  if(!$resultFacDat){
    log_write("ERROR: SAVE-BILLING-MANAGER: No se pudo consultar los datos de cotización",9);
  }
  else{
    log_write("DEBUG: SAVE-BILLING-MANAGER: Num de datos de cotización: ".mysqli_num_rows($resultFacDat),9);
    
    while($rowFacDat = mysqli_fetch_assoc($resultFacDat)){
      log_write("DEBUG: SAVE-BILLING-MANAGER: facturación ID: ".$rowFacDat['facturacion_id']. " feccortdia ".$rowFecCor['fecha_corte_dia'],9);
      
      switch($rowFacDat['tipo_origen']){
        case 0:
          $tableName = 'prospectos';
          $prefName = 'pro';
          $fieldName = 'prospecto_id';
        break;
        
        case 1:
          $tableName = 'clientes';
          $prefName = 'cli';
          $fieldName = 'cliente_id';
        break;
        
        case 2:
          $tableName = 'sitios';
          $prefName = 'sit';
          $fieldName = 'sitio_id';
        break;
      }
     
      $queryCotDat = "SELECT cot.*, $prefName.*, usr.ubicacion_id ".
                     "FROM ".$cfgTableNameMod.".cotizaciones AS cot, ".
                             $cfgTableNameMod.".$tableName AS $prefName, ".
                             $cfgTableNameCat.".cat_coe_ubicacion AS ubi, ".
                             $cfgTableNameUsr.".usuarios AS usr ".
                     "WHERE $prefName.$fieldName=cot.origen_id ".
                     "AND cot.origen_id=".$rowFacDat['origen_id']." ".
                     "AND cot.tipo_origen=".$rowFacDat['tipo_origen']." ".
                     "AND usr.ubicacion_id=ubi.ubicacion_id ".
                     "AND cot.usuario_alta=usr.usuario_id ".
                     "AND cot.contrato=1 ".
                     "ORDER BY cot.origen_id,cot.tipo_origen"; 
                     
      log_write("DEBUG: SAVE-BILLING-MANAGER: Cotización: ".$queryCotDat,9);  
                   
      $resultCotDat = mysqli_query($db_modulos,$queryCotDat);
      
      if(!$resultCotDat){
        log_write("ERROR: SAVE-BILLING-MANAGER: No se pudo obtener los datos de productos para esta cotización",9);
      }
      else{ 
        if(0>=mysqli_num_rows($resultCotDat)){
          log_write("DEBUG: SAVE-BILLING-MANAGER: No existen resultados de datos de facturación",9);     
        }
        else{
          $subtotal = 0;
          $iva = 0;
          $total = 0;
      
          while($rowCotDat = mysqli_fetch_assoc($resultCotDat)){

            $subtotal += $rowCotDat['subtotal'];
            $iva += $rowCotDat['impuesto'];
            $total += $rowCotDat['total'];
            
            log_write("DEBUG: SAVE-BILLING-MANAGER: Cotizacion_id ".$rowCotDat['cotizacion_id']." Subtotal ".$rowCotDat['subtotal']." Impuesto ".$rowCotDat['impuesto']." Total ".$rowCotDat['total'],9);
          
            $corte = date("Y-m")."-".sprintf("%02d",(int)$rowFecCor['fecha_corte_dia']+1);    
            $queryFactPer = "SELECT fact_period_id, periodo_inicio, servicios_extra ".
                            "FROM facturacion_periodos ".
                            "WHERE facturacion_dato_id =".$rowFacDat['facturacion_id']." ".
                            "AND periodo_inicio='$corte' ORDER BY periodo_inicio DESC";
                            
            $resultFactPer = mysqli_query($db_modulos,$queryFactPer);
            if(!$resultFactPer)
            {
              log_write("ERROR: SAVE-BILLING-MANAGER: Error al consultar los datos de pariodo de facturación ".$queryFactPer,9);
            } 
            elseif(0==mysqli_num_rows($resultFactPer)){
              $perfin = date('Y-m-d',strtotime('+1 month',strtotime($corte)));
              $queryFactPerIns = "INSERT INTO facturacion_periodos(facturacion_dato_id,periodo_inicio,periodo_fin,importe,iva,total,servicios_extra,fecha_alta) ".
              "VALUES(".$rowFacDat['facturacion_id'].",'".$corte."','".$perfin."',".$subtotal.",".$iva.",".$total.",'','".date('Y-m-d H:i:s')."')";
              
              log_write("DEBUG: SAVE-BILLING-MANAGER: Inserción: ".$queryFactPerIns,9);
              
              $resultFactPerIns = mysqli_query($db_modulos,$queryFactPerIns);
              
              if(!$resultFactPerIns){
                log_write("ERROR: SAVE-BILLING-MANAGER: Error al insertarse los datos de periodo de facturación",9);
              }
              else{
                log_write("OK: SAVE-BILLING-MANAGER: Se agregaron los datos nuevos de periodo de facturación",9);
              }
            }
            else{
              $rowFactPer = mysqli_fetch_assoc($resultFactPer);
              if((isset($rowFactPer['servicios_extra']))&&(0<strlen($rowFactPer['servicios_extra']))){
                $serExt = explode(",",$rowFactPer['servicios_extra']);
                $queryCatSerExt = "SELECT * FROM cat_servicios_extra WHERE ";
                foreach($serExt as $value){
                  $queryCatSerExt .=" servicio_extra_id=".$value." OR ";
                }
                
                $queryCatSerExt = substr($queryCatSerExt,0,-3);
                
                log_write("DEBUG: SAVE-BILLING-MANAGER: servicios extra: ".$queryCatSerExt,9);
               
                $resultCatSerExt = mysqli_query($db_catalogos,$queryCatSerExt);
               
                while($rowCatSerExt = mysqli_fetch_assoc($resultCatSerExt)){
                  $subtotal = $subtotal+$rowCatSerExt['precio'];
                  
                  log_write("DEBUG: SAVE-BILLING-MANAGER: ser extra ".$rowCatSerExt['precio'],9);
                } 
              }
              else{
                log_write("DEBUG: SAVE-BILLING-MANAGER: servicios extra: Sin servicios extra",9);
              }
              $iva = $subtotal*0.16;
              $total = $subtotal+$iva;  
                     
              $queryFactPerUpd = "UPDATE facturacion_periodos SET importe=$subtotal, iva=$iva, total=$total WHERE facturacion_dato_id=".$rowFacDat['facturacion_id'];
              
              log_write("DEBUG: SAVE-BILLING-MANAGER: Actualización: ".$queryFactPerUpd,9);
              
              $resultfactPerUpd = mysqli_query($db_modulos,$queryFactPerUpd);
              
              if(!$resultfactPerUpd){
                log_write("ERROR: SAVE-BILLING-MANAGER: Error al actualizarse los datos de pariodo de facturación",9);
              }
              else{
                log_write("OK: SAVE-BILLING-MANAGER: Los datos de facturación fueron actualizados ",9);
              }
            }
          }
        }
      }
    }         
  }    
}

//echo "QUeryfacper".$queryFactPer;         

mysqli_close($db_catalogos);
mysqli_close($db_modulos);
mysqli_close($db_usuarios);


?>
