<?php
error_reporting(0);
ini_set('display_errors', 1);

set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

include_once "../libs/db/dbcommon.php";
include_once "../libs/db/common.php";

$cfgLogService = "../log/billingSMSManager";

$db_catalogos = condb_catalogos();
$db_modulos   = condb_modulos();
$db_usuarios  = condb_usuarios();

$diaNow = date('d');

$queryFacDat = "SELECT facdat.facturacion_id, facdat.tel_notificacion, facdat.notificaciones, catfeccot.fecha_corte_venc, ".
               "catfeccot.fecha_corte_dia, catden.denominacion_abvr ".
               "FROM ".$cfgTableNameCat.".cat_fechas_corte AS catfeccot, ".
                       $cfgTableNameCat.".cat_denominacion AS catden, ".
                       $cfgTableNameMod.".facturacion_datos AS facdat ".
               "WHERE facdat.fecha_corte_id=catfeccot.fecha_corte_id ".
               "AND facdat.denominacion=catden.denominacion_id ".
               "AND (facdat.tipo_origen=1 OR facdat.tipo_origen=2) ".
               "AND catfeccot.fecha_corte_dia=(SELECT MAX(fecha_corte_dia) FROM ".
                    $cfgTableNameCat.".cat_fechas_corte WHERE fecha_corte_dia<=$diaNow) ".
               "AND facdat.pagado = 0 ".
               "AND facdat.notificaciones<2";
               
log_write("DEBUG: SAVE-BILLING-MANAGER: fecha de corte: ".$queryFacDat,9);                 
               
$resultFacDat = mysqli_query($db_modulos,$queryFacDat);

if(!$resultFacDat){
 log_write("ERROR: SAVE-BILLING-MANAGER: No se pudo consultar las fechas de corte ",9);
}
else{    
/*COEFICIENTE le recuerda que tiene un saldo de %n% %m% con fecha limite de pago el dia %d%.
En COEFICIENTE le recordamos que la fecha límite de pago de su servicio es el día %d%. Su saldo es de %n% %m%*/
           
  while($rowFacDat= mysqli_fetch_assoc($resultFacDat)){
    $corte = date("Y-m")."-".sprintf("%02d",(int)$rowFacDat['fecha_corte_dia']+1);    
    $queryFactPer = "SELECT total ".
                    "FROM facturacion_periodos ".
                    "WHERE facturacion_dato_id =".$rowFacDat['facturacion_id']." ".
                    "AND periodo_inicio='$corte' ORDER BY periodo_inicio DESC";
                    
    log_write("DEBUG: SAVE-BILLING-MANAGER: periodo de facturacion: ".$queryFactPer,9);
                    
    $resultFactPer = mysqli_query($db_modulos,$queryFactPer);
    
    if(!$resultFactPer){
      log_write("ERROR: SAVE-BILLING-MANAGER: No se pudo consultar el periodo de facturación",9);
    }
    else{
      $sms = array();
      $rowFacPer = mysqli_fetch_assoc($resultFactPer);
      
      if(1==$rowFacDat['notificaciones'])
        $msg = getMiscField('factCteMsgVig');
      elseif(2==$rowFacDat['notificaciones'])
        $msg = getMiscField('factCteMsgVenc');
      elseif(3==$rowFacDat['notificaciones'])
        $msg = getMiscField('factCteMsgRec');
      
      $fecVenc = sprintf("%02d",(int)$rowFacDat['fecha_corte_venc'])."-".date("m-Y");    
      
      $msg = str_replace("%n%",$rowFacPer['total'],$msg);
      $msg = str_replace("%d%",$fecVenc,$msg);
      $msg = str_replace("%m%",$rowFacDat['denominacion_abvr'],$msg);
      
      log_write("DEBUG: SAVE-BILLING-MANAGER: Mensaje a enviar: ".$msg,9);
      
      $sms['number'] = $rowFacDat['tel_notificacion'];
      $sms['msg'] = $msg; 
      $sms['hypermedia'] = '10.1.4.3';
      
      $msg_response = sendSms($sms);
      
      log_write("DEBUG: SAVE-BILLING-MANAGER: respuesta: ".$msg_response,9);
      
      if("PROCEEDING"!=$msg_response){
        $queryUpdMsg = "UPDATE facturacion_periodos SET notificaciones = notificaciones + 1 ".
                       "WHERE facturacion_dato_id =".$rowFacDat['facturacion_id'];
                       
        log_write("DEBUG: SAVE-BILLING-MANAGER: Actualizar Notificaciones query: ".$queryUpdMsg,9);               
                       
        $resultUpdMsg = mysqli_query($db_modulos,$queryUpdMsg);
        
        if(!$resultUpdMsg){
          log_write("ERROR: SAVE-BILLING-MANAGER: No se pudo actualizar en número de notificaciones SMS ",9);
        }
      }
    }
  }
}


mysqli_close($db_catalogos);
mysqli_close($db_modulos);
mysqli_close($db_usuarios);

?>
