<?php

set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

include_once "../libs/db/dbcommon.php";
include_once "../libs/db/log.php";

while(1){
  $db_modulos   = condb_modulos();
  
  $query = "SELECT tic.*, cli.nombre_comercial, pri.ticket_prioridad, sla.ticket_sla, sla.sla_horas ".
           "FROM ".$cfgTableNameMod.".tickets AS tic, ".$cfgTableNameMod.".clientes AS cli, ".$cfgTableNameCat.".cat_ticket_prioridad AS pri, ".$cfgTableNameCat.".cat_ticket_sla AS sla ".
           "WHERE cli.cliente_id=tic.cliente_id AND sla.ticket_sla_id=tic.ticket_sla_id AND pri.ticket_prioridad_id=tic.prioridad_id ".
           "AND tic.estatus!=4";
           
  $result = mysqli_query($db_modulos,$query);

  if(!$result){
    log_write("ERROR: TICKET MANAGER: Ocurrió un problema al obtenerse los datos de los tickets",6);
  }
  else{
    while($row = mysqli_fetch_assoc($result)){
      $fechaLim = strtotime($row['fecha_limite']);     
      $fechaNow = date("Y-m-d H:i:s");
       
      $tiempoRes = round(($fechaLim - strtotime($fechaNow))/3600);
      
      if(0>=$tiempoRes&&5!=$row['estatus']){
        log_write("DEBUG: TICKET MANAGER: se agotó el tiempo de espera se cambiará el estatus a atrasado",6); 
        $fechaNow = date("Y-m-d H:i:s");
        
        $query2 = "UPDATE tickets SET estatus=4, ultima_act='".$fechaNow."' WHERE ticket_id=".$row['ticket_id'];
        
        $result2 = mysqli_query($db_modulos,$query2);
        
        if(!$result2){
          log_write("ERROR: TICKET MANAGER: Ocurrió un problema al actualizarse el estatus del ticket con id ".$row['ticket_id'],6);
          log_write("DEBUG: TICKET MANAGER: ".$query2,6);
        }
        else{
          log_write("OK: TICKET MANAGER: Se actualizó el estatus a atrasado el ticket con id ".$row['ticket_id'],6);
          $tickJour = insertTicketJournal('Atraso de Ticket',$row['ticket_id'],'¡El ticket ha sido marcado como atrasado!',-1,4);
          sendTicketEmail($tickJour,6,"");
        }         
      }
    }
  }
  mysqli_close($db_modulos);
  sleep(60);
}
?>
