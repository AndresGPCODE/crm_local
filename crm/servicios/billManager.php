<?php
error_reporting(0);
ini_set('display_errors', 1);

set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

include_once "../libs/db/dbcommon.php";
include_once "../libs/db/common.php";

$cfgLogService = "../log/billingManager";

$db_catalogos = condb_catalogos();
$db_modulos   = condb_modulos();
$db_usuarios  = condb_usuarios();


$query = "SELECT cot.*, cli.*, usr.ubicacion_id ".
         "FROM ".$cfgTableNameMod.".cotizaciones AS cot, ".
                 $cfgTableNameMod.".clientes AS cli, ".
                 $cfgTableNameCat.".cat_coe_ubicacion AS ubi, ".
                 $cfgTableNameUsr.".usuarios AS usr ".
         "WHERE cli.cliente_id=cot.origen_id ".
         "AND (cot.tipo_origen=1 OR cot.tipo_origen=2) ".
         "AND usr.ubicacion_id=ubi.ubicacion_id ".
         "AND cot.usuario_alta=usr.usuario_id ".
         "AND cot.contrato=1 ".
         "ORDER BY cot.origen_id,cot.tipo_origen"; 

log_write("DEBUG: SAVE-BILLING-MANAGER: Inserción: ".$query,9);
         
$result = mysqli_query($db_modulos,$query);

if(!$result){
  log_write("ERROR: SAVE-BILLING-MANAGER: No se pudo consultar los datos de cotización \n".$query,9);
}
else{
  $subtotal = 0;
  $iva = 0;
  $total = 0;
  while($row = mysqli_fetch_assoc($result)){
    log_write("DEBUG: SAVE-BILLING-MANAGER: Cotización ID: ".$row['cotizacion_id'],9);
    
    $subtotal += $row['subtotal'];
    $iva += $row['impuesto'];
    $total += $row['total'];
        
    $queryFacDat = "SELECT facdat.*, feccot.fecha_corte_dia ".
                   "FROM ".$cfgTableNameMod.".facturacion_datos AS facdat, ".
                           $cfgTableNameCat.".cat_fechas_corte AS feccot ".
                   "WHERE facdat.origen_id=".$row['origen_id']." ".
                   "AND facdat.tipo_origen =".$row['tipo_origen']." ".
                   "AND feccot.fecha_corte_id=facdat.fecha_corte_id";
                   
    log_write("DEBUG: SAVE-BILLING-MANAGER: Cotización: ".$queryFacDat,9);  
                 
    $resultFacDat = mysqli_query($db_modulos,$queryFacDat);
    
    if(!$resultFacDat){
      log_write("ERROR: SAVE-BILLING-MANAGER: No se pudo obtener los datos de productos para esta cotización",9);
    }
    else{ 
      if(0>=mysqli_num_rows($resultFacDat)){
        log_write("DEBUG: SAVE-BILLING-MANAGER: No existen resultados de datos de facturación",9);     
      }
      else{
        while($rowFacDat = mysqli_fetch_assoc($resultFacDat)){
          $corte = date("Y-m")."-".sprintf("%02d",(int)$rowFacDat['fecha_corte_dia']+1);    
          $queryFactPer = "SELECT fact_period_id, periodo_inicio, servicios_extra ".
                          "FROM facturacion_periodos ".
                          "WHERE facturacion_dato_id =".$rowFacDat['facturacion_id']." ".
                          "AND periodo_inicio='$corte' ORDER BY periodo_inicio DESC";
                          
          $resultFactPer = mysqli_query($db_modulos,$queryFactPer);
          if(!$resultFactPer)
          {
            log_write("ERROR: SAVE-BILLING-MANAGER: Error al consultar los datos de pariodo de facturación ".$queryFactPer,9);
          } 
          elseif(0==mysqli_num_rows($resultFactPer)){
            $perfin = date('Y-m-d',strtotime('+1 month',strtotime($corte)));
            $queryFactPerIns = "INSERT INTO facturacion_periodos(facturacion_dato_id,periodo_inicio,periodo_fin,importe,iva,total,servicios_extra,fecha_alta) ".
            "VALUES(".$rowFacDat['facturacion_id'].",'".$corte."','".$perfin."',".$subtotal.",".$iva.",".$total.",'','".date('Y-m-d H:i:s')."')";
            
            log_write("DEBUG: SAVE-BILLING-MANAGER: Inserción: ".$queryFactPerIns,9);
            
            $resultFactPerIns = mysqli_query($db_modulos,$queryFactPerIns);
            
            if(!$resultFactPerIns){
              log_write("ERROR: SAVE-BILLING-MANAGER: Error al insertarse los datos de periodo de facturación",9);
            }
            else{
              log_write("OK: SAVE-BILLING-MANAGER: Se agregaron los datos nuevos de periodo de facturación",9);
            }
          }
          else{
            $rowFactPer = mysqli_fetch_assoc($resultFactPer);
            if((isset($rowFactPer['servicios_extra']))&&(0<strlen($rowFactPer['servicios_extra']))){
              $serExt = explode(",",$rowFactPer['servicios_extra']);
              $queryCatSerExt = "SELECT * FROM cat_servicios_extra WHERE ";
              foreach($serExt as $value){
                $queryCatSerExt .=" servicio_extra_id=".$value." OR";
              }
              
              $queryCatSerExt = substr($queryCatSerExt,0,-3);
              
              log_write("DEBUG: SAVE-BILLING-MANAGER: servicios extra: ".$queryCatSerExt,9);
             
              $resultCatSerExt = mysqli_query($db_catalogos,$queryCatSerExt);
             
              while($rowCatSerExt = mysqli_fetch_assoc($resultCatSerExt)){
                $subtotal = $subtotal+$rowCatSerExt['precio'];
              } 
            }
            else{
              log_write("DEBUG: SAVE-BILLING-MANAGER: servicios extra: Sin servicios extra",9);
            }
            $iva = $subtotal*0.16;
            $total = $subtotal+$iva;  
                   
            $queryFactPerUpd = "UPDATE facturacion_periodos SET importe=$subtotal, iva=$iva, total=$total WHERE facturacion_dato_id=".$rowFacDat['facturacion_id'];
            
            log_write("DEBUG: SAVE-BILLING-MANAGER: Actualización: ".$queryFactPerUpd,9);
            
            $resultfactPerUpd = mysqli_query($db_modulos,$queryFactPerUpd);
            
            if(!$resultfactPerUpd){
              log_write("ERROR: SAVE-BILLING-MANAGER: Error al actualizarse los datos de pariodo de facturación",9);
            }
            else{
              log_write("OK: SAVE-BILLING-MANAGER: Los datos de facturación fueron actualizados ",9);
            }
          }
        }
      }
    }
  }         
}         
//echo "QUeryfacper".$queryFactPer;         


mysqli_close($db_catalogos);
mysqli_close($db_modulos);
mysqli_close($db_usuarios);


?>
