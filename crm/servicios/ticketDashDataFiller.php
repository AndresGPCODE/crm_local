<?php
session_start();

set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

include_once "../libs/db/dbcommon.php";
include_once "../libs/db/log.php";

$cfgLogService = "../../log/ticketdashdatafiller";

while(1){
  $db_modulos   = condb_modulos();
  $db_catalogos = condb_catalogos();
  
  log_write("DEBUG: TICKET DASH DATA FILLER: Inicia operación",7); 

  $queryStat = "SELECT * FROM cat_ticket_estatus";
  
  $resultStat = mysqli_query($db_catalogos,$queryStat);
  
  if(!$resultStat){
    log_write("ERROR: TICKET DASH DATA FILLER: Ocurrió un problema al obtener los datos de los tipos de estatus",7);
  }
  else{
    $fechaNow = date("Y-m-d");
    log_write("DEBUG: TICKET DASH DATA FILLER: Fecha actual: ".$fechaNow,7);
    
    while($rowStat = mysqli_fetch_assoc($resultStat)){         
      //$queryTickNum = "SELECT COUNT(ticket_id) FROM tickets WHERE ultima_act>='".$fechaNow." 00:00:00' AND estatus=".$rowStat['ticket_estatus_id'];
      
      $queryTickNum = "SELECT COUNT(ticket_id) FROM tickets WHERE estatus=".$rowStat['ticket_estatus_id'];    
      
      $queryDashExist = "SELECT 1 FROM ticket_dashboard_data WHERE estatus=".$rowStat['ticket_estatus_id']." AND fecha='".$fechaNow."'";      
          
      $resultNum = mysqli_query($db_modulos,$queryTickNum);
      $resultDashExist = mysqli_query($db_modulos,$queryDashExist);
      
      log_write("DEBUG: TICKET DASH DATA FILLER: ".$queryTickNum,7);
      
      log_write("DEBUG: TICKET DASH DATA FILLER: ".$queryDashExist,7);
  
      if(!$resultNum||!$resultDashExist){
        log_write("ERROR: TICKET DASH DATA FILLER: Ocurrió un problema al obtener la cantidad de tickets con estatus ".$rowStat['estatus']." y fecha ".$fechaNow,7);
      }
      else{ 
        $numRowsTick = mysqli_fetch_row($resultNum); 
        
        log_write("DEBUG: TICKET DASH DATA FILLER: Número de registros: ".$numRowsTick[0],7);                
                  
        //if(0<$numRowsTick[0]){         
          if(0<mysqli_num_rows($resultDashExist)){          
            $query = "UPDATE ticket_dashboard_data SET cantidad=".$numRowsTick[0]." WHERE fecha='".$fechaNow."' AND estatus=".$rowStat['ticket_estatus_id'];
          }
          else{
            $query = "INSERT INTO ticket_dashboard_data(fecha,estatus,cantidad) VALUES('".$fechaNow."',".$rowStat['ticket_estatus_id'].",".$numRowsTick[0].")";          
          }
                   
          $result = mysqli_query($db_modulos,$query); 
        
          log_write("DEBUG: TICKET DASH DATA FILLER: Query: ".$query,7);
          
          if(!$result){
            log_write("ERROR: TICKET DASH DATA FILLER: Ocurrió un problema al insertarse/actualizarse los datos del numero de tickets",7);
          }
          else{
            log_write("OK: TICKET DASH DATA FILLER: Los datos del número de tickets con estatus ".$rowStat['estatus']." y fecha ".$fechaNow." se han agregado/actualizado",7);
          }                          
        //}
      }      
    }  
  }  
  mysqli_close($db_modulos);
  mysqli_close($db_catalogos);
  sleep(20);
}
?>
