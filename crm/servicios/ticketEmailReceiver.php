<?php
error_reporting(0);
ini_set('display_errors', 1);

set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

include_once "../libs/db/dbcommon.php";
include_once "../libs/db/common.php";

/*$cfghostname = "{mail.coeficiente.mx:143/service=imap}INBOX";
$cfgusername = "soporte@coeficiente.mx";
$cfgpassword = decrypt("aTlYbVZDL0hiN0NGQVl3TXdEM2NBdz09");*/

$cfghostname = "{cpanel.scutum.mx:143/novalidate-cert}INBOX";
$cfgusername = "tsanchez@ctclat.com";
$cfgpassword = decrypt("Y1k4KzMvRE8vakcreHJPUnhHTGJQZz09");

$cfgLogService = "../log/ticketEmailReceiver";

if($inbox = imap_open($cfghostname,$cfgusername,$cfgpassword)){
  log_write("OK: TICKET-EMAIL-RECEIVER: Se estableció conexión con el servidor de correo",9);
  
  parseTicketEmails($inbox);
  
  imap_close($inbox); 
  //echo("SERVER ROOT: ".var_dump($_SERVER)."\r\n");
  log_write("OK: TICKET-EMAIL-RECEIVER: Finalizó proceso se cerró la conexión",9); 
  /*while(1){

    sleep(60);
  }*/
}
else{
  log_write("ERROR: TICKET-EMAIL-RECEIVER: Ocurrió un problema al conectarse con el servidor de correo ".imap_last_error(),9);
}

//barre un mensaje y separa cada cachito para generar el $_POST que se enviará a saveticketnew para generar el ticket y gyardar sus archivos
function parseTicketEmails($inbox){
  log_write("DEBUG: PARSE-TICKET-EMAILS: *****INICIA PROCESO*****",9);
  global $cfgServerLocation;
  global $cfgLogService;
  $db_modulos = condb_modulos();
  $emails = imap_search($inbox,'UNSEEN');//fecha temporal
  $emails = array_reverse($emails);   
  
  $attachArr = array();
  $attachJson = "";
  
  $toreplacemsg = '#(<script(.*?)>(.*?)</script>)*?(<style(.*?)>(.*?)</style>)*?(<(/*?)html(.*?)>)*?(<(/*?)head(.*?)>)*?(<(/*?)body(.*?)>)*?(<div id=\'coe_sig\'>(.*?)</div>)*?#s';
  
  if(is_array($emails)){
    //log_write("DEBUG: PARSE-TICKET-EMAILS: emailsArr: ".print_r($emails,true),9);    
    foreach($emails as $email){    
      $header = imap_headerinfo($inbox,$email);
      log_write("DEBUG: PARSE-TICKET-EMAILS: HEADER: ".print_r($header,true),9);     
      $overview = imap_fetch_overview($inbox,$email);
      log_write("DEBUG: PARSE-TICKET-EMAILS: OVERVIEW: ".print_r($overview,true),9);
      $subject  = $overview[0]->subject;
      $to       = $overview[0]->to;
      $cc       = isset($header->cc) ? $header->cc : "";
      $cco      = isset($header->cco) ? $header->cco : "";
      $from     = $header->from;
      $fromMail = $from[0]->mailbox;
      $fromHost = $from[0]->host;
      
      $inReplyTo = isset($header->in_reply_to) ? $header->in_reply_to : "-1";
      $msgId     = isset($header->message_id) ? $header->message_id : "";
      
      $structure = imap_fetchstructure($inbox,$email);
      $flattenedParts = flattenParts($structure->parts);
      
      log_write("DEBUG: PARSE-TICKET-EMAILS: subject: ".print_r($subject,true),9); 
      log_write("DEBUG: PARSE-TICKET-EMAILS: from: ".print_r($from,true),9);
      log_write("DEBUG: PARSE-TICKET-EMAILS: to: ".print_r($to,true),9);
      log_write("DEBUG: PARSE-TICKET-EMAILS: cc: ".print_r($cc,true),9);
      log_write("DEBUG: PARSE-TICKET-EMAILS: cco: ".print_r($cco,true),9);
      log_write("DEBUG: PARSE-TICKET-EMAILS: inReplyTo: ".print_r($inReplyTo,true),9);
      log_write("DEBUG: PARSE-TICKET-EMAILS: msgId: ".print_r($msgId,true),9);
      
      $tickAssocArr = array();
      
      //agrega datos de remitente y otros destinatarios como asociados
      $tickAssocArr[] = array($fromMail,$fromMail."@".$fromHost,1);
      if(""!=$cc)
        foreach($cc as $key){
          if($key->mailbox!=$fromMail)
            $tickAssocArr[] = array($key->mailbox,$key->mailbox."@".$key->host,0);
        }
      if(""!=$cco)
        foreach($cco as $key){
          $tickAssocArr[] = array($key->mailbox,$key->mailbox."@".$key->host,0);
        }
        
      $tickAssocArr = json_encode($tickAssocArr);//convierte a JSON para su envio via ajax
      
      //log_write("DEBUG: PARSE-TICKET-EMAILS: FLATTENEDPARTS: ".print_r($flattenedParts,true),9);
      $count = 0;
      $filenamebase = md5(rand(100,200));
      foreach($flattenedParts as $partNumber => $part){     
	      switch($part->type){		
		      case 0:
			      // parte html
			      $message = getPart($inbox, $email, $partNumber, $part->encoding);			                  
            $message = trim(preg_replace($toreplacemsg,'',$message));
            $message = preg_replace('/\s\s+/', ' ', $message); 
            $msgEncoding = strtoupper(getParameterFromPart($part,'encoding'));

            if('UTF-8'!=$msgEncoding)
              $message = iconv($msgEncoding,'UTF-8',$message);          
		      break;	
		      case 1:
			      // headers
		      break;
		      case 2:
			      // headers de archivos adjuntos
		      break;	
		      case 3: // aplicación
		      case 4: // audio
		      case 5: // imagen
		      case 6: // video
		      case 7: // otro
			      $filename = getParameterFromPart($part,'filename');
			      $fileid = getParameterFromPart($part,'id');
			      $disposition = getParameterFromPart($part,'disposition');
			      
			      $filedata = explode(".",$filename);
			      $fileextension = $filedata[1];
			      
			      if($filename){
			        log_write("DEBUG: PARSE-TICKET-EMAILS: FILENAME: ".$filename,9);
				      $attachment = getPart($inbox, $email, $partNumber, $part->encoding);  
				      
				                              
				      $fName = "../docs/tickets/attachments/temp/".$filenamebase."_".$count.".".$fileextension;
              log_write("DEBUG: PARSE-TICKET-EMAILS: FNAME: ".$fName,9);
              
              if(false!==file_put_contents($fName,$attachment)){
                log_write("DEBUG: PARSE-TICKET-EMAILS: FNAME: Se guardó el archivo ".$fName,9); 
                $fileid = preg_replace('#(<*?)(>*?)#s','',$fileid);
                
                
                log_write("DEBUG: PARSE-TICKET-EMAILS: FNAME: Disposición de archivo ".$disposition,9);
                if('inline'==$disposition){
                  $message = str_replace("cid:".$fileid,$fName,$message);
                }
                elseif('attachment'==$disposition){
                  $attachArr[] = $fName;
                }
                else{
                  log_write("DEBUG: PARSE-TICKET-EMAILS: FNAME: El archivo no tiene una disposición defnida ",9);
                }
                
                /*if(!strpos($message,"cid:".$fileid)){
                  //debe de idicarse al ajax saveticketnew (common.php) que en vez de actualizar el src del codigo html que se guarde la ruta de imagen a la bd (tabla ticket_adjuntos)
                  
                }
                else{
                  $message = str_replace("cid:".$fileid,$fName,$message);
                }*/                             
              }
              else{
                log_write("DEBUG: PARSE-TICKET-EMAILS: FNAME: No se guardó el archivo ".$fName,9); 
              }
              $count++;             
			      }
			      else{
				      log_write("ERROR: PARSE-TICKET-EMAILS: No se identificó el tipo de dato",9);
			      }
		      break;	
	      }	
      }  
      
      $inReplyTo = trim(preg_replace('#<*>*#','',$inReplyTo));    
      
      $query = "SELECT post.ticket_id, post.mensaje FROM ticket_bitacora_posts AS post, rel_email_ticket AS rel ".
               "WHERE rel.email_id='".$inReplyTo."' ".
               "AND post.post_id=rel.post_id ";
               
      log_write("DEBUG: PARSE-TICKET-EMAILS: TicketID Query: ".$query,9); 
               
      $result = mysqli_query($db_modulos,$query);        
      
      //Se mandará a llamar las librerías adecuadas para el guardado del ticket
      if("-1"==$inReplyTo||!$result||0>=mysqli_num_rows($result)){//si no es respuesta se abre un ticket nuevo
        $attachJson = json_encode($attachArr,true);
        $url = $cfgServerLocation."crm/libs/db/common.php";
        
        log_write("DEBUG: PARSE-TICKET-EMAILS: No es respuesta se insertará un ticket nuevo",9);
        $data = array('action'=>'saveTicketNew',
                      'option'=>'0',
                      'remFlag'=>'1',
                      'imgfilename'=>"$filenamebase",
                      'cliId'=>'0',
                      'siteId'=>'0',
                      'ticketOrg'=>'2',
                      'ticketTopic'=>'0',
                      'ticketDepartment'=>'2',
                      'ticketSla'=>'3',
                      'tickAssign'=>'-1',
                      'tickTitle'=>"$subject",
                      'tickDetails'=>"$message",
                      'ticketPrior'=>'2',
                      'tickTopicOther'=>'',
                      'tickOrgOther'=>'',
                      'ticketReassign'=>'',
                      'tickAssocName'=>"$fromMail",
                      'tickAssocEmail'=>"$fromMail",
                      'ticketClientNotify'=>'1',
                      'ticketAssocArray'=>"$tickAssocArr",
                      'ticketStatus'=>'7',
                      'ticketAttachments'=>"$attachJson"
                ); 
      }
      else{//si es respuesta se agrega como réplica en un ticket ya abierto 
        $url = $cfgServerLocation."crm/modulos/tickets/ajax.php";
        
        log_write("DEBUG: PARSE-TICKET-EMAILS: Es una respuesta se insertará solo el post",9);     
        
        $message = str_replace("'","\"",$message);
        
        /*$query = "SELECT post.ticket_id, post.mensaje FROM ticket_bitacora_posts AS post, rel_email_ticket AS rel ".
                 "WHERE rel.email_id='".$inReplyTo."' ".
                 "AND post.post_id=rel.post_id ";
           
        log_write("DEBUG: PARSE-TICKET-EMAILS: TicketID Query: ".$query,9);       
                 
        $result = mysqli_query($db_modulos,$query);*/
        
        $row = mysqli_fetch_assoc($result);
        log_write("DEBUG: PARSE-TICKET-EMAILS: TicketID: ".$row['ticket_id'],9);
        
        //$message = str_replace($row['mensaje'],"",$message);
        
        $data = array('action'=>'saveReply',
                      'remFlag'=>'1',
                      'tickTitle'=>"$subject",
                      'ticketRepOrg'=>1,
                      'ticketId'=>encrypt($row['ticket_id']),
                      'ticketReply'=>"$message",
                      'assocArray'=>"$tickAssocArr",
                      'ticketAttachments'=>"$attachJson");
                      
        mysqli_close($db_modulos);
      }
      
      log_write("DEBUG: PARSE-TICKET-EMAILS: Url: ".$url,9); 

      $databuildt = http_build_query($data);//se parsean los datos enviados para que se puedan leer bien y bonito en la librería
      
      //equivalente a una llamada de ajax pero de php a php via CURL
      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL,$url);
      curl_setopt($ch, CURLOPT_HEADER, 1);
      curl_setopt ($ch, CURLOPT_HTTPHEADER, array('Content-type: application/x-www-form-urlencoded',
                                                  'Content-Length: '.strlen($databuildt)));
      curl_setopt($ch, CURLOPT_POST,1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,$databuildt);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);//espero recibir una respuesta!!

      $server_output = curl_exec($ch);
      
      log_write("DEBUG: PARSE-TICKET-EMAILS: Tickets result ".$server_output,9);

      curl_close($ch);                        
    }
  } 
  else{
    log_write("DEBUG: PARSE-TICKET-EMAILS: El arreglo de emails está vacío",9);
  } 
}

//Cortesía de php.net parsea la info de un email en un arreglo de arreglos con datos más digeridos
function flattenParts($messageParts, $flattenedParts = array(), $prefix = '', $index = 1, $fullPrefix = true){
  global $cfgLogService;
	foreach($messageParts as $part){
	  log_write("DEBUG: FLATTEN-PARTS: PART: ".print_r($part,true),9);
		$flattenedParts[$prefix.$index] = $part;
		if(isset($part->parts)){
			if($part->type == 2){
				$flattenedParts = flattenParts($part->parts, $flattenedParts, $prefix.$index.'.', 0, false);
			}
			elseif($fullPrefix){
				$flattenedParts = flattenParts($part->parts, $flattenedParts, $prefix.$index.'.');
			}
			else{
				$flattenedParts = flattenParts($part->parts, $flattenedParts, $prefix);
			}
			unset($flattenedParts[$prefix.$index]->parts);
		}
		$index++;
	}
	return $flattenedParts;			
}

//obtiene una porción específica del correo, ya sea un archivo adjunto, el texto, o una imagen embebida cortesía de php.net
function getPart($connection, $messageNumber, $partNumber, $encoding) {	
	$data = imap_fetchbody($connection, $messageNumber, $partNumber);	
	//log_write("DEBUG: GET-PART: DATA: ".print_r($data,true),9);
	
	switch($encoding) {
		case 0: return $data; // 7BIT
		case 1: return $data; // 8BIT
		case 2: return $data; // BINARY
		case 3: return base64_decode($data); // BASE64
		case 4: return quoted_printable_decode($data); // QUOTED_PRINTABLE
		case 5: return $data; // OTHER
	}	
}

//obtiene algún parámetro específico de una porción, por ahora el nombre del archivo, el id que tiene dentro del correo o la codificación del texto
function getParameterFromPart($part,$option){
	$output = '';	
	
	if('filename' == $option){
	  if($part->ifdparameters){
	    foreach($part->dparameters as $object){
			  if(strtolower($object->attribute) == 'filename'){			  
				  $output = isset($object->value)?$object->value:"";
			  }
		  }
	  }
	  if(!$output && $part->ifparameters){
	    foreach($part->parameters as $object){		
			  if(strtolower($object->attribute) == 'name'){				
				  $output = isset($object->value)?$object->value:"";
			  }
		  }
	  }
	}
	
	if('id' == $option){
	  if($part->ifdparameters){
	    $output = isset($part->id)?$part->id:"";
	  }
	  if(!$output && $part->ifparameters){
	    $output = isset($part->id)?$part->id:"";
	  }
	}
	
	if('encoding' == $option){
	  foreach($part->parameters as $object){
		  if(strtolower($object->attribute) == 'charset'){			  
			  $output = isset($object->value)?$object->value:"";
		  }
	  }
	}
	
	if('disposition' == $option){
	  if($part->disposition){
	    $output = isset($part->disposition)?$part->disposition:"";
	  }
	  else{
	    $output = "";
	  }
	}
	
	return $output;	
}


?>
