<?php
set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

include_once "../../libs/db/common.php";

$db_modulos = condb_modulos();
$db_catalogos = condb_catalogos();
$db_usuarios = condb_usuarios();


mysqli_close($db_modulos);
mysqli_close($db_catalogos);
mysqli_close($db_usuarios);

function prospectDelayNotif(){
  global $db_modulos;
  global $db_catalogos;
  
  $query = "SELECT pro.fecha_alta, pro.fecha_actualizacion, pro.estatus, ".
           "cvs.tiempo_limite, cvs.venta_estaus_id ".
           "FROM ".$cfgTableNameMod.".prospectos AS pro, ".
           $cfgTableNameCat.".cat_venta_estatus AS cvs, "
           .$cfgTableManeUsr.".usuarios AS usr ".
           "WHERE pro.estatus != 4 ".
           "AND (pro.asignadoa = usr.usuario_id OR pro.usuario_alta = usr.usuario_id) ".
           "AND pro.estatus=cvs.cat_venta_estatus";
  
  $query2 = "SELECT usr.nombre, usr.apellido ".
            "FROM ".$cfgTableManeUsr.".usuarios AS usr, ".
            $cfgTableManeUsr.".rel_grupo_permisos AS rgp, ".
            $cfgTableManeUsr.".rel_usuario_grupo AS rug ".
            "WHERE rug.grupo_id=rgp.grupo_id ".
            "AND usr.usuario";
           
   $result = mysqli_query($db_modulos,$query);     
   
   if(!$result){
     
   }
   else{
     $asunto = "Alerta de retraso de prospecto";
     $msg = "Hola ";
     $row = mysqli_fetch_assoc($result);
     
     $fecNow = date('Y-m-d H:i:s');
     $fecLim = date('Y-m-d H:i:s',strtotime($row['fecha_actualizacion']."+".$row['tiempo_limite']." hours"));
    
     $fecDif = round((strtotime($fecLim) - strtotime($fecNow))/3600);
     
     if($fecDif<=0){
       
       sendPrivMsg(0,$desId,$asunto,$msg,$tipo);
     }
   }   

}

?>
