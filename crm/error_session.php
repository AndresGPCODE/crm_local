<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CRM - Aviso Sesion</title>
    </title>
    <style>
        .centrado {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            -webkit-transform: translate(-50%, -50%);
            border-bottom: 3px solid red;
        }

        h2 {
            text-align: center;
        }

        button {
            width: 190px;
            height: 40px;
            border-radius: 5px;
            color: #fff;
            background-color: #337ab7;
            border-color: #2e6da4;
            margin-left: 50%;
            margin-right: 50%;
            transform: translate(-50%, -30%);
            -webkit-transform: translate(-50%, -30%);
        }
    </style>
</head>

<body>
    <div class="centrado">
        <img src="img/logo_coewhite.png" alt="logo">
        <h2> Ooops!!, la sesión ha expirado.</h2>
        <button onclick="log()">Regresar al LOGIN</button>
    </div>
    <script>
        function log() {
            location.href = "modulos/login/index.php?sub=timeout'";
        }
    </script>
</body>

</html>