<?php
session_start();
set_time_limit(0);


include_once "common.php";
include_once "encrypt.php";
require('../../libs/bookstores/fpdf181/fpdf.php');
require ('pdf.php');
//include_once "../fpdf/fpdf.php";
require_once '../mail/swiftmailer-master/lib/swift_required.php';

header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

$usuario = $_SESSION['usuario'];
$usuarioId = $_SESSION['usrId'];
$gruposArr = $_SESSION['grupos'];
$salida = "";
$row4;
$db_modulos = condb_modulos();
$db_catalogos = condb_catalogos();
$db_usuarios = condb_usuarios();

$opt = isset($_GET['option']) ? 0 : $_POST['procMailOpt']; //se valida GET porque si es visualizar nomás la cotización este valor llega via get. Si no llega significa que se eligió envío por correo

if (0 == $opt) {
    $pricId = isset($_GET['id']) ? decrypt($_GET['id']) : -1;   /* ojo se cachan los valores del common JS */
    $orgType = isset($_GET['orgType']) ? $_GET['orgType'] : -1;   /* ojo se cachan los valores del common JS */
    //echo " echo " . $pricId;
}

if (1 == $opt) {
    $contactEmails = array();
    $pricId = isset($_POST['cotId']) ? decrypt($_POST['cotId']) : -1;
    $orgType = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
    $contArr = isset($_POST['pricMailContact']) ? json_decode($_POST['pricMailContact']) : "";

    $asunto = isset($_POST['msgSubject']) ? $_POST['msgSubject'] : "Cotización"; //"Cotización para ".$row1['nombre_comercial'];
    $mensajeHtml = isset($_POST['msgHtmlContent']) ? $_POST['msgHtmlContent'] : ""; //"<h4><b>Envio cotización</b></h4>";
    $mensajePlano = isset($_POST['msgPlainContent']) ? $_POST['msgPlainContent'] : ""; //"Envio cotización";

    foreach ($contArr as $key => $value) {
        $contactEmails[$value[2]] = $value[1];
    }
}

if (-1 == $pricId || -1 == $orgType) {
    $salida = "ERROR||No se pudo obtener los datos de la cotización para su envío";
    mysqli_close($db_modulos);
    mysqli_close($db_catalogos);
    mysqli_close($db_usuarios); 
    echo $salida;
	return;
  }
  else {
    //$contactEmails = array("tsanchez@ctclat.com" => "Tania Sánchez");
    $pdf = new pdf();
    $pdf->AddPage();
	
    $subtotal = 0;
    $impuesto = 0;
    $totalCot = 0;
	
		switch ($orgType) {
			case 0:  //si se trata de un prospecto
				$dbname = "prospectos";
				$fieldname = "prospecto_id";
            $typename = "prospecto";
            $asignfield = "asignadoa";
            $nameFieldName = " ";
         //   echo "\n" . $cfgTableNameUsr . "\ncat\n" . $cfgTableNameCat . "\nname\n" . $dbname . "\n id hola\n" . $pricId;
            break;
        case 1:  //si se trata de un cliente
            $dbname = "clientes";
            $fieldname = "cliente_id";
            $typename = "cliente";
            $asignfield = "usuario_alta";
            $nameFieldName = " ";
            break;
        case 2:  //si se trata de un sitio
            $dbname = "sitios";
            $fieldname = "sitio_id";
            $typename = "sitio";
            $asignfield = "usuario_alta";
            $nameFieldName = "cli.nombre AS nombre_comercial, con.nombre AS nombre, ";
            break;
    }
 
    $query1 = "SELECT cot.*, cot.fecha_alta AS cotfecalta, con.*, cli.*, " . $nameFieldName . " ubi.*, usr.ubicacion_id " .
            "FROM " . $cfgTableNameMod . ".cotizaciones AS cot, " .
            $cfgTableNameMod . ".contactos AS con, " .
            $cfgTableNameMod . "." . $dbname . " AS cli, " .
            $cfgTableNameCat . ".cat_coe_ubicacion AS ubi, " .
            $cfgTableNameUsr . ".usuarios AS usr " .
            " WHERE cot.cotizacion_id=" . $pricId . " AND cot.contacto_id=con.contacto_id" .
            " AND cli." . $fieldname . "=cot.origen_id" .
            " AND cot.tipo_origen=" . $orgType .
            " AND usr.ubicacion_id=ubi.ubicacion_id" .
            " AND cot.usuario_alta=usr.usuario_id";

  	 
    $result1 = mysqli_query($db_modulos, $query1);
    if (!$result1) {
        $salida = "ERROR:Ocurrió un problema al consultar los datos de la cotización " . $query1;
        mysqli_close($db_modulos);
    mysqli_close($db_catalogos);
    mysqli_close($db_usuarios); 
    echo $salida;
	return;
    } else {
        $row1 = mysqli_fetch_assoc($result1);
        $orgId = $row1[$fieldname];
        $orgStat = $row1['estatus'];
        //$regPrice = $row1['region_precios']; 
        $pdf->printPriceHeader ($row1);
		$pdf->addPage();
        $query2 = "SELECT * FROM usuarios WHERE usuario_id=" . $row1[$asignfield];
        $result2 = mysqli_query($db_usuarios, $query2);
        if (!$result2) {
            $salida = "ERROR||Ocurrió un problema al obtener los datos del usuario ";
            mysqli_close($db_modulos);
    mysqli_close($db_catalogos);
    mysqli_close($db_usuarios); 
    echo $salida;
	return;
        } else {
            $row2 = mysqli_fetch_assoc($result2);
						
            $query3 = "SELECT rcp.relacion_id, cvs.*, cvv.cantidad_vigencia, cvv.factor, rcp.cantidad, " .
                    "rcp.producto_precio_especial, rcp.producto_autorizado_por, rcp.sitio_asignado, rcp.vigencia, cvts.categoria_id, cvc.imagen_ruta " .
                    "FROM " . $cfgTableNameCat . ".cat_venta_servicios AS cvs, " .
                    $cfgTableNameCat . ".cat_venta_tipo_servicios AS cvts, " .
                    $cfgTableNameMod . ".rel_cotizacion_producto AS rcp, " .
                    $cfgTableNameCat . ".cat_venta_vigencia AS cvv, " .
					$cfgTableNameCat . ".cat_venta_categorias AS cvc ".
                    "WHERE cvs.servicio_id = rcp.producto_id " .
                    "AND cvs.tipo_servicio = cvts.tipo_servicio_id " .
                    "AND rcp.vigencia = cvv.vigencia_id " .
					"AND cvts.categoria_id = cvc.categoria_id ".
                    "AND rcp.cotizacion_id = " .$pricId;    

            $result3 = mysqli_query($db_modulos, $query3);
 	        $result33 = mysqli_query($db_modulos, $query3);
 	
            if (!$result3) {
                $salida = "ERROR||No se pudo obtener los datos de los productos para esta cotización";
                mysqli_close($db_modulos);
				mysqli_close($db_catalogos);
				mysqli_close($db_usuarios); 
				//echo $salida;
				return;
            } else {
                $pdf->SetTextColor(0);
                $pdf->SetFont('Arial','B', 9);
				//imprime enlaces
				
            if (0 < mysqli_num_rows($result33)){
			      $pdf->segundahojapdf($result33);
			    }
			if (0 < mysqli_num_rows($result3)) {	    
			    //var_export($result3);
			    $TestImg=$result3;
				$pdf->terserahojapdf();
				$pdf->printPriceHeader1($row1, $result3);
				/* while ($row4 = mysqli_fetch_assoc($result3)){ 
				echo "hola".$row4['cantidad'];
        
	        //	var_export($row4);
                			 
				 /*       $bajada = str_replace("%n%", $row4['bajada'], $row4['descripcion']);
                        $desc = str_replace("%u%", $row4['subida'], $bajada);
                        $posx = $pdf->GetX();
                        $posy = $pdf->GetY();
						//echo json_encode($row4["descripcion"]);
                        $pdf->SetXY($posx + 20.5, $posy); //se recorre a la posición de la segunda columna
						$pdf->SetFont('Arial', 'IB', 9);
						
                        $pdf->MultiCell(95.5, 5, utf8_decode($desc), 'LRTB', 'L', false); //Imprime descripción
                        $pdf->SetFont('Arial','',9);
                        $cellHeight = $pdf->GetY() - $posy; //obtiene la altura de la celda de descripcion porque es dinámica
                        $pdf->SetXY($posx + 95.5 + 20.5, $posy); //se posiciona a la derecha de la celda de descripción
                        //imprime celdas de precios
					     
                        if(0 != $row4['producto_precio_especial']) {
							//$pdf->SetY(20,730);
                            $total = $row4['precio_lista'] * $row4['cantidad'];
                          //  $pdf->Cell(37, $cellHeight, utf8_decode("$ " . number_format($row4['precio_lista'], 2, '.', ',')), 'LRTB', 0, 'C', false);
                          //  $pdf->Cell(37, $cellHeight, utf8_decode("$ " . number_format($total, 2, '.', ',')), 'LRTB', 0, 'C', false);
						  
							} else {
                          //  $total = $row4['producto_precio_especial'] * $row4['cantidad'];
                          //  $pdf->Cell(37, $cellHeight, utf8_decode("$ " . number_format($row4['producto_precio_especial'], 2, '.', ',')), 'LRTB', 0, 'C', false);
                          //  $pdf->Cell(37, $cellHeight, utf8_decode("$ " . number_format($total, 2, '.', ',')), 'LRTB', 0, 'C', false);
						  }		
                        $subtotal += $total;
						//echo $subtotal;
                        //vuelve a la primera posición de la fila e imprime la primera celda (cantidad)
                        $pdf->SetXY($posx,$posy);//estan aqui original mente 
						//$pdf->SetY(30);
                        $pdf->SetFont('Arial', 'B', 9);
                        $pdf->Cell(20.5, $cellHeight, $row4['cantidad'],'LRTB', 1, 'C', false);  //aqui se cambio la cantidad_vigencia a cantidad
						//echo json_encode($row4['tipo_servicio']);
					*/
					/*echo $row4['categoria_id'];
							
						switch($row4['categoria_id']){
							case '1':
					     	break;
							
							case '2':
							$pdf->segundahojapdf('2',',');
							break;
							
							case '3':				
					     	$pdf->segundahojapdf('3',',');
							break;
							
							case '4':		
					     	$pdf->segundahojapdf('4',',');
                            break;
							
							case '5':
							$pdf->segundahojapdf('5',',');
							break;
							
							case '6':
					     	$pdf->segundahojapdf('6',',');
							break;
						//exit(0);
					}
			  	} */
				
					//$pdf->respuesta($result3);
				    //$pdf->printPriceHeader1($row1, $row666);
			
			/* while ($row666 = mysqli_fetch_assoc($result3)){
					 echo $row666['cantidad'];
					1
				 }
	    		*/	
					
					if (0 < mysqli_num_rows($result3)){
                        while ($row3 = mysqli_fetch_assoc($result3)) {						
                            $query = "SELECT prod.producto, prod.precio_unitario, 1 AS tipo_precio, 0 AS rango_min, " . $row3['cantidad'] . " AS rango_max " .
                                    "FROM " . $cfgTableNameCat . ".cat_productos AS prod " .
                                    "WHERE prod.tipo_servicio_id=" . $row3['producto_id'] .
                                    " UNION " .
                                    "SELECT 'producto' AS producto, dynPre.precio AS precio_unitario, 2 AS tipo_precio, dynPre.rango_min, dynPre.rango_max " .
                                    "FROM " . $cfgTableNameCat . ".cat_productos_precios_din AS dynPre " .
                                    "WHERE dynPre.tipo_producto=1 AND dynPre.producto_id=" . $row3['producto_id'] .
                                    " ORDER BY tipo_precio,rango_min";

                            //log_write("DEBUG: GET-PRODUCT-DATA: prodsquery ".$queryProdRan,4);
                            $posx = $pdf->GetX();
                            $posy = $pdf->GetY();

                            $pdf->SetXY($posx + 20.5, $posy); //se recorre a la posición de la segunda columna

                            $pdf->SetFont('Arial', '', 9);

                            if (1 != $row3['unico_pago']) {
                                $pdf->MultiCell(95.5, 5, utf8_decode($row3['producto']), 'LRTB', 'L', false); //Imprime descripción
                            }
                            $cellHeight = $pdf->GetY() - $posy; //obtiene la altura de la celda de descripcion porque es dinámica

                            $pdf->SetXY($posx + 95.5 + 20.5, $posy); //se posiciona a la derecha de la celda de descripción
                            //imprime celdas de precios
                            if (0 != $row3['producto_precio_especial']) {
                                $total = $row3['producto_precio_especial'] * $row3['cantidad'];
                                $pdf->Cell(37, $cellHeight, utf8_decode("$ " . number_format($row3['producto_precio_especial'], 2, '.', ',')), 'LRTB', 0, 'C', false);
                                $pdf->Cell(37, $cellHeight, utf8_decode("$ " . number_format($total, 2, '.', ',')), 'LRTB', 0, 'C', false);		
                            } else {
		//					    echo json_decode("hola mundo"+$row3['producto_id']);
                                $queryProdRan = "SELECT prod.producto, prod.precio_unitario, 1 AS tipo_precio, 0 AS rango_min, " . $row3['cantidad'] . " AS rango_max " .
                                        "FROM " . $cfgTableNameCat . ".cat_productos AS prod " .
                                        "WHERE prod.tipo_servicio_id=" . $row3['producto_id'] .
                                        " UNION " .
                                        "SELECT 'producto' AS producto, dynPre.precio AS precio_unitario, 2 AS tipo_precio, " .
                                        "dynPre.rango_min, dynPre.rango_max " .
                                        "FROM " . $cfgTableNameCat . ".cat_productos_precios_din AS dynPre " .
                                        "WHERE dynPre.tipo_producto=1 AND dynPre.producto_id=" . $row3['producto_id'] .
                                        " ORDER BY tipo_precio,rango_min";

                                //log_write("DEBUG: GET-PRODUCT-DATA: prodquery ".$queryProdRan,4);
                                //$row3['producto_id'];
                                $resultProdRan = mysqli_query($db_catalogos, $queryProdRan);
                                if (!$resultProdRan) {
                                    $salida = "ERROR||Ocurrió un problema al consultar los precios del producto";
                                } else {
                                    $totalFij = 0;
                                    $totalDyn = 0;
                                    $prevMaxRango = 0;
                                    $uPayProdArr = array();
                                      $pdf->SetY(120);
                                    while ($rowProdRan = mysqli_fetch_assoc($resultProdRan)) {
                                        $preUn = $rowProdRan['precio_unitario'];

                                        if (1 == $rowProdRan['tipo_precio'] && 1 >= mysqli_num_rows($resultProdRan)) {
                                            $totalFij = $rowProdRan['precio_unitario'] * $row3['cantidad'];
                                            $cant = $row3['cantidad'];
                                            $total = $totalFij;

                                            $pdf->Cell(37, $cellHeight, utf8_decode("$ " . number_format($preUn, 2, '.', ',')), 'LRTB', 0, 'C', false);
                                            $pdf->Cell(37, $cellHeight, utf8_decode("$ " . number_format($total, 2, '.', ',')), 'LRTB', 0, 'C', false);
                                        } else {
                                            if (2 == $rowProdRan['tipo_precio']) {
                                                if ($rowProdRan['rango_max'] < $row3['cantidad']) {
                                                    $cant = $rowProdRan['rango_max'] - $prevMaxRango;
                                                    $totalDyn = $rowProdRan['precio_unitario'] * $cant;
                                                    $prevMaxRango = $rowProdRan['rango_max'];
                                                } elseif ($row3['cantidad'] >= $rowProdRan['rango_min']) {
                                                    $cant = $row3['cantidad'] - $prevMaxRango;
                                                    $totalDyn = $rowProdRan['precio_unitario'] * $cant;
                                                } elseif ($row3['cantidad'] < $rowProdRan['rango_max']) {
                                                    break;
                                                }
                                                $total = $totalDyn;
                                                if (1 == $row3['unico_pago']) {
                                                    $uPayProdArr[] = array("cantidad" => $cant, "producto" => $row3['producto'], "precio_unitario" => $preUn, "total" => $total);
                                                } else {
                                                    $pdf->Cell(37, $cellHeight, utf8_decode("$ " . number_format($preUn, 2, '.', ',')), 'LRTB', 0,410,20,30, 'C', false);
                                                    $pdf->Cell(37, $cellHeight, utf8_decode("$ " . number_format($total, 2, '.', ',')), 'LRTB', 0,40,20,30, 'C', false);
                                                }
                                                log_write("DEBUG: GET-PRODUCT-DATA: tipoprecio=1 producto " . $row3['producto'] . " precio unitario " . $rowProdRan['precio_unitario'] . " cantidad " . $cant . " total " . $total, 4);
                                            }
                                        }
                                        //log_write("DEBUG: GET-PRODUCT-DATA: total acumulado ".$total,4); 
                                    }
                                    //vuelve a la primera posición de la fila e imprime la primera celda (cantidad)
                                    $pdf->SetXY($posx, $posy);
                                    $pdf->SetFont('Arial', 'B', 9);
                                    if (1 != $row3['unico_pago']) {
                                        $subtotal += $total;
                                        $pdf->Cell(20.5, $cellHeight, $cant, 'LRTB', 1, 'C', false);
											//$pdf->imagenes('1','2','3','4','5','6');		 
		
									 // $this->Image('../../img/coe_cotfooter.jpg', 20, 15, 60);  
									  }
								   //$pdf->Output();
                                }
                                /* else{
                                  $totalFij = 0;
                                  $totalDyn = 0;
                                  $prevMaxRango = 0;

                                  while($rowProdRan = mysqli_fetch_assoc($resultProdRan)){
                                  if(1==$rowProdRan['tipo_precio']){
                                  $prod = $rowProdRan['producto'];
                                  $totalFij = $rowProdRan['precio_unitario']*$row3['cantidad'];
                                  }
                                  else{
                                  if($rowProdRan['rango_max']<$row3['cantidad']){
                                  $totalDyn = $totalDyn+($rowProdRan['precio_unitario']*($rowProdRan['rango_max']-$rowProdRan['rango_min']));
                                  $prevMaxRango = $rowProdRan['rango_max'];
                                  }
                                  elseif($row3['cantidad']>=$rowProdRan['rango_min']){
                                  $totalDyn = $totalDyn+(($row3['cantidad']-$prevMaxRango)*$rowProdRan['precio_unitario']);
                                  }
                                  else{
                                  //no suma
                                  }
                                  }
                                  if(1>=mysqli_num_rows($resultProdRan))
                                  $total = $totalFij;
                                  else
                                  $total = $totalDyn;
                                  $preUn = $rowProdRan['precio_unitario'];
                                  //$leyPreUn .= "( de ".$rowProdRan['rango_min']." ".$rowProdRan['rango_max'].") ".$preUn;

                                  log_write("DEBUG: GET-PRODUCT-DATA: total acumulado ".$total,4);
                                  }

                                  $uPayProdArr = array();

                                  if(1==$row3['unico_pago']){
                                  $uPayProdArr[] = array("cantidad"=>$row3['cantidad'],"producto"=>$row3['producto'],"precio_unitario"=>$preUn,"total"=>$total);
                                  }
                                  else{
                                  $pdf->Cell(37,$cellHeight, utf8_decode("$ ".number_format($preUn, 2, '.', ',')),'LRTB', 0 , 'C', false );
                                  $pdf->Cell(37,$cellHeight, utf8_decode("$ ".number_format($total, 2, '.', ',')),'LRTB', 0 , 'C', false );
                                  }
                                  } */
                            }
                        }
                    }

                    //imprime tarifas telefónicas
 /*                   if(0 < mysqli_num_rows($result5)) {
                        while ($row5 = mysqli_fetch_assoc($result5)) {
                            $posx = $pdf->GetX();
                            $posy = $pdf->GetY();

                            $pdf->SetXY($posx + 20.5, $posy); //se recorre a la posición de la segunda columna

                            $pdf->SetFont('Arial', '', 9);
                            $pdf->MultiCell(95.5, 5, utf8_decode($row5['paquete_tel'] . " (minutos)"), 'LRTB', 'L', false); //Imprime descripción

                            $cellHeight = $pdf->GetY() - $posy; //obtiene la altura de la celda de descripcion porque es dinámica

                            $pdf->SetXY($posx + 95.5 + 20.5, $posy); //se posiciona a la derecha de la celda de descripción
                            //imprime celdas de precios
                            /* if(0!=$row5['telefonia_precio_especial']){
                              $pdf->Cell(37,$cellHeight, utf8_decode("$ ".number_format($row5['telefonia_precio_especial'], 2, '.', ',')." por minuto"),'LRTB', 0 , 'C', false );
                              $pdf->Cell(37,$cellHeight, utf8_decode("-"),'LRTB', 0 , 'C', false );
                              }
                              else{
                              $pdf->Cell(37,$cellHeight, utf8_decode("$ ".number_format($row5['precio_unitario'], 2, '.', ',')." por minuto"),'LRTB', 0 , 'C', false );
                              $pdf->Cell(37,$cellHeight, utf8_decode("-"),'LRTB', 0 , 'C', false );
                              } */
/*
                            if (0 != $row5['telefonia_precio_especial']) {
                                $total = $row5['telefonia_precio_especial'] * $row5['cantidad'];
                                $pdf->Cell(37, $cellHeight, utf8_decode("-"), 'LRTB', 0, 'C', false);
                                $pdf->Cell(37, $cellHeight, "$ " . number_format(($total), 2, '.', ','), 'LRTB', 0, 'C', false);
                            } else {
                                $total = $row5['precio_unitario'] * $row5['cantidad'];
                                $pdf->Cell(37, $cellHeight, utf8_decode("-"), 'LRTB', 0, 'C', false);
                                $pdf->Cell(37, $cellHeight, "$ " . number_format(($total), 2, '.', ','), 'LRTB', 0, 'C', false);
                            }
                            $subtotal +=$total;

                            //vuelve a la primera posición de la fila e imprime la primera celda (cantidad)
                            $pdf->SetXY($posx, $posy);
                            $pdf->SetFont('Arial', 'B', 9);
                            $pdf->Cell(20.5, $cellHeight, $row5['cantidad'], 'LRTB', 1, 'C', false);
                            //$pdf->SetXY($posx,$cellHeight);
                        }
                    }*/

                   /* $impuesto = $subtotal * 0.16;
                    $totalCot = $subtotal + $impuesto;
                    $pdf->printTotal($subtotal, $impuesto, $totalCot);
                    //imprime los datos adicionales que vienen con cada enlace
                    $posx = $pdf->GetX();
                    $posy = $pdf->GetY();*/

/*
                    $query6 = "SELECT DISTINCT catenl.* " .
                            "FROM " . $cfgTableNameMod . ".rel_cotizacion_enlace AS relce, " .
                            " " . $cfgTableNameCat . ".cat_enlaces AS catenl, " .
                            " " . $cfgTableNameCat . ".cat_enlaces_precios AS catenlpre " .
                            "WHERE relce.cotizacion_id =" . $row6['cotizacion_id'] . " " .
                            "AND relce.enlace_precio_id = catenlpre.enlace_precio_id " .
                            "AND catenl.enlace_id = catenlpre.enlace_id ORDER BY catenl.enlace_id ASC";

                    $result6 = mysqli_query($db_modulos, $query6);
					echo json_encode($query6);
                    while ($row6 = mysqli_fetch_assoc($result6)) {

					//echo json_encode("adion"+$row66);
					
                        $pdf->SetFont('Arial', 'B', 10);
                        $pdf->Cell(190, 5, utf8_decode($row6['enlace']), '', 1, 'L', false);
                        if ("" != $row6['router']) {
                            $pdf->printCotNote("Router", $row6['router']);
                        }
                        if ("" != $row6['hosting']) {
                            $pdf->printCotNote("Hosting", $row6['hosting']);
                        }
                        if ("" != $row6['vpn']) {
                            $pdf->printCotNote("VPN", $row6['vpn']);
                        }
                        if ("" != $row6['sla']) {
                            $pdf->printCotNote("SLA", $row666['sla']);
                        }
                        if ("" != $row6['disponibilidad']) {
                            $pdf->printCotNote("Disponibilidad", $row6['disponibilidad']);
                        }
                    }*/
/*
                    $contactEmails[$row2['email']] = $row2['nombre'] . " " . $row2['apellidos']; //añade al usuario como destinatario también
                    //imprime las notas y el footer
                    $posx = $pdf->GetX();
                    $posy = $pdf->GetY();

                    $pdf->SetXY($posx + 37, $posy); //se recorre a la posición de la segunda columna

                    $pdf->SetFont('Arial', 'I', 9);
                    $pdf->MultiCell(153, 5, utf8_decode($row1['notas']), 'LRTB', 'L', false); //Imprime notas

                    $cellHeight = $pdf->GetY() - $posy; //obtiene la altura de la celda de notas porque es dinámica

                    $pdf->SetXY($posx, $posy);
                    $pdf->SetFont('Arial', 'B', 9);
                    $pdf->Cell(37, $cellHeight, "Notas:", 'LRTB', 1, 'L', false); //imprime la celda que dice notas  
                    $pdf->printCotNote(utf8_decode("Plazo de Contratación"), $row1['cantidad_vigencia'] . " " . $row1['tiempo_vigencia']);

                    //imprime los datos del vendedor que hizo el prospecto
                    $pdf->SetTextColor(255, 255, 255);
                    $pdf->SetFont('Arial', 'B', 9);
                    $pdf->Cell(0, 5, utf8_decode("Servicio de Soporte: Tel. " . $supportServiceTel . " | " . $supportServiceMail), 'LRTB', 2, 'C', true);

                    $pdf->SetTextColor(0);

                    //imprime el footer y las promociones

                    $pdf->printPriceFooter($row2, $coeTel, $regPrice);

                    if (1 <= sizeof($uPayProdArr)) {
                        $subtotal = 0;
                        $pdf->addPage();
                        $pdf->printPriceHeader($row1);
                        $pdf->SetTextColor(0);
                        $pdf->SetFont('Arial', 'B', 9);
                        foreach ($uPayProdArr as $key => $value) {
                            $posx = $pdf->GetX();
                            $posy = $pdf->GetY();

                            $pdf->SetXY($posx + 20.5, $posy); //se recorre a la posición de la segunda columna

                            $pdf->SetFont('Arial', '', 9);
                            $pdf->MultiCell(95.5, 5, utf8_decode($value['producto']), 'LRTB', 'L', false); //Imprime descripción

                            $cellHeight = $pdf->GetY() - $posy; //obtiene la altura de la celda de descripcion porque es dinámica

                            $pdf->SetXY($posx + 95.5 + 20.5, $posy); //se posiciona a la derecha de la celda de descripción

                            $pdf->Cell(37, $cellHeight, utf8_decode("$ " . number_format($value["precio_unitario"], 2, '.', ',')), 'LRTB', 0, 'C', false);
                            $pdf->Cell(37, $cellHeight, utf8_decode("$ " . number_format($value["total"], 2, '.', ',')), 'LRTB', 0, 'C', false);

                            $pdf->SetXY($posx, $posy);
                            $pdf->SetFont('Arial', 'B', 9);
                            $pdf->Cell(20.5, $cellHeight, $value['cantidad'], 'LRTB', 1, 'C', false);

                            $subtotal +=$value["total"];
                        }
                        $impuesto = $subtotal * 0.16;
                        $totalCot = $subtotal + $impuesto;

                        $pdf->printTotal($subtotal, $impuesto, $totalCot);
                        $pdf->printPriceFooter($row2, $coeTel, $regPrice);
                    }*/
                }
            }
            //}  
        }
	    if (0 == $opt) //Salida al navegador directa
            $pdf->Output();
        if (1 == $opt) { //Envia por mail la cotización
            $filename = date("YmdHis") . $row1['cotizacion_id'] . "_" . $row1['nombre_comercial'];
            $filepath = realpath("../../docs/pdf/cotizaciones/");
            $pdfdoc = $pdf->Output($filepath . $filename, "S"); //crea un archivo
            // Configura con los datos del servidor de correo
            $transport = Swift_SmtpTransport::newInstance($cfgmailServer, $cfgmailport);
            $transport->setUsername($cfgmailuser);
            $transport->setPassword($cfgmailpass);

            //Crea el mailer con los datos del servidor de correos
            $mailer = Swift_Mailer::newInstance($transport);

            /* Register anti flood plugin
              The AntiFlood plugin is designed to help lessen the load on the HTTP server and the SMTP server. It can also be used to send out very large batches of emails when the SMTP server has restrictions in place to limit the number of emails sent in one go. */
            $mailer->registerPlugin(new Swift_Plugins_AntiFloodPlugin(100, 30));

            //Crea mensaje
            $message = Swift_Message::newInstance();
            $message->setTo($contactEmails);

            if (3 == $orgStat || 6 == $orgStat) {
                $mensajeHtml .= "<br><br>Para conocer nuestro aviso de privacidad requerimientos técnicos y aceptar la propuesta enviada, es posible haciendo click: <a href=\"http://localhost/crm/servicios/confirmnegociationacceptance.php?pricId=" . $_POST['cotId'] . "\">Aquí</a>";

                error_log("PRICEPDF pricePost " . $_POST['cotId']);
                error_log("PRICEPDF LINK: " . $mensajeHtml);
                //$mensajeHtml .= "<br><br>Para conocer nuestro aviso de privacidad requerimientos técnicos y aceptar la propuesta enviada, es posible haciendo click: <a href=\"http://localhost/crm/servicios/confirmnegociationacceptance.php?priId=".$pricId."&orgId=".$orgId."&orgType=".$orgType."\">Aquí</a>";
            }

            if (file_exists('../../img/signatures/' . $usuarioId . '.png')) {
                $sigPic = $message->embed(Swift_Image::fromPath('../../img/signatures/' . $usuarioId . '.png')); //here
                $mensajeHtml .= "<br><hr><img src='" . $sigPic . "' alt='firma'/>";
            }

            $message->setSubject($asunto);
            $message->setBody($mensajeHtml, 'text/html');
            $message->addPart($mensajePlano, 'text/plain');
            $message->setFrom($row2['email'], $row2['nombre'] . " " . $row2['apellidos']);
            $attachment = Swift_Attachment::newInstance($pdfdoc, $filename . ".pdf", 'application/pdf');

            //adjunta archivo
            $message->attach($attachment);

            // manda correo
            if (!$mailer->send($message, $failure)) {
                $salida = "ERROR||Ocurrió un problema al enviar el documento por correo electrónico";
            } else {
                foreach ($contactEmails as $key => $value) {
                    writeOnJournal($usuarioId, "Ha enviado un correo a nombre de [" . $row2['nombre'] . " " . $row2['apellidos'] . "] de la cotización con id: [" . $row1['cotizacion_id'] . "] al email [" . $key . "]");
                }
                //updateOriginStatus($orgType,6,$orgId);
                if (3 == $orgStat) {
                    updateOriginStatus($orgType, 6, $orgId);
                }
                $salida = "OK||El archivo se ha enviado con éxito";
            }
            //borra archivo que se envió y ya no se necesita
            unlink($filepath . $filename);
        }
    }
  }
echo $salida;
/*
$pdf = new fPDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',12);
for($i=1;$i<=40;$i++)
	//$pdf->Cell(0,10,$pdf->Image('../../img/hoja-blanca.jpg',0,0,$pdf->w,$pdf->h),0,0,'L',0);
    //$pdf->Cell(0,0,$pdf->Image('../../img/menu.jpg',6,-25,$pdf->w='200', $pdf->h='120'),0,0,'L',0);
$pdf->Output();*/
?>
