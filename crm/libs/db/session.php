<?php
include_once "dbcommon.php";
include_once "log.php";
//$timeoutTime = "15000"; //numero de segundos de timeout
$timeoutTime = "3600";
$cfgLogService = "../../log/loginlog";
//verifica que la sesión está activa y que no haya pasado el timeout establecido
/*function verifySession($session){
  global $timeoutTime;
  $db_usuarios = condb_usuarios();
  //log_write("DEBUG: LOGIN: Se verificará la sesión",5);   
  $fecha  = date("Y-m-d H:i:s");
  $status = false;
  
  if(null===session_id()||!isset($session['usrId'])){
    $result = false;
    log_write("ERROR: LOGIN: No existen variables de sesión",5);
  }
  else{
    $query = "SELECT * FROM sesiones WHERE usuario_id=".$session['usrId']." AND sesion_hash='".session_id()."'";    
    $result = mysqli_query($db_usuarios,$query);
    
    //log_write("DEBUG: LOGIN: ".$query,5);
    
    if(!$result){
      $status = false;
      log_write("ERROR: LOGIN: Ocurrió un problema al obtener los datos del usuario en la DB",5);
      //die("Error ".$query);
    }
    else{
      if(0>=mysqli_num_rows($result)){
        log_write("ERROR: LOGIN: No se encontraron datos del usuario en la DB",5);
        $status = false;
      }
      else{
        $antes = strtotime("-".$timeoutTime." second",strtotime($fecha));      
        $row = mysqli_fetch_assoc($result);     
        if(strtotime($row['fecha_hora'])<$antes){
          log_write("DEBUG: LOGIN: Se excedió el tiempo de timeout se va a desloguear",5);
          //die("ERROR ".$antes."||".$row['fecha_hora']);
          $status = false;
          mysqli_close($db_usuarios);
          //header("Location: ../../modulos/login/logout.php?sub='timeout'");
        }
        else{ 
          //log_write("OK: LOGIN: La sesión aun sigue activa",5);     
          $status = true;
        }        
      }        
    }   
  }  
  mysqli_close($db_usuarios);
  return $status;
}*/
function verifySession($session){
  global $timeoutTime;
  $fecha  = date("Y-m-d H:i:s");
  $status = false;
  if(null===session_id()||!isset($session['usrId'])){
    $status = false;
    log_write("ERROR: LOGIN: No existen variables de sesión",5);
  }
  else{
    $antes = strtotime("-".$timeoutTime." second",strtotime($fecha)); 
    $fecLog = strtotime($session['loginFecha']);
    if($fecLog<$antes){
      $status = false;
      log_write("DEBUG: LOGIN: Se excedió el tiempo de timeout, se va a desloguear",5);
    }
    else{
      $status = true;
    }
  }
  return $status;
}
?>