<?php
/*error_reporting(~0);
ini_set('display_errors', 1);*/
  date_default_timezone_set('America/Mexico_City');
  //valida si elmodulo que invoca a common ya inicializó las variables de sesión, esto con evitar spam en el log de apache
  if(!isset($_SESSION)){ 
    session_start();
  }
  /* Esta librería incluye funciones comunes que usan varios módulos y, además incluye el archivo dbcommon que contiene los datos de conexión a la db. Algunos archivos usan las funciones, otros lo llaman para peticiones de ajax*/
  //include_once "dbcommon.php";
  include_once "encrypt.php";
  include_once "session.php";
  include_once "log.php";
  //include_once "../mail/swiftmailer-master/lib/swift_required.php";
  /*******Gestión de datos de sesión y bitácora*********/  
  $gruposArr  = isset($_SESSION['grupos'])?$_SESSION['grupos']:"";
  $usuarioId  = isset($_SESSION['usrId'])?$_SESSION['usrId']:"";
  $usuario    = isset($_SESSION['usuario'])?$_SESSION['usuario']:"";
  $usrUbica   = isset($_SESSION['usrUbica'])?$_SESSION['usrUbica']:"";
  $nombre     = isset($_SESSION['usrNombre'])?$_SESSION['usrNombre']:"";
  $usrIDExt   = isset($_SESSION['nIDExtension'])?$_SESSION['nIDExtension']:"";
  $usrIDUsr   = isset($_SESSION['nIDUsuario'])?$_SESSION['nIDUsuario']:"";
  $usrEmail   = isset($_SESSION['usrEmail'])?$_SESSION['usrEmail']:"";
  $usrLoginHr = isset($_SESSION['loginFecha'])?$_SESSION['loginFecha']:"";
  $mailData = array('dest' => "",
                      'cc' => "",
                      'cco' => "",
                      'asunto' => "",
                      'msg' => "",
                      'adjunto1' => "",
                      'adjunto2' => "",
                      'adjunto3' => "",
                      'adjunto4' => "",
                      'adjunto5' => "",
                      'adjunto6' => "",
                      'adjunto7' => "",
                      'adjunto8' => "",
                      'adjunto9' => "",
                      'adjunto10' => "",
                      'adjunto11' => "",
                      'adjunto12' => "",
                      'adjunto13' => "",
                      'adjunto14' => "",
                      'adjunto15' => "",
                      'estatus' => "NO ENVIADO",
                      'fecha_envio' => "0000-00-00 00:00:00",
                      'nIDCorreo' => 1,
                      'folio' => "0",
                      'firma' => "",
                      'observaciones' => "",
                      'bestado' => "",
                      'remitente' => ""
                       );
//redirige a la pantalla de login en caso de timeout 
function logoutTimeout(){
  // header("Location: ../../error_session.php?sub=timeout");
  header("Location: ../../modulos/login/logout.php?sub=timeout");
} 
// if(!verifySession($_SESSION)){
//   logoutTimeout();
// }else{
  $cfgLogService = "../../log/commonlog";
  $coeTel = "01 33 22828282";//valor fijo no modificar
  $supportServiceTel='22828282 opción 2';// valor fijo no modificar
  $supportServiceMail='soporte@coeficiente.mx';// valor fijo no modificar
  $_POST["action"] = isset($_POST["action"]) ? $_POST["action"] : ""; 
  $_POST["hdn_eveAction"] = isset($_POST["hdn_eveAction"]) ? $_POST["hdn_eveAction"] : "";
  $cfgmailServer = 'mail.scutum.mx';
  $cfgmailport   = 587;
  $cfgmailuser   = 'prueba@scutum.mx';
  $cfgmailpass   = decrypt('cGUrekczYXhNZnJxTzEwcEQ2Mkg4UT09');
  $maxDocSize = 16000000; // tamaño máximo de documento 16mb
  //$maxDocSize = 62914560; // tamaño máximo de documento 60 mb
  $salida = "";
  $relMimeImg = array('image/png'=>'file-extension-png-icon.png',
                      'image/jpeg'=>'file-extension-jpeg-icon.png',
                      'image/gif'=>'file-extension-gif-icon.png',
                      'image/bmp'=>'file-extension-bmp-icon.png',
                      'image/tiff'=>'file-extension-tif-icon.png',
                      'application/pdf'=>'file-extension-pdf-icon.png',
                      'application/x-pdf'=>'file-extension-pdf-icon.png',
                      'application/vnd.ms-excel'=>'file-extension-xls-icon.png',
                      'application/vnd.ms-office'=>'file-extension-office-icon.png',
                      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'=>'file-extension-xls-icon.png',
                      'text/csv'=>'file-extension-xls-icon.png',
                      'text/comma-separated-values'=>'file-extension-xls-icon.png',
                      'application/csv'=>'file-extension-xls-icon.png',
                      'application/x-rar-compressed'=>'file-extension-rar-icon.png',
                      'application/zip'=>'file-extension-zip-icon.png',
                      'application/x-tar'=>'file-extension-tgz-icon.png',
                      'other'=>'file-extension-other-icon.png'
                      );
  
  //modId = id del módulo (0 prospectos, 1 clientes etc) perCod = código de permiso del grupo dentro del modulo (r leer, w escribir, d borrar etc)
  //Verifica si cierto grupo contenido dentro del arreglo de grupos del usuario tiene cierto permiso dentro de algún módulo especificado
  function hasPermission($modId,$perCod){  
    //$modId identificador del modulo
    //$perCod codigo del permiso a evaluar      
    global $gruposArr; 
    $cont = 0;
    foreach($gruposArr as $key => $value){
      if(array_key_exists($modId,$value)){
        if(strpos($value[$modId],$perCod)!==false){
          $cont++;
        }        
      }
    }
    if(0<$cont)
      return true;
    else
      return false;   
  }
  //verifica si el usuario loggeado forma parte de cierto grupo
  function isInGroup($gpoId){
    global $gruposArr; 
    $cont = 0;
    foreach($gruposArr as $key){
      if($key==$gpoId){
        $cont++;
      }
    }
    if(0<$cont)
      return true;
    else
      return false;
  }
  //convierte la cantidad de bytes devueltos por el sistema a un valor numérico (usado para carga de archivos)
  function return_bytes($val){
    $val = trim($val);
    $last = strtolower($val[strlen($val)-1]);
    switch($last){
        case 'g':
            $val *= 1024;
        case 'm':
            $val *= 1024;
        case 'k':
            $val *= 1024;
    }
    return $val;
  }
  //parsea los argumentos enviados a un hilo php via exec
  function parseArguments($argv){
    global $cfgLogService;      
    log_write("DEBUG: PARSE-ARGUMENTS: Argumentos a parsear"."<pre>".print_r($argv,true)."</pre>",4);
    
    $param = print_r($argv);
    $_ARG = array();
    foreach($argv as $arg){
      if(preg_match('/--([^=]+)=(.*)/',$arg,$reg)){
        $_ARG[$reg[1]] = $reg[2];
      }
      elseif(preg_match('/-([a-zA-Z0-9])/',$arg,$reg)){
        $_ARG[$reg[1]] = 'true';
      }
    }    
    log_write("DEBUG: PARSE-ARGUMENTS: Argumentos parseados".print_r($_ARG,true),4);
    return($_ARG);
  }
  //obtiene la diferencia entre dos arreglos multidimensionales deprecado por array_diff_assoc
  /*function array_multi_diff($arraya, $arrayb) {
    if(sizeof($arraya)<sizeof($arrayb)){
      $array1 = $arrayb;
      $array2 = $arraya;
    }
    elseif(sizeof($arraya)>sizeof($array2)){
      $array1 = $arraya;
      $array2 = $arrayb;
    }
    else{
      return null;
    }
    
    foreach ($array1 as $key1 => $value1) {
      if(in_array($value1, $array2)) {
        unset($array1[$key1]);
      }
    }
    return $array1;
  }*/
  function array_multi_diff($array1, $array2){
    $result = array();
    foreach($array1 as $key => $val) {
      if(isset($array2[$key])){
        if(is_array($val) && $array2[$key]){
          $result[$key] = array_multi_diff($val, $array2[$key]);
        }
      } 
      else {
         $result[$key] = $val;
      }
    }
    return $result;
  }
  function updateOriginStatus($modId,$estatus,$orgId){
    global $cfgLogService; 
    global $db_modulos;
    
    switch($modId){
      case 0:
        $tableName = "prospectos";
        $idField = "prospecto_id";
      break;
      case 1:
        $tableName = "clientes";
        $idField = "cliente_id";
      break;
      case 2:
        $tableName = "sitios";
        $idField = "sitio_id";
      break;
      default:
        $tableName = "";
      break;  
    }
    
    $now = date('Y-m-d H:i:s');
    
    $query = "UPDATE ".$tableName." SET estatus=".$estatus.", fecha_actualizacion='".$now."' WHERE ".$idField."=".$orgId;
    
    log_write("DEBUG: UPDATE-ORIGIN-STATUS: Query ".$query,4);
    
    $result = mysqli_query($db_modulos,$query);
    
    if(!$result){
      log_write("ERROR: UPDATE-ORIGIN-STATUS: No se pudo actualizar el estatus del ".$tableName." con ID ".$orgId,4);
      return false;
    }
    else{
      log_write("ERROR: UPDATE-ORIGIN-STATUS: Se actualizó el estatus del ".$tableName." con ID ".$orgId,4);
      return true;
    }    
  }
  function createClientFromProspect($orgId){
     $db_modulos = condb_modulos();
     $db_catalogos = condb_catalogos();
     $queryProspect = "SELECT * FROM coecrm_modulos.prospectos WHERE prospecto_id = $orgId";
     // log_write("DEBUG: UPDATE-ORIGIN-STATUS: Query ".$query,4);
     $resultProspect = mysqli_query($db_modulos,$queryProspect);
     if($resultProspect){
       $rowProspect = mysqli_fetch_assoc($resultProspect);
       $rowProspect['origen_otro'] = ($rowProspect['origen_otro'] != null)?$rowProspect['origen_otro']:0;
       $rowProspect['tel_movil'] = ($rowProspect['tel_movil'] != null)?$rowProspect['tel_movil']:0;
       $rowProspect['email'] = ($rowProspect['email'] != null)?$rowProspect['email']:'e@mail.com';
       $queryCliente = "INSERT INTO coecrm_modulos.clientes (razon_social, nombre_comercial, rfc_curp, tipo_persona, telefono, tel_movil, email, email_alt, estatus, origen, origen_otro, acta_constitutiva, fecha_acta, representante_id, domicilio_fiscal, colonia_fiscal, cp_fiscal, municipio_fiscal, estado_fiscal, fecha_alta, usuario_alta, descripcion) ".
       "values ('".$rowProspect['razon_social']."','".$rowProspect['nombre_comercial']."','".$rowProspect['rfc']."',".$rowProspect['tipo_persona'].",".$rowProspect['telefono'].",".$rowProspect['tel_movil'].",'".$rowProspect['email']. "','email@alt.com',0,".$rowProspect['origen'].",".$rowProspect['origen_otro']. ",'acta constitutiva aqui','0000-00-00 00:00:00',".$rowProspect['representante_id'].",'".$rowProspect['domicilio']."','".$rowProspect['colonia']."',".$rowProspect['cp']. ",'" . $rowProspect['municipio'] . "'," . $rowProspect['estado'] . ",'" . $rowProspect['fecha_alta'] . "'," . $rowProspect['usuario_alta'] . ",'" . $rowProspect['descripcion'] ."')";
        $resultCliente = mysqli_query($db_modulos,$queryCliente);
        if($resultCliente){
            // tomar el id del cliente insertado.
          $idNewClient = mysqli_insert_id($db_modulos);

            // tomar las cotizaciones del prospecto para migrarlas y relacionarlas con el cliente nuevo.
            // revisar bien este asunto antes de seguir
          $queryCot = "SELECT * FROM coecrm_modulos.cotizaciones WHERE origen_id = $orgId";
          $resultCot = mysqli_query($db_modulos,$queryCot);
          if($resultCot){
            while($row = $resultCot = mysqli_fetch_assoc($resultCot)){
              $queryCotInsert = "INSERT INTO coecrm_modulos.cotizaciones (fecha_alta, tipo_origen, origen_id, region_id, contacto_id, cotizacion_estatus_id, usuario_alta, subtotal, impuesto, total, notas, contrato, vigencia_id, vigencia_autorizado_por, fecha_actualizacion, email_hash, autorizado_por_id, motivo) 
              VALUES (". $row['fecha_alta']. "," . $row['tipo_origen'] . ",". $idNewClient.",". $row['region_id'].",". $row[''].",". $row[''].",". $row[''].",". $row[''].",". $row[''].",". $row[''].",". $row[''].",". $row[''].",". $row[''].",". $row[''].",". $row[''].",". $row[''].")";
              $queryCotInsert = mysqli_query($db_modulos, $queryCotInsert);
            }
          }

            // tomar el o los sitios del prospecto para migrarlos y asignarselo a el nuevo cliente.
          $querySite = "SELECT * FROM coecrm_modulos.sitios WHERE origen_id = $orgId";
          $resultSite = mysqli_query($db_modulos,$querySite);
          $resultSite = mysqli_fetch_assoc($resultSite);

            // tomar los contactos del sitio para este prospecto y asignarlos el nuevo cliente
          $queryCon = "SELECT cont.* ".
            "FROM coecrm_modulos.rel_origen_contacto AS rel, contactos AS cont, coecrm_catalogos.cat_departamentos AS dep, coecrm_catalogos.cat_puestos AS pue ".
            "WHERE rel.tipo_origen = 2 AND rel.origen_id= ".$orgId.
            " AND rel.contacto_id=cont.contacto_id".
            " ORDER BY cont.apellidos ASC";
          $resultCon = mysqli_query($db_modulos,$queryCon);
          $resultCon = mysqli_fetch_assoc($resultCon);

            // tomar las relaciones de los contactos y sus departamentos del sitio para este prospecto y asignarlos el nuevo cliente es el resto de la informacion del contacto.
          $queryRel = "SELECT rel.*".
            "FROM coecrm_modulos.rel_origen_contacto AS rel, contactos AS cont, coecrm_catalogos.cat_departamentos AS dep, coecrm_catalogos.cat_puestos AS pue ".
            "WHERE rel.tipo_origen = 2 AND rel.origen_id= ".$orgId.
            " AND rel.contacto_id=cont.contacto_id".
            " ORDER BY cont.apellidos ASC";
          $resultRel = mysqli_query($db_modulos,$queryRel);
          $resultRel = mysqli_fetch_assoc($resultRel);

          // var_dump($idNewClient,$resultCot, $resultSite,$resultCon, $resultRel);

          log_write("NEW: INSERT INTO CLIENT: Query ".$queryCliente,4);
          return true;
        }else{
          return false;
        }
     }else{
       return false;
     }
  }
  //funcion que ejecuta el hilo de envío de un email para tickets
  function sendTicketEmail($posId,$alertType,$arrAsocJson){
    global $cfgLogService;
    log_write("DEBUG: SEND-TICKET-EMAIL: Se ejecutará el hilo PostId[".$posId."] Tipo de alerta [".$alertType."]",4);
    $execParser = 'php ../../libs/db/sendticketemail.php '.
                  '--posId="'.$posId.'" '.
                  '--alertType="'.$alertType.'" '.
                  '--assocArrJson="'.$arrAsocJson.'" '.                  
                  '>/dev/null 2>&1 &';
                  
    log_write("DEBUG: SEND-TICKET-EMAIL: Exec parser: ".print_r($execParser,true),4);                  
    exec($execParser,$out);   
    log_write("DEBUG: SEND-TICKET-EMAIL: Exec output: ".print_r($out,true),4);
  }
  //funcion que obtiene cualquier campo del catálogo de campos misceláneos (incluyen msgs de texto a tel moviles)
  function getMiscField($name){
    global $db_catalogos;
    
    $query = "SELECT * FROM cat_campos_misc WHERE campo='".$name."'";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
      log_write("ERROR: GET-BILLING-MSG: Ocurrió un problema al obetener el campo misceláneo [".$name."]",9);
    }
    else{
      $row = mysqli_fetch_assoc($result);
      $valor = $row['valor'];
    }
    return $valor;
  }
   // Funcion que envia un SMS por medio de HTTP, como parametro recibe un arreglo llamado sms donde asociativamente se indicara forzosamente: hypermedia, numero, msg y opcionalmente: msg_id, grupo y send_to_sim
  // Retorna un string con la palabra PROCESSING o ERROR dependiendo de lo que hypermedia proceso
  function sendSms($sms){ 
    $curl_param = array('server_password' => "G4r4b4t0!", 'msg' => $sms['msg'], 'number' => $sms['number']); 
    foreach($sms as $clave => $valor){
      if($clave != "hypermedia")
        $curl_param[$clave] = $valor; 
    }
    $curl_handler = curl_init(); 
    curl_setopt($curl_handler,CURLOPT_URL,"http://".$sms['hypermedia']."/cb/sms_http.php");
    curl_setopt($curl_handler,CURLOPT_POST,1);
    curl_setopt($curl_handler,CURLOPT_POSTFIELDS,$curl_param);
    curl_setopt($curl_handler,CURLOPT_RETURNTRANSFER,1);
    $respuesta = curl_exec($curl_handler);
    curl_close($curl_handler);
    return $respuesta;
  }
  
  function sendBDEmail($mailData){
    $db_usuarios = condb_usuarios();
    global $usuarioId;
    $fecha = date('Y-m-d');
    $hora = date('H:i:s');
    $query = "INSERT INTO tbl_Email_Enviar(nIDUsuario,Fecha,Hora,Email,Cc,Cco,Asunto,Cuerpo,".
                                          "Archivo1,Archivo2,Archivo3,Archivo4,Archivo5,Archivo6,Archivo7,Archivo8,Archivo9,Archivo10,".
                                          "Archivo11,Archivo12,Archivo13,Archivo14,Archivo15,".
                                          "Estatus,FechaEnviado,nIDCorreo,Folio,Firma,FechaModificacion,FechaCreacion,".
                                          "Observaciones,bEstado,EmailRemitente) ".
             "VALUES (".$usuarioId.",'".
                        $fecha."','".
                        $hora."','".
                        $mailData['dest']."','".
                        $mailData['cc']."','".
                        $mailData['cco']."','".
                        $mailData['asunto']."','".
                        $mailData['msg']."','".
                        $mailData['adjunto1']."','".
                        $mailData['adjunto2']."','".
                        $mailData['adjunto3']."','".
                        $mailData['adjunto4']."','".
                        $mailData['adjunto5']."','".
                        $mailData['adjunto6']."','".
                        $mailData['adjunto7']."','".
                        $mailData['adjunto8']."','".
                        $mailData['adjunto9']."','".
                        $mailData['adjunto10']."','".
                        $mailData['adjunto11']."','".
                        $mailData['adjunto12']."','".
                        $mailData['adjunto13']."','".
                        $mailData['adjunto14']."','".
                        $mailData['adjunto15']."','".
                        $mailData['estatus']."','0000-00-00 00:00:00',".
                        $mailData['nIDCorreo'].",'".
                        $mailData['folio']."','".
                        $mailData['firma']."','".
                        $fecha."','".
                        $fecha.' '.
                        $hora."','".
                        $mailData['observaciones']."','".
                        $mailData['bestado']."','".
                        $mailData['remitente']."')";
    $result = mysqli_query($db_usuarios,$query);
    if(!$result){
      log_write("ERROR: SEND-DB-EMAIL: Error al insertar datos de email ".$query." ".mysqli_error($db_usuarios),4);
      $salida = "ERROR: No pudo almacenarse los datos del correo a enviar";
      return false;
    }
    else{
      log_write("OK: SEND-DB-EMAIL: Se insertaron los datos datos de email ".$query,4);
      $salida = "OK: Se han almacenado los datos del correo a enviar";
      return true;      
    }
  }
  function sendEstatusChangeEmail($id_estatus, $id_prospecto){
      // **************************************************+
      // **************************************************+
        $mailData = array('dest' => "",
                    'cc' => "",
                    'cco' => "",
                    'asunto' => "",
                    'msg' => "",
                    'adjunto1' => "",
                    'adjunto2' => "",
                    'adjunto3' => "",
                    'adjunto4' => "",
                    'adjunto5' => "",
                    'adjunto6' => "",
                    'adjunto7' => "",
                    'adjunto8' => "",
                    'adjunto9' => "",
                    'adjunto10' => "",
                    'adjunto11' => "",
                    'adjunto12' => "",
                    'adjunto13' => "",
                    'adjunto14' => "",
                    'adjunto15' => "",
                    'estatus' => "NO ENVIADO",
                    'fecha_envio' => "0000-00-00 00:00:00",
                    'nIDCorreo' => 1,
                    'folio' => "0",
                    'firma' => "",
                    'observaciones' => "",
                    'bestado' => ""
                      );
        $mailData['asunto'] = "Cambio de estado de Prospecto - CRM ";
        $estatusProspecto = array(
          1 => 'Prospeccion',
          2 => 'Cotizacion',
          3 => 'Propuesta',
          4 => 'Cierre',
          5 => 'Estudio',
          6 => 'Negociacion',
          10 => 'Prospecto finalizado',
        );
        $nombreorazon = getProspectNomRazon($id_prospecto);
        $asignadoA = get_userEmail(getProspectAsignado($id_prospecto));
        $mailData['dest'] = "htellez@coeficiente.mx,".$asignadoA;
        $mailData['msg'] = "Hola, El Prospecto con Numero: ". $id_prospecto." y con ".$nombreorazon." a cambiado a fase de ".$estatusProspecto[$id_estatus]."<br><br>Acceso al sistema CRM <a href=\"https://crm.coeficiente.mx/crm/modulos/login/index.php\">Aquí</a>";
        $mailData['remitente'] = "";
        sendBDEmail($mailData);
      // **************************************************+
      // **************************************************+
      //mail de cambio de estatus
      // return null;
  } 
if("readMsg" == $_POST['action']){

  $db_modulos = condb_modulos();
  $db_usuarios = condb_usuarios();
  $msgId = isset($_POST['msgId'])?decrypt($_POST['msgId']):"";
  
  $query = "SELECT * FROM mensajes WHERE mensaje_id=".$msgId;
  
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $salida = "ERROR||No se pudo obtener el cuerpo del mensaje";
  }
  else{
    $row = mysqli_fetch_assoc($result);
    
    if(0 == $row['leido']){
      $query2 = "UPDATE mensajes SET leido=1 WHERE mensaje_id=".$msgId;
      $result2 = mysqli_query($db_modulos,$query2);
    }
    
    $salida = "OK||";
    
    $salida .= /*"<div class=\"container\">".
               "  <button type=\"button\" id=\"btn_msgDelete_".encrypt($row['mensaje_id'])."\" title=\"Borrar Mensaje\" ".
               "class=\"btn btn-xs btn-danger btn-responsive\" onClick=\"deleteMsg('".encrypt($row['mensaje_id'])."');\">".
               "<span class=\"glyphicon glyphicon-remove\"> Eliminar</span></button>".
               "</div>".*/
               "<div class=\"container\">".
               "  <span><b>De:</b> <i>".get_userRealName($row['remitente_id'])."</i></span>".
               "</div>".
               "<div class=\"container\">".
               "  <span><b>Asunto:</b> <i>".$row['asunto']."</i></span>".
               "</div>".
               "<div class=\"container\">".
               "  <span><b>Feha de Envío:</b> <i>".$row['fecha_hora']."</i></span>".
               "</div>".
               "<hr>".
               "<div style=\"overflow-y:auto; min-height:300px; max-height:800px;\">".
               "  <span><b>Mensaje:</b><br> ".
               $row['mensaje']."</span>".
               "</div>";
  }
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}
  //llamada de ajax procesa una petición de autorizacion para varios modulos, por ejemplo guardar una cotización con precios personalizados
  if("sendAuthReq" == $_POST["action"]){
  
    log_write("DEBUG: SEND-AUTH-REQ: se solicitará autorización",4);
  
    $usrName = isset($_POST['usrName']) ? $_POST['usrName'] : "";
    $usrPass = isset($_POST['usrPass']) ? $_POST['usrPass'] : "";
    $option  = isset($_POST['option'])  ? $_POST['option']  : "";//cotización,cliente,prospecto,etc
    
    $db_usuarios = condb_usuarios();
  
    $query = "SELECT usr.usuario_id, rel.grupo_id, gr.grupo, per.modulo_id, per.permiso FROM usuarios as usr, rel_usuario_grupo AS rel, grupos AS gr, rel_grupo_permisos AS per WHERE rel.grupo_id=gr.grupo_id AND per.grupo_id=gr.grupo_id AND usr.username=? AND contrasena=? AND rel.usuario_id=usr.usuario_id";
    
    $stmt1 = mysqli_prepare($db_usuarios,$query);
  
    mysqli_stmt_bind_param($stmt1,"ss",$usrName,encrypt($usrPass));
    
    $result = mysqli_stmt_execute($stmt1);

    //$result = mysqli_query($db_usuarios,$query);
    
    log_write("DEBUG: SEND-AUTH-REQ: query ".$query,4);
    
    if(!$result){
      $salida = "ERROR|| Ocurrió un problema al consultar los datos del usuario que autoriza.";
    }
    else{
      mysqli_stmt_store_result($stmt1);   
      if(0==mysqli_stmt_num_rows($stmt1)){
        $salida = "ERROR|| Los datos del usuario son incorrectos.";
      }
      else{
        $groupArr = array();
        mysqli_stmt_bind_result($stmt1,$usrId,$gpoId,$gpo,$modId,$permission);          
        while(mysqli_stmt_fetch($stmt1)){
          $authUsrId = $usrId;            
          $groupArr[$gpoId][$modId] = $permission;                   
        }
        log_write("DEBUG: SEND-AUTH-REQ: grupos y permisos del usuario autorizador"."<pre>".print_r($groupArr,true)."</pre>",4);
        
        $cont = 0;      
        foreach($groupArr as $key => $value){
          if(array_key_exists($option,$value)){
            if(strpos($value[$option],'a')!==false){
              $cont++;
            }
          } 
        }        
        if(0<$cont){
          log_write("OK: SEND-AUTH-REQ: el usuario [".$authUsrId."] autorizó la operación",4);
          $salida = "OK|| Proceso autorizado||".$authUsrId;
        }
        else{
          log_write("ERROR: SEND-AUTH-REQ: el usuario [".$authUsrId."] intentó autorizar la operación pero no tiene permisos",4);
          $salida = "ERROR|| El usuario no cuenta con los permisos necesarios";
        }              
      }     
    }
    autha:
    echo $salida;
    mysqli_close($db_usuarios);
  }
  if("callMarcatron" == $_POST["action"]){
    $number = isset($_POST['number']) ? $_POST['number'] : "";
    $msg = isset($_POST['msg']) ? $_POST['msg'] : "";
    $tipo = isset($_POST['type']) ? $_POST['type'] : "";
    
    $db_usuarios = condb_usuarios(); 
   
    $hoy = date('Y-m-d H:i:s');
   
    if("TELEFONO"==$tipo)
      $number = substr($number,2,10);
    
    $queryIns = "INSERT INTO tbl_comandos(Telefono,nIDUsuario,nIDExtension,Estatus,".
                                      "FechaModificacion,FechaCreacion,Observaciones,bEstado,Tipo,Mensaje) ".
                "VALUES(".$number.",".$usrIDUsr.",".$usrIDExt.",'NO MARCADO','".$hoy."','".$hoy."',".
                        "'ESTADO INICIAL',0,'".$tipo."','".$msg."')";
    
    $resultIns = mysqli_query($db_usuarios,$queryIns);
   
    if(!$resultIns){
      $salida = "FALSE";
      log_write("ERROR: CALL-PHONE: No se logró hacer el contacto con ".$nombre,4);
    }
    else{
      $salida = "TRUE";
      log_write("OK: CALL-PHONE: Se está realizando la comunicación tipo ".$tipo." a ".$nombre,4);
    }   
    mysqli_close($db_usuarios);
    echo $salida; 
  }
  /*********Obtención de datos de texto a base de sus ids usados para despliegue de información**********/
  //función que obtiene el nombre real del usuario, se usa para despliegue de datos de clientes, prospectos o sitios etc.
  function get_userRealName($usuarioId){
    $db_usuarios = condb_usuarios();
    
    $query = "SELECT nombre, apellidos FROM usuarios WHERE usuario_id=".$usuarioId;    
    $result = mysqli_query($db_usuarios,$query);
    
    if(!$result){
      $nombre = "N/A";
    }
    else{
      $row = mysqli_fetch_row($result);
      
      $nombre = $row[0]." ".$row[1];
    }
    mysqli_close($db_usuarios);
    return $nombre;
  }
    //función que obtiene el correo electronico del usuario, se usa para despliegue de datos de clientes, prospectos o sitios etc.
  function get_userEmail($usuarioId){
    $db_usuarios = condb_usuarios();
    
    $query = "SELECT username FROM usuarios WHERE usuario_id=".$usuarioId;    
    $result = mysqli_query($db_usuarios,$query);
    
    if(!$result){
      return false;
    }
    else{
      $row = mysqli_fetch_row($result);
      $correo = $row[0];
      mysqli_close($db_usuarios);
      return $correo;
    }
    
  }
  //obtiene el estatus de venta (contactado, no le interesa, contrato firmado etc) para prospectos puesto aquí por si las dudas
  function get_sellStat($statId){
    $db_catalogos = condb_catalogos();
    $query = "SELECT estatus FROM cat_venta_estatus WHERE venta_estatus_id=".$statId;
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
      $estatus = "N/A";
    }
    else{
      $row = mysqli_fetch_row($result);
      
      $estatus = $row[0];
    }
    mysqli_close($db_catalogos);
    return $estatus;
  }
  //obtiene el origen de ventas seleccionado en un cliente/prospecto para su despliegue en pantallla.
  function get_sellOrigin($statId){
    $db_catalogos = condb_catalogos();
    $query = "SELECT venta FROM cat_venta_origen WHERE venta_origen_id=".$statId;
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
      $origen = "N/A";
    }
    else{
      $row = mysqli_fetch_row($result);
      
      $origen = $row[0];
    }
    mysqli_close($db_catalogos);
    return $origen;
  }
  //obtiene el estado seleccionado pata un cliente/prospecto/sitio etc. para su despliegue en pantalla
  function get_business($businessId){
    $db_catalogos = condb_catalogos();
    $query = "SELECT giro_descripcion FROM cat_giro_comercial WHERE giro_id=".$businessId;
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
      $giroCom = "N/A";
    }
    else{
      $row = mysqli_fetch_row($result);
      
      $giroCom = $row[0];
    }
    mysqli_close($db_catalogos);
    return $giroCom;
  }
  //obtiene el estado seleccionado pata un cliente/prospecto/sitio etc. para su despliegue en pantalla
  function get_state($stateId){
    $db_catalogos = condb_catalogos();
    $query = "SELECT estado FROM cat_estados WHERE estado_id=".$stateId;
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
      $estado = "N/A";
    }
    else{
      $row = mysqli_fetch_row($result);
      
      $estado = $row[0];
    }
    mysqli_close($db_catalogos);
    return $estado;
  }
  //obtiene el pais seleccionado pata un cliente/prospecto/sitio etc. para su despliegue en pantalla
  function get_country($countryId){
    $db_catalogos = condb_catalogos();
    $query = "SELECT pais FROM cat_paises WHERE pais_id=".$countryId;
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
      $pais = "N/A";
    }
    else{
      $row = mysqli_fetch_row($result);
      
      $pais = $row[0];
    }
    mysqli_close($db_catalogos);
    return $pais;
  }
  function get_personType($personId){
    if(1==$personId)
      return "Física";
    elseif(2==$personId)
      return "Moral";      
  }
  //obtiene el estatus de cotización (creado, entregado, revisado, autorizado etc) para su despliegue
  function get_priceStat($statId){
    $db_catalogos = condb_catalogos();
    $query = "SELECT estatus FROM cat_cotizacion_estatus WHERE cotizacion_estatus_id=".$statId;
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
      $estatus = "N/A";
    }
    else{
      $row = mysqli_fetch_row($result);
      
      $estatus = $row[0];
    }
    mysqli_close($db_catalogos);
    return $estatus;
  }
  //obtiene el tipo de documento (acta constitutiva, comprobante de domicilio, ife) para su despliegue
  function get_docType($docTypeId){
    $db_catalogos = condb_catalogos();
    $query = "SELECT tipo_documento FROM cat_tipo_documento WHERE tipo_documento_id=".$docTypeId;
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
      $estatus = "N/A";
    }
    else{
      $row = mysqli_fetch_row($result);
      
      $estatus = $row[0];
    }
    mysqli_close($db_catalogos);
    return $estatus;
  }
  //obtiene el estatus de actividad de un enlace de sitio (activo, inactivo)
  function get_siteConActive($siteConActiveId){
    $db_catalogos = condb_catalogos();
    $query = "SELECT activo_estado FROM cat_enlace_activo WHERE enlace_activo_id=".$siteConActiveId;
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
      $estatus = "N/A";
    }
    else{
      $row = mysqli_fetch_row($result);
      
      $estatus = $row[0];
    }
    mysqli_close($db_catalogos);
    return $estatus;
  }
  //obtiene la topología de un enlace de sitio (punto a punto, multipunto)
  function get_siteConTopology($siteTopologyId){
    $db_catalogos = condb_catalogos();
    $query = "SELECT topologia FROM cat_enlace_topologias WHERE topologia_id=".$siteTopologyId;
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
      $estatus = "N/A";
    }
    else{
      $row = mysqli_fetch_row($result);
      
      $estatus = $row[0];
    }
    mysqli_close($db_catalogos);
    return $estatus;
  }
  //obtiene el tipo de servicio de un enlace de sitio (internet, vpbx etc)
  function get_serviceType($serviceTypeId){
    $db_catalogos = condb_catalogos();
    $query = "SELECT tipo_servicio FROM cat_tipo_servicios WHERE tipo_servicio_id=".$serviceTypeId;
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
      $estatus = "N/A";
    }
    else{
      $row = mysqli_fetch_row($result);
      
      $estatus = $row[0];
    }
    mysqli_close($db_catalogos);
    return $estatus;
  }
  //obtiene el nivel de prioridad de un ticket (alta, baja, emergencia etc)
  function get_ticketPriority($priorityId){
    $db_catalogos = condb_catalogos();
    $query = "SELECT ticket_prioridad FROM cat_ticket_prioridad WHERE ticket_prioridad_id=".$priorityId;
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
      $estatus = "N/A";
    }
    else{
      $row = mysqli_fetch_row($result);
      
      $estatus = $row[0];
    }
    mysqli_close($db_catalogos);
    return $estatus;
  }
  function sendPrivMsg($remId,$desId,$asunto,$msg,$tipo){
    $db_modulos = condb_modulos();
    $hoy = date('Y-m-d H:i:s');
    
    $query = "INSERT INTO mensajes (remitente_id,destinatario_id,fecha_hora,asunto,mensaje,leido,tipo_mensaje) ".
             "VALUES (".$remId.",".$desId.",'".$hoy."','".$asunto."','".$msg."',0,".$tipo.")";
          
    $result = mysqli_query($db_modulos,$query);
    
    if(!$result){
      log_write("ERROR: SAVE-EVENT: No se envió mensaje al usuario ".$query,4);
      $status = false;
    }
    else{
      $status = true;
    }   
    mysqli_close($db_modulos);
    return $status;
  }
  function sendEmailMsgDepartment($areaId,$regionId,$mailData){
    $db_usuarios = condb_usuarios();
    $destMails = "";
    
    if(0!=$regionId){
      $regAdd = " AND usr.ubicacion_id = ".$regionId;
    }
    else{
      $regAdd = "";
    }
    
    $query = "SELECT usr.email ".
             "FROM rel_usuario_grupo AS rug, ".
             "usuarios AS usr ".
             "WHERE rug.grupo_id = ".$areaId.
             " AND rug.usuario_id=usr.usuario_id".$regAdd;
             
    $result = mysqli_query($db_usuarios,$query);
    
    log_write("ERROR: SEND-EMAIL-MSG-DEPARTMENT: Consulta".$query,4);
    
    if(!$result){
      log_write("ERROR: END-EMAIL-MSG-DEPARTMENT: No se obtuvo listado de usuarios para el envio de mensajes",4);
      $status = false;
    }
    else{
      while($row = mysqli_fetch_assoc($result)){
        $destMails .= $row['email'].",";
      }
      $mailData['dest'] = substr($destMails,1,-1);
      
      if(!sendBDEmail($mailData)){
        log_write("ERROR: Ocurrió un problema al enviarse los mensajes al departamento por correo electrónico ",4);
        $status = false;
      }
      else{
        log_write("OK: El correo se ha enviado",4); 
        $status = true;     
      }
    }
    return $status; 
    mysqli_close($db_usuarios);
  }
  function sendPrivMsgDepartment($remId,$areaId,$regionId,$asunto,$msg,$tipo,$usrctrl){//1 mensajeria interna 2 email
    $db_usuarios = condb_usuarios();
    if(0!=$regionId){
      $regAdd = " AND usr.ubicacion_id = ".$regionId;
    }
    else{
      $regAdd = "";
    }
    $query = "SELECT usr.usuario_id ".
             "FROM rel_usuario_grupo AS rug, ".
             "usuarios AS usr ".
             "WHERE rug.grupo_id = ".$areaId.
             " AND rug.usuario_id=usr.usuario_id".$regAdd;
    
    $result = mysqli_query($db_usuarios,$query);
    log_write("ERROR: SAVE-PRIV-MSG-DEPARTMENT: Consulta".$query,4);
    if(!$result){
      log_write("ERROR: SAVE-PRIV-MSG-DEPARTMENT: No se obtuvo listado de usuarios para el envio de mensajes",4);
      $status = false;
    }
    else{
      if($usrctrl == 7){
        //envio directo a hector
        sendPrivMsg($remId,$usrctrl,$asunto,$msg,$tipo);
      }elseif($usrctrl == 69){    
      //  envio a pablo y aldrin que tienen el estatus de directivo
        while($row = mysqli_fetch_assoc($result)){
          if($row['usuario_id'] != 7){
            sendPrivMsg($remId,$row['usuario_id'],$asunto,$msg,$tipo);
          }
        }  
      }else{
        // envio a otros responsables (jorge o gera dependiendo lo que se necesite)
          while($row = mysqli_fetch_assoc($result)){
          sendPrivMsg($remId,$row['usuario_id'],$asunto,$msg,$tipo);
        }
      }     
      $status = true;
    }
    return $status;
    mysqli_close($db_usuarios);
  }
  function sendPrivEmail($remId,$desId,$asunto,$msg){
    $db_usuarios = condb_usuarios();
    global $cfgmailServer;
    global $cfgmailport;
    global $cfgmailuser;
    global $cfgmailpass;
    global $usrEmail;
    global $nombre;
    global $usuarioId;
    
    if(0!=$remId){
      $remEmail = $usrEmail;
      $remNom = $nombre;
    }
    else{
      $remEmail = $cfgmailuser;
      $remNom = "Sistema de CRM Coeficiente";
    }
    
    $queryDes = "SELECT nombre,apellidos,email FROM usuarios WHERE usuario_id = ".$desId;
    
    $resultDes = mysqli_query($db_usuarios,$queryDes);
    
    if($resultDes){
      $rowDes = mysqli_fetch_assoc($resultDes);
      
      $contactEmails = array($rowDes['email'] => $rowDes['nombre']." ".$rowDes['apellidos']);
      
      $transport = Swift_SmtpTransport::newInstance($cfgmailServer, $cfgmailport);
      $transport->setUsername($cfgmailuser);
      $transport->setPassword($cfgmailpass);
      $mailer = Swift_Mailer::newInstance($transport);
      $message = Swift_Message::newInstance();
      
      $message->setSubject($asunto);
      $message->setBody($msg,'text/html');
      $message->setFrom($remEmail, $remNom);
      $message->setTo($contactEmails);
      
      if(!$mailer->send($message,$failure)){
        $salida = "ERROR||Ocurrió un problema al enviar el documento por correo electrónico";
      }
      else{
        foreach($contactEmails as $key => $value){
          writeOnJournal($usuarioId,"Ha enviado un correo a ".$rowDes['nombre']." ".$rowDes['apellidos']);
          log_write("OK: SAVE-COMMENT-NEW: Se envió un email a usuario id ".$desId." por parte de usuario id ".$remId,4);
        }           
      }
    }
    mysqli_close($db_usuarios);
  }
/*******Creación de listas desplegables para formularios********/
  //llamada de ajax. Crea una lista de estados (jalisco, michoacán, sonora etc) para su selección en el formulario
  if("getStatesList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    $pref = isset($_POST['pref']) ? $_POST['pref'] : "";
    
    $query = "SELECT * FROM cat_estados";

    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de estados";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_".$pref."state\" name=\"sel_".$pref."state\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['estado_id'])
          $salida .= "<option value=".$row['estado_id']." selected>".$row['estado']."</option>"; 
        else
          $salida .= "<option value=".$row['estado_id'].">".$row['estado']."</option>";   
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. Crea una lista de paises (México, Canadá etc) para su selección en el formulario
  if("getCountryList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    $pref = isset($_POST['pref']) ? $_POST['pref'] : "";
    
    $query = "SELECT * FROM cat_paises";

    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de estados";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_".$pref."country\" name=\"sel_".$pref."country\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['pais_id'])
          $salida .= "<option value=".$row['pais_id']." selected>".$row['pais']."</option>"; 
        else
          $salida .= "<option value=".$row['pais_id'].">".$row['pais']."</option>";   
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. crea una lista de origenes de ventas (llamada, visita, sitio web etc) para su selección en el formulario
  if("getOriginList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT * FROM cat_venta_origen";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de orígenes";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_origin\" name=\"sel_origin\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['venta_origen_id'])
          $salida .= "<option value=".$row['venta_origen_id']." selected>".$row['venta']."</option>"; 
        else
          $salida .= "<option value=".$row['venta_origen_id'].">".$row['venta']."</option>";             
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. crea una lista de departamentos (contabilidad, direccion etc) para su selección en el formulario
  if("getDepartmentList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT * FROM cat_departamentos";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de departamentos";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_department\" name=\"sel_department\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['departamento_id'])
          $salida .= "<option value=".$row['departamento_id']." selected>".$row['departamento']."</option>"; 
        else
          $salida .= "<option value=".$row['departamento_id'].">".$row['departamento']."</option>";             
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. crea una lista de departamentos (contabilidad, direccion etc) para su selección en el formulario
  if("getPersonTitleList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT * FROM cat_persona_titulo";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de titulos de persona";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_personTitle\" name=\"sel_personTitle\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['persona_titulo_id'])
          $salida .= "<option value=".$row['persona_titulo_id']." selected>".$row['abreviatura']."</option>"; 
        else
          $salida .= "<option value=".$row['persona_titulo_id'].">".$row['abreviatura']."</option>";             
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. crea una puestos (gerente, ing en sistemas etc) para su selección en el formulario
  if("getPositionList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT * FROM cat_puestos";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de puestos";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_position\" name=\"sel_position\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['puesto_id'])
          $salida .= "<option value=".$row['puesto_id']." selected>".$row['puesto']."</option>"; 
        else
          $salida .= "<option value=".$row['puesto_id'].">".$row['puesto']."</option>";             
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. crea una lista de giro comercial (telecomunicaciones, automotriz etc) para su selección en el formulario
  if("getBusinessList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT * FROM cat_giro_comercial";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de orígenes";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_giroCom\" name=\"sel_giroCom\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['giro_id'])
          $salida .= "<option value=".$row['giro_id']." selected>".$row['giro_descripcion']."</option>"; 
        else
          $salida .= "<option value=".$row['giro_id'].">".$row['giro_descripcion']."</option>";             
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. crea la lista de estatus de ventas (contactado, cita pendiente etc.) para su selección en el formulario
  if("getStatusList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT * FROM cat_venta_estatus WHERE visible = 1 ORDER BY orden";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de estatus";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_status\" name=\"sel_status\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['venta_estatus_id'])
          $salida .= "<option value=".$row['venta_estatus_id']." selected>".$row['estatus']."</option>"; 
        else
          $salida .= "<option value=".$row['venta_estatus_id'].">".$row['estatus']."</option>"; 
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. carga la lista de usuarios que son grupo 4 (vendedores) para su asignación a un cliente/prospecto en el formulario
  if("getAssigntoList" == $_POST["action"]){
    $db_usuarios  = condb_usuarios();
    
    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT DISTINCT usr.usuario_id,usr.username,nombre,apellidos FROM usuarios AS usr, rel_usuario_grupo AS rel WHERE (rel.grupo_id=4 OR rel.grupo_id=3 OR rel.grupo_id=2) AND rel.usuario_id=usr.usuario_id";
    
    $result = mysqli_query($db_usuarios,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de candidatos para asignación";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_assignUsers\" name=\"sel_assignUsers\">".
                "<option value=0>Seleccione...</option>";
      while($row = mysqli_fetch_row($result)){
        if($id==$row[0])
          $salida .= "<option value=".$row[0]." selected>".$row[2]." ".$row[3]." (".$row[1].")</option>"; 
        else
          $salida .= "<option value=".$row[0].">".$row[2]." ".$row[3]." (".$row[1].")</option>";    
      }
      $salida .= "</select>";
    }
    mysqli_close($db_usuarios);
    echo $salida;
  }
  //llamada de ajax. crea la lista de estatus de evento (asignado, enprogreso, terminado etc.) para su selección en el formulario
  if("getEventStatList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    $opt = isset($_POST['opt']) ? $_POST['opt'] : 0;
    
    $query = "SELECT * FROM cat_evento_estatus";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de estatus";
    }
    else{
      if(1==$opt)
        $selname = "sel_eveEstat";
      elseif(2==$opt)
        $selname = "sel_tareaEstat";
      $salida = "<select class=\"form-control\" id=\"$selname\" name=\"$selname\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['evento_estatus_id'])
          $salida .= "<option value=".$row['evento_estatus_id']." selected>".$row['estatus']."</option>"; 
        else
          $salida .= "<option value=".$row['evento_estatus_id'].">".$row['estatus']."</option>"; 
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. crea la lista de tipos de evento (cita, llamada, instalción etc) para su selección en el formulario
  if("getEventTypeList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT * FROM cat_tipo_eventos";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de estatus";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_eveTipo\" name=\"sel_eveTipo\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['tipo_evento_id'])
          $salida .= "<option value=".$row['tipo_evento_id']." selected>".$row['tipo_evento']."</option>"; 
        else
          $salida .= "<option value=".$row['tipo_evento_id'].">".$row['tipo_evento']."</option>"; 
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. crea la lista de estatus de cotización (creado, entregado, autorizado etc.) para su selección en el formulario
  if("getPriceStatList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT * FROM cat_cotizacion_estatus";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de estatus";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_cotEstat\" name=\"sel_cotEstat\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['cotizacion_estatus_id'])
          $salida .= "<option value=".$row['cotizacion_estatus_id']." selected>".$row['estatus']."</option>"; 
        else
          $salida .= "<option value=".$row['cotizacion_estatus_id'].">".$row['estatus']."</option>"; 
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. crea la lista de productos o servicios (extendion, ip fija pyme, telefonia ip etc.) para su selección en el formulario
  /*if("getProductList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT * FROM cat_productos";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de productos";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_products\" name=\"sel_products\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['tipo_servicio_id'])
          $salida .= "<option value=".$row['tipo_servicio_id']." selected>".$row['producto']."</option>"; 
        else
          $salida .= "<option value=".$row['tipo_servicio_id'].">".$row['producto']."</option>"; 
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }*/
  //llamada de ajax. crea la lista de servicios/productos (INTCORWIFI10ASIM-01KM etc) para su selección en el formulario
  if("getProductTypeList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    $categoria = isset($_POST['categoria']) ? $_POST['categoria'] : 0;
    $region = isset($_POST['region']) ? $_POST['region'] : -1;
    
    $query = "SELECT cvs.* ".
             "FROM cat_venta_servicios AS cvs, ".
             "cat_venta_tipo_servicios AS cvts, ".
             "cat_venta_categorias AS cvc ".
             "WHERE cvc.categoria_id= ".$categoria." ".
             "AND cvc.categoria_id != 5 ".
             "AND cvts.categoria_id = cvc.categoria_id ".
             "AND cvs.tipo_servicio = cvts.tipo_servicio_id ".
             "AND cvs.promo = 0 ".
             "AND cvs.vigente = 1 ".
             "AND cvs.region_id = ".$region;
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de tipos de productos";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_prodTipo\" name=\"sel_prodTipo\">".
                "<option value=0 selected>Seleccione...</option>";      
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['servicio_id'])
          $salida .= "<option value=".$row['servicio_id']." selected>".$row['servicio']."</option>"; 
        else
          $salida .= "<option value=".$row['servicio_id'].">".$row['servicio']."</option>"; 
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    // var_dump($query);
    echo $salida;
  }
   //llamada de ajax. crea la lista de productos o servicios (extendion, ip fija pyme, telefonia ip etc.) para su selección en el formulario
  if("getProductPromoList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    $categoria = isset($_POST['categoria']) ? $_POST['categoria'] : 0;
    $region = isset($_POST['region']) ? $_POST['region'] : -1;
    $vigencia = isset($_POST['vigencia']) ? $_POST['vigencia'] : -1;
    
    $hoy = date('Y-m-d');

    $query = "SELECT cvs.* FROM cat_venta_servicios AS cvs, ".
             "cat_venta_tipo_servicios AS cvts, ".
             "cat_venta_categorias AS cvc ".
             "WHERE cvc.categoria_id= ".$categoria." ".
             "AND cvts.categoria_id = cvc.categoria_id ".
             "AND cvs.tipo_servicio = cvts.tipo_servicio_id ".
             "AND cvs.promo = 1 ".
             "AND cvs.vigente = 1 ".
             "AND cvs.servicio_vigencia_inicio<='".$hoy."' ".
             "AND cvs.servicio_vigencia_fin>='".$hoy."' ".
             "AND cvs.region_id = ".$region." ";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de promociones";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_productsPromo\" name=\"sel_productsPromo\">".
                "<option value=0 selected>Seleccione...</option>"; 
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['servicio_id'])
          $salida .= "<option value=".$row['servicio_id']." selected>".$row['servicio']."</option>"; 
        else
          $salida .= "<option value=".$row['servicio_id'].">".$row['servicio']."</option>"; 
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. 
  if("getEventTimeInterval" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT * FROM cat_tiempo_intervalo";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de telefonía";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_intervalo_tiempo\" name=\"sel_intervalo_tiempo\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['intervalo_tiempo_id'])
          $salida .= "<option value=".$row['intervalo_tiempo_id']." selected>".$row['tiempo']."</option>"; 
        else
          $salida .= "<option value=".$row['intervalo_tiempo_id'].">".$row['tiempo']."</option>"; 
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. crea la lista de tipos de documento (ife, acta constitutiva, comprobante de domicilio) para su selección en el formulario
  if("getDocTypeList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT * FROM cat_tipo_documento";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de tipos de documentos";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_docType\" name=\"sel_docType\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['tipo_documento_id'])
          $salida .= "<option value=".$row['tipo_documento_id']." selected>".$row['tipo_documento']."</option>"; 
        else
          $salida .= "<option value=".$row['tipo_documento_id'].">".$row['tipo_documento']."</option>"; 
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. crea la lista de topologias (punto a punto,multipunto etc.) para su selección en el formulario
  if("getConnectionTopologyList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT * FROM cat_enlace_topologias";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de topologias";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_conTop\" name=\"sel_conTop\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['topologia_id'])
          $salida .= "<option value=".$row['topologia_id']." selected>".$row['topologia']."</option>"; 
        else
          $salida .= "<option value=".$row['topologia_id'].">".$row['topologia']."</option>"; 
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. crea la lista de topologias (punto a punto,multipunto etc.) para su selección en el formulario
  if("getConnectionActiveList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT * FROM cat_enlace_activo";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de estatus de enlace";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_conAct\" name=\"sel_conAct\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['enlace_activo_id'])
          $salida .= "<option value=".$row['enlace_activo_id']." selected>".$row['activo_estado']."</option>"; 
        else
          $salida .= "<option value=".$row['enlace_activo_id'].">".$row['activo_estado']."</option>"; 
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. crea la lista de servicios (internet, RPV, Linea anal{ogica etc) para su selección en el formulario
  if("getServiceList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT * FROM cat_tipo_servicios";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de servicios";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_serTipo\" name=\"sel_serTipo\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['tipo_servicio_id'])
          $salida .= "<option value=".$row['tipo_servicio_id']." selected>".$row['tipo_servicio']."</option>"; 
        else
          $salida .= "<option value=".$row['tipo_servicio_id'].">".$row['tipo_servicio']."</option>"; 
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. crea la lista de equipos (routers, modems etc) para su selección en el formulario
  if("getServiceEquipmentList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT * FROM cat_equipos";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de equipos";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_serEquip\" name=\"sel_serEquip\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['equipo_id'])
          $salida .= "<option value=".$row['equipo_id']." selected>".$row['descripcion']."</option>"; 
        else
          $salida .= "<option value=".$row['equipo_id'].">".$row['descripcion']."</option>"; 
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. crea la lista de asignar a (usuario o departamento) para su selección en el formulario
  if("getTicketAssignToList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query1 = "SELECT cat.ticket_staff_id, cat.idstaff, usr.nombre, usr.apellidos ".
              "FROM ".$cfgTableNameCat.".cat_ticket_staff AS cat, ".$cfgTableNameUsr.".usuarios AS usr ".
              "WHERE usr.usuario_id=cat.usuario_id";
    
    $query2 = "SELECT cat.ticket_departamento_id, cat.iddepartamento, grp.grupo ".
              "FROM ".$cfgTableNameUsr.".grupos AS grp, ".$cfgTableNameCat.".cat_ticket_departamento AS cat ".
              "WHERE cat.grupo_id=grp.grupo_id";
    
    $result1 = mysqli_query($db_catalogos,$query1);
    $result2 = mysqli_query($db_catalogos,$query2);
    
    if(!$result1||!$result2){
       $salida = "ERROR: No se pudo cargar la lista";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_tickAssign\" name=\"sel_tickAssign\">";
      $salida .= "<optgroup label=\"Miembros de Staff (".mysqli_num_rows($result1).")\">";
      while($row1 = mysqli_fetch_assoc($result1)){
        if($id==$row1['idstaff'])
          $salida .= "<option value=".$row1['idstaff']." selected>".$row1['nombre']." ".$row1['apellidos']."</option>"; 
        else
          $salida .= "<option value=".$row1['idstaff'].">".$row1['nombre']." ".$row1['apellidos']."</option>"; 
      }
      
      $salida .= "<optgroup label=\"Departamentos (".mysqli_num_rows($result2).")\">";
      while($row2 = mysqli_fetch_assoc($result2)){
        if($id==$row2['iddepartamento'])
          $salida .= "<option value=".$row2['iddepartamento']." selected>".$row2['grupo']."</option>"; 
        else
          $salida .= "<option value=".$row2['iddepartamento'].">".$row2['grupo']."</option>"; 
      }
      
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. crea la lista de origen de tickets (e-mail, telefono etc)
  if("getTicketOriginList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT * FROM cat_ticket_origen";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
      $salida = "ERROR: No se pudo cargar la lista de origen de ticket";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_ticketOrg\" name=\"sel_ticketOrg\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['ticket_origen_id'])
          $salida .= "<option value=".$row['ticket_origen_id']." selected>".$row['ticket_origen']."</option>"; 
        else
          $salida .= "<option value=".$row['ticket_origen_id'].">".$row['ticket_origen']."</option>"; 
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. crea la lista de topic de tickets (altas bajas y cambios, sin servicio, incidentes etc)
  if("getTicketTopicList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT top.ticket_topic_id, top.ticket_subcategoria, top.es_otro, cat.categoria FROM cat_ticket_topic AS top, cat_ticket_categorias AS cat WHERE top.ticket_categoria_id=cat.ticket_categoria_id";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
      $salida = "ERROR: No se pudo cargar la lista de origen de ticket";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_ticketTopic\" name=\"sel_ticketTopic\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['ticket_topic_id'])
          $salida .= "<option value=".$row['ticket_topic_id']." selected>".$row['categoria']." - ".$row['ticket_subcategoria']."</option>"; 
        else
          $salida .= "<option value=".$row['ticket_topic_id'].">".$row['categoria']." - ".$row['ticket_subcategoria']."</option>"; 
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. crea la lista de SLA de tickets (Corporativo, dedicado, pyme etc)
  if("getTicketSlaList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT ticket_sla_id, ticket_sla FROM cat_ticket_sla";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
      $salida = "ERROR: No se pudo cargar la lista de origen de ticket";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_ticketSla\" name=\"sel_ticketSla\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['ticket_sla_id'])
          $salida .= "<option value=".$row['ticket_sla_id']." selected>".$row['ticket_sla']."</option>"; 
        else
          $salida .= "<option value=".$row['ticket_sla_id'].">".$row['ticket_sla']."</option>"; 
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. crea la lista de prioridad de tickets (alta, normal, emergencia, baja etc)
  if("getTicketPriorityList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT ticket_prioridad_id, ticket_prioridad FROM cat_ticket_prioridad";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
      $salida = "ERROR: No se pudo cargar la lista de prioridades de ticket";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_ticketPrior\" name=\"sel_ticketPrior\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['ticket_prioridad_id'])
          $salida .= "<option value=".$row['ticket_prioridad_id']." selected>".$row['ticket_prioridad']."</option>"; 
        else
          $salida .= "<option value=".$row['ticket_prioridad_id'].">".$row['ticket_prioridad']."</option>"; 
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. crea la lista de departamento de tickets (Soporte, ventas etc)
  if("getTicketDepartmentList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT cat.ticket_departamento_id, cat.iddepartamento, grp.grupo ".
             "FROM ".$cfgTableNameUsr.".grupos AS grp, ".$cfgTableNameCat.".cat_ticket_departamento AS cat ".
             "WHERE cat.grupo_id=grp.grupo_id";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
      $salida = "ERROR: No se pudo cargar la lista de origen de ticket";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_ticketDepartment\" name=\"sel_ticketDepartment\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['ticket_departamento_id'])
          $salida .= "<option value=".$row['ticket_departamento_id']." selected>".$row['grupo']."</option>"; 
        else
          $salida .= "<option value=".$row['ticket_departamento_id'].">".$row['grupo']."</option>"; 
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //llamada de ajax. crea la lista de regiones (Guadalajara, Manzanillo etc) para su selección en el formulario
  if("getRegionList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT * FROM cat_coe_ubicacion";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de enlaces";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_region\" name=\"sel_region\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['ubicacion_id'])
          $salida .= "<option value=".$row['ubicacion_id']." selected>".$row['ubicacion_nombre']."</option>"; 
        else
          $salida .= "<option value=".$row['ubicacion_id'].">".$row['ubicacion_nombre']."</option>"; 
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  //obtiene un listado de los usuarios que están disponibles, es decir que no hayan eventos que los tengan como invitados
  if("getAvailablePeople" == $_POST["action"]){
    $db_modulos = condb_modulos();
    
    $fecIni = isset($_POST['fecIni']) ? $_POST['fecIni'] : "";
    $fecFin = isset($_POST['fecFin']) ? $_POST['fecFin'] : "";
    $id = isset($_POST['id']) ? $_POST['id'] : 0;
  
    /*$query = "SELECT usr.usuario_id, usr.nombre, usr.apellidos, usr.username ".
             "FROM ".$cfgTableNameUsr.".usuarios AS usr ". 
             "WHERE NOT EXISTS ( ".
               "SELECT usr.usuario_id ".
               "FROM ".$cfgTableNameMod.".calendario AS cal ".
               "WHERE (cal.fecha_inicio BETWEEN '".$fecIni."' AND '".$fecFin."' ".
               "OR cal.fecha_fin BETWEEN '".$fecIni."' AND '".$fecFin."') ".
               "AND (cal.invitados LIKE CONCAT('%',usr.usuario_id,',%')) ".
               "OR cal.usuario_id = usr.usuario_id) ".
               "OR usr.usuario_id = ".$usuarioId." ".
             "AND usr.usuario_id != 0 ORDER BY usr.nombre ";*/
             
             
    $query = "SELECT usr.usuario_id, usr.nombre, usr.apellidos, usr.username ".
             "FROM ".$cfgTableNameUsr.".usuarios AS usr, ". 
                     $cfgTableNameUsr.".rel_usuario_grupo AS relUsrGrp ". 
             "WHERE NOT EXISTS ( ".
               "SELECT usr.usuario_id ".
               "FROM ".$cfgTableNameMod.".orden_trabajo_tareas AS cal ".
               "WHERE (cal.fecha_inicio BETWEEN '".$fecIni."' AND '".$fecFin."' ".
               "OR cal.fecha_fin BETWEEN '".$fecIni."' AND '".$fecFin."') ".
               "AND (cal.invitados LIKE CONCAT('%',usr.usuario_id,',%')) ".
               "OR cal.usuario_id = usr.usuario_id ".
               "OR usr.usuario_id = ".$usuarioId.") ".
             "AND relUsrGrp.usuario_id = usr.usuario_id ".
             "AND relUsrGrp.grupo_id = 10 ".
             "AND usr.usuario_id != 0 ORDER BY usr.nombre";
             
    $result = mysqli_query($db_modulos,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de invitados disponibles";
    }
    else{
      if(0>=mysqli_num_rows($result)){
        $salida = "No existen usuarios disponibles dentro del rango de fechas seleccionado";
      }
      else{
        $salida = "<select class=\"form-control\" id=\"sel_invitados\" name=\"sel_invitados\">";
        while($row = mysqli_fetch_assoc($result)){
          if($id==$row['usuario_id'])
            $salida .= "<option value=".$row['usuario_id']." selected>".$row['nombre']." ".$row['apellidos']."</option>"; 
          else
            $salida .= "<option value=".$row['usuario_id'].">".$row['nombre']." ".$row['apellidos']."</option>"; 
        }
        $salida .= "</select>";
      }
      
    }
    mysqli_close($db_modulos);
    echo $salida;              
  }
  //llamada de ajax. crea la lista de departamento de tickets (Soporte, ventas etc)
  if("getEquipmentCategoryList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT cat.categoria_id, cat.categoria ".
             "FROM ".$cfgTableNameCat.".cat_mobiliario_categorias AS cat ";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
      $salida = "ERROR: No se pudo cargar la lista de origen de ticket";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_equipCat\" name=\"sel_equipCat\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['categoria_id'])
          $salida .= "<option value=".$row['categoria_id']." selected>".$row['categoria']."</option>"; 
        else
          $salida .= "<option value=".$row['categoria_id'].">".$row['categoria']."</option>"; 
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  if("getAvailableEquipment" == $_POST["action"]){
    $db_modulos = condb_modulos();
    
    $fecIni = isset($_POST['fecIni']) ? $_POST['fecIni'] : "";
    $fecFin = isset($_POST['fecFin']) ? $_POST['fecFin'] : "";
    $categoria = isset ($_POST['categoria']) ? $_POST['categoria'] : "";
    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT equ.equipo_id, equ.descripcion, equ.num_serie ".
             "FROM ".$cfgTableNameCat.".cat_mobiliario_equipo AS equ, ".
                     $cfgTableNameCat.".cat_mobiliario_categorias AS cat ".
             "WHERE NOT EXISTS ( ".
             "SELECT equ.equipo_id ". 
             "FROM ".$cfgTableNameMod.".orden_trabajo_tareas AS cal ".
             "WHERE (cal.fecha_inicio BETWEEN '".$fecIni."' AND '".$fecFin."' ".
             "OR cal.fecha_fin BETWEEN '".$fecIni."' AND '".$fecFin."') ".
             "AND cal.equipo LIKE CONCAT('%',equ.equipo_id,',%')) ".
             "AND cat.categoria_id = equ.categoria_id ".
             "AND cat.categoria_id = ".$categoria;
             
    $result = mysqli_query($db_modulos,$query);
    
    if(!$result){
       $salida = "No se pudo cargar la lista de equipos disponibles";
    }
    else{
      if(0>=mysqli_num_rows($result)){
        $salida = "No existen equipos disponibles dentro del rango de fechas seleccionado";
      }
      else{
        $salida = "<select class=\"form-control\" id=\"sel_eveEquipo\" name=\"sel_eveEquipo\">";
        while($row = mysqli_fetch_assoc($result)){
          if($id==$row['equipo_id'])
            $salida .= "<option value=".$row['equipo_id']." selected>".$row['descripcion']." (".$row['num_serie'].")</option>"; 
          else
            $salida .= "<option value=".$row['equipo_id'].">".$row['descripcion']." (".$row['num_serie'].")</option>"; 
        }
        $salida .= "</select>";
      }
      
    }
    mysqli_close($db_modulos);
    echo $salida; 
  }
  //llamada de ajax. crea la lista de departamento de tickets (Soporte, ventas etc)
/*  if("getValidityList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    $orgType = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
    
    $query = "SELECT * FROM ".$cfgTableNameCat.".cat_venta_vigencia AS cat ";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
      $salida = "ERROR: No se pudo cargar la lista de vigencias";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_validity\" name=\"sel_validity\">";
      while($row = mysqli_fetch_assoc($result)){
        if(0==$row['restringido']){
          if($id==$row['vigencia_id'])
            $salida .= "<option value=".$row['vigencia_id']." selected>".$row['cantidad_vigencia']." ".$row['tiempo_vigencia']."</option>"; 
          else
            $salida .= "<option value=".$row['vigencia_id'].">".$row['cantidad_vigencia']." ".$row['tiempo_vigencia']."</option>";
        }
        else{
          if(hasPermission($orgType,'a')){
            if($id==$row['vigencia_id'])
              $salida .= "<option value=".$row['vigencia_id']." selected>".$row['cantidad_vigencia']." ".$row['tiempo_vigencia']."</option>"; 
            else
              $salida .= "<option value=".$row['vigencia_id'].">".$row['cantidad_vigencia']." ".$row['tiempo_vigencia']."</option>";
          }
        }
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }*/

 if("getValidityList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
 
    $query = "SELECT * FROM ".$cfgTableNameCat.".cat_venta_vigencia AS cat WHERE cantidad_vigencia>0 AND activo=1";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
      $salida = "ERROR: No se pudo cargar la lista de vigencias";
    }
    else{
      //if(0==$opt){
        $salida = "<select class=\"form-control\" id=\"sel_validity\" name=\"sel_validity\">";
        while($row = mysqli_fetch_assoc($result)){
          if($id==$row['vigencia_id'])
            $salida .= "<option value=".$row['vigencia_id']." selected>".$row['cantidad_vigencia']." ".$row['tiempo_vigencia']."</option>"; 
          else
            $salida .= "<option value=".$row['vigencia_id'].">".$row['cantidad_vigencia']." ".$row['tiempo_vigencia']."</option>";
        }
        $salida .= "</select>";
     /* }
      else{
        while($row = mysqli_fetch_assoc($result)){
          if($id==$row['vigencia_id']){
            $salida = $row['restringido'];
          }
        }
      }*/
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  
  //llamada de ajax. crea la lista de categorias de servicio/producto (enlace, telefonia etc) para su selección en el formulario
  if("getProductCategoryList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();

    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    
    $query = "SELECT * FROM cat_venta_categorias WHERE categoria_id != 5";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de enlaces";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_prodCategoria\" name=\"sel_prodCategoria\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['categoria_id'])
          $salida .= "<option value=".$row['categoria_id']." selected>".$row['categoria']."</option>"; 
        else
          $salida .= "<option value=".$row['categoria_id'].">".$row['categoria']."</option>"; 
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  
  //llamada de ajax. crea la lista de adecuaciones (tramos de torre) para las ordenes de trabajo
  if("getAdecList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();
    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    $worOrId = isset($_POST['ordenId']) ? decrypt($_POST['ordenId']) : 0;
    $query = "SELECT cvs.* ".
             "FROM ".$cfgTableNameCat.".cat_venta_servicios AS cvs, ".
                     $cfgTableNameCat.".cat_venta_tipo_servicios AS cvts, ".
                     $cfgTableNameCat.".cat_venta_categorias AS cvc, ".
                     "(SELECT cvs.region_id FROM ".$cfgTableNameCat.".cat_venta_servicios AS cvs,  ".
                                 $cfgTableNameMod.".rel_cotizacion_producto AS relCotPro, ".
                                 $cfgTableNameMod.".ordenes_trabajo AS ordTra ".
                      "WHERE ordTra.orden_id = ".$worOrId.
                      " AND ordTra.rel_cotizacion_producto = relCotPro.relacion_id".
                      " AND relCotPro.producto_id = cvs.servicio_id)regionId ".
             "WHERE cvc.categoria_id= 5 ".
             "AND cvs.region_id = regionId.region_id ".
             "AND cvts.categoria_id = cvc.categoria_id ".
             "AND cvs.tipo_servicio = cvts.tipo_servicio_id ".
             "AND cvs.promo = 0 ";
    $result = mysqli_query($db_catalogos,$query);
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de tipos de productos ";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_adecuaciones\" name=\"sel_adecuaciones\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id==$row['servicio_id'])
          $salida .= "<option value=".$row['servicio_id']." selected>".$row['servicio']."</option>"; 
        else
          $salida .= "<option value=".$row['servicio_id'].">".$row['servicio']."</option>"; 
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  
  //llamada de ajax. crea la lista de departamento de tickets (Soporte, ventas etc)
  if("getWorkOrderTypeList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();
    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    $option = isset($_POST['option']) ? $_POST['option'] : 0;
    $query = "SELECT * FROM ".$cfgTableNameCat.".cat_orden_trabajo_tipo ";
    if(-1==$option){
      $query .= "WHERE tipo_orden_id = 1";//solo estudio de factibilidad
    }
    elseif(-2==$option){
      $query .= "WHERE tipo_orden_id = 5";//solo dimensionamiento
    }
      $result = mysqli_query($db_catalogos,$query);
    if(!$result){
      $salida = "ERROR: No se pudo cargar la lista de origen de ticket";
    }
    else{
      while($row = mysqli_fetch_assoc($result)){
        // if((1==$row['tipo_orden_id'])||(hasPermission(4,'w') && (1 < $row['tipo_orden_id']))){
        if(($id==$row['tipo_orden_id'])||(hasPermission(4,'w'))){
          if($id==$row['tipo_orden_id'])
            $salida .= "<input type=\"radio\" name=\"rad_workOrder\" id=\"rad_workOrder_".$row['tipo_orden_id']."\" class=\" rad_responsive\" value=".encrypt($row['tipo_orden_id'])." checked>".$row['tipo_orden']."</input><br></td>";
          else
            $salida .= "<input type=\"radio\" name=\"rad_workOrder\" id=\"rad_workOrder_".$row['tipo_orden_id']."\" class=\" rad_responsive\" value=".encrypt($row['tipo_orden_id']).">".$row['tipo_orden']."</input><br></td>";
        }
      }
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  
  //llamada de ajax. crea la lista de estatus de orden de trabajo (asignado, enprogreso, terminado etc.) para su selección en el formulario
  if("getWorkOrderStatList" == $_POST["action"]){
    $db_catalogos = condb_catalogos();
    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    $query = "SELECT * FROM cat_orden_trabajo_estatus";
    $result = mysqli_query($db_catalogos,$query);
    if(!$result){
       $salida = "ERROR: No se pudo cargar la lista de estatus";
    }
    else{
      $salida = "<select class=\"form-control\" id=\"sel_worOrdEstatus\" name=\"sel_worOrdEstatus\">";
      while($row = mysqli_fetch_assoc($result)){
        if($id == 5 && $id==$row['estatus_id'])
          $salida .= "<option value=".$row['estatus_id']." selected>".$row['estatus']."</option>";
        elseif($id != 5 && $id==$row['estatus_id'])
          $salida .= "<option value=".$row['estatus_id']." selected>".$row['estatus']."</option>"; 
        elseif($row['estatus_id'] != 5 )
          $salida .= "<option value=".$row['estatus_id'].">".$row['estatus']."</option>"; 
      }
      $salida .= "</select>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }
  
  //Obtiene los tabs que dividen los prospectos o clientes por su estatus
  if("getStatusTabs" == $_POST["action"]){
    $db_catalogos = condb_catalogos();
    $query = "SELECT * FROM cat_venta_estatus WHERE visible = 1 ORDER BY orden";
    $result = mysqli_query($db_catalogos,$query);
    if(!$result){
      $salida = "ERROR|| Ocurrió un problema al obtener los estaus de venta para la carga de pestañas";
    }
    else{
      $salida = "OK||<ul class=\"nav nav-tabs\" id=\"ul_prospectTabs\">";
      // var_dump(mysqli_fetch_assoc($result));
      while($row = mysqli_fetch_assoc($result)){
        if($row['orden']==0){
          $active = "active";
        }
        else{
          $active = "";
        } 
        $salida .= "  <li class=\"nav-item ".$active."\" id=\"li_".$row['li_label']."_".$row['venta_estatus_id']."\">".
                   "    <a href=\"#li_".$row['li_label']."_div\" data-toggle=\"tab\" class=\"nav-link\" role=\"tab\" style=\"color:#".$row['color']."; background-color:#".$row['bgcolor']."\" >".
                   "<i class=\"fa ".$row['icono']."\" style=\"color:#".$row['color']."\"></i> ".
                   $row['estatus'].
                   "</a></li>";    
      }
      $salida .= "</ul>";  
    }
    $result2 = mysqli_query($db_catalogos,$query); 
    if(!$result2){
      $salida = "ERROR|| Ocurrió un problema al obtener los estatus de venta para la carga de páneles ";
    }
    else{
      $salida .= "<div class=\"tab-content\" id=\"div_prospectDivs\">";
      
      while($row = mysqli_fetch_assoc($result2)){
        if($row['orden']==0){
          $active = "active";
        }
        else{
          $active = "";
        }
        $salida .= "  <div class=\"tab-pane ".$active."\" role=\"tabpanel\" id=\"li_".$row['li_label']."_div\" style=\"background-color:#".$row['bgcolor']."\"><div style=\"background-color:#eee\" id=\"div_tbl".$row['li_label']."\"></div></div>";
      }
      $salida .= "</div>";
    }
    mysqli_close($db_catalogos);
    echo $salida;
  }


  //llamada de ajax. Crea un listado de prospectos o clientes que coinciden con los agregados en el formulario, para verificar si no existe ya
  if("clientAutocomplete"==$_POST['action']){
    $db_modulos   = condb_modulos();
    $razonSoc = isset($_POST['razonSoc']) ? $_POST['razonSoc'] : "";
    $nomCom = isset($_POST['nomCom']) ? $_POST['nomCom'] : "";
    $rfc = isset($_POST['rfc']) ? $_POST['rfc'] : "";
    $orgType = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
    $camNom = isset($_POST['fieldName']) ? $_POST['fieldName'] : "";
    $fieldname = "";
    $fielVal = "";
    if((""!=$razonSoc)&&("razonSoc"==$camNom)){
      $fieldName = "razon_social";
      $fieldVal = $razonSoc;
    }
    if((""!=$nomCom)&&("nomCom"==$camNom)){
      $fieldName = "nombre_comercial";
      $fieldVal = $nomCom;
    }
    if((""!=$rfc)&&("rfc"==$camNom)){
      $fieldName = "rfc";
      $fieldVal = $rfc;
    }
    switch($orgType){
      case 0:
        $tableName = "prospectos";
        $fieldAssign = "asignadoa";
        $fieldId = "prospecto_id";
      break;
      case 1:
        $tableName = "clientes";
        $fieldAssign = "usuario_alta";
        $fieldId = "cliente_id";
      break;
      default:
        $tableName = "";
      break; 
    }
    $query = "SELECT ".$fieldId.",".$fieldName.",".$fieldAssign." FROM ".$tableName." WHERE lower(".$fieldName.") LIKE '%".strtolower($fieldVal)."%' LIMIT 5";
    $result = mysqli_query($db_modulos,$query);
    if(!$result){
      $salida = "ERROR: No se pudo cargar la lista de origen de registros existentes";
    }
    else{
      if(0<mysqli_num_rows($result)){
        $salida = "<table id=\"tbl_".$tableName."_\" class=\"table table-striped table-condensed\">\n".
                  "  <thead>\n".
                  "    <tr>\n".
                  "      <th class=\"hidden-xs\">Id</th>\n".
                  "      <th>".$tableName."</th>\n".
                  "      <th>Asignado a/Agregado por</th>\n".
                  "    </tr>\n".
                  "  </thead>\n".
                  "  <tbody>\n"; 
        while($row = mysqli_fetch_assoc($result)){
          $salida .= "    <tr>\n".
                     "      <td>".$row[$fieldId]."</td>\n".
                     "      <td>".$row[$fieldName]."</td>\n".
                     "      <td><a href=/crm/modulos/perfil/index.php?id='".encrypt($row[$fieldAssign])."'>".get_userRealName($row[$fieldAssign])."</a></td>\n".
                     "    </tr>\n";
        }
        $salida .= "  </tbody>\n".
                   "</table>";
      }
      else{
        $salida = "No existen clientes o prospectos con este dato";
      }
    }
    mysqli_close($db_modulos);
    echo $salida;
  }

  /*********Funciones de gestión de comentarios**********/
  
  function saveComment($orgId,$orgType,$comment){
    $db_modulos   = condb_modulos();
    global $usuarioId;
    
    log_write("DEBUG: SAVE-COMMENT-NEW: se guardará un comentario",4);
    
    $fecha  = date("Y-m-d H:i:s");    
    
    switch($orgType){
      case 0:
        $orgName = "prospecto";
        $tableName = "comentarios_prospectos";
        $fieldName = "prospecto_id";
      break;
      case 1:
        $orgName = "cliente";
        $tableName = "comentarios_clientes";
        $fieldName = "cliente_id";
      break;
      case 2:
        $orgName = "sitio";
        $tableName = "comentarios_sitios";
        $fieldName = "sitio_id";
      break;
      case 3:
        $orgName = "evento";
        $tableName = "comentarios_eventos";
        $fieldName = "evento_id";
      break;
      case 9:
        $orgName = "orden de trabajo";
        $tableName = "comentarios";
        $fieldName = "origen_id";
        $orgId = decrypt($orgId);
      break;
      case 12:
        $orgName = "tareas";
        $tableName = "comentarios";
        $fieldName = "origen_id";
        $orgId = decrypt($orgId);
      break;
    }
    
    $query = "INSERT INTO ".$tableName." (".$fieldName.",comentario,fecha_alta,usuario_alta) VALUES (".$orgId.",'".$comment."','".$fecha."',".$usuarioId.")";
    
    if(12==$orgType||9==$orgType){
      $query = "INSERT INTO ".$tableName." (origen_id,tipo_origen,comentario,fecha_alta,usuario_alta) VALUES (".$orgId.",".$orgType.",'".$comment."','".$fecha."',".$usuarioId.")";
    }
    
    $result = mysqli_query($db_modulos,$query);
    
    log_write("DEBUG: SAVE-COMMENT-NEW: ".$query,4);
    
    if(!$result){
      log_write("ERROR: SAVE-COMMENT-NEW: No se guardó el comentario",4);
      $salida = false;
    }
    else{
      log_write("OK: SAVE-COMMENT-NEW: El comentario se guardó con éxito",4);
      writeOnJournal($usuarioId,"Ha insertado el comentario [".$comment."] para el ".$orgName." con id ".$orgId);
      $salida = true;
    }
    
    mysqli_close($db_modulos);
    return $salida; 
  }
  
  if("saveCommentNew" == $_POST["action"]){
    
    $orgId = isset($_POST['orgId']) ? $_POST['orgId'] : -1;
    $orgType = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
    $comment = isset($_POST['comment']) ? $_POST['comment'] : "";
    
    if(!$salida = saveComment($orgId,$orgType,$comment))
      $salida = "ERROR|| Error al insertarse el comentario";
    else
      $salida = "OK|| El comentario se ha insertado correctamente";
    
    echo $salida;
  }
  
  //llamada de ajax. toma todos los comentarios hechos respecto a un prospecto/cliente/sitio etc.
  if("getComments" == $_POST["action"]){
    $db_modulos   = condb_modulos();
    $orgId = isset($_POST['orgId']) ? $_POST['orgId'] : -1;
    $orgType = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
    switch($orgType){
      case 0:
        $orgName = "prospecto";
        $tableName = "comentarios_prospectos";
        $fieldName = "prospecto_id";
      break;
      case 1:
        $orgName = "cliente";
        $tableName = "comentarios_clientes";
        $fieldName = "cliente_id";
      break;
      case 2:
        $orgName = "sitio";
        $tableName = "comentarios_sitios";
        $fieldName = "sitio_id";
      break;
      case 3:
        $orgName = "evento";
        $tableName = "comentarios_eventos";
        $fieldName = "evento_id";
      break;
      case 9:
        $orgName = "orden de trabajo";
        $tableName = "comentarios";
        $fieldName = "origen_id";
        $orgId = decrypt($orgId);
      break;
      case 12:
        $orgName = "tareas";
        $tableName = "comentarios";
        $fieldName = "origen_id";
        $orgId = decrypt($orgId);
      break;
    }        
    $query2 = "SELECT * FROM ".$tableName." WHERE ".$fieldName."=".$orgId." ORDER BY fecha_alta DESC";
    if(12==$orgType||9==$orgType)
      $query2 = "SELECT * FROM ".$tableName." WHERE ".$fieldName."=".$orgId." AND tipo_origen=".$orgType." ORDER BY fecha_alta DESC";
    $result2 = mysqli_query($db_modulos,$query2);
    if(!$result2){
      $salida = "ERROR: No se pudo cargar los comentarios para este ".$orgName." de la DB ".mysqli_error($db_modulos). " ".$query2;
    }
    else{
      $salida .= "<form class=\"form-horizontal\" id=\"frm_newComment_$orgType\" name=\"frm_newComment\" role=\"form\" enctype=\"multipart/form-data\">".
                   "<div class=\"panel-heading\">Nuevo Comentario de $orgName</div>".
                   "<div class=\"panel-body\">".
                      "<textarea class=\"form-control\" id=\"txt_comment_$orgType\" maxlenght=\"300\" rows=\"3\" required></textarea>".
                      "<div><label id=\"txt_comment_".$orgType."_error\" class=\"div-errorVal\"/ required></div>".
                      "<br>".
                      "<button type=\"button\" id=\"btn_commentInsert\" title=\"Guardar Comentario\" class=\"btn btn-xs btn-info\" onClick=\"saveCommentNew(".$orgType.");\">Guardar Comentario</button>".
                   "</div>".
                   "</form>";
      if(0==mysqli_num_rows($result2)){
        $salida .= "No existen comentarios para este ".$orgName;
      }
      else{
        $salida .= "<div class=\"panel-comm-info\">";         
                   "<div class=\"panel panel-info small\">".
                  //  "  <div class=\"panel-heading\">Comentasdfasdrios</div>".
                   "  <div class=\"panel-body\">";
        while($row2 = mysqli_fetch_assoc($result2)){
          $nombreAlta = get_userRealName($row2['usuario_alta']);
          $salida .= "<div class=\"media\">".
                     "  <div class=\"media-body\">".
                     "    <p class=\"media-heading\">".
                     "      <b>".$nombreAlta."</b> <i>".$row2["fecha_alta"]."</i>  ";
          //if((array_key_exists(1, $gruposArr))||(array_key_exists(2, $gruposArr))){}

          if(hasPermission($orgType,'d')){
            $salida .= "      <button type=\"button\" id=\"btn_commentDelete_".$row2['comentario_id']."\" title=\"Eliminar Comentario\" class=\"btn btn-xs btn-danger\" onClick=\"deleteComment(".$row2['comentario_id'].");\"><span class=\"glyphicon glyphicon-remove\"></span></button>";
          }
          // if((array_key_exists(1, $gruposArr))||(array_key_exists(2, $gruposArr))||($usuarioId==$row2['usuario_alta'])){
          //   $salida .= "      <button type=\"button\" id=\"btn_commentEdit_".$row2['comentario_id']."\" title=\"Editar Comentario\" class=\"btn btn-xs btn-success\" onClick=\"editComment(".$row2['comentario_id'].");\"><span class=\"glyphicon glyphicon-edit\"></span></button>";
          // }
          $salida .= "    </p>".
                     "       <span id=\"spn_comment_".$row2['comentario_id']."\">".$row2["comentario"]."</span><hr>".
                     "  </div>".
                     "</div>";
        }                     
      }
      $salida .= "  </div>".
               "</div></div>";
               $salida .= "<input type=\"hidden\" id=\"hdn_commentOriginId\" name=\"hdn_commentOriginId\" class=\"form-control\" value=\"".$orgId."\">".
              "<input type=\"hidden\" id=\"hdn_commentOriginType\" name=\"hdn_commentOriginType\" class=\"form-control\" value=\"".$orgType."\">";      
    } 
    mysqli_close($db_modulos);
    echo $salida;       
  }
  
  //elimina un comentario
  if("deleteComment" == $_POST["action"]){
    $db_modulos   = condb_modulos();
    
    $commId  = isset($_POST['commId']) ? $_POST['commId'] : -1;
    $orgType = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
    $fecha   = date("Y-m-d H:i:s");
    
    switch($orgType){ 
      case -1:
        $salida = "ERROR||No se recibió el tipo de origen de este comentario. No pudo borrarse";
        goto comDela;
      break; 
      case 0:
        $orgName = "prospecto";
        $tableName = "comentarios_prospectos";
        $fieldName = "prospecto_id";
      break;
      case 1:
        $orgName = "cliente";
        $tableName = "comentarios_clientes";
        $fieldName = "cliente_id";
      break;
      case 2:
        $orgName = "sitio";
        $tableName = "comentarios_sitios";
        $fieldName = "sitio_id";
      break;
      case 3:
        $orgName = "evento";
        $tableName = "comentarios_eventos";
        $fieldName = "evento_id";
      break;
    }
    
    $query2 = "SELECT comentario,".$fieldName." FROM ".$tableName." WHERE comentario_id=".$commId;
    $result2 = mysqli_query($db_modulos,$query2);
    if(!$result2){
      $salida = "ERROR|| Ocurrió un error al obtener el id del comentario";
    }
    else{
      $comToDelete = mysqli_fetch_assoc($result2);
      
      $query = "DELETE FROM ".$tableName." WHERE comentario_id=".$commId;
    
      $result = mysqli_query($db_modulos,$query);
      
      if(!$result){
        $salida = "ERROR|| Ocurrió un error al borrarse el comentario";
      }
      else{
        writeOnJournal($usuarioId,"Ha borrado el comentario con id ".$comToDelete[$fieldName]." que decía [".$comToDelete['comentario']."]");
        $salida = "OK|| El comentario se ha eliminado con éxito||".$comToDelete[$fieldName];
      }  
    } 
    comDela:  
    mysqli_close($db_modulos);
    echo $salida;
  }  
  /*********fin de apartado**********/
  
  
  /*********Funciones para creación, edición y borrado de representante legal (usado en prospectos y clientes)**********/
  
  //llamada de ajax. Obtiene los datos detallados del representante legal para su edición
    
  if("getManagerDetails" == $_POST["action"]){
    $db_modulos   = condb_modulos();
    
    $manId = isset($_POST['manId']) ? decrypt($_POST['manId']) : -1;
    $option = isset($_POST['option']) ? $_POST['option'] : 0;
    
    $query = "SELECT * FROM representante_legal WHERE representante_id=".$manId;
    
    $result = mysqli_query($db_modulos,$query);
    
    if(!$result){
      $salida = "ERROR||Ocurrió un problema al obtenerse los datos del representante legal ";
    }
    else{
      $row = mysqli_fetch_assoc($result);
      
      if(0==$option){
        $salida = "  <div class=\"col-lg-6 col-xs-12 col-sm-6 nodatatablerowdark\"><b>ID</b></div>\n".
                  "  <div class=\"col-lg-6 col-xs-12 col-sm-6 nodatatablerowdark\">".$row['representante_id']."</div>\n".
                  
                  "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\"><b>Nombre</b></div>\n".
                  "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\">".$row['nombre']." ".$row['apellidos']."</div>\n".
                  "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\"><b>CP</b></div>\n".
                  "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\">".$row['cp']."</div>\n".
                  
                  "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\"><b># Poder</b></div>\n".
                  "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\">".$row['num_poder']."</div>\n".
                  "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\"><b>Municipio</b></div>\n".
                  "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\">".$row['municipio']."</div>\n".
                  
                  "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\"><b>Fecha de Poder</b></div>\n".
                  "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\">".$row['fecha_poder']."</div>\n".
                  "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\"><b>Estado</b></div>\n".
                  "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\">".$row['estado']."</div>\n".
                  
                  "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\"><b>Domicilio</b></div>\n".
                  "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\">".$row['domicilio']."</div>\n".
                  "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\"><b>Teléfono</b></div>\n".
                  "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\">".$row['telefono']."</div>\n".
                  
                  "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\"><b>Colonia</b></div>\n".
                  "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\">".$row['colonia']."</div>\n".
                  "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\"><b>Tel Móvil</b></div>\n".
                  "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\">".$row['cp']."</div>\n".
                  
                  "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\"><b>Email</b></div>\n".
                  "  <div class=\"col-lg-3 col-xs-12 col-sm-6 nodatatablerowdark\">".$row['email']."</div>\n";
                         
      }
      elseif(1==$option){
        $salida = "OK||".$row['nombre']."||".$row['apellidos']."||".$row['num_poder']."||".$row['fecha_poder']."||".$row['domicilio']."||".$row['colonia']."||".$row['cp']."||".$row['municipio']."||".$row['estado']."||".$row['telefono']."||".$row['tel_movil']."||".$row['email']."||".$row['notario']."||".$row['folio_mercantil'];
      }      
    }
    
    mysqli_close($db_modulos);
    echo $salida;
  }

  //actualiza los datos de un representante legal
  if("updateManager" == $_POST["action"]){
    $db_modulos   = condb_modulos();
    $manArr = isset($_POST["manArr"]) ? json_decode($_POST["manArr"]) :"";
    $option = isset($_POST["option"]) ? json_decode($_POST["option"]) :"";
    $orgType = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
    $orgId = isset($_POST['orgId']) ? $_POST['orgId'] : -1;
    
    switch($orgType){
      case 0:
        $orgName = "prospecto";
        $tableName = "prospectos";
        $fieldName = "prospecto_id";
      break;
      case 1:
        $orgName = "cliente";
        $tableName = "clientes";
        $fieldName = "cliente_id";
      break;
    }
    
    if(0==$option){
      $query2 = "INSERT INTO representante_legal (num_poder, ".
                                               "fecha_poder, ".
                                               "nombre, ".
                                               "apellidos, ".
                                               "telefono, ".
                                               "tel_movil, ".
                                               "email, ".
                                               "domicilio, ".
                                               "colonia, ".
                                               "cp, ".
                                               "municipio, ".
                                               "estado, ".
                                               "notario, ".
                                               "folio_mercantil ) ". 
                "VALUES('".$manArr[3]."', ".
                       "'".$manArr[4]."', ".
                       "'".$manArr[1]."', ".
                       "'".$manArr[2]."', ".
                       "'".$manArr[10]."', ".
                       "'".$manArr[11]."', ".
                       "'".$manArr[12]."', ".
                       "'".$manArr[5]."', ".
                       "'".$manArr[6]."', ".
                       "'".$manArr[7]."', ".
                       "'".$manArr[8]."', ".
                       "'".$manArr[9]."', ".
                       "'".$manArr[13]."', ".
                       "'".$manArr[14]."' )";
      
      $result2 = mysqli_query($db_modulos,$query2);
      
      if(!$result2){
        $salida = "ERROR||Ocurrió un problema al insertarse los datos del representante legal";
      }
      else{
        $manId = mysqli_insert_id($db_modulos);
         writeOnJournal($usuarioId,"Ha agregado datos de representante legal con el id ".$manArr[0]." para el ".$orgName." con id ".$orgId);
        
        $queryUpd = "UPDATE ".$tableName." SET representante_id = ".$manId." WHERE ". $fieldName."=".$orgId;
        
        $resultUpd = mysqli_query($db_modulos,$queryUpd);
        
        if(!$queryUpd){
          $salida = "ERROR||Ocurrió un problema al actualizarse el id del representante para este ".$orgName;
        }
        else{
          $salida = "OK||Los datos de este representante se han agregado con éxito";
        }
      }
    }
    elseif(1==$option){
      $query = "UPDATE representante_legal SET num_poder='".$manArr[3]."', fecha_poder='".$manArr[4]."', nombre='".$manArr[1]."', apellidos='".$manArr[2]."', telefono='".$manArr[10]."', tel_movil='".$manArr[11]."', email='".$manArr[12]."', domicilio='".$manArr[5]."', colonia='".$manArr[6]."', cp=".$manArr[7].", municipio='".$manArr[8]."', estado=".$manArr[9].", notario= '".$manArr[13]."', folio_mercantil='".$manArr[14]."' WHERE representante_id=".decrypt($manArr[0]);
    
      $result = mysqli_query($db_modulos,$query);
      
      if(!$result){
        $salida = "ERROR|| Ocurrió un problema al actualizarse los datos de representante legal para este registro ";
      }
      else{      
        writeOnJournal($usuarioId,"Ha modificado los datos del representante legal con el id ".$manArr[0]);
        $salida = "OK||Los datos de representante se han actualizado correctamente";      
      }
    }
    mysqli_close($db_modulos);
    echo $salida;
  }
  
  function insertManager($reprArr){
    global $salida;
    global $db_modulos;
    $query2 = "INSERT INTO representante_legal (num_poder, ".
                                               "fecha_poder, ".
                                               "nombre, ".
                                               "apellidos, ".
                                               "telefono, ".
                                               "tel_movil, ".
                                               "email, ".
                                               "domicilio, ".
                                               "colonia, ".
                                               "cp, ".
                                               "municipio, ".
                                               "estado) ". 
              "VALUES('".$reprArr[0][3]."', ".
                     "'".$reprArr[0][4]."', ".
                     "'".$reprArr[0][1]."', ".
                     "'".$reprArr[0][2]."', ".
                     "'".$reprArr[0][10]."', ".
                     "'".$reprArr[0][11]."', ".
                     "'".$reprArr[0][12]."', ".
                     "'".$reprArr[0][5]."', ".
                     "'".$reprArr[0][6]."', ".
                     "'".$reprArr[0][7]."', ".
                     "'".$reprArr[0][8]."', ".
                     "'".$reprArr[0][9]."')";
    
    $result2 = mysqli_query($db_modulos,$query2);
    
    if(!$result2){
      $salida = "ERROR||Ocurrió un problema al insertarse los datos del representante legal ";
      return false;
    }
    return mysqli_insert_id($db_modulos);
  }
  
  /*********fin de apartado**********/ 
  
  
  /*********Funciones para creación, edición y borrado de eventos**********/
  
  //llamada de ajax. toma los eventos asociados a un prospecto, cliente, sitio etc...
  
  if("getEvents" == $_POST["action"]){
    $db_modulos   = condb_modulos();

    $orgId = isset($_POST['orgId']) ? $_POST['orgId'] : -1;
    $orgType = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
    
    switch($orgType){
      case 0:  //si se trata de un prospecto
        $fieldname = "prospecto_id";
        $typename = "prospecto";
      break;
      case 1:  //si se trata de un cliente
        $fieldname = "cliente_id";
        $typename = "cliente";
      break;
      case 2:  //si se trata de un sitio
        $fieldname = "sitio_id";
        $typename = "sitio";
      break;    
    }
    
    $query = "SELECT cal.*, sta.estatus FROM ".$cfgTableNameMod.".calendario AS cal, ".$cfgTableNameCat.".cat_evento_estatus AS sta WHERE cal.estatus_id=sta.evento_estatus_id AND cal.tipo_origen=".$orgType." AND cal.origen_id=".$orgId; 
    
    $result = mysqli_query($db_modulos,$query);
    
    if(!$result){
      $salida = "ERROR|| No se pudo obtener los eventos para este registro";
    }
    else{
      $salida = "OK||";
      if(hasPermission($orgType,'w')){ 
        $salida .= "<div class=\"col container-fluid\">".
                   "  <button type=\"button\" id=\"btn_eventNew\" title=\"Nuevo Evento\" class=\"btn btn-default\" onClick=\"openNewEvent();\"><span class=\"glyphicon glyphicon-asterisk\"></span> Nuevo Evento</button><br>".
                   "</div><br>";
      }
      if(0==mysqli_num_rows($result)){
        $salida .= "No existen eventos para el ".$typename." con este id. Haga clic en <b>Nuevo evento</b> para agregar uno.";
      }
      else{        
        $salida .= "<div class=\"table-responsive\"><table id=\"tbl_Events\" class=\"table table-striped table-condensed\">\n".
                   "  <thead>\n".
                   "    <tr>\n".
                   "      <th>Título</th>\n".
                   "      <th class=\"hidden-xs\">Por:</th>\n".
                   "      <th>Programado para:</th>\n".
                   "      <th>Estatus</th>\n".
                   "      <th>Descripción</th>\n".
                   "      <th>Acciones</th>\n".
                   "    </tr>\n".
                   "  </thead>\n".
                   "  <tbody>\n";                
        while($row = mysqli_fetch_assoc($result)){
          $nombreAlta = get_userRealName($row['usuario_id']);
          $salida .=  "    <tr>\n".
                      "      <td>".$row['titulo']."</td>\n".
                      "      <td class=\"hidden-xs\"><a href=/crm/modulos/perfil/index.php?id='".encrypt($row['usuario_id'])."'>".$nombreAlta."</a></td>\n";
                      
          if('0000-00-00 00:00:00'==$row["fecha_inicio"]){
            $salida .=  "      <td><i>Sin agendar</i></td>\n";
          }
          else{
            $salida .=  "      <td>".$row["fecha_inicio"]."</td>\n";
          }
          $salida .= "      <td>".$row['estatus']."</td>\n".
                      "      <td title=\"".$row["descripcion"]."\" onClick=\"alert('".$row["descripcion"]."')\">".
                                 "<a href=\"#\">".substr($row["descripcion"],0,30)."...</a></td>\n";
          
            $salida .= "      <td>";
            
          if('0000-00-00 00:00:00'!=$row["fecha_inicio"]){  
            $salida .= "      <button type=\"button\" title=\"Ir a Calendario\" class=\"btn btn-xs btn-default btn-responsive\" onclick=\"goToCalendarDate('".$row["fecha_inicio"]."');\"><span class=\"glyphicon glyphicon-calendar\"></span></button>";
          }  
          if(hasPermission($orgType,'r')||(hasPermission($orgType,'l')&&$row['usuario_id']==$usuarioId)){
            $salida .= "      <button type=\"button\" id=\"btn_eventComments_".$row['id']."\" title=\"Ver Comentarios\" class=\"btn btn-xs btn-info btn-responsive\" onClick=\"getComments(".$row['id'].",3);\"><span class=\"glyphicon glyphicon-comment\"></span></button>";
          }            
          //if(($usuarioId == $row['usuario_id'])||(array_key_exists(1, $gruposArr))){ 
          
          $btnEditEve = "  <button type=\"button\" id=\"btn_eventEdit_".$row['id']."\" title=\"Editar Evento\" ".
                                  "class=\"btn btn-xs btn-success btn-responsive\" onClick=\"editEvent('".encrypt($row['id'])."');\">".
                                  "<span class=\"glyphicon glyphicon-edit\"></span></button>";
          
          if(5<=$row['tipo_evento_id']&&8>=$row['tipo_evento_id']){
            if((hasPermission(9,'e'))){
              $salida .= $btnEditEve;
            }
          }
          elseif(hasPermission($orgType,'e')||(hasPermission($orgType,'n')&&$row['usuario_id']==$usuarioId)){     
            $salida .= $btnEditEve;
          }            
          //if(array_key_exists(1, $gruposArr)){ 
          /*if(hasPermission($orgType,'d')){ 
            $salida .= "  <button type=\"button\" id=\"btn_eventDelete_".$row['id']."\" title=\"Borrar Evento\" ".
                                  "class=\"btn btn-xs btn-danger btn-responsive\" onClick=\"deleteEvent(".$row['id'].");\">".
                                  "<span class=\"glyphicon glyphicon-remove\"></span></button>";
          }*/
          $salida .= "      </td>\n".
                     "    </tr>\n";  
        }
         $salida .= "  </tbody>\n".
                    "</table>\n</div>";
      }
      //$salida .= "<input type=\"hidden\" id=\"hdn_orgId\" name=\"hdn_orgId\" class=\"form-control\" value=".$orgId.">".
      //           "<input type=\"hidden\" id=\"hdn_orgType\" name=\"hdn_orgType\" class=\"form-control\" value=".$orgType.">";     
    }
    mysqli_close($db_modulos);
    echo $salida;
  } 
  
  // recaba los datos del evento con id especificado para el llenado de los campos en el formulario de diálogo de evento
  if("getEventFields"==$_POST['action']){
    $db_modulos   = condb_modulos();
    $db_usuarios  = condb_usuarios();
    $db_catalogos = condb_catalogos();
    
    $id = isset($_POST['eventId']) ? $_POST['eventId'] : 0;
    $cat = isset($_POST['eveCat']) ? $_POST['eveCat'] : -1;
    
    if(0==$cat)
      $query = "SELECT * FROM calendario WHERE id=".$id;
    elseif(1==$cat)
      $query = "SELECT * FROM orden_trabajo_tareas WHERE tarea_id=".$id;
    // var_dump($query);
    $result = mysqli_query($db_modulos,$query);
    if(!$result){
      $salida = "ERROR||Ocurrió un problema al obtener los datos del evento para su edición";
    }
    else{
      if(0>= mysqli_num_rows($result)){
        $salida = "ERROR||No se encontraron resultados de este registro ";
      }
      else{
        $row = mysqli_fetch_assoc($result);

        if(0==$cat){ 
        $salida = "OK||".
                  $row['titulo']."||".
                  $row['fecha_inicio']."||".
                  $row['fecha_fin']."||".
                  $row['usuario_id']."||".
                  $row['descripcion']."||".
                  $row['tipo_evento_id']."||".
                  $row['todo_dia']."||".
                  $row['estatus_id']."||".
                  $row['origen_id']."||".
                  $row['factible']."||";
                  // se comntaron por que no funcionaban
                  // json_encode($invJsonArray)."||".
                  // json_encode($equJsonArray);
        }
        elseif(1==$cat){
          $invArray = explode(",",substr($row['invitados'],1,-1));
          $equArray = explode(",",substr($row['equipo'],1,-1));
          
          $invJsonArray = array();
          $equJsonArray = array();
          
          foreach($invArray as $key1 => $value1){
            $value1 = str_replace("!","",$value1);
            $queryInv = "SELECT usuario_id,nombre,apellidos FROM ".$cfgTableNameUsr.".usuarios WHERE usuario_id=".$value1;
            $resultInv = mysqli_query($db_usuarios,$queryInv);
            if(!$resultInv){
              error_log("Error al consultar invitado ".$queryInv);
            }
            else{
              $rowInv = mysqli_fetch_assoc($resultInv);
              $invJsonArray[] = array($rowInv['usuario_id'],$rowInv['nombre'],$rowInv['apellidos']);
            }
          }
          foreach($equArray as $key2 => $value2){
            $queryEqu = "SELECT equipo_id,categoria_id,descripcion,num_serie FROM ".$cfgTableNameCat.".cat_mobiliario_equipo WHERE equipo_id=".$value2;
            $resultEqu = mysqli_query($db_catalogos,$queryEqu);
            if(!$resultEqu){
              error_log("Error al consultar equipo ".$queryEqu);
            }
            else{
              $rowEqu = mysqli_fetch_assoc($resultEqu);
              $equJsonArray[] = array($rowEqu['equipo_id'],$rowEqu['categoria_id'],$rowEqu['descripcion'],$rowEqu['num_serie']);
            }
          }
          
          $salida = "OK||".
                  $row['orden_trabajo_id']."||".
                  $row['titulo']."||".
                  $row['fecha_inicio']."||".
                  $row['fecha_fin']."||".
                  $row['usuario_id']."||".
                  $row['descripcion']."||".
                  $row['estatus_id']."||".
                  $row['fecha_alta']."||".
                  $row['factible']."||".
                  json_encode($invJsonArray)."||".
                  json_encode($equJsonArray);
                  
          log_write("DEBUG: GET-EVENT-FIELDS: equipoArray  ".print_r($equJsonArray,true),4);
          log_write("DEBUG: GET-EVENT-FIELDS: inventArray ".print_r($invJsonArray,true),4);       
        }          
      }
    }
    mysqli_close($db_modulos);
    mysqli_close($db_usuarios);
    mysqli_close($db_catalogos);
    echo $salida; 
  }
  
  //llamada de ajax. manda a guardar los datos de cualquier evento desde cualquier modulo (se debe especificar qué tipo de modulo es)
  /* 0: Prospectos
     1: Clientes
     3: Sitios  
  */
  if("saveEvent"==$_POST['action']){
    $db_modulos   = condb_modulos();
    
    log_write("DEBUG: SAVE-EVENT: Se va a guardar un evento",4);
    
    $titulo    = isset($_POST['titulo']) ? $_POST['titulo'] : "";
    $fechaini  = isset($_POST['fechaIni']) ? $_POST['fechaIni'] : "";
    $fechafin  = isset($_POST['fechaFin']) ? $_POST['fechaFin'] : "";
    $estatus   = isset($_POST['eveEstat']) ? $_POST['eveEstat'] : -1;
    $tipo      = isset($_POST['eveTipo']) ? $_POST['eveTipo'] : -1;  //si es llamada, linea de vista etc
    $eveAction = isset($_POST['eveAction']) ? $_POST['eveAction'] : -1; //si se manda a guardar nuevo o editar
    $eveId     = isset($_POST['eveId']) ? decrypt($_POST['eveId']) : -1;   //id del evento por si se quiere editar
    $orgType   = isset($_POST['orgType']) ? $_POST['orgType'] : -1;//si es cliente, prospecto, sitio etc
    $desc      = isset($_POST['eveDesc']) ? $_POST['eveDesc'] : "";
    $orgSiteType = isset($_POST['orgSiteType']) ? $_POST['orgSiteType'] : -1;
    $invitado  = isset($_POST['invitadoArray']) ? json_decode($_POST['invitadoArray'],true) : "";
    $equipo    = isset($_POST['equipoArray']) ? json_decode($_POST['equipoArray'],true) : "";
    $fact      = (isset($_POST['factible'])&&(3==$_POST['eveEstat'])&&(5==$_POST['eveTipo'])) ? $_POST['factible'] : 0;
    
    $id = isset($_POST['id']) ? $_POST['id'] : 0;
    $inv = ",";
    $equ = ",";
    
    $fechaNow = date("Y-m-d H:i:s");
    
    switch($orgType){
      case 0:  //si se trata de un prospecto
        $fieldname = "prospecto_id";
        $typename = "prospecto";
        $tablename = "prospectos";
      break;
      case 1:  //si se trata de un cliente
        $fieldname = "cliente_id";
        $typename = "cliente";
        $tablename = "clientes";
      break;
      case 2:  //si se trata de un sitio
        $fieldname = "sitio_id";
        $typename = "sitio";
        $tablename = "sitios";
      break;    
    }
    
    foreach($invitado as $key=>$value){
      $inv .= $value."!,";
    }
    foreach($equipo as $key=>$value){
      $equ .= $value.",";
    }
    
    log_write("DEBUG: SAVE-EVENT: orgtype ".$orgType,4);
    log_write("DEBUG: SAVE-EVENT: orgId ".$id,4);
    log_write("DEBUG: SAVE-EVENT: eveid ".$eveId,4);
    
    if((-1==$orgType||0==$id)&&(-1==$eveId)&&((0>=strlen($fechaini))||(0>=strlen($fechafin)))){
      log_write("ERROR: SAVE-EVENT: Este evento carece de fechas y no está asociado a ningún registro",4);
      $salida = "ERROR||Este evento carece de fechas y no está asociado a ningún registro";
      goto saveEvea;
    }
    if((5<=$tipo&&8>=$tipo)&&(-1==$orgType||0==$id)&&(0>=$eveId)){
      log_write("ERROR: SAVE-EVENT: Este evento es de tipo soporte y no está asociado a ningún origen",4);
      $salida = "ERROR||Este tipo de evento requiere estar asociado a un cliente, prospecto o sitio, puede darlo de alta en su respectivo módulo";
      goto saveEvea;
    }
        
    if(0==$eveAction){
      if((5<=$tipo&&8>=$tipo)&&!isInGroup(7)){
        $fechaIni = "0000-00-00 00:00:00";
        $fechaFin = "0000-00-00 00:00:00";
      }
      if(!hasPermission($orgType,'a'))
        $estatus = 1;
        
      log_write("DEBUG: SAVE-EVENT: Se va a guardar un evento NUEVO",4);
      $query = "INSERT INTO calendario(tipo_origen,".
                                       "origen_id,".
                                       "titulo,".
                                       "fecha_inicio,".
                                       "fecha_fin,".
                                       "usuario_id,".
                                       "descripcion,".
                                       "tipo_evento_id,".
                                       "todo_dia,".
                                       "estatus_id,".
                                       "fecha_alta,".
                                       "invitados,".
                                       "equipo,".
                                       "factible) ".
                       "VALUES(".$orgType.",".
                                $id.",'".
                                $titulo."','".
                                $fechaini."','".
                                $fechafin."',".
                                $usuarioId.",'".
                                $desc."',".
                                $tipo.",0,".
                                $estatus.",'".
                                $fechaNow."','".
                                $inv."','".
                                $equ."',".
                                $fact.")";
      $result = mysqli_query($db_modulos,$query);
      
      log_write("DEBUG: SAVE-EVENT: ".$query,4);
      
      if(!$result){
        log_write("ERROR: SAVE-EVENT: No se insertó el evento",4);
        $salida = "ERROR|| Ocurrió un error al insertar el evento a la db";
      }
      else{
        $newId = mysqli_insert_id($db_modulos);
        $usuarioNom = get_userRealName($usuarioId);
        
        foreach($invitado as $key=>$value){
          $nombre = get_userRealName($value);
          $msg = "Hola, ".$nombre.".<br><br>".
                 $usuarioNom." te ha invitado a asistir al evento ".$titulo." que comienza en ".$fechaini." y termina en ".$fechafin.":<br><br> ".
                 "<i>".$desc."</i><br><br>".
                 "Para confirmar o cancelar tu asistencia haz click en una de las opciones que se presentan a continuación<br><br> ".
                 "<input type=\"button\" class=\"btn btn-info\" onClick=\"confirmEveAssist(".$newId.",0)\" value=\"Asistiré\"></input>&nbsp".
                 "<input type=\"button\" class=\"btn btn-danger\" onClick=\"confirmEveAssist(".$newId.",1)\" value=\"No Asistiré\"></input><br><br>".
                 "Sistema CRM Coeficiente";
          $asunto = "Invitación a cita";
          if(!sendPrivMsg($usuarioId,$value,$asunto,$msg,1))
            log_write("ERROR: SAVE-EVENT: No se envió mensaje al usuario ".$nombre,4);
          else
            log_write("DEBUG: SAVE-EVENT: Se envió mensaje al usuario ".$nombre,4);
        }
        if(5<=$tipo&&8>=$tipo){
          $asunto = "Programación de evento de soporte";
          $msg = "Hola, ".$nombre.".<br><br>".
                 $usuarioNom."Ha programado un evento del tipo cuya fecha está pendiente por programar.<br><br>".
                 "Por favor dirígete al módulo de Instalaciones en el Menú de soporte para ver detalles de este evento";
          sendPrivMsgDepartment($usuarioId,7,0,$asunto,$msg,2,1);
        }
        
        writeOnJournal($usuarioId,"Ha insertado el evento [".$newId."] para el ".$typename." con id ".$id);
        $salida = "OK|| Los datos de este evento han sido guardados con éxito";          
      }  
    }
    
    elseif(1==$eveAction){
      log_write("DEBUG: SAVE-EVENT: Se va a actualizar un evento EXISTENTE",4);
      $campos = array("id"             => $eveId,
                      "tipo_origen"    => $orgType,
                      "origen_id"      => $id,
                      "titulo"         => $titulo ,
                      "fecha_inicio"   => $fechaini,
                      "fecha_fin"      => $fechafin,
                      "usuario_id"     => $usuarioId,
                      "descripcion"    => $desc,
                      "tipo_evento_id" => $tipo,
                      "todo_dia"       => 0,
                      "estatus_id"     => $estatus,
                      "invitados"      => $inv,
                      "equipo"         => $equ,
                      "factible"       => $fact);
                      
      $query = "SELECT * FROM calendario WHERE id=".$eveId;     
      
      $result = mysqli_query($db_modulos,$query);     
      
      log_write("DEBUG: SAVE-EVENT: ".$query,4);
      
      if(!$result){
        log_write("ERROR: SAVE-EVENT: No se pudo obtener los datos del evento",4);
        $salida = "ERROR|| Ocurrió un problema en la consulta de los datos para su actualización";      
      }
      else{
        $row = mysqli_fetch_assoc($result);
        
        $diff = array_diff_assoc($row,$campos);
        
        $invArray = explode(",",substr($row['invitados'],1,-1));
        
        $diffInv = array_diff_assoc($invitado,$invArray);
        
        foreach($diffInv as $key => $value){
          $nombre = get_userRealName($value);
          $usuarioNom = get_userRealName($usuarioId);
          $msg = "Hola, ".$nombre.".<br><br>".
                 $usuarioNom." te ha invitado a asistir al evento ".$titulo." que comienza en ".$fechaini." y termina en ".$fechafin.":<br><br> ".
                 "<i>".$desc."</i><br><br>".
                 "Para confirmar o cancelar tu asistencia haz click en una de las opciones que se presentan a continuación<br><br> ".
                 "<input type=\"button\" class=\"btn btn-info\" onClick=\"confirmEveAssist(".$eveId.",0)\" value=\"Asistiré\"></input>&nbsp".
                 "<input type=\"button\" class=\"btn btn-danger\" onClick=\"confirmEveAssist(".$eveId.",1)\" value=\"No Asistiré\"></input><br><br>".
                 "Sistema CRM Coeficiente";
          $asunto = "Invitación a cita";
          if(!sendPrivMsg($usuarioId,$value,$asunto,$msg,1))
            log_write("ERROR: SAVE-EVENT: No se envió mensaje al usuario ".$nombre,4);
          else
            log_write("DEBUG: SAVE-EVENT: Se envió mensaje al usuario ".$nombre,4);
        }
               
        log_write("DEBUG: SAVE-EVENT: campos nuevos ".print_r($campos,true),4);
        log_write("DEBUG: SAVE-EVENT: campos actuales ".print_r($row,true),4);
        log_write("DEBUG: SAVE-EVENT: campos diferentes ".print_r($diff,true),4);
        
        $query2 = "UPDATE calendario SET titulo='".$titulo."', fecha_inicio='".$fechaini."', fecha_fin='".$fechafin."', descripcion='".$desc."', tipo_evento_id=".$tipo.", estatus_id=".$estatus.", invitados='".$inv."', equipo='".$equ."', factible=".$fact." WHERE id=".$eveId;
           
        $result2 = mysqli_query($db_modulos,$query2);
        
        log_write("DEBUG: SAVE-EVENT: ".$query2,4);
        
        if(!$result2){
          log_write("ERROR: SAVE-EVENT: No se pudo actualizar el evento",4);
          $salida = "ERROR|| Ocurrió un error al actualizar el evento en la db";
        }
        else{
          foreach($diff as $key => $value){
            writeOnJournal($usuarioId,"Ha modificado el campo \"".$key."\" de [".$value."] a [".$campos[$key]."]"." para evento id [".$id."]");
          }
          log_write("OK: SAVE-EVENT: El evento ha sido actualizado",4);
          $salida = "OK|| Los datos de este evento han sido guardados con éxito";
          
          if(1==$fact&&3==$estatus&&5==$tipo){        
            updateOriginStatus($orgType,3,$id);
            //  cambio de estado a propuesta
            if(0 == $orgType){
              // estatus al que cambia, y id del prospecto
              sendEstatusChangeEmail(3, $id);
            }//fin del if
          }
        }
      }                             
    }
    if(1==$fact&&3==$estatus&&5==$tipo){
      if(2==$orgType)
        updateOriginStatus($orgSiteType,3,$id);
        //  cambio de estado de sitio
      else{
       updateOriginStatus($orgType,3,$id);
        //  cambio de estado a propuesta
        if(0 == $orgType){
          // estatus al que cambia, y id del prospecto
          sendEstatusChangeEmail(3, $id);
        }//fin del if 
      }
    }
    saveEvea: 
    mysqli_close($db_modulos);
    echo $salida; 
  }
  
  //llamada de ajax. Borra el evento seleccionado
  if("deleteEvent"==$_POST['action']){
    $db_modulos   = condb_modulos();
    
    $id = isset($_POST['eventId']) ? $_POST['eventId'] : 0;
    
    $query = "DELETE FROM calendario WHERE id=".$id;
    
    $result = mysqli_query($db_modulos,$query);
    
    if(!$result){
      $salida = "ERROR||El evento no pudo borrarse";
    }
    else{
      writeOnJournal($usuarioId,"Ha eliminado evento con id [".$id."]");
      $salida = "OK||El evento se ha eliminado correctamente";
    }
    
    mysqli_close($db_modulos);
    echo $salida;
  }
  
  if("confirmEveAssist"==$_POST['action']){ 
    $db_modulos   = condb_modulos();
    $eveId = isset($_POST['eventId']) ? $_POST['eventId'] : 0;
    $ao = isset($_POST['assistOption']) ? $_POST['assistOption'] : 0;
    $nombre = get_userRealName($usuarioId);
    $queryCon = "SELECT usuario_id,titulo,fecha_inicio,fecha_fin FROM calendario WHERE id=".$eveId;
    $resultCon = mysqli_query($db_modulos,$queryCon);
    if(!$resultCon){
      $salida = "ERROR||Ocurrió un problema al obtener los datos del evento";
    }    
    else{
      $row = mysqli_fetch_assoc($resultCon);
      switch($ao){
        case 0:
          $confOp = "confirmado";
          $query = "UPDATE calendario SET invitados = REPLACE (invitados,',".$usuarioId."!,',',".$usuarioId.",')".
                   "WHERE id=".$eveId;
          $msg = $nombre." Ha confirmado que asistirá al evento ".$row['titulo']." que programaste para el día ".$row['fecha_inicio'];
        break;
        case 1:
          $confOp = "cancelado";
          //$query = "UPDATE calendario SET invitados = REPLACE (invitados,',".$usuarioId."!,',',')".
          $query = "UPDATE calendario SET invitados = REPLACE (invitados,',".$usuarioId."!,',',".$usuarioId."*,')".
                   "WHERE id=".$eveId;
          $msg = $nombre." Ha confirmado que no asistirá al evento ".$row['titulo']." que programaste para el día ".$row['fecha_inicio'];
        break;
      }
      log_write("DEBUG: CONFRIM-EVENT-ASSIST: ".$query,4);
      $result = mysqli_query($db_modulos,$query);
      if(!$result){
        $salida = "ERROR|| Ocurrió un error en el procedimiento. No se ha ".$confOp." su asistencia";
      }
      else{
        sendPrivMsg(0,$row['usuario_id'],"Confirmación de asistencia",$msg,2);
        writeOnJournal($usuarioId,"Ha ".$confOp." su asistencia al evento id [".$id."]");
        $salida = "OK||Ha ".$confOp." su asistencia al evento";
      }        
    }     
    mysqli_close($db_modulos);
    echo $salida;
  }
  /*****Fin de apartado*****/
  /*********Funciones para creación, edición y borrado de cotizaciones**********/
  //llamada de ajax. toma las cotizaciones asociadas a un prospecto, cliente, sitio etc...
  if("getPrices" == $_POST["action"]){
    $db_modulos   = condb_modulos();
    $orgId = isset($_POST['orgId']) ? $_POST['orgId'] : -1;
    $option = isset($_POST['option']) ? $_POST['option'] : -1;
    $orgType = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
    $orgSiteType = isset($_POST['orgSiteType']) ? $_POST['orgSiteType'] : -1;
    $subtotal = 0;
    $impuesto = 0;
    $total    = 0;
    switch($orgType){
      case 0:  //si se trata de un prospecto
        $typename = "prospecto";
        $tablename = "prospectos";
        $idfieldname = "prospecto_id";
        $stataddquer = "cli.estatus,";
      break;
      case 1:  //si se trata de un cliente
        $typename = "cliente";
        $tablename = "clientes";
        $idfieldname = "cliente_id";
        $stataddquer = "cli.estatus,";
      break;
      case 2:  //si se trata de un sitio
        $typename = "sitio";
        $tablename = "sitios";
        $idfieldname = "sitio_id";
        $stataddquer = "";
      break;    
    }
    if(1==$option)
      $orgId = decrypt($orgId);
    //$query = "SELECT cot.*, con.* FROM cotizaciones AS cot, contactos AS con WHERE cot.tipo_origen=".$orgType." AND cot.origen_id=".$orgId." AND cot.contacto_id=con.contacto_id";
    $query = "SELECT cot.*, con.*, ubi.*, ".$stataddquer. " cli.representante_id, cli.tipo_persona, usr.ubicacion_id, costa.estatus AS cotSta, costa.color AS cotStaCol, cli.estatus, cot.fecha_alta AS fecha_alta_cot ".
             "FROM ".$cfgTableNameMod.".cotizaciones AS cot, ".
                     $cfgTableNameMod.".contactos AS con, ".
                     $cfgTableNameCat.".cat_coe_ubicacion AS ubi, ".
                     $cfgTableNameCat.".cat_cotizacion_estatus AS costa, ".
                     $cfgTableNameUsr.".usuarios AS usr, ".
                     $cfgTableNameMod.".".$tablename." AS cli ".
             "WHERE cot.tipo_origen=".$orgType.
             " AND cot.origen_id=".$orgId.
             " AND usr.ubicacion_id=ubi.ubicacion_id".
             " AND costa.cotizacion_estatus_id=cot.cotizacion_estatus_id".
             " AND cot.usuario_alta=usr.usuario_id".
             " AND cot.contacto_id=con.contacto_id".
             " AND ".$idfieldname."=".$orgId;
  //consulta para saber  si la cotiacion esta autorizada
      // $dbname = "prospectos";
      // $fieldname = "prospecto_id";
      // $typename = "prospecto";
      // $asignfield = "asignadoa";
      // $nameFieldName = " ";
      // $query1 = "SELECT cot.*, cot.fecha_alta AS cotfecalta, con.*, tit.abreviatura, cli.*, cli.logo_ruta AS cliLogo, " . $nameFieldName . " ubi.*, usr.nombre AS venNom, usr.apellidos AS venApe, usr.departamento AS venDepto, usr.puesto AS venPuesto, usr.extension AS venExt, usr.tel_movil AS venTelMovil, usr.email AS venEmail, usr.ubicacion_id " .
      //   "FROM " . $cfgTableNameMod . ".cotizaciones AS cot, " .
      //   $cfgTableNameMod . ".contactos AS con, " .
      //   $cfgTableNameMod . "." . $dbname . " AS cli, " .
      //   $cfgTableNameCat . ".cat_coe_ubicacion AS ubi, " .
      //   $cfgTableNameCat . ".cat_persona_titulo AS tit, " .
      //   $cfgTableNameUsr . ".usuarios AS usr " .  
      //   "WHERE cot.cotizacion_id=" . $pricId .
      //   " AND cot.contacto_id=con.contacto_id" .
      //   " AND cli." . $fieldname . "=cot.origen_id" .
      //   " AND cot.tipo_origen=" . $orgType .
      //   " AND usr.ubicacion_id=ubi.ubicacion_id" .
      //   " AND cli.asignadoa=usr.usuario_id" .
  //   " AND con.titulo_id=tit.persona_titulo_id";
    $result = mysqli_query($db_modulos,$query);
    if(!$result){
      $salida = "ERROR|| No se pudo obtener las cotizaciones para este registro ";
    }
    else{
      if(hasPermission(4,'w')){         
        $salida =  "<button type=\"button\" id=\"btn_priceNew\" title=\"Nueva Cotización\" class=\"btn btn-default\" onClick=\"openNewPrice();\"><span class=\"glyphicon glyphicon-asterisk\"></span> Nueva Cotización</button><br><br>";
      }
      if(0==mysqli_num_rows($result)){
        $salida .= "No existen cotizaciones para el ".$typename." con este id. haga clic en <b>Nueva cotización</b> para agregar una.";
      }
      else{
        $salida .= "<div class=\"table-responsive\"><table id=\"tbl_prices\" class=\"table table-striped\">\n".
                   "  <thead>\n".
                   "    <tr>\n".
                   "      <th class=\"hidden-xs\">Estatus</th>\n".
                   "      <th class=\"hidden-xs\"></th>\n".
                   "      <th class=\"hidden-xs\">Id</th>\n".
                   "      <th>Fecha Alta</th>\n".
                   "      <th class=\"hidden-xs\">Ubicación</th>\n".
                   "      <th>Contacto</th>\n".                  
                   "      <th>Cotizado por:</th>\n".
                   "      <th class=\"hidden-xs\">Subtotal</th>\n".
                   "      <th class=\"hidden-xs\">Impuesto</th>\n".
                   "      <th>Total</th>\n".
                   "      <th class=\"hidden-xs\">Notas</th>\n".
                   "      <th>Acciones</th>\n".
                   "    </tr>\n".
                   "  </thead>\n".
                   "  <tbody>\n";                
        while($row = mysqli_fetch_assoc($result)){
          $nombreAlta = get_userRealName($row['usuario_alta']);
          // var_dump($query);
          $salida .=  "    <tr>\n";
            if($row['contrato'] == 0||$row['contrato']==4)
              $salida .=  "      <td class=\"hidden-xs\"><span class=\"glyphicon glyphicon-inbox\" title=\"Pendiente de Anexar/Firmar\"></span></td>\n";
            elseif($row['contrato'] == 1)
              $salida .=  "      <td class=\"hidden-xs\"><span class=\"glyphicon glyphicon-saved\" style=\"color:blue\" title=\"Contrato Firmado\"></span></td>\n";
            elseif($row['contrato'] == 2)
              $salida .=  "      <td class=\"hidden-xs\"><span class=\"glyphicon glyphicon-copy\"  style=\"color:green\" title=\"Anexado a Contrato\"></span></td>\n";
            elseif($row['contrato'] == 3)
           $salida .=  "      <td class=\"hidden-xs\"><span class=\"glyphicon glyphicon-ok\"  style=\"color:blue\" title=\"Aceptado por Prospecto/Cliente\"></span></td>\n";
           $salida .=  "      <td class=\"hidden-xs\"><span class=\"glyphicon glyphicon-search\" style=\"color:".$row['cotStaCol']."\"  title=\"".$row['cotSta']."\"></span></td>\n";
           $salida .= "      <td class=\"hidden-xs\">".$row['cotizacion_id']."</td>\n".
                      "      <td>".$row['fecha_alta_cot']."</td>\n".
                      "      <td class=\"hidden-xs\">".$row['ubicacion_nombre']."</td>\n".
                      "      <td>".$row['nombre']." ".$row['apellidos']."</td>\n".
                      "      <td><a href=/crm/modulos/perfil/index.php?id='".encrypt($row['usuario_alta'])."'>".$nombreAlta."</a></td>\n";              
           $salida .= "      <td class=\"hidden-xs\">$".number_format($row['subtotal'], 2, '.', ',')."</td>\n".
                      "      <td class=\"hidden-xs\">$".number_format($row['impuesto'], 2, '.', ',')."</td>\n".
                      "      <td>$".number_format($row['total'], 2, '.', ',')."</td>\n".
                      "      <td class=\"hidden-xs\" title=\"".$row["notas"]."\" onClick=\"alert('".$row["notas"]."')\">".
                                 "<a href=\"#\">".substr($row["notas"],0,30)."...</a></td>\n";
                  // boton de detalles de cotizacion
           $salida .= "      <td>".
                      "        <button type=\"button\" id=\"btn_priceDetails_".$row['cotizacion_id']."\" title=\"Ver Detalles\" class=\"btn btn-xs btn-primary btn-responsive\" onClick=\"getPriceDetails('".encrypt($row['cotizacion_id'])."');\"><span class=\"glyphicon glyphicon-list\"></span></button>";
          if(0==$option){
            // boton para edicion de cotizaciones
            if((hasPermission($orgType,'e')||(hasPermission($orgType,'n')&&$row['usuario_alta']==$usuarioId))&&(0==$row['contrato'])){      
              $salida .= "  <button type=\"button\" id=\"btn_priceEdit_".$row['cotizacion_id']."\" title=\"Editar Cotización\" ".
                                    "class=\"btn btn-xs btn-success btn-responsive\" onClick=\"editPrice('".encrypt($row['cotizacion_id'])."');\">".
                                    "<span class=\"glyphicon glyphicon-edit\"></span></button>";
            }
            if(hasPermission($orgType,'d')&&(5>$row['cotizacion_estatus_id'])){ 
              // boton para eliminacion de cotizaciones
              $salida .= "  <button type=\"button\" id=\"btn_priceDelete_".$row['cotizacion_id']."\" title=\"Borrar Cotización\" ".
                                    "class=\"btn btn-xs btn-danger btn-responsive\" onClick=\"deletePrice(".$row['cotizacion_id'].");\">".
                                    "<span class=\"glyphicon glyphicon-remove\"></span></button>";
            }
            if((hasPermission($orgType,'w')||hasPermission($orgType,'m'))&&(2!=$row['cotizacion_estatus_id'])&&(4!=$row['cotizacion_estatus_id'])&&(6!=$row['cotizacion_estatus_id'])){
              // imprimir cotizacion y generar cotizacion pra
              $salida .= "    <button type=\"button\" title=\"Generar PDF\" class=\"btn btn-xs btn-default btn-responsive\" onclick=\"printPrice('".encrypt($row['cotizacion_id'])."',0);\"><span class=\"glyphicon glyphicon-print\"></span></button>".
                         "    <button type=\"button\" title=\"Generar PDF y enviar por e-mail \" class=\"btn btn-xs btn-default btn-responsive\" onclick=\"printPrice('".encrypt($row['cotizacion_id'])."',1);\"><span class=\"glyphicon glyphicon-envelope\"></span></button>";
              if(4==$row['estatus']&&5==$row['cotizacion_estatus_id']){
                if(0>=$row['representante_id']&&1!=$row['tipo_persona']){
                  $salida .= "<span class=\"glyphicon glyphicon-remove\" style=\"color:red\" title=\"Debe agregarse un representante legal antes de proseguir con la generación de contrato\"></span>";
                }
                else{
                  $salida .= "    <button type=\"button\" title=\"Generar Contrato\" class=\"btn btn-xs btn-success btn-responsive\" onclick=\"openNewContract('".encrypt($row['cotizacion_id'])."',".$row['contrato'].");\"><span class=\"glyphicon glyphicon-file\"></span></button>";
                }
              }                               
            }
          }
          if(1==$option){
            $salida .= "    <button type=\"button\" title=\"Generar Orden de Dimensionamiento\" class=\"btn btn-xs btn-default btn-responsive\" onclick=\"newDimWorkOrder('".encrypt($row['cotizacion_id'])."');\"><span class=\"glyphicon glyphicon-wrench\">Generar Orden</span></button>";
          }
          // **********************************
          // boton de autorizacon de cotizacion
          // **********************************
          if($_SESSION['usrId'] == 69 || $_SESSION['usrId'] == 3 || $_SESSION['usrId'] == 72 && $row['estatus'] == 6 && $row['cotSta'] == 'Creado' || $row['cotSta'] == 'Precio Especial Autorizado'){
            $emailhash = encrypt(uniqId());
                $salida .= "    <button type=\"button\" id=\"btn_priceDetails_".$row['cotizacion_id']."\" title=\"Autorizar cotizacion\" class=\"btn btn-xs btn-primary btn-responsive\" onClick=\"cotAutAdmin('".
                encrypt($row['cotizacion_id'])."','".$emailhash."');\"><span class=\"glyphicon glyphicon-ok\"></span></button>";
          }
          // ***********************************
          // /boton de autorizacon de cotizacion
          // ***********************************
          $salida .= "      </td>\n".
                     "    </tr>\n";  
        }
         $salida .= "  </tbody>\n".
                    "</table>\n</div>";
      }
      $salida .= "<input type=\"hidden\" id=\"hdn_orgId\" name=\"hdn_orgId\" class=\"form-control\" value=".$orgId.">".
                 "<input type=\"hidden\" id=\"hdn_orgType\" name=\"hdn_orgType\" class=\"form-control\" value=".$orgType.">";     
    }
    // var_dump($query);
    mysqli_close($db_modulos);
    echo $salida;
  }

  //obtiene los datos detallados de una cotización para mostrarla y para editarla
  if("getPriceDetails" == $_POST["action"]){
    $db_modulos   = condb_modulos();
    $db_catalogos = condb_catalogos();
    
    $priceId = isset($_POST['priceId']) ? decrypt($_POST['priceId']) : -1;
    $orgType = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
    $option  = isset($_POST['option']) ? $_POST['option'] : -1;
    $subtotal = 0;
    
    $productArr = array();
    switch($orgType){
      case 0:  //si se trata de un prospecto
        $dbname = "prospectos";
        $fieldname = "prospecto_id";
        $typename = "prospecto";
        $nameFieldName = " ";
      break;
      case 1:  //si se trata de un cliente
        $dbname = "clientes";
        $fieldname = "cliente_id";
        $typename = "cliente";
        $nameFieldName = " ";
      break;
      case 2:  //si se trata de un sitio
        $dbname = "sitios";
        $fieldname = "sitio_id";
        $typename = "sitio";
        $nameFieldName = "cli.nombre AS nombre_comercial, con.nombre AS nombre, ";
      break;    
    }
    
    $query = "SELECT cot.*, cot.fecha_alta AS cotfecalta, con.*, cli.*, ".$nameFieldName." ubi.*, usr.ubicacion_id ".
             "FROM ".$cfgTableNameMod.".cotizaciones AS cot, ".
                     $cfgTableNameMod.".contactos AS con, ".
                     $cfgTableNameMod.".".$dbname." AS cli, ".
                     $cfgTableNameCat.".cat_coe_ubicacion AS ubi, ".
                     $cfgTableNameUsr.".usuarios AS usr ".
             "WHERE cot.cotizacion_id=".$priceId.
             " AND cot.contacto_id=con.contacto_id".
             " AND cli.".$fieldname."=cot.origen_id".
             " AND cot.tipo_origen=".$orgType.
             " AND usr.ubicacion_id=ubi.ubicacion_id".
             " AND cot.usuario_alta=usr.usuario_id"; 
     
    $result = mysqli_query($db_modulos,$query);
    if(!$result){
      $salida = "ERROR|| No se pudo obtener los datos para esta cotización";
    }
    else{
      $row = mysqli_fetch_assoc($result);
      if(2!=$row['cotizacion_estatus_id']){
      $salida = "  <div>".
                "    <button type=\"button\" title=\"Generar PDF\" class=\"btn btn-sm btn-default\" onclick=\"printPrice('".encrypt($row['cotizacion_id'])."',0);\"><span class=\"glyphicon glyphicon-print\"> Generar PDF</span></button>".
                "    <button type=\"button\" title=\"Generar PDF y enviar por e-mail\" class=\"btn btn-sm btn-default\" onclick=\"printPrice('".encrypt($row['cotizacion_id'])."',1);\"><span class=\"glyphicon glyphicon-envelope\"> Generar PDF y enviar por e-mail</span></button>".
                " </div>";
      }
      
      $salida .=  "<div class=\"table-responsive\"><table id=\"tbl_priceDetails\" class=\"table table-striped\">\n".
                 "  <tbody>\n".
                 "    <tr>".
                 "      <td colspan=\"2\"><b>Clave</b></td>".
                 "      <td colspan=\"3\">".$row['ubicacion_clave']."-".$row['cotizacion_id']."</td>".
                 "    </tr>".
                 "    <tr>".
                 "      <td colspan=\"2\"><b>Fecha</b></td>".
                 "      <td colspan=\"3\">".$row['cotfecalta']."</td>".
                 "    </tr>".
                 "    <tr>".
                 "      <td colspan=\"2\"><b>Cliente</b></td>".
                 "      <td colspan=\"3\">".$row['nombre_comercial']."</td>".
                 "    </tr>".
                 "    <tr>".
                 "      <td colspan=\"2\"><b>Contacto</b></td>".
                 "      <td colspan=\"3\">".$row['nombre']." ".$row['apellidos']."</td>".
                 "    </tr>".
                 /*"    <tr>".
                 "      <td colspan=\"2\"><b>Email</b></td>".
                 "      <td colspan=\"3\">".$row['website']."</td>".
                 "    </tr>".*/
                 "    <tr>".
                 "      <td colspan=\"2\"><b>Teléfono</b></td>".
                 "      <td colspan=\"3\">".$row['telefono']."</td>".
                 "    </tr>".
                 "    <tr>".
                 "      <td colspan=\"2\"><b>Servicio de Soporte:</b></td>".
                 "      <td colspan=\"3\">Tel.&nbsp".$supportServiceTel." ó ".$supportServiceMail."</td>".
                 "    </tr>".
                 "    <tr>".
                 "      <td colspan=\"2\">Notas:</td>".
                 "      <td colspan=\"3\" style=\"word-break:break-all;\">".$row['notas']."</td>".
                 "    </tr>".
                 "    <tr>".
                 "      <td class=\"td-blue\"><b>Cantidad</b></td>".
                 "      <td class=\"td-blue\"><b>Descripción</b></td>".
                 "      <td class=\"td-blue\"><b>Precio Unitario</b></td>".
                 "      <td class=\"td-blue\"><b>Total</b></td>".
                 "      <td class=\"td-blue\"><b>Acciones</b></td>".                
                 "    </tr>";
    
      $query2 = "SELECT rcp.relacion_id, cvs.*, cvv.cantidad_vigencia, cvv.factor, rcp.cantidad, cvts.categoria_id, ".
                "rcp.producto_precio_especial, rcp.producto_autorizado_por, rcp.sitio_asignado, rcp.vigencia, cvv.cantidad_vigencia ".
                "FROM ".$cfgTableNameCat.".cat_venta_servicios AS cvs, ".
                        $cfgTableNameCat.".cat_venta_tipo_servicios AS cvts, ".
                        $cfgTableNameMod.".rel_cotizacion_producto AS rcp, ".
                        $cfgTableNameCat.".cat_venta_vigencia AS cvv ".
                "WHERE cvs.servicio_id = rcp.producto_id ".
                "AND cvs.tipo_servicio = cvts.tipo_servicio_id ".
                "AND rcp.vigencia = cvv.vigencia_id ".
                "AND rcp.cotizacion_id = ".$priceId;
                
      $result2 = mysqli_query($db_modulos,$query2);
    
      if(!$result2){
        $salida = "ERROR|| No se pudo obtener los productos para esta cotización";  
      }
      else{
        //filas que despliegan los productos
        
        if(0<mysqli_num_rows($result2)){
          
          while($row2 = mysqli_fetch_assoc($result2)){
            $regionCot = $row2['region_id'];
            $precioNor = (1==$row2['tipo_cobro'])?$row2['precio_'.$row2['cantidad_vigencia'].'m']:$row2['precio_lista'];;
                    
            if(-1!=$row2['producto_precio_especial']){     
              $precio = $row2['producto_precio_especial'];
            }
            else{
              if(0==$option){
                $precio = $precioNor;
                /*if(1==$row2['tipo_cobro']){ //si es pago recurrente por mes
                  $precio = $row2['precio_'.$row2['cantidad_vigencia'].'m'];
                }
                else{
                  $precio = $row2['precio_lista']; 
                }*/
              }
              elseif(1==$option){
                $precio = -1;
              } 
            }
            
            $subtotal = $subtotal+($row2['cantidad']*$precio);
                
            if(0==$option){
              $salida .=  "    <tr>".
                          // cantidad
                          "      <td>".$row2['cantidad']."</td>".
                          // descripcion
                          "      <td>".$row2['servicio']." (a ".$row2['cantidad_vigencia']." meses) </td>";
                          // precio unitario
              $salida .=  "      <td>$".number_format($precio, 2, '.', ','); 

              if(-1==$row2['producto_autorizado_por']&&-1!=$row2['producto_precio_especial']){
                $btnSite = "";
                if(1==$row2['tipo_cobro']){
                  if($row2['precio_'.$row2['cantidad_vigencia'].'m']<$row2['producto_precio_especial']){
                    $nom = "  Precio por encima del base. No requiere Autorización";
                    $btnSite = "         <button type=\"button\" title=\"Asignar Sitio\" class=\"btn btn-sm btn-default\" onclick=\"openSiteToConnection(".$row[$fieldname].",".$orgType.",'".encrypt($row2['relacion_id'])."');\"><span class=\"glyphicon glyphicon-map-marker\"></span></button>";
                  }
                  else{
                    $nom = "  Pendiente de autorizar";     
                  } 
                }
                else{
                  if($row2['precio_lista']<$row2['producto_precio_especial']){
                    $nom = "  Precio por encima del base. No requiere Autorización";
                    $btnSite = "         <button type=\"button\" title=\"Asignar Sitio\" class=\"btn btn-sm btn-default\" onclick=\"openSiteToConnection(".$row[$fieldname].",".$orgType.",'".encrypt($row2['relacion_id'])."');\"><span class=\"glyphicon glyphicon-map-marker\"></span></button>";
                  }
                  else{
                    $nom = "  Pendiente de autorizar";     
                  } 
                }
              }
              else{
                $nom = "  Autorizado por: ".get_userRealName($row2['producto_autorizado_por']);
                $btnSite = "         <button type=\"button\" title=\"Asignar Sitio\" class=\"btn btn-sm btn-default\" onclick=\"openSiteToConnection(".$row[$fieldname].",".$orgType.",'".encrypt($row2['relacion_id'])."');\"><span class=\"glyphicon glyphicon-map-marker\"></span></button>";
              }    
              if(-1!=$row2['producto_precio_especial']){ 
                $salida .= "<span class=\"glyphicon glyphicon-alert\" title=\"Precio normal: $".number_format($precioNor, 2, '.', ',').$nom."\"></span></td>"; 
              }
              else
                  $salida .= "      </td>"; 
              $salida .= "      <td>$".number_format(($row2['cantidad']*$precio), 2, '.', ',')."</td>";
              $salida .= "<td>";
              if(1==$row2['categoria_id']){
                // si es categoria de enlace solamente
                if(0>=$row2['sitio_asignado']){
                  // si no tiene sitio asignado imprime el boton para asignar el sitio
                  $salida .= $btnSite;
                }
                else{
                    $salida .= "        <button type=\"button\" id=\"btn_siteDetails_".$row2['sitio_asignado']."\" title=\"Ver Detalles de Sitio\" class=\"btn btn-sm btn-primary btn-responsive\" onClick=\"goToSiteDetails('".encrypt($row2['sitio_asignado'])."','".encrypt($orgType)."');\"><span class=\"glyphicon glyphicon-map-marker\"></span></button>";
                    $salida .= "        <button type=\"button\" id=\"btn_siteWorkOrders_".$row2['relacion_id']."\" title=\"Crear Orden de Trabajo\" class=\"btn btn-sm btn-default btn-responsive\" onClick=\"getWorkOrders('".encrypt($row2['relacion_id'])."');\"><span class=\"glyphicon glyphicon-wrench\"></span></button></td>";
                  }
              }
              elseif(9==$row['venta_estatus_id']&&5==$row2['cotizacion_estatus_id']&&0<$row2['sitio_asignado']){
                $salida .= "        <button type=\"button\" id=\"btn_siteWorkOrders_".$row2['relacion_id']."\" title=\"Crear Orden de Trabajo\" class=\"btn btn-sm btn-default btn-responsive\" onClick=\"getWorkOrders('".encrypt($row2['relacion_id'])."');\"><span class=\"glyphicon glyphicon-wrench\"></span></button></td>";       
              }
              else
                $salida .= "<i>Ninguna</i></td>";
                // no muestra ninguna accion a realizar si el servicio no es de tipo enlace
                $salida .= "    </tr>";
            }
            if(1==$option){
              /*if(0<$row2['vigencia'])
                $vigencia = $row2['vigencia'];*/
              $productArr[]=array($row2['relacion_id'],$row2['servicio_id'],$row2['cantidad'],(string)$precio,$row2['producto_autorizado_por'],$row2['vigencia'],$row2['categoria_id']);
            }
          }
          $impuesto = $subtotal*0.16;
          $total = $subtotal+$impuesto; 
        }
        else{
          $salida .= "<td colspan=\"5\" align=\"center\"><b>No existen productos para mostrar</b></td>";
          $productArr = array();
          $impuesto = 0;
          $total = 0; 
        }
        if(0==$option){         
            $salida .= "    <tr>".
                       "      <td></td>".
                       "      <td></td>".
                       "      <td class=\"td-blue\"><b>Subtotal</b></td>".
                       "      <td colspan=\"2\"><i>$".number_format($subtotal, 2, '.', ',')."</i></td>".
                       "    </tr>".
                       "    <tr>".
                       "      <td></td>".
                       "      <td></td>".
                       "      <td class=\"td-blue\"><b>Impuesto</b></td>".
                       "      <td colspan=\"2\"><i>$".number_format($impuesto, 2, '.', ',')."</i></td>".
                       "    </tr>".
                       "    <tr>".
                       "      <td></td>".
                       "      <td></td>".
                       "      <td class=\"td-blue\"><b>Total a Pagar</b></td>".
                       "      <td class=\"td-blue\" colspan=\"2\"><b>$". number_format($total, 2, '.', ',')."</b></td>".
                       "    </tr>";
            $salida .= "<input type=\"hidden\" id=\"hdn_selConnection\" value=\"-1\"></input>"; //ver si es viable
            $salida .= "<input type=\"hidden\" id=\"hdn_priceIdDet\" value='".encrypt($priceId)."'></input>"; 
        }
        else{ 
            $salida = str_replace("\n","",$row['contacto_id']."||".json_encode($productArr)."||".$row['notas']."||".$row['vigencia_id']."||". $regionCot);
           
        }
      }
      //corrige el despliegue de precios en lista de coptizaciones debug
      // $queryCotStatUpd = "UPDATE cotizaciones SET".
      //                    " subtotal=".$subtotal.", impuesto= ".$impuesto.", total=".$total.", region_id=".$regionCot.
      //                    " WHERE cotizacion_id = ".$priceId;
      // if(!$resultCotStatUpd = mysqli_query($db_modulos,$queryCotStatUpd)){
      //   log_write("ERROR: SAVE-WORK-ORDER-NEW: No se actualizó el estatus de la cotizacion para autorizar ".$queryCotStatUpd,4);
      // }
      // else{
      //   log_write("OK: SAVE-WORK-ORDER-NEW: Se actualizó el estatus de la cotizacion para autorizar ".$queryCotStatUpd,4);
      // }
    }
    mysqli_close($db_modulos);
    mysqli_close($db_catalogos);
    echo $salida;
  }

  if("getProductTypeData" == $_POST["action"]){
    $db_catalogos   = condb_catalogos();
    
    $hoy = date('Y-m-d');
    $uniqId = uniqid();
    
    $relId = (isset($_POST['relId'])&& -1<$_POST['relId'])? $_POST['relId'] : $uniqId; 
    $prodId = isset($_POST['prodTipo']) ? $_POST['prodTipo'] : -1; 
    $cantidad = isset($_POST['prodCant']) ? $_POST['prodCant'] : -1;
    $prodVig = isset($_POST['prodVig']) ? $_POST['prodVig'] : -1;
    $prodEsPri = isset($_POST['prodEsPri']) ? $_POST['prodEsPri'] : -1;
  
    log_write("DEBUG: GET-PRODUCT-TYPE-DATA: relid ".$relId." prodid ".$prodId." cantidad ".$cantidad." prodvig".$prodVig." prodespri ".$prodEsPri,4);
  
    $queryVig = "SELECT * FROM cat_venta_vigencia WHERE vigencia_id = ".$prodVig; 
      
    $query = "SELECT cvs.*, cvts.* ".
             "FROM cat_venta_servicios AS cvs, ".
             "cat_venta_tipo_servicios AS cvts ".
             "WHERE cvs.servicio_id = ".$prodId.
             " AND cvs.tipo_servicio = cvts.tipo_servicio_id";
            
    $resultVig = mysqli_query($db_catalogos,$queryVig);
    $result = mysqli_query($db_catalogos,$query);
    
    log_write("DEBUG: GET-PRODUCT-TYPE-DATA: query ".$query,4);
    
    if(!$result||!$resultVig){
      $salida = "ERROR||Ocurrió un problema al consultar los datos del producto";
    }
    else{
      $row = mysqli_fetch_assoc($result);
      $rowVig = mysqli_fetch_assoc($resultVig);
      
      if(-1==$prodEsPri){
        if(1==$row['tipo_cobro']) //si es pago recurrente por mes
          $precio = $row['precio_'.$rowVig['cantidad_vigencia'].'m'];
        else
         $precio = $row['precio_lista'];
        $vigencia = $rowVig['cantidad_vigencia'];
        //$precio = $row['precio_lista']*$rowVig['factor']; 
      }
      else{
        $precio = $prodEsPri;
      }
      
      $sub = $precio*$cantidad;
        
      $salida = "OK||".$relId."||".$row['servicio_id']."||".$row['servicio']."||".$row['descripcion']."||".number_format($precio, 2, '.', '')."||".number_format($sub, 2, '.', '')."||".$row['categoria_id'];
    }
    mysqli_close($db_catalogos);
    echo $salida;   
  }
  
  //obtiene los datos de los productos para visualizarlos en el formulario de cotización al seleccionar alguno de la lista
  if("getProductData" == $_POST["action"]){
    $db_catalogos   = condb_catalogos();
    
    $hoy = date('Y-m-d');
    
    $option = isset($_POST['option']) ? $_POST['option'] : -1;
    $prodId = isset($_POST['prodId']) ? $_POST['prodId'] : -1;    
    $connId = isset($_POST['enlaceId']) ? $_POST['enlaceId'] : -1;
    $telId  = isset($_POST['telefoniaId']) ? $_POST['telefoniaId'] : -1;
    $enCant = isset($_POST['enlaceCant']) ? $_POST['enlaceCant'] : -1;
    $up     = isset($_POST['subida']) ? $_POST['subida'] : -1;
    $cant   = isset($_POST['cant']) ? $_POST['cant'] : -1;
    $min    = isset($_POST['minutos']) ? $_POST['minutos'] : -1;
    $edit   = isset($_POST['edit']) ? $_POST['edit'] : -1;
    $region  = isset($_POST['region']) ? $_POST['region'] : -1;
    $enlPromId = isset($_POST['enlacePromoId']) ? $_POST['enlacePromoId'] : -1;
    $promotext = "";
    
    if(0==$option){    
      $query = "SELECT enl.*, enlpr.* FROM cat_enlaces AS enl, ".
               "cat_enlaces_precios AS enlpr ";
               
      if(0==$edit){
        if(-1!=$connId&&0!=$connId){
          $promotext = "";
          $query .= " WHERE enl.enlace_id=".$connId." ".
                    " AND enlpr.enlace_id=enl.enlace_id ".
                    " AND enlpr.bajada=".$up." ".
                    " AND enlpr.ubicacion=".$region." ".
                    " AND enlpr.promo=0 ".
                    " AND enlpr.enlace_precio_vigente=1";
        }
        else{
          $promotext = "Promoción ";
          $query .= " WHERE enlpr.enlace_precio_id=".$enlPromId." AND enlpr.enlace_id=enl.enlace_id ";
        }
      }
      if(1==$edit){
        $query .= " WHERE enlpr.enlace_precio_id=".$connId." AND enlpr.enlace_id=enl.enlace_id ";
      }
      
      $result = mysqli_query($db_catalogos,$query);
      if(!$result){
        $salida = "ERROR||Ocurrió un problema al consultar los datos del producto ";
      }
      elseif(0>=mysqli_num_rows($result)){
        $salida = "ERROR||No existen enlaces con las velocidades de subida o bajada especificados";
      }
      else{
        while($row = mysqli_fetch_assoc($result)){
          
          $enlace   = $row['enlace'];
          $precio   = $row['precio'];
          $enlPreId = $row['enlace_precio_id'];
          $up = $row['bajada'];
        }
        $total = $enCant*($precio);
        
        $salida = "OK||".$promotext.$enlace." de ".$up." megas||".number_format($precio, 2, '.', '')."||".number_format($total, 2, '.', '')."||".$enlPreId;
        //$salida = "ERROR|| ".$lala." inicio ".$vigIni." fin ".$vigFin." hoy ".$hoy;
      }   
    }
    
    if(1==$option){      
      $prodDynPric = explode('||',getDynPrice($prodId,$cant,0));
      
      $salida = "OK||".$prodDynPric[0]."||".number_format($prodDynPric[1], 2, '.', '')."||".number_format($prodDynPric[2], 2, '.', '');
      //$salida = "ERROR||Ocurrió un problema al consultar los datos del producto ".$query;
    }
    
    if(2==$option){
      $query = "SELECT * FROM cat_telefonia WHERE telefonia_id=".$telId;    
      $result = mysqli_query($db_catalogos,$query);
      if(!$result){
        $salida = "ERROR||Ocurrió un problema al consultar los datos de la tarifa";
      }
      else{
        $row = mysqli_fetch_assoc($result);
        
        $total = $row['precio_unitario']*$min;
        
        $salida = "OK||".$row['paquete_tel']."||".number_format($row['precio_unitario'], 2, '.', '')."||".number_format($total, 2, '.', '');
      }
    }    
        
    mysqli_close($db_catalogos);
    echo $salida;
  }
  
  if("priceAuth" == $_POST["action"]){
      $db_modulos   = condb_modulos();
          
      $option    = isset($_POST['option']) ? $_POST['option'] : -1;
      $pricIdEnc = isset($_POST['pricId']) ? $_POST['pricId'] : -1;
      $prodIdEnc = isset($_POST['prodId']) ? $_POST['prodId'] : -1;
      $precActEnc = isset($_POST['precAct']) ? $_POST['precAct'] : -1;
      $estatus   = 1;
      
      /*$pricId = decrypt($pricIdEnc);
      $prodId = decrypt($prodIdEnc);
      $precAct = decrypt($precActEnc);*/
      
      $pricId = $pricIdEnc;
      $prodId = $prodIdEnc;
      $precAct = $precActEnc;
   
      log_write("DEBUG: PRICE-AUTH: relId ".$prodIdEnc." ".$prodId,4);
      log_write("DEBUG: PRICE-AUTH: cotId ".$pricIdEnc." ".$pricId,4);
      log_write("DEBUG: PRICE-AUTH: precAct ".$precActEnc." ".$precAct,4);
      
      log_write("ERROR: PRICE-AUTH: option ".$option,4);
      
      
     /* $queryCons =  "SELECT rcp.producto_precio_especial, rcp.producto_autorizado_por, cot.origen_id, cot.tipo_origen ".
                    "FROM ".$cfgTableNameMod.".rel_cotizacion_producto AS rcp, ".
                            $cfgTableNameMod.".cotizaciones AS cot, ".
                    "(SELECT cotizacion_id ".
                    "FROM ".$cfgTableNameMod.".rel_cotizacion_producto WHERE relacion_id= ".$prodId." ".
                    ") AS cotizacionId  ".   
                    "WHERE rcp.cotizacion_id=cotizacionId.cotizacion_id ".  
                    "AND rcp.cotizacion_id=cot.cotizacion_id ";*/
                    
                    
      $queryCons =  "SELECT rcp.relacion_id, rcp.producto_precio_especial, rcp.producto_autorizado_por, cot.origen_id, cot.tipo_origen, cvts.categoria_id ".
                    "FROM ".$cfgTableNameMod.".rel_cotizacion_producto AS rcp, ".
                            $cfgTableNameCat.".cat_venta_servicios AS cvs, ".
                            $cfgTableNameCat.".cat_venta_tipo_servicios AS cvts, ".
                            $cfgTableNameMod.".cotizaciones AS cot, ".
                    "(SELECT cotizacion_id ".
                    "FROM ".$cfgTableNameMod.".rel_cotizacion_producto WHERE relacion_id= ".$prodId." ".
                    ") AS cotizacionId  ".   
                    "WHERE rcp.cotizacion_id=cotizacionId.cotizacion_id ". 
                    "AND cvts.tipo_servicio_id=cvs.tipo_servicio ". 
                    "AND cvs.servicio_id=rcp.producto_id ".
                    "AND rcp.cotizacion_id=cot.cotizacion_id ";
  
      log_write("DEBUG: PRICE-AUTH: Consulta de orden ".$queryCons,4);
      
      $resultCons = mysqli_query($db_modulos,$queryCons);
        
      if(!$resultCons){
        log_write("ERROR: PRICE-AUTH: Ocurrió un problema al consultar los datos del producto",4);
        $salida = "ERROR||Ocurrió un problema al consultar los datos del producto";
      }
      else{
        $pricNotAuth = 0;
        if(0>=mysqli_num_rows($resultCons)){
          $salida = "ERROR||No se encontró el producto. Es probable que haya sido eliminado de la cotización";
          goto priAutha;
        }
        else{
          while($rowCons = mysqli_fetch_assoc($resultCons)){    
            if(-1!=$rowCons['producto_precio_especial']&&0>=$rowCons['producto_autorizado_por']&&5!=$rowCons['categoria_id'])
              $pricNotAuth++; 
            if($prodId==$rowCons['relacion_id'])
              $precioCons = $rowCons['producto_precio_especial'];
              
            $orgType = $rowCons['tipo_origen'];
            $orgId = $rowCons['origen_id'];
          }
        }
 
        log_write("DEBUG: PRICE-AUTH: orgtype ".$orgType,4);
        
        if(!hasPermission($orgType,'a')){
          log_write("ERROR: PRICE-AUTH: No cuenta con permisos para autorizar",4);
          $salida = "ERROR: No cuenta con permisos para autorizar";
        }
        else{
          if($precAct!=$precioCons){
            $salida = "ERROR: El precio ha sufrido cambios desde el envío de esta solicitud";
            goto priAutha;
          }
          else{
            if(0==$option){ //aceptada
              $estatus = 3;
              
              $queryProd = "UPDATE rel_cotizacion_producto SET producto_autorizado_por = ".$usuarioId." WHERE relacion_id = ".$prodId;
              $resultProd = mysqli_query($db_modulos,$queryProd);
              if(!$resultProd){
                log_write("ERROR: PRICE-AUTH: No pudo actualizarse el autorizador de producto ".$queryProd,4);
                $salida = "ERROR: No pudo llevarse a cabo la autorización";
                goto priAutha;
              }
              else{
                $pricNotAuth--;
              }
            }
            elseif(1==$option){  //rechazada
              $estatus = 4;     
            }
            else{
              $salida = "ERROR: Ocurrió un problema al determinar la acción a realizar";
              goto priAutha;
            }
            
            if(0>=$pricNotAuth||1==$option){
              $queryCot = "UPDATE cotizaciones SET cotizacion_estatus_id = ".$estatus." WHERE cotizacion_id = ".$pricId;
              $resultCot = mysqli_query($db_modulos,$queryCot);
              if(!$resultCot){
                log_write("ERROR: PRICE-AUTH: No pudo actualizarse el estatus de cotización ".$queryCot,4);
                $salida = "ERROR: No pudo actualizarse el estatus de la cotización ";
                goto priAutha;
              }
              else{
                if(0==$orgType)
                  $estatusPros = getProspectSellStatus($orgId);
                if(3==$estatusPros&&1==$cotStatus){
                  updateOriginStatus($orgType,6,$orgId);
                  // cambio de estado a negociacion
                  //orgId es el id del prospecto
                  if(0 == $orgType){
                    // estatus al que cambia, y id del prospecto
                    sendEstatusChangeEmail(6, $orgId);
                  }//fin del if 
                }
              }
            }
            $salida = "La operación se la realizado con éxito";
          }
        }
      }
      
      priAutha:
      mysqli_close($db_modulos);
      echo $salida;
  }
  
  function getProspectSellStatus($prosId){
    $db_modulos   = condb_modulos();
    $queryPros = "SELECT estatus FROM prospectos WHERE prospecto_id=".$prosId;
    $resultPros = mysqli_query($db_modulos,$queryPros);
    if(!$resultPros||(0>=mysqli_num_rows($resultPros))){
      log_write("ERROR: SAVE-PRICE: No se consiguieron los datos de estatus de prospecto ".$queryPros,4); 
      $result = false;   
    }
    else{
      $rowPros = mysqli_fetch_assoc($resultPros);
      $result = $rowPros['estatus'];  
    } 
    mysqli_close($db_modulos);
    return $result;
  }
  function getProspectNomRazon($prosId){
    $db_modulos   = condb_modulos();
    $queryPros = "SELECT razon_social, nombre_comercial FROM prospectos WHERE prospecto_id=".$prosId;
    $resultPros = mysqli_query($db_modulos,$queryPros);
    if(!$resultPros){
      $result = false;
    }
    else{
      $rowPros = mysqli_fetch_assoc($resultPros);
      if($rowPros['razon_social'] != ""){
        $result = " Razon Social: " . $rowPros['razon_social'];
        if($rowPros['nombre_comercial'] != ""){
          $result .= " y con Nombre Comercia: ". $rowPros['nombre_comercial'];
        }
      }else{
        $result = " Nombre Comercial: " . $rowPros['nombre_comercial'];
      }
    } 
    mysqli_close($db_modulos);
    return $result;
  }

  function getProspectAsignado($prosId){
    $db_modulos   = condb_modulos();
    $queryPros = "SELECT asignadoa FROM prospectos WHERE prospecto_id=".$prosId;
    $resultPros = mysqli_query($db_modulos,$queryPros);
    if(!$resultPros){
      $result = false;   
    }
    else{
      $rowPros = mysqli_fetch_assoc($resultPros);
      if($rowPros['asignadoa'] != ""){
        $result = $rowPros['asignadoa'];
      }else{
        $result = "";
      }
    } 
    mysqli_close($db_modulos);
    return $result;
  }
  
// *************************************************************
// guardar cotizacion de prospecto fin
// *************************************************************
  if("savePrice" == $_POST["action"]){
    $db_modulos   = condb_modulos();
    $db_catalogos = condb_catalogos();
    
    $option     = isset($_POST['option']) ? $_POST['option'] : -1;
    $contactId  = isset($_POST['contactId']) ? $_POST['contactId'] : -1;
    $pricId     = isset($_POST['pricId']) ? decrypt($_POST['pricId']) : -1;
    $authId     = isset($_POST['authId']) ? $_POST['authId'] : -1;
    $orgType    = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
    $orgId      = isset($_POST['orgId']) ? $_POST['orgId'] : -1;
    $notas      = isset($_POST['notas']) ? $_POST['notas'] : "";
    $vigencia   = isset($_POST['vigencia']) ? $_POST['vigencia'] : -1;
    $region     = isset($_POST['region']) ? $_POST['region'] : 0;
    $productArr = isset($_POST['productArr']) ? json_decode($_POST['productArr'],true) : "";

    $fecha  = date("Y-m-d H:i:s");

    $precioEnlace = 0;
    $subtotal = 0;
    $total = 0;
    $impuesto = 0;
    $subtotal = 0;
    $statList = "";
    $msg = "";
    $estatusPros = 0;

    $authUserId = -1; 
    $productArray = array(); //no confundir con productArr este se usa para cuando se editan los productos

    switch($orgType){
      case 0:  //si se trata de un prospecto
        $dbname = "prospectos";
        $fieldname = "prospecto_id";
        $typename = "prospecto";
        $nameFieldName = " ";
      break;
      case 1:  //si se trata de un cliente
        $dbname = "clientes";
        $fieldname = "cliente_id";
        $typename = "cliente";
        $nameFieldName = " ";
      break;
      case 2:  //si se trata de un sitio
        $dbname = "sitios";
        $fieldname = "sitio_id";
        $typename = "sitio";
        $nameFieldName = "cli.nombre AS nombre_comercial, con.nombre AS nombre, ";
      break;    
    }

    $cliQuery = "SELECT cli.nombre_comercial, cli.razon_social ".
                "FROM ".$cfgTableNameMod.".".$dbname." AS cli ".
                "WHERE cli.".$fieldname."=".$orgId; 

    $cliResult = mysqli_query($db_modulos,$cliQuery);

    if(!$cliResult){
      log_write("DEBUG: SAVE-PRICE: Error en conseguir los datos del ".$typename." ".$cliQuery,4);
      $salida = "ERROR|| Error en conseguir los datos del ".$typename;
      goto savePricea;
    }
    else{
      $cliRow = mysqli_fetch_assoc($cliResult);
    }

    log_write("DEBUG: SAVE-PRICE: pricId (o cotId) ".$pricId." enc ".$_POST['pricId']." re-enc ".encrypt($pricId),4);

    $nombre = get_userRealName($usuarioId);

    $asunto = "Autorización de precio especial cotización";

    $msg = "Hola.<br><br>".$nombre." ha dado de alta una cotización cuyos precios de productos están pendientes de autorizar para el prospecto o cliente ".$cliRow['nombre_comercial']." con la siguiente nota: ".$notas."<br>".
           "Para autorizar o rechazar el pecio haz click en las opciones que se presentan a continuación:<br><br> "; 

    $msgMail = "Hola.<br><br>".$nombre." ha dado de alta una cotización en el sistema de CRM cuyos precios de productos están pendientes de autorizar: <br>";

    log_write("DEBUG: SAVE-PRICE: productarr size ".sizeof($productArr),4);
    log_write("DEBUG: SAVE-PRICE: productarr ".print_r($productArr,true),4);
    
    if(0==$orgType){ //consulta de estatus de prospecto para cambio de fase / etapa   
      $estatusPros = getProspectSellStatus($orgId);
    }
    
    $vigArr = array();
    
    foreach($productArr as $key => $value){
      log_write("DEBUG: SAVE-PRICE: prodarr val ".$value[0],4);
      $productArray[$value[0]."a"] = $value; 
      $vigArr[] = $value[5];
    }
    $vigArr = array_diff(array_unique($vigArr),array(0));
    
    if(1<sizeof($vigArr)){
      $salida = "ERROR||Existe más de un tipo de vigencia en los productos seleccionados.";
      goto savePricea;
    }
 
    if(0==$option){  
      log_write("DEBUG: SAVE-PRICE: Se va a guardar una cotización NUEVA",4);
      
      $queryCot = "INSERT INTO cotizaciones ".
                  "(fecha_alta,tipo_origen,origen_id,region_id,contacto_id,cotizacion_estatus_id,usuario_alta,".
                  "subtotal,impuesto,total,notas,vigencia_id,vigencia_autorizado_por,fecha_actualizacion) ".
                  "VALUES ('".$fecha."',".$orgType.",".$orgId.",".$region.",".$contactId.",1,".$usuarioId.",".$subtotal.",".$impuesto.",".$total.",'".$notas."',".$vigencia.",".$authId.",'".$fecha."')";
               
      $result = mysqli_query($db_modulos,$queryCot); 
      
      log_write("DEBUG: SAVE-PRICE: ".$queryCot,4); 
      
      if(!$result){
        log_write("ERROR: SAVE-PRICE: No se insertó la cotización en la DB ".$queryCot,4);
        $salida = "ERROR||Ocurrió un error al insertarse los datos de cotización en la DB "; 
        goto savePricea;
      }      
      else{ 
        $pricId = mysqli_insert_id($db_modulos);
        log_write("DEBUG: SAVE-PRICE: Se insertó la cotización en la DB",4);      
        if(1==$estatusPros){
          // nueva cotizacion
          updateOriginStatus($orgType,2,$orgId);  
          // cambio de estado a cotizacion
          //orgId es el id del prospecto
          if(0 == $orgType){
            // estatus al que cambia, y id del prospecto
            sendEstatusChangeEmail(2, $orgId);
          }//fin del if 
        }
      }            
    }
    elseif(1==$option){
      
      log_write("DEBUG: SAVE-PRICE: Se va a guardar una cotización EXISTENTE",4);
    
      $queryCot = "UPDATE cotizaciones SET ".
                  "contacto_id=".$contactId.", region_id=".$region.", cotizacion_estatus_id=1,subtotal=".$subtotal.",impuesto=".$impuesto.",total=".$total.",notas='".$notas.
                  "',vigencia_id=".$vigencia.",vigencia_autorizado_por=".$authId." WHERE cotizacion_id=".$pricId;
     
      $resultCot = mysqli_query($db_modulos,$queryCot); 
      
      log_write("DEBUG: SAVE-PRICE: ".$queryCot,4); 
      
      if(!$resultCot){
         log_write("ERROR: SAVE-PRICE: No se actualizó la cotización ".$queryCot,4);
        $salida = "ERROR||Ocurrió un error al actualizarse los datos de cotización en la DB"; 
      }
      else{
        log_write("OK: SAVE-PRICE: Se actualizaron los datos generales de la cotización",4);
        writeOnJournal($usuarioId,"Ha actualizado los datos generales de la cotización con el id: ".$pricId);
                             
        $currProdQuery = "SELECT rcp.relacion_id, cvs.*, cvv.cantidad_vigencia, rcp.cantidad, ".
                         "rcp.producto_precio_especial, rcp.producto_autorizado_por, rcp.sitio_asignado, rcp.vigencia, cvts.categoria_id ".
                         "FROM ".$cfgTableNameCat.".cat_venta_servicios AS cvs, ".
                                 $cfgTableNameCat.".cat_venta_tipo_servicios AS cvts, ".
                                 $cfgTableNameMod.".rel_cotizacion_producto AS rcp, ".
                                 $cfgTableNameCat.".cat_venta_vigencia AS cvv ".
                         "WHERE cvs.servicio_id = rcp.producto_id ".
                         "AND cvs.tipo_servicio = cvts.tipo_servicio_id ".
                         "AND rcp.vigencia = cvv.vigencia_id ".
                         "AND rcp.cotizacion_id = ".$pricId;
                               
        $currProdArr = array();  
            
        $currProdResult = mysqli_query($db_catalogos,$currProdQuery);
        
        if(!$currProdResult){
            log_write("ERROR: SAVE-PRICE: No se pudo consultar los productos ".$currProdQuery,4);
            $salida = "ERROR|| Ocurrió un problema al consultar los datos de los productos para su actualizacion";
            goto savePricea;
        }
        else{          
          log_write("DEBUG: SAVE-PRICE: Va a actualizar los datos de los productos",4);
          $msg4 = "";
          $msg3 = "";
          $msg2 = "";
          $msg1 = "";
          $msgMail4 = "";
          $msgMail3 = "";
          $msgMail2 = "";
          $msgMail1 = "";
            
          while($currProdRow = mysqli_fetch_assoc($currProdResult)){
            $currProdArr[$currProdRow['relacion_id']."a"] = array($currProdRow['relacion_id'],$currProdRow['servicio_id'],$currProdRow['cantidad'],$currProdRow['producto_precio_especial'],$currProdRow['producto_autorizado_por'],$currProdRow['vigencia'],$currProdRow['categoria_id']);
          }
          
          arraySort($currProdArr,0);
          arraySort($productArray,0); 

          $currProdSize = sizeof($currProdArr);
          $prodSize = sizeof($productArray);
                                
          log_write("DEBUG: SAVE-PRICE: Productos actuales"."<pre>".print_r($currProdArr,true)."</pre>",4);
          log_write("DEBUG: SAVE-PRICE: Productos actualizados"."<pre>".print_r($productArray,true)."</pre>",4);
          log_write("DEBUG: SAVE-PRICE: Productos actualizados sin desmenuzar"."<pre>".print_r($productArr,true)."</pre>",4);
                                                    
          foreach($productArray as $key => $value){                     
            if(isset($currProdArr[$key])){
            
              if(-1!=$value[3]){     
                $precio = $value[3];
              }
              else{
                $precio = -1;
              }
              
              if($value[3]!=$currProdArr[$key][3]){
                $prodAuth = -1;
              }
              else{
                $prodAuth = $value[4];
              }  
              
              $newProdId = substr(json_encode($value[0]), 1, -1);
              
              log_write("DEBUG: SAVE-PRICE: newprodid ".$newProdId,4);
              
              $queryProdUpd = "UPDATE rel_cotizacion_producto SET cantidad = ".$value[2].", producto_precio_especial = ".
                              $precio.", producto_autorizado_por = ".$prodAuth." WHERE relacion_id = ".$newProdId;
                              
              log_write("DEBUG: SAVE-PRICE: updatequery".$queryProdUpd,4);
                                 
              if(!$resultProdUpd = mysqli_query($db_modulos,$queryProdUpd)){
                log_write("ERROR: SAVE-PRICE: error al actualizar pruducto con id [".$key."] ",4);
                goto savePricea;
              }   
              else{         
                $prodValRes = explode("||",valSpecialPrice($newProdId,$value,$pricId));
                if(4==$prodValRes[0]){
                  $msg4 .= $prodValRes[2];
                  $msgMail4 .= $prodValRes[3];
                }
                elseif(3==$prodValRes[0]){
                  $msg3 .= $prodValRes[2];
                  $msgMail3 .= $prodValRes[3];
                }
                elseif(2==$prodValRes[0]){
                  $msg2 .= $prodValRes[2];
                  $msgMail2 .= $prodValRes[3];
                }
                elseif(1==$prodValRes[0]){
                  $msg1 .= $prodValRes[2];
                  $msgMail1 .= $prodValRes[3];
                }
                
                $statList .= $prodValRes[0].",";
                $subtotal = $subtotal + $prodValRes[1];
                //$msg .= $prodValRes[2];
                
                log_write("DEBUG: SAVE-PRICE: se actualizó producto con id [".$key."]",4);
                unset($productArray[$key]);
                unset($currProdArr[$key]);
              }      
            }
            else{
              log_write("DEBUG: SAVE-PRICE: no se encuentra la llave [".$key."]",4);
            }           
          }
          if(isset($currProdArr)&&0<count($currProdArr)){
            foreach($currProdArr as $key => $value){
              $queryProdDel = "DELETE FROM rel_cotizacion_producto WHERE relacion_id = ".substr($key,0,-1);
                                     
              if(!$resultProdDel = mysqli_query($db_modulos,$queryProdDel)){
                  log_write("ERROR: SAVE-PRICE: error al eliminar pruducto con id [".$key."] ".$queryProdDel,4);
              }   
              else{
                  log_write("DEBUG: SAVE-PRICE: se eliminó pruducto con id [".$key."]",4);
                  unset($currProdArr[$key]);
              }      
            }
          }
        }       
      }             
    }      
    




    if(isset($productArray)&&0<count($productArray)){ 
      log_write("DEBUG: SAVE-PRICE: hay productos nuevos se van a insertar",4);  
      $msg4 = "";
      $msg3 = "";
      $msg2 = "";
      $msg1 = "";
      $msgMail4 = "";
      $msgMail3 = "";
      $msgMail2 = "";
      $msgMail1 = "";
      
      foreach($productArray as $key => $value){        
        if(-1!=$value[3]){     
          $precio = $value[3];
          $prodAuth = -1;
        }
        else{
          $precio = -1;
          $prodAuth = -1;
        }
              
        $prodQuery = "INSERT INTO rel_cotizacion_producto".   
                     "(cotizacion_id,producto_id,cantidad,vigencia,producto_precio_especial,producto_autorizado_por) VALUES ".
                     "(".$pricId.",".$value[1].",".$value[2].",".$value[5].",".$precio.",".$prodAuth.")";
                     
        $prodResult  = mysqli_query($db_modulos,$prodQuery); 
        log_write("DEBUG: SAVE-PRICE: ".$prodQuery,4);     
        if(!$prodResult){
          log_write("ERROR: SAVE-PRICE: No se insertó el producto ".$prodQuery,4);
          $salida = "ERROR|| Ocurrió un error al insertarse los datos de los productos";   
          goto savePricea;
        }
        else{
          $newProdId = mysqli_insert_id($db_modulos);  
          log_write("DEBUG: SAVE-PRICE: newprodid encodejson".json_encode($newProdId),4);
          $prodValRes = explode("||",valSpecialPrice($newProdId,$value,$pricId)); 
          if(4==$prodValRes[0]){
            // gera
            $msg4 .= $prodValRes[2];
            $msgMail4 .= $prodValRes[3];
          }
          elseif(3==$prodValRes[0]){
            // jorge
            $msg3 .= $prodValRes[2];
            $msgMail3 .= $prodValRes[3];
          }
          elseif(2==$prodValRes[0]){
            // juan pablo y demas
            $msg2 .= $prodValRes[2];
            $msgMail2 .= $prodValRes[3];
            $toUsr = 69;
          }
          elseif(1==$prodValRes[0]){
            // hector
            $msg1 .= $prodValRes[2];
            $msgMail1 .= $prodValRes[3];
            $toUsr = 7;
          }
          $statList .= $prodValRes[0].",";
          $subtotal = $subtotal + $prodValRes[1];
          //$msg .= $prodValRes[2];
        }
      }//insercion de productos   
    }
    else{
      log_write("ERROR: SAVE-PRICE: no hay productos en el arreglo",4);   
    }
    log_write("DEBUG: SAVE-PRICE: statList ".$statList,4); 
    $impuesto = $subtotal*0.16;
    $total = $impuesto+$subtotal;
    $msgend = "Sistema CRM Coeficiente";
    $msgMailEnd = "Para autorizar los precios, por favor ingresa al sistema CRM y dirígete a tu apartado de notificaciones. El CRM puede accesarse haciendo clic <a href=\"".$cfgServerLocation."crm/\">Aquí</a><br><br>".$msgend;
    $mailData = array('dest' => "",
                      'cc' => "",
                      'cco' => "",
                      'asunto' => $asunto,
                      'msg' => "",
                      'adjunto1' => "",
                      'adjunto2' => "",
                      'adjunto3' => "",
                      'adjunto4' => "",
                      'adjunto5' => "",
                      'adjunto6' => "",
                      'adjunto7' => "",
                      'adjunto8' => "",
                      'adjunto9' => "",
                      'adjunto10' => "",
                      'adjunto11' => "",
                      'adjunto12' => "",
                      'adjunto13' => "",
                      'adjunto14' => "",
                      'adjunto15' => "",
                      'estatus' => "NO ENVIADO",
                      'fecha_envio' => "0000-00-00 00:00:00",
                      'nIDCorreo' => 1,
                      'folio' => "0",
                      'firma' => "",
                      'observaciones' => "",
                      'bestado' => ""
                       );
    
    if(strpos($statList,"4")!==false){
      log_write("DEBUG: SAVE-PRICE: msgProd ".$msg4,4); 
      $mailData['msg'] = $msgMail.$msgMail4.$msgMailEnd;
      sendPrivMsgDepartment($usuarioId,1,0,$asunto,$msg.$msg4.$msgend,0,1);
      sendEmailMsgDepartment(1,0,$mailData);
      log_write("OK: SAVE-PRICE: Se enviara solicitud de autorizacion al administrador",4);
      $cotStatus = 2;
    }
    if(strpos($statList,"3")!==false){
      // solo manda notificacion a jorge salde
      log_write("DEBUG: SAVE-PRICE: msgProd ".$msg3,4); 
      $mailData['msg'] = $msgMail.$msgMail3.$msgMailEnd;
      sendPrivMsgDepartment($usuarioId,11,0,$asunto,$msg.$msg3.$msgend,0,1);
      sendEmailMsgDepartment(11,0,$mailData);
      log_write("OK: SAVE-PRICE: Se enviara solicitud de autorizacion a director general",4);
      $cotStatus = 2;
    }
    if(strpos($statList,"2")!==false){
      log_write("DEBUG: SAVE-PRICE: msgProd ".$msg2,4);
      $mailData['msg'] = $msgMail.$msgMail2.$msgMailEnd;
      sendPrivMsgDepartment($usuarioId,2,0,$asunto,$msg.$msg2.$msgend,0,$toUsr);
      sendEmailMsgDepartment(2,0,$mailData);
      log_write("OK: SAVE-PRICE: Se enviara solicitud de autorizacion a director comercial",4);
      $cotStatus = 2;
    }
    if(strpos($statList,"1")!==false){
      log_write("DEBUG: SAVE-PRICE: msgProd ".$msg1,4);
      $mailData['msg'] = $msgMail.$msgMail1.$msgMailEnd;
      sendPrivMsgDepartment($usuarioId,3,$usrUbica,$asunto,$msg.$msg1.$msgend,0,1);
      sendPrivMsgDepartment($usuarioId,2,0,$asunto,$msg.$msg1.$msgend,0,$toUsr);
      sendEmailMsgDepartment(3,$usrUbica,$mailData);
      sendEmailMsgDepartment(2,0,$mailData);
      log_write("OK: SAVE-PRICE: Se enviara solicitud de autorizacion a gerente de ventas y director comercial",4);
      $cotStatus = 2;
    }
    if((strpos($statList,"3")===false)&&(strpos($statList,"2")===false)&&(strpos($statList,"1")===false)){
      log_write("OK: SAVE-PRICE: No se envía solicitud",4);
      $cotStatus = 1;
    }
    $emailhash = encrypt(uniqId());
    $queryCotStatUpd = "UPDATE cotizaciones SET cotizacion_estatus_id = ".$cotStatus.",".
                       " subtotal=".$subtotal.", impuesto= ".$impuesto.", total=".$total.", fecha_actualizacion='".$fecha."',".
                       " email_hash = '".$emailhash."' ".
                       " WHERE cotizacion_id = ".$pricId;
    $resultCotStatUpd = mysqli_query($db_modulos,$queryCotStatUpd);
    if(!$resultCotStatUpd){
      log_write("ERROR: SAVE-PRICE: No se actualizó el estatus de la cotizacion para autorizar ".$queryCotStatUpd,4);
    }
    else{
      log_write("OK: SAVE-PRICE: Se actualizó el estatus de la cotizacion para autorizar ".$queryCotStatUpd,4);
    }
    if(0==$option){
      if(1==$estatusPros){
      //actualiza estatus de un prospecto a cotizacion por actualizacion de cotizacion
        updateOriginStatus($orgType,2,$orgId);
        //orgId es el id del prospecto
          if(0 == $orgType){
            // estatus al que cambia, y id del prospecto
            // esta funcion se comento por que se duplicaba el envio del correo, al parecer no se hace esto por actualizacion de cotizacion
            // sendEstatusChangeEmail(2, $orgId);
          }//fin del if 
        }
    }
    else if(1==$option){
      if(3==$estatusPros&&1==$cotStatus){
        updateOriginStatus($orgType,6,$orgId);
        // cambio de estado a negociacion
        //orgId es el id del prospecto
          if(0 == $orgType){
            // estatus al que cambia, y id del prospecto
            sendEstatusChangeEmail(6, $orgId);
          }//fin del if 
      }
    }
    $salida = "OK||Se han guardados los datos de cotización";
    savePricea:
    mysqli_close($db_modulos);
    mysqli_close($db_catalogos);
    $salida = str_replace("\n","",$salida);
    echo $salida;
  }
// *************************************************************
// guardar cotizacion de prospecto fin
// *************************************************************
  function valSpecialPrice($newProdId,$productArr,$newCotId){
    $db_modulos   = condb_modulos();
    $db_catalogos = condb_catalogos();
    $msg = "";
    $msgMail = "";
    $subtotal = 0; 
    $notFlag = 0;   
    
    $newProdIdEnc = $newProdId;
    $newCotIdEnc = $newCotId;
    $precAct = $productArr[3];
    
    log_write("DEBUG: VAL-SPECIAL-PRICE: relId ".$newProdId." ".$newProdIdEnc,4);
    log_write("DEBUG: VAL-SPECIAL-PRICE: cotId ".$newCotId." ".$newCotIdEnc,4);
    log_write("DEBUG: VAL-SPECIAL-PRICE: precAct ".$productArr[3]." ".$precAct,4);
    
    $queryProdCon = "SELECT * FROM cat_venta_servicios WHERE servicio_id = ".$productArr[1];
    $queryVig = "SELECT * FROM cat_venta_vigencia WHERE vigencia_id = ".$productArr[5]; 
    
    $queryProdConResult = mysqli_query($db_catalogos,$queryProdCon);
    $resultVig = mysqli_query($db_catalogos,$queryVig);
    
    if(!$queryProdConResult||!$resultVig){
      $salida = "ERROR|| Error al consultar los datos de producto";
      log_write("ERROR: VAL-SPECIAL-PRICE: Error al consultar productos".$queryProdCon,4);
    }
    else{
      $rowProductCon = mysqli_fetch_assoc($queryProdConResult);  
      $rowVig = mysqli_fetch_assoc($resultVig);
      
      if(1==$rowProductCon['tipo_cobro']){
        $precioLista = $rowProductCon['precio_'.$rowVig['cantidad_vigencia'].'m'];
        $precioBase = ($rowProductCon['precio_base']*$rowProductCon['precio_'.$rowVig['cantidad_vigencia'].'m'])/100;
        $precioMedio = ($rowProductCon['precio_medio']*$rowProductCon['precio_'.$rowVig['cantidad_vigencia'].'m'])/100;
      }
      else{
        $precioLista = $rowProductCon['precio_lista'];
        $precioBase = ($rowProductCon['precio_lista']*$rowProductCon['precio_base'])/100;
        $precioMedio = ($rowProductCon['precio_lista']*$rowProductCon['precio_medio'])/100;
      }
      
      if(-1==$productArr[3]){//checar
        log_write("DEBUG: VAL-SPECIAL-PRICE: Precio sin cambios ",4);
        $subtotal = $subtotal+($productArr[2]*$precioLista);
        $notFlag = 0;
        $addSuccessMsg = "";     
      }
      else{
        log_write("DEBUG: VAL-SPECIAL-PRICE: Precio con cambios ",4);
        $subtotal = $subtotal+($productArr[2]*$productArr[3]);
        
        if($productArr[3]>$precioLista){//se guarda con precio especial sin autorizacion
          log_write("DEBUG: VAL-SPECIAL-PRICE: Precio mayor a precio de lista ",4);
          $notFlag = 0;
          $addSuccessMsg = "";
        }
        else{
          log_write("DEBUG: VAL-SPECIAL-PRICE: Precio menor a precio de lista ",4);  
          $msgprev  = "<b>Producto o servicio:</b>".$rowProductCon['servicio']."<br> ".
                      "<b>Precio base:</b>".$precioBase."<br> ".
                      "<b>Precio de lista:</b>".$precioLista."<br> ".
                      "<b>Precio sugerido por vendedor:</b>".$productArr[3]."&nbsp; ";
          $msg .=   $msgprev."<input type=\"button\" class=\"btn btn-xs btn-info\" onClick=\"priceAuth(\'".$newCotIdEnc."\',\'".$newProdIdEnc."\',0,\'".$precAct."\')\" value=\"Autorizar\"></input>&nbsp".
                    "<input type=\"button\" class=\"btn btn-xs btn-danger\" onClick=\"priceAuth(\'".$newCotIdEnc."\',\'".$newProdIdEnc."\',1,\'".$precAct."\')\" value=\"Rechazar\"></input><br><br>";
          $msgMail .= $msgprev."<br><br>";
          
          if(5==$productArr[6]&&($productArr[3]<$precioLista)){
            log_write("DEBUG: VAL-SPECIAL-PRICE: Precio por debajo de lista. Autorizar jefe de infraestructura",4); //notifica a gera
            $notFlag = 4;
            $addSuccessMsg = ". Se ha enviado una solicitud de autorización al jefe de infraestructura";
          }
          if((($productArr[3] < $precioLista) && ($productArr[3] >= $precioLista * 0.9))&&(-1==$productArr[4])){
            log_write("DEBUG: VAL-SPECIAL-PRICE: Precio por debajo de lista. Autorizar jefe de ventas",4);
            $notFlag = 1;
            $addSuccessMsg = ". Se ha enviado una solicitud de autorización al gerente de ventas";
          }
          elseif((($productArr[3] < $precioLista * 0.899) && ($productArr[3] >= $precioLista * 0.699))&&(-1==$productArr[4])){//manda notif a dir comercial
            log_write("DEBUG: VAL-SPECIAL-PRICE: Precio por debajo de lista y encima del base. Autorizar dirección comercial",4);
            $notFlag = 2;
            $addSuccessMsg = ". Se ha enviado una solicitud de autorización al director comercial";
          }
          elseif(($productArr[3] < ($precioLista * 0.7))&&(-1==$productArr[4])){
            //manda notif a dir general o administrador
            log_write("DEBUG: VAL-SPECIAL-PRICE: Precio por debajo del base. Autorizar director",4);              
            $notFlag = 3;   
            $addSuccessMsg = ". Se ha enviado una solicitud de autorización al director general";
          }  


          // elseif(($productArr[3] < ($precioLista * 0.7))&&(-1==$productArr[4])){
          //   //manda notif a dir general o administrador
          //   log_write("DEBUG: VAL-SPECIAL-PRICE: Precio por debajo del base. Autorizar director",4);              
          //   $notFlag = 3;   
          //   $addSuccessMsg = ". Se ha enviado una solicitud de autorización al director general";
          // }
          // version anterior
          
          //  if((($productArr[3]<$precioLista)&&($productArr[3]>=$precioMedio))&&(-1==$productArr[4])){
          //    // a todos
          //   log_write("DEBUG: VAL-SPECIAL-PRICE: Precio por debajo de lista. Autorizar jefe de ventas",4);
          //   $notFlag = 1;
          //   $addSuccessMsg = ". Se ha enviado una solicitud de autorización al gerente de ventas";
          // }
          // elseif((($productArr[3]<$precioMedio)&&($productArr[3]>=$precioBase))&&(-1==$productArr[4])){//manda notif a dir comercial
          //   // pablo, Hector, aldrin
          //   log_write("DEBUG: VAL-SPECIAL-PRICE: Precio por debajo de lista y encima del base. Autorizar dirección comercial",4);
          //   $notFlag = 2;
          //   $addSuccessMsg = ". Se ha enviado una solicitud de autorización al director comercial";
          // }
          // elseif(($productArr[3]<$precioBase)&&(-1==$productArr[4])){//manda notif a dir general o administrador
          //   //jorge
          //   log_write("DEBUG: VAL-SPECIAL-PRICE: Precio por debajo del base. Autorizar director",4);              
          //   $notFlag = 3;   
          //   $addSuccessMsg = ". Se ha enviado una solicitud de autorización al director general";
          // }
        } 
      }
    } 
    
    mysqli_close($db_modulos);
    mysqli_close($db_catalogos);
    return $notFlag."||".$subtotal."||".$msg."||".$msgMail;   
  }
  function getDynPrice($prodId,$cant,$prodType){ 
    global $db_catalogos;
    global $cfgTableNameCat;
    
    $query = "";
    $prod = "";
    
    switch($prodType){
      case 0:
        $query = "SELECT prod.producto, prod.precio_unitario, 1 AS tipo_precio, 0 AS rango_min, ".$cant." AS rango_max FROM ".$cfgTableNameCat.".cat_productos AS prod WHERE prod.tipo_servicio_id=".$prodId.
                 " UNION ".
                 "SELECT 'producto' AS producto, dynPre.precio AS precio_unitario, 2 AS tipo_precio, dynPre.rango_min, dynPre.rango_max FROM ".$cfgTableNameCat.".cat_productos_precios_din AS dynPre WHERE dynPre.tipo_producto=1 AND dynPre.producto_id=".$prodId.
                 " ORDER BY tipo_precio,rango_min";  
      break;
    }
             
    log_write("DEBUG: GET-DYNAMIC-PRODUCT-PRICE: query ".$query,4); 
      
    $result = mysqli_query($db_catalogos,$query);
    if(!$result){  
      log_write("ERROR: GET-DYNAMIC-PRODUCT-PRICE: Ocurrió un problema al consultar los datos del producto: ".mysqli_error($db_catalogos),4);
      return false;
    }
    else{
      $totalFij = 0;
      $totalDyn = 0;
      $prevMaxRango = 0;
      while($row = mysqli_fetch_assoc($result)){
        log_write("DEBUG: GET-DYNAMIC-PRODUCT-PRICE: tipo_precio= ".$row['tipo_precio'],4);
        if(1==$row['tipo_precio']){
          $prod = $row['producto'];
          $totalFij = $row['precio_unitario']*$cant;
        }
        else{
          log_write("DEBUG: GET-DYNAMIC-PRODUCT-PRICE: cantidad= ".$cant,4);
           
          log_write("DEBUG: GET-DYNAMIC-PRODUCT-PRICE: rango max= ".$row['rango_max'],4);
          log_write("DEBUG: GET-DYNAMIC-PRODUCT-PRICE: rango min= ".$row['rango_min'],4);
          
          log_write("DEBUG: GET-DYNAMIC-PRODUCT-PRICE: precio unitario= ".$row['precio_unitario'],4);
          if($row['rango_max']<$cant){
            log_write("DEBUG: GET-DYNAMIC-PRODUCT-PRICE: sumatoria= ".$totalDyn."+(".$row['precio_unitario']."*(".$row['rango_max']."-".$row['rango_min']."))",4);
            $totalDyn = $totalDyn+($row['precio_unitario']*($row['rango_max']-$row['rango_min']));
            $prevMaxRango = $row['rango_max'];
          }
          elseif($cant>=$row['rango_min']){
            log_write("DEBUG: GET-DYNAMIC-PRODUCT-PRICE: sumatoria= ".$totalDyn."+((".$cant."-".$prevMaxRango.")*".$row['precio_unitario'].")",4);
            $totalDyn = $totalDyn+(($cant-$prevMaxRango)*$row['precio_unitario']);
          }
          else{
            //no suma
          }
        }
        if(1>=mysqli_num_rows($result))
          $total = $totalFij;
        else
          $total = $totalDyn;
        $preUn = $row['precio_unitario']; 
        log_write("DEBUG: GET-DYNAMIC-PRODUCT-PRICE: total acumulado ".$total,4);
      }        
      $salida = $prod."||".$preUn."||".$total;
    }
    return $salida;
  }
 //llamada de ajax. Elimina una cotización 
  if("deletePrice" == $_POST["action"]){
    $db_modulos   = condb_modulos();
    
    log_write("DEBUG: DELETE-PRICE: Se va a eliminar una cotización",4);

    $pricId  = isset($_POST['priceId']) ? $_POST['priceId'] : -1;
    $orgType = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
    $orgId   = isset($_POST['orgId']) ? $_POST['orgId'] : -1;
    
    //$querycon = "SELECT contrato FROM cotizaciones WHERE cotizacion_id=".$pricId;
    
    $queryCons = "SELECT COUNT(cotizacion_id) AS numCot FROM ".$cfgTableNameMod.".cotizaciones WHERE origen_id = ".$orgId." AND tipo_origen = ".$orgType;
    
    $query = "DELETE FROM cotizaciones WHERE cotizacion_id=".$pricId;
    
    $resultCons = mysqli_query($db_modulos,$queryCons);
    
    log_write("DEBUG: DELETE-PRICE: ".$query,4);
    if(!$resultCons){
      log_write("ERROR: DELETE-PRICE: No se pudo consultar los datos de la cotización ".$queryCons,4);
      $salida = "ERROR||Ocurrió un problema al consultar los datos de la cotización";
    }
    else{
      $result = mysqli_query($db_modulos,$query);
      if(!$result){
        log_write("ERROR: DELETE-PRICE: No se eliminó la cotización",4);
        $salida = "ERROR||Ocurrió un problema al intentar eliminarse la cotización";
      }
      else{
        log_write("OK: DELETE-PRICE: Se eliminó la cotización con id [".$pricId."]",4);
        writeOnJournal($usuarioId,"Ha eliminado la cotización con id: ".$pricId);
        $salida = "OK||La cotización se ha eliminado con éxito";
        
        $numCot = mysqli_fetch_assoc($resultCons);
        if(0>=($numCot['numCot']-1)&&0==$orgType){
          updateOriginStatus($orgType,1,$orgId);
          // cambio de estado a prospeccion en cazo de que se haya eliminado un una cotizacion y el prospecto no cuente con otras
          if(0 == $orgType){
            // estatus al que cambia, y id del prospecto
            sendEstatusChangeEmail(1, $orgId);
          }//fin del if
        }
      }
    }
    
    mysqli_close($db_modulos);
    echo $salida;   
  }
  if("getContractsPerClient" == $_POST["action"]){
    $orgType    = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
    $orgId      = isset($_POST['orgId']) ? $_POST['orgId'] : -1;
    
    $db_modulos   = condb_modulos();
    
    $query = "SELECT cot.*, con.contrato_id ".
             "FROM ".$cfgTableNameMod.".cotizaciones AS cot, ".
                      $cfgTableNameMod.".contratos AS con, ".
                      $cfgTableNameMod.".contrato_anexos AS conAx ".
             "WHERE cot.origen_id=".$orgId." ".
             "AND cot.tipo_origen=".$orgType." ".
             "AND con.contrato_id=conAx.contrato_id ".
             "AND conAx.cotizacion_id=cot.cotizacion_id ".
             "AND cot.contrato=1 ORDER BY cot.fecha_alta DESC LIMIT 1";
    
    $result = mysqli_query($db_modulos,$query);
    
    if(!$result){
      $salida = "ERROR: Ocurrió un problema al consultar los contratos existentes para este cliente o sitio";
    }
    else{
      if(0>=mysqli_num_rows($result)){
        $salida = "Este cliente o sitio no tiene contratos firmados ";
      }
      else{
        $salida = "<div class=\"table-responsive\"><table id=\"tbl_pricesAttachContract\" class=\"table table-striped\">\n".
                  "  <thead>\n".
                  "    <tr>\n".
                  "      <th class=\"hidden-xs\">Id</th>\n".
                  "      <th>Fecha</th>\n".                
                  "      <th>Cotizado por:</th>\n".
                  "      <th class=\"hidden-xs\">Subtotal</th>\n".
                  "      <th class=\"hidden-xs\">Impuesto</th>\n".
                  "      <th>Total</th>\n".
                  "      <th class=\"hidden-xs\">Notas</th>\n".
                  "      <th>Acciones</th>\n".
                  "    </tr>\n".
                  "  </thead>\n".
                  "  <tbody>\n"; 
        
        while($row = mysqli_fetch_assoc($result)){
          $nombreAlta = get_userRealName($row['usuario_alta']);
          $estatus = get_PriceStat($row['cotizacion_estatus_id']);
          
          $salida .=  "    <tr>\n".
                      "      <td class=\"hidden-xs\">".$row['cotizacion_id']."</td>\n".
                      "      <td>".$row['fecha_alta']."</td>\n".
                      "      <td><a href=/crm/modulos/perfil/index.php?id='".encrypt($row['usuario_alta'])."'>".$nombreAlta."</a></td>\n";              
           $salida .= "      <td class=\"hidden-xs\">$".number_format($row['subtotal'], 2, '.', ',')."</td>\n".
                      "      <td class=\"hidden-xs\">$".number_format($row['impuesto'], 2, '.', ',')."</td>\n".
                      "      <td>$".number_format($row['total'], 2, '.', ',')."</td>\n".
                      "      <td class=\"hidden-xs\" title=\"".$row["notas"]."\" onClick=\"alert('".$row["notas"]."')\">".
                                 "<a href=\"#\">".substr($row["notas"],0,30)."...</a></td>\n";
           $salida .= "      <td>";
                                   
          if(hasPermission($orgType,'w')||hasPermission($orgType,'m')){
            //if(1==$orgType||1==$orgSiteType){          
              $salida .= "    <button type=\"button\" title=\"Anexar a Contrato\" class=\"btn btn-xs btn-default btn-responsive\" onclick=\"attachToContract('".encrypt($row['contrato_id'])."');\">Anexar a Contrato</button>";
            //}
          }
          $salida .= "      </td>\n".
                     "    </tr>\n"; 
        }
        $salida .= "  </tbody>\n".
                   "</table>\n</div>";
      }
    }
    
    mysqli_close($db_modulos);
    echo $salida;
  }
  if("getContractRepList" == $_POST["action"]){
    $db_catalogos   = condb_catalogos(); 
    
    $query = "SELECT * FROM cat_coe_contrato_dato ORDER BY representante_id ASC";
    
    $result = mysqli_query($db_catalogos,$query);
    
    if(!$result){
      $salida = "Ocurrió un problema al obtenerse los datos de los respresentantes legales";
    }
    else{
      $salida = "<div class=\"table-responsive\"><table id=\"tbl_contractReps\" class=\"table table-striped\">\n".
                "  <thead>\n".
                "    <tr>\n".
                "      <th>Nombre</th>\n".
                "      <th>Razón Social</th>\n".
                "      <th>Acciones</th>\n".
                "    </tr>\n".
                "  </thead>\n".
                "  <tbody>\n";
      while($row = mysqli_fetch_assoc($result)){
        $salida .= "    <tr>".
                   "      <td>".$row['nombre']."</td>".
                   "      <td>".$row['nombre_razon_social']."</td>".
                   "      <td><input type=\"radio\" name=\"rad_contratoRep\" class=\"rad_responsive\" value=".$row['representante_id'].">Seleccionar</input></td>".
                   "    </tr>";
      }
      $salida .= "  </tbody>";
    }
    
    mysqli_close($db_catalogos);
    echo $salida;
  }
  if("getContractData" == $_POST["action"]){
    $cotId      = isset($_POST['cotId']) ? decrypt($_POST['cotId']) : -1;  
    $db_modulos   = condb_modulos();
    $contArr = Array();
    
    $query = "SELECT tipo_movimiento, fecha_activacion, representante_id, contrato_notas, contacto_id1, contacto_id2 FROM contratos WHERE cotizacion_id =".$cotId;
    
    $result = mysqli_query($db_modulos,$query);
    
    if(!$result){
      $salida = "ERROR||Ocurrió un problema al obtenerse los datos de la cotización";
    }
    else{
      $row = mysqli_fetch_assoc($result);
      
      (0!=$row['contacto_id1']) ? $contArr[] = encrypt($row['contacto_id1']) : "";
      (0!=$row['contacto_id2']) ? $contArr[] = encrypt($row['contacto_id2']) : "";
      
      //$contArr = array($contacto1,$contacto2);
      
      $salida = "OK||".$row['tipo_movimiento']."||".$row['contrato_notas']."||".json_encode($contArr)."||".$row['fecha_activacion']."||".$row['representante_id'];
    }
    mysqli_close($db_modulos);
    echo $salida;
  }
  if("attachToContract" == $_POST["action"]){
    $cotId      = isset($_POST['cotId']) ? decrypt($_POST['cotId']) : -1; 
    $conId      = isset($_POST['conId']) ? decrypt($_POST['conId']) : -1;  
    $db_modulos   = condb_modulos();
    $queryVig = "SELECT con.contrato_id ".
                "FROM ".$cfgTableNameMod.".cotizaciones AS cot, ".
                        $cfgTableNameMod.".contratos AS con ".
                "WHERE cot.cotizacion_id =".$cotId." ".
                "AND con.vigencia_id = cot.vigencia_id";
    $resultVig = mysqli_query($db_modulos,$queryVig);
    if(!$resultVig){
      if(0 >= mysqli_fecth_assoc($resultVig))
        $salida = "ERROR||El contrato cuenta con una vigencia diferente a la de esta cotización. No pueden anexarse";
    }
    else{
      $result = addContractAttach($conId,$cotId,$usuarioId,2); //el 2 significa que fue anexado a un contrato que ya existe
      if(!$result){
        $salida = "ERROR||Ocurrió un problema al anexarse los datos al contrato seleccionado";
      }
      else{
        $salida = "OK||Los datos de cotización han sido anexados";
      }
    }
    mysqli_close($db_modulos);
    echo $salida;
  }
  //obtiene la lista de todos los enlaces asociados a a cotización en cuestión
  if("getPriceConnections" == $_POST['action']){

    $cotId = isset($_POST['cotId']) ? decrypt($_POST['cotId']) : -1;
    $orgType  = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
    $db_modulos   = condb_modulos();
    $db_catalogos   = condb_catalogos();
    $db_usuarios   = condb_usuarios();
    
    $query = "SELECT * FROM enlaces WHERE cotizacion_id=".$cotId;
    
    $result = mysqli_query($db_modulos,$query);
    
    if(!$result){
      $salida = "ERROR||Ocurrió un problema al consultar los datos de enlaces para esta cotización en la DB";
    }
    else{
      if(0>=mysqli_num_rows($result)){
        $salida = "No existen enlaces cotizados";
      }
      else{
        $salida = "";
        if(hasPermission($orgType,'w')||(hasPermission($orgType,'m')&&$row['usuario_alta']==$usuarioId)){  
          $salida .= "<div class=\"col container-fluid\">".
                     "  <button type=\"button\" id=\"btn_siteConnectionNew\" title=\"Nuevo Enlace\" class=\"btn btn-default\" onClick=\"openNewSiteConnection();\"><span class=\"glyphicon glyphicon-asterisk\"></span> Nuevo Enlace</button><br>".
                     "</div><br>";
        }
        if(0==mysqli_num_rows($result)){
          $salida .= "No existen registros disponibles haga click en <strong>Nuevo Enlace</strong> para agregar uno nuevo";
        }
        else{
          $salida .= "<div class=\"table-responsive\"><table id=\"tbl_siteConnections\" class=\"table table-striped table-condensed\">\n".
                     "  <thead>\n".
                     "    <tr>\n".
                     "      <th>Id</th>\n".
                     "      <th>Nodo</th>\n".
                     "      <th>Fecha de Instalación</th>\n".
                     "      <th>Comentarios</th>\n".
                     "      <th>Acciones</th>\n".
                     "    </tr>\n".
                     "  </thead>\n".
                     "  <tbody>\n";                
          while($row = mysqli_fetch_assoc($result)){
            $salida .= "    <tr>".
                       "      <td>".$row['idenlace']."</td>".
                       "      <td>".$row['nodo']."</td>".
                       "      <td>".$row['fecha_instalacion']."</td>".
                       "      <td title=\"".$row["comentarios"]."\" onClick=\"alert('".$row["comentarios"]."')\">".
                       "        <a href=\"#\">".substr($row["comentarios"],0,30)."...</a></td>\n".
                       "      <td>";
            if(hasPermission($orgType,'r')||(hasPermission($orgType,'l')&&$row['usuario_alta']==$usuarioId)){           
              $salida .= "        <button type=\"button\" id=\"btn_siteConnectionDetails_".$row['enlace_id']."\" title=\"Ver Detalles\" class=\"btn btn-xs btn-primary btn-responsive\" onClick=\"goToSiteConnectionDetails('".encrypt($row['enlace_id'])."');\"><span class=\"glyphicon glyphicon-list\"></span></button>";
            }  
            if(hasPermission($orgType,'e')||(hasPermission($orgType,'n')&&$row['usuario_alta']==$usuarioId)){  
              $salida .= "        <button type=\"button\" id=\"btn_siteConnectionEdit_".$row['enlace_id']."\" title=\"Editar\" class=\"btn btn-xs btn-success btn-responsive\" onClick=\"siteConnectionEdit('".encrypt($row['enlace_id'])."');\"><span class=\"glyphicon glyphicon-edit\"></span></button>";
            }           
            if(hasPermission($orgType,'d')){      
              $salida .= "        <button type=\"button\" id=\"btn_siteConnectionDelete_".$row['enlace_id']."\" title=\"Eliminar Enlace\" class=\"btn btn-xs btn-danger btn-responsive\" onClick=\"deleteSiteConnection('".encrypt($row['enlace_id'])."');\"><span class=\"glyphicon glyphicon-remove\"></span></button>";
            } 
                       "      </td>";
            $salida .= "    </tr>";
          
          }
          $salida .= "  </tbody>\n".
                     "</table>\n</div>";
        }
      }
      
      
    }
    mysqli_close($db_catalogos);
    mysqli_close($db_modulos);
    mysqli_close($db_usuarios); 
    echo $salida;
  }
  
function addContractAttach($conId,$cotId,$usrId,$stat){
  global $db_modulos;
  
  $fechaNow = date("Y-m-d H:i:s");
  
  log_write("DEBUG: ADD-CONTRACT-ATTACH: Se va a agregar el anexo de servicio para contrato",4);
  
  $query = "INSERT INTO contrato_anexos(contrato_id,cotizacion_id,usuario_alta,fecha_alta) VALUES($conId,$cotId,$usrId,'$fechaNow')";

  $result = mysqli_query($db_modulos,$query);

  if(!$result){
    log_write("ERROR: ADD-CONTRACT-ATTACH: Ocurrió un problema al anexarse los datos al contrato seleccionado",4);
    
    if(1==$stat){
      $queryrollback = "DELETE FROM contratos WHERE contrato_id=".$conId;
      
      $resultrollback = mysqli_query($db_modulos,$queryrollback);
      
      if(!$resultrollback){
        log_write("ERROR: SAVE-CONTRACT: Ocurrió un problema al hacerse el rollback ".mysqli_error($db_modulos),4);
      }
    }
    $salida = false;
  }
  else{
    $salida = mysqli_insert_id($db_modulos);
    
    log_write("OK: ADD-CONTRACT-ATTACH: Los datos de cotización han sido anexados",4);
    
    $queryUpdCont = "UPDATE cotizaciones SET contrato=".$stat." WHERE cotizacion_id=".$cotId;
    
    $resultUpdCont = mysqli_query($db_modulos,$queryUpdCont);
    
    if(!$resultUpdCont){
      log_write("ERROR: SAVE-CONTRACT: Ocurrió un problema al marcarse la cotización como anexo ".mysqli_error($db_modulos),4);
    }
  }
  return $salida;
}

//invocada por la llamada de guardar cotización existente. Actualiza los datos de un producto
function updateProduct($db_modulos,$arr,$relId,$authUserId){ 
  global $salida;
  global $cfgLogService;
  
  log_write("DEBUG: UPDATE-PRODUCT: Se va a actualizar los datos de las productos",4);
  
  if(1==$arr[3]){
    $precio = $arr[2];
  }
  elseif(2==$arr[3]){
    $precio = $arr[2];    
    $authUserId = $arr[4];
  }
  else{
    $precio = 0;
    $authUserId = 0;
  }
  
  $query = "UPDATE rel_cotizacion_producto SET producto_id=".$arr[0].", cantidad=".$arr[1].", producto_precio_especial=".$precio.", producto_autorizado_por=".$authUserId." WHERE relacion_id=".$relId;
  
  $result = mysqli_query($db_modulos,$query);
  
  log_write("DEBUG: UPDATE-PRODUCT: ".$query,4);
  
  if(!$result){
    log_write("ERROR: UPDATE-PRODUCT: Error al actualizar el producto",4);
    $salida = "ERROR||Error al actualizar el producto"; 
    return false;   
  }
  else{
    log_write("OK: UPDATE-PRODUCT: Se actualizó el dato del producto",4);
    return true;    
  }
}
//actualiza la tarifa telefónica
function updatePhonePack($db_modulos,$arr,$relId,$authUserId){ 
  global $salida;
  global $cfgLogService;
   log_write("DEBUG: UPDATE-PHONE-PACK: Se va a actualizar los datos de las tarifas",4);
  if(0!=$arr[3]){
    $precio = $arr[2];
  }
  else{
    $precio = 0;
    $authUserId = 0;
  }
  $query = "UPDATE rel_cotizacion_telefonia SET telefonia_id=".$arr[0].", cantidad=".$arr[1].", telefonia_precio_especial=".$precio.", telefonia_autorizado_por=".$authUserId." WHERE relacion_id=".$relId;
  $result = mysqli_query($db_modulos,$query);
  log_write("DEBUG: UPDATE-PHONE-PACK: ".$query,4);
  if(!$result){
    log_write("ERROR: UPDATE-PHONE-PACK: Error al actualizar la tarifa",4);
    $salida = "ERROR||Error al actualizar la tarifa "; 
    return false;   
  }
  else{
    log_write("OK: UPDATE-PHONE-PACK: Se actualizó el dato de la tarifa",4);
    return true;    
  }
}
//invocada por la llamada de guardar cotización existente. Actualiza los datos de un enlace
function updateConnection($db_modulos,$arr,$relId,$authUserId){ 
  global $salida;
  global $cfgLogService;
  
  log_write("DEBUG: UPDATE-CONNECTION: Se va a actualizar los datos del enlace",4);
  
  if(0!=$arr[3]){
    $precio = $arr[2];
  }
  else{
    $precio = 0;
    $authUserId = 0;
  }
  $query = "UPDATE rel_cotizacion_enlace SET enlace_precio_id=".$arr[0].", subida=".$arr[5].", cantidad=".$arr[1].", enlace_precio_especial=".$precio.", enlace_autorizado_por=".$authUserId." WHERE relacion_id=".$relId;
  
  $result = mysqli_query($db_modulos,$query);
  
   log_write("DEBUG: UPDATE-CONNECTION: ".$query,4);
  
  if(!$result){
     log_write("ERROR: UPDATE-CONNECTION: Error al actualizar el enlace",4);
    $salida = "ERROR||Error al actualizar el enlace"; 
    return false;   
  }
  else{
     log_write("OK: UPDATE-CONNECTION: Se actualizó el dato del enlace",4);
    return true;    
  }
}

//ordena un arreglo por clave indicada en variable pos, en orden ascendente
function arraySort(&$arr,$pos) { 
  $mid = array();
  foreach ($arr as $key => $row) {
      $mid[$key]  = $row[$pos];
  }
  array_multisort($mid, SORT_ASC, $arr);
}
  
  /*****Fin de apartado*****/
  //llamada de ajax. Obtiene la lista de todos los contactos
  if("getContactList" == $_POST["action"]){
    $db_modulos   = condb_modulos();
    $option  = isset($_POST['option']) ? $_POST['option'] : -1;
    
    $orgId   = isset($_POST['orgId']) ? $_POST['orgId'] : -1;
    $orgType = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
    
    if(9==$option)
      $orgId = decrypt($orgId);
       
    /*$query = "SELECT * FROM contactos AS con, rel_origen_contacto AS rel WHERE rel.tipo_origen=".$orgType." ".
             "AND rel.origen_id=".$orgId." ".
             "AND rel.contacto_id=con.contacto_id";*/
             
    $query = "SELECT con.*, dep.departamento, pue.puesto AS puestoPref ".
             "FROM ".$cfgTableNameMod.".contactos AS con, ".
                     $cfgTableNameCat.".cat_departamentos AS dep, ".
                     $cfgTableNameCat.".cat_puestos AS pue, ".
                     $cfgTableNameMod.".rel_origen_contacto AS rel ".
             "WHERE rel.tipo_origen=".$orgType." ".
             "AND rel.origen_id=".$orgId." ".
             "AND rel.contacto_id=con.contacto_id ".
             "AND con.puesto_id = pue.puesto_id ".
             "AND con.departamento_id=dep.departamento_id";
    
    $result = mysqli_query($db_modulos,$query);
    
    if(!$result){
      $salida = "ERROR|| Ocurrió un error al consultar la lista de contactos ".$query;
    }
    else{
      $salida = "<div class=\"table-responsive\"><table id=\"tbl_contacts_".$option."\" class=\"table table-striped\">\n".
                "  <thead>\n".
                "    <tr>\n".
                "      <th class=\"hidden-xs\">Id</th>\n".
                "      <th>Nombre</th>\n".
                "      <th>Apellidos</th>\n".
                "      <th>Puesto</th>\n".
                "      <th>Teléfono</th>\n".
                "      <th>Teléfono Móvil</th>\n".
                "      <th>Correo Electŕonico</th>\n".
                "      <th>Acciones</th>\n".
                "    </tr>\n".
                "  </thead>\n".
                "  <tbody>\n";  
      $chkCount = 0;
      while($row = mysqli_fetch_assoc($result)){
        if(6==$row['puesto_id'])
          $puesto = $row['puesto'];
        else
          $puesto = $row['puestoPref'];
          
        $salida .= "    <tr id=\"tr_contactData_".$row['contacto_id']."\">\n".
                   "      <td id=\"td_contactId_".$row['contacto_id']."\" class=\"hidden-xs\">".$row['contacto_id']."</td>\n".
                   "      <td id=\"td_contactName_".$row['contacto_id']."\">".$row['nombre']."</td>\n".
                   "      <td id=\"td_contactLastName_".$row['contacto_id']."\">".$row['apellidos']."</td>\n".
                   "      <td id=\"td_contactPosition_".$row['contacto_id']."\">".$puesto."</td>\n".
                   "      <td id=\"td_contactPhone_".$row['contacto_id']."\">".$row['telefono']."</td>\n".
                   "      <td id=\"td_contactCell_".$row['contacto_id']."\">".$row['tel_movil']."</td>\n".
                   "      <td id=\"td_contactEmail_".$row['contacto_id']."\">".$row['email']."</td>\n";
        if(1==$option||2==$option)   //cliente y prospecto                          
          $salida .= "      <td id=\"td_contactActions_".$row['contacto_id']."\"> <input type=\"button\" class=\"btn btn-primary\" id=\"btn_agregaContacto\" value=\"Agregar\" onClick=\"addContact(".$row['contacto_id'].",4);\"></td>\n";
        if(3==$option)    //cotizacion email                        
          $salida .= "      <td id=\"td_contactActions_".$row['contacto_id']."\"> <input type=\"button\" class=\"btn btn-primary\" id=\"btn_agregaContacto\" value=\"Agregar\" onClick=\"addMailTo(".$row['contacto_id'].",10);\"></td>\n";                                      
        if(9==$option){  //requerimiento instalacion
          if(0==$chkCount){
            $salida .= "      <td id=\"td_contactActions_".$row['contacto_id']."\"><input type=\"radio\" name=\"rad_insReqConSel\" id=\"rad_".$row['contacto_id']."\" class=\" rad_responsive\" value=".encrypt($row['contacto_id'])." checked>Seleccionar</input><br></td>";
          }
          else{
            $salida .= "      <td id=\"td_contactActions_".$row['contacto_id']."\"><input type=\"radio\" name=\"rad_insReqConSel\" id=\"rad_".$row['contacto_id']."\" class=\" rad_responsive\" value=".encrypt($row['contacto_id']).">Seleccionar</input><br></td>";
          }
        } 
        
        if(10==$option) //cotizacion/contrato
          $salida .= "      <td id=\"td_contactActions_".$row['contacto_id']."\"><input type=\"checkbox\" name=\"chk_contactoContrato\" class=\"rad_responsive chk_contactoContrato\" value=".encrypt($row['contacto_id']).">Seleccionar</input><br></td>";
          
        if(11==$option)    //contrato email                        
          $salida .= "      <td id=\"td_contactActions_".$row['contacto_id']."\"> <input type=\"button\" class=\"btn btn-primary\" id=\"btn_agregaContacto\" value=\"Agregar\" onClick=\"addMailTo(".$row['contacto_id'].",11);\"></td>\n";
         
        $salida .= "    </tr>\n";
        $chkCount++;
      }   
      $salida .= "  <tbody>\n".
                 "</div>\n";
    }    
    mysqli_close($db_modulos);
    echo $salida;
  }
  
  //llamada de ajax obtiene los datos de algún contacto en específico
  if("getContactDetails" == $_POST["action"]){
    $db_modulos   = condb_modulos();
    
    $contId = isset($_POST["contId"]) ? $_POST["contId"] :-1;
  
    $query = "SELECT con.*, dep.departamento, pue.puesto AS puestoPref ".
             "FROM ".$cfgTableNameMod.".contactos AS con, ".
                     $cfgTableNameCat.".cat_departamentos AS dep, ".
                     $cfgTableNameCat.".cat_puestos AS pue ".
             "WHERE con.contacto_id=".$contId." AND con.puesto_id = pue.puesto_id AND con.departamento_id=dep.departamento_id";
    
    $result = mysqli_query($db_modulos,$query);
    
    if(!$result){
      $salida = "ERROR|| Ocurrió un problema al consultar los datos del contacto";
    }
    else{
      $row = mysqli_fetch_assoc($result);
      $salida = "OK||".$row['contacto_id']."||".
                       $row['nombre']."||".
                       $row['apellidos']."||".
                       $row['puesto']."||".
                       $row['telefono']."||".
                       $row['tel_movil']."||".
                       $row['email']."||".
                       $row['puesto_id']."||".
                       $row['departamento_id']."||".
                       $row['puestoPref']."||".
                       $row['departamento']."||".
                       $row['titulo_id'];
    }
    mysqli_close($db_modulos);
    echo $salida;
  }
  
  if("updateContact" == $_POST["action"]){
    $db_modulos   = condb_modulos();
    
    $contId = isset($_POST["contId"]) ? $_POST["contId"] :-1;
    $contArr = isset($_POST["contArr"]) ? json_decode($_POST["contArr"]) :"";
    
    if(!updateContact($db_modulos,$contArr,$contId))
      $salida = "ERROR||Ocurrió un error al actualizarse los datos de este contacto";
    else
      $salida = "OK||Los datos de contacto se actualizaron con éxito";
    
    mysqli_close($db_modulos);
    echo $salida;
  }
  
  //actualiza todos los contactos que un origen pueda tener, si le agregaron más contactos los inserta, si quitaron contactos los elimina, si el número permanece igual solo los actualiza here
  function updateContactsByArray($contArr,$query,&$salida,$orgId,$orgType){
    global $db_modulos;
    global $cfgLogService;
    global $usuarioId;
    
    log_write("DEBUG: UPDATE-CONTACT-BY-ARRAY: Se actualizarán contactos por arreglo",4);
    
    $contArray = array();

    $result = mysqli_query($db_modulos,$query);
    
    log_write("DEBUG: UPDATE-CONTACT-BY-ARRAY: ".$query,4);
    
    foreach($contArr as $key => $value){
      log_write("DEBUG: SAVE-PRICE: prodarr val ".$value[0],4);
      $contArray[$value[0]."a"] = $value; 
    } 
    
    if(!$result){
      log_write("ERROR: UPDATE-CONTACT-BY-ARRAY: No se pudo consultar los datos de los contactos para el origen tipo [".$orgType."] con id [".$orgId."]",4);
      $salida = "ERROR|| No se pudo consultar los datos de los contactos para este registro";
      return false;
    }
    else{
      $currContArr = array();   
             
      while($row = mysqli_fetch_assoc($result)){
        $currContArr[$row["contacto_id"]."a"] = array($row["contacto_id"],$row["nombre"],$row["apellidos"],$row["puesto"],$row["telefono"],$row["tel_movil"],$row["email"],$row["puesto_id"],$row["departamento_id"]);
      }
      
      arraySort($currContArr,0);
      arraySort($contArray,0); 
      
      log_write("DEBUG: UPDATE-CONTACT-BY-ARRAY: Contactos actuales"."<pre>".print_r($currContArr,true)."</pre>",4);
      log_write("DEBUG: UPDATE-CONTACT-BY-ARRAY: Contactos actualizados"."<pre>".print_r($contArray,true)."</pre>",4);     
      
      foreach($contArray as $key => $value){                     
        if(isset($currContArr[$key])){          
          if(!updateContact($db_modulos,$value,substr($key,0,-1))){
            log_write("ERROR: UPDATE-CONTACT-BY-ARRAY: Ocurrió un problema al actualizarse el contacto con id:".$key,4);
            $salida = "ERROR|| Ocurrió un problema al actualizarse el contacto con id:".$key;
            return false;
          }
          else{
            log_write("ERROR: UPDATE-CONTACT-BY-ARRAY: Ha modificado los datos del contacto con el id ".$key,4);
            writeOnJournal($usuarioId,"Ha modificado los datos del contacto con el id ".$key);
            unset($contArray[$key]);
            unset($currContArr[$key]);
          }      
        }
        else{
          log_write("DEBUG: SAVE-PRICE: no se encuentra la llave [".$key."]",4);
        }           
      }
      if(isset($currContArr)&&0<count($currContArr)){
        foreach($currContArr as $key => $value){
        
          if(0<deleteContact($db_modulos,substr($key,0,-1))){
            log_write("ERROR: UPDATE-CONTACT-BY-ARRAY: Error al eliminar los contactos sobrantes",4);
            $salida = "ERROR||No se eliminaron uno o más contactos. Verifica que no estén asociados a un contrato o cotización."; 
          }
          else{
            log_write("ERROR: UPDATE-CONTACT-BY-ARRAY: Ha removido contactos del origen ".$orgType." con id: ".$orgId,4);
            writeOnJournal($usuarioId,"Ha removido contacto del origen ".$orgType." con id: ".$orgId);    
            unset($currContArr[$key]);       
          }
        }
      }
      
      if(isset($contArray)&&0<count($contArray)){ 
        log_write("DEBUG: SAVE-PRICE: hay productos nuevos se van a insertar",4);     
                  
        foreach($contArray as $key => $value){
            
          if(!insertContact($db_modulos,$value,$orgType,$orgId)){
            log_write("ERROR: UPDATE-CONTACT-BY-ARRAY: No se insertó el contacto para este prospecto",4);
            $salida = "ERROR|| Ocurrió un problema al insertar los datos de contacto para este prospecto";
            return false;
          }
          else{
            $newConId = mysqli_insert_id($db_modulos);
            log_write("OK: UPDATE-CONTACT-BY-ARRAY: Ha agregado un contacto nuevo con el id [".$newConId."] al prospecto con id [".$orgId."]",4);
            writeOnJournal($usuarioId,"Ha agregado un contacto nuevo con el id [".$newConId."] al prospecto con id [".$orgId."]");
          }  
        }//insercion de productos   
      }
      else{
        log_write("ERROR: SAVE-PRICE: no hay productos en el arreglo",4);   
      }
      
      
     /* $contArrSize     = sizeof($contArr);
      $currContArrSize = sizeof($currContArr);
      
      if(0<$contArrSize){
        foreach($arrInt as $key=>$value){ //actualiza aquellos que ya existían
          if(!updateContact($db_modulos,$contArr[$key],$value[0])){
            log_write("ERROR: UPDATE-CONTACT-BY-ARRAY: Ocurrió un problema al actualizarse el contacto con id:".$currContArr[$key][0],4);
            $salida = "ERROR|| Ocurrió un problema al actualizarse el contacto con id:".$currContArr[$key][0];
            return false;
          }
          else{
            log_write("ERROR: UPDATE-CONTACT-BY-ARRAY: Ha modificado los datos del contacto con el id ".$currContArr[$key][0],4);
            writeOnJournal($usuarioId,"Ha modificado los datos del contacto con el id ".$currContArr[$key][0]);
          }
        }
      
        if($contArr>$currContArr){ //se agregaron contactos nuevos
          foreach($arrDif as $key=>$value){
            if(!insertContact($db_modulos,$arrDif[$key],$orgType,$orgId)){
              log_write("ERROR: UPDATE-CONTACT-BY-ARRAY: No se insertó el contacto para este prospecto",4);
              $salida = "ERROR|| Ocurrió un problema al insertar los datos de contacto para este prospecto";
              return false;
            }
            else{
              $newConId = mysqli_insert_id($db_modulos);
              log_write("OK: UPDATE-CONTACT-BY-ARRAY: Ha agregado un contacto nuevo con el id [".$newConId."] al prospecto con id [".$orgId."]",4);
              writeOnJournal($usuarioId,"Ha agregado un contacto nuevo con el id [".$newConId."] al prospecto con id [".$orgId."]");
            }
          }
        }
        elseif($contArr<$currContArr){  //se quitaron contactos
          foreach($arrDif as $key=>$value){
            if(!deleteContact($db_modulos,$arrDif[$key][0])){
              log_write("ERROR: UPDATE-CONTACT-BY-ARRAY: Error al eliminar los contactos sobrantes",4);
              $salida = "ERROR||No se eliminaron uno o más contactos"; 
            }
            else{
              log_write("ERROR: UPDATE-CONTACT-BY-ARRAY: Ha removido contactos del origen ".$orgType." con id: ".$orgId,4);
              writeOnJournal($usuarioId,"Ha removido contacto del origen ".$orgType." con id: ".$orgId);           
            }
          }
        }
        elseif($contArr==$currContArr){ //Son del mismo tamaño
          log_write("DEBUG: UPDATE-CONTACT-BY-ARRAY: Los dos arreglos son del mismo tamaño",4);
        }
        else{ //wtf no debería pasar esto
          log_write("ERROR: UPDATE-CONTACT-BY-ARRAY: No se pudo determinar la acción a realizar en los contactos, los arreglos están defectuosos",4);
          return false;
        }        
      }
      else{
        log_write("ERROR: UPDATE-CONTACT-BY-ARRAY: No hay contactos para actualizar",4);
        $salida = "ERROR||No se recibieron los datos de contacto para actualizar"; 
        return false;
      }*/
    }
    return true;
  }
  
  //inserta el contacto nuevo en la base de datos junto a su entidad relación, debe especificarse el origen orgtype y el id de dicho origen orgid
  function insertContact($db_modulos,$arr,$orgType,$orgId){
    global $salida;
    global $cfgLogService;
    global $usuarioId;
    
    log_write("DEBUG: INSERT CONTACT: Se va a insertar un contacto",4);
    
    switch($orgType){
      case 0:
        $orgName = "prospecto";
      break;
      case 1:
        $orgName = "cliente";
      break;
      case 2:
        $orgName = "sitio";
      break;
    }
    
    $hoy = date('Y-m-d H:i:s');
    $query = "INSERT INTO contactos(titulo_id,nombre,apellidos,puesto,telefono,tel_movil,email,puesto_id,departamento_id,fecha_alta) VALUES(".$arr[9].",'".$arr[1]."','".$arr[2]."','".$arr[3]."','".$arr[4]."','".$arr[5]."','".$arr[6]."',".$arr[7].",".$arr[8].",'".$hoy."')";
    
    $result = mysqli_query($db_modulos,$query);
    
    log_write("DEBUG: INSERT CONTACT: ".$query,4);
     
    if(!$result){
      log_write("ERROR: INSERT CONTACT: No se insertó los datos de contacto",4);
      $salida = "ERROR|| Ocurrió un problema al insertar los datos de contacto para este registro";
      return false;
    }
    else{
      $newConId = mysqli_insert_id($db_modulos);
      
      log_write("OK: INSERT CONTACT: Se insertó el contacto nuevo con el id ".$newConId,4);
      writeOnJournal($usuarioId,"Ha agregado un contacto nuevo con el id [".$newConId."] al ".$orgName." con id [".$orgId."]");
      
      $query2 = "INSERT INTO rel_origen_contacto(tipo_origen,origen_id,contacto_id) VALUES(".$orgType.",".$orgId.",".$newConId.")";
      
      $result2 = mysqli_query($db_modulos,$query2);
      
      log_write("DEBUG: INSERT CONTACT: ".$query2,4);
      
      if(!$result2){
        log_write("ERROR: INSERT CONTACT: No se pudo hacer la relación entre el contacto y el registro. Se hará rollback",4);
        $salida = "ERROR|| No se pudo hacer la relación entre los contactos con el registro";
        
        $query3 = "DELETE FROM contactos WHERE contacto_id=".$newConId;
        
        log_write("DEBUG: INSERT CONTACT: ".$query3,4);
        
        $result3 = mysqli_query($db_modulos,$query3);
        if(!$result3){
          log_write("ERROR: INSERT CONTACT: No se pudo hacer rollback, el contacto con id [".$newConId."] permanece en la base de datos",4);
        }
        else{
          log_write("DEBUG: INSERT CONTACT: Se eliminó el contacto con id [".$newConId."] sin relación con origen",4);
        }
        return false;
      }
      else{
        log_write("OK: INSERT CONTACT: Se hizo la relación entre el contacto y el registro",4);
        return true;
      }
    }
  }
  
  //actualiza los datos de un contacto
  function updateContact($db_modulos,$arr,$id){
    global $salida;
    global $cfgLogService;
    global $usuarioId;
    
    log_write("DEBUG: UPDATE CONTACT: Se va a actualizar un contacto",4);
    
    $query = "UPDATE contactos SET nombre='".$arr[1]."', apellidos='".$arr[2]."', puesto='".$arr[3]."', telefono='".$arr[4]."', tel_movil='".$arr[5]."', email='".$arr[6]."', puesto_id=".$arr[7].", departamento_id=".$arr[8].", titulo_id=".$arr[9]." WHERE contacto_id=".$id;
    
    $result = mysqli_query($db_modulos,$query);
    
    log_write("DEBUG: UPDATE CONTACT: ".$query,4);
    
     if(!$result){
      log_write("ERROR: UPDATE CONTACT: No se actualizó los datos del contacto con id [".$id."]",4);
      $salida = "ERROR|| Ocurrió un problema al actualizarse los datos de contacto para este registro";
      return false;
    }
    else{  
      log_write("OK: UPDATE CONTACT: Se actualizó el contacto con id [".$id."]",4);    
      writeOnJournal($usuarioId,"Ha modificado los datos del contacto con el id ".$id);
      return true;      
    }    
  }
  
  //elimina los datos de un contacto, solo si no está ya asociado a un contrato o cotizacación
  function deleteContact($db_modulos,$id){
    log_write("DEBUG: UPDATE CONTACT: Se va a eliminar un contacto",4);
    
    $queryCon = "SELECT cotizacion_id AS id FROM cotizaciones WHERE contacto_id = ".$id.
                " UNION SELECT contrato_id AS id FROM contratos WHERE (contacto_id1 = ".$id." OR contacto_id2 = ".$id.")";
    
    log_write("DEBUG: DELETE CONTACT: QueryCon: ".$queryCon,4);
    $resultCot = mysqli_query($db_modulos,$queryCon);
    
    if(!$resultCot){
      log_write("ERROR: DELETE CONTACT: Ocurrió un error al consultar los contactos asociados",4);
      return 1;
    }
    else{
      if(0<mysqli_num_rows($resultCot)){
        log_write("DEBUG: DELETE CONTACT: Está asociado a un contrato o cotización. No se eliminará",4);
        return 2;
      }
      else{
        log_write("DEBUG: DELETE CONTACT: No está asociado a un contrato o cotización. Se eliminará",4);
        $queryDel = "DELETE FROM contactos WHERE contacto_id=".$id;
        
        log_write("DEBUG: DELETE CONTACT: QueryDel: ".$queryDel,4);
        $resultDel = mysqli_query($db_modulos,$queryDel);
        
        if(!$resultDel){
          log_write("ERROR: DELETE CONTACT: Ocurrió un problema al borrar el contacto",4);
          return 1;
        }
        else{
          log_write("OK: DELETE CONTACT: El contacto ha sido eliminado",4);
          return 0;
        }       
      }
    }    
    return true;
  }  
  /***********Fin de apartado**********/
/*********Funciones de escritura y gestión de documentos *********/
//llamada de ajax. carga un documento y lo asocia a un origen (prospecto/cliente/sitio etc).  
function uploadfile($filesObj,$docPath,$filename){
  global $salida;
  global $maxDocSize;
  
  $finfo = finfo_open(FILEINFO_MIME_TYPE);
  $mime = finfo_file($finfo,$filesObj["tmp_name"]);

  log_write("DEBUG: UPLOAD-NEW-DOCUMENT: Si existe un archivo ".$filesObj["tmp_name"],4);
  log_write("DEBUG: UPLOAD-NEW-DOCUMENT: Tamaño de archivo ".$filesObj["size"],4);
  log_write("DEBUG: UPLOAD-NEW-DOCUMENT: Tipo de archivo ".$mime,4);
  
  $type = array('image/png',
                'image/jpeg',
                'image/gif',
                'image/bmp',
                'image/tiff',
                'application/pdf',
                'application/x-pdf',
                'application/vnd.ms-excel',
                'application/vnd.ms-office',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'text/csv',
                'text/comma-separated-values',
                'application/csv',
                'application/x-rar-compressed',
                'application/zip',
                'application/x-tar'
                );
       
  if(in_array($mime,$type)) {
    if ($filesObj["error"] > 0) {
      $salida = "ERROR|| Error al leer el archivo " . $filesObj["error"];
      log_write("DEBUG: UPLOAD-NEW-DOCUMENT: Error al leer el archivo".$filesObj["error"],4);
      $result = false;
    }
    else if(0 == $filesObj["size"]) {
      $salida = "ERROR|| El archivo está vacío.";
      log_write("DEBUG: UPLOAD-NEW-DOCUMENT: El archivo está vacío",4);
      $result = false;
    }
    else if($filesObj["size"] > $maxDocSize) {
      $salida = "ERROR|| El tamaño del archivo excede el máximo permitido: ".$maxDocSize;
      log_write("DEBUG: UPLOAD-NEW-DOCUMENT: El tamaño del archivo excede el máximo permitido".$maxDocSize,4);
      $result = false;
    } 
    else {    
      if(file_exists($docPath.$filename)) {
        //chmod($docPath.$filename,0755);
        unlink($docPath.$filename);
      }
      if(!file_exists($docPath)) {
        mkdir($docPath,0775);
      }                   
      $result = move_uploaded_file($filesObj["tmp_name"],$docPath.$filename);       
    }
    finfo_close($finfo);       
  }
  else {
    $salida = "ERROR|| Tipo de archivo no soportado. Recuerde cargar un archivo de formato de imagen. Tipo de archivo enviado ".$mime;
    log_write("ERROR: UPLOAD-NEW-DOCUMENT: Tipo de archivo no soportado".$mime,4);
    $result = false;
  } 
  return $result;
}

if("uploadNewDocument" == $_POST["action"]){
  $db_modulos   = condb_modulos();
  
  log_write("DEBUG: UPLOAD-NEW-DOCUMENT: Se va a guardar un documento",4);
  
  $orgId   = isset($_POST['orgId']) ? $_POST['orgId'] : -1;
  $orgType = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
  $docId   = isset($_POST['docId']) ? $_POST['docId'] : -1;
  $docType = isset($_POST['docType']) ? $_POST['docType'] : -1;
  $docDesc = isset($_POST['docDesc']) ? $_POST['docDesc'] : "";
  $option  = isset($_POST['option']) ? $_POST['option'] : -1;
  $fecha  = date("Y-m-d H:i:s"); 
  $fecha2 = date("YmdHis");
  
  switch($orgType){
    case 0:
      $orgName = "prospecto";
      $folderName = "prospectdocs";
    break;
    case 1:
      $orgName = "cliente";
      $folderName = "clientdocs";
    break;
    case 2:
      $orgName = "sitio";
      $folderName = "sitedocs";
    break;
  }
  
  $docPathPrev = "../../docs/scan/".$folderName."/".$orgId."/";  //ruta donde se va a guardar el archivo
  
  //$docPath = "../../".$docPathPrev;
  
  if(isset($_FILES["fl_docNew"])) {
        
        $filename = $fecha2."_".$orgId."_".$docType.".".pathinfo($_FILES["fl_docNew"]["name"], PATHINFO_EXTENSION);
        
        $result = uploadfile($_FILES["fl_docNew"],$docPathPrev,$filename);
        
        if(!$result){
          $salida = "ERROR|| No pudo cargarse el archivo";
          log_write("ERROR: UPLOAD-NEW-DOCUMENT: No se pudo mover el archivo ".$_FILES["fl_docNew"]["name"]." a su carpeta ".$docPath.$filename,4);
          goto newDoca;
        } 
        else{        
          if(0==$option){ //se va a insertar u documento nuevo
            log_write("DEBUG: UPLOAD-NEW-DOCUMENT: Se va a insertar un documento nuevo. Archivo nuevo",4);
            
            $query = "INSERT INTO documentos (origen_id,tipo_origen,tipo_documento_id,descripcion,documento_ruta,fecha_carga,usuario_alta) VALUES (".$orgId.",".$orgType.",".$docType.",'".$docDesc."','".$docPathPrev.$filename."','".$fecha."',".$usuarioId.")";
           
            $result = mysqli_query($db_modulos,$query);
           
            log_write("DEBUG: UPLOAD-NEW-DOCUMENT: ".$query,4);
            
            if(!$result){
              log_write("ERROR: UPLOAD-NEW-DOCUMENT: No se pudo escribir los datos del documento en la DB",4);
              $salida = "ERROR||No se pudo escribir los datos del documento en la DB"; 
              unlink($docPathPrev.$filename);
              goto newDoca;         
            }
            else{
              $newId = mysqli_insert_id($db_modulos);
              $salida = "OK|| Archivo cargado con éxito: ".$_FILES["fl_docNew"]["name"];
              writeOnJournal($usuarioId,"Ha insertado el documento nuevo id [".$newId."] para el ".$orgName." con id ".$orgId);
              log_write("OK: UPLOAD-NEW-DOCUMENT: El archivo se cargó y se guardaron sus datos",4);
            }       
          }
                    
          if(1==$option){ //se van a actualizar los datos, incluyendo la ruta del archivo nuevo
            log_write("DEBUG: UPLOAD-NEW-DOCUMENT: Se va a actualizar los datos del archivo. Archivo nuevo",4);
            
            $query = "UPDATE documentos SET descripcion='".$docDesc."', documento_ruta='".$docPathPrev.$filename."', tipo_documento_id=".$docType." WHERE documento_id=".$docId;
            
            $result = mysqli_query($db_modulos,$query);
           
            log_write("DEBUG: UPLOAD-NEW-DOCUMENT: ".$query,4);
            
            if(!$result){
              log_write("ERROR: UPLOAD-NEW-DOCUMENT: No se pudo actualizar los datos del documento en la DB",4);
              $salida = "ERROR||No se pudo actualizar los datos del documento en la DB"; 
              unlink($docPathPrev.$filename);
              goto newDoca;         
            }
            else{
              $salida = "OK|| Archivo actualizado con éxito: ".$_FILES["fl_docNew"]["name"];
              writeOnJournal($usuarioId,"Ha actualizado el archivo y los datos del documento id [".$docId."] para el ".$orgName." con id ".$orgId);
              log_write("OK: UPLOAD-NEW-DOCUMENT: El archivo se actualizó y se guardaron sus datos",4);
            }
          }           
        }                   
      //}
      //finfo_close($finfo);       
    //} 
    /*else {
      $salida = "ERROR|| Tipo de archivo no soportado. Recuerde cargar un archivo de formato de imagen. Tipo de archivo enviado ".$mime;
      log_write("ERROR: UPLOAD-NEW-DOCUMENT: Tipo de archivo no soportado".$mime,4);
      goto newDoca;
    } */
  }
  else{
    if(1==$option){ //se van a actualizar los datos en la db sin subor archivo
      log_write("DEBUG: UPLOAD-NEW-DOCUMENT: Se va a actualizar los datos del archivo. No archivo nuevo",4);
    
      $query = "UPDATE documentos SET descripcion='".$docDesc."', tipo_documento_id=".$docType." WHERE documento_id=".$docId;
      
      $result = mysqli_query($db_modulos,$query);
     
      log_write("DEBUG: UPLOAD-NEW-DOCUMENT: ".$query,4);
      
      if(!$result){
        log_write("ERROR: UPLOAD-NEW-DOCUMENT: No se pudo actualizar los datos del documento en la DB",4);
        $salida = "ERROR||No se pudo actualizar los datos del documento en la DB"; 
        unlink($docPathPrev.$filename);
        goto newDoca;         
      }
      else{
        $salida = "OK|| Datos del archivo actualizados con éxito: ".$_FILES["fl_docNew"]["name"];
        writeOnJournal($usuarioId,"Ha actualizado los datos del documento id [".$docId."] para el ".$orgName." con id ".$orgId);
        log_write("OK: UPLOAD-NEW-DOCUMENT: Se actualizaron los datos del archivo",4);
      }
    }
    else{
      $salida = "ERROR|| El archivo no pudo subirse.";
      log_write("ERROR: UPLOAD-NEW-DOCUMENT: El campo de archivo está vacío".$_FILES["fl_docNew"]["name"],4);
    }
  }  
  newDoca:
  mysqli_close($db_modulos);
  echo $salida;
}

//llamada de ajax. Manda a recabar los documentos asociados a un origen, y los despliega en una pequeña tabla con una vista prvia del documento
if("getUploadedDocs" == $_POST["action"]){
  $db_modulos   = condb_modulos();
  $db_catalogos   = condb_catalogos();

  $orgId = isset($_POST['orgId']) ? $_POST['orgId'] : -1;
  $orgType = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
  
  $queryCons = "SELECT * FROM cat_tipo_documento WHERE tipo_documento_id != 0 ORDER BY tipo_documento_id";
  
  $query = "SELECT * FROM documentos WHERE origen_id=".$orgId." AND tipo_origen=".$orgType." ORDER BY tipo_documento_id";
  
  $result = mysqli_query($db_modulos,$query);
  $resultCons = mysqli_query($db_catalogos,$queryCons);
  
  if(!$result||!$resultCons){
    $salida = "ERROR||Ocurrió un problema al obtener los datos de los documentos";
  }
  else{
    if(0==mysqli_num_rows($result)){
      $salida = "OK||Este registro no tiene documentos asociados. Haga click en <b>Agregar Documento</b> para añadir uno";
    }
    else{
      $salida = "OK||<div class=\"table-responsive\"><table id=\"tbl_uploadedDocuments\" class=\"table table-hover table-striped\">\n".
                "  <thead>\n".
                "    <tr>\n".
                "      <th>Tipo Documento</th>\n".
                "      <th>Agregado en</th>\n".
                "      <th>Descripcion</th>\n".
                "      <th>Agregado por</th>\n".
                "      <th>Preview</th>\n".
                "      <th>Acciones</th>\n".
                "    </tr>\n".
                "  </thead>\n".
                "  <tbody>\n";
                
      $docTypeArr = array();
      while($rowCons = mysqli_fetch_assoc($resultCons)){
        $docTypeArr[] = array($rowCons["tipo_documento_id"],$rowCons["tipo_documento"]);
      }
      while($row = mysqli_fetch_assoc($result)){
        
        foreach($docTypeArr as $key => $value){
          if($value[0]==$row['tipo_documento_id']){
            unset($docTypeArr[$key]);
          }
        }
        
        if(!file_exists($row['documento_ruta'])){
          $salida .= "    <tr>\n".
                     "      <td colspan=\"4\">No se encuentra el archivo asociado ".$row['documento_ruta']."</td>\n";
        }
        else{
          $nombreAlta = get_userRealName($row['usuario_alta']);
          
          $docType = get_docType($row['tipo_documento_id']);
          
          $mimeType = explode("/",mime_content_type($row['documento_ruta']));
          
          $salida .= "    <tr>\n".
                     "      <td>".$docType."</td>\n".
                     "      <td>".$row['fecha_carga']."</td>\n";
                     if(30<strlen($row["descripcion"]))
                       $salida .= "      <td title=\"".$row["descripcion"]."\">".substr($row["descripcion"],0,30)."...</td>\n"; 
                     else
                       $salida .= "      <td>".$row["descripcion"]."</td>\n";
          $salida .= "      <td><a href=/crm/modulos/perfil/index.php?id='".encrypt($row['usuario_alta'])."'>".$nombreAlta."</a></td>\n".
                     "      <td><a href=\"".$row['documento_ruta']."\" target=\"_blank\">";
                     
          if('image'==$mimeType[0])     
            $salida .= "<img src=\"".$row['documento_ruta']."\" class=\"img-responsive\" alt=\"".$docType."\" width=\"85\"></img>";
          elseif(array_key_exists($mimeType[0]."/".$mimeType[1],$relMimeImg))
            $salida .= "<img src=\"../../img/filetypes/".$relMimeImg[$mimeType[0]."/".$mimeType[1]]."\" class=\"img-responsive\" alt=\"".$docType."\"></img>";
          else
            $salida .= "<img src=\"../../img/filetypes/".$relMimeImg['other']."\" class=\"img-responsive\" alt=\"".$docType."\"></img>";
                     
          $salida .= "</a></td>\n";
                             
        }
        //if((array_key_exists(1, $gruposArr))||(array_key_exists(2, $gruposArr))||($row['usuario_alta']==$usuarioId)){
        $salida .= "      <td>"; 
        if(hasPermission($orgType,'d')||(hasPermission($orgType,'o')&&$row['usuario_alta']==$usuarioId)){ 
          $salida .= "<button type=\"button\" id=\"btn_documentDelete_".$row['documento_id']."\" title=\"Borrar Documento\" ".
                                       "class=\"btn btn-xs btn-danger\" onClick=\"deleteDocument(".$row['documento_id'].");\">".
                                       "<span class=\"glyphicon glyphicon-remove\"></span></button>"; 
        }
        if(hasPermission($orgType,'e')||(hasPermission($orgType,'n')&&$row['usuario_alta']==$usuarioId)){
          $salida .= "          <button type=\"button\" id=\"btn_documentEdit_".$row['documento_id']."\" title=\"Editar Documento\" ".
                                       "class=\"btn btn-xs btn-success\" onClick=\"editDocument(".$row['documento_id'].");\">".
                                       "<span class=\"glyphicon glyphicon-edit\"></span></button>";                   
        }
        $salida .= "      </td>";
        $salida .= "    </tr>\n";                 
      }
      
      foreach($docTypeArr as $key => $value){
        $salida .= "<tr>\n".
                   "  <td colspan=\"6\">".
                   "    <span class=\"glyphicon glyphicon-remove\" style=\"color:red\"></span>Documento ".$value[1]." No encontrado ".
                   "  </td>".
                   "</tr>\n";
      }
      
      $salida .= "  <tbody>\n".
                 "</table></div>\n";
    } 
    $salida .= "<input type=\"hidden\" id=\"hdn_docOrgId\" name=\"hdn_docOrgId\" class=\"form-control\" value=".$orgId.">".
               "<input type=\"hidden\" id=\"hdn_docOrgType\" name=\"hdn_docOrgType\" class=\"form-control\" value=".$orgType.">";
  } 
  mysqli_close($db_modulos);
  mysqli_close($db_catalogos);
  echo $salida;
}

//llamada de ajax. obtiene los datos de un documento para su edición o consulta
if("getDocumentDetails" == $_POST["action"]){
  $db_modulos   = condb_modulos();

  $docId = isset($_POST['docId']) ? $_POST['docId'] : -1;
  
  $query = "SELECT * FROM documentos WHERE documento_id=".$docId;
  
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $salida = "ERROR||Ocurrió un problema al obtener los datos del documento";
  }
  else{
    if(0>=mysqli_num_rows($result)){
      $salida = "ERROR||No se encontraron registros para el documento con este id";
    }
    else{
      $row = mysqli_fetch_assoc($result);
      
      $salida = "OK||".$row['tipo_documento_id']."||".$row['descripcion'];
    }
  }
  mysqli_close($db_modulos);
  echo $salida;
}

//llamada de ajax. borra un documento de su ubicación en el disco duro y elimina sus datos de la db
if("deleteDocument" == $_POST["action"]){
  $db_modulos   = condb_modulos();

  $docId = isset($_POST['docId']) ? $_POST['docId'] : -1;
  $orgId = isset($_POST['orgId']) ? $_POST['orgId'] : -1;
  $orgType = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
  
  switch($orgType){
    case 0:
      $orgName = "prospecto";
    break;
    case 1:
      $orgName = "cliente";
    break;
    case 2:
      $orgName = "sitio";
    break;
  }
 
  $query = "SELECT documento_ruta FROM documentos WHERE documento_id=".$docId;
  
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $salida = "ERROR||Ocurrió un problema al obtener la ruta del documento para su borrado";
  }
  else{
    $row = mysqli_fetch_assoc($result);    
    
    $query2 = "DELETE FROM documentos WHERE documento_id=".$docId;
    
    $result2 = mysqli_query($db_modulos,$query2);
    
    if(!$result2){
      $salida = "ERROR||Ocurrió un problema al borrar los datos del documento";
    }
    else{
      unlink("../../".$row['documento_ruta']);
      $salida = "OK||El documento se ha borrado con éxito";
      writeOnJournal($usuarioId,"Ha borrado el documento con id [".$docId."] del ".$orgName." con id ".$orgId);
      log_write("OK: UPLOAD-NEW-DOCUMENT: El archivo se eliminó junto con sus datos",4);
    }
    
    if(0==$orgType){
      $queryDocCons = "SELECT(SELECT COUNT(ctd.tipo_documento_id) FROM ".$cfgTableNameCat.".cat_tipo_documento AS ctd WHERE ctd.tipo_documento_id != 0) AS cont_tipos, ". 
                      "(SELECT COUNT(DISTINCT doc.tipo_documento_id) FROM ".$cfgTableNameMod.".documentos AS doc WHERE doc.origen_id=".$orgId." AND doc.tipo_origen=".$orgType.") AS cont_docsact FROM dual";
                      
      $resultDocCons = mysqli_query($db_modulos,$queryDocCons);
      
      if(!$resultDocCons){
        $salida = "ERROR||Ocurrió un problema colectar los datos del documento restantes";
      }
      else{
        $rowDocCons = mysqli_fetch_assoc($resultDocCons);
        if($rowDocCons['cont_tipos']>$rowDocCons['cont_docsact']){
          $salida = updProsCompleteDoc($orgId,0);
        }
      }     
    }
    else{
      //nada  
    }    
  } 
  mysqli_close($db_modulos);
  echo $salida;
}

function updProsCompleteDoc($prosId,$stat){
  $db_modulos = condb_modulos(); 
  global $mailData;
  global $nombre;
  $query = "UPDATE prospectos SET admin_auth = ".$stat." WHERE prospecto_id = ".$prosId;
  $result = mysqli_query($db_modulos,$query);
  if(!$result)
    $salida = false;
  else
    $salida = true;
  if($stat == 3){
    $query = "SELECT * FROM coecrm_modulos.prospectos WHERE prospecto_id = " . $prosId;
    $result = mysqli_query($db_modulos,$query);
    if(!$result){
      $salida = false;
    }else{
      // **************************************************+
      // **************************************************+
      // inicio de mensaje de notificacion de inicio de proceso de instalacion
      // notificar a gaby y vendedor para iniciar proceso de instalacion del prospect,
      // si hay incongruencias en los datos del prospecto no se puede proceder con la instalacion.
      // PUNTO A
      // **************************************************+
      // **************************************************+
      $row = mysqli_fetch_assoc($result);
      $mailData['dest'] = "agarcia@softernium.com,".get_userEmail($row['asignadoa']);
      $rs = ($row['razon_social'] != "") ? $row['razon_social'] : "N/A";
      $mailData['asunto'] = "Validacion completada.";
      $mailData['msg'] = "Hola, el usuario $nombre ha verificado los 3 check de validacion de documentos del prospecto: <br> Clave: ".
      $row['prospecto_id'].". <br> Nombre Comercial: ".
      $row['nombre_comercial'].".<br> Razon Social:". $rs .". <br> Contacta con Gabriela de cobranza para coordinar el proceso de instalacion creando una OT en base a la cotizacion correspondiente para seguir con el proceso del prospecto. <br><br>Acceso al sistema CRM <a href=\"https://crm.coeficiente.mx/crm/modulos/login/index.php\">Aquí</a>";
      $mailData['remitente'] = "";
      $msgVendedor = sendBDEmail($mailData);
      if($msgVendedor){
        $rs = ($row['razon_social'] != "") ? $row['razon_social'] : $row['nombre_comercial'];
        $mailData['dest'] = "agarcia@softernium.com,cobranza@coeficiente.mx";
        $mailData['asunto'] = "Validacion completada.";
        $mailData['msg'] = "Hola Gabriela, el prospecto $rs ha pasodo por los 3 checks de documentacion, el vendedor se pondra en contacto contigo para coordinar el proceso de instalacion. <br><br>Acceso al sistema CRM <a href=\"https://crm.coeficiente.mx/crm/modulos/login/index.php\">Aquí</a>";
        sendBDEmail($mailData);
      }
    }
      // **************************************************+
      // fin de notificacion de inicio de instalacion
      // **************************************************+
  }
  mysqli_close($db_modulos);
  return $salida;
}

function valProspectDoc($prosId,$option,$stat){
  global $db_catalogos;
  global $db_modulos;
  global $db_usuarios;
  global $usuarioId;
  $prosId = isset($_POST['prosId']) ? decrypt($_POST['prosId']) : -1;
  $option = isset($_POST['option']) ? $_POST['option'] : -1;
  $stat = isset($_POST['stat']) ? $_POST['stat'] : -1;
  if(0==$option){
    $comment = get_userRealName($usuarioId)." Ha retirado el check ".$stat." para este prospecto";
    $stat = $stat-1;
  }
  else{
    $comment = get_userRealName($usuarioId)." Ha validado el check ".$stat." para este prospecto";
  }
    // actualizacion de la validacion de los docs del prospecto
  if(!$salida = updProsCompleteDoc($prosId,$stat))
    $salida = "ERROR||Ocurrió un problema al activar la verificación de prospecto";
  else{
    if(!$salida = saveComment($prosId,0,$comment))
      $salida = false;
    else
      $salida = true;
  }
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);  
  return $salida;
}

/************Apartado de funciones de gestión de clientes (se usan en modulo clientes y facturación)*************/

//lamada de ajax. obtiene todos los datos de un cliente. Dependiendo su opción crea una tabla con ellos o envía los campos para llenar el formulario de editar cliente
if("getClientDetails" == $_POST["action"]){
  $db_modulos   = condb_modulos();  
  $cliId = isset($_POST['cliId']) ? decrypt($_POST['cliId']) : -1;
  $option = isset($_POST['option']) ? $_POST['option'] : -1;
  
  //if((array_key_exists(1, $gruposArr))||(array_key_exists(2, $gruposArr))){
    $query = "SELECT * FROM clientes WHERE cliente_id=".$cliId;
 /* }
  else{
    $query = "SELECT * FROM clientes WHERE (usuario_alta=".$usuarioId." OR asignadoa=".$usuarioId.") AND cliente_id=".$cliId;
  }*/
  $query2 = "SELECT rel.*, cont.* FROM rel_origen_contacto AS rel, contactos AS cont WHERE rel.tipo_origen=1 AND rel.origen_id=".$cliId." AND rel.contacto_id=cont.contacto_id ORDER BY cont.apellidos ASC";
  $query3 = "SELECT rep.* FROM representante_legal AS rep, clientes AS cli WHERE cliente_id=".$cliId." AND cli.representante_id=rep.representante_id";
  $result = mysqli_query($db_modulos,$query);
  $result2 = mysqli_query($db_modulos,$query2);
  $result3 = mysqli_query($db_modulos,$query3); 
  if(!$result||!$result2||!$result3){
    $salida = "ERROR|| No se pudo cargar los detalles para este cliente de la DB ".mysqli_error($db_modulos);
  }
  else{
    if(0==mysqli_num_rows($result)){
      $salida = "ERROR||No existen registros para el cliente con este id";
    }
    else{
      if(0==$option){
        $salida = "<table id=\"tbl_clientDetails\" class=\"table table-striped\">\n".
                  "  <thead>\n".
                  "    <tr>\n".
                  "      <th>Campo</th>\n".
                  "      <th>Valor</th>\n".
                  "    </tr>\n".
                  "  </thead>\n".
                  "  <tbody>\n";
        $row = mysqli_fetch_assoc($result);
          $estatus = get_sellStat($row['estatus']);
          if(0<strlen($row['origen_otro']))
            $origen = $row['origen_otro'];
          else
            $origen = get_sellOrigin($row['origen']);
          $nombreAlta = get_userRealName($row['usuario_alta']);
          //$nombreAsign = get_userRealName($row['asignadoa']);
          $estado = get_state($row['estado_fiscal']);
          $tipoPer = get_personType($row['tipo_persona']);
          $salida .= "    <tr>\n".
                     "      <td><b>ID</b></td>\n".
                     "      <td>".$row["cliente_id"]."</td>\n".
                     "    </tr>\n".
                     "    <tr>\n".
                     "      <td><b>Razón Social</b></td>\n".
                     "      <td>".$row["razon_social"]."</td>\n".
                     "    </tr>\n".
                     "    <tr>\n".
                     "      <td><b>RFC ó CURP</b></td>\n".
                     "      <td>".$row["rfc_curp"]."</td>\n".
                     "    </tr>\n".
                     "    <tr>\n".
                     "      <td><b>Tipo de Persona</b></td>\n".
                     "      <td>".$tipoPer."</td>\n".
                     "    </tr>\n".
                     "    <tr>\n".
                     "      <td><b>Nombre Comercial</b></td>\n".
                     "      <td>".$row["nombre_comercial"]."</td>\n".
                     "    </tr>\n".
                     "    <tr>\n".
                     "      <td><b>Teléfono</b></td>\n".
                     "      <td>".$row["telefono"]."</td>\n".
                     "    </tr>\n".
                     "    <tr>\n".
                     "      <td><b>Teléfono Móvil</b></td>\n".
                     "      <td>".$row["tel_movil"]."</td>\n".
                     "    </tr>\n".
                     "      <td><b>Correo Electrónico</b></td>\n".
                     "      <td>".$row["email"]."</td>\n".
                     "    </tr>\n".
                     "    <tr>\n";
                  if(2==$row['tipo_persona']){
                    $salida .= "    <tr>\n".
                               "      <td><b>Correo Electrónico Alterno</b></td>\n".
                               "      <td>".$row["email_alt"]."</td>\n".
                               "    </tr>\n".
                               "    <tr>\n".
                               "      <td><b>#Acta Constitutiva</b></td>\n".
                               "      <td>".$row["acta_constitutiva"]."</td>\n".
                               "    </tr>\n".
                               "    <tr>\n".
                               "      <td><b>Fecha de Acta</b></td>\n".
                               "      <td>".$row["fecha_acta"]."</td>\n".
                               "    </tr>\n";  
                   }                   
          $salida .= "    <tr>\n".
                     "      <td><b>Domicilio Fiscal</b></td>\n".
                     "      <td>".$row["domicilio_fiscal"]."</td>\n".
                     "    </tr>\n".
                     "    <tr>\n".
                     "      <td><b>Colonia Fiscal</b></td>\n".
                     "      <td>".$row["colonia_fiscal"]."</td>\n".
                     "    </tr>\n".
                     "    <tr>\n".
                     "      <td><b>CP Fiscal</b></td>\n".
                     "      <td>".$row["cp_fiscal"]."</td>\n".
                     "    </tr>\n".
                     "    <tr>\n".
                     "      <td><b>Municipio Fiscal</b></td>\n".
                     "      <td>".$row["municipio_fiscal"]."</td>\n".
                     "    </tr>\n".
                     "    <tr>\n".
                     "      <td><b>Estado Fiscal</b></td>\n".
                     "      <td>".$estado."</td>\n".
                     "    </tr>\n".  
                     "    <tr>\n".
                     "      <td><b>Origen Cliente</b></td>\n".
                     "      <td>".$origen."</td>\n".
                     "    </tr>\n".                     
                     "    <tr>\n".
                     "      <td><b>Fecha de Alta</b></td>\n".
                     "      <td>".$row["fecha_alta"]."</td>\n".
                     "    </tr>\n".
                     "    <tr>\n".
                     "      <td><b>Usuario que lo Agregó</b></td>\n".
                     "      <td><a href=/crm/modulos/perfil/index.php?id='".encrypt($row['usuario_alta'])."'>".$nombreAlta."</a></td>\n".
                     "    </tr>\n".  
                     "    <tr>\n".
                     "      <td><b>Descripción</b></td>\n".
                     "      <td>".$row["descripcion"]."</td>\n".
                     "    </tr>\n";      
        $salida .= "  </tbody>\n".
                   "</table>\n<hr>";
        //$query3 = "SELECT * FROM representante_legal WHERE representante_id=".$row["representante_id"];        
        $salida .= "<h4><b>Representante Legal:<b></h4>";
        if(!$result3){
          $salida .= "Ocurrió un problema al obtener los datos del representante legal";
        }
        else{
          if(0==mysqli_num_rows($result3)){
            $salida .= "No cuenta con representante legal";
          }
          else{          
            $salida .= "<div class=\"table-responsive\"><table id=\"tbl_ClientRep\" class=\"table table-striped\">\n".
                       "  <thead>\n".
                       "    <tr>\n".
                       "      <th class=\"hidden-xs\">id</th>\n".
                       "      <th>Número de poder</th>\n".
                       "      <th>Fecha de poder</th>\n".
                       "      <th>Nombre</th>\n".
                       "      <th>Apellidos</th>\n".
                       "      <th>Teléfono</th>\n".
                       "      <th>Teléfono Móvil</th>\n".
                       "      <th>Domicilio</th>\n".
                       "      <th>Colonia</th>\n".
                       "      <th>C.P.</th>\n".
                       "      <th>Municipio</th>\n".
                       "      <th>Estado</th>\n".                       
                       "      <th>Correo Electŕonico</th>\n".
                       "    </tr>\n".
                       "  </thead>\n".
                       "  <tbody>\n";
            $row3 = mysqli_fetch_assoc($result3); 
            $estadoRep = get_state($row3['estado']);       
            $salida .= "    <tr>\n".
                       "      <td class=\"hidden-xs\">".$row3["representante_id"]."</td>\n".
                       "      <td>".$row3["num_poder"]."</td>\n".
                       "      <td>".$row3["fecha_poder"]."</td>\n".
                       "      <td>".$row3["nombre"]."</td>\n".
                       "      <td>".$row3["apellidos"]."</td>\n".
                       "      <td>".$row3["telefono"]."</td>\n".
                       "      <td>".$row3["tel_movil"]."</td>\n".
                       "      <td>".$row3["domicilio"]."</td>\n".
                       "      <td>".$row3["colonia"]."</td>\n".         
                       "      <td>".$row3["cp"]."</td>\n".
                       "      <td>".$row3["municipio"]."</td>\n".
                       "      <td>".$estadoRep."</td>\n".
                       "      <td>".$row3["email"]."</td>\n".
                       "    </tr>\n";
            $salida .= "  </tbody>\n".
                       "</table>\n</div>"; 
          }
        }
        $salida .= "<h4><b>Contactos:<b></h4>";           
        if(0==mysqli_num_rows($result2)){
          $salida .= "No existen contactos para este cliente";
        }
        else{          
          $salida .= "<div class=\"table-responsive\"><table id=\"tbl_ClientContacts\" class=\"table table-striped\">\n".
                     "  <thead>\n".
                     "    <tr>\n".
                     "      <th class=\"hidden-xs\">id</th>\n".
                     "      <th>Nombre</th>\n".
                     "      <th>Apellidos</th>\n".
                     "      <th>Puesto</th>\n".
                     "      <th>Teléfono</th>\n".
                     "      <th>Teléfono Móvil</th>\n".
                     "      <th>Correo Electŕonico</th>\n".
                     "    </tr>\n".
                     "  </thead>\n".
                     "  <tbody>\n";   
                             
          while($row2 = mysqli_fetch_assoc($result2)){        
            $salida .= "    <tr>\n".
                       "      <td class=\"hidden-xs\">".$row2["contacto_id"]."</td>\n".
                       "      <td>".$row2["nombre"]."</td>\n".
                       "      <td>".$row2["apellidos"]."</td>\n".
                       "      <td>".$row2["puesto"]."</td>\n".
                       "      <td>".$row2["telefono"]."</td>\n".
                       "      <td>".$row2["tel_movil"]."</td>\n".
                       "      <td>".$row2["email"]."</td>\n".         
                       "    </tr>\n";
          }        
          $salida .= "  </tbody>\n".
                     "</table>\n</div>";        
        }
      }
      
      if(1==$option){
        while($row = mysqli_fetch_assoc($result)){
          $fecActa = ("0000-00-00 00:00:00"==$row["fecha_acta"])?"":$row["fecha_acta"];
          $salida = "OK||".
                    $row['cliente_id']."||".
                    $row['razon_social']."||".
                    $row['nombre_comercial']."||".
                    $row['rfc_curp']."||".
                    $row['tipo_persona']."||".
                    $row['telefono']."||".
                    $row['tel_movil']."||".
                    $row['email']."||".
                    $row['email_alt']."||".
                    //$row['estatus']."||".
                    $row['origen']."||".
                    $row['origen_otro']."||".
                    $row['acta_constitutiva']."||".
                    $fecActa."||".
                    $row['representante_id']."||".
                    $row['domicilio_fiscal']."||".
                    $row['colonia_fiscal']."||".
                    $row['cp_fiscal']."||".
                    $row['municipio_fiscal']."||".
                    $row['estado_fiscal']."||".
                    //$row['asignadoa']."||".
                    $row['fecha_alta']."||".
                    $row['usuario_alta']."||".
                    $row['descripcion']."||";                                        
        }
        
        $contArr = array();
        while($row2 = mysqli_fetch_assoc($result2)){
          $contArr[] = array($row2["contacto_id"],$row2["nombre"],$row2["apellidos"],$row2["puesto"],$row2["telefono"],$row2["tel_movil"],$row2["email"]);
        }
        $salida .= json_encode($contArr);
        
        $salida .= "||";
        
        $repArr = array();
        $row3 = mysqli_fetch_assoc($result3);
        $repArr[] = array($row3["representante_id"],$row3["nombre"],$row3["apellidos"],$row3["num_poder"],$row3["fecha_poder"],$row3["domicilio"],$row3["colonia"],$row3["cp"],$row3["municipio"],$row3["estado"],$row3["telefono"],$row3["tel_movil"],$row3["email"]);
        
        $salida .= json_encode($repArr);       
      }                           
    }
  }
  mysqli_close($db_modulos);
  echo $salida;
}

/************Fin de apartado*************/
/************Apartado de funciones de gestión de sitios***************/

if("getOrgSites" == $_POST['action']){
  $db_modulos   = condb_modulos();
  
  $orgId = isset($_POST['orgId']) ? $_POST['orgId'] : -1;
  $orgType = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
  $option = isset($_POST['option']) ? $_POST['option'] : -1;
  $connectionId = isset($_POST['connId']) ? $_POST['connId'] : -1; //checar si es viable
  
  if(6==$orgType)
    $query = "SELECT * FROM sitios WHERE sitio_id>0 AND tipo_origen=1";  
  else
    $query = "SELECT * FROM sitios WHERE origen_id=".$orgId." AND tipo_origen=".$orgType." AND sitio_id>0";
    
  
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $salida = "ERROR||Ocurrió un problema al obtener los datos de los sitios";
  }
  else{
     if(hasPermission(2,'w')&&($orgType!=6)&&($option==0)){
       $salida = "<div class=\"col container-fluid\">".
                 "  <button type=\"button\" id=\"btn_eventNew\" title=\"Nuevo Sitio\" class=\"btn btn-default\" onClick=\"openNewSite();\"><span class=\"glyphicon glyphicon-asterisk\"></span> Nuevo Sitio</button><br><br>".
                 "</div>";
     }   
     
    if(0==mysqli_num_rows($result)){
      if($option==0)
        $salida .= "Este resgistro no tiene sitios registrados. Haga click en <strong>Nuevo Sitio</strong> para agregar uno nuevo";
      else
        $salida .= "Este resgistro no tiene sitios registrados. Agrega un sitio haciendo click en el botón <span class=\"glyphicon glyphicon-map-marker\"/> desde las opciones del prospecto o cliente";
    }
    else{
      $salida .= "<div class=\"table-responsive\"><table id=\"tbl_sites_".$option."\" class=\"table table-striped\">\n".
                "  <thead>\n".
                "    <tr>\n".
                "      <th class=\"hidden-xs\">Id</th>\n".
                "      <th>Nombre (sucursal)</th>\n".
                "      <th>Cruces</th>\n".
                "      <th>Referencias</th>\n".
                "      <th>Acciones</th>\n".
                "    </tr>\n".
                "  </thead>\n".
                "  <tbody>\n"; 
      $chkCount=0;
      while($row = mysqli_fetch_assoc($result)){
        $salida .= "    <tr>\n".
                   "      <td class=\"hidden-xs\">".$row['sitio_id']."</td>\n".
                   "      <td>".$row['nombre']."</td>\n".
                   "      <td>".$row['calle1']." y ".$row['calle2']."</td>\n".
                   "      <td>".$row['sitio_referencias']."</td>\n".
                   "      <td>";
                   
        if(6==$orgType){
          $salida .= "        <button type=\"button\" id=\"btn_siteTicketAssign_".$row['sitio_id']."\" title=\"Asignar\" class=\"btn btn-xs btn-primary btn-default\" onClick=\"assignTicketToSite('".encrypt($row['sitio_id'])."','".encrypt($row['origen_id'])."');\">Asignar Sitio</button>";
          $orgType2 = 1;
        }
        else{
          $orgType2 = $orgType;
        }
        if(1==$option){
          if(0==$chkCount){
            $salida .= "        <input type=\"radio\" name=\"rad_contrSiteSel\" id=\"rad_".$row['sitio_id']."\" class=\" rad_responsive\" value=".encrypt($row['sitio_id'])." checked>Seleccionar</input><br>";
          }
          else{
            $salida .= "        <input type=\"radio\" name=\"rad_contrSiteSel\" id=\"rad_".$row['sitio_id']."\" class=\" rad_responsive\" value=".encrypt($row['sitio_id']).">Seleccionar</input><br>";
          }
        }        
        else{
          $salida .= "        <button type=\"button\" id=\"btn_siteDetails_".$row['sitio_id']."\" title=\"Ver Detalles\" class=\"btn btn-xs btn-primary btn-responsive\" onClick=\"goToSiteDetails('".encrypt($row['sitio_id'])."','".encrypt($orgType2)."');\"><span class=\"glyphicon glyphicon-list\"></span></button>";

          if(hasPermission(2,'d')){      
            $salida .= "        <button type=\"button\" id=\"btn_siteDelete_".$row['sitio_id']."\" title=\"Eliminar Cliente\" class=\"btn btn-xs btn-danger btn-responsive\" onClick=\"deleteSite(".$row['sitio_id'].");\"><span class=\"glyphicon glyphicon-remove\"></span></button>";
          } 
          if(2==$option){
            $salida .= "        <button type=\"button\" id=\"btn_siteAsignToConn_".$row['sitio_id']."\" title=\"Asignar a Enlace\" class=\"btn btn-xs btn-default btn-responsive\" onClick=\"assignSiteToConn('".encrypt($row['sitio_id'])."','".$connectionId."');\">Asignar a enlace</button>";
          }               
        }
        $salida .= "      </td>\n".
                   "    </tr>\n";
      }
      $salida .= "  </tbody>\n";      
      $salida .= "</table></div>\n";
    } 
  }
  mysqli_close($db_modulos);
  echo $salida;
}

if("getSiteDetails" == $_POST['action']){
  $db_modulos   = condb_modulos();
  
  $siteId = isset($_POST['orgId']) ? $_POST['orgId'] : -1;
  $orgType = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
  $option = isset($_POST['option']) ? $_POST['option'] : -1;
  
  if(0==$orgType){
    $query = "SELECT sit.*, pro.nombre_comercial FROM sitios AS sit, prospectos AS pro WHERE sit.origen_id=pro.prospecto_id AND sit.tipo_origen=0 AND sit.sitio_id=".$siteId;
  }
  else if(1==$orgType){
    $query = "SELECT sit.*, cli.nombre_comercial FROM sitios AS sit, clientes AS cli WHERE sit.origen_id=cli.cliente_id AND sit.tipo_origen=1 AND sit.sitio_id=".$siteId;
  }
  else{
    $salida = "ERROR||No se recibió el tipo de origen para el despliegue de los sitios";
  }
  $query2 = "SELECT rel.*, cont.*, dep.departamento, pue.puesto AS puestoPref ".
            "FROM ".$cfgTableNameMod.".rel_origen_contacto AS rel, contactos AS cont, ".
            $cfgTableNameCat.".cat_departamentos AS dep, ".
            $cfgTableNameCat.".cat_puestos AS pue ".
            "WHERE rel.tipo_origen=2 AND rel.origen_id=".$siteId.
            " AND rel.contacto_id=cont.contacto_id ".
            "AND cont.puesto_id = pue.puesto_id AND cont.departamento_id=dep.departamento_id ".
            "ORDER BY cont.apellidos ASC";
  
  $result = mysqli_query($db_modulos,$query);
  $result2 = mysqli_query($db_modulos,$query2);
  // var_dump($query2);
  if(!$result||!$result2){
    $salida = "ERROR||Ocurrió un problema al consultar los datos del sitio ".$orgType;
  }
  else{
    if(0==mysqli_num_rows($result)){
      $salida = "ERROR||No se encontraron registros con este id";
    }
    else{
      if(0==$option){
        $row = mysqli_fetch_assoc($result);
      
        $salida = "";                   
        if(hasPermission(2,'r')||(hasPermission(2,'l')&&$row['usuario_alta']==$usuarioId)){           
          $salida .= "        <button type=\"button\" id=\"btn_siteComments_".$row['sitio_id']."\" title=\"Ver Comentarios\" class=\"btn btn-xs btn-info btn-responsive\" onClick=\"getComments(".$row['sitio_id'].",2);\"><span class=\"glyphicon glyphicon-comment\"> Ver comentarios</span></button>";
        }    
        if(hasPermission(2,'e')||(hasPermission(2,'n')&&$row['usuario_alta']==$usuarioId)){
          $salida .= "        <button type=\"button\" id=\"btn_siteEdit_".$row['sitio_id']."\" title=\"Editar Registro\" class=\"btn btn-xs btn-success btn-responsive\" onClick=\"editSite('".$row['sitio_id']."');\"><span class=\"glyphicon glyphicon-edit\"> Editar</span></button>";                       
        } 
        if(hasPermission(2,'r')||hasPermission(2,'w')){  //si tiene permisos para leer datos de clientes 
          $salida .= "        <button type=\"button\" id=\"btn_siteDocs_".$row['sitio_id']."_2\" title=\"Ver Documentación\" class=\"btn btn-xs btn-primary btn-responsive\" onClick=\"displayDocuments(".$row['sitio_id'].",2);\"><span class=\"glyphicon glyphicon-file\">Documentos Asociados</span></button>";
        }
                                               

        $salida .= "<div class=\"table-responsive\"><table id=\"tbl_siteDetails\" class=\"table table-hover table-striped table-condensed\">\n".
                  "  <thead>\n".
                  "    <tr>\n".
                  "      <th>Campo</th>\n".
                  "      <th>Valor</th>\n".
                  "    </tr>\n".
                  "  </thead>\n".
                  "  <tbody>\n";
                          
        $nombreAlta = get_userRealName($row['usuario_alta']);
        $estado = get_state($row['estado']);
        
        $salida .= "    <tr>\n".
                   "      <td><b>ID</b></td>\n".
                   "      <td>".$row["sitio_id"]."</td>\n".
                   "    </tr>\n".
                   "    <tr>\n".
                   "      <td><b>Cliente</b></td>\n".
                   "      <td>".$row["nombre_comercial"]."</td>\n".
                   "    </tr>\n".
                   "    <tr>\n".
                   "      <td><b>Nombre (sucursal)</b></td>\n".
                   "      <td>".$row["nombre"]."</td>\n".
                   "    </tr>\n".
                   "    <tr>\n".
                   "      <td><b>Teléfono</b></td>\n".
                   "      <td>".$row["telefono"]."</td>\n".
                   "    </tr>\n".
                   "    <tr>\n".
                   "      <td><b>Domicilio</b></td>\n".
                   "      <td>".$row["domicilio"]."</td>\n".
                   "    </tr>\n".
                   "    <tr>\n".
                   "      <td><b>Colonia</b></td>\n".
                   "      <td>".$row["colonia"]."</td>\n".
                   "    </tr>\n".
                   "    <tr>\n".
                   "      <td><b>C.P.</b></td>\n".
                   "      <td>".$row["cp"]."</td>\n".
                   "    </tr>\n".
                   "    <tr>\n".
                   "      <td><b>Municipio</b></td>\n".
                   "      <td>".$row["municipio"]."</td>\n".
                   "    </tr>\n".
                   "    <tr>\n".
                   "      <td><b>Estado</b></td>\n".
                   "      <td>".$estado."</td>\n".
                   "    </tr>\n".
                   "    <tr>\n".
                   "      <td><b>Entre Calles</b></td>\n".
                   "      <td>".$row["calle1"]." y ".$row["calle2"]."</td>\n".
                   "    </tr>\n".
                   "    <tr>\n".
                   "      <td><b>Referencias</b></td>\n".
                   "      <td>".$row["sitio_referencias"]."</td>\n".
                   "    </tr>\n".                
                   "    <tr>\n".
                   "      <td><b>Fecha de Alta</b></td>\n".
                   "      <td>".$row["fecha_alta"]."</td>\n".
                   "    </tr>\n".
                   "    <tr>\n".
                   "      <td><b>Usuario que lo Agregó</b></td>\n".
                   "      <td><a href=/crm/modulos/perfil/index.php?id='".encrypt($row['usuario_alta'])."'>".$nombreAlta."</a></td>\n".
                   "    </tr>\n".
                   "    <tr>\n".
                   "      <td><b>Descripción</b></td>\n".
                   "      <td>".$row["descripcion"]."</td>\n".
                   "    </tr>\n".
                   "  </tbody>\n".
                   "</table>\n<hr></div>";                
                   
        $salida .= "<h4><b>Contactos:</b></h4>";           
        if(0==mysqli_num_rows($result2)){
          $salida .= "No existen contactos para este sitio";
        }
        else{          
          $salida .= "<div class=\"table-responsive\"><table id=\"tbl_SiteContacts\" class=\"table table-striped table-condensed\">\n".
                     "  <thead>\n".
                     "    <tr>\n".
                     "      <th class=\"hidden-xs\">id</th>\n".
                     "      <th>Nombre</th>\n".
                     "      <th>Apellidos</th>\n".
                     "      <th class=\"hidden-xs\">Puesto</th>\n".
                     "      <th>Teléfono</th>\n".
                     "      <th>Teléfono Móvil</th>\n".
                     "      <th>Correo Electŕonico</th>\n".
                     "    </tr>\n".
                     "  </thead>\n".
                     "  <tbody>\n";   
                             
          while($row2 = mysqli_fetch_assoc($result2)){ 
          
            if(6==$row2['puesto_id'])
              $puesto = $row2['puesto'];
            else
              $puesto = $row2['puestoPref']; 
                    
            $salida .= "    <tr>\n".
                       "      <td class=\"hidden-xs\">".$row2["contacto_id"]."</td>\n".
                       "      <td>".$row2["nombre"]."</td>\n".
                       "      <td>".$row2["apellidos"]."</td>\n".
                       "      <td class=\"hidden-xs\">".$puesto."</td>\n".
                       "      <td>".$row2["telefono"]."</td>\n".
                       "      <td>".$row2["tel_movil"]."</td>\n".
                       "      <td>".$row2["email"]."</td>\n".         
                       "    </tr>\n";
          }        
          $salida .= "  </tbody>\n".
                     "</table>\n</div>||".$row['latitud']."||".$row['longitud'];               
        }              
      }
      
      if(1==$option){
        $row = mysqli_fetch_assoc($result);
      
        $salida = "OK||".
                  $row["sitio_id"]."||".
                  $row["origen_id"]."||".                  
                  $row["nombre"]."||".
                  $row["telefono"]."||".
                  $row["domicilio"]."||".
                  $row["colonia"]."||".
                  $row["cp"]."||".
                  $row["municipio"]."||".
                  $row["estado"]."||".
                  $row["calle1"]."||".
                  $row["calle2"]."||".
                  $row["sitio_referencias"]."||".
                  $row["fecha_alta"]."||".
                  $row["usuario_alta"]."||".
                  $row["descripcion"]."||".
                  $row["tipo_origen"]."||".
                  $row["latitud"]."||".
                  $row["longitud"]."||";
        
        $contArr = array();
        while($row2 = mysqli_fetch_assoc($result2)){
          $contArr[] = array($row2["contacto_id"],$row2["nombre"],$row2["apellidos"],$row2["puesto"],$row2["telefono"],$row2["tel_movil"],$row2["email"],$row2["puesto_id"],$row2["departamento_id"],$row2["puestoPref"],$row2["departamento"],$row2["titulo_id"]);
        }
        
        $salida .= json_encode($contArr);
      } 
    }    
  }
  mysqli_close($db_modulos);
  echo $salida;
}

if("saveSiteNew" == $_POST['action']){
  $db_modulos   = condb_modulos();
  
  $option  = isset($_POST["sitOption"]) ? $_POST["sitOption"] : -1;
  $sitId   = isset($_POST["sitId"])   ? $_POST["sitId"] : -1;
  
  $sitNom  = isset($_POST["sitNom"])  ? $_POST["sitNom"]  : "";
  $sitTel  = isset($_POST["sitTel"])  ? $_POST["sitTel"]  : "";
  $sitDom  = isset($_POST["sitDom"])  ? $_POST["sitDom"]  : "";
  $sitCol  = isset($_POST["sitCol"])  ? $_POST["sitCol"]  : "";
  $sitCp   = isset($_POST["sitCp"])   ? $_POST["sitCp"]   : "";
  $sitMun  = isset($_POST["sitMun"])  ? $_POST["sitMun"]  : "";
  $sitCa1  = isset($_POST["sitCa1"])  ? $_POST["sitCa1"]  : "";
  $sitCa2  = isset($_POST["sitCa2"])  ? $_POST["sitCa2"]  : "";
  $sitRef  = isset($_POST["sitRef"])  ? $_POST["sitRef"]  : "";
  $sitDesc = isset($_POST["sitDesc"]) ? $_POST["sitDesc"] : "";
  $sitEdo  = isset($_POST["sitEdo"])  ? $_POST["sitEdo"]  : 0;
  $contArr = isset($_POST["sitContactoArr"])  ? json_decode($_POST["sitContactoArr"]) : "";
  $sitLat  = isset($_POST["sitLatitud"])  ? $_POST["sitLatitud"]  : -1;
  $sitLon  = isset($_POST["sitLongitud"])  ? $_POST["sitLongitud"]  : -1;
  
  $orgType = isset($_POST["orgType"]) ? $_POST["orgType"] : -1;
  $orgId   = isset($_POST["orgId"])   ? $_POST["orgId"]   : -1;
  
  $fecha  = date("Y-m-d H:i:s");
  
  log_write("DEBUG: SAVE-SITE-NEW: Origen Id ".$_POST["orgId"],4);
  log_write("DEBUG: SAVE-SITE-NEW: Origen Tipo ".$_POST["orgType"],4);
  log_write("DEBUG: SAVE-SITE-EDIT: Sitio Id ".$_POST["sitId"],4);

  log_write("DEBUG: SAVE-SITE-NEW: contactoArray ".print_r($contArr,true),4);
  
  if(0==$option){
    log_write("DEBUG: SAVE-SITE-NEW: Se va a insertar un sitio nuevo",4);
    
    $query = "INSERT INTO sitios(origen_id,".
                                "tipo_origen,".
                                "nombre,".
                                "telefono,".
                                "domicilio,".
                                "colonia,".
                                "cp,".
                                "municipio,".
                                "estado,".
                                "calle1,".
                                "calle2,".
                                "sitio_referencias,".
                                "descripcion,".
                                "usuario_alta,".
                                "fecha_alta,".
                                "latitud,".
                                "longitud) ".
             "VALUES(".$orgId.",".$orgType.",'".$sitNom."','".$sitTel."','".$sitDom."','".$sitCol."',".$sitCp.",'".$sitMun."',".$sitEdo.",'".$sitCa1."','".$sitCa2."','".$sitRef."','".$sitDesc."',".$usuarioId.",'".$fecha."','".$sitLat."','".$sitLon."')";
             
    $result = mysqli_query($db_modulos,$query);
    
    log_write("DEBUG: SAVE-SITE-NEW: ".$query,4);
    
    if(!$result){
      log_write("ERROR: SAVE-SITE-NEW: Ocurrió un problema al guardarse los datos del sitio nuevo",4);
      $salida = "ERROR||Ocurrió un problema al guardarse los datos del sitio nuevo";
    }
    else{
      log_write("DEBUG: SAVE-SITE-NEW: Se insertaron los datos generales del sitio. Se van a insertar sus contactos",4);
      $newSitId = mysqli_insert_id($db_modulos);
      
      log_write("DEBUG: SAVE-SITE-NEW: Nuevo id de Sitio ".$newSitId,4);
      
      foreach($contArr as $key => $value){
        if(!insertContact($db_modulos,$value,2,$newSitId)){
          log_write("ERROR: SAVE-SITE-NEW: Ocurrió un problema al guardarse los datos del contacto",4);
          goto insSite;
        }
      }
      writeOnJournal($usuarioId,"Ha insertado un sitio nuevo id [".$newSitId."] para el origen tipo [".$orgType."] con id [".$orgId."]");
      log_write("OK: SAVE-SITE-NEW: El sitio se ha insertado exitosamente",4);
      //updateOriginStatus($orgType,5,$orgId);
      $salida = "OK||Los datos se han guardado con éxito"; 
    } 
  }
  
  if(1==$option){    
    log_write("DEBUG: SAVE-SITE-UPDATE: Se va a editar un sitio existente",4);
    
    $query = "UPDATE sitios SET nombre='".$sitNom."', telefono='".$sitTel."', domicilio='".$sitDom."', colonia='".$sitCol."', cp=".$sitCp.", municipio='".$sitMun."', estado=".$sitEdo.", calle1='".$sitCa1."', calle2='".$sitCa2."', sitio_referencias='".$sitRef."', descripcion='".$sitDesc."', latitud='".$sitLat."', longitud='".$sitLon."' WHERE sitio_id=".$sitId;
    
    $query2 = "SELECT rel.*, cont.* FROM rel_origen_contacto AS rel, contactos AS cont WHERE rel.tipo_origen=2 AND rel.origen_id=".$sitId." AND rel.contacto_id=cont.contacto_id";
    
    $result = mysqli_query($db_modulos,$query);
    
    log_write("DEBUG: SAVE-SITE-UPDATE: ".$query,4);
    
    if(!$result){
      log_write("ERROR: Ocurrió un problema al actualizar los datos del sitio",4);
      $salida = "ERROR||Ocurrió un problema al actualizar los datos del sitio";
    }
    else{
      log_write("DEBUG: Se actualizaron los datos generales del sitio, se van a acatualizar los contactos",4);
      
      if(!updateContactsByArray($contArr,$query2,$salida,$sitId,2)){ 
        log_write("DEBUG: SAVE-SITE-UPDATE: No se actualizaron los contactos de este sitio",4);
        $salida = "ERROR|| Ocurrió un problema al actualizarse la lista de los contactos";
      }
      else{
        writeOnJournal($usuarioId,"Ha modificado los datos del sitio con id [".$sitId."]");  
        log_write("DEBUG: SAVE-SITE-UPDATE: Los contactos para este sitio fueron actualizados",4);
        $salida = "OK|| Los datos se han actualizado con éxito";
      }
    }
  }
     
  insSite:
  mysqli_close($db_modulos);
  echo $salida;
}

if("assignSiteToConn"==$_POST['action']){
  $db_modulos   = condb_modulos();
  $sitId = isset($_POST['siteId']) ? decrypt($_POST['siteId']) : -1;
  $relCotPro = isset($_POST['relCotPro']) ? decrypt($_POST['relCotPro']) : -1;
  
  $query = "UPDATE rel_cotizacion_producto SET sitio_asignado = ".$sitId." WHERE relacion_id = ".$relCotPro;
  
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result||-1==$sitId||-1==$relCotPro){
    $salida = "ERROR||Ocurrió un error al asignar el sitio al enlace";
  }
  else{
    writeOnJournal($usuarioId,"Ha asignado el sitio con id [".$sitId."] al enlace con id [".$relCotPro."]");
    $salida = "OK||El sitio se ha asignado correctamente";
  }
    
  mysqli_close($db_modulos);
  echo $salida;
}

//llamada de ajax. Borra el sitio seleccionado
if("deleteSite"==$_POST['action']){
  $db_modulos   = condb_modulos();
  
  $sitId = isset($_POST['sitId']) ? $_POST['sitId'] : 0;
  
  $query = "DELETE FROM sitios WHERE sitio_id=".$sitId;
  
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $salida = "ERROR||El sitio no pudo borrarse";
  }
  else{
    writeOnJournal($usuarioId,"Ha eliminado evento con id [".$sitId."]");
    $salida = "OK||El sitio se ha eliminado correctamente";
  }
  
  mysqli_close($db_modulos);
  echo $salida;
}
/*************Fin de sección*****************/

/*************Gestión de ordenes de trabajo*************/

if("getWorkOrderList" == $_POST['action']){
  $db_modulos   = condb_modulos();
  
  $relCotPro = isset($_POST['relCotPro']) ? decrypt($_POST['relCotPro']) : -1;
  $orgType = isset($_POST['orgType']) ? decrypt($_POST['orgType']) : -1;
  
  $query = "SELECT ordTra.*, ordTraTyp.*, ordTraSta.estatus ".
           "FROM ".$cfgTableNameMod.".ordenes_trabajo AS ordTra, ".
                   $cfgTableNameCat.".cat_orden_trabajo_tipo AS ordTraTyp, ".
                   $cfgTableNameCat.".cat_orden_trabajo_estatus AS ordTraSta ".
           "WHERE ordTra.rel_cotizacion_producto = ".$relCotPro." ".
           "AND ordTraSta.estatus_id = ordTra.estatus ".
           "AND ordTra.tipo_orden_id=ordTraTyp.tipo_orden_id";
  
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $salida = "ERROR: Ocurrió un problema al consultar las ordenes de trabajo"; 
  }
  else{
 
    if(0==$orgType&&(!hasPermission(4,'w')||!hasPermission(7,'w'))){
      $salida = "<button type=\"button\" id=\"btn_workOrderNew\" title=\"Nueva Orden de Trabajo\" class=\"btn btn-default\" onClick=\"openNewWorkOrder(-1);\"><span class=\"glyphicon glyphicon-asterisk\"></span> Nueva Orden de Trabajo</button><br><br>";
    } 
    else{        
      $salida = "<button type=\"button\" id=\"btn_workOrderNew\" title=\"Nueva Orden de Trabajo\" class=\"btn btn-default\" onClick=\"openNewWorkOrder(0);\"><span class=\"glyphicon glyphicon-asterisk\"></span> Nueva Orden de Trabajo</button><br><br>";
    }
    if(0>=mysqli_num_rows($result)){
      $salida .= "No existen ordenes de trabajo para este enlace. Haga click en <b>Nueva Orden</b> para crear una";  
    }
    else{
      $salida .= "<div class=\"table-responsive\"><table id=\"tbl_workOrders\" class=\"table table-striped\">\n".
                 "  <thead>\n".
                 "    <tr>\n".
                 "      <th>Tipo Orden</th>\n".
                 "      <th>Descripción</th>\n".
                 "      <th>Estatus</th>\n".                   
                 "      <th>Fecha de Alta</th>\n".
                 "      <th>Acciones</th>\n".
                 "    </tr>\n".
                 "  </thead>\n".
                 "  <tbody>\n";
                 
      while($row = mysqli_fetch_assoc($result)){
        $salida .=  "    <tr>\n";
        $salida .=  "      <td>".$row['tipo_orden']."</td>\n".
                    "      <td>".$row['descripcion']."</td>\n".
                    "      <td>".$row['estatus']."</td>\n".
                    "      <td>".$row['fecha_alta']."</td>\n".
                    "      <td><button type=\"button\" id=\"btn_workOrdTask_".$row['orden_id']."\" title=\"Nueva Tarea\" class=\"btn btn-default\" onClick=\"getWorkOrderDetails('".encrypt($row['orden_id'])."',0);\"><span class=\"glyphicon glyphicon-th-list\"></span></button>";
        $salida .=  "    </td></tr>\n";
      }
      $salida .= "  </tbody>\n".
                 "</table>\n";
    } 
  }
  mysqli_close($db_modulos);
  echo $salida;
}

function get_worOrdFactName($worOrFacId){
  switch($worOrFacId){
    case 0:
      $worOrdStatName = "Por Confirmar";
    break;
    case 1:
      $worOrdStatName = "Sí";
    break;
    case 2:
      $worOrdStatName = "No";
    break;
    case 3:
      $worOrdStatName = "Sí, con adecuaciones";
    break;  
  }
  return $worOrdStatName;
}

if("getWorkOrderDetails" == $_POST['action']){
  $db_modulos   = condb_modulos();
  
  $worOrdId = isset($_POST['worOrdId']) ? decrypt($_POST['worOrdId']) : -1;
  $option   = isset($_POST['option']) ? $_POST['option'] : -1;
  $salida = "";
  
  
  $query = "SELECT wOrd.*, catOrdTraSta.estatus AS ordTraStat, catOrdTraSta.estatus_id AS ordTraStatId ".
           "FROM ".$cfgTableNameMod.".ordenes_trabajo AS wOrd, ".
                   $cfgTableNameCat.".cat_orden_trabajo_estatus AS catOrdTraSta ".
           "WHERE wOrd.orden_id = ".$worOrdId." ".
           "AND wOrd.estatus=catOrdTraSta.estatus_id";
            
  $query2 = "SELECT tar.*, catOrdTraSta.* ".
            "FROM ".$cfgTableNameMod.".orden_trabajo_tareas AS tar, ".
                    $cfgTableNameCat.".cat_orden_trabajo_estatus AS catOrdTraSta ".
            "WHERE tar.orden_trabajo_id = ".$worOrdId." ".
            "AND tar.estatus_id=catOrdTraSta.estatus_id";
               
               
  $queryAdec = "SELECT rcp.relacion_id, rcp.cotizacion_id, rcp.cantidad, cvs.servicio, cvs.descripcion, cvs.servicio_id, ".
               "cvts.categoria_id, rcp.orden_trabajo_id ".
               "FROM ".$cfgTableNameMod.".rel_cotizacion_producto AS rcp, ".
                       $cfgTableNameMod.".ordenes_trabajo AS ot, ".
                       $cfgTableNameCat.".cat_venta_servicios AS cvs, ".
                       $cfgTableNameCat.".cat_venta_tipo_servicios AS cvts ".
               "WHERE rcp.producto_id=cvs.servicio_id ".
               "AND cvs.tipo_servicio=cvts.tipo_servicio_id ".
               "AND ot.orden_id = ".$worOrdId." ".
               "AND rcp.orden_trabajo_id = ot.orden_id ".
               "AND cvts.categoria_id = 5";
  $result = mysqli_query($db_modulos,$query);
  $result2 = mysqli_query($db_modulos,$query2);
  
  if(!$result||!$result2){
    log_write("ERROR: SAVE-WORK-ORDER-NEW: Ocurrió un problema al guardar los datos de la orden ".$query,4);
    $salida = "ERROR||Ocurrió un problema al obtener los datos de la orden";
  }  
  else{
    if(0==$option){
      $row = mysqli_fetch_assoc($result);
      $relCotPro = $row['rel_cotizacion_producto'];  
      $factible = $row['factible'];
      $worOrdStatName = get_worOrdFactName($factible);  
      $ordId = $row['orden_id']; 
      if(hasPermission(9,'e') || ((hasPermission(9,'n')&&$row['usuario_alta']==$usuarioId))){
        $salida .= "<div><button type=\"button\" id=\"btn_workOrdEdit\" title=\"Editar orden\" class=\"btn btn-success\" onClick=\"getWorkOrderDetails('".$_POST['worOrdId']."',1);\"><span class=\"glyphicon glyphicon-edit\"></span>Editar</button><br><br></div>";
      }
      $salida .= "<div class=\"table-responsive\"><table id=\"tbl_workOrderDetails\" class=\"table table-striped\">\n".
                  "  <tbody>\n".      
                  "    <tr>".
                  "      <td colspan=\"2\"><b>ID:</b></td>".
                  "      <td colspan=\"3\">".$ordId."</td>".
                  "    </tr>".
                  "    <tr>".
                  "      <td colspan=\"2\"><b>Nombre:</b></td>".
                  "      <td colspan=\"3\">".$row['titulo']."</td>".
                  "    </tr>". 
                  "    <tr>".
                  "      <td colspan=\"2\"><b>Descripción:</b></td>".
                  "      <td colspan=\"3\">".$row['descripcion']."</td>".
                  "    </tr>".
                  "    <tr>".
                  "      <td colspan=\"2\"><b>Estatus:</b></td>".
                  "      <td colspan=\"3\">".$row['ordTraStat']."</td>".
                  "    </tr>".
                  "    <tr>".
                  "      <td colspan=\"2\"><b>Factible</b></td>".
                  "      <td colspan=\"3\">".$worOrdStatName."</td>".
                  "    </tr><br>".
                  "</tbody></table></div>"; 
      
      if(3==$factible){
        $resultAdec = mysqli_query($db_modulos,$queryAdec);
        if(!$resultAdec){
          $salida .= "<b>Ocurrió un error a obtener los datos de adecuaciones</b><br>";
        }
        else{
          //$salida .= "<h3>Adecuaciones</h3><br>";
          if(0>=mysqli_num_rows($resultAdec)){
            $salida .= "<b><i>No cuenta con adecuaciones</i></b><br>";
          }
          else{
            $salida .= "<div class=\"table-responsive\"><table id=\"tbl_workOrderDetails\" class=\"table table-striped\">\n".
                       "  <thead>\n".
                       "    <tr>\n".
                       "      <th>cantidad</th>\n".
                       "      <th>Servicio</th>\n".
                       "      <th>Descripción</th>\n".
                       "    </tr>\n".
                       "  </thead>\n";
          
            while($rowAdec=mysqli_fetch_assoc($resultAdec)){
              $salida .= "  <tbody>\n".
                         "    <tr>\n".
                         "      <td>".$rowAdec['cantidad']."</td>".
                         "      <td>".$rowAdec['servicio']."</td>".
                         "      <td>".$rowAdec['descripcion']."</td>".
                         "    <tr>\n".
                         "  </tbody>"; 
            }
            $salida .= "</table></div>";
          }
        }
      }
      
      $salida .= "<b>Tareas Programadas</b><br><br>";
      if(hasPermission(9,'w')||((hasPermission(9,'m')&&$row['usuario_alta']==$usuarioId))){      
        $salida .= "<div><button type=\"button\" id=\"btn_workOrdTaskNew\" title=\"Nueva Tarea\" class=\"btn btn-default\" onClick=\"openWorkOrderTaskNew();\"><span class=\"glyphicon glyphicon-asterisk\"></span> Nueva Tarea</button><br><br></div>";
      }
      if(0>=mysqli_num_rows($result2)){
        $salida .= "<div>No existen tareas para esta orden de trabajo. haga click en <b>Nueva Tarea</b> para agregar una.</div>";     
      }
      else{
        $salida .= "<div class=\"table-responsive\"><table id=\"tbl_tasks_".$row['tarea_id']."\" class=\"table table-striped table-condensed\">\n".
                   "  <thead>\n".
                   "    <tr>\n".
                   "      <th>Título</th>\n".
                   "      <th class=\"hidden-xs\">Por:</th>\n".
                   "      <th>Programado para:</th>\n".
                   "      <th>Estatus</th>\n".
                   "      <th>Descripción</th>\n".
                   "      <th>Acciones</th>\n".
                   "    </tr>\n".
                   "  </thead>\n".
                   "  <tbody>\n";                
        while($row2 = mysqli_fetch_assoc($result2)){
          $nombreAlta = get_userRealName($row2['usuario_id']);
          $salida .=  "    <tr>\n".
                      "      <td>".$row2['titulo']."</td>\n".
                      "      <td class=\"hidden-xs\"><a href=/crm/modulos/perfil/index.php?id='".encrypt($row2['usuario_id'])."'>".$nombreAlta."</a></td>\n";
                      
          if('0000-00-00 00:00:00'==$row2["fecha_inicio"]){
            $salida .=  "      <td><i>Sin agendar</i></td>\n";
          }
          else{
            $salida .=  "      <td>".$row2["fecha_inicio"]."</td>\n";
          }
          $salida .= "      <td>".$row2['estatus']."</td>\n".
                      "      <td title=\"".$row2["descripcion"]."\" onClick=\"alert('".$row2["descripcion"]."')\">".
                                 "<a href=\"#\">".substr($row2["descripcion"],0,30)."...</a></td>\n";
          
          $salida .= "      <td>";
            
          if('0000-00-00 00:00:00'!=$row2["fecha_inicio"]){  
            $salida .= "      <button type=\"button\" title=\"Ir a Calendario\" class=\"btn btn-xs btn-default btn-responsive\" onclick=\"goToCalendarDate('".$row["fecha_inicio"]."');\"><span class=\"glyphicon glyphicon-calendar\"></span></button>";
          }  
          if(hasPermission(9,'r')||(hasPermission(9,'l')&&$row['usuario_alta']==$usuarioId)){
            $salida .= "      <button type=\"button\" id=\"btn_worOrdTaskComments_".$row2['tarea_id']."\" title=\"Ver Comentarios\" class=\"btn btn-xs btn-info btn-responsive\" onClick=\"getComments('".encrypt($row2['tarea_id'])."',12);\"><span class=\"glyphicon glyphicon-comment\"></span></button>";
          }          
            
          if(hasPermission(9,'e')||((hasPermission(9,'m')&&$row['usuario_alta']==$usuarioId))){ 
            $salida .= "  <button type=\"button\" id=\"btn_worOrdTaskEdit_".$row2['tarea_id']."\" title=\"Editar Evento\" ".
                       "class=\"btn btn-xs btn-success btn-responsive\" onClick=\"editWorkOrderTask('".encrypt($row2['tarea_id'])."');\">".
                       "<span class=\"glyphicon glyphicon-edit\"></span></button>";
          }
          
          $salida .= "      </td>\n".
                     "    </tr>\n";  
        }
        $salida .= "  </tbody>\n".
                   "</table>\n</div>";          
      }
      $salida .=  "<input type=\"hidden\" id=\"hdn_workOrdId\" name=\"hdn_workOrdId\" class=\"form-control\" value=".encrypt($worOrdId).">\n ";    
      $salida .=  "<input type=\"hidden\" id=\"hdn_relCotPro\" name=\"hdn_relCotPro\" class=\"form-control\" value=".encrypt($relCotPro).">\n ";    
    }
    elseif(1==$option){
      while($row = mysqli_fetch_assoc($result)){  
        $factible = $row['factible'];
        $salida = "OK||".$row['tipo_orden_id']."||".$row['titulo']."||".$row['descripcion']."||".$row['ordTraStatId']."||".$row['factible'];
      }
      $resultAdec = mysqli_query($db_modulos,$queryAdec);
      if(!$resultAdec){
        $salida = "ERROR||Ocurrió un error al obtenerse los datos de las adecuaciones ".$queryAdec;
      }
      else{
        if(0>=mysqli_num_rows($resultAdec)){
          $adecArr = "";
        }
        else{
          $adecArr = array();
          while($rowAdec = mysqli_fetch_assoc($resultAdec)){
            $adecArr[] = array($rowAdec['relacion_id'],$rowAdec['servicio_id'],$rowAdec['cantidad'],$rowAdec['servicio']);
          }
        } 
        $salida .= "||".json_encode($adecArr);    
      }     
    }
  }
  mysqli_close($db_modulos);
  echo $salida;
} 
//*******************************************************************
//*******************************************************************
//*******************************************************************
//*******************************************************************
if("saveWorkOrderNew" == $_POST['action']){
  $db_modulos   = condb_modulos();
  $db_catalogos = condb_catalogos();

  $option   = isset($_POST['option']) ? $_POST['option'] : -1;                        // opcion
  $worOrdId = isset($_POST['worOrdId']) ? decrypt($_POST['worOrdId']) : -1;           // id de la orden de trabajo
  $worOrdType = isset($_POST['worOrdType']) ? decrypt($_POST['worOrdType']) : -1;     // tipo de orden de trabajo
  $titulo = isset($_POST['titulo']) ? $_POST['titulo'] : -1;                          // titulo
  $relCotPro = isset($_POST['relCotPro']) ? decrypt($_POST['relCotPro']) : -1;        // relacion cotizacion producto
  $pricId = isset($_POST['worOrdPricId']) ? decrypt($_POST['worOrdPricId']) : -1;     // id del precio de la orden de trabajo
  $status  = isset($_POST['worOrdStat']) ? $_POST['worOrdStat'] : 1;                  // estatus de la orden de trabajo
  $factible = isset($_POST['factible']) ? $_POST['factible'] : 1;                     // factible
  $desc = isset($_POST['desc']) ? $_POST['desc'] : -1;                                // descuento 
  $adecArr = isset($_POST['adecArray']) ? json_decode($_POST['adecArray'],true) : ""; // adecuaciones
  $adecArray2 = $adecArr;
  $vigencia = 48;
  $newCotId = 0;
  $noSiteCon = 0;
  $pricNotAuth = 0;
  $substraccion = 0;
  $hoy = date('Y-m-d H:i:s');
  if(5==$worOrdType&&0==$option){
    $queryFilter = " WHERE cot.cotizacion_id=".$pricId." ";
    $queryFilter2 = "";
  }
  else{
    $queryFilter = ", (SELECT cotizacion_id ".
                "FROM ".$cfgTableNameMod.".rel_cotizacion_producto WHERE relacion_id= ".$relCotPro." ".
                ") AS cotizacionId ".   
                "WHERE rcp.cotizacion_id=cotizacionId.cotizacion_id ";
    $queryFilter2 = "AND cvts.categoria_id=1";
  }
  $queryCons =  "SELECT rcp.relacion_id, rcp.cotizacion_id, cot.origen_id, cot.tipo_origen, rcp.sitio_asignado, ".    
                "rcp.producto_precio_especial, rcp.producto_autorizado_por, cot.origen_id, cot.tipo_origen, cot.cotizacion_estatus_id, ".
                "cvts.categoria_id, cvs.servicio, cvs.descripcion ".
                "FROM ".$cfgTableNameMod.".rel_cotizacion_producto AS rcp, ".
                        $cfgTableNameCat.".cat_venta_servicios AS cvs, ".
                        $cfgTableNameCat.".cat_orden_trabajo_categoria_grupo AS cotg, ".
                        $cfgTableNameCat.".cat_venta_tipo_servicios AS cvts, ".
                        $cfgTableNameMod.".cotizaciones AS cot ".
                $queryFilter.       
                "AND cvs.servicio_id=rcp.producto_id ".
                "AND rcp.cotizacion_id=cot.cotizacion_id ".
                "AND cvs.tipo_servicio=cvts.tipo_servicio_id ".
                "AND cvts.categoria_id = cotg.categoria_id ".
                $queryFilter2;
  log_write("DEBUG: SAVE-WORK-ORDER: Consulta de orden ".$queryCons,4);
  $resultCons = mysqli_query($db_modulos,$queryCons);
  if(!$resultCons){
    log_write("ERROR: SAVE-WORK-ORDER-NEW: Ocurrió un problema al consultar los datos generales de la orden",4);
    $salida = "ERROR||Ocurrió un problema al consultar los datos generales de la orden ";
  }
  else{
    $totalCon = mysqli_num_rows($resultCons);
    if(0>=$totalCon){
      log_write("ERROR: SAVE-WORK-ORDER-NEW: No se encontraron resultados al consultar los datos generales de la orden",4);
      $salida = "ERROR||No se encontraron resultados al consultar los datos de la orden";
    }
    else{
      $sitArr = array();
      $serArr = array();
      while($rowCons = mysqli_fetch_assoc($resultCons)){
        $newCotId = $rowCons['cotizacion_id'];
        $orgId = $rowCons['origen_id'];
        $orgType = $rowCons['tipo_origen'];
        $cotStatId = $rowCons['cotizacion_estatus_id'];
        if(0>=$rowCons['sitio_asignado'])
          $noSiteCon++;
        if(-1!=$rowCons['producto_precio_especial']&&0>=$rowCons['producto_autorizado_por'])
          $pricNotAuth++;
        $serArr[] = array($rowCons['relacion_id'],$rowCons['categoria_id'],$rowCons['sitio_asignado'],$rowCons['servicio'],$rowCons['descripcion']); 
      }
      switch($orgType){
        case 0:
          $name = "prospecto";
          $tableName = "prospectos";
          $fieldId = "prospecto_id";
        break;
        case 1:
          $name = "cliente";
          $tableName = "clientes";
          $fieldId = "cliente_id";
        break;
        default:
          $tableName = "";
        break; 
      }
      $queryCli = "SELECT cli.razon_social, cli.nombre_comercial, cli.asignadoa, cli.admin_auth, ".
                          "sit.nombre, sit.domicilio, sit.colonia, sit.cp, sit.municipio, cedo.estado ".
                          "FROM ".$cfgTableNameMod.".".$tableName." AS cli, ".
                                  $cfgTableNameMod.".sitios AS sit, ".
                                  $cfgTableNameMod.".rel_cotizacion_producto AS rcp, ".
                                  $cfgTableNameMod.".cotizaciones AS cot, ".
                                  $cfgTableNameCat.".cat_estados AS cedo ".
                          "WHERE rcp.relacion_id = ".$relCotPro.
                          " AND rcp.cotizacion_id = cot.cotizacion_id".
                          " AND cot.tipo_origen = ".$orgType.
                          " AND cli.".$fieldId." = cot.origen_id ".
                          " AND sit.sitio_id = rcp.sitio_asignado".
                          " AND cedo.estado_id = sit.estado";
      $resultCli = mysqli_query($db_modulos,$queryCli);
      log_write("DEBUG: SAVE-WORK-ORDER: Consulta de cliente ".$queryCli,4);
      $rowCli = mysqli_fetch_assoc($resultCli);
      $estatusPros = getProspectSellStatus($orgId);
      if(0==$option){
        $query = "INSERT INTO ".$cfgTableNameMod.
                 ".ordenes_trabajo(tipo_orden_id,rel_cotizacion_producto,titulo,descripcion,estatus,".
                 "vigencia_horas,fecha_alta,usuario_alta) VALUES ";
        if(5!=$worOrdType)
          $query .= "(".$worOrdType.",".$relCotPro.",'".$titulo."','".$desc."',".$status.",".$vigencia.",'".$hoy."',".$usuarioId.")";
        else{
          foreach($serArr AS $serArrKey => $serArrValue){
            $query .= " (".$worOrdType.",".$serArrValue[0].",'".$titulo."','".$desc."(".$serArrValue[4].")',".$status.",".$vigencia.",'".$hoy."',".$usuarioId."),";
          }
          $query = substr($query,0,-1);
        }
      }
      elseif(1==$option){
        // enviar mensaje a abisai cuando cambia de estatus la orden de trabajo y al encargado de ventas
        $querySelect = "SELECT ort.estatus, ort.titulo, ort.usuario_alta FROM ".$cfgTableNameMod.".ordenes_trabajo as ort WHERE orden_id = ".$worOrdId;
        $resultSelect = mysqli_query($db_modulos,$querySelect);
        $resultSelect = mysqli_fetch_assoc($resultSelect);
        $tituloAnt = $resultSelect['titulo'];
        $usuarioIdAltaOrden = $resultSelect['usuario_alta'];
        $resultSelect = $resultSelect['estatus'];
        if( $_SESSION['usrId'] != 6){
          // si no es abisai no puede agregar adecuaciones a una ot
          if($status == 3 && $adecArr != null){
                $statusUpd = 5;
          }else{
            $statusUpd =$status;
          }
        }else{
            $statusUpd =$status;
          }
        // query que actualiza la orden de trabajo - tipo de orden, titulo, descripcion, estatus y factible.
        $query = "UPDATE ".$cfgTableNameMod.".ordenes_trabajo SET tipo_orden_id = ".$worOrdType.", titulo='".$titulo."', descripcion = '".
                 $desc."', estatus = ".$statusUpd.", factible=".$factible." WHERE orden_id = ".$worOrdId;
        if($resultSelect != $status){
          $mailData = array('dest' => "",
                      'cc' => "",
                      'cco' => "",
                      'asunto' => "",
                      'msg' => "",
                      'adjunto1' => "",
                      'adjunto2' => "",
                      'adjunto3' => "",
                      'adjunto4' => "",
                      'adjunto5' => "",
                      'adjunto6' => "",
                      'adjunto7' => "",
                      'adjunto8' => "",
                      'adjunto9' => "",
                      'adjunto10' => "",
                      'adjunto11' => "",
                      'adjunto12' => "",
                      'adjunto13' => "",
                      'adjunto14' => "",
                      'adjunto15' => "",
                      'estatus' => "NO ENVIADO",
                      'fecha_envio' => "0000-00-00 00:00:00",
                      'nIDCorreo' => 1,
                      'folio' => "0",
                      'firma' => "",
                      'observaciones' => "",
                      'bestado' => ""
                       );
          // $mailData['dest'] = "apimentel@coeficiente.mx"; // correo de cambio de estado de orden de trabajo a abisai y a mi
          $mailData['dest'] = "lgarcia@coeficiente.mx"; //
          $mailData['asunto'] = "Cambio de estado en Orden de Trabajo - CRM ";
          $nombreModificacion = get_userRealName($usuarioId);
          $correoUserAltaOrden = get_userEmail($usuarioIdAltaOrden);
          if($usuarioId == 5){
            $correoUserModOrden = "";
          }else{
            $correoUserModOrden = ",".get_userEmail($usuarioId);
          }
          $mailData['dest'] = $mailData['dest'].",".$correoUserAltaOrden.$correoUserModOrden;
          $estatosOrden = array (
            1 => 'Iniciado',
            2 => 'En progreso',
            3 => 'Terminado',
            4 => 'Cancelado',
            5 => 'Por Autorizar'
          );
          $mailData['msg'] = "Hola, el usuario ".$nombreModificacion." ha cambiado el estatus de la orden del trabajo con Numero: ".$worOrdId." y titulo ".$tituloAnt.", de \"".$estatosOrden[$resultSelect]."\" a \"".$estatosOrden[$statusUpd]."\".<br><br>Puedes revisar la informacion entrando al sistema CRM en el apartado Ordenes de Trabajo. <br><br>Acceso al sistema CRM <a href=\"https://crm.coeficiente.mx/crm/modulos/login/index.php\">Aquí</a>";
          $mailData['remitente'] = "";
          sendBDEmail($mailData);
          // **************************************************+
          // **************************************************+
          // cambio de estatus de orden de trabajo
          // mail de cambio de estatus
          // **************************************************+
          // **************************************************+
          // **************************************************+
          // **************************************************+
          // inicio de mensaje de notificacion de instalacion completada
          // notificar a gaby y vendedor para iniciar proceso de cobranza
          // PUNTO B
          // **************************************************+
          // **************************************************+
          if($worOrdType == 3 && $status == 3){
            global $mailData;
            $query12 = "SELECT pros.* FROM coecrm_modulos.prospectos AS pros,coecrm_modulos.cotizaciones AS cot, coecrm_modulos.rel_cotizacion_producto AS rcot,coecrm_modulos.ordenes_trabajo AS ot ".
            "WHERE pros.prospecto_id = cot.origen_id ".
            "AND cot.cotizacion_id = rcot.cotizacion_id ".
            "AND rcot.relacion_id = ot.rel_cotizacion_producto ".
            "AND ot.orden_id =". $worOrdId;
            $resultPros = mysqli_query($db_modulos,$query12);
            if($resultPros){
              $rowPros = mysqli_fetch_assoc($resultPros);
              $mailData['dest'] = "agarcia@softernium.com,cobranza@coeficiente.mx";
              $mailData['asunto'] = "Instalacion completada.";
              $mailData['msg'] = "Hola Gabriela, se ha finalizado el proceso de instalacion para el prospecto: <br> Clave: ".
              $rowPros['prospecto_id']."<br> Nombre Comercial: ".
              $rowPros['nombre_comercial']." <br>El vendedor se pondra en contacto con tigo sobre la satisfaccion del cliente por el servicio que se le instaló para que puedas proseguir con el proceso de cobro. <br><br>Acceso al sistema CRM <a href=\"https://crm.coeficiente.mx/crm/modulos/login/index.php\">Aquí</a>";
              $mailData['remitente'] = "";
              sendBDEmail($mailData);
              // -------------------------- //
              $mailData['dest'] = "agarcia@softernium.com,". get_userEmail($rowPros['asignadoa']);
              $mailData['asunto'] = "Instalacion completada.";
              $mailData['msg'] = "Hola ". get_userRealName($rowPros['asignadoa']).", se ha finalizado la instalacion al prospecto ".
              $rowPros['nombre_comercial']. ",<br> Verifica la satisfaccion del prospecto en base al servicio que se le instaló y notifica a Gabriela de cobranza (cobranza@coeficiente.mx), para que pueda proceder con el cobro.<br><br>Acceso al sistema CRM <a href=\"https://crm.coeficiente.mx/crm/modulos/login/index.php\">Aquí</a>";
              sendBDEmail($mailData);
            }
          } // fin de condicional para enviar la notificacion de instalacion completada
        }
      }
      $result = mysqli_query($db_modulos,$query);
      if(!$result){
        log_write("ERROR: SAVE-WORK-ORDER-NEW: Ocurrió un problema al guardar los datos de la orden ".$query,4);
        $salida = "ERROR||Ocurrió un problema al guardar los datos de la orden ".$query;
      }
      else{
        if(0==$option){
          $mailData = array('dest' => "",
                      'cc' => "",
                      'cco' => "",
                      'asunto' => "",
                      'msg' => "",
                      'adjunto1' => "",
                      'adjunto2' => "",
                      'adjunto3' => "",
                      'adjunto4' => "",
                      'adjunto5' => "",
                      'adjunto6' => "",
                      'adjunto7' => "",
                      'adjunto8' => "",
                      'adjunto9' => "",
                      'adjunto10' => "",
                      'adjunto11' => "",
                      'adjunto12' => "",
                      'adjunto13' => "",
                      'adjunto14' => "",
                      'adjunto15' => "",
                      'estatus' => "NO ENVIADO",
                      'fecha_envio' => "0000-00-00 00:00:00",
                      'nIDCorreo' => 1,
                      'folio' => "0",
                      'firma' => "",
                      'observaciones' => "",
                      'bestado' => ""
                       );
          // ---------------------
          $worOrdId = mysqli_insert_id($db_modulos);
          $nombreAlta = get_userRealName($usuarioId);  
          $salida = "OK||Se han guardado los datos de la orden"; 
          $asunto = "Alta de Orden de Trabajo Nueva";
          $msg = "Hola.<br>".
            $nombreAlta." Ha agregado una orden de trabajo nueva:<br>".
            "<b>Titulo:</b> ".$titulo."<br>".
            "<b>Nombre Comercial del ".$name.":</b> ".$rowCli['nombre_comercial']."<br>".
            "<b>Nombre del Sitio:</b> ".$rowCli['nombre']."<br>".
            "<b>Dirección:</b> ".$rowCli['domicilio']." Col. ".$rowCli['colonia']." C.P. ".$rowCli['cp']."<br>".
            $rowCli['municipio'].", ".$rowCli['sitEdo']."<br>".
            "<b></b>Descripción:</b> ".$desc."<br>".
            "Por favor verifica en tu módulo de ordenes de trabajo para la asignación de tareas<br><br>".
            "Sistema CRM Coeficiente.";
            $mailData['dest'] = "lgarcia@coeficiente.mx";
            $mailData['asunto'] = "Alta de Orden de trabajo";
            // $mailData['msg'] = $msg;
            $mailData['msg'] = $msg . "<br><br>Acceso al sistema CRM <a href=\"https://crm.coeficiente.mx/crm/modulos/login/index.php\">Aquí</a>";
            $mailData['remitente'] = "";
          if(5==$worOrdType){
            //mensaje para abisai de creacion de orden de trabajo por dimensionamiento
            //aqui va el email para abisai
            $mailData['asunto'] = "Alta de Orden de trabajo - Dimensionamiento";
            sendBDEmail($mailData);
            $queryGrupoOrden = "SELECT grupo_id FROM cat_orden_trabajo_categoria_grupo WHERE"; //abisai
            foreach($serArr AS $serArrKey => $serArrValue){
              $queryGrupoOrden .= " categoría_id = ".$serArrValue[1]." OR";
            }
            $query = substr($queryGrupoOrden, 0, -3);
            $resultGrupoOrden = mysqli_query($db_modulos,$queryGrupoOrden);
            if(!$resultGrupoOrden){
              log_write("ERROR: SAVE-WORK-ORDER-NEW: Ocurrió un problema al obetenerse los grupos a notificar para la orden de dimensionamiento",4);
              $salida = "ERROR:Ocurrió un problema al obetenerse los grupos a notificar para la orden de dimensionamiento";
            }
            else{
              while($rowGrupoOrden = mysqli_fetch_assoc($resultGrupoOrden)){
                $ordensec = explode(",",$rowGrupoOrden['grupo_id']);
                foreach($ordenSec AS $ordenSecKey => $ordenSecValue){
                  sendPrivMsgDepartment($usuarioId,$ordenSecValue,0,$asunto,$msg,0,1);
                }
              }
            }
          }
          else{
            if(1==$worOrdType){
              // mensaje a abisai de estudio de factibilidad 
               $mailData['asunto'] = "Alta de Orden de trabajo - Estudio de Factibilidad";
               sendBDEmail($mailData);
            }
            if(3==$worOrdType){
              // mensaje para abisai de instalacion
               $mailData['asunto'] = "Alta de Orden de trabajo - Instalación"; 
               sendBDEmail($mailData);
            }
            // mensaje a Hector
            sendPrivMsgDepartment($usuarioId,7,0,$asunto,$msg,0,1);
          }// fin del else
          //comentado por tania
          //sendPrivMsgDepartment($usuarioId,1,0,$asunto,$msg,0,1); //pruebas. Quitar
        }
        elseif(1==$option){
          // elseif de actualizacion de cotizacion - segundo
          foreach($adecArr as $key => $value){
            $adecArray[$value[0]."a"] = array($value[0],$value[1],$value[2],-1,0,0); 
          }
          
          $currAdecQuery = "SELECT rcp.relacion_id, rcp.cotizacion_id, rcp.cantidad, cvs.servicio, rcp.producto_precio_especial, cvs.precio_lista, cvs.servicio_id, cvts.categoria_id, rcp.orden_trabajo_id ".
                           "FROM ".$cfgTableNameMod.".rel_cotizacion_producto AS rcp, ".
                                   $cfgTableNameMod.".ordenes_trabajo AS ot, ".
                                   $cfgTableNameCat.".cat_venta_servicios AS cvs, ".
                                   $cfgTableNameCat.".cat_venta_tipo_servicios AS cvts ".
                           "WHERE rcp.producto_id=cvs.servicio_id ".
                           "AND cvs.tipo_servicio=cvts.tipo_servicio_id ".
                           "AND ot.orden_id = ".$worOrdId." ".
                           "AND rcp.orden_trabajo_id = ot.orden_id ".
                           "AND cvts.categoria_id = 5"; 
                            // selecciona las adecuaciones relacionadas a esa orden de trabajo si es que tiene.
          $currAdecArr = array();
          $currAdecResult = mysqli_query($db_catalogos,$currAdecQuery);
          if(!$currAdecResult){
            log_write("ERROR: SAVE-WORK-ORDER: No se pudo consultar las adecuaciones actuales ".$currAdecQuery,4);
            $salida = "ERROR|| Ocurrió un problema al consultar los datos de las posibles adecuaciones existentes para su actualización";
          }
          else{
            log_write("DEBUG: SAVE-WORK ORDER: Va a actualizar los datos de las adecuaciones",4);
            while($currAdecRow = mysqli_fetch_assoc($currAdecResult)){
              if(-1==$currAdecRow['producto_precio_especial'])
                $substraccion = $substraccion-$currAdecRow['precio_lista'];
              else
                $substraccion = $substraccion-$currAdecRow['producto_precio_especial'];
              $currAdecArr[$currAdecRow['relacion_id']."a"] = array($currAdecRow['relacion_id'],$currAdecRow['servicio_id'],$currAdecRow['cantidad'],-1,0,0);//estos ceros se agregan para que no truene el validador de precios especiales y así se sumen los subtotales en esa función
            }
            arraySort($currAdecArr,0);//el existente, que ya está guardado
            arraySort($adecArray,0); //el nuevo
            if($adecArray>$currAdecArr)
              $arrdif = array_diff_key($adecArray,$currAdecArr);
            else
              $arrdif = array_diff_key($currAdecArr,$adecArray);
            if(0<sizeof($arrdif)&&5==$cotStatId){
              $salida = "ERROR||El cliente ya aceptó la cotización con adecuaciones diferentes. Por favor contacta al gerente del área de ventas para su validación.";
            }
            else{
              $currAdecSize = sizeof($currAdecArr);
              $adecSize = sizeof($adecArray);
              log_write("DEBUG: SAVE-WORK-ORDER: Productos actuales"."<pre>".print_r($currAdecArr,true)."</pre>",4);
              log_write("DEBUG: SAVE-WORK-ORDER: Productos actualizados"."<pre>".print_r($adecArray,true)."</pre>",4);
              log_write("DEBUG: SAVE-WORK-ORDER: Productos actualizados sin desmenuzar"."<pre>".print_r($adecArr,true)."</pre>",4);  
              log_write("DEBUG: SAVE-WORK-ORDER: Productos diferencia key"."<pre>".print_r($arrdif,true)."</pre>",4);
              foreach($adecArray as $key => $value){                      
                if(isset($currAdecArr[$key])){
                  $newProdId = substr($key,0,-1);
                  $queryProdUpd = "UPDATE rel_cotizacion_producto SET cantidad = ".$value[2]." WHERE relacion_id = ".$newProdId;
                  log_write("DEBUG: SAVE-WORK-ORDER: updatequery".$queryProdUpd,4);
                  if(!$resultProdUpd = mysqli_query($db_modulos,$queryProdUpd)){
                    log_write("ERROR: SAVE-WORK-ORDER: error al actualizar la adecuacion con id [".$key."] ",4);
                  }   
                  else{
                    $prodValRes = explode("||",valSpecialPrice($newProdId,$value,$newCotId)); 
                    $statList .= $prodValRes[0].",";
                    $subtotal = $subtotal + $prodValRes[1];
                    log_write("DEBUG: SAVE-WORK-ORDER: se actualizó adecuacion con id [".$key."]",4);
                    unset($adecArray[$key]);
                    unset($currAdecArr[$key]);
                  }      
                }
                else{
                  log_write("DEBUG: SAVE-WORK-ORDER: no se encuentra la llave [".$key."]",4);
                }           
              }
              if(isset($currAdecArr)&&0<count($currAdecArr)){
                foreach($currAdecArr as $key => $value){
                  $queryAdecDel = "DELETE FROM rel_cotizacion_producto WHERE relacion_id = ".substr($key,0,-1);
                  if(!$resultAdecDel = mysqli_query($db_modulos,$queryAdecDel)){
                    log_write("ERROR: SAVE-WORK-ORDER-NEW: error al eliminar adecuacion con id [".$key."] ".$queryAdecDel,4);
                  }   
                  else{
                    log_write("DEBUG: SAVE-WORK-ORDER-NEW: se eliminó adecuacion con id [".$key."]",4);
                    unset($currAdecArr[$key]);
                  }      
                }
              }
            }   
          }  
        }// fin del elseif de actualizacion - segundo
        if(0==$option||(1==$option&&false!==$currAdecResult)){
          // si es una creacion de una orden Ò si es una actualizacion y tiene adecuaciones registradas
          // 
          if(0<sizeof($arrdif)&&5==$cotStatId){
            $salida = "ERROR||El cliente ya aceptó la cotización con adecuaciones diferentes. Por favor contacta al gerente del área de ventas para su validación";
          }
          else{
            if((isset($adecArray)&&0<count($adecArray))){
              log_write("DEBUG: SAVE-WORK-ORDER-NEW: adecuaciones ".print_r($adecArray,true),4);               
              log_write("DEBUG: SAVE-WORK-ORDER-NEW: hay adecuaciones nuevas se van a insertar",4);
              foreach($adecArray as $key => $value){
                //insercion de adecuaciones
                // si esta orden necesito adecuaciones se agregan aqui
              //tempArray = [relid,id,prodCant,prodEsPri,prodEsAut,prodVig]
                $prodQuery = "INSERT INTO rel_cotizacion_producto".   
                            "(cotizacion_id,producto_id,cantidad,vigencia,producto_precio_especial,producto_autorizado_por,orden_trabajo_id) VALUES ".
                            "(".$newCotId.",".$value[1].",".$value[2].",".$value[5].",".$value[3].",".$value[4].",".$worOrdId.")";
                $prodResult  = mysqli_query($db_modulos,$prodQuery);
                log_write("DEBUG: SAVE-WORK-ORDER-NEW: ".$prodQuery,4);
                if(!$prodResult){
                    log_write("ERROR: SAVE-WORK-ORDER-NEW: No se insertó la adecuación ".$prodQuery,4);
                    $salida = "ERROR|| Ocurrió un error al insertarse los datos de los productos";   
                }
                else{
                  log_write("ERROR: SAVE-WORK-ORDER-NEW: Se insertó adecuación, se validará el precio y se calculará nueva cotización ",4);
                  $newProdId = mysqli_insert_id($db_modulos);
                  $prodValRes = explode("||",valSpecialPrice($newProdId,$value,$newCotId));
                  $statList .= $prodValRes[0].",";
                  $subtotal = $subtotal + $prodValRes[1];
                }
              }
            }
            else{
              log_write("ERROR: SAVE-WORK-ORDER-NEW: no hay productos en el arreglo",4);   
            }
            $impuesto = $subtotal*0.16;
            $total = $impuesto+$subtotal;
            $queryCotStatUpd = "UPDATE cotizaciones SET".
                               " subtotal=".$subtotal.", impuesto= ".$impuesto.", total=".$total.
                               " WHERE cotizacion_id = ".$newCotId;
            if(!$resultCotStatUpd = mysqli_query($db_modulos,$queryCotStatUpd)){
              log_write("ERROR: SAVE-WORK-ORDER-NEW: No se actualizó el estatus de la cotizacion para autorizar ".$queryCotStatUpd,4);
            }
            else{
              log_write("OK: SAVE-WORK-ORDER-NEW: Se actualizó el estatus de la cotizacion para autorizar ".$queryCotStatUpd,4);
            }
            $salida = "OK||Se han guardado los datos de la orden de trabajo";
            writeOnJournal($usuarioId,"Ha guardado la orden de trabajo con id [".$worOrdId."] con descripcion [".$desc."] y tipo [".$worOrdType."]"); 
          } // fin del else de actualizacion de adecuaciones a la cotizacion y el precio de la cotizacion total
        }
        log_write("DEBUG: SAVE-WORK-ORDER: numero de enlaces sin sitio: ".$noSiteCon,4);
        log_write("DEBUG: SAVE-WORK-ORDER: numero de precios sin autorización: ".$pricNotAuth,4);
        log_write("DEBUG: SAVE-WORK-ORDER: estatus de prospecto: [".$estatusPros."] Tipo de orden de trabajo [".$worOrdType."]",4);
        if(0>=$noSiteCon||0>=$pricNotAuth){
          if(2==$estatusPros){
            updateOriginStatus($orgType,5,$orgId); 
            // se cambia a estudio
            //orgId es el id del prospecto
              if(0 == $orgType){
                // estatus al que cambia, y id del prospecto
                sendEstatusChangeEmail(5, $orgId);
              }
              //fin del if
            // cambio de estatus a estudio
          }
          elseif(5==$estatusPros&&3==$status){
            // aqui se actualiza el estatus del prospecto de estudio a propuesta
            // si esta en estudio y si el estatus de la orden es Terminado = 3
            if($_SESSION['usrId'] == 6 || $adecArr == null){
              updateOriginStatus($orgType,3,$orgId);
              // cambio de estatus de estudio a propuesta del prospecto relaionado a esta orden de trabajo
              //orgId es el id del prospecto
              if(0 == $orgType){
                // estatus al que cambia, y id del prospecto
                sendEstatusChangeEmail(3, $orgId);
              }//fin del if
            }else{
              // correo a abisai sobre la autorizacion de adecuaciones para 
              // ***************************************************
              $queryOT = "SELECT * FROM coecrm_modulos.ordenes_trabajo where orden_id = ".$worOrdId;
              $resultOT  = mysqli_query($db_modulos, $queryOT);
              // **************************************************+
              $mailData = array(
                'dest' => "",
                'cc' => "",
                'cco' => "",
                'asunto' => "",
                'msg' => "",
                'adjunto1' => "",
                'adjunto2' => "",
                'adjunto3' => "",
                'adjunto4' => "",
                'adjunto5' => "",
                'adjunto6' => "",
                'adjunto7' => "",
                'adjunto8' => "",
                'adjunto9' => "",
                'adjunto10' => "",
                'adjunto11' => "",
                'adjunto12' => "",
                'adjunto13' => "",
                'adjunto14' => "",
                'adjunto15' => "",
                'estatus' => "NO ENVIADO",
                'fecha_envio' => "0000-00-00 00:00:00",
                'nIDCorreo' => 1,
                'folio' => "0",
                'firma' => "",
                'observaciones' => "",
                'bestado' => ""
              );
              if($resultOT != false){
                $rowOT = mysqli_fetch_assoc($resultOT);
                $mailData['dest'] = "apimentel@coeficiente.mx"; // descomentar para subir a produccion
                $mailData['asunto'] = "Asignacion de adecuaciones OT - CRM ";
                $mailData['msg'] = "Hola, <br>El usuario, ".$_SESSION['usrNombre']." á asignado adecuaciones para la siguiente Orden de Trabajo: ".
                  "<br><b>Titulo</b>: ". $rowOT['titulo'].
                  "<br><b>Descripcion</b>: ".$rowOT['descripcion'].
                  "<br><b>Alta por</b>: ". get_userRealName($rowOT['usuario_alta']).
                  "<br><b>Estado Actual</b>: Por Autorizar".
                  "<br><b>Observacion</b>: La OT ha pasado a unestado pendiente de autorizacion, para autorizar la OT debes cambiar el estado de la misma a terminado y asignar las adecuaciones que consideres o dejar las que el tecnico ha asignado a la OT.".
                  "<br>".
                  "<br><br>Revisa el sistema CRM en el apartado Ordenes de Trabajo. <br><br>Acceso al sistema CRM <a href=\"https://crm.coeficiente.mx/crm/modulos/login/index.php\">Aquí</a>";
              }else{
                $mailData['dest'] = "agarcia@softernium.com";
                $mailData['asunto'] = "CRM - OT Fallo Consulta Datos";
                $mailData['msg'] = "Hola, no se pudo consultar la informacion de la orden de trabajo con ID: ".$worOrdId.".<br><br>Acceso al sistema CRM <a href=\"https://crm.coeficiente.mx/crm/modulos/login/index.php\">Aquí</a>";
              }
              $mailData['remitente'] = "";
              sendBDEmail($mailData);
              // **************************************************
              // **************************************************
              // fin del correo de abiso a abisai
            }
          } // fin del elseif si 5==$estatusPros&&3==$status, si el estado del prospecto no esta estudio y estatdo de la orden en 3 osea terminado
          elseif (3 == $status) {
            if ($_SESSION['usrId'] == 6 || $adecArr == null) {
            } else {
              // correo a abisai sobre la autorizacion de adecuaciones para 
              // ***************************************************
              $queryOT = "SELECT * FROM coecrm_modulos.ordenes_trabajo where orden_id = " . $worOrdId;
              $resultOT  = mysqli_query($db_modulos, $queryOT);
              // **************************************************+
              $mailData = array(
                'dest' => "",
                'cc' => "",
                'cco' => "",
                'asunto' => "",
                'msg' => "",
                'adjunto1' => "",
                'adjunto2' => "",
                'adjunto3' => "",
                'adjunto4' => "",
                'adjunto5' => "",
                'adjunto6' => "",
                'adjunto7' => "",
                'adjunto8' => "",
                'adjunto9' => "",
                'adjunto10' => "",
                'adjunto11' => "",
                'adjunto12' => "",
                'adjunto13' => "",
                'adjunto14' => "",
                'adjunto15' => "",
                'estatus' => "NO ENVIADO",
                'fecha_envio' => "0000-00-00 00:00:00",
                'nIDCorreo' => 1,
                'folio' => "0",
                'firma' => "",
                'observaciones' => "",
                'bestado' => ""
              );
              if ($resultOT != false) {
                $rowOT = mysqli_fetch_assoc($resultOT);
                $mailData['dest'] = "apimentel@coeficiente.mx"; // descomentar para subir a produccion
                $mailData['asunto'] = "Asignacion de adecuaciones OT - CRM ";
                $mailData['msg'] = "Hola, <br>El usuario, " . $_SESSION['usrNombre'] . " á asignado adecuaciones para la siguiente Orden de Trabajo: " .
                  "<br><b>Titulo</b>: " . $rowOT['titulo'] .
                  "<br><b>Descripcion</b>: " . $rowOT['descripcion'] .
                  "<br><b>Alta por</b>: " . get_userRealName($rowOT['usuario_alta']) .
                  "<br><b>Estado Actual</b>: Por Autorizar" .
                  "<br><b>Observacion</b>: La OT ha pasado a unestado pendiente de autorizacion, para autorizar la OT debes cambiar el estado de la misma a terminado y asignar las adecuaciones que consideres o dejar las que el tecnico ha asignado a la OT." .
                  "<br>" .
                  "<br><br>Revisa el sistema CRM en el apartado Ordenes de Trabajo. <br><br>Acceso al sistema CRM <a href=\"https://crm.coeficiente.mx/crm/modulos/login/index.php\">Aquí</a>";
              } else {
                $mailData['dest'] = "agarcia@softernium.com";
                $mailData['asunto'] = "CRM - OT Fallo Consulta Datos";
                $mailData['msg'] = "Hola, no se pudo consultar la informacion de la orden de trabajo con ID: " . $worOrdId . ".<br><br>Acceso al sistema CRM <a href=\"https://crm.coeficiente.mx/crm/modulos/login/index.php\">Aquí</a>";
              }
              $mailData['remitente'] = "";
              sendBDEmail($mailData);
              // **************************************************
              // **************************************************
              // fin del correo de abiso a abisai
            }
          }
          if(3==$rowCli['admin_auth']&&5==$worOrdType){
            log_write("DEBUG: SAVE-WORK-ORDER: cambiará estatus de verificado a terminado definitivo",4);
            if(1==$factible||3==$factible){
              $queryOthDime = "SELECT ordTra.estatus, ordTra.factible ".
                              "FROM ".$cfgTableNameMod.".ordenes_trabajo AS ordTra, ".
                                      $cfgTableNameMod.".rel_cotizacion_producto AS relCotPro ".
                              "WHERE relCotPro.cotizacion_id = ".$newCotId.
                              " AND relCotPro.relacion_id = ordTra.rel_cotizacion_producto";
              log_write("DEBUG: SAVE-WORK-ORDER: Dimensionamiento verificando que las demás estén terminadas y factibles: ".$queryOthDime,4);
              $resultOthDime = mysqli_query($db_modulos,$queryOthDime);
              if(!$resultOthDime){
                $salida = "ERROR||ocurrió un problema al verificar los datos de las ordenes asociadas a la cotización";
                log_write("ERROR: SAVE-WORK-ORDER: ocurrió un problema al verificar los datos de las ordenes asociadas a la cotización ".$queryOthDime,4);
              }
              else{
                $othDimeCont = 0; //cuantas ordenes de dimensionamiento están terminadas o canceladas
                $othDimeFactCont = 0; //cuantas ordenes de dimensionamiento son factibles
                $cantDimeOthTot = mysqli_num_rows($resultOthDime); //cuantas ordenes de dimensionamiento son en total
                while($rowOthDime = mysqli_fetch_assoc($resultOthDime)){
                  if(3==$rowOthDime['estatus']||4==$rowOthDime['estatus']){//si estan terminadas o canceladas
                    $othDimeCont++;
                    if(1==$rowOthDime['factible']){//si son factibles
                      $othDimeContFact++;
                    }
                  }
                }
              }

              log_write("DEBUG: SAVE-WORK-ORDER: Número de ordenes de dimensionamiento relacionadas: ".$cantDimeOth,4);
              log_write("DEBUG: SAVE-WORK-ORDER: Número de ordenes de dimensionamiento terminadas y factibles: ".$othDimeCont,4);

              if($cantDimeOthTot<=$othDimeCont){
                if($othDimeCont>=$othDimeContFact){
                  if(0 <= getProspectSellStatus($orgId)){
                    updateOriginStatus($orgType, 10, $orgId);
                    createClientFromProspect($orgId);
                    // cammbio de estaatus proceso finalizado
                      // cuando cambie a estado de finalizado se debe crear un cliente en base a los datos de este y este prospecto se debe congelar para queno se pueda modificar
                      // PUNTO C
                    // orgId es el id del prospecto
                    if (0 == $orgType) {
                      // estatus al que cambia, y id del prospecto
                      sendEstatusChangeEmail(10, $orgId);
                    } //fin del if 
                    $factMsgTxt = "El proceso de cierre ha finalizado.<br>";
                    $factText = "Todas las ordenes de dimensionamiento fueron factibles";
                    $factFlag = 0;
                  }
                }
                else{
                  $factMsgTxt = "Por favor pongase en contacto con el encargado de las órdenes de dimensionamiento.<br>";
                  $factText = "Una o varias ordenes de dimensionamiento de este contrato no fueron factibles";
                  $factFlag = 1;
                }           
              }
              else{
                $factText = "No se han llevado a cabo todas las ordenes de dimensionamiento";
                $factFlag = 2;
              }
            }
            else{
              $factMsgTxt = "Por favor pongase en contacto con el encargado de las órdenes de dimensionamiento.<br>";
              $factText = "Orden de dimensionamiento no factible";
              $factFlag = 3;
            }
            log_write("OK: SAVE-WORK-ORDER: ".$factText,4);
            if(0==$factFlag||1==$factFlag||3==$factFlag){
              $msg = "Hola.<br>".
                     "Se ha finalizado el procedimiento de la orden de dimensionamiento para el ".$name." ".$rowCli['nombre_comercial'].
                     " el resultado es :".$factText."<br>".
                     $factMsgTxt.
                     "Orden de trabajo ID: ".$worOrdId."<br> ".
                     "Cotización ID: ".$newCotId;
              sendPrivMsg($usuarioId,$rowCli['asignadoa'],"Orden de dimensionamiento terminada",$msg,1);
              sendPrivMsgDepartment($usuarioId,9,0,"Orden de dimensionamiento terminada",$msg,0,1);
              sendPrivMsgDepartment($usuarioId,2,0,"Orden de dimensionamiento terminada",$msg,0,1);
            }
          }
        }
      }
    }
  }
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  echo $salida;
} 
//*******************************************************************
//*******************************************************************
//*******************************************************************
//*******************************************************************
if("saveWorkOrderTask"==$_POST['action']){
  $db_modulos   = condb_modulos();
  log_write("DEBUG: SAVE-EVENT: Se va a guardar una tarea",4);
  $workOrdId = isset($_POST['workOrdId']) ? decrypt($_POST['workOrdId']): -1;
  $titulo    = isset($_POST['titulo']) ? $_POST['titulo'] : "";
  $fechaini  = isset($_POST['fechaIni']) ? $_POST['fechaIni'] : "";
  $fechafin  = isset($_POST['fechaFin']) ? $_POST['fechaFin'] : "";
  $estatus   = isset($_POST['eveEstat']) ? $_POST['eveEstat'] : 1;
  $tipo      = isset($_POST['eveTipo']) ? $_POST['eveTipo'] : -1;  //si es llamada, linea de vista etc
  $eveAction = isset($_POST['eveAction']) ? $_POST['eveAction'] : -1; //si se manda a guardar nuevo o editar
  $eveId     = isset($_POST['eveId']) ? decrypt($_POST['eveId']) : -1;   //id del evento por si se quiere editar
  $desc      = isset($_POST['eveDesc']) ? $_POST['eveDesc'] : "";
  $invitado  = isset($_POST['invitadoArray']) ? json_decode($_POST['invitadoArray'],true) : "";
  $equipo    = isset($_POST['equipoArray']) ? json_decode($_POST['equipoArray'],true) : "";
  $fact      = (isset($_POST['factible'])&&(3==$_POST['eveEstat'])&&(5==$_POST['eveTipo'])) ? $_POST['factible'] : 0;
  $inv = ",";
  $equ = ",";
  $fechaNow = date("Y-m-d H:i:s");
  foreach($invitado as $key=>$value){
    $inv .= $value.",";
  }
  foreach($equipo as $key=>$value){
    $equ .= $value.",";
  }
  log_write("DEBUG: SAVE-EVENT: workOrdId ".$workOrdId,4);
  log_write("DEBUG: SAVE-EVENT: workOrdId ".$_POST['workOrdId'],4);
  log_write("DEBUG: SAVE-EVENT: tarid ".$eveId,4);
  if((-1==$workOrdId)&&(0==$eveId)&&((0>=strlen($fechaini))||(0>=strlen($fechafin)))){
    log_write("ERROR: SAVE-EVENT: Esta tarea carece de fechas y no está asociada a ningúna orden de trabajo",4);
    $salida = "ERROR||Esta tarea carece de fechas y no está asociada a ningúna orden de trabajo";
  }
  else{
    if(0==$eveAction){
      if((5<=$tipo&&8>=$tipo)&&!isInGroup(7)){
        $fechaIni = "0000-00-00 00:00:00";
        $fechaFin = "0000-00-00 00:00:00";
      }
      if(!hasPermission(7,'a'))
        $estatus = 1;
      log_write("DEBUG: SAVE-EVENT: Se va a guardar una tarea NUEVA",4);
      $query = "INSERT INTO orden_trabajo_tareas(orden_trabajo_id,".
                                                 "titulo,".
                                                 "fecha_inicio,".
                                                 "fecha_fin,".
                                                 "usuario_id,".
                                                 "descripcion,".
                                                 "estatus_id,".
                                                 "fecha_alta,".
                                                 "invitados,".
                                                 "equipo,".
                                                 "factible) ".
                                 "VALUES(".$workOrdId.",'".
                                          $titulo."','".
                                          $fechaini."','".
                                          $fechafin."',".
                                          $usuarioId.",'".
                                          $desc."',".
                                          $estatus.",'".
                                          $fechaNow."','".
                                          $inv."','".
                                          $equ."',".
                                          $fact.")";
      $result = mysqli_query($db_modulos,$query);
      log_write("DEBUG: SAVE-EVENT: ".$query,4);
      if(!$result){
        log_write("ERROR: SAVE-EVENT: No se insertó la tarea",4);
        $salida = "ERROR|| Ocurrió un error al insertar la tarea a la db";
      }
      else{
        $newId = mysqli_insert_id($db_modulos);
        $usuarioNom = get_userRealName($usuarioId);
        foreach($invitado as $key=>$value){
          $nombre = get_userRealName($value);
          $usuarioNom = get_userRealName($usuarioId);
          $msg = "Hola, ".$nombre.".<br><br>".
                 $usuarioNom." te ha asignado la tarea ".$titulo." que comienza en ".$fechaini." y termina en ".$fechafin.":<br><br> ".
                 "<i>".$desc."</i><br><br>".
                 "Por favor, ponte en contacto con la persona encargada de Instalaciones para el seguimiento de esta tarea".
                 "Sistema CRM Coeficiente";
          $asunto = "Asignación de Tarea";
          if(!sendPrivMsg($usuarioId,$value,$asunto,$msg,1))
            log_write("ERROR: SAVE-EVENT: No se envió mensaje al usuario ".$nombre,4);
          else
            log_write("DEBUG: SAVE-EVENT: Se envió mensaje al usuario ".$nombre,4);
        }
        if(5<=$tipo&&8>=$tipo){
          $asunto = "Programación de evento de soporte";
          $msg = "Hola, ".$nombre.".<br><br>".
                 $usuarioNom."Ha programado un evento del tipo cuya fecha está pendiente por programar.<br><br>".
                 "Por favor dirígete al módulo de Instalaciones en el Menú de soporte para ver detalles de este evento";
          sendPrivMsgDepartment($usuarioId,7,0,$asunto,$msg,2,1);
        }
        writeOnJournal($usuarioId,"Ha insertado el evento [".$newId."] para el ".$typename." con id ".$id);
        $salida = "OK|| Los datos de este evento han sido guardados con éxito";          
      }  
    }
    elseif(1==$eveAction){
      log_write("DEBUG: SAVE-EVENT: Se va a actualizar un evento EXISTENTE",4);
      $campos = array("titulo"         => $titulo ,
                      "fecha_inicio"   => $fechaini,
                      "fecha_fin"      => $fechafin,
                      "usuario_id"     => $usuarioId,
                      "descripcion"    => $desc,
                      "estatus_id"     => $estatus,
                      "invitados"      => $inv,
                      "equipo"         => $equ,
                      "factible"       => $fact);
      $query = "SELECT * FROM orden_trabajo_tareas WHERE tarea_id=".$eveId;
      $result = mysqli_query($db_modulos,$query);
      log_write("DEBUG: SAVE-EVENT: ".$query,4);
      if(!$result){
        log_write("ERROR: SAVE-EVENT: No se pudo obtener los datos del evento",4);
        $salida = "ERROR|| Ocurrió un problema en la consulta de los datos para su actualización ";      
      }
      else{
        $row = mysqli_fetch_assoc($result);
        $diff = array_diff_assoc($row,$campos);
        $invArray = explode(",",substr($row['invitados'],1,-1));
        $diffInv = array_diff_assoc($invitado,$invArray);
        foreach($diffInv as $key => $value){
          $nombre = get_userRealName($value);
          $usuarioNom = get_userRealName($usuarioId);
          $msg = "Hola, ".$nombre.".<br><br>".
                 $usuarioNom." te ha asignado la tarea ".$titulo." que comienza en ".$fechaini." y termina en ".$fechafin.":<br><br> ".
                 "<i>".$desc."</i><br><br>".
                 "Por favor, ponte en contacto con la persona encargada de Instalaciones para el seguimiento de esta tarea".
                 "Sistema CRM Coeficiente";
          $asunto = "Asignación de Tarea";
          if(!sendPrivMsg($usuarioId,$value,$asunto,$msg,1))
            log_write("ERROR: SAVE-EVENT: No se envió mensaje al usuario ".$nombre,4);
          else
            log_write("DEBUG: SAVE-EVENT: Se envió mensaje al usuario ".$nombre,4);
        }
        log_write("DEBUG: SAVE-EVENT: campos nuevos ".print_r($campos,true),4);
        log_write("DEBUG: SAVE-EVENT: campos actuales ".print_r($row,true),4);
        log_write("DEBUG: SAVE-EVENT: campos diferentes ".print_r($diff,true),4);
        $query2 = "UPDATE orden_trabajo_tareas SET titulo='".$titulo."', fecha_inicio='".$fechaini."', fecha_fin='".$fechafin."', descripcion='".$desc."', estatus_id=".$estatus.", invitados='".$inv."', equipo='".$equ."', factible=".$fact." WHERE tarea_id=".$eveId;
        $result2 = mysqli_query($db_modulos,$query2);
        log_write("DEBUG: SAVE-EVENT: ".$query2,4);
        if(!$result2){
          log_write("ERROR: SAVE-EVENT: No se pudo actualizar la orden de trabajo",4);
          $salida = "ERROR|| Ocurrió un error al actualizar la orden de trabajo en la db ";
        }
        else{
          foreach($diff as $key => $value){
            writeOnJournal($usuarioId,"Ha modificado el campo \"".$key."\" de [".$value."] a [".$campos[$key]."]"." para evento id [".$id."]");
          }
          log_write("OK: SAVE-EVENT: La orden de trabajo ha sido actualizado",4);
          $salida = "OK|| Los datos de esta orden de trabajo han sido guardados con éxito";
          if(1==$fact&&3==$estatus&&5==$tipo){         
            updateOriginStatus($orgType,3,$id);
            // cambio de estatus a propuesta
            //orgId es el id del prospecto
            if(0 == $orgType){
              // estatus al que cambia, y id del prospecto
              sendEstatusChangeEmail(3, $id);
            }//fin del if 
          }
        } 
      }                                
    }
    /*if(1==$fact&&3==$estatus&&5==$tipo){  
      if(2==$orgType)
        updateOriginStatus($orgSiteType,3,$id);
      else
       updateOriginStatus($orgType,3,$id);
    }*/
  }
  mysqli_close($db_modulos);
  echo $salida; 
}
/***************************Fin de sección***************************/
//*******************************************************************
//*******************************************************************
//*******************************************************************
/***************Sección de gestión de enlaces de sitio***************/
if("getSiteConnectionDetails" == $_POST['action']){
  $db_catalogos = condb_catalogos();
  $db_modulos   = condb_modulos();
  $db_usuarios  = condb_usuarios();
  $sitConId = isset($_POST['siteConId']) ? decrypt($_POST['siteConId']) : -1;
  $option   = isset($_POST['option']) ? $_POST['option'] : -1;
  
  $query = "SELECT * FROM enlaces WHERE enlace_id=".$sitConId;
  
  $query2 = "SELECT rel.*, cont.* FROM rel_origen_contacto AS rel, contactos AS cont WHERE rel.tipo_origen=8 AND rel.origen_id=".$sitConId." AND rel.contacto_id=cont.contacto_id ORDER BY cont.apellidos ASC";
 
  $result = mysqli_query($db_modulos,$query);
  
  $result2 = mysqli_query($db_modulos,$query2);
  
  if(-1 == $option||-1 == $sitConId){
    $salida = "ERROR||No se recibieron los datos de id del enlace";
    goto sitConDet;
  }  
  if(!$result||!$result2){
    $salida = "ERROR||Ocurrió un problema al consultar los datos del enlace de sitio";
  }  
  else{    
    $salida = "OK||";  
    $row = mysqli_fetch_assoc($result);                      
    if(hasPermission(8,'e')||((hasPermission(8,'n')&&$row['usuario_alta']==$usuarioId))){
      $salida .= "<button type=\"button\" id=\"btn_siteConEdit_".$sitConId."\" title=\"Editar Registro\" class=\"btn btn-xs btn-success btn-responsive\" onClick=\"siteConnectionEdit('".encrypt($sitConId)."');\"><span class=\"glyphicon glyphicon-edit\"> Editar</span></button>";                       
    }   
    if(0 == $option){    
      $salida .= "<div class=\"table-responsive\"><table id=\"tbl_siteConnectionDetails\" class=\"table table-hover table-striped table-condensed\">\n".
                "  <thead>\n".
                "    <tr>\n".
                "      <th>Campo</th>\n".
                "      <th>Valor</th>\n".
                "    </tr>\n".
                "  </thead>\n".
                "  <tbody>\n";
                
      //$row = mysqli_fetch_assoc($result);  
      $nombreAlta = get_userRealName($row['usuario_alta']);
      $topologia = get_siteConTopology($row["topologia"]);
      $activo = get_siteConActive($row["activo"]);
              
      $salida .= "    <tr>\n".
                 "      <td><b>Enlace ID</b></td>\n".
                 "      <td>".$row["idenlace"]."</td>\n".
                 "    </tr>\n".  
                 "    <tr>\n".
                 "      <td><b>Nodo</b></td>\n".
                 "      <td>".$row["nodo"]."</td>\n".
                 "    </tr>\n".
                 /*"    <tr>\n".
                 "      <td><b>Sitio</b></td>\n".
                 "      <td>".$row["sitio_id"]."</td>\n".
                 "    </tr>\n".*/
                 "    <tr>\n".
                 "      <td><b>BW Bajada</b></td>\n".
                 "      <td>".$row["bw_download"]." Mbps</td>\n".
                 "    </tr>\n".
                 "    <tr>\n".
                 "      <td><b>BW Subida</b></td>\n".
                 "      <td>".$row["bw_upload"]." Mbps</td>\n".
                 "    </tr>\n".
                 "    <tr>\n".
                 "      <td><b>Topologia</b></td>\n".
                 "      <td>".$topologia."</td>\n".
                 "    </tr>\n".
                 "    <tr>\n".
                 "      <td><b>Fecha de Instalación</b></td>\n".
                 "      <td>".$row["fecha_instalacion"]."</td>\n".
                 "    </tr>\n".
                 "    <tr>\n".
                 "      <td><b>Activo</b></td>\n".
                 "      <td>".$activo."</td>\n".
                 "    </tr>\n".
                 "    <tr>\n".
                 "      <td><b>DBM</b></td>\n".
                 "      <td>".$row["dbm"]."</td>\n".
                 "    </tr>\n".
                 "    <tr>\n".
                 "      <td><b>Radiobase</b></td>\n".
                 "      <td>".$row["radiobase"]."</td>\n".
                 "    </tr>\n".
                 "    <tr>\n".
                 "      <td><b>Modelo de Router</b></td>\n".
                 "      <td>".$row["router_modelo"]."</td>\n".
                 "    </tr>\n".
                 "    <tr>\n".
                 "      <td><b>No. Serie de Router</b></td>\n".
                 "      <td>".$row["router_serie"]."</td>\n".
                 "    </tr>\n".
                 "    <tr>\n".
                 "      <td><b>Modelo Antena 1</b></td>\n".
                 "      <td>".$row["ant_1_modelo"]."</td>\n".
                 "    </tr>\n".
                 "    <tr>\n".
                 "      <td><b>No. Serie Antena 1</b></td>\n".
                 "      <td>".$row["ant_1_serie"]."</td>\n".
                 "    </tr>\n".
                 "    <tr>\n".
                 "      <td><b>Modelo Antena 2</b></td>\n".
                 "      <td>".$row["ant_2_modelo"]."</td>\n".
                 "    </tr>\n".
                 "    <tr>\n".
                 "      <td><b>No. Serie Antena 2</b></td>\n".
                 "      <td>".$row["ant_2_serie"]."</td>\n".
                 "    </tr>\n".
                 "    <tr>\n".
                 "      <td><b>Comentarios</b></td>\n".
                 "      <td>".$row["comentarios"]."</td>\n".
                 "    </tr>\n".
                 "    <tr>\n".
                 "      <td><b>Fecha de Desinstalación</b></td>\n".
                 "      <td>".$row["fecha_desinstalacion"]."</td>\n".
                 "    </tr>\n".
                 "    <tr>\n". 
                 "      <td><b>Agregado por:</b></td>\n".
                 "      <td><a href=/crm/modulos/perfil/index.php?id='".encrypt($row['usuario_alta'])."'>".$nombreAlta."</a></td>\n".
                 "    </tr>\n";                 
      $salida .= "  </tbody>".
                 "</div>";       
    
    }
    if(1 == $option){
      //$row = mysqli_fetch_assoc($result);
      
      $salida = "OK||".$row['idenlace']."||".
                       $row['nodo']."||".
                       $row['sitio_id']."||".
                       $row['bw_download']."||".
                       $row['bw_upload']."||".
                       $row['topologia']."||".
                       $row['fecha_instalacion']."||".
                       $row['activo']."||".
                       $row['dbm']."||".
                       $row['radiobase']."||".
                       $row['router_modelo']."||".
                       $row['router_serie']."||".
                       $row['ant_1_modelo']."||".
                       $row['ant_1_serie']."||".
                       $row['ant_2_modelo']."||".
                       $row['ant_2_serie']."||".
                       $row['comentarios']."||".
                       $row['fecha_desinstalacion']."||";
                       
      $contArr = array();
      while($row2 = mysqli_fetch_assoc($result2)){
        $contArr[] = array($row2["contacto_id"],$row2["nombre"],$row2["apellidos"],$row2["puesto"],$row2["telefono"],$row2["tel_movil"],$row2["email"]);
      }
      $salida .= json_encode($contArr);
    }
  }
  sitConDet:
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios); 
  echo $salida;
}
//*******************************************************************
//*******************************************************************
//*******************************************************************
//*******************************************************************
//guarda los datos del enlace de sitio ya sea guardado (option=0) o editado (option=1)
if("saveSiteConnections" == $_POST['action']){
  $db_catalogos = condb_catalogos();
  $db_modulos   = condb_modulos();
  $db_usuarios  = condb_usuarios();

  $sitioId = isset($_POST['siteId']) ? $_POST['siteId'] : -1;
  $sitConId = isset($_POST['siteConId']) ? decrypt($_POST['siteConId']) : -1;
  
  $nodSitCon     = isset($_POST['nodSitCon'])     ? $_POST['nodSitCon']     : "";
  $downSitCon    = isset($_POST['downSitCon'])    ? $_POST['downSitCon']    : "";
  $upSitCon      = isset($_POST['upSitCon'])      ? $_POST['upSitCon']      : "";
  $fecInsSitCon  = isset($_POST['fecInsSitCon'])  ? $_POST['fecInsSitCon']  : ""; 
  $dbmSitCon     = isset($_POST['dbmSitCon'])     ? $_POST['dbmSitCon']     : "";
  $radSitCon     = isset($_POST['radSitCon'])     ? $_POST['radSitCon']     : "";
  $modAnt1SitCon = isset($_POST['modAnt1SitCon']) ? $_POST['modAnt1SitCon'] : "";
  $serAnt1SitCon = isset($_POST['serAnt1SitCon']) ? $_POST['serAnt1SitCon'] : "";
  $modAnt2SitCon = isset($_POST['modAnt2SitCon']) ? $_POST['modAnt2SitCon'] : "";
  $serAnt2SitCon = isset($_POST['serAnt2SitCon']) ? $_POST['serAnt2SitCon'] : "";
  $modRouSitCon  = isset($_POST['modRouSitCon'])  ? $_POST['modRouSitCon']  : "";
  $serRouSitCon  = isset($_POST['serRouSitCon'])  ? $_POST['serRouSitCon']  : "";
  $fecDesSitCon  = isset($_POST['fecDesSitCon'])  ? $_POST['fecDesSitCon']  : 'NULL';
  $sitConDesc    = isset($_POST['sitConDesc'])    ? $_POST['sitConDesc']    : "";
  $selConTop     = isset($_POST['selConTop'])     ? $_POST['selConTop']     : 0;
  $selConAct     = isset($_POST['selConAct'])     ? $_POST['selConAct']     : 0;
  
  $option = isset($_POST['siteConnOption']) ? $_POST['siteConnOption'] : -1;
  
  //$idEnlace = "ABC-iekd-1383";
  
  $fecha = date("Y-m-d H:i:s");
  
  $anio = date("Y");
  $mes = date("m");
  
  $queryPrev = "SELECT idenlace FROM enlaces WHERE fecha_alta>='".$anio."-".$mes."-01 00:00:00' ORDER BY enlace_id DESC LIMIT 1";
  
  $resultPrev = mysqli_query($db_modulos,$queryPrev);
  
  if(!$resultPrev){
    $salida = "ERROR||Ocurrió un problema al generarse el id del enlace";
    goto insSiteConn;
  }
  
  $rowPrev = mysqli_fetch_assoc($resultPrev);
  
  if(0>=mysqli_num_rows($resultPrev)){
    $idIndex = 1;
  }
  else{
    $idEnlLast = explode("-",$rowPrev['idenlace']); 
    $idIndex = (string)(((int)$idEnlLast[2])+1);  
  } 
  
  for($a=strlen($idIndex);$a<4;$a++){
    $idIndex = "0".$idIndex;
  }
  
  $idEnlace = "EC-".date("y").$mes."-".$idIndex;
  
  /*$salida = "ERROR||test ".$idEnlace;
    goto insSiteConn;*/
  
  $contArr = isset($_POST['sitConnContact']) ? json_decode($_POST['sitConnContact']) : "";
  
  if(-1 == $option){
    $salida = "ERROR||No se pudo determinar la acción de guardado para este enlace";     
    goto insSiteConn;
  }
  
  if(0 == $option){
    if(-1 == $sitioId){
      $salida = "ERROR||Ocurrió un problema al recibir los datos para su guardado";     
      goto insSiteConn;
    }
  
    log_write("DEBUG: SAVE-SITECONNECTION-NEW: Se va a insertar un enlace de sitio nuevo",2);
  
    $query = "INSERT INTO enlaces(idenlace,".
                                 "nodo,".
                                 "sitio_id,".
                                 "bw_download,".
                                 "bw_upload,".
                                 "topologia,".
                                 "fecha_instalacion,".
                                 "activo,".
                                 "dbm,".
                                 "radiobase,".
                                 "router_modelo,".
                                 "router_serie,".
                                 "ant_1_modelo,".
                                 "ant_1_serie,".
                                 "ant_2_modelo,".
                                 "ant_2_serie,".
                                 "comentarios,".
                                 "fecha_desinstalacion,".
                                 "usuario_alta,".
                                 "fecha_alta) ".
             "VALUES('".$idEnlace."',".
                    "'".$nodSitCon."',".
                        $sitioId.",".
                    "'".$downSitCon."',".
                    "'".$upSitCon."',".
                        $selConTop.",".
                    "'".$fecInsSitCon."',".
                        $selConAct.",".
                        $dbmSitCon.",".
                    "'".$radSitCon."',".    
                    "'".$modRouSitCon."',".
                    "'".$serRouSitCon."',".                        
                    "'".$modAnt1SitCon."',".
                    "'".$serAnt1SitCon."',".
                    "'".$modAnt2SitCon."',".
                    "'".$serAnt2SitCon."',".                    
                    "'".$sitConDesc."',".                  
                    "'".$fecDesSitCon."',".
                        $usuarioId.",".
                    "'".$fecha."')";
    
    $result = mysqli_query($db_modulos,$query);
    log_write("DEBUG: SAVE-SITECONNECTION-NEW: ".$query,2);
    
    if(!$result){
      log_write("ERROR: SAVE-SITECONNECTION-NEW: Ocurrió un problema al guardarse los datos del enlace de sitio nuevo",2);
      $salida = "ERROR||Ocurrió un problema al guardarse los datos del enlace de sitio nuevo";     
    }
    else{
      log_write("DEBUG: SAVE-SITECONNECTION-NEW: Se insertaron los datos generales del enlace de sitio. Se van a insertar sus contactos",2);
      $newSitConnId = mysqli_insert_id($db_modulos);
      
      log_write("DEBUG: SAVE-SITECONNECTION-NEW: Nuevo id de Sitio ".$newSitConnId,2);
      
      foreach($contArr as $key => $value){
        if(!insertContact($db_modulos,$value,8,$newSitConnId)){
          log_write("ERROR: SAVE-SITECONNECTION-NEW: Ocurrió un problema al guardarse los datos del contacto",2);
          goto insSiteConn;
        }
      }
      writeOnJournal($usuarioId,"Ha insertado un enlace nuevo id [".$newSitConnId."] para el sitio con id [".$sitioId."]");
      log_write("OK: SAVE-SITECONNECTION-NEW: El enlace de sitio se ha insertado exitosamente",2);
      $salida = "OK||Los datos se han guardado con éxito";  
    }  
  }
  
  if(1 == $option){
    if(-1 == $sitConId){
      $salida = "ERROR||Ocurrió un problema al recibir los datos para su guardado";     
      goto insSiteConn;
    }
  
    log_write("DEBUG: SAVE-SITECONNECTION-NEW: Se va a actualizar un enlace de sitio existente",2); 
    
    $query = "UPDATE enlaces SET nodo = '".$nodSitCon."',".
                                //"sitio_id = ".$sitioId.",".
                                "bw_download = '".$downSitCon."',".
                                "bw_upload = '".$upSitCon."',".
                                "topologia = ".$selConTop.",".
                                "fecha_instalacion = '".$fecInsSitCon."',".
                                "activo = ".$selConAct.",".
                                "dbm = ".$dbmSitCon.",".
                                "radiobase = '".$radSitCon."',".
                                "router_modelo = '".$modRouSitCon."',".
                                "router_serie = '".$serRouSitCon."',".
                                "ant_1_modelo = '".$modAnt1SitCon."',".
                                "ant_1_serie = '".$serAnt1SitCon."',".
                                "ant_2_modelo = '".$modAnt2SitCon."',".
                                "ant_2_serie = '".$serAnt2SitCon."',".
                                "comentarios = '".$sitConDesc."',".
                                "fecha_desinstalacion = '".$fecDesSitCon."' WHERE enlace_id=".$sitConId;
    
    $query2 = "SELECT rel.*, cont.* FROM rel_origen_contacto AS rel, contactos AS cont WHERE rel.tipo_origen=8 AND rel.origen_id=".$sitConId." AND rel.contacto_id=cont.contacto_id";
    
    $result = mysqli_query($db_modulos,$query);
    
    log_write("DEBUG: SAVE-SITECONNECTION-UPDATE: ".$query,2);
    
    if(!$result){
      log_write("ERROR: Ocurrió un problema al actualizar los datos del enlace de sitio",2);
      $salida = "ERROR||Ocurrió un problema al actualizar los datos del enlace de sitio";
    }
    else{
      log_write("DEBUG: Se actualizaron los datos generales del enlace de sitio, se van a acatualizar los contactos",2);
      
      if(!updateContactsByArray($contArr,$query2,$salida,$sitConId,8)){ 
        log_write("DEBUG: SAVE-SITE-UPDATE: No se actualizaron los contactos del enlace de este sitio",2);
        $salida = "ERROR|| Ocurrió un problema al actualizarse la lista de los contactos";
      }
      else{
        writeOnJournal($usuarioId,"Ha modificado los datos del enlace de sitio con id [".$sitConId."]");  
        log_write("DEBUG: SAVE-SITE-UPDATE: Los contactos para este enlace de sitio fueron actualizados",2);
        $salida = "OK|| Los datos se han actualizado con éxito";
      }     
    }                                
  } 
  insSiteConn: 
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios); 
  echo $salida;
}
//*******************************************************************
//*******************************************************************
//*******************************************************************
//*******************************************************************
/*************Fin de sección*****************/

/*************Sección de gestión de servicios y equipos asociados a servicios*****************/

//llamada de ajax. Obtiene los datos detallados de un servicio, dependiendo de la opción es para su despliegue en la ventana de detalles o para su edición
if("getServiceDetails" == $_POST["action"]){
  $db_catalogos = condb_catalogos();
  $db_modulos   = condb_modulos();
  $db_usuarios  = condb_usuarios();
  
  $serviceId = isset($_POST['serviceId']) ? $_POST['serviceId'] : -1;
  $option = isset($_POST['option']) ? $_POST['option'] : -1;
  
  $query = "SELECT * FROM servicios WHERE servicio_id=".$serviceId;
  
  $result = mysqli_query($db_modulos,$query);
  
  if(-1==$serviceId||-1==$option){
    $salida = "ERROR||No se recibió el identificador del servicio para su despliegue o edición";
    goto serDeta;
  }  
  if(!$result){
    $salida = "ERROR||Ocurrió un problema al obtenerse los datos del servicio";
  }
  else{
    if(0==mysqli_num_rows($result)){
      $salida = "ERROR||No existen registros para este servicio";
    }
    else{
      $row = mysqli_fetch_assoc($result); 
      if(0 == $option){
         $salida .= "<table id=\"tbl_serviceDetails\" class=\"table table-striped\">\n".
                    "  <thead>\n".
                    "    <tr>\n".
                    "      <th>Campo</th>\n".
                    "      <th>Valor</th>\n".
                    "    </tr>\n".
                    "  </thead>\n".
                    "  <tbody>\n";         
         $nombreAlta = get_userRealName($row['usuario_alta']);
         $tipoServicio = get_serviceType($row['tipo_servicio']);
         $salida .= "    <tr>\n".
                    "      <td><b>ID</b></td>\n".
                    "      <td>".$row['servicio_id']."</td>\n".                  
                    "    </tr>\n".
                    "    <tr>\n".
                    "      <td><b>Tipo de Servicio</b></td>\n".
                    "      <td>".$tipoServicio."</td>\n".
                    "    </tr>\n".
                    "    <tr>\n".                    
                    "      <td><b>BW download</b></td>\n".
                    "      <td>".$row['bw_bajada']." Mbps</td>\n".
                    "    </tr>\n".
                    "    <tr>\n". 
                    "      <td><b>Cantidad IP públicas</b></td>\n".
                    "      <td>".$row['cant_ip_publica']."</td>\n".
                    "    </tr>\n".
                    "    <tr>\n". 
                    "      <td><b>Línea analógica</b></td>\n".
                    "      <td>".$row['linea_analogica']."</td>\n".
                    "    </tr>\n".
                    "    <tr>\n". 
                    "      <td><b>Cantidad Dids</b></td>\n".
                    "      <td>".$row['cant_did']."</td>\n".
                    "    </tr>\n".
                    "    <tr>\n". 
                    "      <td><b>BW upload</b></td>\n".
                    "      <td>".$row['bw_subida']." Mbps</td>\n".
                    "    </tr>\n".
                    "    <tr>\n". 
                    "      <td><b>Cantidad canales</b></td>\n".
                    "      <td>".$row['cant_canales']."</td>\n".
                    "    </tr>\n".
                    "    <tr>\n". 
                    "      <td><b>Cantidad extensiones</b></td>\n".
                    "      <td>".$row['cant_extensiones']."</td>\n".
                    "    </tr>\n".
                    "    <tr>\n". 
                    "      <td><b>Dominio VPBX</b></td>\n".
                    "      <td>".$row['dom_vpbx']."</td>\n".
                    "    </tr>\n".
                    "    <tr>\n". 
                    "      <td><b>IP VPBX</b></td>\n".
                    "      <td>".$row['ip_vpbx']."</td>\n".
                    "    </tr>\n".
                    "    <tr>\n". 
                    "      <td><b>Costo</b></td>\n".
                    "      <td>$".number_format($row['costo'], 2, '.', ',')."</td>\n".
                    "    </tr>\n".
                    "    <tr>\n". 
                    "      <td><b>Agregado por:</b></td>\n".
                    "      <td><a href=/crm/modulos/perfil/index.php?id='".encrypt($row['usuario_alta'])."'>".$nombreAlta."</a></td>\n".
                    "    </tr>\n";                    
         $salida .= "  </tbody>\n".
                    "</table>\n<hr>";  
      }
      
      if(1 == $option){
        $salida = "OK||".
                  $row['servicio_id']."||".
                  $row['tipo_servicio']."||".
                  $row['bw_bajada']."||".
                  $row['cant_ip_publica']."||".
                  $row['linea_analogica']."||".
                  $row['cant_did']."||".
                  $row['bw_subida']."||".
                  $row['cant_canales']."||".
                  $row['cant_extensiones']."||".
                  $row['dom_vpbx']."||".
                  $row['ip_vpbx']."||".
                  $row['costo']."||".
                  $row['usuario_alta'];     
      }              
    }    
  } 
  serDeta:
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida; 
}

if("saveServiceNew" == $_POST["action"]){

  $db_catalogos = condb_catalogos();
  $db_modulos   = condb_modulos();
  $db_usuarios  = condb_usuarios();
  
  log_write("DEBUG: SAVE-SERVICE-NEW: se guardará un servicio",4);
  
  $option      = isset($_POST['option'])      ? $_POST['option']      : -1;
  $serviceId   = isset($_POST['serviceId'])   ? $_POST['serviceId']   : -1;
  $serType     = isset($_POST['serType'])     ? $_POST['serType']     : -1;
  $serDownload = isset($_POST['serDownload']) && (0<strlen((string)$_POST['serDownload'])) ? $_POST['serDownload'] : 0;
  $serUpload   = isset($_POST['serUpload'])   && (0<strlen((string)$_POST['serUpload']))   ? $_POST['serUpload']   : 0;
  $serCantIp   = isset($_POST['serCantIp'])   && (0<strlen((string)$_POST['serCantIp']))   ? $_POST['serCantIp']   : 0;
  $serLinAna   = isset($_POST['serLinAna'])   && (0<strlen($_POST['serLinAna']))           ? $_POST['serLinAna']   : "N/A";
  $serCantDid  = isset($_POST['serCantDid'])  && (0<strlen((string)$_POST['serCantDid']))  ? $_POST['serCantDid']  : 0;
  $serCantCan  = isset($_POST['serCantCan'])  && (0<strlen((string)$_POST['serCantCan']))  ? $_POST['serCantCan']  : 0;
  $serCantExt  = isset($_POST['serCantExt'])  && (0<strlen((string)$_POST['serCantExt']))  ? $_POST['serCantExt']  : 0;
  $serDomVpbx  = isset($_POST['serDomVpbx'])  && (0<strlen($_POST['serDomVpbx']))          ? $_POST['serDomVpbx']  : "N/A";
  $serIpVpbx   = isset($_POST['serIpVpbx'])   && (0<strlen($_POST['serIpVpbx']))           ? $_POST['serIpVpbx']   : "N/A";
  $serCosto    = isset($_POST['serCosto'])    && (0<strlen((string)$_POST['serCosto']))    ? $_POST['serCosto']    : 0.0;
  $siteConId   = isset($_POST['siteConId'])   ? decrypt($_POST['siteConId'])   : -1; 
  
  if(-1 == $option||-1 == $siteConId){
    log_write("DEBUG: SAVE-SERVICE-NEW: No se recibió dato de opción de guardado",4);
  }
  if(0 == $option){ //si se guarda servicio nuevo
    log_write("DEBUG: SAVE-SERVICE-NEW: se guardará un servicio nuevo",4);
    
    $query = "INSERT INTO servicios(enlace_id,".
                                   "tipo_servicio,".
                                   "bw_bajada,".
                                   "cant_ip_publica,".
                                   "linea_analogica,".
                                   "cant_did,".
                                   "bw_subida,".
                                   "cant_canales,".
                                   "cant_extensiones,".
                                   "dom_vpbx,".
                                   "ip_vpbx,".
                                   "costo,".
                                   "usuario_alta) ".
             "VALUES(".$siteConId.",".
                       $serType.",".
                       $serDownload.",".
                       $serCantIp.",".                        
                   "'".$serLinAna."',".
                       $serCantDid.",".
                       $serUpload.",".
                       $serCantCan.",".
                       $serCantExt.",".
                   "'".$serDomVpbx."',".
                   "'".$serIpVpbx."',".
                       $serCosto.",".
                       $usuarioId.")";
  
    $result = mysqli_query($db_modulos,$query);
    
    log_write("DEBUG: SAVE-SERVICE-NEW: ".$query,4);
    
    if(!$result){
      $salida = "ERROR||Ocurrió un problema al insertar los datos de servicio";
      log_write("DEBUG: SAVE-SERVICE-NEW: Ocurrió un problema al insertar los datos de servicio",4);
    }
    else{
      $salida = "OK||Los datos del servicio se han guardado correctamente";
      log_write("DEBUG: SAVE-SERVICE-NEW: Los datos del servicio se han guardado correctamente",4);
      writeOnJournal($usuarioId,"Ha insertado el servicio id [".mysqli_insert_id($db_modulos)."] para el enlace con id ".$siteConId);
    }
  }  
  if(1 == $option){ //si se edita servicio existente
    log_write("DEBUG: SAVE-SERVICE-NEW: se editará un servicio existente",4);


    $query = "UPDATE servicios SET tipo_servicio=$serType,".
                                   "bw_bajada=$serDownload,".
                                   "cant_ip_publica=$serCantIp,".
                                   "linea_analogica='$serLinAna',".
                                   "cant_did=$serCantDid,".
                                   "bw_subida=$serUpload,".
                                   "cant_canales=$serCantCan,".
                                   "cant_extensiones=$serCantExt,".
                                   "dom_vpbx='$serDomVpbx',".
                                   "ip_vpbx='$serIpVpbx',".
                                   "costo=$serCosto WHERE servicio_id=$serviceId";
                                   
     $result = mysqli_query($db_modulos,$query);
     
     log_write("DEBUG: SAVE-SERVICE-NEW: ".$query,4);
     
     if(!$result){
       $salida = "ERROR||Ocurrió un problema al actualizarse los datos del servicio";
       log_write("ERROR: SAVE-SERVICE-NEW: ocurrió un problema al actualizarse los datos del servicio",4);
       writeOnJournal($usuarioId,"Ha modificado los datos del servicio con id [".$serviceId."] para el enlace con id ".$siteConId);
     }
     else{
       $salida = "OK||Los datos del servicio se han actualizado con éxito";
       log_write("OK: SAVE-SERVICE-NEW: los datos del servicio se han actualizado con éxito",4);      
     }
  }
   
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}

if("deleteService" == $_POST["action"]){
  $db_catalogos = condb_catalogos();
  $db_modulos   = condb_modulos();
  $db_usuarios  = condb_usuarios();

  log_write("DEBUG: DELETE-SERVICE: Se va a eliminar una cotización",4);

  $serviceId = isset($_POST['serviceId']) ? $_POST['serviceId'] : -1; 
  
  $query = "DELETE FROM servicios WHERE servicio_id=".$serviceId;
  
  $result = mysqli_query($db_modulos,$query);
  
  log_write("DEBUG: DELETE-SERVICE: ".$query,4);
  
  if(-1 == $serviceId){
    $salida = "ERROR||No se pudo recibir el id del servicio para su eliminación";
    goto delSera;
  }
  
  if(!$result){
    log_write("ERROR: DELETE-SERVICE: No se eliminó el servicio",4);
    $salida = "ERROR||Ocurrió un problema al intentar eliminarse el servicio";
  }
  else{
    log_write("OK: DELETE-SERVICE: Se eliminó el servicio con id [".$serviceId."]",4);
    writeOnJournal($usuarioId,"Ha eliminado el equipo con id [".$serviceId."]");
    $salida = "OK||El servicio se ha eliminado con éxito";
  } 
  delSera:
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}

if("getServiceEquipment" == $_POST["action"]){
  $db_catalogos = condb_catalogos();
  $db_modulos   = condb_modulos();
  $db_usuarios  = condb_usuarios();
  
  $serviceId = isset($_POST['serviceId']) ? $_POST['serviceId'] : -1;
  
  $query = "SELECT rel.*, cat.*, ser.usuario_alta FROM ".$cfgTableNameMod.".servicios AS ser, ".$cfgTableNameMod.".rel_servicio_equipo AS rel, ".$cfgTableNameCat.".cat_equipos AS cat WHERE rel.servicio_id=".$serviceId." AND rel.equipo_id=cat.equipo_id AND rel.servicio_id=ser.servicio_id";
  
  $query2 = "SELECT usuario_alta FROM servicios WHERE servicio_id=".$serviceId;
  
  $result = mysqli_query($db_modulos,$query);
  $result2 = mysqli_query($db_modulos,$query2);
  
  if(!$result||!$result2){
    $salida = "ERROR||Ocurrió un problema al obtenerse los datos de los equipos para este servicio";
  }
  else{
    $salida = "";
    
    $row2 = mysqli_fetch_assoc($result2);
    if(hasPermission(5,'w')||(hasPermission(5,'m')&&$row2['usuario_alta']==$usuarioId)){ 
      $salida .= "<div class=\"col container-fluid\">".
                 "  <button type=\"button\" id=\"btn_serviceEquipmentNew\" title=\"Agregar Equipo\" class=\"btn btn-default\" onClick=\"openNewEquipment($serviceId);\"><span class=\"glyphicon glyphicon-asterisk\"></span> Agregar Equipo</button><br>".
                 "</div><br>";
    }
    if(0==mysqli_num_rows($result)){
      $salida .= "No existen registros disponibles haga click en <strong>Agregar Equipo</strong> para añadir uno nuevo";
    }
    else{
      $salida .= "<table id=\"tbl_serviceEquipmentDetails\" class=\"table table-striped\">\n".
                 "  <thead>\n".
                 "    <tr>\n".
                 "      <th>Equipo</th>\n".
                 "      <th>Num. Serie</th>\n".
                 "      <th>Acciones</th>\n".
                 "    </tr>\n".
                 "  </thead>\n".
                 "  <tbody>\n";
      while($row = mysqli_fetch_assoc($result)){ 
        $salida .= "    <tr>\n".
                   "      <td>".$row['descripcion']."</td>\n".
                   "      <td>".$row['serie']."</td>\n". 
                   "      <td>";
        if(hasPermission(5,'e')||(hasPermission(5,'n')&&$row['usuario_alta']==$usuarioId)){  
          $salida .= "        <button type=\"button\" id=\"btn_serviceEquipmentEdit_".$row['relacion_id']."\" title=\"Editar\" class=\"btn btn-xs btn-success btn-responsive\" onClick=\"serviceEquipmentEdit(".$row['relacion_id'].");\"><span class=\"glyphicon glyphicon-edit\"></span></button>";
        }           
        if(hasPermission(5,'d')||(hasPermission(5,'o')&&$row['usuario_alta']==$usuarioId)){      
          $salida .= "        <button type=\"button\" id=\"btn_serviceEquipmentDelete_".$row['relacion_id']."\" title=\"Eliminar Equipo\" class=\"btn btn-xs btn-danger btn-responsive\" onClick=\"deleteServiceEquipment(".$row['relacion_id'].");\"><span class=\"glyphicon glyphicon-remove\"></span></button>";
        }                 
        $salida .= "      </td>\n".                  
                   "    </tr>\n";
      }
      $salida .= "  </tbody>\n".
                 "</table>\n<hr>";
    }  
  } 
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida; 
}

if("getServiceEquipmentDetails" == $_POST["action"]){
  $db_catalogos = condb_catalogos();
  $db_modulos   = condb_modulos();
  $db_usuarios  = condb_usuarios();
  $serEquId  = isset($_POST['serviceEquipId']) ? $_POST['serviceEquipId'] : -1;

  $query = "SELECT * FROM rel_servicio_equipo WHERE relacion_id=".$serEquId;
  
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $salida = "ERROR||No se pudo obtener los datos del equipo para su edición";
  }
  else{
    $row = mysqli_fetch_assoc($result);
    $salida = "OK||".$row['servicio_id']."||".$row['equipo_id']."||".$row['serie'];
  }
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}

if("saveServiceEquipmentNew" == $_POST["action"]){
  $db_catalogos = condb_catalogos();
  $db_modulos   = condb_modulos();
  $db_usuarios  = condb_usuarios();

  log_write("DEBUG: SAVE-SERVICE-EQUIPMENT: Se va a guardar un equipo",4);

  $option   = isset($_POST['option']) ? $_POST['option'] : -1;
  
  $serEquId  = isset($_POST['serviceEquipId']) ? $_POST['serviceEquipId'] : -1;
  $serviceId = isset($_POST['serviceId']) ? $_POST['serviceId'] : -1;   
  $equipo    = isset($_POST['serEquip']) ? $_POST['serEquip'] : -1;
  $serie     = isset($_POST['serEquipNumSerie']) ? $_POST['serEquipNumSerie'] : "";
  
  if(0 == $option){
    log_write("DEBUG: SAVE-SERVICE-EQUIPMENT: Se va a guardar un equipo nuevo",4);
    $query = "INSERT INTO rel_servicio_equipo(servicio_id,equipo_id,serie) VALUES(".$serviceId.",".$equipo.",'".$serie."')";  
    
    $result = mysqli_query($db_modulos,$query);
    log_write("ERROR: SAVE-SERVICE-EQUIPMENT: ".$query,4);
    
    if(!$result){
      $salida = "ERROR||Ocurrió un problema al insertarse los datos del equipo";
      log_write("ERROR: SAVE-SERVICE-EQUIPMENT: Ocurrió un problema al insertarse los datos del equipo",4);
    }
    else{
      $salida = "OK||Los datos se han almacenado con éxito";
      log_write("OK: SAVE-SERVICE-EQUIPMENT: Se guardaron los datos del equipo",4);
      writeOnJournal($usuarioId,"Ha agregado un equipo con el id [".mysqli_insert_id($db_modulos)."] para el servicio con id ".$serviceId);
    }
  }
  if(1 == $option){
    log_write("DEBUG: SAVE-SERVICE-EQUIPMENT: Se va a actualizar un equipo existente",4);
    $query = "UPDATE rel_servicio_equipo SET servicio_id=".$serviceId.",equipo_id=".$equipo.",serie='".$serie."' WHERE relacion_id=".$serEquId;
    
    $result = mysqli_query($db_modulos,$query);
    log_write("ERROR: SAVE-SERVICE-EQUIPMENT: ".$query,4);
    
    if(!$result){
      $salida = "ERROR||Ocurrió un problema al actualizarse los datos del equipo";
      log_write("ERROR: SAVE-SERVICE-EQUIPMENT: Ocurrió un actualizarse al insertarse los datos del equipo",4);
    }
    else{
      $salida = "OK||Los datos se han almacenado con éxito";
      log_write("OK: SAVE-SERVICE-EQUIPMENT: Se guardaron los datos del equipo",4);
      writeOnJournal($usuarioId,"Ha actualizado los datos del equipo con el id ".$serEquId." servicio con id ".$serviceId);
    }
  }

  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}

if("deleteServiceEquipment" == $_POST["action"]){
  $db_catalogos = condb_catalogos();
  $db_modulos   = condb_modulos();
  $db_usuarios  = condb_usuarios();

  log_write("DEBUG: DELETE-SERVICE-EQUIPMENT: Se va a eliminar una cotización",4);

  $serEquId  = isset($_POST['serEquipId']) ? $_POST['serEquipId'] : -1;
  $serviceId = isset($_POST['serviceId']) ? $_POST['serviceId'] : -1; 
  
  $query = "DELETE FROM rel_servicio_equipo WHERE relacion_id=".$serEquId;
  
  $result = mysqli_query($db_modulos,$query);
  
  log_write("DEBUG: DELETE-SERVICE-EQUIPMENT: ".$query,4);
  
  if(-1 == $serEquId){
    $salida = "ERROR||No se pudo recibir el id del equipo para su eliminación";
    goto delSerEqua;
  }
  
  if(!$result){
    log_write("ERROR: DELETE-SERVICE-EQUIPMENT: No se eliminó el equipo",4);
    $salida = "ERROR||Ocurrió un problema al intentar eliminarse el equipo asociado al servicio";
  }
  else{
    log_write("OK: DELETE-SERVICE-EQUIPMENT: Se eliminó el equipo con id [".$serEquId."]",4);
    writeOnJournal($usuarioId,"Ha eliminado el equipo con id [".$serEquId."] del servicio con id [".$serviceId."]");
    $salida = "OK||El equipo se ha eliminado con éxito";
  } 
  delSerEqua:
  mysqli_close($db_catalogos);
  mysqli_close($db_modulos);
  mysqli_close($db_usuarios);
  echo $salida;
}

/*************Fin de sección*****************/

/*************Sección de gestión de contratos*****************/
if("saveContract"==$_POST['action']){ 
  $db_modulos   = condb_modulos();
  
  $cotId  = isset($_POST["cotId"])  ? decrypt($_POST["cotId"]) :-1;
  $orgId  = isset($_POST["orgId"])  ? $_POST["orgId"] :-1;
  $conSer = isset($_POST["conSer"]) ? $_POST["conSer"] :-1;
  $conCon  = isset($_POST["conCon"]) ? json_decode($_POST["conCon"],true) : -1;
  $conSit = isset($_POST["conSit"]) ? $_POST["conSit"]  :-1;
  $conNot = isset($_POST["conNot"]) ? $_POST["conNot"]  :"";
  $orgType = isset($_POST["orgType"]) ? $_POST["orgType"]  :-1;
  $conAct = isset($_POST["conOpt"]) ? $_POST["conOpt"]  :-1; //0 marca contrato como firmado 1 solo lo actualiza
  $fecAct = isset($_POST["fecAct"]) ? $_POST["fecAct"]  :"0000-00-00 00:00:00";
  $repId = isset($_POST["repId"]) ? $_POST["repId"] :-1;
  
  $conCon1 = isset($conCon[0]) ? decrypt($conCon[0]) : 0;
  $conCon2 = isset($conCon[1]) ? decrypt($conCon[1]) : 0;
  //$conAct = 0;
  
  log_write("ERROR: SAVE-CONTRACT: Se guardará un contrato nuevo",4);
  
  log_write("DEBUG: SAVE-CONTRACT: cotId".$cotId,4);
  log_write("DEBUG: SAVE-CONTRACT: orgId".$orgId,4);
  log_write("DEBUG: SAVE-CONTRACT: conSer".$conSer,4);
  log_write("DEBUG: SAVE-CONTRACT: conCon".print_r($conCon,true),4);
  log_write("DEBUG: SAVE-CONTRACT: orgType".$orgType,4);
  log_write("DEBUG: SAVE-CONTRACT: action ".$conAct,4); 
  
  $fechaNow = date("Y-m-d H:i:s");
  $query = "";
  $contCve = "0000000000";
  
  switch($orgType){
    case 0:
      $name = "prospecto";
      $tableName = "prospectos";
      $fieldId = "prospecto_id";
    break;
    case 1:
      $name = "cliente";
      $tableName = "clientes";
      $fieldId = "cliente_id";
    break;
    default:
      $tableName = "";
    break; 
  }
  
  if(2!=$orgType)
    $conSit = decrypt($_POST["conSit"]);
    
  if(0==$conAct||2==$conAct){
    $queryLoc = "SELECT cve_municipio, num_edo, estado_id ".
                "FROM ".$cfgTableNameCat.".cat_municipios AS catmun, ".
                        $cfgTableNameMod.".".$tableName." cli ".
                "WHERE UPPER(catmun.municipio) LIKE UPPER(cli.municipio) ".
                "AND ".$fieldId." = ".$orgId;
                
    $resultLoc = mysqli_query($db_modulos,$queryLoc);  
    
    if(!$resultLoc){
      $salida = "ERROR||Ocurrió un error al consultar la ubicación del ".$name;
    }     
    else{
      if(0>=mysqli_num_rows($resultLoc)){
        $salida = "ERROR||No se encontró información del municipio. La clave de contrato no puede generarse. Verifica que el campo municipio del ".$name." exista o se encuentre correctamente capturado";
        goto savecontracta;
      }
      else{
        $rowLoc = mysqli_fetch_assoc($resultLoc);
        $contCve = $rowLoc['num_edo'].$rowLoc['cve_municipio']; 
        $contUbi = $rowLoc['estado_id'];
      }
    }  
  }  
  
  if(0==$conAct){ //guardar contrato nuevo
    $query = "INSERT INTO contratos(cotizacion_id,representante_id,fecha_activacion,contrato_preclave,contrato_ubicacion,tipo_movimiento,contacto_id1,contacto_id2,contrato_notas,usuario_alta,fecha_alta) ".
           "VALUES($cotId,$repId,'$fecAct','$contCve',$contUbi,$conSer,".$conCon1.",".$conCon2.",'$conNot',$usuarioId,'$fechaNow')";
           
    $query2 = "UPDATE cotizaciones SET contrato=4 WHERE cotizacion_id=".$cotId;
    
    $result2 = mysqli_query($db_modulos,$query2);
  }
  elseif(1==$conAct){ //guardar contrato existente
    $query = "UPDATE contratos SET contrato_notas='".$conNot."', representante_id = ".$repId.", fecha_activacion='".$fecAct."', contacto_id1=".$conCon1.", contacto_id2=".$conCon2.", tipo_movimiento=".$conSer." WHERE cotizacion_id = ".$cotId;
  }   
  elseif(2==$conAct){ //marcar como firmado
    $queryCons = "SELECT contrato_id FROM contratos WHERE cotizacion_id = ".$cotId;
    
    if("0000-00-00 00:00:00"==$fecAct)
      $fecha = date("Y-m-d H:i:s");
    else
      $fecha = $fecAct;
    
    $resultCons = mysqli_query($db_modulos,$queryCons);
    
    if(!$resultCons||0==mysqli_num_rows($resultCons)){
      $query = "INSERT INTO contratos(cotizacion_id,representante_id,fecha_activacion,contrato_preclave,contrato_ubicacion,tipo_movimiento,contacto_id1,contacto_id2,contrato_notas,usuario_alta,fecha_alta) ".
               "VALUES($cotId,$repId,'$fecha','$contCve',$contUbi,$conSer,".$conCon1.",".$conCon2.",'$conNot',$usuarioId,'$fechaNow')";
    }
    else{
      $rowCon = mysqli_fetch_assoc($resultCons);
      $query = "UPDATE contratos SET fecha_activacion='".$fecha."', representante_id = ".$repId.", contrato_notas='".$conNot."', contacto_id1=".$conCon1.", contacto_id2=".$conCon2.", tipo_movimiento=".$conSer." WHERE contrato_id = ".$rowCon['contrato_id'];
    }
    
    $query2 = "UPDATE cotizaciones SET contrato=1 WHERE cotizacion_id=".$cotId;
    
    $result2 = mysqli_query($db_modulos,$query2);
  } 
  
  log_write("OK: SAVE-CONTRACT: query ".$query,4);    
  log_write("OK: SAVE-CONTRACT: query2 ".$query2,4); 
  
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $salida = "ERROR||ocurrió un problema al guardarse el registro de contrato";
    log_write("ERROR: SAVE-CONTRACT: ocurrió un problema al guardarse el registro de contrato ".mysqli_error($db_modulos),4);
  }
  else{
    $newConId = mysqli_insert_id($db_modulos);
    
    if(2==$conAct||0==$conAct){
      if(!$result2){
        $salida = "ERROR||ocurrió un problema al guardarse el contrato";
        log_write("ERROR: SAVE-CONTRACT: Ocurrió un problema al guardarse el contrato".mysqli_error($db_modulos),4);
      }
      else{
      
        if(2==$conAct)
          $result3 = addContractAttach($newConId,$cotId,$usuarioId,1); //el 1 significa que es una cotización madre (se la cual se generó el contrato)
        elseif(0==$conAct)
          $result3 = addContractAttach($newConId,$cotId,$usuarioId,4);
          
        if(!$result3){
          $salida = "ERROR||Ocurrió un problema al insertarse el anexo de contrato";
          log_write("ERROR: SAVE-CONTRACT: Ocurrió un problema al insertarse el anexo de contrato ".mysqli_error($db_modulos),4);
        }
        else{
          if(0==$conAct){
            writeOnJournal($usuarioId,"Ha asociado la cotización ID [$cotId] como contrato ID [$newConId]");
            log_write("OK: SAVE-CONTRACT: Se ha guardado el contrato [$newConId]",4);
            $salida = "OK||Se ha guardado el contrato";
          }
          elseif(2==$conAct){
            writeOnJournal($usuarioId,"Ha asociado la cotización ID [$cotId] como contrato firmado ID [$newConId]");
            log_write("OK: SAVE-CONTRACT: Se ha marcado el contrato com firmado [$newConId]",4);
            $salida = "OK||Se ha marcado el contrato com firmado";
          }
        }
      } 
    }
    log_write("OK: SAVE-CONTRACT: Se han guardado los datos del contrato [$newConId]",4);
    $salida = "OK||Se han guardado los datos del contrato"; 
  }
  savecontracta:
  mysqli_close($db_modulos);
  echo $salida;
}
/*************Fin de sección*****************/

/*************Sección de gestión de tickets*****************/

if("getTicketList" == $_POST["action"]){

  $db_catalogos = condb_catalogos();
  $db_modulos   = condb_modulos();
  $db_usuarios  = condb_usuarios();
  
  $orgType = isset($_POST['orgType']) ? $_POST['orgType'] : -1;  
  $orgId   = isset($_POST['orgId'])   ? $_POST['orgId']   : -1;
  
  if(1==$orgType){
    $query = "SELECT tic.*, cli.nombre_comercial, cli.cliente_id, pri.ticket_prioridad, sla.ticket_sla, sla.sla_horas ".
             "FROM ".$cfgTableNameMod.".tickets AS tic, ".$cfgTableNameMod.".clientes AS cli, ".$cfgTableNameCat.".cat_ticket_prioridad AS pri, ".$cfgTableNameCat.".cat_ticket_sla AS sla ".
             "WHERE tic.cliente_id=".$orgId." AND cli.cliente_id=tic.cliente_id AND sla.ticket_sla_id=tic.ticket_sla_id AND pri.ticket_prioridad_id=tic.prioridad_id AND cli.cliente_id>0";
  }         
  elseif(2==$orgType){
    $query = "SELECT tic.*, cli.cliente_id, sit.nombre, pri.ticket_prioridad, sla.ticket_sla, sla.sla_horas ".
             "FROM ".$cfgTableNameMod.".tickets AS tic, ".$cfgTableNameMod.".clientes AS cli, ".$cfgTableNameMod.".sitios AS sit, ".$cfgTableNameCat.".cat_ticket_prioridad AS pri, ".$cfgTableNameCat.".cat_ticket_sla AS sla ".
             "WHERE tic.sitio_id=".$orgId." AND cli.cliente_id=tic.cliente_id AND sit.sitio_id=tic.sitio_id AND sla.ticket_sla_id=tic.ticket_sla_id AND pri.ticket_prioridad_id=tic.prioridad_id AND cli.cliente_id>0";
  } 
  
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){    
      $salida = "ERROR||Ocurrió un problema al obtenerse la lista de tickets";
  }
  else{
    $salida = "OK||";
    if(0==mysqli_num_rows($result)){
      if(1==$orgType)
        $salida .= "No existen registros disponibles. Podrá agregar uno a cualquiera de sus sitios haciendo clic en <strong>Agregar Ticket</strong> desde el modulo de sitios";
      elseif(2==$orgType)
        $salida .= "No existen registros disponibles haga click en <strong>Agregar Ticket</strong> para añadir uno nuevo";
    }
    else{
      $salida .= "<div class=\"table-responsive\"><table id=\"tbl_ticketList\" class=\"table table-striped table-condensed\">\n".
                "  <thead>\n".
                "    <tr>\n".
                "      <td class=\"hidden-xs\"><b>Ticket</b></td>\n".
                "      <td><b>Fecha Limite</b></td>\n".
                "      <td><b>Asunto</b></td>\n".
                "      <td><b>SLA</b></td>\n".
                "      <td><b>De</b></td>\n".
                "      <td><b>Prioridad</b></td>\n".
                "      <td><b>Acciones</b></td>\n".
                "    </tr>\n".
                "  </thead>\n".
                "  <tbody>";
      while($row = mysqli_fetch_assoc($result)){
        $cliId = $row['cliente_id'];
        $salida .= "  <tr>\n".
                   "    <td class=\"hidden-xs\">".$row['ticket_id']."</td>\n"; 
                   
        $fechaLim = strtotime($row['fecha_limite']);     
        $fechaNow = date("Y-m-d H:i:s");
        
        $tiempoRes = round(($fechaLim - strtotime($fechaNow))/3600);
        
        $interUnit = (int)$row['sla_horas']/3;
        
        if((0>=$tiempoRes)&&(5!=$row['estatus'])){
          $salida .= "    <td>".$row['fecha_limite']."<span class=\"glyphicon glyphicon-alert\" style=\"color:red\" title=\"Atrasado\"></span></td>\n";
        }
        else if(($interUnit>=$tiempoRes)&&(0<$tiempoRes)&&(5!=$row['estatus'])){
          $salida .= "    <td>".$row['fecha_limite']."<span class=\"glyphicon glyphicon-alert\" style=\"color:orange\" title=\"Próximo a atrasarse\"></span></td>\n";
        }
        else if(5==$row['estatus']){
          $salida .= "    <td>".$row['fecha_limite']."<span class=\"glyphicon glyphicon-ok\" style=\"color:green\" title=\"Ticket cerrado\"></span></td>\n";
        }
        else{
          $salida .= "    <td>".$row['fecha_limite']."</td>\n";
        }
      if(1==$orgType)
        $nombre = $row['nombre_comercial'];
      if(2==$orgType)
        $nombre = $row['nombre'];
        
        $salida .= "    <td>".$row['titulo']."</td>\n".
                   "    <td>".$row['ticket_sla']."</td>\n".
                   "    <td>".$nombre."</td>\n".  
                   "    <td>".$row['ticket_prioridad']."</td>\n".
                   "    <td>".                
                   "        <button type=\"button\" id=\"btn_ticketDetails_".$row['ticket_id']."\" title=\"Ver Detalles\" class=\"btn btn-xs btn-primary btn-responsive\" onClick=\"goToTicketDetails('".encrypt($row['ticket_id'])."');\"><span class=\"glyphicon glyphicon-list\"></span></button>";
                   
        if(hasPermission(6,'d')){     
          $salida.= "        <button type=\"button\" id=\"btn_ticketDelete_".$row['ticket_id']."\" title=\"Eliminar Ticket\" class=\"btn btn-xs btn-danger btn-responsive\" onClick=\"deleteTicket('".encrypt($row['ticket_id'])."',".$orgType.");\"><span class=\"glyphicon glyphicon-remove\"></span></button>";
        }
        $salida.=  "    </td>\n".  
                   "  </tr>\n";
      }
      $salida .= "  </tbody>\n".
                 "</table></div>\n";
      //$salida .= "<input type=\"hidden\" id=\"hdn_cliId\" name=\"hdn_cliId\" class=\"form-control\" value=".$cliId.">\n"; 
    }
  }  
  
        mysqli_close($db_catalogos);
        mysqli_close($db_modulos);
        mysqli_close($db_usuarios); 
        echo $salida;
}


if("saveTicketNew" == $_POST["action"]){
  $db_catalogos = condb_catalogos();
  $db_modulos   = condb_modulos();
  $db_usuarios  = condb_usuarios();
  
  log_write("DEBUG: SAVE-TICKET-NEW: Se va a insertar un ticket de forma manual",8);

  $tickId  = isset($_POST['tickId']) ? decrypt($_POST['tickId']) : -1;
  $option  = isset($_POST['option']) ? $_POST['option'] : -1;
  $cliId   = isset($_POST['cliId']) ? $_POST['cliId'] : -1;
  $siteId  = isset($_POST['siteId']) ? $_POST['siteId'] : -1;
  
  $ticketOrg          = isset($_POST['ticketOrg'])           ? $_POST['ticketOrg'] : -1;
  $ticketTopic        = isset($_POST['ticketTopic'])         ? $_POST['ticketTopic'] : -1;
  $ticketDepartment   = isset($_POST['ticketDepartment'])    ? $_POST['ticketDepartment'] : -1;
  $ticketSla          = isset($_POST['ticketSla'])           ? $_POST['ticketSla'] : -1;
  $tickAssign         = isset($_POST['tickAssign'])          ? $_POST['tickAssign'] : -1;
  $tickTitle          = isset($_POST['tickTitle'])           ? $_POST['tickTitle'] : "";
  $tickDetails        = isset($_POST['tickDetails'])         ? $_POST['tickDetails'] : "";
  $ticketPrior        = isset($_POST['ticketPrior'])         ? $_POST['ticketPrior'] : -1;
  $tickTopicOther     = isset($_POST['tickTopicOther'])      ? $_POST['tickTopicOther'] : "";
  $tickOrgOther       = isset($_POST['tickOrgOther'])        ? $_POST['tickOrgOther'] : "";
  $ticketAssign       = isset($_POST['ticketReassign'])      ? $_POST['ticketReassign'] : -1;
  $ticketAssocName    = isset($_POST['tickAssocName'])       ? $_POST['tickAssocName'] : "";
  $ticketAssocMail    = isset($_POST['tickAssocEmail'])      ? $_POST['tickAssocEmail'] : "";
  $ticketClientNotify = isset($_POST['ticketClientNotify'])  ? $_POST['ticketClientNotify'] : 0;
  $ticketAssocArray   = isset($_POST['ticketAssocArray'])    ? json_decode($_POST['ticketAssocArray'],true) : "";
  $ticketImageArray   = isset($_POST['ticketImgArr'])        ? json_decode($_POST['ticketImgArr'],true) : "";
  $ticketAttachArray  = isset($_POST['ticketAttachments'])   ? json_decode($_POST['ticketAttachments'],true) : "";
  
  $ticketStatus       = isset($_POST['ticketStatus'])        ? $_POST['ticketStatus'] : 1;
  $usrIdAlta          = isset($_POST['remFlag'])             ? -1 : $usuarioId;
  
  log_write("DEBUG: SAVE-TICKET-NEW: ".print_r($_POST,true),8);
    
  $fecha = date("Y-m-d H:i:s");
  $fecLim = "";

  if(-1 == $tickAssign){
    log_write("DEBUG: SAVE-TICKET-NEW: No hay usuario asignado se elegirá uno de soporte",8);
    $queryUsr = "SELECT DISTINCT usuario_id FROM rel_usuario_grupo WHERE grupo_id=6";
    $resultUsr = mysqli_query($db_usuarios,$queryUsr);
  }
  else{
    if("s"==$tickAssign[0]){
      log_write("DEBUG: SAVE-TICKET-NEW: Se asignó al miembro del staff [".$tickAssign."] ",8);
      $queryUsr = "SELECT usuario_id FROM cat_ticket_staff WHERE idstaff='".$tickAssign."'";
      $resultUsr = mysqli_query($db_catalogos,$queryUsr);
    }
    if("d"==$tickAssign[0]){
      log_write("DEBUG: SAVE-TICKET-NEW: Se asignó al grupo [".$tickAssign."] ",8);
      $queryUsr = "SELECT rel.usuario_id FROM ".$cfgTableNameUsr.".rel_usuario_grupo AS rel, ".$cfgTableNameCat.".cat_ticket_departamento AS cat WHERE cat.grupo_id=rel.grupo_id AND cat.iddepartamento='".$tickAssign."' ORDER BY rel.usuario_id ASC";
      $resultUsr = mysqli_query($db_usuarios,$queryUsr);
    }
  }
  
  $queryRr = "SELECT usuario_id,ticket_id FROM ticket_round_robin ORDER BY round_id DESC LIMIT 1"; 
  
  $querySla = "SELECT sla_horas FROM cat_ticket_sla WHERE ticket_sla_id=".$ticketSla;
  
  log_write("DEBUG: SAVE-TICKET-NEW: Query asignación ".$queryUsr,8);
  log_write("DEBUG: SAVE-TICKET-NEW: Query round robin ".$queryRr,8);
  log_write("DEBUG: SAVE-TICKET-NEW: Query sla ".$querySla,8);
  
  $resultRr = mysqli_query($db_modulos,$queryRr);
  
  $resultSla = mysqli_query($db_catalogos,$querySla);
  
  if(!$resultUsr||!$resultRr||!$resultSla){
    log_write("ERROR: SAVE-TICKET-NEW: Ocurrió un problema al obtener los datos del usuario para la asignación del ticket",8);
    $salida = "ERROR||Ocurrió un problema al obtener los datos del usuario para la asignación del ticket";
    goto saveTicketa;
  }
  else{
    log_write("DEBUG: SAVE-TICKET-NEW: Querys exitosos",8);  
    
    /*Round Robin*/
    $usrArr = array();   
    while($rowUsr = mysqli_fetch_assoc($resultUsr)){
      $usrArr[] = $rowUsr['usuario_id'];
    }
    
    log_write("DEBUG: SAVE-TICKET-NEW: RR Array: ".print_r($usrArr,true),8);
    
    $rowRr = mysqli_fetch_assoc($resultRr);
    
    $rrPos = array_search($rowRr['usuario_id'],$usrArr);
    
    if(($rrPos == (count($usrArr)-1))||(0==mysqli_num_rows($resultRr))||(1==count($usrArr))){
      $nextUsr = $usrArr[0];  
    }
    else{
      $nextUsr = $usrArr[$rrPos+1];
    }
    
    $rowSla = mysqli_fetch_row($resultSla);  
    
    log_write("DEBUG: SAVE-TICKET-NEW: Fecha Límite: ".$fecLim,8);
    
    log_write("DEBUG: SAVE-TICKET-NEW: Siguiente Usuario RR: ".$nextUsr,8);
    
    $tickDetails = str_replace("'","''",$tickDetails);
    
    /*Fin round robin*/
    
    if(0 == $option){ //guardar ticket nuevo
      log_write("DEBUG: SAVE-TICKET-NEW: Se va a guardar un ticket nuevo",8);
      
      $fecLim = date("Y-m-d H:i:s",strtotime($fecha." +".$rowSla[0]." hours"));   
      
      $queryIns = "INSERT INTO tickets(cliente_id,sitio_id,ticket_origen_id,ticket_origen_otro,ticket_sla_id,".
                                      "ticket_topic_id,ticket_topic_otro,departamento_id,".
                                      "asignadoa,fecha_limite,fecha_alta,prioridad_id,estatus,".
                                      "usuario_alta,titulo,detalles,ultima_act,ultima_act_usuario) ".
                  "VALUES(".$cliId.",".$siteId.",".$ticketOrg.",'".$tickOrgOther."',".$ticketSla.",".
                            $ticketTopic.",'".$tickTopicOther."','".$ticketDepartment."','".
                            $nextUsr."','".$fecLim."','".$fecha."',".$ticketPrior.",".$ticketStatus.",".$usrIdAlta.",'".
                            $tickTitle."','".$tickDetails."','".$fecha."',".$usrIdAlta.")";
      
      $resultIns = mysqli_query($db_modulos,$queryIns);
      
      $newTickId = mysqli_insert_id($db_modulos);
      
      log_write("DEBUG: SAVE-TICKET-NEW: Query Insert ".$queryIns,8);
      
      if(!$resultIns){
        log_write("ERROR: SAVE-TICKET-NEW: Ocurrió un problema al insertar los datos del nuevo ticket",8);
        $salida = "ERROR||Ocurrió un problema al insertar los datos del nuevo ticket ";
      }
      else{
        log_write("OK: SAVE-TICKET-NEW: Los datos del ticket se han guardado satisfactoriamente",8);
        $salida = "OK||Los datos del ticket se han guardado satisfactoriamente";
        
        insertRoundRobin($newTickId,$nextUsr);
        
        if(-1!=$usrIdAlta)
          insertTicketJournal("Nuevo Ticket por Staff",$newTickId,"Ticket creado por Staff - ".$nombre,"u".$usuarioId,3);
        else
          insertTicketJournal("Nuevo Ticket recibido via E-mail",$newTickId,"Ticket creado via Email - $ticketAssocName","c".$siteId,3);
          
        $tickJour = insertTicketJournal($tickTitle,$newTickId,$tickDetails,"c".$siteId,1);
           
        log_write("OK: SAVE-TICKET-NEW: TickJour value: ".$tickJour,8);
                      
        if(false!=$tickJour){              
          //if((-1!=$usrIdAlta&&false!==moveImgsToFtp($newTickId,$tickJour,$tickDetails,0))||(-1==$usrIdAlta&&false!==moveImgsToFtp($newTickId,$tickJour,$ticketImageArray,4))){
          if(false!==moveImgsToFtp($newTickId,$tickJour,$tickDetails,$ticketAttachArray,0)){
            sendTicketEmail($tickJour,2,"");
            sendTicketEmail($tickJour,3,"");
            if(1 == $ticketClientNotify){
              log_write("DEBUG: SAVE-TICKET-NEW: ASSOC ARRAY: ".print_r($ticketAssocArray,true),8);
              foreach($ticketAssocArray as $key => $value){
                //$tickAssocId = insertTicketAssociate($ticketAssocName,$ticketAssocMail,$newTickId,0);                
                $tickAssocId = insertTicketAssociate($value[0],$value[1],$newTickId,$value[2]);
                log_write("DEBUG: SAVE-TICKET-NEW: TickAssocId value: ".$tickAssocId,8);             
              }
              if(-1!=$usrIdAlta){
                $tickAssocArr = json_encode(array($tickAssocId));
                sendTicketEmail($tickJour,0,$tickAssocArr);
                writeOnJournal($usuarioId,"Ha insertado un ticket nuevo con con Id [".$tickId."]");               
              }
              $salida = "OK||Los datos del ticket se guardaron";                                    
            }            
          }
          else{
            $salida = "ERROR||No se trasladó el archivo al servidor remoto, no se pudo enviar la notificación por correo";
          }
                                     
        }                             
      }      
    }
    
      if(1 == $option){ //modificar ticket existente
        log_write("DEBUG: SAVE-TICKET-NEW: Se va a actualizar un ticket existente",8);
        
        $queryFec = "SELECT fecha_alta, estatus FROM tickets WHERE ticket_id=".$tickId;
        
        $resultFec = mysqli_query($db_modulos,$queryFec);
        
        if($resultFec){
          $rowFec = mysqli_fetch_assoc($resultFec);
          $fecLim = date("Y-m-d H:i:s",strtotime(date($rowFec['fecha_alta'])." +".$rowSla[0]." hours"));
          $fecField = "fecha_limite='".$fecLim."',";
          $slaField = "ticket_sla_id=".$ticketSla.",";
          
          $tiempoRes = round(($fechaLim - strtotime($fecha))/3600);          
        }
        else{
          $fecLim = "Sin cambio";
          $fecField = "";
          $slaField = "";
        }
        
        $tickDetails = str_replace("'","''",$tickDetails);
        
        $queryEdit = "UPDATE tickets SET ticket_origen_id=".$ticketOrg.",ticket_origen_otro='".$tickOrgOther."',".
                                        $slaField."ticket_topic_id=".$ticketTopic.",".
                                        "ticket_topic_otro='".$tickTopicOther."',departamento_id=".$ticketDepartment.",".
                                        "titulo='".$tickTitle."',detalles='".$tickDetails."',".$fecField.
                                        "prioridad_id=".$ticketPrior.", ultima_act_usuario=".$usuarioId;
                                        
        if((0>=$tiempoRes)&&(5!=$rowFec['estatus'])&&(7!=$rowFec['estatus'])){
          $queryEdit .= ",estatus=1";
        }                                      
        if(hasPermission(6,'t')&&1==$ticketAssign){                                 
          $queryEdit .= ",asignadoa=".$nextUsr;
        }                               
        $queryEdit .= " WHERE ticket_id=".$tickId;
                                
        $resultEdit = mysqli_query($db_modulos,$queryEdit);
        
        log_write("DEBUG: SAVE-TICKET-NEW: Query Update ".$queryEdit,8);
        
        if(!$resultEdit){
          $salida = "ERROR||Ocurrió un problema al actualizarse los datos del ticket";
          log_write("ERROR: Ocurrió un problema al actualizarse los datos del ticket",8);
          goto saveTicketa;
        }
        else{
          writeOnJournal($usuarioId,"Ha actualizado el campo origen a [".$ticketOrg."] para el ticket con Id ".$tickId);
          writeOnJournal($usuarioId,"Ha actualizado el campo origen_otro a [".$tickOrgOther."] para el ticket con Id ".$tickId);
          writeOnJournal($usuarioId,"Ha actualizado el campo sla a [".$ticketSla."] para el ticket con Id ".$tickId);
          writeOnJournal($usuarioId,"Ha actualizado el campo topic id a [".$ticketTopic."] para el ticket con Id ".$tickId);
          writeOnJournal($usuarioId,"Ha actualizado el campo topic_otro a [".$tickTopicOther."] para el ticket con Id ".$tickId);
          writeOnJournal($usuarioId,"Ha actualizado el campo departamento id a [".$ticketDepartment."] para el ticket con Id ".$tickId);
          writeOnJournal($usuarioId,"Ha actualizado el campo asignado a [".$nextUsr."] para el ticket con Id ".$tickId);
          writeOnJournal($usuarioId,"Ha actualizado el campo fecha_límite a [".$fecLim."] para el ticket con Id ".$tickId);
          writeOnJournal($usuarioId,"Ha actualizado el campo prioridad id a [".$ticketPrior."] para el ticket con Id ".$tickId);
          writeOnJournal($usuarioId,"Ha actualizado el campo titulo a [".$tickTitle."] para el ticket con Id ".$tickId);
          writeOnJournal($usuarioId,"Ha actualizado el campo detalles a [".$tickDetails."] para el ticket con Id ".$tickId);
          
          /*if(-1!=$usrIdAlta){
            sendTicketEmail($tickJour,1,"");            
          }*/       
          if(1==$ticketAssign){
            writeOnJournal($usuarioId,"Ha reasignado a [".$nextUsr."] el ticket con Id ".$tickId);
            $tickJour = insertTicketJournal("Actualización de ticket",$tickId,"Datos de ticket actualizados y Ticket reasignado a ".get_userRealName($nextUsr)." - ".$nombre,"u".$usuarioId,3);
            insertRoundRobin($tickId,$nextUsr); 
            sendTicketEmail($tickJour,4,"");
            sendTicketEmail($tickJour,3,"");
          }
          else{
            insertTicketJournal("Actualización de ticket",$tickId,"Datos de ticket actualizados - ".$nombre,"u".$usuarioId,3);
          }
          
          log_write("OK: SAVE-TICKET-NEW: Los datos del ticket se han actualizado satisfactoriamente",8);
          $salida = "OK||Los datos del ticket se han actualizado con éxito";
        }     
      }
      }//else assign
      saveTicketa:
      mysqli_close($db_catalogos);
      mysqli_close($db_modulos);
      mysqli_close($db_usuarios);
      echo $salida;
}

//Esta función ejecuta un hilo que sube los archivos adjuntos e imagenes embebidas al servidor ftp, reemplaza todas las rutas que apuntan
//a dichos archivos hacia su ubicación remota.
/*opt
0- detalles del ticket (post principal)
1- reply
2- nota interna
3- cierre/reapertura de ticket
4- dato adjunto en correo (no se toman las imágenes del cuerpo del mensaje)
*/

//VERSION ESTABLE
function moveImgsToFtp($tickId,$tickJour,$details,$attachments,$opt){
  global $cfgFtpServerLocation;
  global $cfgServerLocation;
  
  log_write("DEBUG: MOVE-IMAGES-TO-FTP: Detalles: ".$details,8);
  
  $db_modulos = condb_modulos();
  $conFtp = ftpConnectLogin();
   
  if(-1==$conFtp||-2==$conFtp){
    log_write("ERROR: MOVE-IMAGES-TO-FTP: No se estableció conexión con el servidor ftp",8);
    $resFtp = false;
  }
  else{
    $details = str_replace("''","'",$details);
  
    $dom = new domDocument;
    $dom->loadHTML($details,LIBXML_HTML_NOIMPLIED|LIBXML_HTML_NODEFDTD);
    $dom->preserveWhiteSpace = false;
    $imgs = $dom->getElementsByTagName("img");
    
    $imageArr = array();
    
    $destDir = "web/crm/docs/tickets/attachments/".$tickId."/".$tickJour;
    
    for($i=0;$i<$imgs->length;$i++){ //por cada imagen que hay  
      $source = $imgs->item($i)->getAttribute("src"); 
      
      $filePathArr = explode('/',$source);
      $fileNamePos = (count($filePathArr))-1;
      
      $filename = $filePathArr[$fileNamePos];
      
      $orgFile = "../../docs/tickets/attachments/temp/".$filename;      
      //$orgFile =  $cfgServerLocation."crm/docs/tickets/attachments/temp/".$filename;         
          
      $destFile = $destDir."/".$filename; 
      
      $destDirAbs = $cfgFtpServerLocation."crm/docs/tickets/attachments/".$tickId."/".$tickJour."/".$filename;                     
      
      log_write("DEBUG: MOVE-IMAGES-TO-FTP: Ruta origen: ".$orgFile,8);
      log_write("DEBUG: MOVE-IMAGES-TO-FTP: Ruta destino: ".$destFile,8);
      log_write("DEBUG: MOVE-IMAGES-TO-FTP: Ruta absoluta destino: ".$destDirAbs,8);
      
      $execParser = 'php ../../libs/db/ftpfilefunctions.php '.
                    '--orgdir="'.$orgFile.'" '.
                    '--destdir="'.$destFile.'" '.
                    '--action="upload" '.
                    '>/dev/null 2>&1 &';
                    
      log_write("DEBUG: DELETE-TICKET-FTP-DATA: Exec parser: ".print_r($execParser,true),8);                  
      exec($execParser,$out);   
      log_write("DEBUG: DELETE-TICKET-FTP-DATA: Exec output: ".print_r($out,true),8);
                        
      $imageArr[] = array($source,$destDirAbs,$orgFile);        
    }
    
    log_write("DEBUG: MOVE-IMAGES-TO-FTP: ftpImgArray ".print_r($imageArr,true),8);
    
    if(0<count($imageArr)){
      foreach($imageArr as $key => $value){    
        log_write("DEBUG: MOVE-IMAGES-TO-FTP: reemplazando ".$value[0]." con ".$value[1],8); 
        $details = str_replace($value[0],$value[1],$details);
        //unlink($value[2]);
      } 
         
      $resFtp = str_replace("'","''",$details); 
         
      $queryPoUp = "UPDATE ticket_bitacora_posts SET mensaje='".$resFtp."' WHERE post_id=".$tickJour;
      
      if(0==$opt){ //si es la descripción del ticket
        $queryTiUp = "UPDATE tickets SET detalles='".$resFtp."' WHERE ticket_id=".$tickId; 
        $resultTiUp = mysqli_query($db_modulos,$queryTiUp);
      }
        
      $resultPoUp = mysqli_query($db_modulos,$queryPoUp);
      
      if((0==$opt&&!$resultTiUp)||!$resultPoUp){
        log_write("DEBUG: MOVE-IMAGES-TO-FTP: No se actualizaron las referencias a las imagenes para este ticket",8);
        $salida = "ERROR||No se actualizaron las referencias a las imagenes para este ticket";
        $resFtp = false;
      }
      else{
        $resFtp = true;
      }    
    }
    
    log_write("DEBUG: MOVE-IMAGES-TO-FTP: ftpAttArray ".print_r($attachments,true),8);
    
    foreach($attachments as $key => $file){   
      $filePathArr = explode('/',$file);
      $fileNamePos = (count($filePathArr))-1;
      
      $filename = $filePathArr[$fileNamePos];
    
      //$orgFile = "../../docs/tickets/attachments/temp/".$filename; 
      $orgFile = $file;
      $destFile = $destDir."/".$filename;  
      $destDirAbs = $cfgFtpServerLocation."crm/docs/tickets/attachments/".$tickId."/".$tickJour."/".$filename; 
      
      log_write("DEBUG: MOVE-IMAGES-TO-FTP: Ruta origen: ".$orgFile,8);
      log_write("DEBUG: MOVE-IMAGES-TO-FTP: Ruta destino: ".$destFile,8);
      log_write("DEBUG: MOVE-IMAGES-TO-FTP: Ruta absoluta destino: ".$destDirAbs,8);
      
      $execParser = 'php ../../libs/db/ftpfilefunctions.php '.
                    '--orgdir="'.$orgFile.'" '.
                    '--destdir="'.$destFile.'" '.
                    '--action="upload" '.
                    '>/dev/null 2>&1 &';
                    
      log_write("DEBUG: DELETE-TICKET-FTP-DATA: Exec parser: ".print_r($execParser,true),8);                  
      exec($execParser,$out);   
      log_write("DEBUG: DELETE-TICKET-FTP-DATA: Exec output: ".print_r($out,true),8);
      
      
      $queryAtUp = "INSERT INTO ticket_adjuntos (post_id,adjunto_ruta) VALUES ('".$tickJour."','".$destDirAbs."')";
      
      $resultAtUp = mysqli_query($db_modulos,$queryAtUp);
      
      if(!$resultAtUp){
        log_write("DEBUG: MOVE-IMAGES-TO-FTP: No se agregaron las referencias a los datos adjuntos para este ticket",8);
        $salida = "ERROR||No se agregaron las referencias a los datos adjuntos para este ticket";
        $resFtp = false;
      }
      else{
        $resFtp = true;
      }
    }
    ftp_close($conFtp);
  } 
  mysqli_close($db_modulos);  
  return $resFtp; 
}

//agrega el usuario asignado a la lista de round robin
function insertRoundRobin($tickId,$usrId){
  $db_modulos = condb_modulos();  
  global $cfgLogService;
  
  $fecha = date("Y-m-d H:i:s");
  $query = "INSERT INTO ticket_round_robin(ticket_id,usuario_id,fecha_hora) VALUES(".$tickId.",".$usrId.",'".$fecha."')";
  
  $result = mysqli_query($db_modulos,$query);
  
  log_write("DEBUG: INSERT-ROUND-ROBIN: ".$query,8);
  
  if(!$result){
    log_write("ERROR: INSERT-ROUND-ROBIN: Ocurrió un error al guardar el registro de Round Robin",8);
    $res = false;
  }
  else{
    log_write("OK: INSERT-ROUND-ROBIN: Se registró el usuario en el Round Robin",8);
    $res = true;
  }  
  mysqli_close($db_modulos);
  return $res; 
}

function insertTicketJournal($subject,$ticketId,$msg,$agreg,$postType){
  global $cfgLogService;
  log_write("DEBUG: INSERT-TICKET-JOURNAL: Se insertará datos en la bitácora",8);
  /* posttype
  1.- Post cliente
  2.- Post usuario
  3.- Nota Interna  
  4.- Nota sistema      
  */
  $db_modulos = condb_modulos();  
  $fecha = date("Y-m-d H:i:s");
  
  $query = "INSERT INTO ticket_bitacora_posts(fecha_alta,asunto,ticket_id,mensaje,agregado_por,bitacora_post_tipo) ".
           "VALUES ('".$fecha."','".$subject."',".$ticketId.",'".$msg."','".$agreg."',".$postType.")";
  
  $result = mysqli_query($db_modulos,$query);
  
  log_write("DEBUG: INSERT-TICKET-JOURNAL: ".$query,8);
  
  if(!$result){
    log_write("ERROR: INSERT-TICKET-JOURNAL: No se escribió la info en la bitácora",8);
    $res = false;
  }
  else{
    log_write("OK: INSERT-TICKET-JOURNAL: Se insertaron los datos en la bitácora",8);
    $res = mysqli_insert_id($db_modulos);
  }
    
  mysqli_close($db_modulos);
  return $res;
}

function insertTicketAssociate($name,$email,$ticketId,$origen){
  $db_modulos = condb_modulos();
  global $cfgLogService;
  
  log_write("DEBUG: INSERT-TICKET-ASSOCIATE: Se insertará un asociado",8);
  
  $query = "INSERT INTO ticket_asociados(asociado_nombre,asociado_email,ticket_id,es_mail_origen) VALUES('".$name."','".$email."',".$ticketId.",".$origen.")";
  $result = mysqli_query($db_modulos,$query);
  
  log_write("DEBUG: INSERT-TICKET-ASSOCIATE: ".$query,8);
  
  if(!$result){
    log_write("ERROR: INSERT-TICKET-ASSOCIATE: No se escribieron los datos de asociado",8);
    error_log("ERROR ".$query);
    $res = false;
  }
  else{
    log_write("OK: INSERT-TICKET-ASSOCIATE: Se escribieron los datos de asociado",8);
    $res = mysqli_insert_id($db_modulos);
  }  
  mysqli_close($db_modulos);
  return $res;
}

//borra un ticket sus posts asociados u ejecuta la función que manda a eliminar los archivos en el servidor ftp
if("deleteTicket" == $_POST["action"]){
  $db_modulos   = condb_modulos();
  
  log_write("DEBUG: DELETE-TICKET: Se va a eliminar un ticket",8);

  $tickId  = isset($_POST['ticketId']) ? decrypt($_POST['ticketId']) : -1;

  $query = "DELETE FROM tickets WHERE ticket_id=".$tickId;
  
  $result = mysqli_query($db_modulos,$query);
  
  log_write("DEBUG: DELETE-TICKET: Query: ".$query,8);
  
  if(!$result){
    log_write("ERROR: DELETE-TICKET: Ocurrió un problema al borrarse el ticket",8);
    $salida = "ERROR||Ocurrió un problema al borrarse el ticket seleccionado";
  }
  else{
    deleteTicketFtpData($tickId);
    log_write("OK: DELETE-TICKET: El ticket ha sido borrado con éxito",8);
    $salida = "OK||El ticket ha sido borrado con éxito";
  } 
  mysqli_close($db_modulos);
  echo $salida;
}

//funcion que ejecuta el hilo de eliminación de los archivos de un ticket en el servidor ftp
function deleteTicketFtpData($tickId){
  global $cfgLogService;
  log_write("DEBUG: DELETE-TICKET-FTP-DATA: Se ejecutará el hilo TicketId[".$tickId."]",8);
  
  $execParser = 'php ../../libs/db/ftpfilefunctions.php '.
                '--dir="/web/crm/docs/tickets/attachments/'.$tickId.'" '.
                '--action="delete" '.                 
                '>/dev/null 2>&1 &';
                
  log_write("DEBUG: DELETE-TICKET-FTP-DATA: Exec parser: ".print_r($execParser,true),8);                  
  exec($execParser,$out);   
  log_write("DEBUG: DELETE-TICKET-FTP-DATA: Exec output: ".print_r($out,true),8);
  
}
/*************Fin de sección*****************/
// }
//fin else sesión
?>