<?php //-*-mode: php-*-
error_reporting(E_ALL);
$cfgDbServer = array();
// //valores para la gestión de datos en la base de datos definitiva (descomentar estos antes de subir los archivos al server)
// $cfgTableNameMod = 'coecrm_modulos';
// $cfgTableNameUsr = 'coecrm_usuarios';
// $cfgTableNameCat = 'coecrm_catalogos';
// $cfgTableNameOmn = 'bdOMNI';
// $cfgDbServer['location'] = '10.1.0.109';
// $cfgDbServer['user'] = 'supervisor';
// $cfgDbServer['pass'] = 'Kb.204.h32017';
// $cfgServerLocationAbs = '/var/www/html/crm/';
// $cfgServerLocation = 'https://crm.coeficiente.mx/';  //el definitivo con dominio
// //$cfgServerLocation = "http://10.1.0.109/";

//prubas compu andres
//valores para pruebas locales en las bases de datos de pruebas (comentar estos antes de subir los archivos al server)
$cfgTableNameMod = 'coecrm_modulos';
$cfgTableNameUsr = 'coecrm_usuarios';
$cfgTableNameCat = 'coecrm_catalogos';
$cfgTableNameOmn = 'bdOMNI';
$cfgDbServer['location'] = '10.1.0.20';
$cfgDbServer['user'] = 'desarrollo';
$cfgDbServer['pass'] = 'Pa55w0rd!crm';
$cfgServerLocationAbs = '/var/www/crm/';
$cfgServerLocation = "http://localhost/";  //para pruebas locales
$cfgServerLocation = "http://10.1.0.49/";  //server de pruebas

//servidor ftp
$cfgFtpServerLocation = "http://10.1.0.10/";

function ftpConnectLogin(){
  $ftpResult = "";
  $ftp_conn = ftp_connect('10.1.0.10');
  
  if(!$ftp_conn){
    $ftpResult = -1;
  }
  else{    
    $ftp_login = ftp_login($ftp_conn,'coecrm_ftp','c03_4dm1n');
    if(!$ftp_login){     
      $ftpResult = -2;
    } 
    else 
      $ftpResult = $ftp_conn;  
  }
  return $ftpResult;    
}

function condb_modulos(){
  global $cfgTableNameMod;
  global $cfgDbServer;
  $db_modulos = mysqli_connect($cfgDbServer['location'], $cfgDbServer['user'], $cfgDbServer['pass'], $cfgTableNameMod);
  mysqli_query($db_modulos,"SET NAMES 'utf8'");
  return $db_modulos; 
}

function condb_usuarios(){
  global $cfgTableNameUsr;
  global $cfgDbServer;
  $db_usuarios = mysqli_connect($cfgDbServer['location'], $cfgDbServer['user'], $cfgDbServer['pass'], $cfgTableNameUsr);
  mysqli_query($db_usuarios,"SET NAMES 'utf8'");
  return $db_usuarios; 
}

function condb_catalogos(){
  global $cfgTableNameCat;
  global $cfgDbServer;
  $db_catalogos = mysqli_connect($cfgDbServer['location'], $cfgDbServer['user'], $cfgDbServer['pass'], $cfgTableNameCat);
  mysqli_query($db_catalogos,"SET NAMES 'utf8'");
  return $db_catalogos; 
}

function condb_onmidb(){
  global $cfgTableNameOmn;
  global $cfgDbServer;
  $db_omni = mysqli_connect($cfgDbServer['location'], $cfgDbServer['user'], $cfgDbServer['pass'], $cfgTableNameOmn);
  mysqli_query($db_omni,"SET NAMES 'utf8'");
  return $db_omni; 
}

?>
