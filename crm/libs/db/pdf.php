<?php
include_once "common.php";
   
class PDF extends FPDF {
    public $numeimg,$row4,$row666;
    public $numeroimg,$fter;
	public $img,$total,$subtotal;
	public $numerocomoparametro;

    function printCotConcept($cant, $desc, $pre1, $pre2) {
        $posx = $this->GetX();
        $posy = $this->GetY();
		
        $this->SetXY($posx + 20.5, $posy); //se recorre a la posición de la segunda columna
        $this->SetFont('Arial', 'I', 9);
        $this->MultiCell(95.5, 5, utf8_decode($desc), 'LRTB', 'L', false); //Imprime descripción
		
        $this->SetFont('Arial', '', 9);
        $cellHeight = $this->GetY() - $posy; //obtiene la altura de la celda de descripcion porque es dinámica
		
        $this->SetXY($posx + 95.5 + 20.5, $posy); //se posiciona a la derecha de la celda de descripción
        //imprime celdas de precios
        //$this->Cell(37,$cellHeight, utf8_decode($pre1),'LRTB', 0 , 'C', false);
        $this->Cell(74, $cellHeight, utf8_decode($pre2), 'LRTB', 0, 'C', false);
        //vuelve a la primera posición de la fila e imprime la primera celda (cantidad)
        $this->SetXY($posx, $posy);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(20.5, $cellHeight, $cant, 'LRTB', 1, 'C', false);
    }
    function printCotNote($name, $desc) {
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(37, 5, $name . ":", 'LRTB', 0, 'L', false);
        $this->SetFont('Arial', 'IB', 8);
        $this->Cell(153, 5, utf8_decode($desc), 'LRTB', 1, 'L', false);
	}
	
	function printPriceHeader($dataArrr){
        $fecha = date("Y-m-d H:i:s");
        $this->SetXY(10,10);
		$this->Image('../../img/cotizacion/hoja_blanca1.jpg', -10, 15, 220, 284);
		//$this->Image('../../img/cotizacion/hoja-blanca1.jpg', -10, 15, 220, 284);
		if (file_exists($dataArrr['logo_ruta'])){
			$this->Image(utf8_decode($dataArrr['logo_ruta']), 30, 18, 40, 25);
		}
	    //$this->Image(utf8_decode($dataArr['logo_ruta']), 30, 18, 40, 25);
		//$this->Image('../../docs/scan/prospectdocs/logo.jpg', 30, 18, 40, 25);
		
		$this->SetFont('Arial', 'B', 10);
        $this->SetX(10);
	    $this->SetFont('Arial','B',8);
		$this->SetX(10);
		$this->Cell(0, 54, utf8_decode('Atencion: '.$dataArrr['nombre'] . " " . $dataArrr['apellidos']), '', 2, 'L', false);
		$this->Cell(80,-40,utf8_decode($dataArrr['nombre']),0,0,'L');
        $this->MultiCell(0, 4, utf8_decode("PRESENTE"), '', 'L', false);
        $this->SetFont('Arial','',10);
		$this->MultiCell(190, 4, utf8_decode("De antemano queremos agradecer la oportunidad que nos brinda para presentarle nuestra propuesta, estamos seguros que podrá encontrar en nuestro servicio una atención personalizada inmejorable, ya que nuestra función principal es mantener los estándares más altos de calidad, enfocados al servicio al cliente, permitiéndonos crecer en conjunto con ustedes generando relaciones de negocio a largo plazo cubriendo sus más altas expectativas. Esperamos que esta información sea de su utilidad quedando a sus órdenes para cualquier requerimiento o aclaración."),'', 'L', false);
		$this->SetFont('Arial','',8);
		$this->Cell(0,10,'Atentamente',5,5,'C');
		$this->SetFont('Arial','B',9);
		$this->Cell(0,3,'Ing. Jorge Salde',5,5,'C');
		$this->SetFont('Arial','',8);
		$this->Cell(0,6,'Director',5,5,'C');
		$this->SetFont('Arial','B',9);
		$this->Cell(0,6,'Correo Electronico                        Telefonio: (33) 2282 8282',0,0,'C');
		$this->Image('../../img/cotizacion/menu.jpg',15,130,180,120);
	}
	
	function segundahojapdf($numero){
        $this->SetXY(10,10);
	   // echo $numero;
		while($row5 = mysqli_fetch_assoc($numero)) {
	
		$this->Image('../../img/cotizacion/hoja_blanca.jpg', -10, 15, 220, 285);
		      switch($row5['categoria_id']) {
							case '1':
							$this->Image('../../img/cotizacion/internet1.jpg', 10, 55, 190, 99);
							break;
							
							case '2':
							$this->Image('../../img/cotizacion/telefonia.jpg', 10, 165, 190, 99);
							break;
							
							case '3':
							//$this->addPage();
							$this->Image('../../img/cotizacion/cloud_services.jpg', 10, 55, 190, 99);
							break;
							
							case '4':		
                        	$this->Image('../../img/cotizacion/ingenieria.jpg', 10, 165, 190, 99);
							break;
							
							case '5':
							//$this->addPage();
							$this->Image('../../img/cotizacion/metroethernet.jpg', 10, 555, 190, 99);
						    break;
							
							case '6':
							$this->Image('../../img/cotizacion/servicios_administrados.jpg', 10, 165, 190, 99);
							break;
					}	
	      }
		   	//$this->terserahojapdf();
	}
		
	function terserahojapdf(){
		$this->SetXY(10,10);
		$this->AddPage();
		$this->Image('../../img/cotizacion/hoja_blanca.jpg', -10, 15, 220, 285);			
		$this->Image('../../img/cotizacion/clientes.jpg', 10, 45, 180, 99);
		$this->Image('../../img/cotizacion/noc.jpg', 10, 165, 180, 109);	       
		// $this->setY(10,10);
	}
	
	function printPriceHeader1($dataArr, $datos){
	   //$this->terserahojapdf();
		$fecha = date("Y-m-d H:i:s");
		$this->AddPage();			
	  //var_dump($datos);
		//echo $datos;
		//$this->SetY(-560);
		//$this->Image('../../img/cotizacion/telefonia.jpg', 10, 45, 190, 99);
		//tersera hoja		                  //   x   y   w    h
	    //$this->AddPage();		
		
    	//tersera hoja del PDF
		
        //$this->AddPage();		
		//$this->Image('../../img/cotizacion/hoja-blanca.jpg', -10, 15, 220, 285);
		//$this->SetXY(110, 110);
		
        $this->Image('../../img/cotizacion/hoja_blanca1.jpg', -10, 15, 220, 285);
        $this->Ln(30);
        $this->SetTextColor(0);	
        $this->SetFont('Arial', 'B', 10);
        $this->SetX(20);			
        $this->Cell(0, 4, utf8_decode($dataArr['nombre'] . " " . $dataArr['apellidos']), '', 2, 'L', false);
        $this->Ln(3);
        $this->SetX(20);
        $this->SetFont('Arial', '', 10);
        $this->MultiCell(190, 4, utf8_decode("Agradeciendo la oportunidad que nos brinda de ponernos a sus órdenes y colaborar para la mejora de sus 
		servicios de telecomunicaciones, ponemos a su disposición la siguiente cotización:"), '', 'L', false);
        $this->Ln(12);
        $this->SetFillColor(2, 12, 117);
        $this->SetTextColor(255, 255, 255);
        $this->SetFont('Arial', 'B', 10);
		
        $this->Cell(0, 4,utf8_decode("Internet | Telefonía | Cloud PBX | Web Hosting & VPS | Security Operations Center | LAN to LAN"), 'LRT', 2, 'C', true);
        $this->Ln(4);		
        $this->SetFont('Arial', 'B', 9);
        $this->SetTextColor(0);
        $this->Cell(35, 4, utf8_decode("Cotización:"), '', 0, 'L', false);
        $this->SetTextColor(255, 0, 0);
        $this->Cell(35, 4, utf8_decode($dataArr['ubicacion_clave'] . "-" . $dataArr['cotizacion_id']), '', 1, 'L', false);
        $this->SetTextColor(0);
        $this->Cell(35, 4, utf8_decode("Fecha:"), '', 0, 'L', false);
        $this->SetTextColor(66, 80, 143);
        $this->Cell(35, 4, utf8_decode($fecha), '', 1, 'L', false);
        $this->SetTextColor(0);
        $this->Cell(35, 4, utf8_decode("Cliente:"), '', 0, 'L', false);
        $this->SetTextColor(66, 80, 143);
        if ("" != $dataArr['razon_social'])
            $this->Cell(35, 4, utf8_decode(strtoupper($dataArr['razon_social'])), '', 1, 'L', false);
        else
            $this->Cell(35, 4, utf8_decode(strtoupper($dataArr['nombre_comercial'])), '', 1, 'L', false);
        $this->Ln(4);
		//$this->SetY(23);
        $this->SetTextColor(255, 255, 255);
        $this->SetFont('Arial', 'B', 10);
        $this->Cell(20.5, 5, utf8_decode("Cantidad"), 'LRTB', 0, 'C', true);
        $this->Cell(95.5, 5, utf8_decode("Descripción"), 'LRTB', 0, 'C', true);
        $this->Cell(37, 5, utf8_decode("Precio Unitario"), 'LRTB', 0, 'C', true);
        $this->Cell(37, 5, utf8_decode("Total"), 'LRTB', 1, 'C', true);	
		//echo "entro";
	//	echo "halo".json_encode($dataArr);
	 //	if (0 < mysqli_num_rows($datos)){	
			//echo "entro en el if";
			//echo "h_o_l_a".$datos;
		//echo "entro en el if";	
	    $this->MultiCell(190, 4, utf8_decode("Agradeciendo la oportunidad que nos brinda de ponernos a sus órdenes y colaborar para la mejora de sus servicios de telecomunicaciones, ponemos a su disposición la siguiente cotización:"), '', 'L', false);
        $this->Ln(12);
		$this->SetFont('Arial', 'B', 10);
		$this->SetFillColor(255, 255, 255);
        $this->SetTextColor(3, 3, 3);
		$this->SetY(117);
		$this->SetX(167);
		$this->Cell(33, 5, utf8_decode("$".number_format($dataArr['subtotal'], 2, '.', ',')),0.1,0.1,'C', false);
		$this->SetY(147);
		$this->SetX(165);
		$this->Cell(37, 5, utf8_decode("$".number_format($dataArr['subtotal'], 2, '.', ',')),0.1,0.1,'C', false);
		$this->Cell(37, 5, utf8_decode("$".number_format($dataArr['impuesto'], 2, '.', ',')),0.1,0.1, 'C' , false);
		$this->Cell(37, 5, utf8_decode("$".number_format($dataArr['total'], 2, '.', ',')),0.1,0.1, 'C' , false);
	
	    //var_export($datos);
		//var_dump($datos);
		$this->SetY(99);
	    while($row4 = mysqli_fetch_assoc($datos)){
				//echo "entro en el while";
				//echo $this->$row4['descripcion'];
			//var_export($row4);	
		//			echo json_encode($row4);
		if(0 != $row4['producto_precio_especial']){
		    $this->SetX(40);
			$this->SetFont('Arial', 'B', 9);
		    $total = $row4['producto_precio_especial'] * $row4['cantidad'];
		    $this->Cell(37, 0, utf8_decode("".$row4['descripcion']),'',11,22,33,'C', false);  
          //$this->Cell(37, 10, utf8_decode("$ " .number_format($row4['descripcion'], 2, '.', ',')), 'LRTB', 0, 'C', false);
           //$this->Cell(37, 10, utf8_decode("$ " .number_format($this->total, 2, '.', ',')), 'LRTB', 0, 'C', false);
		} else {
           $total = $row4['producto_precio_especial'] * $row4['cantidad'];
           //$this->Cell(37, $cellHeight, utf8_decode("$ " . number_format($row4['producto_precio_especial'], 2, '.', ',')), 'LRTB', 0, 'C', false);
           //$this->Cell(37, $cellHeight, utf8_decode("$ " . number_format($this->total, 2, '.', ',')), 'LRTB', 0, 'C', false);
        }
          //$this->subtotal += $this->total;
          //$this->SetXY($posx , $posy);//estan aqui original mente   
		  $this->SetFont('Arial', 'B', 9);
		  $this->SetX(20);
		  //$this->SetY(100);
          $this->Cell(5.5, 6,$row4['cantidad'],'',4,2,'C', false);  //aqui se cambio la cantidad_vigencia a cantidad	 		
        // echo json_encode($row4);
        // $this->SetY(-80);
		switch($row4['vigencia']){
			case '0':
	        	$this->Cell(70, 4,'Notas :0 meses','C','L',true);
			break;
			
			case '1':
			$this->Cell(70,4,'Notas : 6 meses','C', 0,'L',true);
			break;
			
			case '2':
			$this->Cell(70, 4,'Notas :12 meses','C',0,'L',true);
			break;
			
			case '3':
			$this->Cell(70, 4,'Notas :18 meses','C',0,'L',true);
			break;
			
			case '4':
			//$this->SetY(-180);
			$this->Cell(70, 4,'Notas :24 meses','C',12,'L',true);
			break;
			
			case '5':
			$this->Cell(70, 4,'Notas :30 meses','C', 0,'L',true);
			break;
			
			case '6':
			$this->Cell(70, 4,'Notas :36 meses','C', 0,'L',true);
			break;
		}
  }
		$this->SetFillColor(2, 12, 117);
        $this->SetTextColor(255, 255, 255);
        $this->SetFont('Arial', 'B', 8);
		$this->SetY(147);
		$this->SetX(133);
	    $this->Cell(30, 5,utf8_decode("Subtotales"), 'LRT', 1, 'L', true);
	    $this->SetY(152);
		$this->SetX(133);
		$this->Cell(30, 5,utf8_decode("IVA"), 'LRT', 1, 'L', true);
		$this->SetY(157);
		$this->SetX(133);
		$this->Cell(30, 5,utf8_decode("Total a Pagar"), 'LRT', 1,'L', true);
	
		//aqui ba el footer
		$this->SetFont('Arial', 'B', 10);
		$this->SetY(165);
		$this->SetFillColor(2, 12, 117);
        $this->SetTextColor(255, 255, 255);
        $this->Cell(0, 4,utf8_decode("                                                                                                                                   							          Precios en pesos mexicanos"), 'LRT', 3, 'C', true);
		$this->SetFillColor(2, 12, 117);
        $this->SetTextColor(255, 255, 255);
		
		$this->SetFont('Arial', 'B', 8);
		$this->SetFillColor(255, 255, 255);
        $this->SetTextColor(3, 3, 3);
		$this->SetY(170);
		$this->SetY(175);
		$this->MultiCell(100,4, utf8_decode('SLA tiempo maximo de solucion de 4 horas en caso de falla. 
Precios en pesos mexicanos Este Servicio Tiene garantizado una alta Disponibilidad (99.865% ) Asignacion de un Ingeniero personalizado para la Cuenta.'),"C",'L', false);
			
		$this->Ln();
		$this->SetY(193);
		$this->SetFillColor(2, 12, 117);
        $this->SetTextColor(255, 255, 255);
        $this->Cell(0, 1,utf8_decode(" "), 'LRT', 3, 'C', true);
		$this->SetFont('Arial', 'B', 8);
		$this->SetFillColor(255, 255, 255);
        $this->SetTextColor(3, 3, 3);
		$this->SetY(195);
		$this->MultiCell(210,4, utf8_decode('Agradecemos la confianza brindada para Coeficiente Comunicaciones, así mismo reitéranos nuestro compromiso de atenderles 
		como se merecen y seguir creciendo en sus metas y objetivos conjuntamente. La confirmación de este documento ya sea por escrito,  
		correo electrónico y/o firma contempla la aceptación de la propuesta planteada. Sin más por el momento quedamos a sus órdenes para 
		cualquier aclaración o duda') , "C" , "L" ,false);
		
		$this->SetFont('Arial', 'B', 9);
		$this->SetY(230);
		$this->SetX(30);
		$this->MultiCell(210,4, utf8_decode('Atentamente') , "C" , "L" ,false);
		$this->SetY(245);
		$this->SetX(20);
		$this->MultiCell(210,4, utf8_decode('_________________________'), "C" , "L" ,false);
		$this->SetX(30);
		$this->SetFont('Arial', 'B', 7);
		$this->MultiCell(210,4, utf8_decode('Ing. Jorge Salde'), "C" , "L" ,false);
		$this->SetX(35);
		$this->SetFont('Arial', 'B', 7);
		$this->MultiCell(210,4, utf8_decode('Director'), "C" , "L" ,false);
		$this->SetX(30);
	    $this->MultiCell(281,6,'jsalde@coeciente.mx                                                                                                                      		                       (33)2282 8282',$this->Image('../../img/cotizacion/email.png',25, 259.0, 3, 3),$this->Image('../../img/cotizacion/tele.png',147, 259, 3, 3),0,0,'C');
//$this->Cell(valor1, valor2, $this->Image('ruta-imagen/imagen', $this->GetX(),$this->GetY()),'LR',0,'R');
		$this->SetFont('Arial', 'B', 9);
		$this->SetY(230);
		$this->SetX(156);
		$this->MultiCell(210,4, utf8_decode('Acepto') , "C" , "L" ,false);
		$this->SetY(245);
		$this->SetX(140);
		$this->MultiCell(210,4, utf8_decode('_________________________'), "C" , "L" ,false);
		$this->SetX(150);
	    $this->SetFont('Arial', 'B', 7);
		$this->MultiCell(210,4, utf8_decode(''.$dataArr['nombre'] . " " . $dataArr['apellidos']), "C" , "L" ,false);  //no estatico
		$this->SetX(144);
		$this->SetFont('Arial', 'B', 7);
		$this->MultiCell(210,4, utf8_decode($dataArr['razon_social']." ".$dataArr['ubicacion_nombre']), "C" , "L" ,false); //no estatico	
	}
	/*
    function printPriceFooter($dataArr, $tel, $regPrice){
        //imprime el footer y las promociones
        $posx = $this->GetX();
        $posy = $this->GetY();
        $this->SetXY($posx, $posy + 5);
        $this->Image('../../img/cotizacion/coe_cotfooter_' . $regPrice . '.jpg', $posx, $posy + 8, 190);
        $this->SetXY($posx + 130, $posy + 3);
        $this->MultiCell(60, 4, utf8_decode($dataArr['nombre'] . " " . $dataArr['apellidos'] . " \n " . $dataArr['departamento'] . " \n " . $tel . " Ext. " . $dataArr['extension'] . " \n Cel. " . $dataArr['tel_movil'] . " \n " . $dataArr['email']), '', 'L', false);
    }
    function printTotal($subtotal, $impuesto, $totalCot) {
        $this->Cell(0, 5, "", 'LR', 1, 'C', false);
        $posx = $this->GetX();
        $posy = $this->GetY();
        $this->Cell(20.5 + 95.5, 5, "", 'L', 1, 'C', false);
        /* $this->Cell(20.5+95.5,5, utf8_decode("__________________________"),'L', 1 , 'C', false );
          $this->Cell(20.5+95.5,5, utf8_decode("Firma de autorización"),'L', 1 , 'C', false ); */
      /*  $this->Cell(20.5 + 95.5, 5, utf8_decode(""), 'L', 1, 'C', false);
        $this->Cell(20.5 + 95.5, 5, utf8_decode(""), 'L', 1, 'C', false);
		
        $this->SetXY($posx + 20.5 + 95.5, $posy);
        $this->SetFont('Arial', 'B', 9);
        $this->Cell(37, 5, "Subtotales", 'R', 0, 'L', false);
        $this->SetFont('Arial', '', 9);
        $this->Cell(37, 5, "$ " . number_format($subtotal, 2, '.', ','), 'LRTB', 1, 'C', false);
        $posx = $this->GetX();
        $posy = $this->GetY();
        $this->SetXY($posx + 20.5 + 95.5, $posy);
        $this->SetFont('Arial', 'B', 9);
        $this->Cell(37, 5, "IVA", 'R', 0, 'L', false);
        $this->SetFont('Arial', '', 9);
        $this->Cell(37, 5, "$".number_format($impuesto, 2, '.', ','), 'LRTB', 1, 'C', false);
        $posx = $this->GetX();
        $posy = $this->GetY();
        $this->SetXY($posx + 20.5 + 95.5, $posy);
        $this->SetFont('Arial', 'B', 9);
        $this->Cell(37, 5, "Total a Pagar", 'R', 0, 'L', false);
        $this->Cell(37, 5, "$ " . number_format($totalCot, 2, '.', ','), 'LRTB', 1, 'C', false);
        $this->SetTextColor(255, 255, 255);
        $this->SetFont('Arial', 'B', 9);
        $this->Cell(0, 5, utf8_decode("Precios en pesos mexicanos MXN"), 'LRTB', 2, 'R', true);
        $this->Ln(5);
        $this->SetTextColor(0);
    }*/
}
