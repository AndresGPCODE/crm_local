<?php
/*Este archivo contiene funciones para gestión de datos via ftp que ahorran el engorroso trabajo de hacer todo paso a paso 
y manual, por ejemplo la eliminación de directorios con archivos dentro. Se usa principalmente por cualquier modulo o función que suba o elimine archivos del servidor ftp (especificado en dbcommon)*/
include_once "common.php";

$parametros = parseArguments($argv);

$action = $parametros['action'];
$cfgLogService = "../../log/ftpfunctionlog";

$dir = $parametros['dir'];
$orgDir = $parametros['orgdir'];
$destDir = $parametros['destdir'];

$conFtp = ftpConnectLogin();

if(-1==$conFtp||-2==$conFtp){
  log_write("ERROR: No se pudo establecer conexión con el servidor remoto",8);
  echo("No se pudo establecer conexión con el servidor remoto\n");   
}
elseif(!isset($action)||0>=count($action)){
  log_write("ERROR: Tipo de acción no especificada",8);
  echo("Especifique un tipo de acción ('delete', 'upload')\n");
}
else{
  switch($action){
    case 'delete':
      if(0>=count($dir))
        echo("No se recibió nombre del directorio\n");
      else
        ftpDirDelete($dir);      
    break;
    
    case 'upload':
      if(0>=count($orgDir)){
        echo("No se recibió nombre del directorio origen\n");
        log_write("ERROR: No se recibió nombre del directorio origen",8);
      }
      elseif(0>=count($destDir)){
        echo("No se recibió nombre del directorio destino\n");
        log_write("ERROR: No se recibió nombre del directorio destino",8); 
      }
      else      
        ftpUpload($orgDir,$destDir);
    break;
    
    default:
      echo("No se especificó una acción válida\n");
    break;
  }
  echo("***FTP TERMINA PROCESO***\n");
  ftp_close($conFtp); 
}

function ftpUpload($orgDir,$destDir){
  global $conFtp;
  echo("***FTP UPLOAD***\n");
  
  $folders = explode("/",trim($destDir));
  $levelNum = count($folders)-2;
  $fileName = $folders[count($folders)-1];
  
  echo("Folders ".print_r($folders,true)."\n");
  
  if(!file_exists($orgDir)){
    log_write("ERROR: No existe el archivo origen $orgDir no se puede subir",8);
    return false;
  }
  //Se creará la ruta donde se guardará el archivo si no existe
  for($a=0;$a<=$levelNum;$a++){    
    if(!ftp_chdir($conFtp,$folders[$a])){    
      echo("No existe el directorio ".$folders[$a]." se intentará crear\n");
      log_write("DEBUG: No existe el directorio ".$folders[$a]." se intentará crear",8);
      
      if(!ftp_mkdir($conFtp,$folders[$a])){
        log_write("DEBUG: No su pudo crear el directorio",8);
        echo("No su pudo crear el directorio\n");
        return false;
      }
      else{
        log_write("DEBUG: Se creó ".$folders[$a],8);
        echo("Se creó ".$folders[$a]."\n");
        ftp_chmod($conFtp,0755,$folders[$a]); 
        ftp_chdir($conFtp,$folders[$a]);      
      }
    }
    else{
      log_write("DEBUG: La carpeta ".$folders[$a]." ya existe",8);
      echo("La carpeta ".$folders[$a]." ya existe\n");
    }
  }
  echo("Dir final ".ftp_pwd($conFtp)."\n");
  log_write("DEBUG: Dir final ".ftp_pwd($conFtp),8);   
  
  if(!ftp_put($conFtp,$fileName,$orgDir,FTP_BINARY)){    
    log_write("ERROR: No se subió el archivo al servidor ftp",8);
    echo("No se subió el archivo al servidor ftp\n");
    return false;
  }
  else{ 
    log_write("ERROR: Se agregó la imagen ".$orgDir." al servidor ftp ".$destDir,8);  
    ftp_chmod($conFtp,0755,$fileName);                 
    echo("Se agregó la imagen ".$orgDir." al servidor ftp ".$destDir); 
    unlink($orgDir);           
  }
  return true;
}

/*hace un borrado recursivo de una carpeta especificada en una ruta ABSOLUTA y todos sus archivos y carpetas dentro*/
function ftpDirDelete($dir){   
  global $conFtp;
  echo("***FTP DIR DELETE START***\n");
    
  /*$listing = ftp_nlist($conFtp,$dir);
  if(empty($listing)) {
    echo("El directorio no existe\n");   
  }  
  else{*/
    echo("Dir actual ".ftp_pwd($conFtp)."\n");         
    $fileList = listDetailed($dir);  
      
    if(!(@ftp_rmdir($conFtp,$dir)||@ftp_delete($conFtp,$dir))){
      echo("El directorio no está vacío\n");          
      echo("File list: ".print_r($fileList,true)."\n");
      
      foreach($fileList as $file => $filedata){
        echo("Su carpeta actual ".$file."\n");
        ftpDirDelete($dir."/".$file);
      }
      ftpDirDelete($dir);      
    } 
  //}       
} 

/*obtiene y parsea todos los archivos y carpetas(incluyendo ocultos) en una carpeta via ftp. Devuelve un arreglo asociativo de arreglos con
  el nombre de la carpeta o archivo como índice y dentro de cada uno están los datos como permisos y propietario (cortesía de php.net)*/
function listDetailed($dir){ 
  global $conFtp;
  if(is_array($children=@ftp_rawlist($conFtp,$dir))){ 
    $items = array(); 
    foreach ($children as $child) { 
      $chunks = preg_split("/\s+/", $child); 
      list($item['rights'], $item['number'], $item['user'], $item['group'], $item['size'], $item['month'], $item['day'], $item['time']) = $chunks; 
      $item['type'] = $chunks[0]{0} === 'd' ? 'dir' : 'file'; 
      array_splice($chunks, 0, 8); 
      $items[implode(" ", $chunks)] = $item; 
    } 
    return $items; 
  } 
  return false; 
} 

?>
