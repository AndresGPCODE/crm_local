<?php
session_start();
set_time_limit(0);

include_once "common.php";
include_once "encrypt.php";
include_once "../fpdf/fpdf.php";
//require_once "../fpdf/image_alpha.php";
require_once '../mail/swiftmailer-master/lib/swift_required.php';

header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

$usuario    = $_SESSION['usuario'];
$usuarioId  = $_SESSION['usrId'];
$gruposArr  = $_SESSION['grupos'];
$usrUbica   = $_SESSION['usrUbica'];
               
$salida = "";

class PDF extends FPDF
{
  function printCotConcept($cant,$desc,$pre1,$pre2){
    $posx = $this->GetX();
    $posy = $this->GetY();
    
    $this->SetXY($posx+20.5,$posy); //se recorre a la posición de la segunda columna
    
    $this->SetFont('Arial','I',9); 
    $this->MultiCell(95.5,5, utf8_decode($desc),0, 'L', false ); //Imprime descripción
    
    $this->SetFont('Arial','',9); 
    $cellHeight = $this->GetY()-$posy; //obtiene la altura de la celda de descripcion porque es dinámica
                 
    $this->SetXY($posx+95.5+20.5,$posy); //se posiciona a la derecha de la celda de descripción
    
    //imprime celdas de precios
    //$this->Cell(37,$cellHeight, utf8_decode($pre1),'LRTB', 0 , 'C', false );
    $this->Cell(74,$cellHeight, utf8_decode($pre2),0, 0 , 'C', false );
    
     //vuelve a la primera posición de la fila e imprime la primera celda (cantidad)
    $this->SetXY($posx,$posy);
    $this->SetFont('Arial','B',8); 
    $this->Cell(20.5,$cellHeight, $cant,0, 1 , 'C', false );
  }
  
  function printCotNote($name,$desc){
    $this->SetFont('Arial','B',8); 
    $this->Cell(37,5,$name.":",0, 0 , 'L', false );
    $this->SetFont('Arial','IB',8); 
    $this->Cell(153,5,utf8_decode($desc),'LRTB', 1 , 'L', false );
  }
  
  function printPriceBackground($opt){
    if(0==$opt)
      $this->Image('../../img/cotizacion/hoja_blanca1.jpg', -10, 6, 225, 0);
    else if(1==$opt)
      $this->Image('../../img/cotizacion/hoja_blanca.jpg', -10, 6, 225, 0);
  }
  
  function printPriceFirstHeader($dataArr){
    $this->SetXY(10, 10);
    if(file_exists($dataArr['cliLogo']))
      $this->Image($dataArr['cliLogo'],15,10,0,20);
    $this->SetFont('Arial','',10);
		$this->SetY(50);
		$this->Cell(0,0, utf8_decode($dataArr['abreviatura'].' '.$dataArr['nombre']." ".$dataArr['apellidos']), '', 1, 'L', false);
		$this->Cell(0,15,utf8_decode($dataArr['razon_social']),0,1,'LT',false);
    $this->Cell(0,5,utf8_decode("PRESENTE"),0,1,'L',false);
		$this->MultiCell(190, 4, utf8_decode("De antemano queremos agradecer la oportunidad que nos brinda para presentarle nuestra propuesta, estamos seguros que podrá encontrar en nuestro servicio una atención personalizada inmejorable, ya que nuestra función principal es mantener los estándares más altos de calidad, enfocados al servicio al cliente, permitiéndonos crecer en conjunto con ustedes generando relaciones de negocio a largo plazo cubriendo sus más altas expectativas. Esperamos que esta información sea de su utilidad quedando a sus órdenes para cualquier requerimiento o aclaración."),'', 'L', false);
      
		$this->SetFont('Arial','',8);
		$this->Cell(0,10,'Atentamente',5,5,'C');
		$this->SetFont('Arial','B',9);
		$this->Cell(0,3,utf8_decode($dataArr['venNom']." ".$dataArr['venApe']),5,5,'C');
		$this->SetFont('Arial','',8);
		$this->Cell(0,6,utf8_decode($dataArr['venPuesto']),5,5,'C');
		$this->SetFont('Arial','B',9);
		$this->Cell(0,6,utf8_decode($dataArr['venEmail'].' '.$dataArr['venTelMovil']),0,0,'C');
  }
  
  function printPriceHeader($dataArr){
    $fecha  = date("Y-m-d H:i:s");
     
    $this->SetXY(10, 10);
    if(file_exists($dataArr['logo_ruta'])&&("../../img/logo_coe.png"!=$dataArr['logo_ruta']))
      $this->Image($dataArr['logo_ruta'],15,10,0,20);
    //$this->Image('../../img/logo_coewhite.png',120,5,60);
    $this->Ln(30);
    $this->SetTextColor(0);
    $this->SetFont('Arial','B',10); 
    $this->SetX(20);
    $this->Cell(0,4, utf8_decode($dataArr['nombre']." ".$dataArr['apellidos']),'', 2 , 'L', false );
    $this->Ln(3);
    $this->SetX(20);
    $this->SetFont('Arial','',10); 
    $this->MultiCell(190,4, utf8_decode("        Agradeciendo la oportunidad que nos brinda de ponernos a sus órdenes y colaborar para la mejora de sus servicios de telecomunicaciones, ponemos a su disposición la siguiente cotización:"),'', 'L', false );
    $this->Ln(12);
    $this->SetFillColor(2,12,117);
    $this->SetTextColor(255,255,255);
    $this->SetFont('Arial','B',9); 
    $this->Cell(0,4, utf8_decode("Internet | Telefonía | Cloud PBX | Web Hosting & VPS | Security Operations Center | LAN to LAN"),'LRT', 2 , 'C', true );
    $this->Ln(4);
     
    $this->SetFont('Arial','B',9); 
    $this->SetTextColor(0);
    $this->Cell(35,4, utf8_decode("Cotización:"),'', 0 , 'L', false );
    $this->SetTextColor(255,0,0);
    $this->Cell(35,4, utf8_decode($dataArr['ubicacion_clave']."-".$dataArr['cotizacion_id']),'', 1 , 'L', false );
    $this->SetTextColor(0);
    $this->Cell(35,4, utf8_decode("Fecha:"),'', 0 , 'L', false );
    $this->SetTextColor(66,80,143);
    $this->Cell(35,4, utf8_decode($fecha),'', 1 , 'L', false );
    $this->SetTextColor(0);
    $this->Cell(35,4, utf8_decode("Cliente:"),'', 0 , 'L', false );
    $this->SetTextColor(66,80,143);
    if(""!=$dataArr['razon_social'])
      $this->Cell(35,4, utf8_decode(strtoupper($dataArr['razon_social'])),'', 1 , 'L', false );
    else
      $this->Cell(35,4, utf8_decode(strtoupper($dataArr['nombre_comercial'])),'', 1 , 'L', false );
    $this->Ln(4);
    $this->SetTextColor(255,255,255);
    $this->SetFont('Arial','B',12); 
    $this->Cell(20.5,5, utf8_decode("Cantidad"),'LRTB', 0 , 'C', true );
    $this->Cell(95.5,5, utf8_decode("Descripción"),'LRTB', 0 , 'C', true );
    $this->Cell(37,5, utf8_decode("Precio Unitario"),'LRTB', 0 , 'C', true );
    $this->Cell(37,5, utf8_decode("Total"),'LRTB', 1 , 'C', true );
  }
  
  function printPriceFooter($dataArr,$tel){
    //imprime el footer y las promociones
    $posx = $this->GetX();
    $posy = $this->GetY();
    
    $this->SetTextColor(0);
    
    $this->SetXY($posx,$posy+5);
    
    $this->SetXY ($posx,$posy+10);
    
    $this->SetFont('Courier', '', 10);
    $this->MultiCell(190,4, utf8_decode("Agradecemos la confianza brindada para Coeficiente Comunicaciones, así mismo reiteramos nuestro compromiso de atenderles como se merecen y seguir creciendo en sus metas y objetivos conjuntamente.
La confirmación de este documento ya sea por escrito, correo electrónico y/o firma contempla la aceptación de la propuesta planteada. Sin más por el momento quedamos a sus órdenes para cualquier aclaración o duda.") , "C" , "L" ,false);
    
    $posx = $this->GetX();
    $posy = $this->GetY();
       
    $this->SetXY ($posx,$posy+5);
		$this->SetFont('Courier', 'B', 10);
    $this->Cell(95,10, utf8_decode('Atentamente'),0, 1 , 'C', false );
    $this->Cell(95,15, utf8_decode('__________________________________'),0, 1 , 'C', false );
		$this->SetFont('Courier', 'B', 9);
    $this->Cell(95,4,utf8_decode($dataArr['venNom']." ".$dataArr['venApe']),0, 1 , 'C', false );
		$this->SetFont('Courier', 'B', 9);
    $this->Cell(95,4, utf8_decode($dataArr['venPuesto']),0, 1 , 'C', false );
    $this->SetFont('Courier', 'B', 9);
    $this->Cell(95,4, utf8_decode($dataArr['venEmail'].' '.$dataArr['venTelMovil']),0, 1 , 'C', false );
    $this->SetXY ($posx+90,$posy+5);    
    $this->SetFont('Courier', 'B', 10);
    $this->Cell(95,10, utf8_decode('Acepto'),0, 1 , 'C', false );
    $this->SetX ($posx+90);
    $this->Cell(95,15, utf8_decode('__________________________________'),0, 1 , 'C', false );
		$this->SetFont('Courier', 'B', 9);
    $this->SetX ($posx+90);
    $this->Cell(95,4, utf8_decode(''.$dataArr['nombre']." ".$dataArr['apellidos']),0, 1 , 'C', false );
		$this->SetFont('Courier', 'B', 9);
    $this->SetX ($posx+90);
    $this->Cell(95,4, utf8_decode($dataArr['razon_social']),0, 1 , 'C', false );
  }
  
  function printTotal($subtotal,$impuesto,$totalCot){
    $this->Cell(0,5, "",0, 1 , 'C', false );
    $posx = $this->GetX();
    $posy = $this->GetY();
    $this->Cell(20.5+95.5,5, "",0, 1 , 'C', false );
   /*$this->Cell(20.5+95.5,5, utf8_decode("__________________________"),'L', 1 , 'C', false );
    $this->Cell(20.5+95.5,5, utf8_decode("Firma de autorización"),'L', 1 , 'C', false );*/
    $this->Cell(20.5+95.5,5, utf8_decode(""),0, 1 , 'C', false );
    $this->Cell(20.5+95.5,5, utf8_decode(""),0, 1 , 'C', false );
    
    $this->SetXY($posx+20.5+95.5,$posy);
    $this->SetFont('Arial','B',9); 
    $this->SetFillColor(0,49,91);
    $this->SetTextColor(255,255,255);
    $this->Cell(37,5, "Subtotales",0, 0 , 'L', true ); 
    $this->SetFont('Arial','',9); 
    $this->SetFillColor(0);
    $this->SetTextColor(0);
    $this->Cell(37,5, "$ ".number_format($subtotal, 2, '.', ','),0, 1 , 'C', false ); 
    $posx = $this->GetX();
    $posy = $this->GetY();
    $this->SetXY($posx+20.5+95.5,$posy);
    $this->SetFont('Arial','B',9); 
    $this->SetFillColor(0,49,91);
    $this->SetTextColor(255,255,255);
    $this->Cell(37,5, "IVA",0, 0 , 'L', true ); 
    $this->SetFont('Arial','',9);
    $this->SetFillColor(0);
    $this->SetTextColor(0); 
    $this->Cell(37,5, "$ ".number_format($impuesto, 2, '.', ','),0, 1 , 'C', false );
    $posx = $this->GetX();
    $posy = $this->GetY();
    $this->SetXY($posx+20.5+95.5,$posy);
    $this->SetFont('Arial','B',9); 
    $this->SetFillColor(0,49,91);
    $this->SetTextColor(255,255,255);
    $this->Cell(37,5, "Total a Pagar",0, 0 , 'L', true ); 
    $this->SetFont('Arial','BI',9);
    $this->SetFillColor(0);
    $this->SetTextColor(0);
    $this->Cell(37,5, "$ ".number_format($totalCot, 2, '.', ','),0, 1 , 'C', false );
    $this->SetFillColor(2,12,117);
    $this->SetTextColor(255,255,255);
    $this->SetFont('Arial','B',9); 
    $this->Cell(0,5, utf8_decode("Precios en pesos mexicanos MXN"),0, 2 , 'R', true );
    $this->Ln(5);
    $this->SetTextColor(0);
  }
}

$db_modulos = condb_modulos();
$db_catalogos = condb_catalogos();
$db_usuarios = condb_usuarios();

$opt = isset($_GET['option']) ? 0 : $_POST['procMailOpt']; //se valida GET porque si es visualizar nomás la cotización este valor llega via get. Si no llega significa que se eligió envío por correo

if(0==$opt){
  $pricId  = isset($_GET['id']) ? decrypt($_GET['id']) : -1;
  $orgType = isset($_GET['orgType']) ? $_GET['orgType'] : -1;
}
if(1==$opt){
  //$contactEmails = array();
  $contactEmails = "";
  $pricId  = isset($_POST['cotId']) ? decrypt($_POST['cotId']) : -1;
  $orgType = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
  $contArr = isset($_POST['pricMailContact']) ? json_decode($_POST['pricMailContact']) : "";
  
  $asunto       = isset($_POST['msgSubject']) ? $_POST['msgSubject'] : "Cotización";//"Cotización para ".$row1['nombre_comercial'];
  $mensajeHtml  = isset($_POST['msgHtmlContent']) ? $_POST['msgHtmlContent'] : "";//"<h4><b>Envio cotización</b></h4>";
  $mensajePlano = isset($_POST['msgPlainContent']) ? $_POST['msgPlainContent'] : "";//"Envio cotización";
  
  foreach($contArr as $key => $value){
    $contactEmails .= $value[2].",";
   // $contactEmails[$value[2]] = $value[1];
  }
  $contactEmails = substr($contactEmails,0,-1);
}

if(-1==$pricId||-1==$orgType){
  $salida = "ERROR||No se pudo obtener los datos de la cotización para su envío";
  goto a;
}
else{
  //$contactEmails = array("tsanchez@ctclat.com" => "Tania Sánchez");

  $pdf = new PDF();

  $subtotal = 0;
  $impuesto = 0;
  $totalCot = 0;

  switch($orgType){
    case 0:  //si se trata de un prospecto
      $dbname = "prospectos";
      $fieldname = "prospecto_id";
      $typename = "prospecto";
      $asignfield = "asignadoa";
      $nameFieldName = " ";
    break;
    case 1:  //si se trata de un cliente
      $dbname = "clientes";
      $fieldname = "cliente_id";
      $typename = "cliente";
      $asignfield = "usuario_alta";
      $nameFieldName = " ";
    break;
    /*case 2:  //si se trata de un sitio
      $dbname = "sitios";
      $fieldname = "sitio_id";
      $typename = "sitio";
      $asignfield = "usuario_alta";
      $nameFieldName = "cli.nombre AS nombre_comercial, con.nombre AS nombre, ";
    break;   */ 
  }
  
  $queryimg = "SELECT DISTINCT(cvts.imagen_ruta)".
              "FROM ".$cfgTableNameCat.".cat_venta_servicios AS cvs, ".
                      $cfgTableNameCat.".cat_venta_tipo_servicios AS cvts, ".
                      $cfgTableNameCat.".cat_venta_categorias AS cvc, ".
                      $cfgTableNameMod.".rel_cotizacion_producto AS rcp ".
              "WHERE cvs.servicio_id = rcp.producto_id ".
              "AND cvs.tipo_servicio = cvts.tipo_servicio_id ".
              "AND cvts.categoria_id = cvc.categoria_id ".
              "AND rcp.cotizacion_id = ".$pricId." ".
              "AND cvts.imagen_ruta<>''";
          
  $query1 = "SELECT cot.*, cot.fecha_alta AS cotfecalta, con.*, tit.abreviatura, cli.*, cli.logo_ruta AS cliLogo, ".$nameFieldName." ubi.*, usr.nombre AS venNom, usr.apellidos AS venApe, usr.departamento AS venDepto, usr.puesto AS venPuesto, usr.extension AS venExt, usr.tel_movil AS venTelMovil, usr.email AS venEmail, usr.ubicacion_id ".
             "FROM ".$cfgTableNameMod.".cotizaciones AS cot, ".
                     $cfgTableNameMod.".contactos AS con, ".
                     $cfgTableNameMod.".".$dbname." AS cli, ".
                     $cfgTableNameCat.".cat_coe_ubicacion AS ubi, ".
                     $cfgTableNameCat.".cat_persona_titulo AS tit, ".
                     $cfgTableNameUsr.".usuarios AS usr ".
             "WHERE cot.cotizacion_id=".$pricId.
             " AND cot.contacto_id=con.contacto_id".
             " AND cli.".$fieldname."=cot.origen_id".
             " AND cot.tipo_origen=".$orgType.
             " AND usr.ubicacion_id=ubi.ubicacion_id".
             " AND cli.asignadoa=usr.usuario_id".
             " AND con.titulo_id=tit.persona_titulo_id"; 
  
  $result1 = mysqli_query($db_modulos,$query1);
  $resultimg = mysqli_query($db_modulos,$queryimg);
  
  if(!$result1||!$resultimg){
    $salida = "ERROR:Ocurrió un problema al consultar los datos de la cotización";
    goto a;
  }
  else{
    $row1=mysqli_fetch_assoc($result1);
    $cotStatus = $row1['cotizacion_estatus_id'];
    $venEmail = $row1['venEmail']; 
    $venNom = $row1['venNom'];
    $venApe = $row1['venApe'];
    $suppostServiceTel = $row['telefono'];
    $supportServiceMail = $row['email'];
    
    $pdf->AddPage();
    $pdf->printPriceBackground(0);
    $pdf->printPriceFirstHeader($row1);
    $pdf->Image('../../img/cotizacion/servicios.jpg', 15, 130, 180, 120); 
  
    if(0<mysqli_num_rows($resultimg)){
      $pdf->AddPage();
      $pdf->printPriceBackground(1);
      $imgcnt = 0;

      $pdf->setY(30);
      while($rowimg = mysqli_fetch_assoc($resultimg)){ 
        $imgarr = explode(",",$rowimg['imagen_ruta']);
        foreach($imgarr as $keyimg => $valueimg){
          if((0==$imgcnt%2)&&(0!=$imgcnt)){
            //error_log("SALTO DE PAGINA IMAGEN");
            $pdf->AddPage();
            $pdf->printPriceBackground(1);
            $pdf->setY(30);
          } 
          //error_log("imagen impresa ".$valueimg);
          $posy = $pdf->GetY(); 
          if(file_exists('../../img/cotizacion/'.$valueimg)&&(""!=$valueimg)){
            $pdf->Image('../../img/cotizacion/'.$valueimg, 0, $posy, 200, 0); 
          }   
          $posy = $posy+130; 
          $pdf->setXY(0,$posy);
          $imgcnt++;
        }
      } 
      if(0!=$imgcnt%2){
        $pdf->Image('../../img/cotizacion/clientes.jpg',0,160,200,0);  
      }
      else{
        $pdf->AddPage();
        $pdf->printPriceBackground(1);
        $pdf->SetXY(0,100);
        $pdf->Image('../../img/cotizacion/noc.jpg',0,30,200,0);
        $pdf->Image('../../img/cotizacion/clientes.jpg',0,160,200,0);
      }
    }
    
    $pdf->AddPage();
    $orgId = $row1[$fieldname]; 
    $orgStat = $row1['estatus'];
    
    $pdf->printPriceBackground(0);
    $pdf->printPriceHeader($row1);

    /* if(($usuarioId!=$row1['usuario_alta'])&&(!array_key_exists(2, $gruposArr))&&(!array_key_exists(1, $gruposArr)))
       die("El usuario no tiene permisos para ver esa cotización".$query1);
     else{*/
         $query2 = "SELECT rcp.relacion_id, cvs.*, cvv.cantidad_vigencia, cvv.factor, rcp.cantidad, cvts.categoria_id, ".
                   "rcp.producto_precio_especial, rcp.producto_autorizado_por, rcp.sitio_asignado, rcp.vigencia, rcp.orden_trabajo_id ".
                   "FROM ".$cfgTableNameCat.".cat_venta_servicios AS cvs, ".
                           $cfgTableNameCat.".cat_venta_tipo_servicios AS cvts, ".
                           $cfgTableNameMod.".rel_cotizacion_producto AS rcp, ".
                           $cfgTableNameCat.".cat_venta_vigencia AS cvv ".
                   "WHERE cvs.servicio_id = rcp.producto_id ".
                   "AND cvs.tipo_servicio = cvts.tipo_servicio_id ".
                   "AND rcp.vigencia = cvv.vigencia_id ".
                   "AND rcp.cotizacion_id = ".$pricId." ORDER BY cvs.tipo_servicio";
         
         $result2 = mysqli_query($db_modulos,$query2);
         
         if(!$result2){
           $salida = "ERROR||No se pudo obtener los datos de los productos para esta cotización ";
           goto a;
         }
         else{
           $pdf->SetTextColor(0);
           $pdf->SetFont('Arial','B',9); 
           
           //imprime servicios
           if(0<mysqli_num_rows($result2)){
             $adecArr = array();
             $enlArr = array();
             
             while($row2 = mysqli_fetch_assoc($result2)){
               if(1==$row2['categoria_id']){
                 $vigenciaMeses = $row2['cantidad_vigencia'];
                 $enlArr[] = array($row2['servicio_id'],$row2['precio_'.$row2['cantidad_vigencia'].'m'],$row2['cargo_adecuacion_'.$row2['cantidad_vigencia'].'m'],$row2['costo_'.$row2['cantidad_vigencia'].'m'],$row2['sub_'.$row2['cantidad_vigencia'].'m'],$row2['adec_'.$row2['cantidad_vigencia'].'m'],$row2['cantidad_vigencia'],$row2['descripcion']);
               } 
               if(-1!=$row2['producto_precio_especial']){     
                 $precio = $row2['producto_precio_especial'];
               }
               else{             
                 if(1==$row2['tipo_cobro']) //si es pago recurrente por mes
                   $precio = $row2['precio_'.$row2['cantidad_vigencia'].'m'];
                 else
                   $precio = $row2['precio_lista'];
               }
               if(5==$row2['categoria_id']){
                 $adecArr[] = array($row2['descripcion'],$precio,$row2['cantidad'],$row2['servicio'],$row2['orden_trabajo_id']);
                 //error_log("Precio adecuación ".$row2['descripcion']." ".$precio);
               }
               else{
                 $subtotal = $subtotal+($row2['cantidad']*$precio);
             
                 $bajada = str_replace("%n%",$row2['bajada'],$row2['descripcion']);            
                 $desc = str_replace("%u%",$row2['subida'],$bajada)." (a ".$row2['cantidad_vigencia']." meses)";
                 
                 $posx = $pdf->GetX();
                 $posy = $pdf->GetY();
                 
                 $pdf->SetXY($posx+20.5,$posy); //se recorre a la posición de la segunda columna
                 
                 $pdf->SetFont('Arial','IB',9); 
                 $pdf->MultiCell(95.5,5, utf8_decode($desc),0, 'L', false ); //Imprime descripción
                 
                 $pdf->SetFont('Arial','',9); 
                 $cellHeight = $pdf->GetY()-$posy; //obtiene la altura de la celda de descripcion porque es dinámica
                              
                 $pdf->SetXY($posx+95.5+20.5,$posy); //se posiciona a la derecha de la celda de descripción
                 
                 //imprime celdas de precios
                 $total = $precio*$row2['cantidad'];
                 $pdf->Cell(37,$cellHeight, utf8_decode("$ ".number_format($precio, 2, '.', ',')),0, 0 , 'C', false );
                 $pdf->Cell(37,$cellHeight, utf8_decode("$ ".number_format($total, 2, '.', ',')),0, 0 , 'C', false );
                 
                  //vuelve a la primera posición de la fila e imprime la primera celda (cantidad)
                 $pdf->SetXY($posx,$posy);
                 $pdf->SetFont('Arial','B',9); 
                 
                 $pdf->Cell(20.5,$cellHeight, $row2['cantidad'],0, 1 , 'C', false ); 
               }              
             }
           }
                      
           $impuesto = $subtotal*0.16;
           $totalCot = $subtotal+$impuesto;
           
           $pdf->printTotal($subtotal,$impuesto,$totalCot);
           
           $posx = $pdf->GetX();
           $posy = $pdf->GetY();
           
           $pdf->SetXY($posx+37,$posy); //se recorre a la posición de la segunda columna
           
           $pdf->SetFont('Arial','I',10); 
           $pdf->MultiCell(153,5, utf8_decode($row1['notas']),0, 'L', false ); //Imprime notas
           
           $cellHeight = $pdf->GetY()-$posy; //obtiene la altura de la celda de notas porque es dinámica
                         
           $pdf->SetXY($posx,$posy);
           $pdf->SetFont('Arial','B',10); 
           $pdf->Cell(37,$cellHeight,"Notas:",0, 1 , 'L', false ); //imprime la celda que dice notas  
           
           //imprime los datos adicionales que vienen con cada enlace
           $posx = $pdf->GetX();
           $posy = $pdf->GetY();
          
           //if(0<sizeof($adecArr)){
             $pdf->Cell(70,5,utf8_decode("Adecuación"),0, 0 , 'L', false );
             $pdf->Cell(30,5,utf8_decode("Precio Normal"),0, 0 , 'L', false );
             $pdf->Cell(35,5,utf8_decode("Cargo Adecuación"),0, 0 , 'L', false );
             $pdf->Cell(30,5,utf8_decode("Subsidio"),0, 0 , 'L', false ); 
             $pdf->Cell(25,5,utf8_decode("Precio Final"),0, 1 , 'L', false ); 
           //}
           
           $adecsubtotal = 0;
            
           $adecSubAplPila = array();
           //$adecArr[] = array($row2['descripcion'],$precio,$row2['cantidad'],$row2['servicio'],$row2['orden_trabajo_id']);
            
           foreach($adecArr AS $key => $value){
             $posx = $pdf->GetX();
             $posy = $pdf->GetY();
              
             //error_log(print_r($value,true));
             
             $pdf->MultiCell(70,5, utf8_decode($value[0]." (pago único)"),0, 'L', false );
             
             error_log("vigencia ".$vigenciaMeses);
              
             $queryAdecDet = "SELECT cvs.servicio_id, cvs.descripcion, cvs.sub_".$vigenciaMeses."m, cvs.cargo_adecuacion_".$vigenciaMeses."m, cvs.servicio_id ".
                             "FROM ".$cfgTableNameMod.".rel_cotizacion_producto AS rcp, ". 
                                     $cfgTableNameMod.".ordenes_trabajo AS ot, ".
                                     $cfgTableNameCat.".cat_venta_servicios AS cvs ".
                             "WHERE ot.orden_id = ".$value[4]." ".
                             "AND ot.rel_cotizacion_producto = rcp.relacion_id ".
                             "AND rcp.producto_id = cvs.servicio_id ";
                              
             $resultAdecDet = mysqli_query($db_modulos,$queryAdecDet);
             if($resultAdecDet){
               $rowAdecDet = mysqli_fetch_assoc($resultAdecDet);
                
                
               if(!in_array($rowAdecDet['servicio_id'],$adecSubAplPila)){
                 $carAdec = $rowAdecDet['cargo_adecuacion_'.$vigenciaMeses.'m'];
                 $subAdec = $rowAdecDet['sub_'.$vigenciaMeses.'m'];
               }
               else{
                 $carAdec = 0;
                 $subAdec = 0;
               }
               if(106==$pricId||107==$pricId||114==$pricId||116==$pricId||373==$pricId||497==$pricId||574==$pricId||575==$pricId){
                 $carAdec = 0; 
                 $subAdec = 0;
               }
               
               error_log("subadec ".$subAdec);
                
               if(0<$value[1]){
                 $precioSub = ($value[1]*$value[2])+$carAdec-$subAdec;
                  
                 if(0>=$precioSub)
                   $precioSub = 0;
           
                  $cellHeight = $pdf->GetY()-$posy; //obtiene la altura de la celda de descripcion porque es dinámica
                  $pdf->setXY($posx+70,$posy);
                  $pdf->Cell(30,$cellHeight,utf8_decode("$ ".number_format($value[1]*$value[2], 2, '.', ',')),0, 0 , 'L', false );
                  $pdf->setXY($posx+100,$posy);
                  $pdf->Cell(35,$cellHeight,utf8_decode("$ ".number_format($carAdec, 2, '.', ',')),0, 0 , 'L', false );
                  $pdf->setXY($posx+135,$posy);
                  $pdf->Cell(30,$cellHeight,utf8_decode("$ ".number_format($subAdec, 2, '.', ',')),0, 0 , 'L', false );
                }
                $adecSubAplPila[] = $rowAdecDet['servicio_id'];
              }
              $adecsubtotal = $adecsubtotal+$precioSub;
              $pdf->Cell(40,$cellHeight,utf8_decode("$ ".number_format($precioSub, 2, '.', ',')),0, 1 , 'L', false );              
           }

           error_log(" adecSubAplPila: ".print_r($adecSubAplPila,true));
           
           foreach($enlArr AS $key => $value){
             $posx = $pdf->GetX();
             $posy = $pdf->GetY();
             
             if(!in_array($value[0],$adecSubAplPila)){
               $pdf->MultiCell(70,5, utf8_decode($value[7]." (Cargo pago único)"),0, 'L', false );
             
               /*$enlArr[] = array($row2['servicio_id'],
               $row2['precio_'.$row2['cantidad_vigencia'].'m'],
               $row2['cargo_adecuacion_'.$row2['cantidad_vigencia'].'m'],
               $row2['costo_'.$row2['cantidad_vigencia'].'m'],
               $row2['sub_'.$row2['cantidad_vigencia'].'m'],
               $row2['adec_'.$row2['cantidad_vigencia'].'m'],
               $row2['cantidad_vigencia'],
               $row2['descripción']);*/
             
               $carAdec = $value[2];
               $subAdec = 0;
               
               if(106==$pricId||107==$pricId||114==$pricId||116==$pricId||373==$pricId||497==$pricId||574==$pricId||575==$pricId){
                 $carAdec = 0; 
                 $subAdec = 0;
               }
               
               $precioSub = $carAdec-$subAdec;
               
               if(0>=$precioSub)
                 $precioSub = 0;
               
               $cellHeight = $pdf->GetY()-$posy; //obtiene la altura de la celda de descripcion porque es dinámica
               $pdf->setXY($posx+70,$posy);
               $pdf->Cell(30,$cellHeight,utf8_decode("N/A"),0, 0 , 'L', false );
               $pdf->setXY($posx+100,$posy);
               $pdf->Cell(35,$cellHeight,utf8_decode("$ ".number_format($carAdec, 2, '.', ',')),0, 0 , 'L', false );
               $pdf->setXY($posx+135,$posy);
               $pdf->Cell(30,$cellHeight,utf8_decode("$ ".number_format($subAdec, 2, '.', ',')),0, 0 , 'L', false );
               
               $adecsubtotal = $adecsubtotal+$precioSub;
               $pdf->Cell(45,$cellHeight,utf8_decode("$ ".number_format($precioSub, 2, '.', ',')),0, 1 , 'L', false );
             }   
           }
     
           $adecimpuesto = $adecsubtotal*0.16;
           $adectotal = $adecsubtotal+$adecimpuesto;

           $pdf->Cell(120,5,'',0, 0 , 'L', false );
           $pdf->Cell(45,5,utf8_decode('Subtotal'),0, 0 , 'R', false );
           $pdf->Cell(45,5,utf8_decode("$ ".number_format($adecsubtotal, 2, '.', ',')),0, 1 , 'L', false );
           $pdf->Cell(120,5,'',0, 0 , 'L', false );
           $pdf->Cell(45,5,utf8_decode('Impuesto'),0, 0 , 'R', false );
           $pdf->Cell(45,5,utf8_decode("$ ".number_format($adecimpuesto, 2, '.', ',')),0, 1 , 'L', false );
           $pdf->Cell(120,5,'',0, 0 , 'L', false );
           $pdf->Cell(45,5,utf8_decode('Total'),0, 0 , 'R', false );
           $pdf->Cell(45,5,utf8_decode("$ ".number_format($adectotal, 2, '.', ',')),0, 1 , 'L', false );
            
            
           $query6 = "SELECT DISTINCT(cvts.tipo_servicio_id), cvts.tipo_servicio, cvts.router, cvts.disponibilidad, cvts.latencia, cvts.ab_entregado, cvts.sla ".
                     "FROM ".$cfgTableNameCat.".cat_venta_servicios AS cvs, ".
                             $cfgTableNameCat.".cat_venta_tipo_servicios AS cvts, ".
                             $cfgTableNameMod.".rel_cotizacion_producto AS rcp ".
                     "WHERE cvs.servicio_id = rcp.producto_id ".
                     "AND cvs.tipo_servicio = cvts.tipo_servicio_id ".
                     "AND cvts.categoria_id = 1 ".
                     "AND rcp.cotizacion_id = ".$pricId;
           
           $result6 = mysqli_query($db_modulos,$query6);
           
           if(!$result6){
             $salida = "ERROR||No se pudo obtener los datos de los productos para esta cotización ";
             goto a;
           }
           else{ 
             while($row6 = mysqli_fetch_assoc($result6)){
               $pdf->SetFont('Arial','',10); 
               $pdf->Cell(190,5,utf8_decode($row6['tipo_servicio']),0, 1 , 'L', false );
               if("N/A"!=$row6['router']) {
                 $pdf->Cell(153,5, utf8_decode("Router ".$row6['router']),0, 1 , 'L', false );
               }
               if("N/A"!=$row6['sla']) {
                 $pdf->Cell(153,5, utf8_decode("SLA tiempo máximo de solución de ".$row6['sla']." horas en caso de falla."),0, 1 , 'L', false );
               }
               if("N/A"!=$row6['disponibilidad']) {
                 $pdf->Cell(153,5, utf8_decode("Este servicio tiene una disponibilidad de ".$row6['disponibilidad']."%."),0, 1 , 'L', false );
               }
             }
           }
           //$contactEmails[$row1['venEmail']] = $row1['venNom']." ".$row1['venApe']; //añade al usuario como destinatario también
           
           //imprime las notas y el footer
           
           //imprime los datos del vendedor que hizo el prospecto
           if(270>=$pdf->getY()){
             $pdf->SetTextColor(255,255,255);
             $pdf->SetFont('Arial','B',9); 
             $pdf->Cell(0,5, utf8_decode($row1['venNom']." ".$row1['venApe']." | Ext: ".$row1['venExt']." | Cel.: ".$row1['venTelMovil']." | E-mail: ".$row1['venEmail']),'LRTB',2,'C', true);
             $pdf->Cell(0,5, utf8_decode("Servicio de Soporte: Tel. ".$supportServiceTel." | ".$supportServiceMail),'LRTB',2,'C', true);
             
             if(200<=($pdf->GetY())){
               $pdf->AddPage();
               $pdf->printPriceBackground(0);
               $pdf->setY(30);
             }
           }
    
           //imprime el footer
           
           $pdf->printPriceFooter($row1,$coeTel); 
         }                
     //}  
   }

  if(0==$opt) //Salida al navegador directa
    $pdf->Output(); 
  if(1==$opt){ //Envia por mail la cotización

    $filename = date("YmdHis").$row1['cotizacion_id']."_".str_replace(" ","",$row1['nombre_comercial']);
    $filepath = realpath('../../docs/pdf/cotizaciones/');
    $pdfdoc = $pdf->Output($filepath."/".$filename.".pdf", "F"); //crea un archivo
    
    // Configura con los datos del servidor de correo
    /*$transport = Swift_SmtpTransport::newInstance($cfgmailServer, $cfgmailport);
    $transport->setUsername($cfgmailuser);
    $transport->setPassword($cfgmailpass);*/
   
    //Crea el mailer con los datos del servidor de correos
    //$mailer = Swift_Mailer::newInstance($transport);
    
    /*Register anti flood plugin
      The AntiFlood plugin is designed to help lessen the load on the HTTP server and the SMTP server. It can also be used to send out very large batches of emails when the SMTP server has restrictions in place to limit the number of emails sent in one go. */
    //$mailer->registerPlugin(new Swift_Plugins_AntiFloodPlugin(100, 30));
       
    //Crea mensaje
    //$message = Swift_Message::newInstance();
    
    if((3==$orgStat||6==$orgStat||4==$orgStat)&&(1==$cotStatus||3==$cotStatus||5==$cotStatus||8==$cotStatus)){
      $emailhash = encrypt(uniqId());
      $mensajeHtml .= "<br><br>Para conocer nuestro aviso de privacidad requerimientos técnicos y aceptar la propuesta enviada, es posible haciendo click: <a href=\"".$cfgServerLocation."crm/servicios/confirmnegociationacceptance.php?pricId=".$_POST['cotId']."&hs=".$emailhash."\">Aquí</a>";
      
      //error_log("PRICEPDF pricePost ".$_POST['cotId']);
       
      //error_log("PRICEPDF LINK: ".$mensajeHtml);
      //$mensajeHtml .= "<br><br>Para conocer nuestro aviso de privacidad requerimientos técnicos y aceptar la propuesta enviada, es posible haciendo click: <a href=\"http://localhost/crm/servicios/confirmnegociationacceptance.php?priId=".$pricId."&orgId=".$orgId."&orgType=".$orgType."\">Aquí</a>";
    } 
    else{
      $salida = "ERROR||La cotización se encuentra en proceso de validación. Verifique su estado antes de enviarla";
    }      

    /*if(file_exists('../../img/signatures/'.$usuarioId.'.png')){
      $sigPic = $message->embed(Swift_Image::fromPath('../../img/signatures/'.$usuarioId.'.png'));
      $mensajeHtml .= "<br><hr><img src='".$sigPic."' alt='firma'/>";  
    }*/               

    /*$message->setSubject($asunto);
    $message->setBody($mensajeHtml,'text/html');
    $message->addPart($mensajePlano,'text/plain');
    $message->setFrom($row1['venEmail'], $row1['venNom']." ".$row1['venApe']);
    $message->setTo($contactEmails);
    $attachment = Swift_Attachment::newInstance($pdfdoc, $filename.".pdf", 'application/pdf');*/
    
    //adjunta archivo
    //$message->attach($attachment);
    
    $queryUpd = "UPDATE cotizaciones SET email_hash='".$emailhash."' WHERE cotizacion_id = ".$pricId;
      
    $resultUpd = mysqli_query($db_modulos,$queryUpd);
    
    if(!$resultUpd)
      $salida = "ERROR||No se actualizó el hash de envío de email";    
    else{
      // manda correo
      /*if(!$mailer->send($message,$failure)){
        $salida = "ERROR||Ocurrió un problema al enviar el documento por correo electrónico";
      }
      else{
        foreach($contactEmails as $key => $value){
          writeOnJournal($usuarioId,"Ha enviado un correo a nombre de [".$row1['venNom']." ".$row1['venApe']."] de la cotización con id: [".$row1['cotizacion_id']."] al email [".$key."]");
        }        
        if(3==$orgStat){
          updateOriginStatus($orgType,6,$orgId);
        }
        $salida = "OK||El archivo se ha enviado con éxito";     
      }*/
      if(file_exists($cfgServerLocationAbs.'img/signatures/'.$usuarioId.'.png'))
        $sigPath = $cfgServerLocationAbs.'img/signatures/'.$usuarioId.'.png';
      else
        $sigPath = "";
      
      $mailData = array('dest' => $contactEmails.",".$venEmail,
                        'cc' => $venEmail,
                        'cco' => "",
                        'asunto' => $asunto,
                        'msg' => $mensajeHtml,
                        'adjunto1' => $cfgServerLocationAbs."docs/pdf/cotizaciones/".$filename.".pdf",
                        'adjunto2' => "",
                        'adjunto3' => "",
                        'adjunto4' => "",
                        'adjunto5' => "",
                        'adjunto6' => "",
                        'adjunto7' => "",
                        'adjunto8' => "",
                        'adjunto9' => "",
                        'adjunto10' => "",
                        'adjunto11' => "",
                        'adjunto12' => "",
                        'adjunto13' => "",
                        'adjunto14' => "",
                        'adjunto15' => "",
                        'estatus' => "NO ENVIADO",
                        'fecha_envio' => "0000-00-00 00:00:00",
                        'nIDCorreo' => 1,
                        'folio' => "0",
                        'firma' => $sigPath,
                        'observaciones' => "",
                        'bestado' => "",
                        'remitente' => $venEmail,
                       );   
                      
      if(!sendBDEmail($mailData)){
        $salida = "ERROR||Ocurrió un problema al enviar el documento por correo electrónico ";
      }
      else{
        $contactEmailsArr = explode(",",$contactEmails);
        foreach($contactEmailsArr as $key => $value){
          writeOnJournal($usuarioId,"Ha enviado un correo a nombre de [".$venNom." ".$venApe."] de la cotización con id: [".$pricId."] al email [".$value."]");
        }
        $salida = "OK||El archivo se ha enviado";      
      }
    }
    
    //borra archivo que se envió y ya no se necesita
    //unlink($filepath.$filename);
  }
}
a:
mysqli_close($db_modulos);
mysqli_close($db_catalogos);
mysqli_close($db_usuarios);
echo $salida;

?>
