<?php
session_start();
set_time_limit(0);

include_once "common.php";
include_once "encrypt.php";
include_once "../fpdf/fpdf.php";
//require "../fpdf/forcejustify.php";
require_once '../mail/swiftmailer-master/lib/swift_required.php';

header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

$usuario = $_SESSION['usuario'];
$usuarioId = $_SESSION['usrId'];
$gruposArr = $_SESSION['grupos'];

$salida = "";
$fechaNow = date("Y-m-d");

class PDF extends FPDF
{
  function PrintDecLeft($text){
    $this->SetFont('Courier', 'B', 12);
    $this->SetTextColor(0,0,0);
    $this->MultiCell(85,8,utf8_decode($text),0,'L',true);
    //$this->Cell(85,10, utf8_decode(strtoupper($text)),0,2,'L', false );
  }
  
  function PrintDecRight($text){
    $this->SetX(110);
    $this->SetFont('Courier', 'B', 12);
    $this->SetTextColor(0,0,0);
    $this->MultiCell(85,8,utf8_decode($text),0,'L',true);
    //$this->Cell(85,10, utf8_decode(strtoupper($text)),0,2,'L', false );
  }
  
  function BlackTextMulti($text){
    $this->SetFont('Times', '', 12);
    $this->SetTextColor(0,0,0);
    $this->MultiCell(160,4, utf8_decode($text),'', 'J', false );
    $this->Ln(5);
  }
  
  function NextConsBR(){
    $posY = $this->GetY(); 
    if(260<$posY){
      $this->MultiCell(190,1,'','LRB', 'L', false );
      $this->AddPage();
      $this->MultiCell(190,1,'','LRT', 'L', false );
    }
  }
  
  function NextAnnexedBR($title){
    $posY = $this->GetY(); 
    if(240<$posY){
      $this->AddPage();
      $this->SetFont('Times', 'B', 20);
      $this->Image('../../img/logo_coe.png', 20, 12, 40, 0);
      $this->Cell(190,15, utf8_decode($title),0,1,'C', false );
      $this->Cell(190,1, '','B',1,'C', false );
      $this->Ln(4);
    }
  }
  
  function PrintTable($rows){
    $this->SetFont('Arial', 'B', 12);
    $this->Cell(30,6, utf8_decode('CANTIDAD'),'LRBT',0,'C', false );
    $this->Cell(65,6, utf8_decode('DESCRIPCION'),'LRBT',0,'C', false );
    $this->Cell(30,6, utf8_decode('MONTO'),'BT',0,'C', false );
    $this->Cell(65,6, utf8_decode('OBSERVACIONES'),'LRBT',1,'C', false );
    for($i=0;$i<$rows;$i++){
      $this->Cell(30,6, '','LRB',0,'C', false );
      $this->Cell(65,6, '','RB',0,'C', false );
      $this->Cell(30,6, '','RB',0,'C', false );
      $this->Cell(65,6, '','RB',1,'C', false );
    }
  }
  
  function PrintHeader($titulo,$secLogoPath){
    $this->SetX(-10);
    $this->SetFont('Times', 'B', 12);
    $this->Image('../../img/logo_coe.png', 20, 12, 40, 0);
    if(''!=$secLogoPath)
      $this->Image($secLogoPath, 165, 10, 25, 0);
    $this->SetX(50); 
    $this->MultiCell(120,5, utf8_decode($titulo),0,'C', false );
    //$this->Cell(190,15, utf8_decode($titulo),0,1,'C', false );
    $this->Cell(160,6, '','B',1,'C', false );
    $this->Ln(5);
  }
  
  function regTextTwoColsLeft($headtext){
    $this->SetFillColor(255);
    $this->SetTextColor(0);
    $this->SetFont('Times','',9); 
    $this->MultiCell(85,3,utf8_decode($headtext),0,'J',true);
  }
  
  function regTextTwoColsRight($headtext){
    $this->SetX(110);
    $this->SetFillColor(255);
    $this->SetTextColor(0);
    $this->SetFont('Times','',9); 
    $this->MultiCell(85,3,utf8_decode($headtext),0,'J',true);
  }
  
  function printCover($rowCot,$conDataArr,$rowCont){
    $this->AddPage();
    $this->SetLeftMargin(10);
    $this->Image('../../img/logo_coe.png', 20, 15, 40, 0);
    $this->Ln(8); 
    $this->SetFont('Arial', 'B', 8);
    $this->Cell(45,15, utf8_decode(''),0,0,'C', false );
    $this->MultiCell(100,3, utf8_decode('CARÁTULA DE CONTRATO DE PRESTACIÓN DE SERVICIOS DE TELECOMUNICACIONES'),0, 'C', false );
    $this->Ln(5);
    $this->SetFont('Arial', 'B', 8);
    $this->SetTextColor(0,0,0);
    $this->SetDrawColor(0,0,100);
    $this->SetFillColor(255,255,255);
    $this->Cell(5,5,'',0,0, 'L', false );
    $this->Cell(15,5, utf8_decode('FECHA:'),0,0, 'L', false );
    $this->Cell(35,5,utf8_decode($conDataArr['fecAlta']),0,0 , 'C', false );
    $this->Cell(5,5,'',0,0, 'L', false );
    $this->Cell(20,5, utf8_decode('CLIENTE #:'),0,0, 'L', false );
    $this->Cell(35,5,utf8_decode($conDataArr['regiNumCte']),0,0 , 'C', false );
    $this->Cell(5,5,'',0,0, 'L', false );
    $this->Cell(25,5, utf8_decode('CONTRATO #:'),0,0, 'L', false );
    $this->Cell(35,5,utf8_decode($conDataArr['regiNumCon']),0,1 , 'C', false );
    $this->Ln(2);
    $this->SetFont('Arial', 'B', 8);
    $this->Cell(190,4, utf8_decode('USUARIO: '.strtoupper($rowCot['razon_social']) ),0,1,'L', false );
    
    if(""!=$rowCot['cliNumPoder']&&""!=$rowCot['cliFechaPoder']&&""!=$rowCot['cliNotario']&&""!=$rowCot['cliFolio'])
      $this->Cell(190,4, utf8_decode('ESCRITURA No. '.$rowCot['cliNumPoder'].'    FECHA: '.$rowCot['cliFechaPoder'].'    NOTARIO: '.$rowCot['cliNotario'].'    FOLIO MERCANTIL: '.$rowCot['cliFolio'] ),0,1,'L', false);
    $this->Cell(190,4, utf8_decode('REPRESENTANTE LEGAL: '.$rowCot['repNombre'].' '.$rowCot['repApellidos'] ),0,1,'L', false);
    $this->Cell(190,4, utf8_decode('ESCRITURA No. '.$rowCot['escritura'].'    FECHA: '.$rowCot['fecha_poder'].'    NOTARIO: '.$rowCot['notario'].'    FOLIO MERCANTIL: '.$rowCot['folio_mercantil'] ),0,1,'L', false);
    $this->MultiCell(190,4, utf8_decode('DOMICILIO: '.strtoupper($rowCot['domicilio'])),0, 'L', false );
    $this->Cell(95,4, utf8_decode('RFC EMPRESA: '.strtoupper($rowCot['rfc'])),0,0,'L', false);
    $this->Cell(95,4, utf8_decode('CORREO ELECTRÓNICO: '.strtoupper($rowCot['email'])),0,1,'L', false);
    $this->Cell(190,4, utf8_decode('TEL: '.$rowCot['telefono'].'   TEL. OFICINA: '.$rowCot['telefono2'].'   CEL: '.$rowCot['telefono3'] ),0,1,'L', false);  
    
    $this->Ln(1);
    $posX = $this->GetX();
    $posY = $this->GetY();
    
    $this->Cell(190,5, utf8_decode('DATOS DE CONTACTO Autorizo a las siguientes personas en caso de ser necesario para recibir la instalación del servicio contratado.'),0, 1 , 'L', false );
    
    $contCont = 1;
    foreach($rowCont AS $key => $value){
      $this->Cell(90,4, utf8_decode($contCont.'.-'.$value[0]),0, 0 , 'L', false );
      $this->Cell(37,4, utf8_decode(' CEL: '.$value[1]),0, 0 , 'L', false );
      $this->Cell(63,4, utf8_decode(' EMAIL: '.$value[2]),0, 1 , 'L', false );
      $contCont++;
    }
    
    $this->Cell(190,2, '',0, 1 , 'C', false );
    
    $this->Ln(1);
    $posX = $this->GetX();
    $posY = $this->GetY();
    
    $this->SetFont('Arial', 'B', 10);
    $this->MultiCell(190,5, utf8_decode('FORMA DE PAGO'),0, 'C', false );
    $this->SetFont('Arial', '', 8);
    $this->Rect($posX+20,$posY+6,5,5);
    $this->Cell(30,8, utf8_decode('DEPOSITO ' ),0,0,'L', false );
    $this->Rect($posX+60,$posY+6,5,5);
    $this->Cell(40,8, utf8_decode('TRANSFERENCIA ' ),0,0,'L', false );
    $this->Cell(50,8, utf8_decode('BANCO: ' ),0,0,'L', false );
    $this->Cell(30,8, utf8_decode('CUENTA: ' ),0,1,'L', false );
    $this->setX($posX+95);
    
    $this->Ln(1);
    $posX = $this->GetX();
    $posY = $this->GetY();
    
    $this->SetFont('Arial','B',9); 
    $this->Cell(190,5, 'SERVICIOS CONTRATADOS',0, 1 , 'C', false );
    $this->Cell(20.5,5, 'CANTIDAD',0, 0 , 'C', false );
    $this->Cell(95.5,5, 'SERVICIO',0, 0 , 'C', false );
    $this->Cell(37,5, 'PRECIO UNITARIO',0, 0 , 'C', false );
    $this->Cell(37,5, 'PRECIO TOTAL',0, 1 , 'C', false );
  }
  
  function printCoverFooter(){
    $this->Ln(1);
    $posX = $this->GetX();
    $posY = $this->GetY();

    $this->SetFont('Arial', 'B', 11);
    $this->Cell(190,7, utf8_decode('CONSIDERACIONES'),'LRT',1,'C', false );
    $this->SetFont('Arial', '', 8);
    $this->MultiCell(190,3, utf8_decode('1. "EL PROVEEDOR" deberá efectuar las instalaciones y empezar a prestar el servicio motivo del presente contrato dentro de un plazo que no exceda de 10 días naturales posteriores a su firma. '),'LR', 'J', false );
    $this->NextConsBR();
    $this->MultiCell(190,3, utf8_decode('2. A partir de que "EL USUARIO" cuente con el servicio se empezará a cobrar la mensualidad, dependiendo de la fecha de inicio de servicio, será total o parcialmente la mensualidad correspondiente únicamente al periodo utilizado.'),'LR', 'L', false );
    $this->NextConsBR();
    $this->MultiCell(190,3, utf8_decode('3. En caso de existir imposibilidad física o técnica para la instalación del servicio, este contrato no tendrá validez alguna, por lo que se dará por terminado, sin perjuicio alguno para las partes, "EL PROVEEDOR" deberá de realizar la devolución de todas las cantidades dadas por adelantado, en su caso, dentro de los 10 días naturales siguientes en que se determine dicho supuesto, sin que exista posibilidad de prórroga para "EL PROVEEDOR".'),'LR', 'J', false );
    $this->NextConsBR();
    $this->MultiCell(190,3, utf8_decode('4. "EL USUARIO" conviene en permitir el libre acceso a su domicilio con previo aviso por parte de "EL PROVEEDOR", a los operativos y empleados de este en donde se encuentren las instalaciones, previa presentación de su credencial o tarjeta de identificación, para efecto de inspección, modificación o reparación de las instalaciones en su caso.'),'LR', 'J', false );
    $this->NextConsBR();
    $this->MultiCell(190,3, utf8_decode('5. Para verificar la autenticidad de las credenciales de los operativos y empleados, "EL SUSCRIPTOR", deberá llamar al teléfono del  "EL PROVEEDOR".'),'LR', 'L', false );
    $this->NextConsBR();
    $this->MultiCell(190,3, utf8_decode('6. El ancho de banda contratado se repartirá entre los aparatos conectados simultáneamente. 
    La velocidad contratada estará comprendida entre el modem y el primer punto de acceso a la red de "EL PROVEEDOR".'),'LR', 'J', false );
    $this->NextConsBR();
    $this->MultiCell(190,3, utf8_decode('7. La velocidad contratada estará comprendida entre el modem y el primer punto de acceso a la red de "EL PROVEEDOR"'),'LR', 'J', false );
    $this->NextConsBR();
    $this->MultiCell(190,3, utf8_decode('8. Los equipos financiados y los costos de operaciones de adecuación serán prorrateados entre la vigencia pactada, en caso de que EL USUARIO quiera dar por terminado el presente contrato, deberá cubrir el costo total de dichos conceptos previo a la cancelación, de conformidad a lo pactado en el propio contrato.'),'LRB', 'J', false );
    $this->Ln(2);
    $posY = $this->GetY();
    $posX = $this->GetX();
    $this->SetFont('Arial', 'B', 8);
    $this->MultiCell(90,4, utf8_decode('El Usuario autoriza al Prestador de Servicios para consulta de Buro de Crédito e Historial Crediticio.'),0, 'C', false );
    $this->Cell(90,5, '',0,1,'C', false);
    $this->Cell(90,20, utf8_decode('________________________________________'),0,1,'C', false );
    $this->Cell(90,5, utf8_decode('FIRMA DE "EL USUARIO"'),0,1,'C', false );

    $this->Ln(2);
    $this->SetXY($posX+100,$posY);
    $this->SetFont('Arial', 'B', 8);
    $this->SetX($posX+100);
    $this->MultiCell(90,4, utf8_decode('Bajo protesta de decir verdad, manifiesto que la información personal vertida en el presente documento es verídica y precisa. Y que  he leído y he aceptado los términos del contrato anexo de Prestación de Servicios de Telecomunicaciones.'),0, 'C', false );
    $this->SetX($posX+100);
    $this->Cell(90,15, utf8_decode('________________________________________'),0,1,'C', false );
    $this->SetX($posX+100);
    $this->Cell(90,5, utf8_decode('FIRMA DE "EL USUARIO"'),0,1,'C', false );
  }
}

//Cell(ancho,alto,texto,borde[0:no 1:borde] posicion borde[L:izquierda T:arriba R:derecha B:abajo],posicion posterior[0:derecha 1:siguiente linea 2:abajo],alineación[L:derecha C:centro R:derecha],relleno[true:relleno false:sin relleno],url);

$pdf = new PDF();

$db_modulos = condb_modulos();
$db_catalogos = condb_catalogos();
$db_usuarios = condb_usuarios();

$opt = isset($_GET['option']) ? 0 : $_POST['option'];
$mailflag = isset($_POST['isemail']) ? 1 : 0;

error_log("OPTION ".$opt);

if(0==$opt){
  $cotId     = isset($_GET['cotId'])  ? decrypt($_GET['cotId']) : -1;
  $conId     = isset($_GET['conId'])  ? decrypt($_GET['conId']) : -1;
  $contContr = isset($_GET['conCon']) ? json_decode($_GET['conCon'],true) : -1;
  $orgType   = isset($_GET['orgType']) ? $_GET['orgType'] : -1;
  $conMod    = isset($_GET['option']) ? $_GET['option'] : -1;
  $conTipIns = isset($_GET['tipIns']) ? $_GET['tipIns'] : -1;
}
else if(1<=$opt&&4>=$opt){
  $cotId     = isset($_POST['cotId'])  ? decrypt($_POST['cotId']) : -1;
  $conId     = isset($_POST['conId'])  ? decrypt($_POST['conId']) : -1;
  $orgType   = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
  $conMod    = isset($_POST['option']) ? $_POST['option'] : -1;
  $conTipIns = isset($_POST['tipIns']) ? $_POST['tipIns'] : -1;
  $contContr = array();
}
if(1==$mailflag){
  $contactEmails = array();
  $cotId     = isset($_POST['cotId'])  ? decrypt($_POST['cotId']) : -1;
  $conId     = isset($_POST['conId'])  ? decrypt($_POST['conId']) : -1;
  $orgType   = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
  $conMod    = isset($_POST['option']) ? $_POST['option'] : -1;
  $conTipIns = isset($_POST['tipIns']) ? $_POST['tipIns'] : -1;
  $contContr = isset($_POST['contContractArr']) ? json_decode($_POST['contContractArr'],true) : "";//contactos imprimibles en el contrato
  $contArr   = isset($_POST['contMailContact']) ? json_decode($_POST['contMailContact'],true) : "";//destinatarios mail
  
  $asunto       = isset($_POST['contractMsgSubject']) ? $_POST['contractMsgSubject'] : "Contrato";//asunto
  $mensajeHtml  = isset($_POST['contractMsgHtmlContent']) ? $_POST['contractMsgHtmlContent'] : "";//"<h4><b>Envio cotización</b></h4>";
  $mensajePlano = isset($_POST['contractMsgPlainContent']) ? $_POST['contractMsgPlainContent'] : "";//"Envio cotización";
  
  foreach($contArr as $key => $value){
    $contactEmails[$value[2]] = $value[1];
  }
}

/*error_log("contacto arreglo ".print_r($contContr,true));
error_log("contacto arreglo email".print_r($contArr,true));*/

$fecha = date("Y-m-d H:i:s");

$mesArr = array("N/A","enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre");

switch($orgType){
  case 0:  //si se trata de un prospecto
    $dbname = "prospectos";
    $fieldname = "prospecto_id";
    $typename = "prospecto";
    $asignfield = "asignadoa";
    $nameFieldName = " ";
  break;
  case 1:  //si se trata de un cliente
    $dbname = "clientes";
    $fieldname = "cliente_id";
    $typename = "cliente";
    $asignfield = "usuario_alta";
    $nameFieldName = " ";
  break;   
}

$queryCont = "SELECT con.nombre, con.apellidos, con.tel_movil, con.email ".
              "FROM ".$cfgTableNameMod.".contactos AS con ";

if(0==$conMod){
  $queryCot = "SELECT cot.*, cot.fecha_alta AS cotfecalta, con.*, cli.*, ".$nameFieldName." ubi.*, dirdet.*, ".
              "cli.num_poder AS cliNumPoder, cli.fecha_poder AS cliFechaPoder, cli.notario AS cliNotario, cli.folio_mercantil AS cliFolio, ".
              "rep.nombre AS repNombre, rep.apellidos AS repApellidos, rep.num_poder AS escritura, rep.fecha_poder, rep.notario, rep.folio_mercantil, ".
              "usr.nombre AS venNom, usr.apellidos AS venApe, usr.departamento AS venDepto, usr.puesto AS venPuesto, usr.extension AS venExt, ".
              "usr.tel_movil AS venTelMovil, usr.email AS venEmail, usr.ubicacion_id ".
              "FROM ".$cfgTableNameMod.".cotizaciones AS cot, ".
                      $cfgTableNameMod.".contactos AS con, ".
                      $cfgTableNameMod.".".$dbname." AS cli, ".
                      $cfgTableNameCat.".cat_coe_ubicacion AS ubi, ".
                      $cfgTableNameUsr.".usuarios AS usr, ".
                      $cfgTableNameMod.".direccion_detalles AS dirdet, ".
                      $cfgTableNameMod.".representante_legal AS rep ".
              " WHERE cot.cotizacion_id=".$cotId.
              " AND cot.contacto_id=con.contacto_id".
              " AND cli.".$fieldname."=cot.origen_id".
              " AND cot.tipo_origen=".$orgType.
              " AND usr.ubicacion_id=ubi.ubicacion_id".
              " AND cot.origen_id=dirdet.origen_id".
              " AND dirdet.tipo_origen=cot.tipo_origen".
              " AND rep.representante_id=cli.representante_id".
              " AND cot.usuario_alta=usr.usuario_id"; 
} 
else if(1<=$conMod||4>=$conMod){
  $queryCot = "SELECT cot.*, contr.*, cot.fecha_alta AS cotfecalta, con.*, cli.*, ".$nameFieldName." ubi.*, dirdet.*, ".
              "cli.num_poder AS cliNumPoder, cli.fecha_poder AS cliFechaPoder, cli.notario AS cliNotario, cli.folio_mercantil AS cliFolio, ".
              "rep.nombre AS repNombre, rep.apellidos AS repApellidos, rep.num_poder AS escritura, rep.fecha_poder, rep.notario, rep.folio_mercantil, ".
              "usr.nombre AS venNom, usr.apellidos AS venApe, usr.departamento AS venDepto, usr.puesto AS venPuesto, usr.extension AS venExt, ".
              "usr.tel_movil AS venTelMovil, usr.email AS venEmail, usr.ubicacion_id ".
              "FROM ".$cfgTableNameMod.".cotizaciones AS cot, ".
                      $cfgTableNameMod.".contactos AS con, ".
                      $cfgTableNameMod.".contratos AS contr, ".
                      $cfgTableNameMod.".".$dbname." AS cli, ".
                      $cfgTableNameCat.".cat_estados AS ubi, ".
                      $cfgTableNameUsr.".usuarios AS usr, ".
                      $cfgTableNameMod.".direccion_detalles AS dirdet, ".
                      $cfgTableNameMod.".representante_legal AS rep, ".
                      $cfgTableNameMod.".contrato_anexos AS conan, ".
              "(SELECT contrato_id FROM contrato_anexos WHERE cotizacion_id = ".$cotId.") AS contId ".
              " WHERE conan.contrato_id=contId.contrato_id".
              " AND cot.contacto_id=con.contacto_id".
              " AND cli.".$fieldname."=cot.origen_id".
              " AND cot.tipo_origen=".$orgType.
              " AND contr.contrato_ubicacion = ubi.estado_id".
              //" AND usr.ubicacion_id=ubi.ubicacion_id".
              " AND contr.cotizacion_id=cot.cotizacion_id".
              " AND cot.origen_id=dirdet.origen_id".
              " AND dirdet.tipo_origen=cot.tipo_origen".
              " AND rep.representante_id=cli.representante_id".
              " AND contr.usuario_alta=usr.usuario_id"; 
}

//error_log("query cot ".$queryCot);
if(0==$conMod){
  $querProdSubTbl = "";
  $querProdSubQ = "";
  $querProdSubCon = "AND rcp.cotizacion_id = ".$cotId." ";
}
else if(2==$conMod){
  $querProdSubTbl = "";
  $querProdSubQ = ", ".$cfgTableNameMod.".contratos AS contr ".
                  ", (SELECT contrato_id FROM ".$cfgTableNameMod.".contrato_anexos WHERE cotizacion_id = ".$cotId.") AS contId ";
  $querProdSubCon = "AND contId.contrato_id = contr.contrato_id ".
                    "AND (contr.cotizacion_id = rcp.cotizacion_id OR rcp.cotizacion_id = ".$cotId.") ";
}
else if(1==$conMod||3==$conMod||4==$conMod){
  $querProdSubTbl = $cfgTableNameMod.".contrato_anexos AS conan, ";
  $querProdSubQ = ", ".$cfgTableNameMod.".contratos AS contr ".
                  ", (SELECT contrato_id FROM ".$cfgTableNameMod.".contratos WHERE cotizacion_id = ".$cotId.") AS contId ";
  $querProdSubCon = "AND (contId.contrato_id = conan.contrato_id AND contr.contrato_id = contId.contrato_id) ".
                    "AND (conan.cotizacion_id = rcp.cotizacion_id OR rcp.cotizacion_id = contr.cotizacion_id) ";
}

/*$queryProd = "SELECT rcp.relacion_id, cvs.*, cvv.cantidad_vigencia, cvv.factor, rcp.cantidad, cvts.categoria_id, ".
             "rcp.producto_precio_especial, rcp.producto_autorizado_por, rcp.sitio_asignado, rcp.vigencia, cvc.categoria_id ".
             "FROM ".$cfgTableNameCat.".cat_venta_servicios AS cvs, ".
                     $cfgTableNameCat.".cat_venta_tipo_servicios AS cvts, ".
                     $cfgTableNameMod.".rel_cotizacion_producto AS rcp, ".
                     $cfgTableNameCat.".cat_venta_vigencia AS cvv, ".
                     $cfgTableNameCat.".cat_venta_categorias AS cvc ".
             "WHERE cvs.servicio_id = rcp.producto_id ".
             "AND cvs.tipo_servicio = cvts.tipo_servicio_id ".
             "AND rcp.vigencia = cvv.vigencia_id ".
             "AND cvc.categoria_id = cvts.categoria_id ".
             "AND rcp.cotizacion_id = ".$cotId." ORDER BY cvv.cantidad_vigencia";*/

$queryProd = "SELECT rcp.relacion_id, cvs.*, cvv.cantidad_vigencia, cvv.factor, rcp.cantidad, cvts.categoria_id, cvts.disponibilidad_txt, cvts.latencia_txt, ".
             "cvts.entrega_txt, cvts.ab_entregado_txt, ".
             "rcp.producto_precio_especial, rcp.producto_autorizado_por, rcp.sitio_asignado, rcp.vigencia, cvc.categoria_id, rcp.orden_trabajo_id ".
             "FROM ".$cfgTableNameCat.".cat_venta_servicios AS cvs, ".
                     $cfgTableNameCat.".cat_venta_tipo_servicios AS cvts, ".
                     $cfgTableNameMod.".rel_cotizacion_producto AS rcp, ".
                     $querProdSubTbl.
                     $cfgTableNameCat.".cat_venta_vigencia AS cvv, ".
                     $cfgTableNameCat.".cat_venta_categorias AS cvc ".                   
             $querProdSubQ.   
             "WHERE cvs.servicio_id = rcp.producto_id ".
             "AND cvs.tipo_servicio = cvts.tipo_servicio_id ".
             "AND rcp.vigencia = cvv.vigencia_id ".
             "AND cvc.categoria_id = cvts.categoria_id ".
             $querProdSubCon.
             "ORDER BY rcp.cotizacion_id";
       
$resultCot = mysqli_query($db_modulos,$queryCot);
$resultProd = mysqli_query($db_modulos,$queryProd);

error_log("Queryprod: ".$queryProd); 

if(!$resultCot||!$resultProd){
  $salida = "ERROR|| No se pudo obtener los datos para este contrato";
  goto a;
}
else{
  $rowCot = mysqli_fetch_assoc($resultCot);
  $subtotal = 0;
  $total = 0;
  $subtotalAdec = 0;
  $totalAdec = 0;
  $precio = 0;
  $totalEquip = 0;
  $subtotalEquip = 0;
  
  if(1==$rowCot['estado_id']){
    $coeRazonSocial = "SARIA";
    $coeFechaPoder = "7,167 de Fecha 09 de octubre de 2017";
    $coeRepLegal = "Lic. Juan Emilio Lomeli Acosta, Notario público Número 7 siete de Tonala, Jalisco";
    $coeConcText = "autorización para comercialización de servicios de telecomunicaciones expedida por el Instituto Federal de Telecomunicaciones de la Secretaría de Comunicaciones  y Transportes (SCT).";
    $coeTel = "(449) 322 07 47";
    $secLogoPath = '../../img/logo_saria.png';
    
  }
  else{
    $coeRazonSocial = "COEFICIENTE";
    $coeFechaPoder = "7,837 de Fecha 11 de octubre de 2013";
    $coeRepLegal = "Lic. Mario Antonio Sosa Salcedo, Notario público Número 44 cuarenta y cuatro de Zapopan, Jalisco";
    $coeConcText = "concesión por parte de la Secretaría de Comunicaciones  y Transportes (SCT) para la prestación de los servicios.";
    $coeTel = "(33) 22828282";
    $secLogoPath = '';
  }
  
  $venEmail = $rowCot['venEmail']; 
  $venNom = $rowCot['venNom'];
  $venApe = $rowCot['venApe'];
  
  $contCondition = "";
  $conDataArr = array();
  
  if(0==$conMod){
    foreach($contContr AS $key => $value){
      if(0==$key)
        $contCondition = "WHERE con.contacto_id=".decrypt($value);
      elseif(2>$key)
        $contCondition .= " OR con.contacto_id=".decrypt($value);  
    }
  
    //$regiNumCon = "________";
    //$regiNumCte = "________";
    $fecAltaAnio = "________";
    $fecAltaMes = "__________"; 
    $fecAltaDia = "________";  
    //$contNotas = "";  
    
    $conDataArr['regiNumCon'] = "________";
    $conDataArr['regiNumCte'] = "________";
    $conDataArr['fecAlta'] = $fechaNow;
    $conDataArr['contNotas'] = "";  
  }
  else if(1<=$conMod&&4>=$conMod){
    $contCondition = "WHERE con.contacto_id=".$rowCot['contacto_id1']." OR con.contacto_id=".$rowCot['contacto_id2'];
    //$regiNumCon = $rowCot['contrato_preclave'].$rowCot['contrato_id'];
    //$regiNumCte = $rowCot[$fieldname];
    $fecAlta = explode(' ',$rowCot['fecha_alta']);
    $fecAltaNum = explode('-',$fecAlta[0]);
    $fecAltaAnio = $fecAltaNum[0];
    $fecAltaMes = $mesArr[(int)$fecAltaNum[1]]; 
    $fecAltaDia = $fecAltaNum[2];
    //$contNotas = $rowCot['contrato_notas'];
    
    
    $conDataArr['regiNumCon'] = $rowCot['contrato_preclave'].$rowCot['contrato_id'];
    $conDataArr['regiNumCte'] = $rowCot[$fieldname];
    $conDataArr['fecAlta'] = $fecAlta[0];
    $conDataArr['contNotas'] = $rowCot['contrato_notas'];
  }
  
  $queryCont = $queryCont.$contCondition; 
  
  $resultCont = mysqli_query($db_modulos,$queryCont);
  
  $contactArr = array();
   
  while($rowCont = mysqli_fetch_assoc($resultCont)){
    $contactArr[] = array($rowCont['nombre'].' '.$rowCont['apellidos'],$rowCont['tel_movil'],$rowCont['email']);
  }

  $pdf->AddPage();
  $pdf->SetLeftMargin(25);
  $pdf->PrintHeader('CONTRATO DE PRESTACIÓN Y SUMINISTRO DE SERVICIOS DE TELECOMUNICACIONES',$secLogoPath);
  $pdf->SetFillColor(255,255,255);
  $pdf->SetY(30);
  //$pdf->SetTextColor(114,159,207);
  $pdf->SetTextColor(0,0,0);
  $pdf->SetFont('Times', '', 12);
  //$pdf->MultiCell(160,5, utf8_decode($queryProd),'', 'J', false );
  $pdf->MultiCell(160,4, utf8_decode('QUE CELEBRAN POR UNA PARTE '.$coeRazonSocial.' COMUNICACIONES S.A. DE C.V. A QUIEN EN LO SUCESIVO SE LE DENOMINARÁ "EL PROVEEDOR" REPRESENTADO EN ESTE ACTO POR EL ING. JORGE GERARDO SALDE GONZÁLEZ, Y POR LA OTRA PARTE COMPARECE LA PERSONA CUYO NOMBRE APARECE EN LA CARÁTULA DEL PRESENTE CONTRATO A QUIEN EN LO SUCESIVO Y PARA LOS EFECTOS DE ESTE CONTRATO SE LE DENOMINARÁ COMO "EL USUARIO", DE CONFORMIDAD CON LAS SIGUIENTES DECLARACIONES Y CLÁUSULAS.'),'', 'J', false );
  $pdf->PrintDecLeft('DECLARACIONES');
  $pdf->PrintDecLeft('I.- DECLARA '.$coeRazonSocial.':');
  $pdf->BlackTextMulti('i. Que es una sociedad mercantil debidamente constituida bajo las leyes de los Estados Unidos, Mexicanos, como consta con el testimonio de Escritura pública número '.$coeFechaPoder.', Pasada ante la fe del '.$coeRepLegal.', debidamente inscrito ante el registro público de comercio del estado de Jalisco, cuyo objeto social le permite la celebración del presente contrato.');
  $pdf->BlackTextMulti('ii. Que a la fecha del presente contrato cuenta con la capacidad legal y económica para celebrar el presente contrato ya que cuenta con una '.$coeConcText);
  $pdf->BlackTextMulti('iii. El domicilio de su poderdante, para todos los efectos de este contrato, es el ubicado en Avenida Agustín Yáñez número 2440 en la colonia Arcos Vallarta en el municipio de Guadalajara, Jalisco, México.');
  $pdf->PrintDecLeft('II.- DECLARA EL USUARIO:');
  $pdf->BlackTextMulti('i. Que en la carátula del presente contrato se asentaron sus datos generales, domicilio, datos de localización, Registro Federal de Contribuyentes y que todos los datos asentados en la misma son ciertos. ');
  $pdf->BlackTextMulti('ii. Que ha leído y entendido los términos del presente contrato en su totalidad y que sabe y conoce los términos en los que '.$coeRazonSocial.' realiza la prestación de los servicios, razón por la cual se obliga al cabal cumplimiento de todas y cada una de las obligaciones a las que se compromete en este mismo documento. ');
  $pdf->BlackTextMulti('iii. Que en este acto solicita los servicios que se describen en la misma carátula del presente contrato y que entiende que cualquier otro servicio que requiera se realizará en términos de lo que se pacta en el clausulado de este mismo documento.');
  $pdf->PrintDecLeft('III.- DECLARAN LAS PARTES:');
  //$pdf->BlackTextMulti('');
  $pdf->BlackTextMulti('i.  Que se reconocen recíprocamente el carácter con que comparecen;');
  $pdf->BlackTextMulti('ii. Que celebran el presente contrato, libres de error, dolo, mala fe, violencia, lesión o cualquier vicio que pudiera afectar su voluntad y, consecuentemente, conocen a plenitud la obligatoriedad del acto que se celebra, razón por la que las partes consienten que éste deberá ser interpretado en los términos literales del mismo;');
  $pdf->BlackTextMulti('iii.  Que han leído minuciosamente todos y cada uno de los anexos que se mencionan a lo largo de este instrumento y están de acuerdo con el contenido de los mismos; Ambas partes por lo tanto, manifiestan su entera conformidad en celebrar el presente contrato de prestación de los servicios de telecomunicaciones descritos en la carátula, en las condiciones que en forma voluntaria y de común acuerdo convienen en las siguientes:');
  
  $pdf->AddPage();
  $pdf->PrintHeader('CONTRATO DE PRESTACIÓN Y SUMINISTRO DE SERVICIOS DE TELECOMUNICACIONES',$secLogoPath);
  $pdf->Cell(160,10, utf8_decode('DECLARACIONES'),0,2,'C', false );
  $posX = $pdf->GetX();
  $posY = $pdf->GetY();
  $pdf->PrintDecLeft('CONCEPTOS');
  $pdf->PrintDecLeft('CLAUSULA PRIMERA.-');
  $pdf->regTextTwoColsLeft('Las Partes convienen que los conceptos que a continuación se definen para este Contrato, tendrán ya sea en singular o plural, el significado que se les atribuye, salvo indicación expresa en contrario.');
  $pdf->regTextTwoColsLeft('Carátula.- Documento inicial de este contrato, que forma parte del mismo, en el cual se consignan los datos principales de EL USUARIO, servicios contratados,  forma y monto de pago y demás características particulares de la contratación.');
  $pdf->Ln(4);
  $pdf->regTextTwoColsLeft('Anexo.- Es el documento que detalla ciertas condiciones derivadas del contrato y que forma parte del mismo obligando a las partes, no importando la fecha en la que se suscriban.');
  $pdf->Ln(4);
  $pdf->regTextTwoColsLeft('Acta de Entrega y Recepción.- Documento en el cual se hace constar tanto la instalación del servicio como a las operaciones de adecuación, equipamiento y en su caso los equipo (s) que se entregan en comodato y/o venta con el servicio, haciendo constar la fecha y hora de dicha entrega recepción. ');
  $pdf->Ln(4);
  $pdf->regTextTwoColsLeft('Contrato.- Se refiere a la Carátula, el presente clausulado y sus Anexos, en donde se establecen los derechos y obligaciones contraídas por las partes.');
  $pdf->Ln(4);
  $pdf->regTextTwoColsLeft('Tasa de Interés Interbancaria de Equilibrio ("T.I.I.E.").- Es la tasa de interés interbancaria de equilibrio a plazo de 28 días obtenida y publicada por el Banco de México, según resolución de dicho banco central publicada en el Diario Oficial de la Federación con fecha 23 de marzo de 1995 y de conformidad con lo establecido en la Circular-Telefax 8/96 del propio Banco el 29 de febrero de 1996 dirigida a instituciones de Banca Múltiple, o bien, en su defecto, aquella que la sustituya y que refleje el costo del dinero. ');
  $pdf->Ln(4);
  $pdf->regTextTwoColsLeft('Día Hábil.- Cualquier día del año, con excepción de los sábados, los domingos y los días de descanso obligatorios de conformidad con la Ley Federal del Trabajo vigente en México. ');
  $pdf->Ln(4);
  $pdf->regTextTwoColsLeft('Equipo.- Son todos aquellos aparatos e infraestructura física que hacen posible la prestación del Servicio. Puede haber equipos con costo o sin costo al Usuario, siendo estos detallados en el anexo respectivo.');
  $pdf->Ln(4);
  $pdf->regTextTwoColsLeft('Especificaciones Técnicas.- Los requisitos y aspectos específicos mínimos requeridos para las condiciones de prestación del servicio, contenidos tanto en el Contrato y sus Anexos.');
  $pdf->Ln(4);
  $pdf->regTextTwoColsLeft('Instalaciones.- Es el lugar físico en donde, a solicitud del Usuario, se debe instalar o se encuentra instalado el Equipo para la prestación de los servicios contratados, previo estudios de factibilidad que se realice.');
  $pdf->Ln(4);
  $pdf->regTextTwoColsLeft('Partes.- Se refiere conjuntamente a '.$coeRazonSocial.' y EL USUARIO');
  $pdf->Ln(4);
  $pdf->regTextTwoColsLeft('Servicio(s).- Es aquél (aquellos) a que se hace referencia en la Carátula de este Contrato y/o en cada Anexo de Servicio.');
  $pdf->Ln(4);
  $pdf->regTextTwoColsLeft('Servicios Adicionales o Complementarios.- Son aquellos servicios que no fueron contratados de inicio por EL USUARIO y que se contratan posteriormente a la celebración del contrato, por medio de Anexos.');
  $pdf->Ln(4);
  $pdf->regTextTwoColsLeft('SCT.- Secretaría de Comunicaciones y Transportes. ');
  $pdf->Ln(4);
  $pdf->regTextTwoColsLeft('El Usuario.- Persona física o moral que contrata o recibe los Servicios, cuyos datos de identificación aparecen en la Carátula del presente Contrato o se encuentra físicamente en el lugar de la prestación de los servicios. ');
  $pdf->Ln(4);
  $pdf->SetXY($posX+110,$posY);
  $pdf->regTextTwoColsRight('Periodo de Facturación.-  El lapso de tiempo en el cual se realizan los cortes de cuentas respecto a los servicios contratados, el cual regularmente se limita a periodos mensuales.');
  $pdf->Ln(4);  
  $pdf->regTextTwoColsRight('Fecha de Pago.- La fecha máxima en la que de manera obligatoria tendrá que registrarse el pago de los servicios contratados por parte de EL USUARIO.');
  $pdf->Ln(4);
  $pdf->regTextTwoColsRight('Plazo de Vigencia.- El tiempo que las partes pactan para la adquisición y suministro de los servicios contratados en el presente contrato, mismo plazo que podrá darse por terminado por cualquiera de las partes bajo los términos y condiciones establecidas en el presente documento');
  $pdf->PrintDecRight('DESCRIPCIÓN DE LOS SERVICIOS');
  $pdf->PrintDecRight('CLÁUSULA SEGUNDA.- ');
  $pdf->regTextTwoColsRight('Los servicios pactados en el presente contrato son exclusivamente en materia de Telecomunicaciones y sus respectivas variantes de conformidad a la ley aplicable. Para efectos del presente contrato, los servicios que requiere EL USUARIO y que serán prestados por '.$coeRazonSocial.' se encuentran enlistados en la carátula o anexos de este documento.');
  $pdf->PrintDecRight('OBJETO Y CONDICIONES GENERALES');
  $pdf->PrintDecRight('CLÁUSULA TERCERA. ');
  $pdf->regTextTwoColsRight('Las Partes convienen que '.$coeRazonSocial.' prestará en favor de EL USUARIO, las 24 veinticuatro horas del día los 365 trescientos sesenta y cinco días del año de acuerdo a la vigencia contratada, los Servicios de Telecomunicaciones señalados en la carátula del presente contrato, EL USUARIO podrá contratar servicios adicionales a los originalmente contratados, lo cual deberá realizar invariablemente suscribiendo el o los Anexos respectivos. ');
  $pdf->regTextTwoColsRight(''.$coeRazonSocial.' podrá prestar a EL USUARIO cualquiera de los Servicios establecidos en el presente Contrato, por sí o a través de cualquier tercero que cuente con las autorizaciones gubernamentales o infraestructura necesarias para tales efectos. ');
  $pdf->PrintDecRight('PROCESO DE CONEXIÓN DE SERVICIOS');
  $pdf->PrintDecRight('CLÁUSULA CUARTA.- ');
  $pdf->regTextTwoColsRight('Previo a la firma del presente contrato y sujeto a lo establecido en la carátula y a los Anexos correspondientes, para la prestación de los Servicios las partes aceptan que sucedió lo siguiente: (i) el Usuario señaló el o los posibles puntos de conexión de los Servicios, (ii) una vez realizadas las pruebas de factibilidad por '.$coeRazonSocial.' y comprobados los niveles técnicos y de funcionamiento de los mismos, éste emitió un comunicado interno de viabilidad');
  $pdf->regTextTwoColsRight('A la suscripción del presente contrato, '.$coeRazonSocial.' iniciará los procesos de instalación al domicilio solicitado por EL USUARIO y previamente validado en cuanto a factibilidad por '.$coeRazonSocial.'.');
  $pdf->regTextTwoColsRight(''.$coeRazonSocial.'  a través de su personal de ingeniería determinará en un anexo al presente contrato, la cantidad de maniobras de operaciones y gastos de adecuación que deberá realizar debido a la ubicación geográfica, así como la descripción del equipo o los equipos que se requieran instalar  para brindar los servicios contratados, los mismos que podrán o no ser financiados por '.$coeRazonSocial.' en favor de EL USUARIO, mediante la facturación mensual de servicios correspondiente, así mismo se indicará en dicho anexo los equipos que quedaran en comodato en favor del USUARIO. ');
  $pdf->regTextTwoColsRight('Una vez finalizada la instalación el personal de '.$coeRazonSocial.' realizará el Acta de entrega y recepción, la cual deberá ser firmada por el personal autorizado de EL USUARIO, requisito sin el cual no podrán activarse los servicios contratados. Una vez firmada dicha acta, formará parte del presente contrato, toda vez que a partir de su fecha de firma iniciará a contar la vigencia pactada de los servicios.');
  $pdf->AddPage();
  $pdf->PrintHeader('CONTRATO DE PRESTACIÓN Y SUMINISTRO DE SERVICIOS DE TELECOMUNICACIONES',$secLogoPath);
  $posX = $pdf->GetX();
  $posY = $pdf->GetY();
  $pdf->regTextTwoColsLeft('Si en la fecha y hora programada para la instalación no se encuentra en el domicilio EL USUARIO o alguno de los expresamente autorizados en la carátula del presente documento, se cobrará la cantidad de $500.00 (quinientos pesos 00/100 M/N) más IVA por cada visita que, por causa imputable a EL USUARIO o sus autorizados, sea necesario realizar. ');
  $pdf->regTextTwoColsLeft('Cualquier cambio solicitado por el Usuario a las características de los Servicios contratados, en caso de ser factible, obliga al EL USUARIO al pago de la tarifa devengada  correspondiente. ');
  $pdf->PrintDecLeft('SERVICIOS ADICIONALES.');
  $pdf->PrintDecLeft('CLÁUSULA QUINTA.- ');
  $pdf->regTextTwoColsLeft('En caso de que EL USUARIO requiera cualquier servicio adicional a los anteriores deberá solicitarse por escrito a '.$coeRazonSocial.', y '.$coeRazonSocial.' podrá proporcionarlo amparado en el presente Contrato, siempre y cuando existan capacidades disponibles y no existan costos que requieran un análisis separado del mismo, en el entendido de las Partes deberán adicionar algún anexo para el nuevo servicio. Aquellos casos que requieran un análisis por separado se le informará a EL USUARIO, y se negociará por las partes.');
  $pdf->regTextTwoColsLeft('En el mismo sentido, podrán existir gastos de adecuaciones o inversiones por proyectos especiales, los cuales deberán ser cubiertos previamente y en su totalidad por EL USUARIO, lo anterior previa autorización y factibilidad por parte de '.$coeRazonSocial.'.');
  $pdf->regTextTwoColsLeft(''.$coeRazonSocial.' se reserva el derecho exclusivo de rechazar cualquier solicitud de Servicios adicionales o complementarios, cuando existan situaciones adversas que consideren relevantes para la prestación de los Servicios de conformidad con lo señalado en el presente Contrato, tales como dificultades físicas para la instalación de equipos, condiciones geográficas o climáticas adversas, etc., lo anterior, sin responsabilidad, indemnización o penalidad a cargo de '.$coeRazonSocial.'. ');
  $pdf->PrintDecLeft('FACTURACIÓN Y PAGO.');
  $pdf->PrintDecLeft('CLÁUSULA SEXTA.-  ');
  $pdf->regTextTwoColsLeft('El USUARIO reconoce los servicios solicitados en la carátula del presente contrato y esté se obliga a pagar a '.$coeRazonSocial.', a partir de la fecha de la firma del Acta de entrega y recepción, descritas en la cláusula cuarta del presente contrato, el monto generado en las facturas correspondientes por periodos de 30 o 31 días naturales, y en su caso con los ajustes de tiempo debidos. Las facturas podrán ser emitidas los días 1, 5, 10, 15 y 25 de cada mes, según corresponda el periodo de EL USUARIO.');
  $pdf->regTextTwoColsLeft('En dicha facturación se describirán los cargos realizados por cada uno de los servicios contratados así como el desglose de los gastos de adecuación, el costo financiado de los equipos instalados o en su caso el costo total de los mismos cuando así se haya pactado.');
  $pdf->regTextTwoColsLeft('EL USUARIO estará obligado a pagar en su totalidad el monto facturado a más tardar en la fecha de pago establecida en la misma carátula y/o factura, ');
  $pdf->regTextTwoColsLeft('En el caso de que EL USUARIO no reciba la factura correspondiente de forma física, ya sea por correo o medios electrónicos y/o desconozca su fecha límite de pago, deberá solicitarla mensualmente al Centro de Atención a USUARIOS de '.$coeRazonSocial.', con la finalidad de dar cumplimiento al contenido del presente Contrato.');
  $pdf->regTextTwoColsLeft(''.$coeRazonSocial.' podrá compensar, a su criterio, cualquier ajuste o descuento, contra los adeudos que EL USUARIO presente, a fin de determinar el saldo a pagar.');
  $pdf->PrintDecLeft('FORMA DE PAGO');
  $pdf->PrintDecLeft('CLÁUSULA SÉPTIMA.- ');
  $pdf->regTextTwoColsLeft('EL USUARIO se obliga a pagar dichas facturas a más tardar 05 cinco días posteriores a la fecha de su emisión. Los pagos deberán ser, ya sea por depósitos o por transferencias bancarias en las cuentas señaladas en las facturas respectivas.');
  $pdf->regTextTwoColsLeft(''.$coeRazonSocial.' podrá presentar facturas complementarias por servicios omitidos o incorrectamente facturados y EL USUARIO tendrá obligación de pagarlas de conformidad con la fecha de pago de los Servicios a que se refiere la presente cláusula.');
  $pdf->SetXY($posX+110,$posY);
  $pdf->PrintDecRight('SUSPENSIÓN, INTERESES Y RECONEXIONES');
  $pdf->PrintDecRight('CLÁUSULA OCTAVA.- ');
  $pdf->regTextTwoColsRight('Las partes acuerdan que en caso de que EL USUARIO incumpliera con el pago de la factura en tiempo y forma establecidos en el presente contrato, incurrirá en estado de mora y se procederá a la suspensión automática del servicio, generándose un cobro automático por concepto de reconexión por la cantidad de $500.00 (Quinientos pesos 00/100 M/N) ');
  $pdf->regTextTwoColsRight('más el Impuesto al Valor Agregado. ');//esta es porción del escrito de arriba
  $pdf->regTextTwoColsRight('En caso de que la mora continúe por más de 30 días, '.$coeRazonSocial.' dará aviso de dicho retraso al Buró de Crédito y EL USUARIO se obliga a recorrer el término de la vigencia del presente contrato en la misma proporción de tiempo en que incurrió en falta de pago y a pagar en favor de '.$coeRazonSocial.' un interés moratorio equivalente a la tasa TIIE (Tasa de Interés Interbancario de Equilibrio a 28 días publicada por el Banco de México) mensual vigente publicada en el Diario Oficial de la Federación por el Banco de México, más 1% (uno por ciento). El cómputo de los intereses se calculará desde la fecha en que EL USUARIO incurra en mora y hasta la fecha en que realice el pago.');
  $pdf->regTextTwoColsRight(''.$coeRazonSocial.' podrá dejar de facturar por falta de pago y podrá cancelar definitivamente la prestación de los servicios contratados, sin responsabilidad, en caso de que la mora subsista durante 60 sesenta días continuos. Lo anterior sin menoscabo de la obligación de cumplimiento a cargo de EL USUARIO.');
  $pdf->PrintDecRight('CARGOS NO RECONOCIDOS ');
  $pdf->PrintDecRight('CLAUSULA NOVENA.- ');
  $pdf->regTextTwoColsRight('En caso de existir una inconformidad con la factura, EL USUARIO deberá pagar la cantidad que sí reconoce y enviar por escrito a '.$coeRazonSocial.' su inconformidad u objeción, detallando los motivos dentro de los 05 (cinco) días naturales siguientes a la fecha de facturación  correspondiente. Una vez transcurrido el mencionado plazo sin que EL USUARIO impugne los cargos contenidos en su factura, estos se entenderán aceptados y reconocidos. ');
  $pdf->regTextTwoColsRight('Por lo que se refiere únicamente a los cargos de la factura que se encuentren en proceso de objeción, EL USUARIO no será considerado por '.$coeRazonSocial.' como en estado de mora, hasta en tanto la objeción haya sido resuelta en ese sentido.');
  $pdf->regTextTwoColsRight('La objeción de las facturas que no reúna los requisitos siguientes no tendrá efecto o validez alguna, y, en consecuencia, las facturas y estados de adeudos correspondientes se tendrán por consentidos por EL USUARIO:');

  $pdf->Ln(4);
  $pdf->regTextTwoColsRight('9.1.- Explicación por escrito, en forma detallada, de las causas por las cuales la factura es objetada, misma que servirá como prueba de la objeción de las mismas');
  $pdf->Ln(4);
  $pdf->regTextTwoColsRight('9.2.- Asimismo, entrega de la información detallada descrita anteriormente, en medios electrónicos o magnéticos para ser verificada por parte de '.$coeRazonSocial.'.');
  $pdf->Ln(4);
  $pdf->regTextTwoColsRight('9.3.- Por lo que a las facturas objetadas se refiere se estará a lo siguiente:');
  $pdf->Ln(4);
  $pdf->regTextTwoColsRight('9.3.1 Aquellas facturas que objete EL USUARIO, serán revisadas por las partes para determinar su procedencia y en su caso el monto procedente se deberá cubrir en efectivo dentro de un plazo que no excederá de 30 (treinta) días naturales contados a partir de la recepción de la notificación por escrito de la objeción de la factura por parte de EL USUARIO. ');
  $pdf->Ln(4);
  $pdf->regTextTwoColsRight('9.3.2 Si se determina que dichas contraprestaciones objetadas proceden a favor de '.$coeRazonSocial.', entonces EL USUARIO pagará a '.$coeRazonSocial.' en adición a dichas contraprestaciones, los intereses en términos de lo establecido en el presente contrato, a más tardar 5 (cinco) días naturales contados a partir de la fecha de la notificación correspondiente. ');
  
  $pdf->AddPage();
  $pdf->PrintHeader('CONTRATO DE PRESTACIÓN Y SUMINISTRO DE SERVICIOS DE TELECOMUNICACIONES',$secLogoPath);
  $posX = $pdf->GetX();
  $posY = $pdf->GetY();
  $pdf->regTextTwoColsLeft('9.3.3 Los intereses a que se refieren las cláusulas anteriores, se computarán desde la fecha límite de pago establecido en la factura objetada en que EL USUARIO debió efectuar el pago y hasta la fecha en que éste realice el mismo');
  $pdf->Ln(4);
  $pdf->regTextTwoColsLeft('Por su parte, si se llegare a determinar que dichas cantidades objetadas procedan a favor de EL USUARIO, éste no estará obligado a pagar a '.$coeRazonSocial.' cantidad alguna por dicho concepto en caso de que así se determinase o bien el mismo se obliga a reembolsar la cantidad parcial que resulte aplicable, una vez realizada la aclaración. ');
  $pdf->regTextTwoColsLeft('En caso de que EL USUARIO presente alguna inconformidad que resulte improcedente, se obliga a pagar a '.$coeRazonSocial.' intereses moratorios sobre saldos insolutos calculados a partir del primer día en que EL USUARIO debió de haber liquidado la factura respectiva, de conformidad al clausulado del presente contrato. En caso de que EL USUARIO realice 3 tres reclamaciones improcedentes en el periodo de vigencia contratado, '.$coeRazonSocial.' podrá dar por terminado de manera anticipada el presente contrato sin responsabilidad alguna.');  
  $pdf->PrintDecLeft('IMPUESTOS');
  $pdf->PrintDecLeft('CLÁUSULA DÉCIMA.-');
  $pdf->regTextTwoColsLeft('Durante la vigencia del presente Contrato, cada una de las PARTES será responsable de pagar los impuestos, aportaciones, derechos, aprovechamientos y toda contribución que, conforme a las disposiciones legales y reglamentarias vigentes, les correspondan, ya sea que se causen en el ámbito Federal, Estatal o Municipal. Las PARTES reconocen y aceptan que, en caso de que las autoridades competentes de carácter Federal y/o Estatal y/o Municipal, modifiquen las bases y tasas de los impuestos, aportaciones, derechos, aprovechamientos y contribuciones que se causen conforme al presente contrato, e incluso que dichas autoridades determinen la aplicación de nuevos gravámenes fiscales, el importe total que EL USUARIO deba pagar será ajustado con la inclusión o traslado de éstos, a partir de su entrada en vigor, lo cual quedará reflejado en los comprobantes fiscales que al efecto se expidan.');
  $pdf->PrintDecLeft('GARANTÍAS');
  $pdf->PrintDecLeft('CLÀUSULA DÈCIMA PRIMERA.-  ');
  $pdf->regTextTwoColsLeft(''.$coeRazonSocial.' se compromete a proporcionar servicios  confiables y de alta calidad. Como parte de este compromiso, '.$coeRazonSocial.' se complace en ofrecer a los Clientes elegibles las garantías establecidas en el Anexo 3 en donde se describe detalladamente el tiempo de respuesta en tiempo de fallo y en general todas las condiciones para hacer efectiva la garantía respecto a los siguientes rubros:');
  $posX2 = $pdf->GetX();
  $pdf->setX($posX2+15);
  $pdf->MultiCell(145,5, utf8_decode('
  * Disponibilidad de red
  * Garantía de latencia de red
  * Garantía de entrega de paquetes
  * Garantía de ancho de banda entregado'),'', 'J', false );
  $pdf->Ln(4);
  $pdf->regTextTwoColsLeft('Para efectos de la presente Cláusula, en caso de suscitarse alguna interrupción, falla, u otra situación irregular en el Servicio, el Usuario deberá inmediatamente comunicar dicha falla o interrupción a '.$coeRazonSocial.' a los números telefónicos '.$coeTel.' o al e-mail soporte@'.$coeRazonSocial.'.mx, al efecto señalados en el recibo-factura, lo anterior, a efecto de que '.$coeRazonSocial.' proceda a solucionar la falla o interrupción reportada en los términos previstos.  Queda entendido entre las Partes que los tiempos de corrección de fallas correrán a partir de que la misma sea reportada por el Usuario en términos de lo antes señalado.');
  $pdf->regTextTwoColsLeft('EL USUARIO se obliga a no conectar o reparar por sí o por terceros, equipo alguno, aparato, accesorio y/o dispositivo, relacionado con la prestación de LOS SERVICIOS, sin el previo consentimiento por escrito de '.$coeRazonSocial.'. En caso de contravenir esta disposición, EL USUARIO deberá pagar a '.$coeRazonSocial.' los daños y perjuicios que le ocasione, además de convertirse en causa de rescisión automática del presente Contrato, mediante simple notificación, sin responsabilidad');
  $pdf->SetXY($posX+110,$posY);
  $pdf->regTextTwoColsRight('alguna para '.$coeRazonSocial.' y sin necesidad de previa declaración judicial.'); //parte de parrafo anterior
  $pdf->PrintDecRight('PROHIBICION DE COMERCIALIZACION');
  $pdf->PrintDecRight('CLÁUSULA DECIMA SEGUNDA.-');
  $pdf->regTextTwoColsRight('EL USUARIO no podrá comercializar en forma alguna LOS SERVICIOS contratados con '.$coeRazonSocial.', a menos que acredite contar con la concesión y/o permiso de autoridades respectivas, así como el contrato correspondiente celebrado con '.$coeRazonSocial.' por lo que desde este momento Libera a '.$coeRazonSocial.', de cualquier responsabilidad, reclamación, denuncia o demanda iniciada en su contra por cualquier autoridad o tercero y reembolsarle cualquier cantidad que por este motivo haya erogado. Lo anterior sin perjuicio del derecho de '.$coeRazonSocial.' de ejercer las acciones legales que le competan y dar por rescindido automáticamente el presente Contrato Marco, sin necesidad de declaración judicial previa y sin responsabilidad alguna para '.$coeRazonSocial.'.');
  
  $pdf->PrintDecRight('SEGURIDAD DE LA INFORMACIÓN.');
  $pdf->PrintDecRight('CLÁUSULA DÉCIMA TERCERA.- ');
  $pdf->regTextTwoColsRight('Las partes están de acuerdo en que '.$coeRazonSocial.' no tiene acceso a la información transmitida a través de su Red Pública de Telecomunicaciones, por lo que no será responsable de pérdidas, acceso no autorizado, alteración, robo o destrucción de tal información. No obstante lo anterior, '.$coeRazonSocial.' se reserva el derecho de aplicar las medidas de seguridad que considere necesarias para el uso de LOS SERVICIOS, así como para cumplir con las disposiciones legales aplicables.');
  $pdf->PrintDecRight('RESPONSABILIDAD SOBRE LOS EQUIPOS.');
  $pdf->PrintDecRight('CLÁUSULA DÈCIMA CUARTA.-  ');
  $pdf->regTextTwoColsRight('En caso de ser aplicable, los equipos que se utilizan al momento de la instalación para el debido funcionamiento y operación del servicio son propiedad de EL USUARIO mismos que serán financiados y se pagaran con una parte proporcional de pago mensual. De igual forma habrá equipo e implementos que se otorguen en comodato, mismos que serán descritos en el correspondiente Anexo 1 (Acta de entrega recepción). EL USUARIO se obliga a pagar a '.$coeRazonSocial.' por los daños y perjuicios ocasionados por negligencia o mal uso de los equipos, instalaciones y/o dispositivos, inclusive vandalismo, turbas, aglomeración de personas, etc.');
  $pdf->Ln(4);
  $pdf->regTextTwoColsRight('Para el caso de que el presente contrato se dé por terminado de manera anticipada a la vigencia pactada y EL USUARIO tenga en su poder y/o pendiente de pago, equipo financiado, cobros por operaciones de adecuación o algún otro concepto pendiente, como requisito previo a su terminación, EL USUARIO deberá cubrir el total de los pagos pendientes por los conceptos antes mencionados.');
  $pdf->Ln(4);
  $pdf->PrintDecRight('RESPONSABILIDAD SOBRE EL USO DE LOS SERVICIOS');
  $pdf->PrintDecRight('CLÁUSULA DECIMA QUINTA.- ');
  $pdf->regTextTwoColsRight('EL USUARIO se obliga a que los SERVICIOS son y seguirán siendo utilizados de conformidad con las disposiciones legales y administrativas aplicables, entre las que destacan, la Ley Federal de Telecomunicaciones y Radiodifusión, la Ley de Vías Generales de Comunicación, las Reglas del Servicio de Larga Distancia y las Reglas de Telecomunicaciones Internacionales. Por lo que EL USUARIO asume la responsabilidad del uso que haga de los SERVICIOS a un fin distinto al objeto de este contrato, así como del tráfico que curse a través de los mismos, obligándose a sacar en paz y a salvo a '.$coeRazonSocial.' de cualquier controversia que surja al respecto, por su parte');
  $pdf->AddPage();
  $pdf->PrintHeader('CONTRATO DE PRESTACIÓN Y SUMINISTRO DE SERVICIOS DE TELECOMUNICACIONES',$secLogoPath);
  $posX = $pdf->GetX();
  $posY = $pdf->GetY();
  $pdf->regTextTwoColsLeft(''.$coeRazonSocial.' no será responsable por cualquier daño, perjuicio o hecho ilícito, tales como, entre otros, manipulación indebida del tráfico o de la señalización, intervención o interferencia del tráfico, alteración, robo, destrucción, uso de equipo, software, datos o sistemas o cualquier ilícito  que cometa EL USUARIO o cualquier tercero como consecuencia del uso indebido de cualquier red pública o privada de Telecomunicaciones de terceros o de '.$coeRazonSocial.'.');
  $pdf->PrintDecLeft('CASO FORTUITO O DE FUERZA MAYOR');
  $pdf->PrintDecLeft('CLÀUSULA DECIMA SEXTA.-');
  $pdf->regTextTwoColsLeft(''.$coeRazonSocial.' no será responsable por la suspensión, interrupción o detrimento de LOS SERVICIOS ocasionados por causas de fuerza mayor, situaciones climáticas, geográficas, caso fortuito, o bien por aquellas que no le sean imputables ni previsibles, incluyendo fallas de transmisión, así como la suspensión o interrupción de comunicaciones por otras redes a través de las cuales puedan cursarse las señales o tráfico de EL USUARIO por dichas situaciones.');
  $pdf->regTextTwoColsLeft(''.$coeRazonSocial.' hará su mejor esfuerzo para restablecer LOS SERVICIOS a la brevedad bajo las condiciones de calidad, funcionalidad y aspectos técnicos que tengan registrados ante las autoridades competentes. ');
  $pdf->PrintDecLeft('OBLIGACIONES DE COEFICIENT');
  $pdf->PrintDecLeft('CLÁUSULA DÉCIMA SEPTIMA.- ');
  $pdf->regTextTwoColsLeft('Son obligaciones de '.$coeRazonSocial.': ');
  $pdf->regTextTwoColsLeft('17.1. Instalar, mantener y operar la red pública de telecomunicaciones (la "Red"), hasta el punto terminal de conexión en el límite interior del inmueble en donde se encuentra localizado el domicilio de EL USUARIO o en donde se haya determinado su instalación, aclarando previamente los cargos que por operaciones de adecuación correspondan a EL USUARIO. ');
  $pdf->Ln(4);
  $pdf->regTextTwoColsLeft('17.2. Instalar el dispositivo de interconexión terminal, en el límite interior del inmueble en donde se encuentra localizado el domicilio de EL USUARIO.');
  $pdf->Ln(4);
  $pdf->regTextTwoColsLeft('17.3. En su caso, suministrar a solicitud de EL USUARIO, y mediante un cargo específico, un primer aparato telefónico básico o modem o equipo necesario, así como su instalación incluyendo el cableado necesario en el inmueble de EL USUARIO hasta el dispositivo de interconexión terminal a cargo de EL USUARIO. ');
  $pdf->Ln(4);
  $pdf->regTextTwoColsLeft('17.4. En su caso, suministrar a solicitud de EL USUARIO y mediante un cargo específico, un aparato telefónico básico o modem o equipo necesario y el cableado necesario dentro del inmueble del Usuario. ');
  $pdf->Ln(4);
  $pdf->regTextTwoColsLeft('17.5. Iniciar la prestación del Servicio en un plazo máximo de 48 (cuarenta y ocho) horas contados a partir de la fecha en que se adecúe e instale el equipo de interconexión del Servicio, previa recepción por parte de EL USUARIO o sus autorizados.');
  $pdf->Ln(4);
  $pdf->regTextTwoColsLeft('17.6. Facturar a EL USUARIO de manera mensual los conceptos aplicables respecto a los servicios contratados acordados por las Partes. La fecha de corte para el Usuario será el estipulado en la caratula del presente Contrato Marco. ');
  $pdf->Ln(4);
  $pdf->regTextTwoColsLeft('17.7. Enviar al usuario la factura correspondiente a los Servicios.');
  $pdf->Ln(4);
  $pdf->regTextTwoColsLeft('17.8. Dentro del procedimiento de objeción de facturas, realizar las investigaciones o verificaciones necesarias sobre las llamadas que EL USUARIO no reconozca, siguiendo los mecanismos descritos para tal efecto en el presente Contrato Marco.');
  $pdf->Ln(4);
  $pdf->PrintDecLeft('OBLIGACIONES DEL USUARIO');
  $pdf->PrintDecLeft('CLÁUSULA DÉCIMA OCTAVA.- ');
  $pdf->SetXY($posX+110,$posY);
  $pdf->regTextTwoColsRight('Son obligaciones del Usuario: ');
  $pdf->regTextTwoColsRight('18.1. Cumplir con los requerimientos que le señale '.$coeRazonSocial.' en el presente Contrato y sus Anexos. ');
  $pdf->Ln(4);
  $pdf->regTextTwoColsRight('18.2. Proporcionar a '.$coeRazonSocial.' la información que ésta le requiera, así como informar de forma inmediata (siguientes 48 cuarenta y ocho horas a que suceda) cualquier cambio respecto a la información contenida en la carátula del presente contrato; ');
  $pdf->Ln(4);
  $pdf->regTextTwoColsRight('18.3. Realizar todos los pagos de las cantidades que se generen por Servicios contratados, en tiempo y forma, así como los gastos de operaciones de adecuación, equipos, etc.; ');
  $pdf->Ln(4);
  $pdf->regTextTwoColsRight('18.4. No ceder, total ni parcialmente, las obligaciones o derechos derivados de este Contrato a terceros sin el previo consentimiento por escrito de '.$coeRazonSocial.'.');
  $pdf->Ln(4);
  $pdf->regTextTwoColsRight('18.5 Abstenerse de comercializar o ceder el uso los Servicios otorgados por '.$coeRazonSocial.' a terceros, excepto cuando cuente con las concesiones, permisos y autorizaciones gubernamentales que sean necesarias para ello, previa autorización y convenio con '.$coeRazonSocial.'.');
  $pdf->Ln(4);
  $pdf->regTextTwoColsRight('18.6 Responder por las comunicaciones originadas en las instalaciones puestas a su servicio, o recibidas en ellas y cuyo importe sea cargado en el recibo de pago. ');
  $pdf->Ln(4);
  $pdf->regTextTwoColsRight('18.7. Pagar a '.$coeRazonSocial.' los gastos que se generen por  reconexión, cobranza,  localización, en caso de mora o no haber manifestado su cambio de domicilio y los derivados de las gestiones de abogados, cuando tenga adeudos pendientes por cubrir a '.$coeRazonSocial.'.' );
  $pdf->Ln(4);
  $pdf->regTextTwoColsRight('18.8. Pagar a '.$coeRazonSocial.' los gastos de cobranza y los derivados de las gestiones de abogados sean de carácter extrajudicial o judiciales, cuando se requiera su intervención en virtud de existir adeudos en pago de servicios, así como en aquellos casos en que EL USUARIO no permita el acceso a personal de '.$coeRazonSocial.'  para desmontar equipo o este no sea devuelto en tiempo a éste último.');
  $pdf->Ln(4);
  $pdf->PrintDecRight('PROHIBICIONES PARA EL USUARIO');
  $pdf->PrintDecRight('CLÁUSULA DECIMA NOVENA.- ');
  $pdf->regTextTwoColsRight('19.1. EL USUARIO es consciente y tiene el conocimiento que tiene prohibido reproducir, difundir o transmitir información ilegal, obscena, o prohibida por la legislación mexicana.');
  $pdf->Ln(4);
  $pdf->regTextTwoColsRight('19.2. Reproducir, difundir o transferir información, archivos o software, deliberadamente afectados por algún virus o componentes dañinos que pudieren de alguna manera afectar el servicio que se está prestando.');
  $pdf->Ln(4);
  $pdf->regTextTwoColsRight('19.3. Usar fraudulentamente el servicio en cualquier forma que contravenga las especificaciones incluidas en este anexo.');
  $pdf->Ln(4);
  $pdf->regTextTwoColsRight('19.4. Transmitir, copiar, pegar, publicar, reproducir o distribuir en cualquier forma, información, software o cualquier otro material obtenido a través de Internet, el cual esté protegido por derechos de propiedad industrial o intelectual o cualquier otro derecho, sin obtener permiso previo del propietario o titular del derecho y nuevas contrataciones que requiera para los servicios de Internet, de Red  y otros que ofrezca o llegare a ofrecer EL USUARIO.');
  $pdf->Ln(4);
  $pdf->regTextTwoColsRight('19.5. Que por medio de la Red de '.$coeRazonSocial.' realice actividades, así como transmitir o cursar tráfico By-Pass a través de su red.');
  $pdf->Ln(4);
  $pdf->regTextTwoColsRight('19.6. Que sustraiga del domicilio autorizado, dilapide, dañe, modifique o intente repara por si o a través de terceros, los equipos brindados por '.$coeRazonSocial.', ya sean propios o en comodato.');
  $pdf->AddPage();
  $pdf->PrintHeader('CONTRATO DE PRESTACIÓN Y SUMINISTRO DE SERVICIOS DE TELECOMUNICACIONES',$secLogoPath);
  $posX = $pdf->GetX();
  $posY = $pdf->GetY();
  $pdf->regTextTwoColsLeft('Siendo las anteriores causales de rescisión y terminación del presente Contrato de prestación de servicios, sin responsabilidad para '.$coeRazonSocial.'.');
  $pdf->Ln(4);
  $pdf->PrintDecLeft('LÍMITE DE RESPONSABILIDAD.');
  $pdf->PrintDecLeft('CLÁUSULA VIGESIMA.- ');
  $pdf->regTextTwoColsLeft('EL USUARIO y sus usuarios autorizados, reconocen y aceptan que ni '.$coeRazonSocial.', ni ninguna de sus filiales opera o controla Internet y que todos los contenidos, productos, información o servicios disponibles en Internet son ofrecidos por terceras partes, por lo que '.$coeRazonSocial.' no tendrá obligación alguna con respecto de los mismos. En tal virtud EL USUARIO asume toda la responsabilidad y riesgo que derive del uso y empleo de los servicios que brinde '.$coeRazonSocial.'. En relación con cualquier comercialización, información o suministro de servicios a través de Internet, '.$coeRazonSocial.' y sus filiales no serán responsables de la idoneidad de los productos que se adquieran, ni de la comercialización de los productos o servicios que se anuncian, ni de ningún daño o perjuicio derivado directa o indirectamente de cualquiera de dichas transacciones o contenidos.');
  $pdf->regTextTwoColsLeft('Será exclusivamente responsabilidad de EL USUARIO y de los usuarios autorizados el evaluar la exactitud, utilidad, veracidad y aprovechamiento de las opiniones, informes, servicios y cualquier otra información, así como la calidad y comercialización de todos los productos, proporcionados a través del servicio que brinde '.$coeRazonSocial.'.');
  $pdf->regTextTwoColsLeft('Sera responsabilidad de EL USUARIO la seguridad en el acceso a sus servidores, ruteadores y demás equipos conectados al servicio de '.$coeRazonSocial.'; por lo que es su responsabilidad la instalación de equipos y/o programas necesarios para mantener la seguridad de su información, tales como "firewalls", antivirus, etc. '.$coeRazonSocial.' no garantiza que la información solicitada o enviada, estará libre de errores, o que la información, software o cualquier otro material que se maneje en el Internet esté libre de virus o componentes dañinos.');
  $pdf->regTextTwoColsLeft(''.$coeRazonSocial.' manifiesta quedar libre de toda responsabilidad respecto de llamadas y/o por envió o recepción de información, contenido, formato, tipo-modo o utilización que EL USUARIO transmita o curse a través de la red de telecomunicaciones de '.$coeRazonSocial.' o de terceros con los cuales '.$coeRazonSocial.' haya contratado, por lo que EL USUARIO en este acto se obliga a utilizar LOS SERVICIOS única y exclusivamente de acuerdo al límite de uso contratado y para actividades lícitas, absteniéndose de transmitir o reproducir  material o información prohibida por las leyes mexicanas y tratados internacionales, o bien, que constituya o fomente un comportamiento que conduzca a la comisión de un delito o dé lugar a responsabilidades civiles y penales . Por lo que desde ahora EL USUARIO se obliga a sacar en paz a salvo a '.$coeRazonSocial.' de cualquier reclamación, denuncia, demanda, querella, procedimiento, judicial que pudiera fincarse en su contra.');
  $pdf->Ln(4);
  $pdf->PrintDecLeft('ACCESO A LAS INSTALACIONES DEL USUARIO.');
  $pdf->PrintDecLeft('CLÁUSULA VIGÉSIMA PRIMERA.- ');
  $pdf->regTextTwoColsLeft('Por medio del presente EL USUARIO deberá permitir el acceso al personal de '.$coeRazonSocial.' o sus subcontratistas entre las 7:00 hrs y las 19:00 hrs de los 365 días del año, quienes deberán identificarse, a los lugares en los cuales se presten los SERVICIOS o se encuentre instalado EQUIPO o medios de transmisión asociados al mismo, con el fin de realizar la instalación, mantenimiento, operaciones de adecuación y/o reparaciones en su caso.');
  $pdf->Ln(4);
  $pdf->PrintDecLeft('ACONDICIONAMIENTO DEL INMUEBLE.');
  $pdf->PrintDecLeft('CLÁUSULA VIGÉSIMA SEGUNDA.- ');
  $pdf->regTextTwoColsLeft('Podrá ser, en caso de ser necesario, a cargo de EL USUARIO el pago de las operaciones de adecuaciones necesarias y suficientes'); 
  $pdf->SetXY($posX+110,$posY);
  $pdf->regTextTwoColsRight('para la debida instalación de los equipos necesarios para que '.$coeRazonSocial.' brinde los servicios contratados, por medio de equipos e implementos que pueden ser otorgados en comodato o financiados o subsidiados por '.$coeRazonSocial.', los cuales en su caso,  se podrán prorratear de forma mensual de acuerdo al plazo de vigencia contratado en la carátula del presente Contrato o los Anexos correspondientes.');//fragmento parrafo anterior
  $pdf->regTextTwoColsRight('Para el caso de que el presente contrato se dé por terminado de manera anticipada a la vigencia pactada y EL USUARIO tenga en su poder y/o pendiente de pago, equipo financiado, cobros por operaciones de adecuación o algún otro concepto pendiente, como requisito previo a su terminación, EL USUARIO deberá cubrir el total de los pagos pendientes por los conceptos antes mencionados.');
  $pdf->PrintDecRight('CESIÓN DE DERECHOS Y OBLIGACIONES.');
  $pdf->PrintDecRight('CLÁUSULA VIGÉSIMA TERCERA.- ');
  $pdf->regTextTwoColsRight('EL USUARIO no podrá ceder sus derechos y obligaciones derivadas del presente Contrato sin el previo consentimiento por escrito de '.$coeRazonSocial.'. Cualquier cesión que se realice sin el previo consentimiento de éste se considerará nula; por su parte, '.$coeRazonSocial.' podrá ceder sus derechos y obligaciones a favor de cualesquiera de sus controladoras, subsidiarias, filiales, afiliadas o empresas del grupo de inversionistas al que pertenece, o cuando se trate de la cesión de sus derechos de cobro contra EL USUARIO.');
  $pdf->PrintDecRight('CONFIDENCIALIDAD.');
  $pdf->PrintDecRight('CLÁUSULA VIGESIMA CUARTA.-');
  $pdf->regTextTwoColsRight('EL USUARIO conviene en que los precios acordados, los términos y condiciones de este Contrato, así como la información que reciba en consecuencia de la presente relación contractual, será considerada como "Información Confidencial" por lo que se abstendrá de divulgar la misma a terceras personas sean físicas o morales, así como de utilizarla en beneficio propio, durante la vigencia del presente Contrato y una vez terminado éste, por un plazo de 5 (Cinco) años. El uso indebido de la "Información Confidencial" que realice EL USUARIO, dará derecho a '.$coeRazonSocial.' para rescindir el presente Contrato sin necesidad de declaración judicial, en cuyo caso EL USUARIO se obliga a pagar el monto de los daños y perjuicios que '.$coeRazonSocial.' determine que le fueron ocasionados, independientemente del derecho que tenga de ejercitar las acciones legales a que haya lugar.');
  $pdf->regTextTwoColsRight('En el supuesto de que alguna autoridad competente requiera cualquier información, EL USUARIO se obliga a hacer del conocimiento de '.$coeRazonSocial.' tal situación. Por su parte EL USUARIO acepta que '.$coeRazonSocial.' podrá entregar información comercial relativa al presente Contrato, y confidencial sólo cuando alguna autoridad competente así lo requiera.');
  $pdf->PrintDecRight('PROPIEDAD INDUSTRIAL E INTELECTUAL');
  $pdf->PrintDecRight('CLÁUSULA VIGESIMA QUINTA.- ');
  $pdf->regTextTwoColsRight('EL USUARIO se obliga a no reproducir o utilizar las marcas, diseños, logos, avisos, comerciales, nombres, comerciales y demás derechos de propiedad industrial e intelectual, utilizadas o registradas a nombre de '.$coeRazonSocial.', o de cualquiera de sus filiales, subsidiarias o licenciadas a estas, sin el previo consentimiento por escrito de '.$coeRazonSocial.', igual limitación se extiende a aquellas que tengan coincidencias tales que puedan causar confusión con las mismas. Por lo que EL USUARIO reconoce que por la firma del presente Contrato no adquiere derecho alguno sobre los derechos de propiedad industrial o intelectual de '.$coeRazonSocial.', enunciando, sin que esto constituya limitación alguna, patentes, marcas, nombres o avisos comerciales, derechos de autor, así como licencias, permisos, autorizaciones de uso de propiedad industrial e intelectual, derivado del cumplimiento del presente Contrato. ');
  
  $pdf->AddPage();
  $pdf->PrintHeader('CONTRATO DE PRESTACIÓN Y SUMINISTRO DE SERVICIOS DE TELECOMUNICACIONES',$secLogoPath);
  $posX = $pdf->GetX();
  $posY = $pdf->GetY();
  $pdf->regTextTwoColsLeft('Si EL USUARIO contraviniera lo que en esta cláusula se estipula, se obliga a su costa a defender e indemnizar por los daños y perjuicios, y a sacar en paz y a salvo a '.$coeRazonSocial.' en contra de cualquier reclamación, demanda o procedimiento judicial instaurado en su contra, con independencia de tener que responder por los daños y perjuicios que dicho uso indebido le cause a '.$coeRazonSocial.'.');
  $pdf->PrintDecLeft('RECISIÓN DE CONTRATO ');
  $pdf->PrintDecLeft('CLÁUSULA VIGESIMA SEXTA.-');
  $pdf->regTextTwoColsLeft('Además de las causas de recisión contenidas en las cláusulas del presente contrato, EL USUARIO podrá solicitar la terminación anticipada del mismo, siempre y cuando esté al corriente de sus pagos y cubra el monto correspondiente a la totalidad de las operaciones de Adecuación realizadas por '.$coeRazonSocial.', además de la liquidación del pago por los equipos financiados o subsidiados, mismos que una vez finiquitados serán de propiedad de EL USUARIO. Así como las demás disposiciones estipuladas en el presente documento. ');
  $pdf->PrintDecLeft('RELACIÓN LABORAL.');
  $pdf->PrintDecLeft('CLÁUSULA VIGESIMA SEPTIMA.- ');
  $pdf->regTextTwoColsLeft('Las partes reconocen que no existe relación laboral alguna entre los empleados de '.$coeRazonSocial.' y EL USUARIO ni entre los empleados de EL USUARIO y '.$coeRazonSocial.' y por lo tanto cada una de las PARTES será la única responsable y principal obligado para con sus respectivos empleados de todas y cada una de las obligaciones laborales.');
  $pdf->PrintDecLeft('AVISO DE PRIVACIDAD.');
  $pdf->PrintDecLeft('CLÀUSULA VIGESIMA OCTAVA.- ');
  $pdf->regTextTwoColsLeft('A la firma del presente contrato y la caratula correspondiente EL USUARIO otorga su autorización a '.$coeRazonSocial.'  para utilizar su información proporcionada con fines mercadológicos, técnicos y/o publicitarios, así como también para transferir a terceros dicha información para fines publicitarios. No obstante lo anterior, '.$coeRazonSocial.' está obligado a tratar la información de EL USUARIO de conformidad con la normatividad aplicable en la materia. EL USUARIO puede consultar el aviso de privacidad completo que se encuentra en la página  de internet www.'.$coeRazonSocial.'.mx.');
  $pdf->PrintDecLeft('RENOVACION');
  $pdf->PrintDecLeft('CLÀUSULA VIGESIMA NOVENA.-');
  $pdf->regTextTwoColsLeft('Al terminar la vigencia del presente contrato, EL USUARIO deberá de dar aviso de su deseo de terminación de la prestación de servicios si es que así lo deseará, con 30 (treinta) días de anticipación, de lo contrario se dará por renovado automáticamente y '.$coeRazonSocial.' realizara las adecuaciones necesarias para el mantenimiento debido, para el buen funcionamiento de los equipos, actualizándolos o renovándolos de ser necesario, mismos que de igual forma serán financiados por '.$coeRazonSocial.' y EL USUARIO los ira pagando en forma mensual junto con su factura correspondiente al servicio.');
  
  $pdf->SetXY($posX+110,$posY);
  $pdf->PrintDecRight('DOMICILIOS Y NOTIFICACIONES');
  $pdf->PrintDecRight('CLÀUSULA TRIGESIMA.- ');
  $pdf->regTextTwoColsRight('Las partes reconocen los domicilios establecidos en la caratula del presente Contrato Macro y en su apartado de Declaraciones, respectivamente, sin embargo se obligan a hacer del conocimiento entre ellas de cualquier cambio de domicilio, mismo que deberá ser por escrito. En caso de omitir dicho aviso se tendrá por notificado el estipulado en la caratula, lo anterior para efectos de cualquier notificación que fuese necesaria, judicial o extrajudicialmente, originada por las obligaciones contraídas en el presente documento.');
  $pdf->PrintDecRight('LEYES APLICABLES Y JURISDICCIÓN');
  $pdf->PrintDecRight('CLÀUSULA TRIGESIMA PRIMERA.-');
  $pdf->regTextTwoColsRight('El presente Contrato se sujetará a los términos y condiciones estipulados por las Partes en el mismo instrumento, el cual estará regulado por las disposiciones del Código de Comercio, leyes en materia de Telecomunicaciones en los Estados Unidos Mexicanos, así como a las leyes federales y del estado de Jalisco, y los tratados internacionales suscritos y ratificados por el estado Mexicano y el senado de la Republica.');
  $pdf->regTextTwoColsRight('Para cualquier controversia relacionada con la interpretación, cumplimiento, incumplimiento, ejecución o cualquier otra relacionada con este Contrato, las Partes se someten expresamente a la jurisdicción y leyes de los tribunales federales o locales, competentes en Guadalajara, Jalisco renunciando expresamente a cualquier otro fuero que pudiere corresponderles por razón de sus domicilios presentes o futuros o por cualquier otra razón.');
  $pdf->PrintDecRight('DATOS REGISTRALES');
  $pdf->regTextTwoColsRight('Este contrato fue aprobado y registrado por la Procuraduría federal del Consumidor bajo el No. '.$conDataArr['regiNumCon'].' de fecha '.$fecAltaDia.' de '.$fecAltaMes.' del '.$fecAltaAnio.'. Cualquier variación del presente contrato en perjuicio de EL USUARIO, frente al contrato de adhesión registrado se tendrá por no puesta.');
  $pdf->regTextTwoColsRight('Leído que fue el presente contrato por las partes y bien enterados de su alcance, contenido y fuerza legal, manifestaron su conformidad con el mismo, firmando al calce para constancia en el lugar y  fecha referida en la carátula del mismo. ');

//desplegado de productos
 /* $pdf->AddPage();
  $pdf->SetLeftMargin(10);
  $pdf->Image('../../img/logo_coe.png', 20, 15, 40, 0);
  $pdf->Ln(8);
  $pdf->SetFont('Arial', 'B', 8);
  $pdf->Cell(45,15, utf8_decode(''),0,0,'C', false );
  $pdf->MultiCell(100,3, utf8_decode('CARÁTULA DE CONTRATO DE PRESTACIÓN DE SERVICIOS DE TELECOMUNICACIONES'),0, 'C', false );
  $pdf->Ln(5);
  $pdf->SetFont('Arial', 'B', 8);
  $pdf->SetTextColor(0,0,0);
  $pdf->SetDrawColor(0,0,100);
  $pdf->SetFillColor(255,255,255);
  $pdf->Cell(5,5,'',0,0, 'L', false );
  $pdf->Cell(15,5, utf8_decode('FECHA:'),0,0, 'L', false );
  $pdf->Cell(35,5,utf8_decode($fechaNow),0,0 , 'C', false );
  $pdf->Cell(5,5,'',0,0, 'L', false );
  $pdf->Cell(20,5, utf8_decode('CLIENTE #:'),0,0, 'L', false );
  $pdf->Cell(35,5,utf8_decode($regiNumCte),0,0 , 'C', false );
  $pdf->Cell(5,5,'',0,0, 'L', false );
  $pdf->Cell(25,5, utf8_decode('CONTRATO #:'),0,0, 'L', false );
  $pdf->Cell(35,5,utf8_decode($regiNumCon),0,1 , 'C', false );
  $pdf->Ln(2);
  $pdf->SetFont('Arial', 'B', 8);
  $pdf->Cell(190,4, utf8_decode('USUARIO: '.strtoupper($rowCot['razon_social']) ),0,1,'L', false );
  
  if(""!=$rowCot['cliNumPoder']&&""!=$rowCot['cliFechaPoder']&&""!=$rowCot['cliNotario']&&""!=$rowCot['cliFolio'])
    $pdf->Cell(190,4, utf8_decode('ESCRITURA No. '.$rowCot['cliNumPoder'].'    FECHA: '.$rowCot['cliFechaPoder'].'    NOTARIO: '.$rowCot['cliNotario'].'    FOLIO MERCANTIL: '.$rowCot['cliFolio'] ),0,1,'L', false);
  $pdf->Cell(190,4, utf8_decode('REPRESENTANTE LEGAL: '.$rowCot['repNombre'].' '.$rowCot['repApellidos'] ),0,1,'L', false);
  $pdf->Cell(190,4, utf8_decode('ESCRITURA No. '.$rowCot['escritura'].'    FECHA: '.$rowCot['fecha_poder'].'    NOTARIO: '.$rowCot['notario'].'    FOLIO MERCANTIL: '.$rowCot['folio_mercantil'] ),0,1,'L', false);
  $pdf->MultiCell(190,4, utf8_decode('DOMICILIO: '.strtoupper($rowCot['domicilio'])),0, 'L', false );
  $pdf->Cell(95,4, utf8_decode('RFC EMPRESA: '.strtoupper($rowCot['rfc'])),0,0,'L', false);
  $pdf->Cell(95,4, utf8_decode('CORREO ELECTRÓNICO: '.strtoupper($rowCot['email'])),0,1,'L', false);
  $pdf->Cell(190,4, utf8_decode('TEL: '.$rowCot['telefono'].'   TEL. OFICINA: '.$rowCot['telefono2'].'   CEL: '.$rowCot['telefono3'] ),0,1,'L', false);
  
  $pdf->Ln(1);
  $posX = $pdf->GetX();
  $posY = $pdf->GetY();
  
  $pdf->Cell(190,5, utf8_decode('DATOS DE CONTACTO Autorizo a las siguientes personas en caso de ser necesario para recibir la instalación del servicio contratado.'),0, 1 , 'L', false );
  
  while($rowCont = mysqli_fetch_assoc($resultCont)){
    $pdf->Cell(90,4, utf8_decode($contCont.'.-'.$rowCont['nombre'].' '.$rowCont['apellidos']),0, 0 , 'L', false );
    $pdf->Cell(37,4, utf8_decode(' CEL: '.$rowCont['tel_movil']),0, 0 , 'L', false );
    $pdf->Cell(63,4, utf8_decode(' EMAIL: '.$rowCont['email']),0, 1 , 'L', false );
    $contCont++;
  }
  
  $pdf->Cell(190,2, '',0, 1 , 'C', false );
  
  $pdf->Ln(1);
  $posX = $pdf->GetX();
  $posY = $pdf->GetY();
  
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->MultiCell(190,5, utf8_decode('FORMA DE PAGO'),0, 'C', false );
  $pdf->SetFont('Arial', '', 8);
  $pdf->Rect($posX+20,$posY+6,5,5);
  $pdf->Cell(30,8, utf8_decode('DEPOSITO ' ),0,0,'L', false );
  $pdf->Rect($posX+60,$posY+6,5,5);
  $pdf->Cell(40,8, utf8_decode('TRANSFERENCIA ' ),0,0,'L', false );
  $pdf->Cell(50,8, utf8_decode('BANCO: ' ),0,0,'L', false );
  $pdf->Cell(30,8, utf8_decode('CUENTA: ' ),0,1,'L', false );
  $pdf->setX($posX+95);
  
  $pdf->Ln(1);
  $posX = $pdf->GetX();
  $posY = $pdf->GetY();
  
  $pdf->SetFont('Arial','B',9); 
  $pdf->Cell(190,5, 'SERVICIOS CONTRATADOS',0, 1 , 'C', false );
  $pdf->Cell(20.5,5, 'CANTIDAD',0, 0 , 'C', false );
  $pdf->Cell(95.5,5, 'SERVICIO',0, 0 , 'C', false );
  $pdf->Cell(37,5, 'PRECIO UNITARIO',0, 0 , 'C', false );
  $pdf->Cell(37,5, 'PRECIO TOTAL',0, 1 , 'C', false );*/
  
  $pdf->printCover($rowCot,$conDataArr,$contactArr);
  
  $vigencia = 0;
  
  $adecArr = array();
  
  $slaTxt = 0;
  
  while($rowProd = mysqli_fetch_assoc($resultProd)){    
    
    if(-1!=$rowProd['producto_precio_especial']){     
      $precio = $rowProd['producto_precio_especial'];
    }
    else{
      $precio = $rowProd['precio_lista']*$rowProd['factor'];
    }
    if(5==$rowProd['categoria_id']){
      $adecArr[] = array($rowProd['descripcion'],$precio,$rowProd['cantidad'],$rowProd['servicio'],$rowProd['orden_trabajo_id']);
    }
    else{
      $vigencia = $rowProd['cantidad_vigencia'];
      $subtotal = $subtotal+($rowProd['cantidad']*$precio);
    
      $bajada = str_replace("%n%",$rowProd['bajada'],$rowProd['descripcion']);            
      $desc = str_replace("%u%",$rowProd['subida'],$bajada);
      
      $posx = $pdf->GetX();
      $posy = $pdf->GetY();
      
      $pdf->SetXY($posx+20.4,$posy); //se recorre a la posición de la segunda columna
      
      $pdf->SetFont('Arial','IB',8); 
      $pdf->MultiCell(95.5,4, utf8_decode($desc),0, 'L', false ); //Imprime descripción
      
      $pdf->SetFont('Arial','',8); 
      $cellHeight = $pdf->GetY()-$posy; //obtiene la altura de la celda de descripcion porque es dinámica
                   
      $pdf->SetXY($posx+95.5+20.5,$posy); //se posiciona a la derecha de la celda de descripción
      
      //imprime celdas de precios
      $total = $precio*$rowProd['cantidad'];
      $pdf->Cell(37,$cellHeight, utf8_decode("$ ".number_format($precio, 2, '.', ',')),0, 0 , 'C', false );
      $pdf->Cell(37,$cellHeight, utf8_decode("$ ".number_format($total, 2, '.', ',')),0, 0 , 'C', false );
      
      //vuelve a la primera posición de la fila e imprime la primera celda (cantidad)
      $pdf->SetXY($posx,$posy);
      $pdf->SetFont('Arial','B',8); 
      
      $pdf->Cell(20.5,$cellHeight, $rowProd['cantidad'],0, 1 , 'C', false );
    } 
    $slaTxt .= $rowProd['disponibilidad_txt']." ".$rowProd['latencia_txt']." ".$rowProd['entrega_txt']." ".$rowProd['ab_entregado_txt']; 
  }
  
  $impuesto = $subtotal*0.16;
  $totalCot = $subtotal+$impuesto;
  
  $precioSub = 0;
  $adecsubtotal = 0;
  
  foreach($adecArr AS $key => $value){   
    $serMts = str_replace("ADEC-TORRE-ELE-","",$value[3]);
    $serMts = str_replace("ADEC-TORRE-INM-","",$serMts);
    
    $queryRowCheckAdec = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '".$cfgTableNameCat."' ".
                         "AND TABLE_NAME = 'cat_venta_servicios' AND COLUMN_NAME = 'sub_".$serMts."m' ";

    $resultRowCheckAdec = mysqli_query($db_catalogos,$queryRowCheckAdec);
        
    if(0<mysqli_num_rows($resultRowCheckAdec)){
    
      $queryAdecDet = "SELECT cvs.sub_".$serMts."m ".
                      "FROM ".$cfgTableNameMod.".rel_cotizacion_producto AS rcp, ". 
                              $cfgTableNameMod.".ordenes_trabajo AS ot, ".
                              $cfgTableNameCat.".cat_venta_servicios AS cvs ".
                      "WHERE ot.orden_id = ".$value[4]." ".
                      "AND ot.rel_cotizacion_producto = rcp.relacion_id ".
                      "AND rcp.producto_id = cvs.servicio_id ";
                                            
      $resultAdecDet = mysqli_query($db_modulos,$queryAdecDet);
      
      if($resultAdecDet){
        $rowAdecDet = mysqli_fetch_assoc($resultAdecDet);
        $precioSub = ($value[1]*$value[2])-$rowAdecDet['sub_'.$serMts.'m'];
      }
    }
    else{
      $precioSub = ($value[1]*$value[2]);
    }
    $adecsubtotal = $adecsubtotal+$precioSub;
  }
  
  $adecimpuesto = $adecsubtotal*0.16;
  $adectotal = $adecsubtotal*1.16; 
  
  $pdf->SetFont('Arial', 'B', 8);
  
  $pdf->Ln(4);
  $posx = $pdf->GetX();
  $posy = $pdf->GetY();
  
  $pdf->Cell(116,4, '',0, 0 , 'C', false );
  $pdf->Cell(37,4, utf8_decode('Subtotal'),0, 0 , 'R', false );
  $pdf->Cell(37,4, utf8_decode("$ ".number_format($subtotal, 2, '.', ',')),0, 1 , 'C', false );
  $pdf->Cell(116,4, '',0, 0 , 'C', false );
  $pdf->Cell(37,4, utf8_decode('IVA'),0, 0 , 'R', false );
  $pdf->Cell(37,4, utf8_decode("$ ".number_format($impuesto, 2, '.', ',')),0, 1 , 'C', false );
  $pdf->Cell(116,4, '',0, 0 , 'C', false );
  $pdf->Cell(37,4, utf8_decode('Total'),0, 0 , 'R', false );
  $pdf->Cell(37,4, utf8_decode("$ ".number_format($totalCot, 2, '.', ',')),0, 1 , 'C', false );
  
  $pdf->Ln(2);
  $posx = $pdf->GetX();
  $posy = $pdf->GetY();
  
  $pdf->Cell(95,8, utf8_decode('COSTO OPERACIONES DE ADECUACION'),0,0,'L', false );
  $pdf->setX($posX+95);
  $pdf->Cell(95,5, utf8_decode('MONTO TOTAL: '."$ ".number_format($adectotal, 2, '.', ',')),0,1,'L', false );
  
  $pdf->Ln(2);
  $pdf->Cell(190,4, utf8_decode('VIGENCIA CONTRATADA: '.$vigencia.' meses'),0,1,'L', false );
  //$pdf->Cell(190,4, '',0, 1 , 'C', false );
  
  $pdf->Ln(1);
  $posX = $pdf->GetX();
  $posY = $pdf->GetY();
  
  if($posy>$posY){
    $cellHeight = $posy-$posY;
    $pdf->setXY($posX+95,$posY);
  }
  elseif($posy<$posY){
    $cellHeight = $posY-$posy;
    $pdf->setXY($posX,$posy);
  }
  else{
    $cellHeight = 0;
  }
  $pdf->Cell(95,$cellHeight-1,'',0,1,'L', false );
  //$pdf->Cell(190,1,'',0,1,'L', false );

  $pdf->Cell(190,7, utf8_decode('NOTAS: '.$conDataArr['contNotas'] ),0,1,'L', false );
  
  $pdf->printCoverFooter();
 /* $pdf->Ln(1);
  $posX = $pdf->GetX();
  $posY = $pdf->GetY();

  $pdf->SetFont('Arial', 'B', 11);
  $pdf->Cell(190,7, utf8_decode('CONSIDERACIONES'),'LRT',1,'C', false );
  $pdf->SetFont('Arial', '', 8);
  $pdf->MultiCell(190,3, utf8_decode('1. "EL PROVEEDOR" deberá efectuar las instalaciones y empezar a prestar el servicio motivo del presente contrato dentro de un plazo que no exceda de 10 días naturales posteriores a su firma. '),'LR', 'J', false );
  $pdf->NextConsBR();
  $pdf->MultiCell(190,3, utf8_decode('2. A partir de que "EL USUARIO" cuente con el servicio se empezará a cobrar la mensualidad, dependiendo de la fecha de inicio de servicio, será total o parcialmente la mensualidad correspondiente únicamente al periodo utilizado.'),'LR', 'L', false );
  $pdf->NextConsBR();
  $pdf->MultiCell(190,3, utf8_decode('3. En caso de existir imposibilidad física o técnica para la instalación del servicio, este contrato no tendrá validez alguna, por lo que se dará por terminado, sin perjuicio alguno para las partes, "EL PROVEEDOR" deberá de realizar la devolución de todas las cantidades dadas por adelantado, en su caso, dentro de los 10 días naturales siguientes en que se determine dicho supuesto, sin que exista posibilidad de prórroga para "EL PROVEEDOR".'),'LR', 'J', false );
  $pdf->NextConsBR();
  $pdf->MultiCell(190,3, utf8_decode('4. "EL USUARIO" conviene en permitir el libre acceso a su domicilio con previo aviso por parte de "EL PROVEEDOR", a los operativos y empleados de este en donde se encuentren las instalaciones, previa presentación de su credencial o tarjeta de identificación, para efecto de inspección, modificación o reparación de las instalaciones en su caso.'),'LR', 'J', false );
  $pdf->NextConsBR();
  $pdf->MultiCell(190,3, utf8_decode('5. Para verificar la autenticidad de las credenciales de los operativos y empleados, "EL SUSCRIPTOR", deberá llamar al teléfono del  "EL PROVEEDOR".'),'LR', 'L', false );
  $pdf->NextConsBR();
  $pdf->MultiCell(190,3, utf8_decode('6. El ancho de banda contratado se repartirá entre los aparatos conectados simultáneamente. 
  La velocidad contratada estará comprendida entre el modem y el primer punto de acceso a la red de "EL PROVEEDOR".'),'LR', 'J', false );
  $pdf->NextConsBR();
  $pdf->MultiCell(190,3, utf8_decode('7. La velocidad contratada estará comprendida entre el modem y el primer punto de acceso a la red de "EL PROVEEDOR"'),'LR', 'J', false );
  $pdf->NextConsBR();
  $pdf->MultiCell(190,3, utf8_decode('8. Los equipos financiados y los costos de operaciones de adecuación serán prorrateados entre la vigencia pactada, en caso de que EL USUARIO quiera dar por terminado el presente contrato, deberá cubrir el costo total de dichos conceptos previo a la cancelación, de conformidad a lo pactado en el propio contrato.'),'LRB', 'J', false );
  $pdf->Ln(2);
  $posY = $pdf->GetY();
  $posX = $pdf->GetX();
  $pdf->SetFont('Arial', 'B', 8);
  $pdf->MultiCell(90,4, utf8_decode('El Usuario autoriza al Prestador de Servicios para consulta de Buro de Crédito e Historial Crediticio.'),0, 'C', false );
  $pdf->Cell(90,5, '',0,1,'C', false);
  $pdf->Cell(90,20, utf8_decode('________________________________________'),0,1,'C', false );
  $pdf->Cell(90,5, utf8_decode('FIRMA DE "EL USUARIO"'),0,1,'C', false );

  $pdf->Ln(2);
  $pdf->SetXY($posX+100,$posY);
  $pdf->SetFont('Arial', 'B', 8);
  $pdf->SetX($posX+100);
  $pdf->MultiCell(90,4, utf8_decode('Bajo protesta de decir verdad, manifiesto que la información personal vertida en el presente documento es verídica y precisa. Y que  he leído y he aceptado los términos del contrato anexo de Prestación de Servicios de Telecomunicaciones.'),0, 'C', false );
  $pdf->SetX($posX+100);
  $pdf->Cell(90,15, utf8_decode('________________________________________'),0,1,'C', false );
  $pdf->SetX($posX+100);
  $pdf->Cell(90,5, utf8_decode('FIRMA DE "EL USUARIO"'),0,1,'C', false );*/
  
  //fin catalogo
  
if(1!=$mailflag){
  $pdf->NextAnnexedBR('ANEXO 1');
  /*$pdf->AddPage();
  $pdf->SetFont('Times', 'B', 20);
  $pdf->Image('../../img/logo_coe.png', 20, 12, 40, 0);
  $pdf->Cell(190,15, utf8_decode('ANEXO 1'),0,1,'C', false );
  $pdf->Cell(190,1, '','B',1,'C', false );*/
  $pdf->Ln(4);
  $pdf->SetFont('Arial', '', 12);
  $pdf->MultiCell(190,5, utf8_decode('Maniobras de Operaciones y gastos de Adecuación, así como la descripción del equipo o equipos que se requieren instalar para prestación del servicio de Telecomunicaciones derivado del contrato de prestación y suministro de servicios de telecomunicaciones que han celebrado por una parte '.$coeRazonSocial.' COMUNICACIONES S.A. DE C.V. (EL PROVEEDOR) y por la otra parte comparece la persona cuyo nombre aparece en la carátula del contrato principal (EL USUARIO).'),0, 'J', false );
  $pdf->Ln(5);
  $pdf->Cell(190,15, utf8_decode('Maniobras de Operaciones'),0,1,'L', false );
  $pdf->PrintTable(5);
  $pdf->Ln(5);
  $pdf->SetFont('Arial', '', 12);
  $pdf->Cell(190,15, utf8_decode('Gastos de Adecuaciones'),0,1,'L', false );
  $pdf->PrintTable(5);
  $pdf->Ln(5);
  $pdf->SetFont('Arial', '', 12);
  $pdf->Cell(190,15, utf8_decode('Equipos Instalados'),0,1,'L', false );
  $pdf->PrintTable(5);
  $pdf->Ln(10);
  $pdf->SetFont('Arial', '', 12);
  $pdf->Cell(95,5, utf8_decode('El presente anexo debe firmarse por EL USUARIO'),0,1,'C', false );
  $pdf->Cell(95,20, '','B',1,'C', false );
  $pdf->SetFont('Arial', 'B', 12);
  $pdf->Cell(95,10, utf8_decode('Firma de EL USUARIO'),0,1,'C', false );
  
  $pdf->NextAnnexedBR('ANEXO 2');
  /*$pdf->AddPage();
  $pdf->SetFont('Times', 'B', 20);
  $pdf->Image('../../img/logo_coe.png', 20, 12, 40, 0);
  $pdf->Cell(190,15, utf8_decode('ANEXO 2'),0,1,'C', false );
  $pdf->Cell(190,1, '','B',1,'C', false );
  $pdf->Ln(4);*/
  $pdf->SetFont('Arial', '', 12);
  $pdf->MultiCell(190,5, utf8_decode('Acta de entrega recepción de instalaciones, equipos y servicio de Telecomunicaciones derivado del contrato de prestación y suministro de servicios de telecomunicaciones que han celebrado por una parte '.$coeRazonSocial.' COMUNICACIONES S.A. DE C.V. (EL PROVEEDOR) y por la otra parte comparece la persona cuyo nombre aparece en la carátula del contrato principal (EL USUARIO).'),0, 'J', false );
  $pdf->Ln(4);
  $pdf->MultiCell(190,5, utf8_decode('Por medio de la presente acta se hace constar que EL USUARIO o sus autorizados, han recibido en este acto las instalaciones, equipos y servicio pactado en la carátula del contrato principal, los cuales están descritos en el Anexo 1 del mismo contrato, haciendo constar que EL PROVEEDOR ha cumplido así con lo pactado en los documentos antes mencionados.'),0, 'J', false );
  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 12);
  $pdf->Cell(190,15, utf8_decode('Para uso exclusivo de EL PROVEEDOR'),0,1,'L', false );
  $pdf->SetFont('Arial', '', 12);
  $pdf->Cell(190,6, utf8_decode('Observaciones:'),'LRTB',1,'L', false );
  for($i=0;$i<5;$i++){
    $pdf->Cell(190,6, '','LRB',1,'L', false );
  }
  $pdf->Ln(4);
  $pdf->SetFont('Arial', '', 12);
  $pdf->Cell(190,15, utf8_decode('Domicilio de insatalación:'),0,1,'L', false );
  $pdf->Cell(50,6, utf8_decode('Calle y número:'),'LTB',0,'R', false );
  $pdf->Cell(140,6, utf8_decode(''),'LRTB',1,'L', false );
  $pdf->Cell(50,6, utf8_decode('Colonia:'),'LTB',0,'R', false );
  $pdf->Cell(140,6, utf8_decode(''),'LRTB',1,'L', false );
  $pdf->Cell(50,6, utf8_decode('Municipio y Estado:'),'LTB',0,'R', false );
  $pdf->Cell(140,6, utf8_decode(''),'LRTB',1,'L', false );
  $pdf->Cell(50,6, utf8_decode('C.P.:'),'LTB',0,'R', false );
  $pdf->Cell(140,6, utf8_decode(''),'LRTB',1,'L', false );
  $pdf->Cell(50,6, utf8_decode('Entre Calles:'),'LTB',0,'R', false );
  $pdf->Cell(140,6, utf8_decode(''),'LRTB',1,'L', false );
  $pdf->Cell(50,6, utf8_decode('Referencias:'),'LTB',0,'R', false );
  $pdf->Cell(140,6, utf8_decode(''),'LRTB',1,'L', false );
  
  $pdf->Ln(8);
  $pdf->Cell(50,6, utf8_decode('Fecha:'),'LTB',0,'R', false );
  $pdf->Cell(140,6, utf8_decode(''),'LRTB',1,'L', false );
  
  $pdf->Ln(4);
  $pdf->SetFont('Arial', '', 12);
  $pdf->Cell(190,15, utf8_decode('Persona que lo recibió:'),0,1,'L', false );
  
  $pdf->SetFont('Arial', '', 10);
  $pdf->Cell(45,6, utf8_decode('Nombre:'),'LTB',0,'R', false );
  $pdf->Cell(65,6, utf8_decode(''),'LRTB',0,'L', false );
  $pdf->Cell(40,6, utf8_decode('Se identifica con:'),'LTB',0,'R', false );
  $pdf->Cell(40,6, utf8_decode(''),'LRTB',1,'L', false );

  $pdf->Cell(45,6, utf8_decode('Numero de identificación:'),'LTB',0,'R', false );
  $pdf->Cell(65,6, utf8_decode(''),'LRTB',0,'L', false );
  $pdf->Cell(40,6, utf8_decode('Teléfono:'),'LTB',0,'R', false );
  $pdf->Cell(40,6, utf8_decode(''),'LRTB',1,'L', false );
  
  $pdf->Ln(10);
  
  $posY = $pdf->GetY();
  $posX = $pdf->GetX();
  
  $pdf->Cell(85,20, '','B',1,'C', false );
  $pdf->SetFont('Arial', 'B', 12);
  $pdf->Cell(85,5, utf8_decode('Firma de recepción'),0,1,'C', false );
  $pdf->Cell(85,5, utf8_decode('El USUARIO o su autorizado'),0,1,'C', false );
  
  $pdf->SetXY($posX+105,$posY);
  $pdf->Cell(85,20, '','B',1,'C', false );
  $pdf->SetX($posX+105);
  $pdf->SetFont('Arial', 'B', 12);
  $pdf->Cell(85,5, utf8_decode('Firma de instalador'),0,1,'C', false );
  
  $pdf->NextAnnexedBR('ANEXO 3');
  /*$pdf->AddPage();
  $pdf->SetFont('Times', 'B', 20);
  $pdf->Image('../../img/logo_coe.png', 20, 12, 40, 0);
  $pdf->Cell(190,15, utf8_decode('ANEXO 3'),0,1,'C', false );
  $pdf->Cell(190,1, '','B',1,'C', false );*/
  $pdf->SetFont('Arial', '', 12);
  $pdf->Ln(4);
  $pdf->MultiCell(190,5, utf8_decode('Garantías de los equipos y servicios de telecomunicaciones que derivado del contrato de prestación
y suministro de servicios de telecomunicaciones han pactado por una parte '.$coeRazonSocial.' COMUNICACIONES S.A. DE C.V. (EL PROVEEDOR) y por la otra parte comparece la persona cuyo nombre aparece en la carátula del contrato principal (EL USUARIO).'),0, 'J', false );
  $pdf->Ln(4);
  $pdf->Cell(190,15, utf8_decode('A continuación se enlistan:'),0,1,'L', false );
  $pdf->Ln(2);
  $pdf->SetFont('Times','',9);
  $pdf->MultiCell(190,4, utf8_decode($slaTxt),0, 'J', false );

  $pdf->Ln(4);
  $pdf->NextAnnexedBR('ANEXO 3');
  /*if(240<$posY){
    $pdf->AddPage();
    $pdf->SetFont('Times', 'B', 20);
    $pdf->Image('../../img/logo_coe.png', 20, 12, 40, 0);
    $pdf->Cell(190,15, utf8_decode('ANEXO 3'),0,1,'C', false );
    $pdf->Cell(190,1, '','B',1,'C', false );
    $pdf->Ln(4);
  }*/
  $pdf->SetFont('Times', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('Se estipula que EL USUARIO será el responsable de pagar el deducible correspondiente del o de los equipos en caso de daño material súbito e imprevisto especificado en la póliza se seguro contratada por EL PROVEEDOR, así mismo queda establecido que en caso de que el daño fuese causado de manera intencional (previo peritaje del técnico) el usuario pagará el importe total para reponer el o los equipos dañados.'),0, 'J', false );
  $pdf->Ln(30);
  $pdf->SetFont('Arial', '', 12);
  $pdf->Cell(95,20, '','B',1,'C', false );
  $pdf->SetFont('Arial', 'B', 12);
  $pdf->Cell(95,10, utf8_decode('Firma de EL USUARIO'),0,1,'C', false );
}
  if(1!=$mailflag) //Salida al navegador directa
    $pdf->Output(); 
  else{ //Envia por mail la cotización

    $filename = date("YmdHis").$cotId."_".$rowCot['razon_social'];
    $filepath = realpath("../../docs/pdf/contratos/");
    $pdfdoc = $pdf->Output($filepath.$filename, "S"); //crea un archivo
    
    // Configura con los datos del servidor de correo
    $transport = Swift_SmtpTransport::newInstance($cfgmailServer, $cfgmailport);
    $transport->setUsername($cfgmailuser);
    $transport->setPassword($cfgmailpass);
   
    //Crea el mailer con los datos del servidor de correos
    $mailer = Swift_Mailer::newInstance($transport);
    
    /*Register anti flood plugin
      The AntiFlood plugin is designed to help lessen the load on the HTTP server and the SMTP server. It can also be used to send out very large batches of emails when the SMTP server has restrictions in place to limit the number of emails sent in one go. */
    $mailer->registerPlugin(new Swift_Plugins_AntiFloodPlugin(100, 30));
       
    //Crea mensaje
    $message = Swift_Message::newInstance();

    if(file_exists('../../img/signatures/'.$usuarioId.'.png')){
      $sigPic = $message->embed(Swift_Image::fromPath('../../img/signatures/'.$usuarioId.'.png'));
      $mensajeHtml .= "<br><hr><img src='".$sigPic."' alt='firma'/>";  
    }               

    $message->setSubject($asunto);
    $message->setBody($mensajeHtml,'text/html');
    $message->addPart($mensajePlano,'text/plain');
    $message->setFrom($venEmail, $venNom." ".$venApe);
    $message->setTo($contactEmails);
    $attachment = Swift_Attachment::newInstance($pdfdoc, $filename.".pdf", 'application/pdf');
    
    //adjunta archivo
    $message->attach($attachment);
       
    // manda correo
    if(!$mailer->send($message,$failure)){
      $salida = "ERROR||Ocurrió un problema al enviar el documento por correo electrónico";
    }
    else{
      foreach($contactEmails as $key => $value){
        writeOnJournal($usuarioId,"Ha enviado un correo a nombre de [".$venNom." ".$venApe."] de la cotización con id: [".$cotId."] al email [".$key."]");
      }
      $salida = "OK||El archivo se ha enviado con éxito";      
    }
    //borra archivo que se envió y ya no se necesita
    unlink($filepath.$filename);
  }
}

a:
mysqli_close($db_modulos);
mysqli_close($db_catalogos);
mysqli_close($db_usuarios);
echo $salida;

?>
