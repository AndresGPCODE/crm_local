<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
ini_set('allow_url_fopen',1);
set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');


include_once "../../libs/db/common.php";
include_once "../../libs/db/dbcommon.php";
include_once "../../libs/db/log.php";
include_once "../../libs/db/encrypt.php";
include_once "../../libs/fpdf/fpdf.php";
require_once '../../libs/mail/swiftmailer-master/lib/swift_required.php';

log_write("DEBUG: SEND-TICKET-EMAIL: ***INICIA PROCESO***",8);

$cfgLogService = "../../log/ticketlog";

$db_modulos   = condb_modulos();

$parametros = parseArguments($argv);

log_write("DEBUG: SEND-TICKET-EMAIL: Argumentos parseados"."<pre>".print_r($parametros,true)."</pre>",8);

/*$cfgfromname  = 'Soporte Coeficiente CRM';
$cfgfromemail = 'soporte@coeficiente.mx';*/

$cfgfromname  = 'Soporte Coeficiente CRM';
$cfgfromemail = 'tsanchez@ctclat.com';

$msgSigPicAssocPath = '../../img/logo_coewhite.png';//ruta de firma para asociados
$msgSigPicStaffPath = '../../img/logo_support.png';//ruta de firma para usuarios (staff)

$posId      = $parametros['posId']; //id del post que se va a enviar
$alertType  = $parametros['alertType']; //tipo de alerta que se va a enviar

$selAssocArr = isset($parametros['assocArrJson']) ? json_decode($parametros['assocArrJson']) : "";
$message = "";
$mailer = "";

log_write("DEBUG: SEND-TICKET-EMAIL: Post id: ".$posId,8);
log_write("DEBUG: SEND-TICKET-EMAIL: Tipo de Alerta: ".$alertType,8);
log_write("DEBUG: SEND-TICKET-EMAIL: Assoc Array: ".print_r($selAssocArr,true),8);

//obtiene los datos del post y del ticket al que corresponde
/*$queryPost = "SELECT post.*, tick.ticket_id, tick.titulo, tick.detalles, ".
                    "tick.ticket_topic_otro, tick.asignadoa, tick.ultima_act_usuario, ".
                    "tick.usuario_alta, tick.estatus, cat.categoria, asoc.asociado_nombre, ".
             "top.ticket_subcategoria, top.es_otro, grp.grupo ".
             "FROM ".$cfgTableNameMod.".ticket_bitacora_posts AS post, ".
                     $cfgTableNameMod.".tickets AS tick, ".
                     $cfgTableNameCat.".cat_ticket_topic AS top, ".
                     $cfgTableNameCat.".cat_ticket_categorias AS cat, ".
                     $cfgTableNameCat.".cat_ticket_departamento AS dep, ".
                     $cfgTableNameUsr.".grupos AS grp, ".
                     $cfgTableNameMod.".ticket_asociados AS asoc ".
             "WHERE post.ticket_id=tick.ticket_id ".
             "AND tick.ticket_topic_id=top.ticket_topic_id ".
             "AND top.ticket_categoria_id=cat.ticket_categoria_id ".
             "AND grp.grupo_id=dep.grupo_id ".
             "AND dep.ticket_departamento_id=tick.departamento_id ".
             "AND (asoc.ticket_id=tick.ticket_id OR asoc.es_mail_origen=1) ".
             "AND post.post_id=".$posId;*/

$queryPost = "SELECT post.*, tick.ticket_id, tick.titulo, tick.detalles, tick.ticket_topic_otro, tick.asignadoa, tick.ultima_act_usuario, tick.usuario_alta, tick.estatus, cat.categoria, ".
             "top.ticket_subcategoria, top.es_otro, grp.grupo ".
             "FROM ".$cfgTableNameMod.".ticket_bitacora_posts AS post, ".
                     $cfgTableNameMod.".tickets AS tick, ".
                     $cfgTableNameCat.".cat_ticket_topic AS top, ".
                     $cfgTableNameCat.".cat_ticket_categorias AS cat, ".
                     $cfgTableNameCat.".cat_ticket_departamento AS dep, ".
                     $cfgTableNameUsr.".grupos AS grp ".
             "WHERE post.ticket_id=tick.ticket_id ".
             "AND tick.ticket_topic_id=top.ticket_topic_id ".
             "AND top.ticket_categoria_id=cat.ticket_categoria_id ".
             "AND grp.grupo_id=dep.grupo_id ".
             "AND dep.ticket_departamento_id=tick.departamento_id ".
             "AND post.post_id=".$posId;

$resultPost  = mysqli_query($db_modulos,$queryPost);

log_write("DEBUG: SEND-TICKET-EMAIL: Post Query ".$queryPost,8);

if(!$resultPost){
  log_write("ERROR: SEND-TICKET-EMAIL: No se pudieron conseguir los datos del post con ID:[".$posId."]",8);
}
else{
  //Configura con los datos del servidor de correo
  $transport = Swift_SmtpTransport::newInstance($cfgmailServer, $cfgmailport);
  $transport->setUsername($cfgmailuser);
  $transport->setPassword($cfgmailpass);

  //Crea el mailer con los datos del servidor de correos
  $mailer = Swift_Mailer::newInstance($transport);

  /*Register anti flood plugin
  The AntiFlood plugin is designed to help lessen the load on the HTTP server and the SMTP server. It can also be used to send out very large batches of emails when the SMTP server has restrictions in place to limit the number of emails sent in one go. */
  $mailer->registerPlugin(new Swift_Plugins_AntiFloodPlugin(100, 30));

  log_write("DEBUG: SEND-TICKET-EMAIL: Se obtuvieron los datos del ticket y su cliente",8);  
  $rowPost = mysqli_fetch_assoc($resultPost); //datos del post
  
  switch($alertType){
    case 0: //en caso de alta manual destinatario asociado  
      $asunto = $rowPost['titulo']." [#".$rowPost['ticket_id']."]";       
      $queryAssoc = "SELECT asoc.ticket_asociado_id, asoc.asociado_nombre, asoc.asociado_email ".
                    "FROM ticket_asociados AS asoc, ".
                         "ticket_bitacora_posts AS post ".
                    "WHERE post.post_id=".$posId." ".
                    "AND asoc.ticket_id=post.ticket_id";
                    //"AND asoc.es_mail_origen=1";
                            
      $queryStaff = "";
    break;
    
    case 1: //en caso de alta por email destinatario asociado 
      $asunto = "Ticket de Soporte Abierto [#".$rowPost['ticket_id']."]";
      $queryAssoc = "SELECT asoc.ticket_asociado_id, asoc.asociado_nombre, asoc.asociado_email ".
                    "FROM ticket_asociados AS asoc, ".
                         "ticket_bitacora_posts AS post ".
                    "WHERE post.post_id=".$posId." ".
                    "AND asoc.ticket_id=post.ticket_id";
                    //"AND asoc.es_mail_origen=1";  
      $queryStaff = "";   
    break;
    
    case 2: //alerta del ticket nuevo para los usuarios del sistema  
      $asunto = "Alerta de Nuevo Ticket";  
      if(-1==$rowPost['usuario_alta']){ //enviado por mail avisa a depto soporte y gerencia 
        $queryAssoc = ""; 
        $queryStaff = "SELECT DISTINCT usr.nombre, usr.apellidos, usr.email ".
                      "FROM ".$cfgTableNameUsr.".usuarios AS usr, ".
                              $cfgTableNameUsr.".rel_usuario_grupo AS rel ".                   
                      "WHERE (rel.grupo_id=5 OR rel.grupo_id=6) ".
                      "AND rel.usuario_id=usr.usuario_id ";                                                   
      }
      else{ //alta manual avisa a depto relacionado y gerencia
        $queryAssoc = "";       
        $queryStaff = "SELECT DISTINCT usr.nombre, usr.apellidos, usr.email ".
                      "FROM ".$cfgTableNameUsr.".usuarios AS usr, ".
                              $cfgTableNameUsr.".rel_usuario_grupo AS rel, ".
                              $cfgTableNameMod.".tickets AS tick, ".
                              $cfgTableNameCat.".cat_ticket_departamento AS dep ".                        
                      "WHERE tick.departamento_id=dep.ticket_departamento_id ".
                      "AND ((dep.grupo_id=rel.grupo_id AND rel.usuario_id=usr.usuario_id)".
                      "OR (rel.grupo_id=5 AND rel.usuario_id=usr.usuario_id))".
                      "AND tick.ticket_id=".$rowPost['ticket_id'];     
      }
    break;
    
    case 3: //cuando se asigna un ticket avisa al usuario asignado (valgame la redundancia)
      $asunto = "Se te ha asignado un Ticket";
      $queryAssoc = "";
      $queryStaff = "SELECT DISTINCT usr.nombre, usr.apellidos, usr.email ".
                    "FROM ".$cfgTableNameUsr.".usuarios AS usr, ".
                            $cfgTableNameMod.".tickets AS tick, ".
                            $cfgTableNameMod.".ticket_bitacora_posts AS post ".
                    "WHERE tick.ticket_id=".$rowPost['ticket_id']." ".
                    "AND tick.asignadoa=usr.usuario_id";
    break;
    
    case 4: //cuando se reasigna un ticket, hacia el departamento asociado (soporte, instalaciones etc)
      $asunto = "Ticket #".$rowPost['ticket_id']." Transferido";
      $queryAssoc = "";
      $queryStaff = "SELECT DISTINCT usr.nombre, usr.apellidos, usr.email ".
                    "FROM ".$cfgTableNameUsr.".usuarios AS usr, ".
                            $cfgTableNameUsr.".rel_usuario_grupo AS rel, ".
                            $cfgTableNameMod.".tickets AS tick, ".
                            $cfgTableNameCat.".cat_ticket_departamento AS dep ".                        
                    "WHERE tick.departamento_id=dep.ticket_departamento_id ".
                    "AND ((dep.grupo_id=rel.grupo_id AND rel.usuario_id=usr.usuario_id)".
                    "OR (rel.grupo_id=5 AND rel.usuario_id=usr.usuario_id))".
                    "AND tick.ticket_id=".$rowPost['ticket_id'];
    break;
    
    case 5: //nota interna hacia el usuario asignado y gerencia
      $asunto = "Alerta de Nota Interna. Ticket #".$rowPost['ticket_id'];
      $queryAssoc = "";
      $queryStaff = "SELECT DISTINCT usr.nombre, usr.apellidos, usr.email ".
                    "FROM ".$cfgTableNameUsr.".usuarios AS usr, ".
                            $cfgTableNameUsr.".rel_usuario_grupo AS rel, ".
                            $cfgTableNameMod.".tickets AS tick, ".
                            $cfgTableNameMod.".ticket_bitacora_posts AS post ".
                    "WHERE tick.ticket_id=".$rowPost['ticket_id']." ".
                    "AND (tick.asignadoa=usr.usuario_id OR (rel.grupo_id=5 AND rel.usuario_id=usr.usuario_id))";
    break;
    
    case 6: //retraso de ticket, hacia los usuarios del departamento relacionado y gerencia (soporte, instalciones etc)
      $asunto = "Alerta de Retraso de Ticket";
      $queryAssoc = "";
      $queryStaff = "SELECT DISTINCT usr.nombre, usr.apellidos, usr.email ".
                    "FROM ".$cfgTableNameUsr.".usuarios AS usr, ".
                            $cfgTableNameUsr.".rel_usuario_grupo AS rel, ".
                            $cfgTableNameMod.".tickets AS tick, ".
                            $cfgTableNameCat.".cat_ticket_departamento AS dep ".                        
                    "WHERE tick.departamento_id=dep.ticket_departamento_id ".
                    "AND ((dep.grupo_id=rel.grupo_id AND rel.usuario_id=usr.usuario_id) ".
                    "OR (rel.grupo_id=5 AND rel.usuario_id=usr.usuario_id)) ".
                    "AND tick.ticket_id=".$rowPost['ticket_id'];     
    break;
    
    case 7: //cierre de ticket, hacia gerencia y departamento relacionado      
      if(5==$rowPost['estatus']){
        $asunto = "Alerta de Cierre de Ticket";
      }
      else{
        $asunto = "Alerta de Reapertura de Ticket";
      }
      $queryAssoc = "";
      $queryStaff = "SELECT DISTINCT usr.nombre, usr.apellidos, usr.email ".
                    "FROM ".$cfgTableNameUsr.".usuarios AS usr, ".
                            $cfgTableNameUsr.".rel_usuario_grupo AS rel, ".
                            $cfgTableNameMod.".tickets AS tick, ".
                            $cfgTableNameCat.".cat_ticket_departamento AS dep ".                        
                    "WHERE tick.departamento_id=dep.ticket_departamento_id ".
                    "AND ((dep.grupo_id=rel.grupo_id AND rel.usuario_id=usr.usuario_id) ".
                    "OR (rel.grupo_id=5 AND rel.usuario_id=usr.usuario_id)) ".
                    "AND tick.ticket_id=".$rowPost['ticket_id'];        
    break;
    
    case 8: //alerta de mensaje escrito por el cliente hacia todos los asociados
      $asunto = $rowPost['titulo']." [#".$rowPost['ticket_id']."]";
      $queryAssoc = "SELECT asoc.ticket_asociado_id, asoc.asociado_nombre, asoc.asociado_email ".
                    "FROM ticket_asociados AS asoc, ".
                         "ticket_bitacora_posts AS post ".
                    "WHERE post.post_id=".$posId." ".
                    "AND asoc.ticket_id=post.ticket_id";
                    //"AND asoc.es_mail_origen=1";                    

    break;
    
    case 9: //alerta de mensaje escrito por el cliente hacia usuarios asignados y gerencia    
      $asunto = "Alerta de Nuevo Mensaje";                
      $queryStaff = "SELECT DISTINCT usr.nombre, usr.apellidos, usr.email ".
                    "FROM ".$cfgTableNameUsr.".usuarios AS usr, ".
                            $cfgTableNameUsr.".rel_usuario_grupo AS rel, ".
                            $cfgTableNameMod.".tickets AS tick, ".
                            $cfgTableNameCat.".cat_ticket_departamento AS dep ".                        
                    "WHERE tick.departamento_id=dep.ticket_departamento_id ".
                    "AND ((dep.grupo_id=rel.grupo_id AND rel.usuario_id=usr.usuario_id) ".
                    "OR (rel.grupo_id=5 AND rel.usuario_id=usr.usuario_id)) ".
                    "AND tick.ticket_id=".$rowPost['ticket_id'];                    
    break;
    
    case 10: //alerta de mensaje escrito por el usuario del sistema, hacia los asociados (reply)
      $asunto = $rowPost['asunto'];
        
      if(is_array($selAssocArr)){ //si el usuario seleccionó solo cierta cantidad de asociados
        $queryAssoc = "SELECT DISTINCT asoc.ticket_asociado_id, asoc.asociado_nombre, asoc.asociado_email ".
                      "FROM ticket_asociados AS asoc ";                      
        foreach($selAssocArr as $key => $value){
          if(0==$key){
            $queryAssoc .= "WHERE asoc.ticket_asociado_id=".$value." ";
          }
          else{
            $queryAssoc .= "OR asoc.ticket_asociado_id=".$value." ";
          }                    
        }
        $queryAssoc .= "OR (asoc.es_mail_origen=1 AND ticket_id=".$rowPost['ticket_id'].")";                                         
      }
      else{
        $queryAssoc = "SELECT asoc.ticket_asociado_id, asoc.asociado_nombre, asoc.asociado_email ".
                      "FROM ticket_asociados AS asoc, ".
                           "ticket_bitacora_posts AS post ".
                      "WHERE post.post_id=".$posId." ".
                      "AND asoc.ticket_id=post.ticket_id";
                      "AND asoc.es_mail_origen=1";      
      }     
                    
      $queryStaff = "";  
    break;
    
    case 11: //alerta de mensaje escrito por el usuario del sistema, hacia los miembros del staff (reply)
      $asunto = $rowPost['asunto'];
      $queryAssoc = "";
      $queryStaff = "SELECT DISTINCT usr.nombre, usr.apellidos, usr.email ".
                    "FROM ".$cfgTableNameUsr.".usuarios AS usr, ".
                       $cfgTableNameUsr.".rel_usuario_grupo AS rel, ".
                       $cfgTableNameMod.".tickets AS tick, ".
                       $cfgTableNameCat.".cat_ticket_departamento AS dep ".                        
                    "WHERE tick.departamento_id=dep.ticket_departamento_id ".
                    "AND ((dep.grupo_id=rel.grupo_id AND rel.usuario_id=usr.usuario_id) ".
                    "OR (rel.grupo_id=5 AND rel.usuario_id=usr.usuario_id)) ".
                    "AND tick.ticket_id=".$rowPost['ticket_id'];
    break;   
  }

 /*Aquí se valida si hay un query para obtener los datos del asociado ($queryAssoc definido en el case anterior) si existe, hace la consulta, obtiene los datos y genera el arreglo de emails de destinatario para que el swiftmailer envie el correo. 
 
 Si el query está vacío o indefinido significa que será enviado solo a staff, entonces establecerá la estructura de la firma que se enviará a ellos de acuerdo al tipo de mensaje (definido por la variable $alertType)*/  
           
  if(isset($queryAssoc)&&0<strlen($queryAssoc)){ //habemus asociados
    $resultAssoc = mysqli_query($db_modulos,$queryAssoc);   
               
    log_write("DEBUG: SEND-TICKET-EMAIL: Assoc Query ".$queryAssoc,8);                   
    
    //Se obtienen los datos de los asociados y genera el arreglo de destinatario para que el swiftmailer envie el correo    
    if($resultAssoc){           
      while($rowAssoc = mysqli_fetch_assoc($resultAssoc)){
        $message = "";
        $destArrAssoc = array();
        $destArrAssoc[$rowAssoc['asociado_email']] = $rowAssoc['asociado_nombre']; //arreglo swiftmailer  
        log_write("DEBUG: SEND-TICKET-EMAIL: Mail: ".$rowStaff['asociado_email']." Nombre: ".$rowStaff['asociado_nombre'],8);
        
        //Crea una instancia de mensaje de swiftmailer
        $message = Swift_Message::newInstance();        
        $mensajeHtml = buildMessageBody($alertType,$rowPost,$rowStaff,$rowAssoc,$message);
        sendEmail($asunto,$mensajeHtml,$destArrAssoc,$message);
      }    
    }              
  }              
  
 /*Aquí se valida si hay un query para obtener los datos del staff ($queryStaff definido en el case anterior) si existe, hace la consulta, obtiene los datos y genera el arreglo de emails de destinatario para que el swiftmailer envie el correo.
 
 Si el query está vacío o indefinido significa que será enviado solo a asociados, entonces establecerá la estructura de la firma que se enviará a ellos de acuerdo al tipo de mensaje (definido por la variable $alertType)*/  
 
  if(isset($queryStaff)&&0<strlen($queryStaff)){ //habemus staff
    $resultStaff = mysqli_query($db_modulos,$queryStaff);  
    
    log_write("DEBUG: SEND-TICKET-EMAIL: Staff Query ".$queryStaff,8);

    //toma los datos de los usuarios (staff) y los acomoda en un arreglo para su gestión con el mailer 
    if($resultStaff){           
      while($rowStaff = mysqli_fetch_assoc($resultStaff)){
        $message = "";
        $destArrStaff = array();     
        //$destArrStaff[$rowStaff['email']] = $rowStaff['nombre']." ".$rowStaff['apellido'];  //arreglo swiftmailer    
        log_write("DEBUG: SEND-TICKET-EMAIL: Mail: ".$rowStaff['email']." Nombre: ".$rowStaff['nombre'],8);
        
        //$destArrStaff['parasauroloph@hotmail.com'] = 'Tania Sánchez'; //prueba temporal
        $destArrStaff['tsanchez@ctclat.com'] = 'Tania Sánchez'; //prueba temporal
        
        //Crea una instancia de mensaje de swiftmailer 
        $message = Swift_Message::newInstance(); 
        $mensajeHtml = buildMessageBody($alertType,$rowPost,$rowStaff,$rowAssoc,$message);
        sendEmail($asunto,$mensajeHtml,$destArrStaff,$message);
      }          
    }     
  }    
  mysqli_close($db_modulos);      
}

//funcion que hace el envío del correo electrónico
function sendEmail($asunto,$mensajeHtml,$contactEmails,&$message){
  //global $message;
  global $mailer;
  global $cfgfromemail;
  global $cfgfromname;

  log_write("DEBUG: SEND-EMAIL: Se va a enviar el correo",8);
  
  log_write("DEBUG: SEND-EMAIL: El mensaje completo es este: ".$mensajeHtml,8);
  
  log_write("DEBUG: SEND-EMAIL: Destinatarios: ".print_r($contactEmails,true),8);
  
  $message->setSubject($asunto);
  log_write("DEBUG: SEND-EMAIL: Se seteó el asunto",8);
  $message->setBody($mensajeHtml,'text/html');
  log_write("DEBUG: SEND-EMAIL: Se seteó el cuerpo del mensaje",8);
  $message->setFrom($cfgfromemail,$cfgfromname);
  log_write("DEBUG: SEND-EMAIL: Se seteó la dirección de remitente",8);
  /*$attachment = Swift_Attachment::newInstance($pdfdoc, $filename.".pdf", 'application/pdf');
  
  //adjunta archivo
  $message->attach($attachment); */
      
  //agrega destinatarios
  $message->setTo($contactEmails);                    
  log_write("DEBUG: SEND-EMAIL: Se seteó la dirección de destinatario",8);
  
  //obtiene id del mensaje
  $msgId = $message->getId();
  
  //manda correo
  if(!$mailer->send($message,$failure)){
    log_write("ERROR: SEND-EMAIL: No se pudo enviar el correo al email ".print_r($failure),8);
  }
  else{
    foreach($contactEmails as $key => $value){
      log_write("OK: SEND-EMAIL: Ha enviado un correo a nombre de [".$cfgfromname."] del ticket al email [".$key."]",8);     
    }
    saveMsgIdOnTicketPost($msgId);
  }
  log_write("DEBUG: SEND-EMAIL: Mensaje de error recibido ".print_r($failure),8); 
}

function saveMsgIdOnTicketPost($msgId){
  global $db_modulos;
  global $posId;  
  
  $query = "INSERT INTO rel_email_ticket (post_id,email_id) VALUES(".$posId.",'".$msgId."')";
  
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result)
    log_write("ERROR: SEND-EMAIL: No se guardó el id de mensaje para el post Id [".$posId."]",8);
  else
    log_write("OK: SEND-EMAIL: Se guardó el id de mensaje [".$msgId."] para el post Id [".$posId."]",8);
}

//crea el cuerpo del mensaje de acuerdo a los parámetros que reciba
function buildMessageBody($alertType,$rowPost,$rowStaff,$rowAssoc,&$message){
  //global $message;
  global $mailer;
  global $supportServiceTel;
  global $supportServiceMail;
  global $msgSigPicStaffPath;
  global $msgSigPicAssocPath;
  
  log_write("DEBUG: BUILD-MESSAGE-BODY: se va a construir el cuerpo del mensaje",8);
  
  $htmlMsg = "";
  $mensajeFirma = ""; 
  $mensajeFirmaAssoc = "";
  $mensajeFirmaStaff = "";
  $sigPicStaff = "";
  $sigPicAssoc = "";
  
  //Se crean las firmas personalizadas para adjuntarse en cualquier mensaje claro, si existe su imagen  
  //para usuarios (staff)  
  
  if(((2<=$alertType)&&(7>=$alertType))||(9==$alertType)||(11==$alertType)){
    $sigPicStaff = $message->embed(Swift_Image::fromPath($msgSigPicStaffPath));
    
    $mensajeFirmaStaff = "<hr><br>".
                         "Para ver/responder el ticket por favor <a href='https://crm.coeficiente.mx/crm/modulos/tickets/index.php?id=".encrypt($rowPost['ticket_id'])."'>dirígete a este enlace</a> ó <a href='https://crm.coeficiente.mx/'>inicia sesión</a> y dirígete al módulo de tickets en el menú de Soporte<br>".
                         "<i><p style='#777'>Su amigable Sistema de Soporte al Cliente CRM Coeficiente</p></i><br>".
                         "<img src='".$sigPicStaff."' alt='firma'/><br>";  
  }
  
  //para asociados y clientes  
  elseif(10==$alertType){
    $sigPicAssoc = $message->embed(Swift_Image::fromPath($msgSigPicAssocPath));
    $mensajeFirmaAssoc = "<hr><br>".
                         "<i><p style='#777'>Esperamos que esta respuesta aclare todas sus dudas. En caso contrario, por favor no envie otro correo electónico. En su lugar, responda este correo electónico.</p></i><br>".
                         //"o <a href=#>acceda a este enláce<a> para verificar sus solicitud y respuestas.</p></i></div>"; 
                         "<i><b>Soporte al Cliente</b></i><br>".
                         "<b>Teléfono:</b> ".$supportServiceTel."<br>".
                         "<b>Correo Electrónico:</b> ".$supportServiceMail."<br>".
                         "<img src='".$sigPicAssoc."' alt='firma'/><br>";                                                 
  }
  elseif((0==$alertType)||(1==$alertType)||(8==$alertType)){
    $sigPicAssoc = $message->embed(Swift_Image::fromPath($msgSigPicAssocPath));
    $mensajeFirmaAssoc = "<hr><br>".
                         "<i><p style='#777'>Si desea agregar un comentario o proporcionar información adicional sobre este inconveniente, por favor responda este correo electrónico.</p></i><br>".
                         //"o <a href=#>acceda a este enláce<a> para completar la información a su solicitud.</p></i></div>";
                         "<i><b>Soporte al Cliente</b></i><br>".
                         "<b>Teléfono:</b> ".$supportServiceTel."<br>".
                         "<b>Correo Electrónico:</b> ".$supportServiceMail."<br>".
                         "<img src='".$sigPicAssoc."' alt='firma'/><br>";                                                    
  }  
  
  //aquí se define el contenido del mensaje de acuerdo al tipo de alerta
  switch($alertType){   
    case 0: //en caso de alta manual destinatario asociado
      
      $msgSaludo = "Estimado/a <b>".$rowAssoc['asociado_nombre'].",</b>";
      $msgEstatus = "Nuestro equipo de atención al cliente le ha asignado el ticket, #".$rowPost['ticket_id'].", con los siguientes datos:<br>".
      "Tema de Ayuda: <b>".$rowPost['categoria']." - ".$rowPost['ticket_subcategoria']." ".$rowPost['ticket_topic_otro']."</b><br>".
      "Asunto: <b>".$rowPost['titulo']."</b><br>";
      $msgCuerpo = $rowPost['detalles'];        
      $mensajeFirma = $mensajeFirmaAssoc;    
    break;
    
    case 1: //en caso de alta por email destinatario asociado
      
      $msgSaludo = "Estimado/a <b>".$rowAssoc['asociado_nombre'].",</b>";
      $msgEstatus = "";
      $msgCuerpo = "Su solicitud de soporte ha sido creada y se asigno el ticket #".$rowPost['ticket_id'].". ".
      "Uno de nuestros representantes le dara seguimiento a la brevedad posible.<br>";
      //"Usted puede ver el progreso de este ticket <a href=#>en línea.</a>";      
      $mensajeFirma = $mensajeFirmaAssoc;                   
    break;
    
    case 2: //alerta del ticket nuevo para los usuarios del sistema         
      
      $msgSaludo = "Estimado/a <b>".$rowStaff['nombre'].",</b>";      
      $msgEstatus = "Se ha creado un nuevo ticket #".$rowPost['ticket_id']."<br>";
      
      if(-1==$rowPost['usuario_alta']){ //enviado por mail avisa a depto soporte y gerencia
        $msgEstatus .= "De: <b>".$rowPost['asociado_nombre']."</b><br>".       
        "Departamento: <b>Soporte</b>";                                         
      }
      else{ //alta manual avisa a depto asociado
        $msgEstatus = "De: <b>".get_userRealName($rowPost['usuario_alta'])."</b><br>".
        "Departamento: <b>".$rowPost['grupo']."</b>";            
      }
      $msgCuerpo = $rowPost['detalles']; 
      $mensajeFirma = $mensajeFirmaStaff;                     
    break;  
     
    case 3: //cuando se asigna un ticket avisa al usuario asignado (valgame la redundancia)
      
      $msgSaludo = "Estimado/a <b>".$rowStaff['nombre'].",</b>";
      $msgEstatus = "El ticket #".$rowPost['ticket_id']." ha sido asignado a ti por el sistema o el gerente";
      $msgCuerpo = $rowPost['detalles'];  
      $mensajeFirma = $mensajeFirmaStaff;                   
    break;
    
    case 4: //cuando se reasigna un ticket, hacia el departamento asociado
      
      $msgSaludo = "Estimado/a <b>".$rowStaff['nombre'].",</b>";
      $msgEstatus = "El ticket #".$rowPost['ticket_id']." ha sido transferido a ".get_userRealName($rowPost['asignadoa']).
      " por: ".get_userRealName($rowPost['ultima_act_usuario']);
      $msgCuerpo = $rowPost['mensaje']; 
      $mensajeFirma = $mensajeFirmaStaff;                   
    break;
    
    case 5: //nota interna hacia los usuarios asignados y gerencia
    
      $msgSaludo = "Estimado/a <b>".$rowStaff['nombre'].",</b>";
      $msgEstatus = "Una nota interna ha sido añadida al ticket #".$rowPost['ticket_id'];
      $msgCuerpo = $rowPost['mensaje']; 
      $mensajeFirma = $mensajeFirmaStaff;                     
    break;
    
    case 6: //retraso de ticket, hacia los usuarios del departamento asignado y gerencia
      
      $msgSaludo = "Estimado/a <b>".$rowStaff['nombre'].",</b>";
      $msgEstatus = "El ticket #".$rowPost['ticket_id']." tiene un retraso considerable.";
      $msgCuerpo = "Todos debemos trabajar duro para garantizar que todos los tickets se están abordando de una manera oportuna<br>".
      "Atentamente <br>".
      "Gerencia de Soporte";
      $mensajeFirma = $mensajeFirmaStaff;                       
    break;
    
    case 7: //cierre de ticket, hacia gerencia y usuarios asignados
      
      $msgSaludo = "Estimado/a <b>".$rowStaff['nombre'].",</b>";
      $msgEstatus = "Una nota interna ha sido añadida al ticket #".$rowPost['ticket_id']."<br>".
      "De: <b>".get_userRealName(substr($rowPost['agregado_por'],1))."</b><br>";
      if(5==$rowPost['estatus']){
        $msgEstatus .= "Título: <b>Ticket Cerrado</b>";
      }
      else{
        $msgEstatus .= "Título: <b>Ticket Reabierto</b>";
      }
      $msgCuerpo .= $rowPost['mensaje']; 
      $mensajeFirma = $mensajeFirmaStaff;                       
    break;
    
    case 8: //alerta de mensaje escrito por el cliente hacia sus asociados
      
      $msgSaludo = "Estimado/a <b>".$rowAssoc['asociado_nombre'].",</b>".
      $msgEstatus = "Se ha añadido un nuevo mensaje al ticket #".$rowPost['ticket_id']." en donde usted participa<br>".
      "De: <b>".$rowAssoc['asociado_nombre']."</b><br>".
      $msgCuerpo = $rowPost['detalles']; 
      $mensajeFirma = $mensajeFirmaAssoc;                
    break;
    
    case 9: //alerta de mensaje escrito por el cliente hacia usuarios asignados y gerencia
      
      $msgSaludo = "Estimado/a <b>".$rowStaff['nombre'].",</b>";
      $msgEstatus = "Se ha añadido un nuevo mensaje al ticket #".$rowPost['ticket_id']."<br>".
      "De: <b>".$rowAssoc['asociado_nombre']."</b>".       
      "Departamento: <b>".$rowPost['grupo']."</b><br>";
      $msgCuerpo = $rowPost['detalles']; 
      $mensajeFirma = $mensajeFirmaStaff;                
    break;
    
    case 10: //alerta de mensaje escrito por el usuario del sistema, hacia los asociados(reply)
      
      $msgSaludo = "Estimado/a <b>".$rowAssoc['asociado_nombre'].",</b>";
      $msgEstatus = get_userRealName(substr($rowPost['agregado_por'],1))." ha registrado un mensaje en un ticket en el que usted participa #".$rowPost['ticket_id'];
      $msgCuerpo = $rowPost['mensaje'];
      $mensajeFirma = $mensajeFirmaAssoc;                 
    break;
    
    case 11: //alerta de mensaje escrito por el usuario del sistema, hacia los miembros del staff (reply)
      
      $msgSaludo = "Estimado/a <b>".$rowStaff['nombre'].",</b>";
      $msgEstatus = get_userRealName(substr($rowPost['agregado_por'],1))." ha añadido un nuevo mensaje al ticket #".$rowPost['ticket_id'];   
      $msgCuerpo = $rowPost['mensaje'];
      $mensajeFirma = $mensajeFirmaStaff;         
    break;
  }
  
  log_write("DEBUG: BUILD-MESSAGE-BODY: se van a embeber las imágenes en el e-mail",8);
  
  /*esta parte genera un domdocument para la obtención de las rutas de las imagenes que tiene el mensaje
    los archivos de esas rutas son añadidas como imagenes embebidas y datos adjuntos en el cuerpo del email via swiftmailer
    esto es para resolver la bronca de las imágenes que no se despliegan en thunderbird, y hotmail 
  */  
  $dom = new domDocument;
  $dom->loadHTML($msgCuerpo,LIBXML_HTML_NOIMPLIED|LIBXML_HTML_NODEFDTD);
  $dom->preserveWhiteSpace = false;
  $imgs = $dom->getElementsByTagName("img");
  
  for($i=0;$i<$imgs->length;$i++){ //por cada imagen que hay  
    $source = $imgs->item($i)->getAttribute("src"); 
    
    $folders = explode("/",trim($source));
    $fileName = $folders[count($folders)-1];
    /*$fileData = explode(".",trim($fileName));
    $fileExtension = $fileData[1];

    $msgCuerpoPicEmbed = $message->embed(Swift_Image::newInstance(file_get_contents($source))->setFilename('image'.$i.'.'.$fileExtension));*/
    $msgCuerpoPicEmbed = $message->embed(Swift_Image::newInstance(file_get_contents($source))->setFilename($fileName));
    
    $msgCuerpo = str_replace($source,$msgCuerpoPicEmbed,$msgCuerpo);
    log_write("DEBUG: BUILD-MESSAGE-BODY: se agregó la imagen ".$source." al arreglo de imagenes para embeber",8);    
  }    
  
  $msgCuerpo = str_replace("''","'",$msgCuerpo);
  $msgCuerpo = str_replace("\"","'",$msgCuerpo);  
  
  //crea la estructura principal del correo
  $htmlMsg = $msgSaludo."<br><br>".//estimado bla bla
             "<i>".$msgEstatus."</i><br><br>". //asunto del mensaje
             $msgCuerpo."<br><br>". //detalles del mensaje
             $mensajeFirma;//firma
                 
  log_write("DEBUG: BUILD-MESSAGE-BODY: Mensaje generado: ".$htmlMsg,8);
  
  return $htmlMsg;
}  

?>
