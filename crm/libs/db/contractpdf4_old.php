<?php
session_start();
set_time_limit(0);

include_once "common.php";
include_once "encrypt.php";
include_once "../fpdf/fpdf.php";
require_once '../mail/swiftmailer-master/lib/swift_required.php';
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

$usuario = $_SESSION['usuario'];
$usuarioId = $_SESSION['usrId'];
$gruposArr = $_SESSION['grupos'];
$usrUbica  = $_SESSION['usrUbica'];

$salida = "";
$fechaNow = date("Y-m-d");



class PDF extends FPDF
{
    public $ctrl;
    public $salto = false;
    // Cargar los datos 
    public $imgLogo;
    public $qrcode;
    public $numContrato;
    public $razonSocial;
    public $nombreComercial;
    public $rfc;
    public $domicilio;
    public $repLegal;
    public $numEscritura;
    public $fecha;
    public $notario;
    public $folioMercantil;
    public $homoclave;
    public $telCasa;
    public $telOficina;
    public $telCelular; 
    

    public $vigenciaMeses;
    public $precio;
    public $vigencia = 0;
    public $adecArr = array();
    public $slaTxt = 0;
    public $router = "";
    public $subtotal;
    public $desc;
    public $total;
    public $ift;
    public $subidadn;
    public $bajadadn;
    public $enlArr = array();
    public $totalCot;
    public $precioSub = 0;
    public $adecsubtotal = 0;

    public $numExterior;
    public $numInterior;
    public $calle;
    public $colonia;
    public $municipio;
    public $codigoPostal;
    public $estado;

    public $contInicioVig;
    public $fechaPago;

    public $region = "";
    public $cargoCancelacion;
    public $cargoInstalacion;
  
    public $nombreSuscriptor;
    public $apellidosSuscriptor; 
    public $emailSuscriptor;

    protected $col = 0; // Columna actual
    protected $y0;      // Ordenada de comienzo de la columna
    // Cargar los datosn desde los txt
    function LoadData($file)
    {
        // Leer las líneas del fichero
        $lines = file($file);
        $data = array();
        foreach($lines as $line)
            $data[] = explode(";;",trim($line));
        return $data;
    }
    function Header()
    {
        // Cabacera
        $this->SetFont('Arial', 'B', 9);
        
        $imagen = getimagesize($this->imgLogo);
        $x = round($imagen[0]/100);
        $y = round($imagen[1]/100);
        // var_dump($y);
        $this->SetTextColor(0,0,0);

        $this->Cell(30,7.5, utf8_decode(''),'LTR', 0 , 'L', false );
        if($this->nombreComercial){
            $this->Cell(160, 7.5, utf8_decode("Nombre Comercial: " . $this->nombreComercial), 'TR', 1, 'R');
        }else{
            $this->Cell(160, 7.5, utf8_decode(""), 'TR', 1, 'L');
        }
        $this->Cell(30,7.5, utf8_decode(''),'LR', 0 , 'L', false );
        if($this->razonSocial){
            $this->Cell(160, 7.5, utf8_decode("Razon Social: " . $this->razonSocial), 'R', 1, 'R');
        }else{
            $this->Cell(160, 7.5, utf8_decode(" "), 'R', 1, 'L');
        }
        $this->Cell(30,7.5, utf8_decode(''),'LR', 0 , 'L', false );
        // $this->Cell(160, 7.5, utf8_decode("RFC: ". $this->rfc.' '.$this->homoclave), 'R', 1, 'R');
        $this->Cell(160, 7.5, utf8_decode("RFC: " . $this->rfc), 'R', 1, 'R');
        $this->Cell(30,7.5, utf8_decode(''),'LBR', 0 , 'L', false );
        $this->Cell(160, 7.5, utf8_decode("Domicilio: ". $this->domicilio), 'BR', 1, 'R');
        if($y > 5){
            $this->Image($this->imgLogo, 12, 18, 26, 0);
        }else{
            $this->Image($this->imgLogo, 12, 20, 26, 0);
        }
        $this->SetFont('Arial', '', 8);
        $this->SetTextColor(128);
        // Guardar ordenada
        $this->y0 = $this->GetY();
        $this->Ln(6);
    }
    function Footer()
    {
        // Pie de página
        $this->SetY(-15);
        $this->SetFont('Arial', '', 10);
        $this->SetTextColor(128);
        $this->Cell(0, 10, utf8_decode($this->PageNo()), 0, 0, 'C');
    }
    // Tabla simple
    function Caratula()
    {
        $alt = 6;
        // $this->Ln($alt);
        $this->SetFillColor(242);
        $this->SetFont('Arial', 'B', 8);
        $this->MultiCell(190, $alt, utf8_decode('SUSCRIPTOR'),'TBRL','C', true);
        $this->Cell(64, $alt, utf8_decode(strtoupper($this->nombreSuscriptor)),'TBRL',0,'C');
        // Tabla para los datos del suscriptor
        
        $data = explode(" ", trim($this->apellidosSuscriptor));
        if (count($data)>1){
            for ($i = 0; $i < count($data); $i++) {
                if ($i == 0) {
                    $apep =  $data[$i];
                    $this->Cell(63, $alt, utf8_decode(strtoupper($apep)), 'TBRL', 0, 'C');
                } elseif ($i == 1) {
                    $apem =  $data[$i];
                    $this->Cell(63, $alt, utf8_decode(strtoupper($apem)), 'TBRL', 0, 'C');
                } else {
                    $this->Cell(63, $alt, utf8_decode(''), 'TBRL', 0, 'C');
                }
            }
        }else{
            for ($i = 0; $i < count($data); $i++) {
                if ($i == 0) {
                    $apep =  $data[$i];
                    $this->Cell(63, $alt, utf8_decode(strtoupper($apep)), 'TBRL', 0, 'C');
                    $this->Cell(63, $alt, utf8_decode(''), 'TBRL', 0, 'C');
                } 
            }
        }
        
        $this->Ln($alt);
        $this->Cell(64, $alt, utf8_decode('Nombre'),'TBL',0,'C', true);
        $this->Cell(63, $alt, utf8_decode('Apellido Paterno'),'TB',0,'C', true);
        $this->Cell(63, $alt, utf8_decode('Apellido Materno'),'TBR',0,'C', true);
        $this->Ln($alt);
        //datos de domicilio
        // var_dump($this->numInterior);
        if($this->numInterior=='') $this->numInterior = 'N/A';
        $this->MultiCell(190, $alt, utf8_decode('DOMICILIO'),'TBRL','C', true);
        $this->Cell(60, $alt, utf8_decode(ucfirst(strtolower($this->calle))),'TBRL',0,'C');
        $this->Cell(10, $alt, utf8_decode($this->numExterior),'TBRL',0,'C');
        $this->Cell(10, $alt, utf8_decode($this->numInterior),'TBRL',0,'C');
        $this->Cell(37, $alt, utf8_decode(ucfirst(strtolower($this->colonia))),'TBRL',0,'C');
        $this->Cell(30, $alt, utf8_decode(ucfirst(strtolower($this->municipio))),'TBRL',0,'C');
        $this->Cell(30, $alt, utf8_decode($this->estado),'TBRL',0,'C');
        $this->Cell(13, $alt, utf8_decode($this->codigoPostal),'TBRL',0,'C');
        $this->Ln($alt);
        $this->Cell(60, $alt, utf8_decode('Calle'),'TBL',0,'C',true);
        $this->Cell(10, $alt, utf8_decode('#Ext'),'TB',0,'C',true);
        $this->Cell(10, $alt, utf8_decode('#Int'),'TB',0,'C',true);
        $this->Cell(37, $alt, utf8_decode('Colonia'),'TB',0,'C',true);
        $this->Cell(30, $alt, utf8_decode('Alcaldía/Municipio'),'TB',0,'C',true);
        $this->Cell(30, $alt, utf8_decode('Estado'),'TB',0,'C',true);
        $this->Cell(13, $alt, utf8_decode('C.P. '),'TBR,true',0,'C',true);
        $this->Ln($alt);
        // datos de telefono
        $this->Cell(20, $alt, utf8_decode('TELEFÓNO'),'TBL',0,'C',true);
        if($this->telCelular){
            $this->Cell(7, $alt, utf8_decode('Fijo'), 'TB', 0, 'C', true);
            $this->Cell(7, $alt, utf8_decode('[  ]'), 'TB', 0, 'C', true);
            $this->Cell(9, $alt, utf8_decode('Móvil'), 'TB', 0, 'C', true);
            $this->Cell(7, $alt, utf8_decode('[ X ]'), 'TBR', 0, 'C', true);
            $this->Cell(53, $alt, utf8_decode($this->telCelular), 'TBRL', 0, 'C');
        }elseif($this->telCasa){
            $this->Cell(7, $alt, utf8_decode('Fijo'), 'TB', 0, 'C', true);
            $this->Cell(7, $alt, utf8_decode('[ X ]'), 'TB', 0, 'C', true);
            $this->Cell(9, $alt, utf8_decode('Móvil'), 'TB', 0, 'C', true);
            $this->Cell(7, $alt, utf8_decode('[  ]'), 'TBR', 0, 'C', true);
            $this->Cell(53, $alt, utf8_decode($this->telCasa), 'TBRL', 0, 'C');
        }elseif($this->telOficina){
            $this->Cell(7, $alt, utf8_decode('Fijo'), 'TB', 0, 'C', true);
            $this->Cell(7, $alt, utf8_decode('[ X ]'), 'TB', 0, 'C', true);
            $this->Cell(9, $alt, utf8_decode('Móvil'), 'TB', 0, 'C', true);
            $this->Cell(7, $alt, utf8_decode('[  ]'), 'TBR', 0, 'C', true);
            $this->Cell(53, $alt, utf8_decode($this->telOficina), 'TBRL', 0, 'C');
        }else{
            $this->Cell(7, $alt, utf8_decode('Fijo'), 'TB', 0, 'C', true);
            $this->Cell(7, $alt, utf8_decode('[  ]'), 'TB', 0, 'C', true);
            $this->Cell(9, $alt, utf8_decode('Móvil'), 'TB', 0, 'C', true);
            $this->Cell(7, $alt, utf8_decode('[  ]'), 'TBR', 0, 'C', true);
            $this->Cell(53, $alt, utf8_decode('S/N'), 'TBRL', 0, 'C');
        }
        $this->Cell(15, $alt, utf8_decode('RFC'),'TBRL',0,'C',true);
        $this->Cell(36, $alt, utf8_decode($this->rfc),'TBRL',0,'C');
        $this->Cell(36, $alt, utf8_decode(''.$this->homoclave),'TBRL',0,'C');
        $this->Ln($alt*2);


        // Tabla para los datos del servicio <contratado></contratado>
        //titulo tabla
        $this->MultiCell(190, $alt, utf8_decode('SERVICIO DE INTERNET FIJO EN CASA'),'TBRL','C', true);

        //titulo columnas
        $this->Cell(70, $alt, utf8_decode('DESCRIPCIÓN PAQUETE/OFERTA'),'TLR',0,'C',true);
        $this->Cell(57, $alt, utf8_decode('TARIFA'),'TLR',0,'C',true);
        $this->Cell(34, $alt, utf8_decode('FECHA DE PAGO'),'TLR',0,'C',true);
        $this->Cell(29, $alt, utf8_decode(''),'TLR',0,'C');

        //segunda parte titulo columnas
        $this->Ln($alt);
        $this->Cell(70, $alt, utf8_decode('(INCISO l) Nom numeral 5.2.1)'),'BLR',0,'C',true);
        $this->Cell(57, $alt, utf8_decode('FOLIO IFT:   '. $this->ift),'BLR',0,'L',true);
        $this->Cell(34, $alt, utf8_decode('Modalidad Pos Pago'),'BLR',0,'C',true);
        $this->Cell(29, $alt, utf8_decode(''),'BLR',0,'C');

        //cuerpo de la seccion anterior
        //MultiCell(float w, float h, string txt [, mixed border [, string align [, boolean fill]]])
        $this->Ln($alt);
        $this->SetFont('Arial', '', 8);
        $y = $this->GetY();
        $this->MultiCell(70, $alt*1.7, utf8_decode($this->desc.' ' . $this->bajadadn . " de bajada y " . $this->subidadn . ' de subida en promedio'),'TLRB','C');
        $this->SetFont('Arial', 'B', 8);
        $y2 = $this->GetY();
        $this->SetY($y);
        $this->SetX(80);
        $this->MultiCell(32, ($y2-$y)/2, utf8_decode('Total  Mensualidad'),'TBLR','L',true);
        $this->SetY($y);
        $this->SetX(112);
        $this->MultiCell(25, ($y2-$y)/2, utf8_decode('$ '.number_format($this->totalCot, 2, '.', ',').' M.N'),'TBLR','R');
        $this->SetY($y);
        $this->SetX(137);
        $this->MultiCell(34, ($y2-$y)/2, utf8_decode('VIGENCIA'),'TBLR','C',true);
        $this->SetY($y);
        $this->SetX(171);
        $this->MultiCell(29, ($y2-$y)/2, utf8_decode($this->vigenciaMeses." meses"),'BLR','C');

        $y = $this->GetY(); 
        $this->SetY($y);
        $this->SetX(80);
        $this->MultiCell(32, ($y2 - $y)/3, utf8_decode('Aplica Tarifa por Reconexión:                       SI  [  ]    NO  [  ]'), 'TBLR', 'L', true);
        $this->SetY($y);
        $this->SetX(112);
        $this->MultiCell(25, ($y2 - $y), utf8_decode('$                M.N' ), 'TBLR', 'R');
        $this->SetY($y);
        $this->SetX(137);
        $this->MultiCell(34, ($y2 - $y), utf8_decode('PENALIDAD'), 'TBLR', 'C', true);
        $this->SetY($y);
        $this->SetX(171);
        $this->MultiCell(29, ($y2 - $y), utf8_decode(''), 'BLR', 'C');
        $this->MultiCell(190, $alt - 1, utf8_decode('En el Estado de cuenta y/o factura se podrá visualizar la fecha de corte del servicio y fecha de pago.'), 'TBRL', 'C', true);

        // $this->MultiCell(190, $alt, utf8_decode('SERVICIO DE INTERNET FIJO EN CASA'), 'TBRL', 'C', true);

        // //titulo columnas
        // $this->Cell(70, $alt, utf8_decode('DESCRIPCIÓN PAQUETE/OFERTA'), 'TLR', 0, 'C', true);
        // $this->Cell(57, $alt, utf8_decode('TARIFA'), 'TLR', 0, 'C', true);
        // $this->Cell(34, $alt, utf8_decode('FECHA DE PAGO'), 'TLR', 0, 'C', true);
        // $this->Cell(29, $alt, utf8_decode(''), 'TLR', 0, 'C');

        // //segunda parte titulo columnas
        // $this->Ln($alt);
        // $this->Cell(70, $alt, utf8_decode('(INCISO l) Nom numeral 5.2.1)'), 'BLR', 0, 'C', true);
        // $this->Cell(57, $alt, utf8_decode('FOLIO IFT:   ' . $this->ift), 'BLR', 0, 'L', true);
        // $this->Cell(34, $alt, utf8_decode('Modalidad Pos Pago'), 'BLR', 0, 'C', true);
        // $this->Cell(29, $alt, utf8_decode(''), 'BLR', 0, 'C');

        // //cuerpo de la seccion anterior
        // $this->Ln($alt);
        // $this->SetFont('Arial', '', 8);
        // $this->Cell(70, $alt * 2, utf8_decode($this->desc), 'TLR', 0, 'C');
        // $this->SetFont('Arial', 'B', 8);
        // $this->Cell(32, $alt * 2, utf8_decode('Total  Mensualidad'), 'BLR', 0, 'L', true);
        // $this->Cell(25, $alt * 2, utf8_decode('$ ' . number_format($this->totalCot, 2, '.', ',') . ' M.N'), 'BLR', 0, 'R');
        // $this->Cell(34, $alt * 2, utf8_decode('VIGENCIA'), 'BLR', 0, 'C', true);
        // $this->Cell(29, $alt * 2, utf8_decode($this->vigenciaMeses . " meses"), 'BLR', 0, 'C');
        // $this->Ln($alt * 2);
        // $this->SetFont('Arial', '', 7.5);
        // $this->Cell(70, 4, utf8_decode($this->bajadadn . " de bajada y " . $this->subidadn . ' de subida en promedio'), 'LR', 0, 'C');
        // $this->SetFont('Arial', 'B', 8);
        // $this->Cell(32, 4, utf8_decode('Aplica Tarifa por'), 'LR', 0, 'L', true);
        // $this->Cell(25, 4, utf8_decode(''), 'LR', 0, 'R');
        // $this->Cell(34, 4, utf8_decode(''), 'LR', 0, 'C', true);
        // $this->Cell(29, 4, utf8_decode(''), 'LR', 0, 'C');
        // $this->Ln(4);
        // $this->Cell(70, 4, utf8_decode(''), 'LR', 0, 'L');
        // $this->Cell(32, 4, utf8_decode('Reconexión:'), 'LR', 0, 'L', true);
        // $this->Cell(25, 4, utf8_decode('$                M.N'), 'LR', 0, 'R');
        // $this->Cell(34, 4, utf8_decode('PENALIDAD'), 'LR', 0, 'C', true);
        // $this->Cell(29, 4, utf8_decode(''), 'LR', 0, 'C');
        // $this->Ln(4);
        // $this->Cell(70, 4, utf8_decode(''), 'BLR', 0, 'L');
        // $this->Cell(32, 4, utf8_decode('SI  [  ]    NO  [  ]'), 'BLR', 0, 'C', true);
        // $this->Cell(25, 4, utf8_decode(''), 'BLR', 0, 'R');
        // $this->Cell(34, 4, utf8_decode(''), 'BLR', 0, 'C', true);
        // $this->Cell(29, 4, utf8_decode(''), 'BLR', 0, 'C');
        // $this->Ln(4);
        // $this->MultiCell(190, $alt - 1, utf8_decode('En el Estado de cuenta y/o factura se podrá visualizar la fecha de corte del servicio y fecha de pago.'), 'TBRL', 'C', true);


        //
        //
        // /*******************   INICIO DE LAS TABLAS GEMELAS   *******************/
        // /**************************   SEGUNDA TABLA  ****************************/
        $this->Ln($alt);
        $tabla_bottom = $this->GetY();
        // $this->MultiCell(90, $alt, utf8_decode('DATOS DEL EQUIPO'), 'TBLR', 'C', true);
        $this->SetY($tabla_bottom);
        $this->SetX(110);
        $this->MultiCell(90, $alt, utf8_decode('INSTALACIÓN DEL EQUIPO'), 'TBLR', 'C', true);
        $this->SetX(130);
        $this->SetFont('Arial', '', 8);
        $this->numInterior = ($this->numInterior == 'N/A' || $this->numInterior == 0) ? null : $this->numInterior;
        $num = ($this->numInterior)? $this->numExterior .' Int: '. $this->numInterior : $this->numExterior;
        $alt_dom_bottom = $this->GetY();
        $this->MultiCell(70, $alt, utf8_decode(ucfirst(strtolower($this->calle)) . ' ' . $num . ' col. ' . ucfirst(strtolower($this->colonia)) . ' C.P. ' . $this->codigoPostal . ' ' . ucwords(strtolower($this->municipio)) . ', ' . ucwords(strtolower($this->estado))), 'TBLR', 'C', false);
        $alt_dom_top = $this->GetY();
        $this->SetFont('Arial', 'B', 8);
        $this->Sety($tabla_bottom + $alt);
        $this->SetX(110);
        $this->MultiCell(20, ($alt_dom_top-$alt_dom_bottom)/2, utf8_decode('Domicilio Instalación:'), 'TBLR', 'C', true);
        $this->SetX(110);
        $this->Cell(15, $alt, utf8_decode('Fecha:'), 'TBLR', 0, 'C', true);
        $this->Cell(30, $alt, utf8_decode(''), 'BLR', 0, 'C');
        $this->Cell(15, $alt, utf8_decode('Hora:'), 'TBLR', 0, 'C', true);
        $this->Cell(30, $alt, utf8_decode(''), 'BLR', 0, 'C');
        $this->Ln($alt);
        $this->SetX(110);
        $this->Cell(15, $alt, utf8_decode('Costo'), 'TBLR', 0, 'C', true);
        $this->Cell(75, $alt, utf8_decode('$ '. $this->cargoInstalacion), 'TBLR', 0, 'L');
        $this->Ln($alt);
        $this->SetFont('Arial', '', 8);
        $this->SetX(110);
        $this->MultiCell(90,$alt, utf8_decode('EL PROVEEDOR" deberá efectuar las instalaciones y empezar a prestar el servicio en un plazo que no exceda de 10 días naturales posteriores a la firma del contrato.'), 'TBLR', 'L', true);
        $tabla_top = $this->GetY();
        // /**************************   PRIMERA TABLA  ****************************/
        $this->SetFont('Arial', 'B', 8);
        $alt_tabla = $tabla_top - $tabla_bottom;
        $this->SetY($tabla_bottom);
        $this->MultiCell(90, $alt_tabla/8, utf8_decode('DATOS DEL EQUIPO'), 'TLR', 'C', true);
        $this->MultiCell(90, $alt_tabla/8, utf8_decode('Modem entregado en:  COMODATO'), 'BLR', 'L', true);
        $this->MultiCell(33, $alt_tabla/4, utf8_decode('Marca:'), 'TBLR', 'C', true);
        $this->SetY($this->GetY()-$alt_tabla/4);
        $this->SetX(43);
        $this->MultiCell(57, $alt_tabla/4, utf8_decode(''), 'TBLR', 'C', false);
        $this->MultiCell(33, $alt_tabla/4, utf8_decode('Modelo:'), 'TBLR', 'C', true);
        $this->SetY($this->GetY() - $alt_tabla/4);
        $this->SetX(43);
        $this->MultiCell(57, $alt_tabla/4, utf8_decode(''), 'TBLR', 'C', false);
        $this->MultiCell(33, $alt_tabla/8, utf8_decode('Número de Serie:'), 'TBLR', 'C', true);
        $this->SetY($this->GetY() - $alt_tabla/8);
        $this->SetX(43);
        $this->MultiCell(57, $alt_tabla/8, utf8_decode(''), 'TBLR', 'C', false);
        $this->MultiCell(33, $alt_tabla/8, utf8_decode('Número de Equipos:'), 'TBLR', 'C', true);
        $this->SetY($this->GetY() - $alt_tabla/8);
        $this->SetX(43);
        $this->MultiCell(57, $alt_tabla/8, utf8_decode(''), 'TBLR', 'C', false);
        // /*******************   FIN DE LAS TABLAS GEMELAS   *******************/

        // PRIMERA LINEA - TABLA 1
        // ESPACIO ENTRE TABLAS
        // TERCERA LINEA - SEGUNDA TABLA
        // $this->Ln($alt);
        // $this->Cell(90, $alt, utf8_decode('DATOS DEL EQUIPO'),'TLR',0,'C',true);
        // $this->Cell(10, $alt, utf8_decode(''),0,0,'C');
        // $this->Cell(90, $alt, utf8_decode('INSTALACIÓN DEL EQUIPO'),'TLR',0,'C',true);

        // $this->Ln($alt);
        // $this->Cell(90, $alt, utf8_decode('Modem entregado en:  COMODATO'), 'BLR', 0, 'L', true);
        // $this->Cell(10, $alt, utf8_decode(''), 0, 0, 'C');
        // $this->Cell(20, $alt, utf8_decode('Domicilio'), 'TLR', 0, 'C', true);
        // $this->SetFont('Arial', '', 7.5);
        // $this->numInterior = ($this->numInterior == 'N/A' || $this->numInterior == 0)? null : $this->numInterior;
        // $num = ($this->numInterior)? $this->numExterior .' Int: '. $this->numInterior : $this->numExterior;
        // $this->Cell(70, $alt, utf8_decode(ucfirst(strtolower($this->calle)).' '.$num.' col. '. ucfirst(strtolower($this->colonia))), 'TLR', 0, 'C');

        // $this->Ln($alt);
        // $this->SetFont('Arial', 'B', 8);
        // $this->Cell(33, $alt, utf8_decode('Marca:'), 'TLR', 0, 'C',true);
        // $this->Cell(57, $alt, utf8_decode(''), 'TLR', 0, 'C');
        // $this->Cell(10, $alt, utf8_decode(''), 0, 0, 'C');
        // $this->Cell(20, $alt, utf8_decode(' Instalación:'), 'BLR', 0, 'C', true);
        // $this->SetFont('Arial', '', 7.5);
        // $this->Cell(70, $alt, utf8_decode('C.P. ' . $this->codigoPostal.' '. ucwords(strtolower($this->municipio)).', '. ucwords(strtolower($this->estado))), 'BLR', 0, 'C');
        
        // $this->Ln($alt);
        // $this->SetFont('Arial', 'B', 8);
        // $this->Cell(33, $alt, utf8_decode(''), 'BLR', 0, 'C', true);
        // $this->Cell(57, $alt, utf8_decode(''), 'BLR', 0, 'C');
        // $this->Cell(10, $alt, utf8_decode(''), 0, 0, 'C');
        // $this->Cell(15, $alt, utf8_decode('Fecha:'), 'TBLR', 0, 'C', true);
        // $this->Cell(30, $alt, utf8_decode(''), 'BLR', 0, 'C');
        // $this->Cell(15, $alt, utf8_decode('Hora:'), 'TBLR', 0, 'C', true);
        // $this->Cell(30, $alt, utf8_decode(''), 'BLR', 0, 'C');

        // $this->Ln($alt);
        // $this->Cell(33, $alt, utf8_decode('Modelo:'), 'TLR', 0, 'C', true);
        // $this->Cell(57, $alt, utf8_decode(''), 'TLR', 0, 'C');
        // $this->Cell(10, $alt, utf8_decode(''), 0, 0, 'C');
        // $this->Cell(15, $alt, utf8_decode('Costo'), 'TBLR', 0, 'C', true);
        // $this->Cell(75, $alt, utf8_decode('$ '. $this->cargoInstalacion), 'TBLR', 0, 'L');

        // $this->Ln($alt);
        // $this->Cell(33, $alt, utf8_decode(''), 'BLR', 0, 'C', true);
        // $this->Cell(57, $alt, utf8_decode(''), 'BLR', 0, 'C');
        // $this->Cell(10, $alt, utf8_decode(''), 0, 0, 'C');
        // $this->SetFont('Arial', '', 8);
        // $this->Cell(90, $alt, utf8_decode('EL PROVEEDOR" deberá efectuar las instalaciones y empezar a prestar el servicio en un plazo que no exceda de 10 días naturales posteriores a la firma del contrato.'), 'TLR', 0, 'L', true);
        // $this->SetFont('Arial', 'B', 8);

        // $this->Ln($alt);
        // $this->Cell(33, $alt, utf8_decode('Número de Serie:'), 'TBLR', 0, 'C', true);
        // $this->Cell(57, $alt, utf8_decode(''), 'TBLR', 0, 'C');
        // $this->Cell(10, $alt, utf8_decode(''), 0, 0, 'C');
        // $this->SetFont('Arial', '', 8);
        // $this->Cell(90, $alt, utf8_decode('prestar el servicio en un plazo que no exceda de 10 días naturales posteriores a la firma del contrato.'), 'LR', 0, 'L', true);
        // $this->SetFont('Arial', 'B', 8);

        // $this->Ln($alt);
        // $this->Cell(33, $alt, utf8_decode('Número de Equipos:'), 'TBLR', 0, 'C', true);
        // $this->Cell(57, $alt, utf8_decode(''), 'TBLR', 0, 'C');
        // $this->Cell(10, $alt, utf8_decode(''), 0, 0, 'C');
        // $this->SetFont('Arial', '', 8);
        // $this->Cell(90, $alt, utf8_decode('posteriores a la firma del contrato.'), 'BLR', 0, 'L', true);
        // $this->SetFont('Arial', 'B', 8);
        // /*******************   FIN DE LAS TABLAS GEMELAS   *******************/

        // /*******************   NUMEROS DE CUENTAS BANCARIAS   *******************/
        //Metodo de pago
        $this->Ln($alt);
            if($this->region == 3){
                $this->Cell(190, $alt * 2, utf8_decode('MÉTODO DE PAGO'), 'TBLR', 0, 'C', true);
                $this->Ln($alt * 2);
                $this->Cell(70, $alt, utf8_decode(''), 'TLR', 0, 'L', true);
                $this->Cell(120, $alt, utf8_decode('  Datos para el método de pago elegido.'), 'TBLR', 0, 'L', true);
                $this->Ln($alt);
                $this->Cell(70, $alt, utf8_decode('  [  ] Efectivo'), 'LR', 0, 'L', true);
                $this->Cell(120, $alt, utf8_decode('   BANCO:Bancomer'), 'LR', 0, 'L');
                $this->Ln($alt);
                $this->Cell(70, $alt, utf8_decode('  [  ] Domiciliado con Tarjeta'), 'LR', 0, 'L', true);
                $this->Cell(120, $alt, utf8_decode('   DEPÓSITO CUENTA No.: CLAVE: 0123 2000 1111 616953'), 'LR', 0, 'L');
                $this->Ln($alt);
                $this->Cell(70, $alt, utf8_decode('  [  ] Transferencia Bancaria'), 'LR', 0, 'L', true);
                $this->Cell(120, $alt, utf8_decode('   BANCO:Santander'), 'LR', 0, 'L');
                $this->Ln($alt);
                $this->Cell(70, $alt, utf8_decode('  [  ] Depósito a cuenta Bancaria'), 'BLR', 0, 'L', true);
                $this->Cell(120, $alt, utf8_decode('   DEPÓSITO CUENTA No.: CLAVE: 01 432 0655 0659 26347'), 'BLR', 0, 'L');
            }else{
                
                $this->Cell(190, $alt * 2, utf8_decode('MÉTODO DE PAGO'), 'TBLR', 0, 'C', true);
                $this->Ln($alt * 2);
                $this->Cell(70, $alt, utf8_decode(''), 'TLR', 0, 'L', true);
                $this->Cell(120, $alt, utf8_decode('  Datos para el método de pago elegido.'), 'TBLR', 0, 'L', true);
                $this->Ln($alt);
                $this->Cell(70, $alt, utf8_decode('  [  ] Efectivo'), 'LR', 0, 'L', true);
                $this->Cell(120, $alt, utf8_decode('   BANCO:Bancomer'), 'LR', 0, 'L');
                $this->Ln($alt);
                $this->Cell(70, $alt, utf8_decode('  [  ] Domiciliado con Tarjeta'), 'LR', 0, 'L', true);
                $this->Cell(120, $alt, utf8_decode('   DEPÓSITO CUENTA No.: CLAVE: 0123 2000 1000 465231 Cajeros: 01 000 46523'), 'LR', 0, 'L');
                $this->Ln($alt);
                $this->Cell(70, $alt, utf8_decode('  [  ] Transferencia Bancaria'), 'LR', 0, 'L', true);
                $this->Cell(120, $alt, utf8_decode('   BANCO:Santander'), 'LR', 0, 'L');
                $this->Ln($alt);
                $this->Cell(70, $alt, utf8_decode('  [  ] Depósito a cuenta Bancaria'), 'BLR', 0, 'L', true);
                $this->Cell(120, $alt, utf8_decode('   DEPÓSITO CUENTA No.: CLAVE: 01 432 0655 0571 38790 Cajeros: 65-50571387-9'), 'BLR', 0, 'L');
            }

        //autorizacion para pago con tarjeta
        $this->AddPage();
        // $this->Ln($alt);
        $this->Cell(190, $alt, utf8_decode('AUTORIZACIÓN PARA CARGO DE TARJETA DE CRÉDITO O DÉBITO'), 'TBLR', 0, 'C', true);
        $this->Ln($alt);
        $this->SetFont('Arial', '', 8);
        $this->Cell(190, $alt-2, utf8_decode('Por medio de la  presente SÍ [  ] NO [  ] autorizo a "EL PROVEEDOR", para que cargue a mi tarjeta de crédito o débito, la cantidad por '), 'TLR', 0, 'C', true);
        $this->Ln($alt-2);
        $this->Cell(190, $alt-2, utf8_decode('concepto de servicios que mensualmente me presta. La vigencia de los cargos será por __________ meses. '), 'LR', 0, 'C', true);
        $this->Ln($alt-2);
        $this->Cell(190, $alt+4, utf8_decode(''), 'LR', 0, 'C', true);
        $this->Ln($alt+4);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(70, $alt, utf8_decode(''), 'L', 0, 'C', true);
        $this->Cell(50, $alt, utf8_decode('Firma'), 'T', 0, 'C', true);
        $this->Cell(70, $alt, utf8_decode(''), 'R', 0, 'C', true);
        $this->Ln($alt);
        $this->Cell(20, $alt, utf8_decode('Banco:'), 'TBLR', 0, 'C', true);
        $this->Cell(60, $alt, utf8_decode(''), 'TBLR', 0, 'C');
        $this->Cell(35, $alt, utf8_decode('Número de Tarjeta:'), 'TBLR', 0, 'C', true);
        $this->Cell(75, $alt, utf8_decode(''), 'TBLR', 0, 'C');

        //Servicios adicionales
        $this->Ln($alt * 2);
        $this->Cell(190, $alt, utf8_decode('SERVICIOS ADICIONALES'), 'TBLR', 0, 'C', true);
        $this->Ln($alt);
        $this->Cell(10, $alt, utf8_decode('1.-'), 'TBLR', 0, 'C', true);
        $this->Cell(85, $alt, utf8_decode(''), 'TBLR', 0, 'C');
        $this->Cell(10, $alt, utf8_decode('2.-'), 'TBLR', 0, 'C', true);
        $this->Cell(85, $alt, utf8_decode(''), 'TBLR', 0, 'C');
        $this->Ln($alt);
        $this->Cell(50, $alt, utf8_decode('DESCRIPCIÓN'), 'TBLR', 0, 'L', true);
        $this->Cell(15, $alt, utf8_decode('COSTO:'), 'TBLR', 0, 'C', true);
        $this->Cell(30, $alt, utf8_decode(''), 'TBLR', 0, 'C');
        $this->Cell(50, $alt, utf8_decode('DESCRIPCIÓN'), 'TBLR', 0, 'L', true);
        $this->Cell(15, $alt, utf8_decode('COSTO:'), 'TBLR', 0, 'C', true);
        $this->Cell(30, $alt, utf8_decode(''), 'TBLR', 0, 'C');
        $this->Ln($alt);
        $this->Cell(95, $alt, utf8_decode(''), 'TLR', 0, 'C');
        $this->Cell(95, $alt, utf8_decode(''), 'TRL', 0, 'C');
        $this->Ln($alt);
        $this->Cell(95, $alt, utf8_decode(''), 'BLR', 0, 'C');
        $this->Cell(95, $alt, utf8_decode(''), 'BRL', 0, 'C');

        //Conceptos facturales
        $this->Ln($alt * 2);
        $this->Cell(190, $alt, utf8_decode('CONCEPTOS FACTURABLES'), 'TLR', 0, 'C', true);
        $this->SetFont('Arial', 'B', 8);
        $this->Ln($alt);
        $this->Cell(190, $alt-2, utf8_decode('(Ejemplo: Costo por cambio de domicilio, Costos administrativos adicionales)'), 'BLR', 0, 'C', true);
        $this->SetFont('Arial', 'B', 8);
        $this->Ln($alt-2);
        $this->Cell(10, $alt, utf8_decode('1.-'), 'TBLR', 0, 'C', true);
        $this->Cell(85, $alt, utf8_decode(''), 'TBLR', 0, 'C');
        $this->Cell(10, $alt, utf8_decode('2.-'), 'TBLR', 0, 'C', true);
        $this->Cell(85, $alt, utf8_decode(''), 'TBLR', 0, 'C');
        $this->Ln($alt);
        $this->Cell(50, $alt, utf8_decode('DESCRIPCIÓN'), 'TBLR', 0, 'L', true);
        $this->Cell(15, $alt, utf8_decode('COSTO:'), 'TBLR', 0, 'C', true);
        $this->Cell(30, $alt, utf8_decode(''), 'TBLR', 0, 'C');
        $this->Cell(50, $alt, utf8_decode('DESCRIPCIÓN'), 'TBLR', 0, 'L', true);
        $this->Cell(15, $alt, utf8_decode('COSTO:'), 'TBLR', 0, 'C', true);
        $this->Cell(30, $alt, utf8_decode(''), 'TBLR', 0, 'C');
        $this->Ln($alt);
        $this->Cell(95, $alt, utf8_decode(''), 'TLR', 0, 'C');
        $this->Cell(95, $alt, utf8_decode(''), 'TRL', 0, 'C');
        $this->Ln($alt);
        $this->Cell(95, $alt, utf8_decode(''), 'BLR', 0, 'C');
        $this->Cell(95, $alt, utf8_decode(''), 'BRL', 0, 'C');

        //autorizar envio de correo electronico
        $this->Ln($alt*2);
        $this->Cell(190, $alt, utf8_decode('EL SUSCRIPTOR AUTORIZA SE LE ENVIE POR CORREO ELECTRÓNICO:'), 'TBLR', 0, 'C', true);
        $this->Ln($alt);
        $this->Cell(15, $alt, utf8_decode('Factura'), 'TBLR', 0, 'C', true);
        $this->Cell(30, $alt, utf8_decode('SI [  ] NO [  ] '), 'TBLR', 0, 'C');
        $this->Cell(45, $alt, utf8_decode('Carta de Derechos Mínimos'), 'TBLR', 0, 'C', true);
        $this->Cell(30, $alt, utf8_decode('SI [  ] NO [  ] '), 'TBLR', 0, 'C');
        $this->Cell(40, $alt, utf8_decode('Contrato de Adhesión'), 'TBLR', 0, 'C', true);
        $this->Cell(30, $alt, utf8_decode('SI [  ] NO [  ] '), 'TBLR', 0, 'C');
        $this->Ln($alt);
        $this->Cell(50, $alt, utf8_decode('CORREO ELECTRÓNICO '), 'TLR', 0, 'C', true);
        $this->Cell(55, $alt, utf8_decode(''), 'TLR', 0, 'C');
        $this->Cell(40, $alt, utf8_decode('FIRMA'), 'TLR', 0, 'C', true);
        $this->Cell(45, $alt, utf8_decode(''), 'TLR', 0, 'C');
        $this->Ln($alt);
        $this->Cell(50, $alt, utf8_decode('AUTORIZADO:'), 'BLR', 0, 'C', true);
        $this->Cell(55, $alt, utf8_decode(''), 'BLR', 0, 'C');
        $this->Cell(40, $alt, utf8_decode('SUSCRIPTOR:'), 'BLR', 0, 'C', true);
        $this->Cell(45, $alt, utf8_decode(''), 'BLR', 0, 'C');


        //AUTORIZACIÓN PARA USO DE INFORMACIÓN DEL SUSCRIPTOR
        $this->Ln($alt * 2);
        $this->Cell(190, $alt, utf8_decode('AUTORIZACIÓN PARA USO DE INFORMACIÓN DEL SUSCRIPTOR'), 'TBLR', 0, 'C', true);
        $this->SetFont('Arial', '', 8);
        $this->Ln($alt);
        $this->Cell(5, $alt, utf8_decode('1. '), 'TL', 0, 'C');
        $this->Cell(185, $alt, utf8_decode('El Suscriptor SI [  ] NO [  ] autoriza que su información sea cedida o transmitida por el proveedor a terceros con fines'), 'TR', 0, 'L');
        $this->Ln($alt);
        $this->Cell(5, $alt-2, utf8_decode(''), 'L', 0, 'C');
        $this->Cell(46, $alt-2, utf8_decode('mercadotécnicos o publicitarios.'), '', 0, 'L');
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(12, $alt-2, utf8_decode('FIRMA'), '', 0, 'L');
        $this->SetFont('Arial', '', 8);
        $this->Cell(40, $alt-2, utf8_decode(''), 'B', 0, 'L');
        $this->Cell(87, $alt-2, utf8_decode(''), 'R', 0, 'L');
        $this->Ln($alt-2);
        $this->Cell(5, $alt, utf8_decode('2. '), 'L', 0, 'C');
        $this->Cell(185, $alt, utf8_decode('El suscriptor acepta SI [  ] NO [  ] recibir llamadas del proveedor de promociones de servicios o paquetes.'), 'R', 0, 'L');
        $this->Ln($alt);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(5, $alt-2, utf8_decode(''), 'L', 0, 'C');
        $this->Cell(12, $alt-2, utf8_decode('FIRMA'), '', 0, 'L');
        $this->Cell(40, $alt-2, utf8_decode(''), 'B', 0, 'L');
        $this->Cell(133, $alt-2, utf8_decode(''), 'R', 0, 'L');
        $this->Ln($alt-2);
        $this->Cell(190, $alt-4, utf8_decode(''), 'BLR', 0, 'C');

        //MEDIOS DE CONTACTO DEL PROVEEDOR PARA QUEJAS, ACLARACIONES, CONSULTAS Y CANCELACIONES
        $this->Ln($alt+2);
        $this->Cell(190, $alt, utf8_decode('MEDIOS DE CONTACTO DEL PROVEEDOR PARA QUEJAS, ACLARACIONES, CONSULTAS Y CANCELACIONES'), 'TBLR', 0, 'C', true);
        $this->Ln($alt);
        $this->Cell(21, $alt, utf8_decode('TELÉFONO:'), 'TBLR', 0, 'L', true);
        $this->Cell(74, $alt, utf8_decode(''), 'TBLR', 0, 'C');
        $this->SetFont('Arial', '', 8);
        $this->Cell(95, $alt, utf8_decode('Disponible las 24 horas del día los 7 días de la semana'), 'TBLR', 0, 'L');
        $this->Ln($alt);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(40, $alt, utf8_decode('CORREO ELECTRÓNICO:'), 'TBLR', 0, 'C', true);
        $this->Cell(55, $alt, utf8_decode(''), 'TBLR', 0, 'C');
        $this->SetFont('Arial', '', 8);
        $this->Cell(95, $alt, utf8_decode('Disponible las 24 horas del día los 7 días de la semana'), 'TBLR', 0, 'L');
        $this->Ln($alt);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(60, $alt, utf8_decode('CENTROS DE ATENCIÓN A CLIENTES'), 'TLR', 0, 'L', true);
        $this->SetFont('Arial', '', 8);
        $this->Cell(130, $alt, utf8_decode('Consultar horarios disponibles, días disponibles y centros de atención a clientes'), 'TLR', 0, 'L');
        $this->Ln($alt);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(60, $alt, utf8_decode(''), 'BLR', 0, 'L', true);
        $this->SetFont('Arial', '', 8);
        $this->Cell(130, $alt, utf8_decode('disponibles en la página de internet www.________________________.com.mx'), 'BLR', 0, 'L');

        //LA PRESENTE CARÁTULA Y EL CONTRATO DE ADHESIÓN SE ENCUENTRAN DISPONIBLES
        $this->AddPage();
        // $this->Ln($alt);
        $this->Cell(190, $alt, utf8_decode('LA PRESENTE CARÁTULA Y EL CONTRATO DE ADHESIÓN SE ENCUENTRAN DISPONIBLES EN:'), 'TBLR', 0, 'C', true);
        $this->Ln($alt);
        $this->Cell(40, $alt, utf8_decode('1.	La página del proveedor'), 'TBLR', 0, 'L', true);
        $this->Cell(150, $alt, utf8_decode('www.______________.com.mx'), 'TBLR', 0, 'L');
        $this->Ln($alt);
        $this->Cell(48, $alt, utf8_decode('2.	Buró comercial de PROFECO'), 'TBLR', 0, 'L', true);
        $this->Cell(142, $alt, utf8_decode('https://burocomercial.profeco.gob.mx/'), 'TBLR', 0, 'L');
        $this->Ln($alt);
        $this->Cell(81, $alt, utf8_decode('3.	Físicamente en los centros de atención del proveedor'), 'TBLR', 0, 'L', true);
        $this->Cell(109, $alt, utf8_decode('Consultar centros de atención a clientes en www.______________.com.mx'), 'TBLR', 0, 'L');

        //Scan
        $this->Ln($alt*2);
        $this->SetFont('Arial', 'B', 8);
        $this->MultiCell(190, $alt-1, utf8_decode('LA PRESENTE CARÁTULA SE RIGE CONFORME A LAS CLÁUSULAS DEL CONTRATO DE ADHESIÓN REGISTRADO EN PROFECO EL ____/____/______, CON NÚMERO: _________ DISPONIBLE EN EL SIGUIENTE CÓDIGO:'), 'TRL', 'L');
        $this->Image($this->qrcode, 85, 90, 40, 0);
        $this->Ln(($alt*5)+15);
        $this->MultiCell(190, $alt, utf8_decode('LAS FIRMAS INSERTAS ABAJO SON LA ACEPTACIÓN DE LA PRESENTE CARÁTULA Y CLAUSULADO DEL CONTRATO CON NÚMERO __________'), 'BRL', 'C');
        $x = 10;
        $y = 86;
        $this->Line($x, $y, $x, $y + 45);
        $x = 200;
        $y = 86;
        $this->Line($x, $y, $x, $y + 45);


        //FIRMAS
        $this->SetFont('Arial', '', 8);
        $this->Ln($alt * 2);
        $this->Cell(190, $alt, utf8_decode('Este contrato se firmó por duplicado en la Ciudad de _______________________, a ___ de __________ de ______.'), 0, 0, 'L');
        $this->Ln($alt * 4);
        $this->Cell(30, $alt, utf8_decode(''), 0, 0, 'C');
        $this->Cell(50, $alt, utf8_decode(''), 'B', 0, 'C');
        $this->Cell(30, $alt, utf8_decode(''), 0, 0, 'C');
        $this->Cell(50, $alt, utf8_decode(''), 'B', 0, 'C');
        $this->Cell(30, $alt, utf8_decode(''), 0, 0, 'C');
        $this->Ln($alt);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(30, $alt, utf8_decode(''), 0, 0, 'C');
        $this->Cell(50, $alt, utf8_decode('PROVEEDOR '), 0, 0, 'C');
        $this->Cell(30, $alt, utf8_decode(''), 0, 0, 'C');
        $this->Cell(50, $alt, utf8_decode('SUSCRIPTOR'), 0, 0, 'C');
        $this->Cell(30, $alt, utf8_decode(''), 0, 0, 'C');


    }

    function SetCol($col)
    {
        // Establecer la posición de una columna dada
        $this->col = $col;
        $x = 10 + $col * 100;
        $this->SetLeftMargin($x);
        $this->SetX($x);
    }

    function AcceptPageBreak()
    {
        // Método que acepta o no el salto automático de página
        if ($this->col < 1) {
            // Ir a la siguiente columna
            $this->SetCol($this->col + 1);
            // Establecer la ordenada al principio
            $this->SetY($this->y0);
            if($this->ctrl == false){
                if($this->salto) $this->Ln(6);
                $this->salto=true;
            }else{
                $this->ctrl = false;
            }
            
            // Seguir en esta página
            return false;
        } else {
            // Volver a la primera columna
            $this->SetCol(0);
           
            // Salto de página
            return true;
        }
    }
    function ChapterTitle($num, $label)
    {
        if ($num == 2) {
            // Título
            $this->SetFont('Arial', 'B', 8);
            $this->SetFillColor(255, 255, 255);
            $this->Cell(0, 3, utf8_decode($label), 0, 1, 'C', true);
            $this->Ln(3);
            // Guardar ordenada
            $this->y0 = $this->GetY();
        }
        $this->SetLeftMargin(10);
    }

    function ChapterTitleM($num, $label)
    {
        // Título
        $this->SetFont('Arial', 'B', 8);
        $this->SetFillColor(255, 255, 255);
        $this->Cell(0, 3, " ", 0, 1, 'C', true);
        $this->Cell(0, 6, utf8_decode($label), 0, 1, 'C', true);
        $this->Cell(0, 3, " ", 0, 1, 'C', true);
        $this->Ln(3);
        // Guardar ordenada
        $this->y0 = $this->GetY();
    }
    //estructura de las declaraciones
    function ChapterBody($num, $file)
    {
        // Fuente
        if ($num == 1) {
            $this->SetFont('Arial', 'B', 8);
        } else {
            $this->SetFont('Arial', '', 8);
        }
        if ($num == 2) {
            // Abrir fichero de texto
            $this->SetLeftMargin(20);
            // Imprimir texto en una columna de 6 cm de ancho
            $this->MultiCell(0, 4, "");
            // Datos
            foreach ($file as $row) {
                $this->SetFont('Arial', 'B', 8);
                $this->Cell(10, 4, utf8_decode($row[0]), 0, 'J');
                $this->SetFont('Arial', '', 8);
                $this->MultiCell(100, 4, utf8_decode($row[1]), 0, 'J');
                $this->Ln(3); //salto de linea
            }
        } elseif ($num == 3) {
            // Datos
            foreach ($file as $row) {
                $this->SetLeftMargin(30);
                $this->SetFont('Arial', 'B', 8);
                // Imprimir texto en una columna de 6 cm de ancho
                $this->Cell(5, 4, utf8_decode($row[0]), 0, 0);
                $this->SetFont('Arial', '', 8);
                $this->MultiCell(165, 4, utf8_decode($row[1]), 0, 'J');
            }
        } else {
            // Abrir fichero de texto
            $txt = file_get_contents($file);
            // Imprimir texto en una columna de 6 cm de ancho
            $this->MultiCell(0, 4, utf8_decode($txt));
            $this->Ln(3); //salto de linea
            // Volver a la primera columna
            $this->SetCol(0);
        }
        $this->SetLeftMargin(10);
    }
    //estructura de las columnas de las clausulas
    function ChapterBodyM($num, $file)
    {
        // Datos
        foreach ($file as $row) {
            if (count($row) > 1) {
                // Fuente
                $this->SetFont('Arial', 'B', 8);
                // Imprimir texto en una columna de 6 cm de ancho
                $this->MultiCell(90, 4, utf8_decode($row[0]), 0, 'J');
                // Fuente
                $this->SetFont('Arial', '', 8);
                $this->MultiCell(90, 4, utf8_decode($row[1]), 0, 'J');
                $this->Ln(2); //salto de linea
            } else {
                if($row[0] == 'imagen'){
                    $this->SetFont('Arial', '', 8);
                    $this->setFont('','U');
                    $this->SetTextColor(0, 21, 255);
                    $this->write(4, 'https://burocomercial.profeco.gob.mx/ca_spt/Coeficiente%20Comunicaciones,%20S.A.%20de%20C.V.!!Coeficiente%20Comunicaciones%20210-17.pdf','https://burocomercial.profeco.gob.mx/ca_spt/Coeficiente%20Comunicaciones,%20S.A.%20de%20C.V.!!Coeficiente%20Comunicaciones%20210-17.pdf');
                    $this->Ln(4); //salto de linea
                    $this->setFont('', '');
                    $this->SetTextColor(0, 0, 0);
                    $this->MultiCell(90, 4, utf8_decode('y en el siguiente código:'), 0, 'J');
                    $this->Ln(2); //salto de linea
                    $this->Image($this->qrcode, 135, 195, 40, 0);
                    $this->Ln(43); //salto de linea
                }else{
                    // Fuente
                    $this->SetFont('Arial', '', 8);
                    $this->MultiCell(90, 4, utf8_decode($row[0]), 0, 'J');
                    $this->Ln(2); //salto de linea
                }
            }
        }
        // Volver a la primera columna
        $this->SetCol(0);
    }
    //imprimir cuerpo de los segmentos del contrato
    function PrintDeclaraciones($num, $title, $file)
    {
        // Añadir declaraciones del contrato
        if ($num == 1) {
            $this->AddPage();
        }
        $this->ChapterTitle($num, $title);
        $this->ChapterBody($num, $file);
    }
    function PrintClausulas($num, $title, $file)
    {
        // Añadir clausulas del contrato
        $this->ChapterTitleM($num, $title);
        $this->ChapterBodyM($num, $file);
    }

}//FIN DE LA CLASE

$pdf = new PDF('P', 'mm', 'A4');
// Carga de datos
$pdf->SetFont('Arial','',0);
$pdf->imgLogo = 'logo_coe.png';
// trabajar los valores antes de añadir laprimera pagina


// INICIO DEL PROCESADO DE DATOS PARA LAS CONSULTAS
$db_modulos = condb_modulos();
$db_catalogos = condb_catalogos();
$db_usuarios = condb_usuarios();

$opt = isset($_GET['option']) ? 0 : $_POST['option'];
$mailflag = isset($_POST['isemail']) ? 1 : 0;

if (0 == $opt) {
    $cotId     = isset($_GET['cotId'])  ? decrypt($_GET['cotId']) : -1;
    $conId     = isset($_GET['conId'])  ? decrypt($_GET['conId']) : -1;
    $contContr = isset($_GET['conCon']) ? json_decode($_GET['conCon'], true) : -1;
    $orgType   = isset($_GET['orgType']) ? $_GET['orgType'] : -1;
    $conMod    = isset($_GET['option']) ? $_GET['option'] : -1;
    $coeRep    = isset($_GET['repLeg']) ? $_GET['repLeg'] : -1;
} else if (1 <= $opt && 4 >= $opt) {
    $cotId     = isset($_POST['cotId'])  ? decrypt($_POST['cotId']) : -1;
    $conId     = isset($_POST['conId'])  ? decrypt($_POST['conId']) : -1;
    $orgType   = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
    $conMod    = isset($_POST['option']) ? $_POST['option'] : -1;
    $contContr = array();
}
if (1==$mailflag){//revisar si el usuario necesita enviar el contrato por correo electroinico (email)
    $contactEmails = "";
    //$contactEmails = array();
    $cotId     = isset($_POST['cotId'])  ? decrypt($_POST['cotId']) : -1;
    $conId     = isset($_POST['conId'])  ? decrypt($_POST['conId']) : -1;
    $orgType   = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
    $conMod    = isset($_POST['option']) ? $_POST['option'] : -1;
    $contContr = isset($_POST['contContractArr']) ? json_decode($_POST['contContractArr'], true) : ""; //contactos imprimibles en el contrato
    $contArr   = isset($_POST['contMailContact']) ? json_decode($_POST['contMailContact'], true) : ""; //destinatarios mail

    $asunto       = isset($_POST['contractMsgSubject']) ? $_POST['contractMsgSubject'] : "Contrato Coeficiente Telecomunicaciones"; //asunto
    $mensajeHtml  = isset($_POST['contractMsgHtmlContent']) ? $_POST['contractMsgHtmlContent'] : ""; //"<h4><b>Envio cotización</b></h4>";
    $mensajePlano = isset($_POST['contractMsgPlainContent']) ? $_POST['contractMsgPlainContent'] : ""; //"Envio cotización";

    foreach ($contArr as $key => $value) {
        $contactEmails .= $value[2] . ",";
        // $contactEmails[$value[2]] = $value[1];
    }

    $contactEmails = substr($contactEmails, 0, -1);
}//fin de los datos para email

//*******************  pruebas segmento del correo */
// var_dump($cotId);
//***********************************************************************/


$fecha = date("Y-m-d H:i:s");

$mesArr = array("N/A", "enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre");

switch ($orgType) {
    case 0:  //si se trata de un prospecto
        $dbname = "prospectos";
        $fieldname = "prospecto_id";
        $typename = "prospecto";
        $asignfield = "asignadoa";
        $nameFieldName = " ";
        break;
    case 1:  //si se trata de un cliente
        $dbname = "clientes";
        $fieldname = "cliente_id";
        $typename = "cliente";
        $asignfield = "usuario_alta";
        $nameFieldName = " ";
        break;
}
//FIN DEL PROCESADO DE DATOS PARA LAS CONSULTAS










//INICIO DE LAS CONSUTAS DE LA BASE DE DATOS
$queryCont = "SELECT con.contacto_id, con.nombre, con.apellidos, con.tel_movil, con.email " .
    "FROM " . $cfgTableNameMod . ".contactos AS con ";
$contCont = 1;

if (0 == $conMod) {
    $queryCot = "SELECT cot.*, cot.fecha_alta AS cotfecalta, con.*, cli.*, cli.logo_ruta AS cliLogo, " . $nameFieldName . " ubi.*, dirdet.*, " .
        "usr.nombre AS venNom, usr.apellidos AS venApe, usr.departamento AS venDepto, usr.puesto AS venPuesto, usr.extension AS venExt, " .
        "usr.tel_movil AS venTelMovil, usr.email AS venEmail, usr.ubicacion_id " .
        "FROM " . $cfgTableNameMod . ".cotizaciones AS cot, " .
        $cfgTableNameMod . ".contactos AS con, " .
        $cfgTableNameMod . "." . $dbname . " AS cli, " .
        $cfgTableNameCat . ".cat_coe_ubicacion AS ubi, " .
        $cfgTableNameCat . ".cat_coe_contrato_dato AS ccd, " .
        $cfgTableNameUsr . ".usuarios AS usr, " .
        $cfgTableNameMod . ".direccion_detalles AS dirdet " .
        " WHERE cot.cotizacion_id=" . $cotId .
        " AND cot.contacto_id=con.contacto_id" .
        " AND cli." . $fieldname . "=cot.origen_id" .
        " AND cot.tipo_origen=" . $orgType .
        " AND usr.ubicacion_id=ubi.ubicacion_id" .
        " AND cot.origen_id=dirdet.origen_id" .
        " AND dirdet.tipo_origen=cot.tipo_origen" .
        " AND ccd.region = ubi.ubicacion_id" .
        " AND cot.usuario_alta=usr.usuario_id";
} else if (1 <= $conMod || 4 >= $conMod) {
    $queryCot = "SELECT cot.*, contr.*, contr.representante_id AS contrRep, contr.fecha_alta AS confecalta, cot.fecha_alta AS cotfecalta, con.*, " .
        "cli.*, cli.logo_ruta AS cliLogo, " .
        $nameFieldName . " ubi.*, dirdet.*, " .
        "usr.nombre AS venNom, usr.apellidos AS venApe, usr.departamento AS venDepto, usr.puesto AS venPuesto, usr.extension AS venExt, " .
        "usr.tel_movil AS venTelMovil, usr.email AS venEmail, usr.ubicacion_id " .
        "FROM " . $cfgTableNameMod . ".cotizaciones AS cot, " .
        $cfgTableNameMod . ".contactos AS con, " .
        $cfgTableNameMod . ".contratos AS contr, " .
        $cfgTableNameMod . "." . $dbname . " AS cli, " .
        $cfgTableNameCat . ".cat_estados AS ubi, " .
        $cfgTableNameUsr . ".usuarios AS usr, " .
        $cfgTableNameMod . ".direccion_detalles AS dirdet, " .
        $cfgTableNameMod . ".contrato_anexos AS conan, " .
        "(SELECT contrato_id FROM contrato_anexos WHERE cotizacion_id = " . $cotId . ") AS contId " .
        " WHERE conan.contrato_id=contId.contrato_id" .
        " AND contr.contrato_id=conan.contrato_id" .
        " AND cot.contacto_id=con.contacto_id" .
        " AND cli." . $fieldname . "=cot.origen_id" .
        " AND cot.tipo_origen=" . $orgType .
        " AND contr.contrato_ubicacion = ubi.estado_id" .
        " AND contr.cotizacion_id=cot.cotizacion_id" .
        " AND cot.origen_id=dirdet.origen_id" .
        " AND dirdet.tipo_origen=cot.tipo_origen" .
        " AND contr.usuario_alta=usr.usuario_id";
}

if (0 == $conMod) {
    $querProdSubTbl = "";
    $querProdSubQ = "";
    $querProdSubCon = "AND rcp.cotizacion_id = " . $cotId . " ";
} else if (2 == $conMod) {
    $querProdSubTbl = "";
    $querProdSubQ = ", " . $cfgTableNameMod . ".contratos AS contr " .
        ", (SELECT contrato_id FROM " . $cfgTableNameMod . ".contrato_anexos WHERE cotizacion_id = " . $cotId . ") AS contId ";
    $querProdSubCon = "AND contId.contrato_id = contr.contrato_id " .
        "AND (contr.cotizacion_id = rcp.cotizacion_id OR rcp.cotizacion_id = " . $cotId . ") ";
} else if (1 == $conMod || 3 == $conMod || 4 == $conMod) {
    $querProdSubTbl = $cfgTableNameMod . ".contrato_anexos AS conan, ";
    $querProdSubQ = ", " . $cfgTableNameMod . ".contratos AS contr " .
        ", (SELECT contrato_id FROM " . $cfgTableNameMod . ".contratos WHERE cotizacion_id = " . $cotId . ") AS contId ";
    $querProdSubCon = "AND (contId.contrato_id = conan.contrato_id AND contr.contrato_id = contId.contrato_id) " .
        "AND (conan.cotizacion_id = rcp.cotizacion_id OR rcp.cotizacion_id = contr.cotizacion_id) ";
}

$queryProd = "SELECT rcp.relacion_id, cvs.*, cvv.cantidad_vigencia, cvv.factor, rcp.cantidad, cvts.categoria_id, cvts.disponibilidad_txt, cvts.latencia_txt, " .
    "cvts.entrega_txt, cvts.ab_entregado_txt, cvts.router, cvts.tipo_servicio, " .
    "rcp.producto_precio_especial, rcp.producto_autorizado_por, rcp.sitio_asignado, rcp.vigencia, cvc.categoria_id, rcp.orden_trabajo_id " .
    "FROM " . $cfgTableNameCat . ".cat_venta_servicios AS cvs, " .
    $cfgTableNameCat . ".cat_venta_tipo_servicios AS cvts, " .
    $cfgTableNameMod . ".rel_cotizacion_producto AS rcp, " .
    $querProdSubTbl .
    $cfgTableNameCat . ".cat_venta_vigencia AS cvv, " .
    $cfgTableNameCat . ".cat_venta_categorias AS cvc " .
    $querProdSubQ .
    "WHERE cvs.servicio_id = rcp.producto_id " .
    "AND cvs.tipo_servicio = cvts.tipo_servicio_id " .
    "AND rcp.vigencia = cvv.vigencia_id " .
    "AND cvc.categoria_id = cvts.categoria_id " .
    $querProdSubCon .
    "ORDER BY rcp.cotizacion_id";

$queryRep = "SELECT cli.num_poder AS cliNumPoder, cli.fecha_poder AS cliFechaPoder, cli.notario AS cliNotario, cli.folio_mercantil AS cliFolio, " .
    "rep.nombre AS repNombre, rep.apellidos AS repApellidos, rep.num_poder AS escritura, rep.fecha_poder, rep.notario, rep.folio_mercantil " .
    "FROM " . $cfgTableNameMod . ".cotizaciones AS cot, " .
    $cfgTableNameMod . "." . $dbname . " AS cli, " .
    $cfgTableNameMod . ".representante_legal AS rep " .
    " WHERE cot.cotizacion_id=" . $cotId .
    " AND cli." . $fieldname . "=cot.origen_id" .
    " AND cot.tipo_origen=" . $orgType .
    " AND rep.representante_id=cli.representante_id";

$querySit = "SELECT DISTINCT sit.*, edo.estado AS edo " .
    "FROM " . $cfgTableNameMod . ".rel_cotizacion_producto AS rcp, " .
    $cfgTableNameMod . ".sitios AS sit, " .
    $cfgTableNameCat . ".cat_estados AS edo " .
    " WHERE rcp.cotizacion_id = " . $cotId .
    " AND sit.sitio_id = rcp.sitio_asignado " .
    " AND sit.estado = edo.estado_id";

$resultCot = mysqli_query($db_modulos, $queryCot);
// var_dump($resultCot);
$resultProd = mysqli_query($db_modulos, $queryProd);
// var_dump($queryProd);
$resultRep = mysqli_query($db_modulos, $queryRep);
// var_dump($queryRep);
$resultSit = mysqli_query($db_modulos, $querySit);
// var_dump($querySit);

//*************************************************************************/

/***IMPRIME FECHAS DE INSTALACIÓN***/
/***OBTENEMOS DATOS DE INSTALACIÓN***/
$domicilio = mysqli_fetch_assoc($resultSit);
$pdf->calle = $domicilio['domicilio'];
$pdf->codigoPostal = $domicilio['cp'];
$pdf->colonia = $domicilio['colonia'];
$pdf->municipio = $domicilio['municipio'];
$pdf->estado = $domicilio['edo'];
// var_dump($pdf->codigoPostal);

// *************************************************************************

// $prueba = mysqli_fetch_assoc($resultSit);
// for ($i = 0; $i < count($prueba); $i++) { }
if (!$resultCot || !$resultProd || !$resultRep || !$resultSit) {
    die("No se pudo obtener los datos para este contrato " . $queryCot);
} else {
    // die($queryProd);
    $rowCot = mysqli_fetch_assoc($resultCot);
    // var_dump($rowCot);
    $rowRep = mysqli_fetch_assoc($resultRep);

    $subtotal = 0;
    $total = 0;
    $subtotalAdec = 0;
    $totalAdec = 0;
    $precio = 0;
    $totalEquip = 0;
    $subtotalEquip = 0;

    if (0 == $conMod)
        $rowCot['contrRep'] = $coeRep;

    $queryCoeRep = "SELECT ccd.*, ccu.*, edo.*, ccd.logo_ruta AS coeLogo " .
        "FROM " . $cfgTableNameCat . ".cat_coe_contrato_dato AS ccd, " .
        $cfgTableNameCat . ".cat_coe_ubicacion AS ccu, " .
        $cfgTableNameCat . ".cat_estados AS edo " .
        "WHERE ccd.representante_id = " . $rowCot['contrRep'] . " " .
        "AND ccd.region = ccu.ubicacion_id " .
        "AND ccu.estado_id = edo.estado_id ";

    $resultCoeRep = mysqli_query($db_catalogos, $queryCoeRep);

    if (!$resultCoeRep) {
        die("No se pudo obtener los datos de representante legal para este contrato ");
    } else {
        $rowCoeRep = mysqli_fetch_assoc($resultCoeRep);
        $pdf->region = $rowCoeRep['region']; // se almacena el numero de region para los datos de cuenta bancaria quese imprimiran enel contrato
        $coeRazonSocial = $rowCoeRep['nombre_razon_social'];
        $coeNumPoder = $rowCoeRep['num_poder'];
        $coeFechaPoder = $rowCoeRep['fecha_poder'];
        $coeRepLegal = $rowCoeRep['nombre'];
        $coeNotario = $rowCoeRep['notario'];
        $coeConcText = $rowCoeRep['concesion_contrato'];
        $coeTel = $rowCoeRep['coe_tel'];
        $coeCdFirma = $rowCoeRep['ubicacion_nombre'];
        $coeEdoFirma = $rowCoeRep['estado'];
        $pdf->imgLogo = $rowCoeRep['coeLogo'];
    }

    $venEmail = $rowCot['venEmail'];
    $venNom = $rowCot['venNom'];
    $venApe = $rowCot['venApe'];

    $contCondition = "";
    if (0 == $conMod) {
        foreach ($contContr as $key => $value) {
            if (0 == $key)
                $contCondition = "WHERE con.contacto_id=" . decrypt($value);
            elseif (2 > $key)
                $contCondition .= " OR con.contacto_id=" . decrypt($value);
        }
        $cteEmail = "_______________";
        $regiNumCon = "________";
        $regiNumCte = "________";
        $fecAltaAnio = "________";
        $fecAltaMes = "__________";
        $fecAltaDia = "________";
        $contNotas = "";
    } else if (1 <= $conMod && 4 >= $conMod) {
        $contCondition = "WHERE con.contacto_id=" . $rowCot['contacto_id1'] . " OR con.contacto_id=" . $rowCot['contacto_id2'];
        $regiNumCon = $rowCot['contrato_preclave'] . $rowCot['contrato_id'];
        $regiNumCte = $rowCot[$fieldname];
        $cteEmail = $rowCot['email'];
        $fecAlta = explode(' ', $rowCot['confecalta']);
        $fecAltaNum = explode('-', $fecAlta[0]);
        $fecAltaAnio = $fecAltaNum[0];
        $fecAltaMes = $mesArr[(int) $fecAltaNum[1]];
        $fecAltaDia = $fecAltaNum[2];
        $contNotas = $rowCot['contrato_notas'];
        $contInicioVig = explode(" ", $rowCot['fecha_activacion']);
        $pdf->contInicioVig = $contInicioVig[0];
    }
}



//****** datos del suscriptor de la cotizacion */
$queryCont = $queryCont . $contCondition;
$resultCont = mysqli_query($db_modulos, $queryCont);
$suscriptor = mysqli_fetch_assoc($resultCont);
$pdf->nombreSuscriptor = $suscriptor['nombre'];
$pdf->apellidosSuscriptor = $suscriptor['apellidos'];
$pdf->emailSuscriptor = $suscriptor['email'];
// var_dump($suscriptor['contacto_id'],$suscriptor['nombre'], $suscriptor['apellidos'], $suscriptor['email']);
// FIN DE LAS CONSULTAS A LA BASE DE DATOS

/***OBTENER LOS DATOS PARA LA CARATULA***/
// /***IMPRIME CARATULA CON SERVICIOS***/
$pdf->numContrato = $regiNumCon;
if ("" != $rowCot['razon_social']){//si tiene razon social 
    $pdf->razonSocial = $rowCot['razon_social'];
    $pdf->nombreComercial = 'N/E';
    
}else{//si no tiene configura 
    $pdf->nombreComercial = $rowCot['nombre_comercial'];
    $pdf->razonSocial = 'N/E';
}
if (0 < mysqli_num_rows($resultRep)) {
    $pdf->repLegal = $rowRep['repNombre'] . ' ' . $rowRep['repApellidos'];
    $pdf->numEscritura = $rowRep['escritura'];
    $pdf->fecha = $rowRep['fecha_poder'];
    $pdf->notario = $rowRep['notario'];
    $pdf->folioMercantil = $rowRep['folio_mercantil'];
}
if ("" != $rowCot['num_interior'])
    $pdf->numInterior = $rowCot['num_interior'];
else
    $pdf->numInterior = "";
$pdf->domicilio = strtoupper($rowCot['domicilio'] . " " . $rowCot['num_exterior'] . " " . $pdf->numInterior);
$pdf->numExterior = $rowCot['num_exterior'];
$pdf->calle = $rowCot['domicilio'];
// $pdf->Cell(190, 6, utf8_decode("DOMICILIO: " . strtoupper($rowCot['domicilio'] . " " . $rowCot['num_exterior'] . " " . $intNum)), 'LR', 1, 'L', false);
$rfc = explode("-", $rowCot['rfc']);
$pdf->rfc = strtoupper($rfc[0] . "-" . $rfc[1]);
$pdf->homoclave = strtoupper($rfc[2]);
// var_dump($rowCot['telefono']);
$pdf->telCasa = $rowCot['telefono'];
$pdf->telOficina = $rowCot['telefono2'];
$pdf->telCelular = $rowCot['telefono3'];

//******************************************************/
// /***IMPRIME SERVICIOS EN PANEL***/
// /***OBTENEMOS LOS DATOS DEL PRODUCTO CONTRATADO PARA LASECCION DE DATOS DEL EQUIPO***/
/**
     * $vigenciaMeses
     * $precio
     * $vigencia = 0;
     * $adecArr = array();
     * $slaTxt = 0;
     * $router = "";
     * $subtotal
     * $desc
     * $total
     * $ift
     * $subidadn
     * $bajadadn
     * $enlArr = array();
     * $totalCot = $subtotal * 1.16;
     * $precioSub = 0;
     * $adecsubtotal = 0;
     * 
     * 
     * 
     */

$vigencia = 0;
$adecArr = array();
$slaTxt = 0;
$router = "";
if (0 >= mysqli_num_rows($resultProd)) {
    die("No se encuentran los datos de producto");
} else {
    $pdf->enlArr = array();
    while ($rowProd = mysqli_fetch_assoc($resultProd)) {
        if (1 == $rowProd['categoria_id']) {
            $pdf->vigenciaMeses = $rowProd['cantidad_vigencia'];
             $pdf->fechaPago = date('d/m/Y', strtotime("+" . $pdf->vigenciaMeses . " months", strtotime($pdf->contInicioVig)));
        }
        if (-1 != $rowProd['producto_precio_especial']) {
            $pdf->precio = $rowProd['producto_precio_especial'];
        } else {
            if (1 == $rowProd['tipo_cobro']) //si es pago recurrente por mes
                $pdf->precio = $rowProd['precio_' . $rowProd['cantidad_vigencia'] . 'm'];
            else
                $pdf->precio = $rowProd['precio_lista'];
        }
        if (5 == $rowProd['categoria_id']) {
            $pdf->adecArr[] = array($rowProd['descripcion'], $pdf->precio, $rowProd['cantidad'], $rowProd['servicio'], $rowProd['orden_trabajo_id']);
        } else {
            $pdf->subtotal = $pdf->subtotal + ($rowProd['cantidad'] * $pdf->precio);
            //$bajada NO ES DE LA CLASE SOLO SE USA AQUI 
            $bajada = str_replace("%n%", $rowProd['bajada'], $rowProd['descripcion']);
            $pdf->desc = str_replace("%u%", $rowProd['subida'], $bajada);
            //se posiciona a la derecha de la celda de descripción
            //imprime celdas de precios
            $pdf->total = $pdf->precio * $rowProd['cantidad'];

            //vuelve a la primera posición de la fila e imprime la primera celda (cantidad)

            $pdf->ift = (isset($rowProd['folio_ift_' . $rowProd['cantidad_vigencia'] . 'm']) && (0 != $rowProd['folio_ift_' . $rowProd['cantidad_vigencia'] . 'm'])) ? $rowProd['folio_ift_' . $rowProd['cantidad_vigencia'] . 'm'] : "N/A";
            if (1 == $rowProd['categoria_id']) {
                $pdf->subidadn = (1000 <= $rowProd['subida']) ? ($rowProd['subida'] / 1000) . " Gbps" : $rowProd['subida'] . " Mbps";
                $pdf->bajadadn = (1000 <= $rowProd['bajada']) ? ($rowProd['bajada'] / 1000) . " Gbps" : $rowProd['bajada'] . " Mbps";
                $pdf->router = $rowProd['router'];

                if (106 == $cotId || 107 == $cotId || 114 == $cotId || 116 == $cotId || 373 == $cotId || 497 == $cotId || 574 == $cotId || 575 == $cotId) {
                    $pdf->carIns = 0;
                } else
                    $pdf->carIns = $rowProd['cargo_adecuacion_' . $rowProd['cantidad_vigencia'] . 'm'] - $rowProd['sub_' . $rowProd['cantidad_vigencia'] . 'm'];

                $pdf->carCan = $rowProd['adec_' . $rowProd['cantidad_vigencia'] . 'm'];
                $pdf->enlArr[] = array($rowProd['servicio'], $pdf->desc, $pdf->carIns, $pdf->carCan, $rowProd['orden_trabajo_id'], $rowProd['sitio_asignado'], $rowProd['relacion_id']);
            }
        }
        $pdf->slaTxt .= $rowProd['disponibilidad_txt'] . " " . $rowProd['latencia_txt'] . " " . $rowProd['entrega_txt'] . " " . $rowProd['ab_entregado_txt'];
        
        if($rowProd['cantidad_vigencia']!= 0){
            $pdf->cargoCancelacion = number_format($rowProd['cargo_adecuacion_cancelacion_' . $rowProd['cantidad_vigencia'] . 'm'], 2, '.', ',');
        }else{
            $pdf->cargoCancelacion = "0.00";
        }
        if($rowProd['cantidad_vigencia'] != 0){
            $pdf->cargoInstalacion = number_format($rowProd['cuota_instalacion_' . $rowProd['cantidad_vigencia'] . 'm'], 2, '.', ',');
        }else{
           $pdf->cargoInstalacion = "0.00";
        }
        
    }
}

$pdf->totalCot = $pdf->subtotal * 1.16;

$pdf->precioSub = 0;
$pdf->adecsubtotal = 0;
//*************************************************************************/

/***IMPRIME FECHAS DE INSTALACIÓN***/
/***OBTENEMOS DATOS DE INSTALACIÓN***/

// *************************************************************************








// *************************************************************************
// *************************************************************************
// *************************************************************************
// *************************************************************************
$title = '';
$pdf->SetTitle($title);
$pdf->qrcode = '../../img/coe_cont_qrcode.png';
//primera pagina del contrato
$pdf->addPage();
$pdf->Caratula();
$pdf->PrintDeclaraciones(1, 'D E C L A R A C I O N E S', 'declaraciones1.txt');
$data = $pdf->LoadData('declaraciones2.txt');
$pdf->PrintDeclaraciones(2, 'D E C L A R A C I O N E S', $data);
$data = $pdf->LoadData('declaraciones3.txt');
$pdf->PrintDeclaraciones(3, 'D E C L A R A C I O N E S', $data);
$data = $pdf->LoadData('clausulas.txt');
$pdf->PrintClausulas(1, 'C L Á U S U L A S', $data);
$nombre = ($pdf->nombreComercial)? $pdf->nombreComercial : $pdf->razonSocial;

//aqui va la seccion de email
//si se solicita el envio de un email de este contrato

//si no entonces solo lo genera y lo muestra
// $pdf->Output('I','Contrato_'. $pdf->razonSocial.'.pdf',true);
// $pdf->Output();




//*************************************************************************
//*********************************EMAIL***********************************/
//si se solicita el envio de un email de este contrato
//si no entonces solo lo genera y lo muestra
//  procesa si debe enviar el correo o solo mostrarlo en pantalla
// *************************************************************************

if (1 != $mailflag) //Salida al navegador directa
    $pdf->Output();
else { //Envia por mail la cotización

    $filename = date("YmdHis") . $cotId . "_" . str_replace(" ", "", $rowCot['nombre_comercial']);
    $filepath = realpath('../../docs/pdf/contratos/');
    $pdfdoc = $pdf->Output($filepath . "/" . $filename . ".pdf", "F"); //crea un archivo

    if (file_exists($cfgServerLocationAbs . 'img/signatures/' . $usuarioId . '.png'))
        $sigPath = $cfgServerLocationAbs . 'img/signatures/' . $usuarioId . '.png';
    else
        $sigPath = "";

    $mailData = array(
        'dest' => $contactEmails . "," . $venEmail,
        'cc' => $venEmail,
        'cco' => "",
        'asunto' => $asunto,
        'msg' => $mensajeHtml,
        'adjunto1' => $cfgServerLocationAbs . "docs/pdf/contratos/" . $filename . ".pdf",
        'adjunto2' => "",
        'adjunto3' => "",
        'adjunto4' => "",
        'adjunto5' => "",
        'adjunto6' => "",
        'adjunto7' => "",
        'adjunto8' => "",
        'adjunto9' => "",
        'adjunto10' => "",
        'adjunto11' => "",
        'adjunto12' => "",
        'adjunto13' => "",
        'adjunto14' => "",
        'adjunto15' => "",
        'estatus' => "NO ENVIADO",
        'fecha_envio' => "0000-00-00 00:00:00",
        'nIDCorreo' => 1,
        'folio' => "0",
        'firma' => $sigPath,
        'observaciones' => "",
        'bestado' => "",
        'remitente' => $venEmail
    );

    // manda correo
    if (!sendBDEmail($mailData)) {
        $salida = "ERROR||Ocurrió un problema al enviar el documento por correo electrónico";
    } else {
        $contactEmailsArr = explode(",", $contactEmails);
        foreach ($contactEmailsArr as $key => $value) {
            writeOnJournal($usuarioId, "Ha enviado un correo a nombre de [" . $venNom . " " . $venApe . "] de la cotización con id: [" . $cotId . "] al email [" . $value . "]");
        }
        $salida = "OK||El archivo se ha enviado";
    }
    //borra archivo que se envió y ya no se necesita
    //unlink($filepath.$filename);
}
