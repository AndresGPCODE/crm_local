<?php  //-*-mode: php-*-
header('Content-Type: text/html; charset=UTF-8');

//esta librería se encarga de la encriptación de contraseñas

$metodo = "AES-256-CBC";
$key    = "G4r4b4t0!";
$iv     = "543m4dm1n";

$hash = hash('md5', $key);   
$genIV = substr(hash('md5',$iv), 0, 16);

function encrypt($input) {
  global $metodo;
  global $key;
  global $iv;
  global $hash;
  global $genIV;
  
  //error_log("Entrada: ".$input); 
    
  /*$hash = hash('md5', $key);   
  $genIV = substr(hash('md5',$iv), 0, 16);*/
  $output = openssl_encrypt($input, $metodo, $hash, 0, $genIV);
  //error_log("openssl_encrypt: ".$output); 
  $output = base64_encode($output);
  //error_log("Salida base64_encode: ".$output); 
  
  return $output;
}
  
function decrypt($input) {
  global $metodo;
  global $key;
  global $iv;
  global $hash;
  global $genIV; 
  
  //error_log("Entrada: ".$input); 
  
  /*$hash = hash('md5', $key);    
  $genIV = substr(hash('md5',$iv), 0, 16);  */
  $output = base64_decode($input);
  //error_log("base64_decode : ".$output); 
  $output = openssl_decrypt($output, $metodo, $hash, 0, $genIV);
  //error_log("Salida openssl_decrypt: ".$output); 
  
  return $output;
}

?>
