<?php
session_start();
set_time_limit(0);

include_once "common.php";
include_once "encrypt.php";
include_once "../fpdf/fpdf.php";
//require "../fpdf/forcejustify.php";
require_once '../mail/swiftmailer-master/lib/swift_required.php';

header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

$usuario = $_SESSION['usuario'];
$usuarioId = $_SESSION['usrId'];
$gruposArr = $_SESSION['grupos'];
$usrUbica  = $_SESSION['usrUbica'];

$salida = "";
$fechaNow = date("Y-m-d");
$region = "";

class PDF extends FPDF
{
  function Footer(){
    $this->SetY(-15);
    $this->SetFont('Arial','B',8);
    $this->Cell(0,5,utf8_decode('Página '.$this->PageNo()."/{nb}"),0,0,'R');
  }

}

$pdf = new PDF();
$pdf->PageNo();
$pdf->AliasNbPages();
$db_modulos = condb_modulos();
$db_catalogos = condb_catalogos();
$db_usuarios = condb_usuarios();

$opt = isset($_GET['option']) ? 0 : $_POST['option'];
$mailflag = isset($_POST['isemail']) ? 1 : 0;

if(0==$opt){
  $cotId     = isset($_GET['cotId'])  ? decrypt($_GET['cotId']) : -1;
  $conId     = isset($_GET['conId'])  ? decrypt($_GET['conId']) : -1;
  $contContr = isset($_GET['conCon']) ? json_decode($_GET['conCon'],true) : -1;
  $orgType   = isset($_GET['orgType']) ? $_GET['orgType'] : -1;
  $conMod    = isset($_GET['option']) ? $_GET['option'] : -1;
  $coeRep    = isset($_GET['repLeg']) ? $_GET['repLeg'] : -1;
}
else if(1<=$opt&&4>=$opt){
  $cotId     = isset($_POST['cotId'])  ? decrypt($_POST['cotId']) : -1;
  $conId     = isset($_POST['conId'])  ? decrypt($_POST['conId']) : -1;
  $orgType   = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
  $conMod    = isset($_POST['option']) ? $_POST['option'] : -1;
  $contContr = array();
}
if(1==$mailflag){
  $contactEmails = "";
  //$contactEmails = array();
  $cotId     = isset($_POST['cotId'])  ? decrypt($_POST['cotId']) : -1;
  $conId     = isset($_POST['conId'])  ? decrypt($_POST['conId']) : -1;
  $orgType   = isset($_POST['orgType']) ? $_POST['orgType'] : -1;
  $conMod    = isset($_POST['option']) ? $_POST['option'] : -1;
  $contContr = isset($_POST['contContractArr']) ? json_decode($_POST['contContractArr'],true) : "";//contactos imprimibles en el contrato
  $contArr   = isset($_POST['contMailContact']) ? json_decode($_POST['contMailContact'],true) : "";//destinatarios mail
  
  $asunto       = isset($_POST['contractMsgSubject']) ? $_POST['contractMsgSubject'] : "Contrato Coeficiente Telecomunicaciones";//asunto
  $mensajeHtml  = isset($_POST['contractMsgHtmlContent']) ? $_POST['contractMsgHtmlContent'] : "";//"<h4><b>Envio cotización</b></h4>";
  $mensajePlano = isset($_POST['contractMsgPlainContent']) ? $_POST['contractMsgPlainContent'] : "";//"Envio cotización";
  
  foreach($contArr as $key => $value){
    $contactEmails .= $value[2].",";
   // $contactEmails[$value[2]] = $value[1];
  }
  $contactEmails = substr($contactEmails,0,-1);
}

$fecha = date("Y-m-d H:i:s");

$mesArr = array("N/A","enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre");

switch($orgType){
  case 0:  //si se trata de un prospecto
    $dbname = "prospectos";
    $fieldname = "prospecto_id";
    $typename = "prospecto";
    $asignfield = "asignadoa";
    $nameFieldName = " ";
  break;
  case 1:  //si se trata de un cliente
    $dbname = "clientes";
    $fieldname = "cliente_id";
    $typename = "cliente";
    $asignfield = "usuario_alta";
    $nameFieldName = " ";
  break;   
}

$queryCont = "SELECT con.nombre, con.apellidos, con.tel_movil, con.email ".
              "FROM ".$cfgTableNameMod.".contactos AS con ";
$contCont = 1;

if(0==$conMod){
  $queryCot = "SELECT cot.*, cot.fecha_alta AS cotfecalta, con.*, cli.*, cli.logo_ruta AS cliLogo, ".$nameFieldName." ubi.*, dirdet.*, ".
              "usr.nombre AS venNom, usr.apellidos AS venApe, usr.departamento AS venDepto, usr.puesto AS venPuesto, usr.extension AS venExt, ".
              "usr.tel_movil AS venTelMovil, usr.email AS venEmail, usr.ubicacion_id ".
              "FROM ".$cfgTableNameMod.".cotizaciones AS cot, ".
                      $cfgTableNameMod.".contactos AS con, ".
                      $cfgTableNameMod.".".$dbname." AS cli, ".
                      $cfgTableNameCat.".cat_coe_ubicacion AS ubi, ".
                      $cfgTableNameCat.".cat_coe_contrato_dato AS ccd, ".
                      $cfgTableNameUsr.".usuarios AS usr, ".
                      $cfgTableNameMod.".direccion_detalles AS dirdet ".
              " WHERE cot.cotizacion_id=".$cotId.
              " AND cot.contacto_id=con.contacto_id".
              " AND cli.".$fieldname."=cot.origen_id".
              " AND cot.tipo_origen=".$orgType.
              " AND usr.ubicacion_id=ubi.ubicacion_id".
              " AND cot.origen_id=dirdet.origen_id".
              " AND dirdet.tipo_origen=cot.tipo_origen".
              " AND ccd.region = ubi.ubicacion_id".
              " AND cot.usuario_alta=usr.usuario_id"; 
} 
else if(1<=$conMod||4>=$conMod){
  $queryCot = "SELECT cot.*, contr.*, contr.representante_id AS contrRep, contr.fecha_alta AS confecalta, cot.fecha_alta AS cotfecalta, con.*, ".
              "cli.*, cli.logo_ruta AS cliLogo, ".
              $nameFieldName." ubi.*, dirdet.*, ".
              "usr.nombre AS venNom, usr.apellidos AS venApe, usr.departamento AS venDepto, usr.puesto AS venPuesto, usr.extension AS venExt, ".
              "usr.tel_movil AS venTelMovil, usr.email AS venEmail, usr.ubicacion_id ".
              "FROM ".$cfgTableNameMod.".cotizaciones AS cot, ".
                      $cfgTableNameMod.".contactos AS con, ".
                      $cfgTableNameMod.".contratos AS contr, ".
                      $cfgTableNameMod.".".$dbname." AS cli, ".
                      $cfgTableNameCat.".cat_estados AS ubi, ".
                      $cfgTableNameUsr.".usuarios AS usr, ".
                      $cfgTableNameMod.".direccion_detalles AS dirdet, ".
                      $cfgTableNameMod.".contrato_anexos AS conan, ".
              "(SELECT contrato_id FROM contrato_anexos WHERE cotizacion_id = ".$cotId.") AS contId ".
              " WHERE conan.contrato_id=contId.contrato_id".
              " AND contr.contrato_id=conan.contrato_id".
              " AND cot.contacto_id=con.contacto_id".
              " AND cli.".$fieldname."=cot.origen_id".
              " AND cot.tipo_origen=".$orgType.
              " AND contr.contrato_ubicacion = ubi.estado_id".
              " AND contr.cotizacion_id=cot.cotizacion_id".
              " AND cot.origen_id=dirdet.origen_id".
              " AND dirdet.tipo_origen=cot.tipo_origen".
              " AND contr.usuario_alta=usr.usuario_id"; 
}

if(0==$conMod){
  $querProdSubTbl = "";
  $querProdSubQ = "";
  $querProdSubCon = "AND rcp.cotizacion_id = ".$cotId." ";
}
else if(2==$conMod){
  $querProdSubTbl = "";
  $querProdSubQ = ", ".$cfgTableNameMod.".contratos AS contr ".
                  ", (SELECT contrato_id FROM ".$cfgTableNameMod.".contrato_anexos WHERE cotizacion_id = ".$cotId.") AS contId ";
  $querProdSubCon = "AND contId.contrato_id = contr.contrato_id ".
                    "AND (contr.cotizacion_id = rcp.cotizacion_id OR rcp.cotizacion_id = ".$cotId.") ";
}
else if(1==$conMod||3==$conMod||4==$conMod){
  $querProdSubTbl = $cfgTableNameMod.".contrato_anexos AS conan, ";
  $querProdSubQ = ", ".$cfgTableNameMod.".contratos AS contr ".
                  ", (SELECT contrato_id FROM ".$cfgTableNameMod.".contratos WHERE cotizacion_id = ".$cotId.") AS contId ";
  $querProdSubCon = "AND (contId.contrato_id = conan.contrato_id AND contr.contrato_id = contId.contrato_id) ".
                    "AND (conan.cotizacion_id = rcp.cotizacion_id OR rcp.cotizacion_id = contr.cotizacion_id) ";
}

$queryProd = "SELECT rcp.relacion_id, cvs.*, cvv.cantidad_vigencia, cvv.factor, rcp.cantidad, cvts.categoria_id, cvts.disponibilidad_txt, cvts.latencia_txt, ".
             "cvts.entrega_txt, cvts.ab_entregado_txt, cvts.router, cvts.tipo_servicio, ".
             "rcp.producto_precio_especial, rcp.producto_autorizado_por, rcp.sitio_asignado, rcp.vigencia, cvc.categoria_id, rcp.orden_trabajo_id ".
             "FROM ".$cfgTableNameCat.".cat_venta_servicios AS cvs, ".
                     $cfgTableNameCat.".cat_venta_tipo_servicios AS cvts, ".
                     $cfgTableNameMod.".rel_cotizacion_producto AS rcp, ".
                     $querProdSubTbl.
                     $cfgTableNameCat.".cat_venta_vigencia AS cvv, ".
                     $cfgTableNameCat.".cat_venta_categorias AS cvc ".                   
             $querProdSubQ.   
             "WHERE cvs.servicio_id = rcp.producto_id ".
             "AND cvs.tipo_servicio = cvts.tipo_servicio_id ".
             "AND rcp.vigencia = cvv.vigencia_id ".
             "AND cvc.categoria_id = cvts.categoria_id ".
             $querProdSubCon.
             "ORDER BY rcp.cotizacion_id";
             
$queryRep = "SELECT cli.num_poder AS cliNumPoder, cli.fecha_poder AS cliFechaPoder, cli.notario AS cliNotario, cli.folio_mercantil AS cliFolio, ".
            "rep.nombre AS repNombre, rep.apellidos AS repApellidos, rep.num_poder AS escritura, rep.fecha_poder, rep.notario, rep.folio_mercantil ".
            "FROM ".$cfgTableNameMod.".cotizaciones AS cot, ".
                    $cfgTableNameMod.".".$dbname." AS cli, ".
                    $cfgTableNameMod.".representante_legal AS rep ".
            " WHERE cot.cotizacion_id=".$cotId.
            " AND cli.".$fieldname."=cot.origen_id".
            " AND cot.tipo_origen=".$orgType.
            " AND rep.representante_id=cli.representante_id"; 
            
$querySit = "SELECT DISTINCT sit.*, edo.estado AS edo ".
            "FROM ".$cfgTableNameMod.".rel_cotizacion_producto AS rcp, ".
                    $cfgTableNameMod.".sitios AS sit, ".
                    $cfgTableNameCat.".cat_estados AS edo ".
            " WHERE rcp.cotizacion_id = ".$cotId.
            " AND sit.sitio_id = rcp.sitio_asignado ".
            " AND sit.estado = edo.estado_id";
            

$resultCot = mysqli_query($db_modulos,$queryCot);
// var_dump($resultCot);
$resultProd = mysqli_query($db_modulos,$queryProd);
// var_dump($queryProd);
$resultRep = mysqli_query($db_modulos,$queryRep);
// var_dump($queryRep);
$resultSit = mysqli_query($db_modulos,$querySit);
// var_dump($querySit);
//if(false){
if(!$resultCot||!$resultProd||!$resultRep||!$resultSit){
  die("No se pudo obtener los datos para este contrato ".$queryCot);
}
else{
 // die($queryProd);
  $rowCot = mysqli_fetch_assoc($resultCot);
  $rowRep = mysqli_fetch_assoc($resultRep);
  
  $subtotal = 0;
  $total = 0;
  $subtotalAdec = 0;
  $totalAdec = 0;
  $precio = 0;
  $totalEquip = 0;
  $subtotalEquip = 0;
  
  if(0==$conMod)
    $rowCot['contrRep'] = $coeRep;
  
  $queryCoeRep = "SELECT ccd.*, ccu.*, edo.*, ccd.logo_ruta AS coeLogo ".
                 "FROM ".$cfgTableNameCat.".cat_coe_contrato_dato AS ccd, ".
                         $cfgTableNameCat.".cat_coe_ubicacion AS ccu, ".
                         $cfgTableNameCat.".cat_estados AS edo ".
                 "WHERE ccd.representante_id = ".$rowCot['contrRep']." ".
                 "AND ccd.region = ccu.ubicacion_id ".
                 "AND ccu.estado_id = edo.estado_id ";
  
  $resultCoeRep = mysqli_query($db_catalogos,$queryCoeRep);
  
  if(!$resultCoeRep){
    die("No se pudo obtener los datos de representante legal para este contrato ");
  }
  else{
    $rowCoeRep = mysqli_fetch_assoc($resultCoeRep);
    $region = $rowCoeRep['region'];// se almacena el nuero de region para los datos de cuenta bancaria quese imprimiran enel contrato
    $coeRazonSocial = $rowCoeRep['nombre_razon_social'];
    $coeNumPoder = $rowCoeRep['num_poder'];
    $coeFechaPoder = $rowCoeRep['fecha_poder'];
    $coeRepLegal = $rowCoeRep['nombre'];
    $coeNotario = $rowCoeRep['notario'];
    $coeConcText = $rowCoeRep['concesion_contrato'];
    $coeTel = $rowCoeRep['coe_tel'];
    $coeCdFirma = $rowCoeRep['ubicacion_nombre'];
    $coeEdoFirma = $rowCoeRep['estado'];
    $secLogoPath = $rowCoeRep['coeLogo'];
  }
  
  $venEmail = $rowCot['venEmail']; 
  $venNom = $rowCot['venNom'];
  $venApe = $rowCot['venApe'];
  
  $contCondition = "";
  if(0==$conMod){
    foreach($contContr AS $key => $value){
      if(0==$key)
        $contCondition = "WHERE con.contacto_id=".decrypt($value);
      elseif(2>$key)
        $contCondition .= " OR con.contacto_id=".decrypt($value);
    }
    $cteEmail = "_______________";
    $regiNumCon = "________";
    $regiNumCte = "________";
    $fecAltaAnio = "________";
    $fecAltaMes = "__________"; 
    $fecAltaDia = "________";
    $contNotas = "";
  }
  else if(1<=$conMod&&4>=$conMod){
    $contCondition = "WHERE con.contacto_id=".$rowCot['contacto_id1']." OR con.contacto_id=".$rowCot['contacto_id2'];
    $regiNumCon = $rowCot['contrato_preclave'].$rowCot['contrato_id'];
    $regiNumCte = $rowCot[$fieldname];
    $cteEmail = $rowCot['email'];
    $fecAlta = explode(' ',$rowCot['confecalta']);
    $fecAltaNum = explode('-',$fecAlta[0]);
    $fecAltaAnio = $fecAltaNum[0];
    $fecAltaMes = $mesArr[(int)$fecAltaNum[1]]; 
    $fecAltaDia = $fecAltaNum[2];
    $contNotas = $rowCot['contrato_notas'];
    $contInicioVig = explode(" ",$rowCot['fecha_activacion']);
  }
  /*else{
    $cteEmail = "_______________";
    $regiNumCon = "________";
    $regiNumCte = "________";
    $fecAltaAnio = "________";
    $fecAltaMes = "__________"; 
    $fecAltaDia = "________";
    $contNotas = "";
    $venEmail = "";
    $venNom = "_________";
    $venApe = "___________";
    $contNotas = "____________";
    $contInicioVig = "_________";
    $rowCot['razon_social'] = "____________________________________";
    $rowCot['nombre_comercial'] = "____________________________________";
    $rowRep['repNombre'] = "________________________";
    $rowRep['repApellidos'] = "________________________";
    $rowRep['escritura'] = "___________________";
    $rowRep['fecha_poder'] = "___________________";
    $rowRep['notario'] = "___________________";
    $rowRep['folio_mercantil'] = "___________________";
    $rowCot['domicilio'] = "____________________________________";
    $rowCot['num_exterior'] = "_____";
    
    
    
    
  }*/
  
  $queryCont = $queryCont.$contCondition; 
  $resultCont = mysqli_query($db_modulos,$queryCont);
  $pdf->AddPage();//carga la pagina del pdf
  $pagCnt = 0;
  
  //Cell(ancho,alto,texto,borde[0:no 1:borde] posicion borde[L:izquierda T:arriba R:derecha B:abajo],posicion posterior[0:derecha 1:siguiente linea 2:abajo],alineación[L:derecha C:centro R:derecha],relleno[true:relleno false:sin relleno],url);
  
  /***IMPRIME CARATULA CON SERVICIOS***/
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->SetTextColor(0,0,0);
  $pdf->Cell(120,30, utf8_decode(''),'LTR', 0 , 'L', false );
  $pdf->Cell(70,20, utf8_decode('Contrato # '.$regiNumCon),'TR', 2 , 'L', false );
  $pdf->Cell(70,10, utf8_decode('Anexo A: Carátula'),'RB', 1 , 'C', false );
  if(file_exists($secLogoPath))
    $pdf->Image($secLogoPath, 20, 12, 50, 0); // imagen de la empresa
  $pdf->SetFont('Arial', '', 8);
  if(""!=$rowCot['razon_social'])
    $pdf->Cell(190,6, utf8_decode("SUSCRIPTOR O RAZÓN SOCIAL: ".strtoupper($rowCot['razon_social'])),'LR', 1 , 'L', false );
  else
    $pdf->Cell(190,6, utf8_decode("SUSCRIPTOR O RAZÓN SOCIAL: ".strtoupper($rowCot['nombre_comercial'])),'LR', 1 , 'L', false );
    
  if(0<mysqli_num_rows($resultRep)){
    $pdf->Cell(190,6, utf8_decode("REPRESENTANTE LEGAL: ".$rowRep['repNombre'].' '.$rowRep['repApellidos']),'LR', 1 , 'L', false );
    $pdf->Cell(190,6, utf8_decode('ESCRITURA No. '.$rowRep['escritura'].'    FECHA: '.$rowRep['fecha_poder'].'    NOTARIO: '.$rowRep['notario'].'    FOLIO MERCANTIL: '.$rowRep['folio_mercantil'] ),'LR', 1 , 'L', false );
  }
  if(""!=$rowCot['num_interior'])
    $intNum = "Int. ".$rowCot['num_interior'];
  else
    $intNum = "";
  
  $pdf->Cell(190,6, utf8_decode("DOMICILIO: ".strtoupper($rowCot['domicilio']." ".$rowCot['num_exterior']." ".$intNum)),'LR', 1 , 'L', false );
  $rfc = explode("-",$rowCot['rfc']);
  $pdf->Cell(95,6, utf8_decode("R.F.C.: ".strtoupper($rfc[0]."-".$rfc[1])),'L', 0 , 'L', false );
  $pdf->Cell(95,6, utf8_decode("HOMOCLAVE: ".strtoupper($rfc[2])),'R', 1 , 'L', false );
  $pdf->Cell(63,6, utf8_decode("TELEFONO CASA: (".$rowCot['telefono'].")"),'LB', 0 , 'L', false );
  $pdf->Cell(63,6, utf8_decode("OFICINA: (".$rowCot['telefono2'].")"),'B', 0 , 'L', false );
  $pdf->Cell(64,6, utf8_decode("CELULAR: (".$rowCot['telefono3'].")"),'RB', 1 , 'L', false );
  
  /***IMPRIME SERVICIOS EN PANEL***/
  
  $posx2 = $pdf->GetX();
  $posy2 = $pdf->GetY();
  
  $vigencia = 0;  
  $adecArr = array();
  $slaTxt = 0;
  $router = "";
  
  $pdf->Cell(95,5, utf8_decode("PLAN"),'LR', 1 , 'C', false );
  $pdf->SetFont('Arial','B',8);

  if(0>=mysqli_num_rows($resultProd)){
    die("No se encuentran los datos de producto");
  }
  else{
    $enlArr = array();
    while($rowProd = mysqli_fetch_assoc($resultProd)){ 
      if(1==$rowProd['categoria_id']){
        $vigenciaMeses = $rowProd['cantidad_vigencia'];
      } 
      if(-1!=$rowProd['producto_precio_especial']){
        $precio = $rowProd['producto_precio_especial'];
      }
      else{
        if(1==$rowProd['tipo_cobro']) //si es pago recurrente por mes
          $precio = $rowProd['precio_'.$rowProd['cantidad_vigencia'].'m'];
        else
         $precio = $rowProd['precio_lista'];
      }
      if(5==$rowProd['categoria_id']){
        $adecArr[] = array($rowProd['descripcion'],$precio,$rowProd['cantidad'],$rowProd['servicio'],$rowProd['orden_trabajo_id']);
      }
      else{
        $subtotal = $subtotal+($rowProd['cantidad']*$precio);
      
        $bajada = str_replace("%n%",$rowProd['bajada'],$rowProd['descripcion']);
        $desc = str_replace("%u%",$rowProd['subida'],$bajada);
        
        $posx = $pdf->GetX();
        $posy = $pdf->GetY();
        
        $pdf->SetXY($posx+10.4,$posy); //se recorre a la posición de la segunda columna
        
        $pdf->SetFont('Arial','IB',8); 
        $pdf->MultiCell(84.5,4, utf8_decode($rowProd['servicio']),'R', 'L', false ); //Imprime descripción
        
        $pdf->SetFont('Arial','',8); 
        $cellHeight = $pdf->GetY()-$posy; //obtiene la altura de la celda de descripcion porque es dinámica
        
        $pdf->SetXY($posx+84.5+10.4,$posy); //se posiciona a la derecha de la celda de descripción
        
        //imprime celdas de precios
        $total = $precio*$rowProd['cantidad'];
        
        //vuelve a la primera posición de la fila e imprime la primera celda (cantidad)
        $pdf->SetXY($posx,$posy);
        $pdf->SetFont('Arial','B',8); 
        
        $pdf->Cell(10.5,$cellHeight, $rowProd['cantidad'],'L', 1 , 'C', false );
        
        $ift = (isset($rowProd['folio_ift_'.$rowProd['cantidad_vigencia'].'m'])&&(0!=$rowProd['folio_ift_'.$rowProd['cantidad_vigencia'].'m']))?$rowProd['folio_ift_'.$rowProd['cantidad_vigencia'].'m']:"N/A";
        $pdf->Cell(95,5, utf8_decode('FOLIO IFT: '.$ift),'LR', 2 , 'L', false );
        
        $posx = $pdf->GetX();
        $posy = $pdf->GetY();
        
        $pdf->SetXY($posx+40,$posy);
        
        $pdf->MultiCell(55,4, utf8_decode('CARACTERÍSTICAS: '.$desc),'R', 'L', false );
        
        $cellHeight = $pdf->GetY()-$posy;
        $pdf->SetXY($posx,$posy);
        $pdf->Cell(40,$cellHeight, utf8_decode('GIGABYTE:_____________'),'L', 2 , 'L', false );
        if(1==$rowProd['categoria_id']){
          $subidadn = (1000<=$rowProd['subida'])?($rowProd['subida']/1000)." Gbps":$rowProd['subida']." Mbps";
          $bajadadn = (1000<=$rowProd['bajada'])?($rowProd['bajada']/1000)." Gbps":$rowProd['bajada']." Mbps";
          
          $pdf->Cell(95,5, utf8_decode('VELOCIDAD PROMEDIO DE RECEPCIÓN:'.$bajadadn),'LR', 1 , 'L', false );
          $pdf->Cell(95,5, utf8_decode('VELOCIDAD PROMEDIO DE ENVÍO:'.$subidadn),'LR', 1 , 'L', false );
          $router = $rowProd['router'];
          
          if(106==$cotId||107==$cotId||114==$cotId||116==$cotId||373==$cotId||497==$cotId||574==$cotId||575==$cotId){
            $carIns = 0; 
          }
          else
            $carIns = $rowProd['cargo_adecuacion_'.$rowProd['cantidad_vigencia'].'m']-$rowProd['sub_'.$rowProd['cantidad_vigencia'].'m'];
          
          $carCan = $rowProd['adec_'.$rowProd['cantidad_vigencia'].'m'];
            
               
          $enlArr[] = array($rowProd['servicio'],$desc,$carIns,$carCan,$rowProd['orden_trabajo_id'],$rowProd['sitio_asignado'],$rowProd['relacion_id']);
        } 
        $pdf->Cell(95,2, utf8_decode(''),'LR', 2 , 'L', false );     
      } 
      $slaTxt .= $rowProd['disponibilidad_txt']." ".$rowProd['latencia_txt']." ".$rowProd['entrega_txt']." ".$rowProd['ab_entregado_txt']; 
    }
  }
  
  $posy3 = $pdf->GetY();
  
  $totalCot = $subtotal*1.16;
  
  $precioSub = 0;
  $adecsubtotal = 0;
  
  
  /***IMPRIME PANEL DE MODEM***/
  
  $pdf->MultiCell(95,6, utf8_decode(''),'LRB', 'L', false );
  if(0<strlen($router)){
    $pdf->Cell(95,5, utf8_decode("MODEM"),'LR', 1 , 'C', false );
    $pdf->Cell(95,5, utf8_decode("MODEM:____________________________________________"),'LR', 1 , 'L', false );
    $pdf->Cell(95,5, utf8_decode("MODELO:___________________________________________"),'LR', 1 , 'L', false );
    $pdf->Cell(95,5, utf8_decode("NUMERO DE SERIE:__________________________________"),'LRB', 1 , 'L', false );
  }
  $pdf->Cell(95,5, utf8_decode("DECODIFICADOR"),'LR', 1 , 'C', false );
  $pdf->Cell(95,5, utf8_decode("CANTIDAD____________________________________________"),'LR', 1 , 'L', false );
  $pdf->Cell(95,5, utf8_decode("MARCA_______________________________________________"),'LR', 1 , 'L', false );
  $pdf->Cell(95,5, utf8_decode("MODELO______________________________________________"),'LR', 1 , 'L', false );
  $pdf->Cell(95,5, utf8_decode("NÚMERO DE SERIE_____________________________________"),'LRB', 1 , 'L', false );
  
  $posy4 = $pdf->GetY();
  
  /***IMPRIME DATOS BANCARIOS***/
  $pdf->SetXY($posx2+95,$posy2);
  $pdf->Cell(95,5, utf8_decode('PAGO TOTAL MENSUAL: $'.number_format($totalCot, 2, '.', ',').' IVA incluído'),'LR', 2 , 'L', false );
  $pdf->Cell(95,5, utf8_decode(''),'LR', 2 , 'L', false );
  $pdf->Cell(95,5, utf8_decode('EFECTIVO (  )'),'R', 2 , 'L', false );
  $pdf->Cell(95,5, utf8_decode('TARJETA CREDITO (  )'),'R', 2 , 'L', false ); 
  $pdf->Cell(95,5, utf8_decode('TARJETA DÉBITO (  )'),'LR', 2 , 'L', false );

  /*******************   NUMEROS DE CUENTAS BANCARIAS   *******************/
  //BANCOMER
  if($region == 3){
    $pdf->MultiCell(95,5, utf8_decode('DEPÓSITO CUENTA No.: CLAVE: 0123 2000 1111 616953'),'LR', 'L', false );
    $pdf->SetX($posx2+95);
    $pdf->Cell(95,5, utf8_decode('BANCO:Bancomer'),'LR', 2 , 'L', false );
    //SANTANDER
    $pdf->MultiCell(95,5, utf8_decode('DEPÓSITO CUENTA No.: CLAVE: 01 432 0655 0659 26347'),'LR', 'L', false );
    $pdf->SetX($posx2+95);
    $pdf->Cell(95,5, utf8_decode('BANCO:Santander'),'LRB', 2 , 'L', false );
  }else{
  $pdf->MultiCell(95,5, utf8_decode('DEPÓSITO CUENTA No.: CLAVE: 0123 2000 1000 465231 Cajeros: 01 000 46523'),'LR', 'L', false );
  $pdf->SetX($posx2+95);
  $pdf->Cell(95,5, utf8_decode('BANCO:Bancomer'),'LR', 2 , 'L', false );
  //SANTANDER
  $pdf->MultiCell(95,5, utf8_decode('DEPÓSITO CUENTA No.: CLAVE: 01 432 0655 0571 38790 Cajeros: 65-50571387-9'),'LR', 'L', false );
  $pdf->SetX($posx2+95);
  $pdf->Cell(95,5, utf8_decode('BANCO:Santander'),'LRB', 2 , 'L', false );
  }
 
  
  
  /***IMPRIME FECHAS DE INSTALACIÓN***/
  $pdf->SetX($posx2+95);
  if(0>=mysqli_num_rows($resultSit)){
    die("No existen datos de sitio");
  }
  else{
    $sitArr = array();
    while($rowSit = mysqli_fetch_assoc($resultSit)){
      $pdf->Cell(95,5, utf8_decode(''),'LR', 2 , 'L', false );
      $pdf->MultiCell(95,5, utf8_decode('LUGAR DE INSTALACIÓN: '.$rowSit['domicilio'].' col. '.$rowSit['colonia'].' C.P. '.$rowSit['cp'].' '.$rowSit['municipio'].', '.$rowSit['edo']),'LR', 'L', false );
      $sitArr[] = array($rowSit['domicilio'],$rowSit['colonia'],$rowSit['cp'],$rowSit['municipio'],$rowSit['edo']);
      $pdf->SetX($posx2+95);
      $pdf->Cell(95,5, utf8_decode('FECHA DE INSTALACIÓN:_______________'),'LR', 2 , 'L', false ); 
    }
  }  
  
  $posy3 = $pdf->GetY();
  
  $cellHeight2 = $posy4-$posy3;
  $pdf->Cell(95,$cellHeight2, utf8_decode(''),'LRB', 1 , 'L', false );
  $pdf->Ln(5);
  
  /***IMPRIME CONSIDERACIONES CARATULA***/
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(10,6, utf8_decode(""),'', 0 , 'L', false );
  $pdf->Cell(180,6, utf8_decode("CONSIDERACIONES"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 9);
  $pdf->Cell(10,4, utf8_decode("1.-"),'', 0 , 'R', false );
  $pdf->MultiCell(180,4, utf8_decode('"EL PROVEEDOR" deberá efectuar las instalaciones y empezar a prestar el servicio motivo del presente contrato dentro de un plazo que no exceda de 10 días naturales posteriores a su firma.'),'', 'L', false );
  $pdf->Cell(10,4, utf8_decode("2.-"),'', 0 , 'R', false );
  $pdf->MultiCell(180,4, utf8_decode('A partir de que "EL SUSCRIPTOR" cuente con el servicio se empezará a cobrar la mensualidad, dependiendo de la fecha de inicio de servicio, será total o parcialmente la mensualidad correspondiente únicamente al periodo utilizado.'),'', 'L', false );
  $pdf->Cell(10,4, utf8_decode("3.-"),'', 0 , 'R', false );
  $pdf->MultiCell(180,4, utf8_decode('En caso de existir imposibilidad física o técnica para la instalación del servicio, este contrato no tendrá validez alguna, por lo que se dará por terminado, sin perjuicio alguno para "EL SUSCRIPTOR", "EL PROVEEDOR" deberá de realizar
la devolución de todas las cantidades dadas por adelantado dentro de los 10 días siguientes en que se determine dicho supuesto, sin que exista posibilidad de prórroga para "EL PROVEEDOR".'),'', 'L', false );
  $pdf->Cell(10,4, utf8_decode("4.-"),'', 0 , 'R', false );
  $pdf->MultiCell(180,4, utf8_decode('"EL SUSCRIPTOR" conviene en permitir el acceso a su domicilio con previo aviso por parte de "EL PROVEEDOR", a los operativos y empleados de este en donde se encuentren las instalaciones, previa presentación de su credencial o tarjeta de identificación, a los operativos y empleados de "EL PROVEEDOR", para efecto de modificación o reparación de las instalaciones en su caso.'),'', 'L', false );
  $pdf->Cell(10,4, utf8_decode("5.-"),'', 0 , 'R', false );
  $pdf->MultiCell(180,4, utf8_decode('Para verificar la autenticidad de las credenciales de los operativos y empleados, "EL SUSCRIPTOR", deberá llamar al teléfono del "EL PROVEEDOR".'),'', 'L', false );
  $pdf->Cell(10,4, utf8_decode("6.-"),'', 0 , 'R', false );
  $pdf->MultiCell(180,4, utf8_decode('El ancho de banda o GIGABYTE, contratado se repartirá entre los aparatos conectados simultáneamente.'),'', 'L', false );
  $pdf->Cell(10,4, utf8_decode("7.-"),'', 0 , 'R', false );
  $pdf->MultiCell(180,4, utf8_decode('La velocidad contratada estará comprendida entre el modem y el primer punto de acceso a la red de "EL PROVEEDOR".'),'', 'L', false );
  
  $pdf->AddPage();
  /***IMPRIME AUTORIZACION CARGO TARJETA***/
  $pdf->SetFont('Arial', 'B', 10);
  //$pdf->Cell(190,5, utf8_decode(''),'LTR', 1 , 'L', false );
  $pdf->Cell(190,5, utf8_decode("Anexo A: Carátula"),'TLR', 1 , 'R', false );
  $pdf->Cell(190,5, utf8_decode("AUTORIZACION PARA CARGO DE TARJETA DE CREDITO O DEBITO"),'LR', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 9);
  $pdf->MultiCell(190,5, utf8_decode('Por medio de la presente autorizo a "EL PROVEEDOR", para que cargue a mi tarjeta  de crédito o débito, la cantidad por concepto de servicios que mensualmente me presta y en su caso el costo del Equipo adquirido, la vigencia de los cargos será por '.$vigenciaMeses.' meses.'),'LR', 'L', false );//PONER DATO
  $pdf->Cell(190,5, utf8_decode('No. de Tarjeta____________________.'),'LR', 1 , 'L', false );
  $pdf->Cell(190,5, utf8_decode('Institución Bancaria_____________________.'),'LR', 1 , 'L', false );
  $pdf->Cell(30,5, utf8_decode(''),'L', 0 , 'L', false );
  $pdf->Cell(160,5, utf8_decode('Firma de "EL SUSCRIPTOR"_______________________________.'),'R', 1 , 'L', false );
  $pdf->Cell(190,5, utf8_decode(''),'LBR', 1 , 'L', false );
  $pdf->Ln(3);
  
  /***IMPRIME AUTORIZACION CORREO ELECTRONICO***/
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(190,5, utf8_decode(''),'LTR', 1 , 'L', false );
  $pdf->Cell(190,5, utf8_decode("AUTORIZACION PARA USO DE CORREO ELECTRÓNICO"),'LR', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 9);
  $pdf->Cell(190,5, utf8_decode('"EL SUSCRIPTOR" SI (	) NO (	) acepta que la carta de derechos mínimos le sea enviada por correo electrónico. '),'LR', 1 , 'L', false );
  $pdf->Cell(190,5, utf8_decode('En caso de aceptar el correo electrónico es el siguiente: '.$cteEmail),'LR', 1 , 'L', false );//PONER DATO
  $pdf->Cell(30,5, utf8_decode(''),'L', 0 , 'L', false );
  $pdf->Cell(160,5, utf8_decode('Firma de "EL SUSCRIPTOR"_______________________________.'),'R', 1 , 'L', false );
  $pdf->Cell(190,5, utf8_decode(''),'LBR', 1 , 'L', false );
  $pdf->Ln(3);
  
  /***IMPRIME AUTORIZACION CORREO ELECTRONICO PARTE 2***/
  $pdf->Cell(190,5, utf8_decode(''),'LTR', 1 , 'L', false );
  $pdf->Cell(190,5, utf8_decode('"EL SUSCRIPTOR" SI (	) NO (	) acepta que la carta de derechos mínimos le sea enviada por correo electrónico. '),'LR', 1 , 'L', false );$pdf->SetFont('Arial', '', 9);
  $pdf->Cell(30,5, utf8_decode(''),'L', 0 , 'L', false );
  $pdf->Cell(160,5, utf8_decode('Firma de "EL SUSCRIPTOR"_______________________________.'),'R', 1 , 'L', false );
  $pdf->Cell(190,5, utf8_decode(''),'LBR', 1 , 'L', false );
  $pdf->Ln(3);
  
  /***IMPRIME AUTORIZACION USO DE INFORMACIÓN***/
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(190,5, utf8_decode(''),'LTR', 1 , 'L', false );
  $pdf->Cell(190,5, utf8_decode("AUTORIZACIÓN PARA USO DE INFORMACIÓN"),'LR', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 9);
  $pdf->MultiCell(190,5, utf8_decode('"EL SUSCRIPTOR" SI (	) NO (	) acepta que su información sea cedida o transmitida por el proveedor a terceros, con fines mercadotécnicos o publicitarios.'),'LR', 'L', false );
  $pdf->Cell(30,5, utf8_decode(''),'L', 0 , 'L', false );
  $pdf->Cell(160,5, utf8_decode('Firma de "EL SUSCRIPTOR"_______________________________.'),'R', 1 , 'L', false );
  $pdf->Cell(190,5, utf8_decode(''),'LBR', 1 , 'L', false );
  
  /***IMPRIME LOGO y DECLARACIONES***/
  $pdf->AddPage();
  //$pdf->Image($secLogoPath, 60, 148, 80, 0);
  if(file_exists($secLogoPath))
    $pdf->Image($secLogoPath, 70, 10, 60, 0);
  $pdf->Ln(28);
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('CONTRATO DE PRESTACIÓN DE SERVICIO DE TELEFONÍA FIJA E INTERNET, QUE CELEBRAN  POR UNA PARTE '.$coeRazonSocial.', A QUIEN EN LO SUCESIVO SE LE DENOMINARÁ "EL PROVEEDOR" Y POR OTRA PARTE EL	QUIEN EN LO SUCESIVO SE LE DENOMINARÁ "EL SUSCRIPTOR", BAJO LAS SIGUIENTES:'),'', 'J', false );
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(190,5, utf8_decode("DECLARACIONES"),'', 1 , 'C', false );
  $pdf->Cell(190,5, utf8_decode('I.- Declara "EL PROVEEDOR"'),'', 1 , 'L', false );
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(10,4, utf8_decode("A) "),'', 0 , 'R', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(180,4, utf8_decode('Ser una persona moral con capacidad para celebrar el presente contrato, de acuerdo al Acta Constitutiva No. '.$coeNumPoder.' de fecha '.$coeFechaPoder.', presentada ante el '.$coeNotario.'.'),'', 'L', false );
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(10,4, utf8_decode("B) "),'', 0 , 'R', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(180,4, utf8_decode('Su representante legal cuenta con poderes específicos de administración para la celebración de este Contrato.'),'', 'L', false );
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(10,4, utf8_decode("C) "),'', 0 , 'R', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(180,4, utf8_decode('Objeto Social: Proporcionar Servicios para la prestación de servicios de telecomunicaciones.'),'', 'L', false );
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(10,4, utf8_decode("D) "),'', 0 , 'R', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(180,4, utf8_decode('Con Registro Federal de Contribuyente CC0131011CW8.'),'', 'L', false ); //dato nuevo
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(10,4, utf8_decode("E) "),'', 0 , 'R', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(180,4, utf8_decode('Con dirección fiscal en Herrera y Cairo #1959 Col. Ladrón de Guevara, Guadalajara, Jalisco CP 44600.'),'', 'L', false ); //dato nuevo
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(10,4, utf8_decode("F) "),'', 0 , 'R', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(180,4, utf8_decode('Que es Concesionario de Telecomunicaciones para prestar el servicio de TELEFONÍA FIJA E INTERNET en el territorio de la República Mexicana. Acreditando lo anterior con el título de concesión fecha 23 de noviembre de 2016, otorgado por Instituto Federal de Telecomunicaciones, mismo que podrá ser constatado en la página de internet www.ift.org.mx'),'', 'L', false ); //dato nuevo
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(10,4, utf8_decode("G) "),'', 0 , 'R', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(180,4, utf8_decode('Señalo como domicilio legal el ubicado en Calle Montes Urales Número 425 Piso 4, Col. Lomas de Chapultepec, C.P. 11000, en la Ciudad de México.'),'', 'L', false );


  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(190,5, utf8_decode('II.- Declara "EL SUSCRIPTOR"'),'', 1 , 'L', false );
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(10,4, utf8_decode("A) "),'', 0 , 'R', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(180,4, utf8_decode('Que es una persona física o moral cuyo nombre corresponde al asentado en la carátula.'),'', 'L', false );
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(10,4, utf8_decode("B) "),'', 0 , 'R', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(180,4, utf8_decode('Que tiene pleno goce de sus derechos y capacidad legal para contratar y obligarse en términos del presente contrato.'),'', 'L', false );
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(10,4, utf8_decode("C) "),'', 0 , 'R', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(180,4, utf8_decode('Para efectos del presente contrato señala como su domicilio el indicado en la carátula.'),'', 'L', false );
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(10,4, utf8_decode("D) "),'', 0 , 'R', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(180,4, utf8_decode('Que la instalación del servicio será en el domicilio señalado en la carátula'),'', 'L', false );
  
  /***IMPRIME CLAUSULAS***/
  //$pdf->AddPage();
  $pdf->Ln(5);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(190,5, utf8_decode("CLÁUSULAS"),'', 1 , 'C', false );
  $pdf->Ln(3);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(180,6, utf8_decode("PRIMERA. OBJETO"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('"EL PROVEEDOR" se obliga a proporcionar el servicio de telefonía fija e internet, de acuerdo a los estándares mínimos de calidad establecidos por el Instituto Federal de Telecomunicaciones, de forma continua, uniforme, regular y eficiente, cumpliendo con las normas y metas de calidad aplicables, mediante el pago de la cuota de instalación y de las mensualidades por parte de "EL SUSCRIPTOR"
  
  "EL PROVEEDOR" será el único responsable frente a "EL SUSCRIPTOR" por la prestación del servicio de telefonía fija e internet'),'', 'J', false );
  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(180,6, utf8_decode("SEGUNDA. TARIFAS"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('"EL PROVEEDOR", está obligado a tener en su portal de internet http://coeficiente.mx/  a disposición y a la vista del público con caracteres claramente legibles, las tarifas por el servicio vigentes aprobadas e inscritas en el Registro de Tarifas del Instituto Federal de Telecomunicaciones, las cuales pueden ser consultadas en www.ift.org.mx

Las condiciones comerciales establecidas en este contrato, los planes, paquetes, las áreas o regiones geográficas con cobertura que tiene autorizado "EL PROVEEDOR" pueden ser consultados en la página de internet http://coeficiente.mx/'),'', 'J', false );

  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(180,6, utf8_decode("TERCERA. EQUIPO EN COMODATO"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('Los aparatos y accesorios colocados para la prestación del servicio por "EL PROVEEDOR" en el domicilio de "EL SUSCRIPTOR" son exclusiva propiedad de "EL PROVEEDOR", mismos que son otorgados en comodato. "EL SUSCRIPTOR" se compromete a la guarda, custodia y conservación de lo(s) equipo(s), durante todo el tiempo que se encuentre en su poder y hasta el momento en que el Equipo sea recibido de conformidad por "EL PROVEEDOR" a la terminación del contrato.
  
1. "EL SUSCRIPTOR" deberá cubrir el costo tanto de las reparaciones del Equipo cuando le sea imputable la descompostura, mal uso, mal funcionamiento o fallas del mismo.
2. "EL SUSCRIPTOR" se obliga a: no vender el Equipo, ni cambiar de domicilio la instalación original, NO permitir que un tercero no autorizado haga uso del Equipo, NO ceder el Equipo sin el consentimiento de "EL PROVEEDOR" y utilizar el Equipo en la forma autorizada por "EL PROVEEDOR".
3. "EL SUSCRIPTOR" deberá contar con la instalación eléctrica que cumpla con las especificaciones de la Comisión Federal de Electricidad; "EL SUSCRIPTOR" reconoce que "EL PROVEEDOR" no se hará responsable de los daños o pérdidas en los equipos propiedad de "EL SUSCRIPTOR" por variaciones de voltaje o descargas eléctricas., "EL SUSCRIPTOR" y/o un tercero que haya manipulado las instalaciones eléctricas.	"EL PROVEEDOR" se hará responsable de las fallas del servicio provocadas por variaciones de voltaje o descargas eléctricas en los equipos terminales otorgados en comodato.
4. En el caso de que el Equipo sea robado u objeto de siniestro, "EL SUSCRIPTOR" deberá dar aviso inmediato al Centro de Atención Telefónica, en un plazo que no excederá de veinticuatro horas posteriores al evento para la reposición del Equipo
5. "EL SUSCRIPTOR" tendrá un plazo de 30 días hábiles posteriores al mismo para presentar copia certificada de la constancia correspondiente levantada ante una Autoridad Competente, para acreditar el objeto de robo o siniestro, para que no tenga costo la reposición del equipo.

En caso de que "EL SUSCRIPTOR" opte por usar Equipo de su propiedad para la prestación de servicio de Telefonía, este deberá ser un equipo homologado de conformidad con las disposiciones del IFT.'),'', 'J', false );

  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(180,6, utf8_decode("CUARTA. VIGENCIA"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('La vigencia del presente contrato será indefinida. "EL SUSCRIPTOR", podrá darlo por terminado en cualquier momento, sin penalidad alguna, mediante simple aviso en el domicilio de "EL PROVEEDOR" y/o vía correo electrónico enviado a "EL PROVEEDOR" el cual dará por terminado el contrato de forma inmediata.

La cancelación en el servicio por parte de "EL SUSCRIPTOR" no lo exime del pago de las cantidades adeudadas a "EL PROVEEDOR" y permitir el retiro de las instalaciones realizadas en el domicilio de "EL SUSCRIPTOR".

"EL PROVEEDOR" podrá dar por terminado el presente contrato, en caso de que quede en imposibilidad material y jurídica de continuar prestando el servicio, previo aviso a "EL SUSCRIPTOR" con quince días naturales de anticipación.'),'', 'J', false );

  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(180,6, utf8_decode("QUINTA. PAGOS"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('El pago de la mensualidad señalada en la carátula, "EL SUSCRIPTOR" deberá hacerlo en los 10 primeros días de cada mes, en el lugar o forma establecida en la misma. Entregando a "EL   SUSCRIPTOR" el comprobante fiscal correspondiente a dicho pago.

"EL PROVEEDOR", notificará por cualquier medio a "EL SUSCRIPTOR", incluido el electrónico, de cualquier cambio en las condiciones originalmente contratadas.

"EL PROVEEDOR", cuando realice actualizaciones a las condiciones originalmente contratados, tendrá la obligación de notificar a "EL SUSCRIPTOR" con al menos 15 días naturales de anticipación a que se lleve a cabo dicha modificación, "EL SUSCRIPTOR" podrá solicitar el cumplimiento forzoso y en caso de que no lo cumpla rescindir el mismo, dando por terminado el contrato sin penalidad alguna dentro de los 30 días siguientes a que entren en vigor dichas modificaciones, independientemente de las acciones legales que correspondan para su cumplimiento.'),'', 'J', false );

  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(180,6, utf8_decode("SEXTA. FACTURACIÓN"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('"EL PROVEEDOR" se encuentra obligado a entregar en el domicilio de "EL SUSCRIPTOR" un estado de cuenta y/o factura, el cual deberá contener el desglose del adeudo total de los conceptos de cada uno de los servicios utilizados, por lo menos 10 días naturales antes de la fecha de pago de los servicios de telecomunicaciones contratados. "EL PROVEEDOR" podrá pactar expresamente con "EL SUSCRIPTOR" que el estado de cuenta y/o factura sea enviada al correo electrónico señalado en la carátula o consultado a través de la página de Internet http://coeficiente.mx/.'),'', 'J', false );
  
  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(180,6, utf8_decode("SÉPTIMA. SUSPENSIÓN"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('Cuando "EL SUSCRIPTOR" no paga dentro de ese mes "EL PROVEEDOR" suspenderá el servicio sin ninguna responsabilidad dentro de los 5 primeros días del mes siguiente al incumplimiento de pago, corriendo por cuenta de "EL SUSCRIPTOR" el pago por mensualidad y reconexión vigente, tarifas aprobadas e inscritas en el Registro de Tarifas del Instituto Federal de Telecomunicaciones, las cuales pueden ser consultadas en www.ift.orq.mx

"EL PROVEEDOR" deberá reconectar el servicio dentro de las 24 horas a la fecha en que hubiese liquidado los adeudos y la cuota por reconexión.'),'', 'J', false );

  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(180,6, utf8_decode("OCTAVA.INICIO Y CONTRAPRESTACIÓN"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('"EL PROVEEDOR" deberá efectuar las instalaciones y empezar a prestar el servicio objeto del presente contrato dentro de un plazo que no exceda de 1O días hábiles a partir de la firma del contrato.

Una vez instalado el equipo terminal y que "EL SUSCRIPTOR" cuente con el servicio "EL PROVEEDOR" podrá comenzar a cobrarlo, dependiendo de la fecha de inicio de servicio, será total o parcialmente el cobro de la mensualidad, correspondiente únicamente al periodo utilizado.

En caso de existir imposibilidad física o técnica para la instalación del servicio, este contrato no tendrá validez alguna, por lo que se dará por terminado, sin perjuicio alguno para "EL SUSCRIPTOR" y en perjuicio de "EL PROVEEDOR", el cual deberá de realizar la devolución de todas las cantidades dadas por adelantado dentro de los 10 días siguientes en que se determine dicho supuesto, sin que exista posibilidad de prórroga para "EL PROVEEDOR".

"EL PROVEEDOR" reembolsará a "EL SUSCRIPTOR", las cantidades que éste haya pagado por concepto de anticipo y que no sean compensables con otros adeudos.

1. "EL SUSCRIPTOR" contará con los equipos personales mínimos necesarios, con las características necesarias para hacer uso del Servicio de Internet, "EL PROVEEDOR" informará previo a la contratación cuáles son los requerimientos mínimos de los aparatos personales.
2. "EL SUSCRIPTOR" tomará todas las medidas necesarias para proteger la información de su propiedad y/o software.
3. "EL PROVEEDOR" no será responsable de daños que se causen por piratas informáticos y/o virus transmitidos a través de Internet.
4. "EL PROVEEDOR" no es responsable de la configuración de dispositivos tales como "ruteadores", "conmutador", "hubs/concentrador", "proxy", que resulten necesarios para el uso concurrente del o de los Equipos Personales. "EL PROVEEDOR" no realizará la instalación de la red de área local (LAN) en el domicilio del "EL SUSCRIPTOR", por lo que no será responsable del buen funcionamiento de la misma.'),'', 'J', false );

  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(180,6, utf8_decode("NOVENA NEUTRALIDAD DE LA RED"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('Los Servicios provistos por "EL PROVEEDOR" a "EL SUSCRIPTOR", cumplen con los principios de neutralidad de las redes contenidos en la Ley Federal de Telecomunicaciones y Radiodifusión.

"EL SUSCRIPTOR" podrán acceder a cualquier contenido, aplicación o servicio ofrecido por "EL PROVEEDOR", dentro del marco legal aplicable, sin limitar, degradar, restringir o discriminar el acceso a los mismos.

"EL PROVEEDOR" se abstendrá de obstruir, interferir, inspeccionar, filtrar o discriminar contenidos, aplicaciones o servicio.

"EL PROVEEDOR" preservará la privacidad de "EL SUSCRIPTOR" y la seguridad de la red.

"EL PROVEEDOR" publicará en su página de Internet la información relativa a las características del servicio ofrecido, incluyendo las políticas de gestión de tráfico y administración de red autorizada por el Instituto, velocidad, calidad, la naturaleza y garantía del servicio.

"EL PROVEEDOR" podrá tomar las medidas o acciones necesarias para la gestión de tráfico y administración de red conforme a las políticas autorizadas por el Instituto, a fin de garantizar la calidad o la velocidad de servicio contratada por "EL SUSCRIPTOR", siempre que ello no constituya una práctica contraria a la sana competencia y libre concurrencia'),'', 'J', false );

  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(180,6, utf8_decode("DÉCIMA PORTABILIDAD"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('Cuando "EL SUSCRIPTOR" decida portar su número hacia otra compañía, a partir de la fecha en que se ejecute la portabilidad numérica y sin la exigencia de requisitos adicionales, se dará por terminado el contrato de manera automática la relación contractual con el proveedor, únicamente de aquellos servicios cuya prestación requiera de los números telefónicos a ser portados, por lo que los servicios no requieran de los números telefónicos portados podrán continuar activos.

Si el presente contrato es celebrado derivado de la portabilidad numérica de "EL SUSCRIPTOR"; éste tiene derecho a exigir el pago de la penas convencionales establecidas en el contrato si la portación no se ejecuta en los plazos establecidos, o en la fecha comprometida por "EL PROVEEDOR" al que contrate el servicio cuando de manera expresa se haya acordado con "EL SUSCRIPTOR".

"EL SUSCRIPTOR" tiene derecho a cancelar, sin el pago de penas convencionales, los servicios de telecomunicaciones contratados a "EL PROVEEDOR" cuando se haya solicitado la portabilidad del número y ésta no se ejecute dentro de los plazos establecidos, por causa imputables "EL SUSCRIPTOR".'),'', 'J', false );

  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(180,6, utf8_decode("DÉCIMA PRIMERA. BONIFICACIÓN"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('"EL SUSCRIPTOR" deberá comunicar en forma inmediata a "EL PROVEEDOR" las fallas o interrupciones del
Servicio(s).

Siempre y cuando por causas imputables a "EL PROVEEDOR" no se preste el servicio de telecomunicaciones en la forma y términos convenidos, éste dejará de cobrar a "EL SUSCRIPTOR" la parte proporcional del precio del servicio que se dejó de prestar, y deberá bonificar por lo menos el 20% del monto del periodo de afectación.

Cuando la suspensión sea por casos fortuitos o de fuerza mayor, si la misma dura 24 horas siguientes al reporte "EL PROVEEDOR", hará la compensación por la parte proporcional del periodo en que se dejó de prestar el servicio contratado por "EL SUSCRIPTOR", dicha compensación se verá reflejada en el siguiente recibo y/o factura; además deberá bonificar por lo menos el 20% del monto del periodo de afectación.

A partir de que "EL PROVEEDOR" reciba la llamada respecto a fallas y/o interrupciones en el servicio de telefonía fija e internet se procederá a verificar el tipo de falla y en base a este se dictaminará el tiempo para la reparación, la cual no puede exceder las 72 horas siguientes al reporte recibido.

"EL PROVEEDOR" dará aviso a "EL SUSCRIPTOR", por cualquier medio incluido el electrónico y al Instituto Federal de Telecomunicaciones por escrito, de cualquier circunstancia previsible que repercuta en forma generalizada y significativa en la prestación del servicio, con una antelación mínima de 24 horas.. Dicha circunstancia no podrá afectar el servicio por más de 24 horas en cuyo caso "EL PROVEEDOR" dejará de cobrar al "EL SUSCRIPTOR" la parte proporcional del precio del servicio que se dejó de prestar, y deberá bonificar por lo menos el 20% del monto del periodo de afectación, lo mismo ocurrirá si "EL PROVEEDOR" no da aviso al Instituto Federal de Telecomunicaciones conforme a la presente cláusula.

Tratándose de cargos o cobros indebidos realizados por "EL PROVEEDOR" los mismos deberán ser reembolsados en un plazo no mayor a 2 ciclos de facturación además de bonificar al menos el 20% del monto del cobro realizado indebidamente.'),'', 'J', false );

  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(180,6, utf8_decode("DÉCIMA SEGUNDA. RECISIÓN POR EL PROVEEDOR"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('El presente Contrato podrá rescindirse por "EL PROVEEDOR" en forma inmediata y sin necesidad de que medie resolución o declaración judicial alguna en los siguientes casos:

a) Por utilizar el Equipo o los Servicios en contravención de las disposiciones legales, reglamentarías, planes fundamentales de telecomunicaciones, así como cualquier disposición vigente en materia de telecomunicaciones que sea aplicable a la prestación de Servicios;
b) Si "EL SUSCRIPTOR", comercializa o revende los Servicios contratados, así como por la obtención de cualquier lucro por la venta o reventa del Servicio a cualquier tercero, sin la debida autorización por parte de "EL PROVEEDOR" o de la Secretaría o el IFT;
c) Si "EL SUSCRIPTOR", cede los derechos y/u obligaciones derivados del presente contrato o de la carátula correspondiente, sin la autorización previa y por escrito de "EL PROVEEDOR";
d) Si "EL SUSCRIPTOR", no cubre todas las cantidades adeudadas a "EL PROVEEDOR" dentro de los 30 (treinta) días naturales siguientes a la fecha de suspensión de los servicios.'),'', 'J', false );

  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(180,6, utf8_decode("DÉCIMA TERCERA. RECISIÓN POR EL SUSCRIPTOR"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('El presente Contrato podrá rescindirse sin responsabilidad por el "EL SUSCRIPTOR", en los siguientes casos:

a) Si "EL PROVEEDOR"; hace caso omiso a cualquier solicitud presentada por "EL SUSCRIPTOR", para la prestación del servicio principal o de los servicios adicionales contratados, ya sea por queja o ajuste. La solicitud tiene que ser debidamente formulada;
b) Si "EL PROVEEDOR"; incumple cualquier disposición del presente contrato.
c) Si "EL PROVEEDOR"; no cumple con los estándares de calidad convenidos de conformidad con lo establecido
en la CLÁUSULA PRIMERA y en las especificaciones técnicas del servicio contratado en la solicitud de servicio o carátula anexo de este contrato.
d) Si "EL PROVEEDOR" modifica unilateralmente las condiciones originalmente contratadas.
e) Por terminación o revocación del Título de Concesión.
f) Cuando existe cesión de derechos del Título de Concesión a otro Proveedor.'),'', 'J', false );

  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(180,6, utf8_decode("DÉCIMA CUARTA. CAMBIO DE DOMICILIO"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('Las partes convienen en notificarse cualquier cambio en sus domicilios en el entendido de que si el nuevo domicilio de "EL SUSCRIPTOR", queda fuera del área de cobertura de prestación del servicio a cargo de "EL PROVEEDOR", será causa automática de terminación del contrato sin responsabilidad para ambas partes, "EL SUSCRIPTOR" permitirá el retiro por parte de "EL PROVEEDOR", del equipo y accesorios que con motivo del presente Contrato hubiesen sido proporcionados a "EL SUSCRIPTOR".'),'', 'J', false );
  
  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(180,6, utf8_decode("DÉCIMA QUINTA. TERMINACIÓN ANTICIPADA"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('En caso de que "EL PROVEEDOR" no preste los servicios  pactados en el presente contrato en la forma y términos convenidos, contratados ofrecidos o publicitados, así como los estándares de calidad y conforme a la Ley Federal de Telecomunicaciones y Radiodifusión, "EL SUSCRIPTOR" tendrá el derecho de dar por terminado el contrato en términos de la cláusula DÉCIMA SEGUNDA.'),'', 'J', false );
  
  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(180,6, utf8_decode("DÉCIMA SEXTA. CAMBIOS EN EL CONTRATO DE ADHESIÓN"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('Cuando "EL PROVEEDOR" modifique las condiciones originalmente contratadas, "EL SUSCRIPTOR" podrá solicitar el cumplimiento forzoso del contrato, y en caso de que no las cumpla a rescindir el mismo. De igual manera cuando se haya suscrito un contrato de adhesión, solo se podrá cambiar a otro por acuerdo de las partes, y el consentimiento podrá otorgarse por medios electrónicos.'),'', 'J', false );
  
  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(180,6, utf8_decode("DÉCIMA SÉPTIMA. SERVICIOS ADICIONALES"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('Los servicios adicionales se prestarán previa solicitud de "EL SUSCRIPTOR", por evento, tiempo, capacidad o cualquier otra modalidad con que cuente "EL PROVEEDOR".

"EL PROVEEDOR" no podrá obligar a "EL SUSCRIPTOR" a contratar algún servicio adicional como requisitos para la contratación o continuar con los servicios contratados; así mismo "EL SUSCRIPTOR" en caso de tener algún servicio adicional, podrá cancelarlo en cualquier momento sin responsabilidad.para "EL SUSCRIPTOR" y sin que este afecte el servicio principal contratado. "EL PROVEEDOR" deberá cancelar los servicios dentro de los cinco días naturales siguientes a dicha manifestación.

"EL PROVEEDOR" podrá ofrecer los servicios adicionales o productos de manera empaquetada o por separado cuando
"EL SUSCRIPTOR" así lo solicite.'),'', 'J', false );

  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(180,6, utf8_decode("DÉCIMA OCTAVA.PROTECCIÓN DE DATOS PERSONALES"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('"EL PROVEEDOR", está obligado a proteger y tratar conforme a la Ley Federal de Protección de Datos Personales en
Posesión de Particulares los datos personales que le sean proporcionados por el "EL SUSCRIPTOR”.

La aceptación del "EL SUSCRIPTOR" para que "EL PROVEEDOR" emplee la información con fines mercadotécnicos o de publicidad, se encuentra sujeta a lo manifestado por el "EL SUSCRIPTOR" en la carátula, previa firma de autorización.'),'', 'J', false );

  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(180,6, utf8_decode("DÉCIMA NOVENA. USUARIOS CON DISCAPACIDAD"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('En cuanto a la contratación para usuarios con discapacidad, "EL PROVEEDOR" cuenta con diversos mecanismos para atender las necesidades de los usuarios con discapacidad, para que "EL SUSCRIPTOR" conozca las condiciones comerciales establecidas en el presente contrato, así como los servicios adicionales y los paquetes que ofrezca "EL PROVEEDOR".

"EL PROVEEDOR" deberá prestar el servicio a todo aquel que lo solicite en condiciones equitativas, sin establecer privilegios o distinciones en forma discriminatoria.'),'', 'J', false );

  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(180,6, utf8_decode("VIGÉSIMA. CARTA DE DERECHOS MÍNIMOS"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('"EL PROVEEDOR" deberá entregar a "EL SUSCRIPTOR" la carta de derechos mínimos de los usuarios de los servicios públicos de telecomunicaciones, a la firma del contrato y mantenerla de manera permanente en su página web.'),'', 'J', false );
  
  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(180,6, utf8_decode("VIGÉSIMA PRIMERA DERECHOS DEL SUSCRIPTOR"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('"EL SUSCRIPTOR" no recibirá llamadas del "PROVEEDOR", sobre la promoción de servicios o paquetes a menos que expresamente manifieste su consentimiento a través de medios electrónicos.'),'', 'J', false );
  
  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(180,6, utf8_decode("VIGÉSIMA SEGUNDA. ATENCIÓN A FALLAS"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('"EL SUSCRIPTOR" podrá presentar sus quejas por fallas y/o deficiencias en el servicio y/o equipos; así como consultas, sugerencias y reclamaciones a "EL PROVEEDOR" de manera gratuita a través de su Sistema de Atención a Solicitudes al teléfono '.$coeTel.', el cual se encuentra disponible las 24 horas del día, todos los días del año y al correo electrónico legal@coeficiente.mx información que se encuentra disponible en la página http://coeficiente.mx/'),'', 'J', false );
  
  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(180,6, utf8_decode("VIGÉSIMA TERCERA. SOLUCIÓN DE CONTROVERSIAS"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->MultiCell(190,4, utf8_decode('Para todo lo relativo a la interpretación, cumplimiento y ejecución del presente contrato, las partes se someten en la vía administrativa ante la Procuraduría Federal del Consumidor.'),'', 'J', false );
  
  $pdf->Ln(4);
  $pdf->SetFont('Arial', 'B', 10);
  $posx = $pdf->GetX();
  $posy = $pdf->GetY();
  $pdf->Cell(150,6, utf8_decode("VIGÉSIMA CUARTA"),'', 1 , 'L', false );
  $pdf->SetFont('Arial', '', 10);
  $pdf->SetXY($posx+150,$posy);
  $pdf->Cell(20,6, $pdf->Image('../../img/coe_cont_qrcode.png', $pdf->GetX(), $pdf->GetY(), 35, 0),'', 1 , 'L', false );
  $pdf->MultiCell(150,4, utf8_decode('Este modelo de Contrato de Adhesión utilizado, ha sido registrado por la Procuraduría Federal del Consumidor, bajo el número 210 de fecha 29 del mes de septiembre de 2017.'),'', 'J', false );
  $pdf->Ln(5);
  $pdf->MultiCell(150,4, utf8_decode('Así	mismo el suscriptor podrá consultar dicho registro en https://burocomercial.profeco.gob.mx/ca_spt/Coeficiente%20Comunicaciones,%20S.A.
%20de%20C.V.!!Coeficiente%20Comunicaciones%20210-17.pdf y en el siguiente código'),'', 'L', false );
 
  $pdf->Ln(10);
  
  $pdf->MultiCell(190,4, utf8_decode('Cualquier  variación en la carátula y de las cláusulas del presente contrato, con el que firme   "EL SUSCRIPTOR", se tendrán por no puestas y las mismas serán nulas. Este contrato se firmó por duplicado en la Ciudad de '.$coeCdFirma.', '.$coeEdoFirma.', México a '.$fecAltaDia.' de '.$fecAltaMes.' de '.$fecAltaAnio.'.'),'', 'J', false );
  $pdf->Ln(10);
  $posY = $pdf->GetY();
  $posX = $pdf->GetX();
  $pdf->Cell(90,5, utf8_decode('EL PROVEEDOR'),0,0,'C', false );
  $pdf->Cell(90,5, utf8_decode('EL SUSCRIPTOR'),0,1,'C', false ); 
  $pdf->Cell(90,20, utf8_decode('________________________________________'),0,0,'C', false );
  $pdf->Cell(90,20, utf8_decode('________________________________________'),0,1,'C', false );
  
  $pdf->SetX($posX+90);
  
  $pdf->Ln(8);
  $pdf->Cell(190,20, utf8_decode('Sin más por el momento.'),0,1,'L', false );
  $pdf->Ln(8);
  $pdf->Cell(190,20, utf8_decode('ATENTAMENTE'),0,1,'C', false );
  $pdf->Cell(190,5, utf8_decode($coeRepLegal),0,1,'C', false );
  $pdf->Cell(190,5, utf8_decode("Representante legal"),0,1,'C', false );
  
  
  //$enlArr[] = array($rowProd['servicio'],$desc,$carIns,$carCan,$rowProd['orden_trabajo_id'],$rowProd['sitio_asignado'],$rowProd['relacion_id']);
  
  //$adecArr[] = array($rowProd['descripcion'],$precio,$rowProd['cantidad'],$rowProd['servicio'],$rowProd['orden_trabajo_id']);
  
  foreach($enlArr AS $key => $value){
    /*$querySit2 = "SELECT sit.*, edo.estado AS edo, ot.orden_id ".
                 "FROM ".$cfgTableNameMod.".sitios AS sit, ".
                         $cfgTableNameCat.".cat_estados AS edo, ".
                         $cfgTableNameMod.".ordenes_trabajo AS ot ".
                 " WHERE sit.sitio_id = ".$value[5].
                 " AND ot.rel_cotizacion_producto=".$value[6].
                 " AND sit.estado = edo.estado_id";
                 
    $resultSit2 = mysqli_query($db_modulos,$querySit2);
    
    if(!$resultSit2){
      die("No se pudo obtener los datos de sitio para el anexo de este contrato ");
    }
    else{
      $costInst = 0;
      $rowSit2 = mysqli_fetch_assoc($resultSit2);
      
      foreach($adecArr AS $adecKey => $adecValue){
        if($rowSit2 ['orden_id'] == $adecValue[4]){
          $lugarInst .= $rowSit2['domicilio'].' col. '.$rowSit2['colonia'].' C.P. '.$rowSit2['cp'].' '.$rowSit2['municipio'].', '.$rowSit2['edo'];
          //$costInst += $value[2]+$adecValue[1]*$adecValue[2];
        }
      }
    }*/
    $costIns += $value[3];
  }
  
  
      /*if(0<sizeof($adecArr)){
      $adecsubtotal = 0;
      $adecSubAplPila = array();
      foreach($adecArr AS $key => $value){
        $queryAdecDet = "SELECT cvs.sub_".$vigenciaMeses."m, cvs.cargo_adecuacion_".$vigenciaMeses."m, cvs.servicio_id ".
                        "FROM ".$cfgTableNameMod.".rel_cotizacion_producto AS rcp, ". 
                                $cfgTableNameMod.".ordenes_trabajo AS ot, ".
                                $cfgTableNameCat.".cat_venta_servicios AS cvs ".
                        "WHERE ot.orden_id = ".$value[4]." ".
                        "AND ot.rel_cotizacion_producto = rcp.relacion_id ".
                        "AND rcp.producto_id = cvs.servicio_id ";
                        
        $resultAdecDet = mysqli_query($db_modulos,$queryAdecDet);
        if($resultAdecDet){
          $rowAdecDet = mysqli_fetch_assoc($resultAdecDet);
          
          if(!in_array($rowAdecDet['servicio_id'],$adecSubAplPila)){
            $carAdec = $rowAdecDet['cargo_adecuacion_'.$vigenciaMeses.'m'];
            $subAdec = $rowAdecDet['sub_'.$vigenciaMeses.'m'];
          }
          else{
            $carAdec = 0;
            $subAdec = 0;
          }
          
          if(0<$value[1]){
            $precioSub = ($value[1]*$value[2])-$subAdec+$carAdec;
          }
          $adecSubAplPila[] = $rowAdecDet['servicio_id'];
        }
        $adecsubtotal = $adecsubtotal+$precioSub;
      }
        
      $adecimpuesto = $adecsubtotal*0.16;
      $adectotal = $adecsubtotal+$adecimpuesto;
    }*/
    
      
      /***IMPRIME ANEXO CON SERVICIOS***/
      $pdf->AddPage();
      $pdf->SetFont('Arial', 'B', 10);
      $pdf->SetTextColor(0,0,0);
      $pdf->Cell(120,30, utf8_decode(''),'LTR', 0 , 'L', false );
      $pdf->Cell(70,20, utf8_decode('Contrato # '.$regiNumCon),'TR', 2 , 'L', false );
      $pdf->Cell(70,10, utf8_decode('Anexo B: Adecuaciones'),'RB', 1 , 'C', false );
      if(file_exists($secLogoPath))
        $pdf->Image($secLogoPath, 20, 12, 70, 0);
      $pdf->SetFont('Arial', '', 12);
      $pdf->Cell(190,5, utf8_decode(""),'LR', 1 , 'L', false );
      if(""!=$rowCot['razon_social'])
        $pdf->MultiCell(190,8,utf8_decode("SUSCRIPTOR O RAZÓN SOCIAL: ".strtoupper($rowCot['razon_social'])),'LR', 'L', false );
      else
        $pdf->MultiCell(190,8,utf8_decode("SUSCRIPTOR O RAZÓN SOCIAL: ".strtoupper($rowCot['nombre_comercial'])),'LR', 'L', false );
      
      $pdf->MultiCell(190,8,utf8_decode("TIPO DE INSTALACIÓN: "),'LR', 'J', false );
      foreach($enlArr AS $key => $value){  
        $pdf->MultiCell(190,8,utf8_decode("-".strtoupper($value[1])),'LR', 'J', false );
        $costIns += $value[3];
      }
      $pdf->Cell(190,8, utf8_decode("COSTO POR INSTALACIÓN: $".number_format($costIns, 2, '.', ',')." +IVA"),'LR', 1 , 'L', false );
      $pdf->Cell(190,8, utf8_decode("PAGO POR: "),'LR', 1 , 'L', false );
      $pdf->Cell(190,8, utf8_decode("ANTICIPO DE INSTALACIÓN: $0.00 +IVA"),'LR', 1 , 'L', false );
      $pdf->Cell(190,8, utf8_decode("ENTREGA DE INSTALACIÓN: $".number_format($costIns, 2, '.', ',')." +IVA"),'LR', 1 , 'L', false );
      $pdf->Cell(190,8, utf8_decode("DIFERIDO EN UN PLAZO DE: ".$vigenciaMeses." MESES $".number_format($costIns/$vigenciaMeses, 2, '.', ',')." (+IVA) POR CADA MENSUALIDAD."),'LR', 1 , 'L', false );
      foreach($sitArr AS $sitKey => $sitValue){
        $pdf->MultiCell(190,8,utf8_decode("LUGAR DE INSTALACIÓN: ".strtoupper($sitValue[0].' col. '.$sitValue[1].' C.P. '.$sitValue[3].' '.$sitValue[4].', '.$sitValue[5])),'LR', 'L', false );
      }
      $pdf->Cell(190,8, utf8_decode("FECHA DE INSTALACIÓN:__________________"),'LR', 1 , 'L', false );
      $pdf->Cell(190,8, utf8_decode(""),'LR', 1 , 'L', false );
      $pdf->MultiCell(190,6, utf8_decode('POR MEDIO DE LA PRESENTE AUTORIZO A '.$coeRazonSocial.', LA INSTALACIÓN PARA LA PRESTACIÓN DEL SERVICIO ASÍ COMO LA CANTIDAD POR CONCEPTO DE SERVICIOS QUE MENSUALMENTE ME PRESTA Y EN SU CASO EL COSTO TOTAL DE LA INSTALACIÓN ADQUIRIDA.'),'LR', 'J', false );
      $pdf->Cell(190,10, utf8_decode(""),'LR', 1 , 'L', false );
      $pdf->SetFont('Arial', 'B', 12);
      $pdf->Cell(190,10, utf8_decode("FIRMA DE \"EL SUSCRIPTOR\":______________________________________________"),'LR', 1 , 'C', false );
      $pdf->Cell(190,10, utf8_decode(""),'LRB', 1 , 'L', false );
      $pdf->SetFont('Arial', '', 12);
      $pdf->MultiCell(190,6, utf8_decode("MONTO A PAGAR ANTES DE ".date('d/m/Y', strtotime("+".$vigenciaMeses." months", strtotime($contInicioVig[0]))).": $".number_format($costIns, 2, '.', ',')." (+IVA). ESTE MONTO SE PODRÁ CONDONAR EN EL CASO DE QUE EL CLIENTE CONSERVE LOS SERVICIOS DE COEFICIENTE DURANTE TODO DICHO PERIODO AL DÍA."),'', 'J', false );
    //}   
  //}
  
  if(1!=$mailflag) //Salida al navegador directa
    $pdf->Output(); 
  else{ //Envia por mail la cotización

    $filename = date("YmdHis").$cotId."_".str_replace(" ","",$rowCot['nombre_comercial']);
    $filepath = realpath('../../docs/pdf/contratos/');
    $pdfdoc = $pdf->Output($filepath."/".$filename.".pdf", "F"); //crea un archivo
    
    if(file_exists($cfgServerLocationAbs.'img/signatures/'.$usuarioId.'.png'))
      $sigPath = $cfgServerLocationAbs.'img/signatures/'.$usuarioId.'.png';
    else
      $sigPath = "";
    
    $mailData = array('dest' => $contactEmails.",".$venEmail,
                      'cc' => $venEmail,
                      'cco' => "",
                      'asunto' => $asunto,
                      'msg' => $mensajeHtml,
                      'adjunto1' => $cfgServerLocationAbs."docs/pdf/contratos/".$filename.".pdf",
                      'adjunto2' => "",
                      'adjunto3' => "",
                      'adjunto4' => "",
                      'adjunto5' => "",
                      'adjunto6' => "",
                      'adjunto7' => "",
                      'adjunto8' => "",
                      'adjunto9' => "",
                      'adjunto10' => "",
                      'adjunto11' => "",
                      'adjunto12' => "",
                      'adjunto13' => "",
                      'adjunto14' => "",
                      'adjunto15' => "",
                      'estatus' => "NO ENVIADO",
                      'fecha_envio' => "0000-00-00 00:00:00",
                      'nIDCorreo' => 1,
                      'folio' => "0",
                      'firma' => $sigPath,
                      'observaciones' => "",
                      'bestado' => "",
                      'remitente' => $venEmail
                     );            
    
    // manda correo
    if(!sendBDEmail($mailData)){
      $salida = "ERROR||Ocurrió un problema al enviar el documento por correo electrónico";
    }
    else{
      $contactEmailsArr = explode(",",$contactEmails);
      foreach($contactEmailsArr as $key => $value){
        writeOnJournal($usuarioId,"Ha enviado un correo a nombre de [".$venNom." ".$venApe."] de la cotización con id: [".$cotId."] al email [".$value."]");
      }
      $salida = "OK||El archivo se ha enviado";      
    }
    //borra archivo que se envió y ya no se necesita
    //unlink($filepath.$filename);
  } 
  mysqli_close($db_modulos);
  mysqli_close($db_catalogos);
  mysqli_close($db_usuarios);
}

echo $salida;

?>
