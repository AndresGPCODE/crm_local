<?php
set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

include_once "dbcommon.php";
include_once "log.php";
include_once "common.php";

if("uploadimage" == $_POST['action']){
  if($_FILES['file']['name']){
    if(!$_FILES['file']['error']){
      $name = md5(rand(100, 200));
      $ext = explode('.', $_FILES['file']['name']);
      $filename = $name.'.'.$ext[1];
      $destination = '../../docs/tickets/attachments/temp/'.$filename;
      $location = $_FILES["file"]["tmp_name"];
      move_uploaded_file($location,$destination);
      chmod($destination,775);
      echo 'OK||http://localhost/crm/docs/tickets/attachments/temp/'.$filename; //temporal para pruebas locales
      //echo 'http://crm.coeficiente.mx/crm/docs/tickets/attachments/temp/'.$filename; //el mero bueno
    }
    else{
      $salida = 'ERROR|| Ocurrió un problema al cargarse la imagen: '.$_FILES['file']['error'];
    } 
  }
}

if("removeimage" == $_POST['action']){
  $source = $_POST['src'];
  
  $filePathArr = explode('/',$source);
  $fileNamePos = (count($filePathArr))-1;
    
  $filename = $filePathArr[$fileNamePos];
  
  $origin = '../../docs/tickets/attachments/temp/'.$filename;
  
  if(file_exists($origin)){
    unlink($origin);
    $salida = 'OK||La imagen se eliminó con éxito';
  }
  else{    
    $salida = 'ERROR||La imagen no existe';
  }
}

echo $salida;

?>
