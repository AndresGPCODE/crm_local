<?php
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

/*Cuadros de diálogo que gestionan contactos uno muestra los contactos que se encuantran disponibles y el otro sirve para la captura de contactos nuevos. Deben cargarse a un div en el módulo que los requiera*/
?>

<script type="text/javascript">
//estos comandos se ponen para que al cerrar las ventanas de captura de prospectos, sitios etc los divs que cargan las ventanas de contactos se limpien, esto porque habían conflictos donde para algunos modulos la ventana se abría en el fondo y no dejaba escribir
  $(document).ready(function(){
    $('#mod_prospectInsert').on('hide.bs.modal', function(){
      $("#contactProspectDialog").html("");
    });
    $('#mod_priceInsert').on('hide.bs.modal', function(){
      $("#contactPriceDialog").html("");
    });
    $('#mod_clientInsert').on('hide.bs.modal', function(){
      $("#contactClientDialog").html("");
    });
    $('#mod_siteInsert').on('hide.bs.modal', function(){
      $("#contactSiteDialog").html("");
    });
    $('#mod_siteConnectionInsert').on('hide.bs.modal', function(){
      $("#contactConnDialog").html("");
    });
    $('#mod_clientNewContract').on('hide.bs.modal', function(){
      $("#contactContractDialog").html("");
    });
  });
</script>
  
<!--Cuadro de diálogo de captura de contacto nuevo-->
<div id="mod_contactInsert" class="modal fade" data-backdrop="static" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Capturar contacto</h4>
      </div>
      <div class="container-fluid">
        <div id="div_msgAlertContact"></div>
      </div>
      <div class="modal-body">  
        <form class="form-horizontal" id="frm_newContact" name="frm_newContact" role="form" method="post" enctype="multipart/form-data">
          <div>
            <label>Título:<span id="spn_conTit"></span></label>
          </div>
          <div>
            <label id="sel_personTitle_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Nombre(s):
              <input type="text" id="txt_contNombre" name="txt_contNombre" class="form-control" placeholder="Nombre" maxlength="120" size="40" required>
            </label>  
          </div>
          <div>
            <label id="txt_contNombre_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Apellidos:
              <input type="text" id="txt_contApellidos" name="txt_contApellidos" class="form-control" placeholder="Apellidos" maxlength="120" size="40" required>
            </label>  
          </div>
          <div>
            <label id="txt_contApellidos_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Departamento:<span id="spn_conDepa"></span></label>
          </div>
          <div>
            <label id="sel_department_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Puesto:<span id="spn_conPuesto"></span></label>
          </div>
          <div>
            <label id="sel_position_error" class="div-errorVal"/>
          </div>
          <div id="div_contPuesto_otro">
            <div>
              <label>Puesto (otro):
                <input type="text" id="txt_contPuesto" name="txt_contPuesto" class="form-control" placeholder="Nombre" maxlength="80" size="40">
              </label>  
            </div>
            <div>
              <label id="txt_contPuesto_error" class="div-errorVal"/>
            </div>
          </div>
          <div>
            <label>Teléfono:
              <input type="tel" id="txt_contTel" name="txt_contTel" class="form-control" placeholder="10 dígitos" maxlength="10" size="10">
            </label>  
            <label>Teléfono Móvil:
              <input type="tel" id="txt_contCell" name="txt_contCell" class="form-control" placeholder="10 dígitos" maxlength="10" size="10">
            </label>
          </div>
          <div>
            <label id="txt_contTel_error" class="div-errorVal"/>
            <label id="txt_contCell_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Correo Electrónico(*):
              <input type="email" id="txt_contEmail" name="txt_contEmail" class="form-control" placeholder="Correo Electrónico" maxlength="80" size="20" required>
            </label>
          </div>
          <div>
            <label id="txt_contEmail_error" class="div-errorVal"/>
          </div>
          <input type="hidden" id="hdn_contactNewAction" name="hdn_contactNewAction" class="form-control" value=0>        
        </form>
      </div>
      <div class="modal-footer">         
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
        <input type="submit" class="btn btn-primary" id="btn_contInsert" value="Guardar Contacto" onClick="insertContactOnForm(0,'');">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 

<!--_Cuadro de dialogo de visualización de los contactos-->
<div id="mod_contactList" class="modal fade" data-backdrop="static" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Contactos</h4>
      </div>
      <div class="modal-body row">  
        <div class="col container-fluid" id="div_contactList"></div> 
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 
