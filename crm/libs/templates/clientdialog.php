 <?php
  header('Content-Type: text/html; charset=UTF-8');
  date_default_timezone_set('America/Mexico_City');


  /*Estos son los cuadros de diálogo para la captura de un cliente y un representante legal es usado por el modulo de clientes y de prospectos cuando se van a migrar a clientes:
  
hdn_commentOriginId = id del cliente/prospecto/sitio etc
hdn_commentOriginType  = tipo de origen (0=prospecto, 1=cliente, 2=sitio)
  */
?>       

<!--Cuadro de dialogo de insertar cliente -->
<!--<div id="mod_clientInsert" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Nuevo Cliente</h4>
      </div>
      <div class="container-fluid">
        <div id=div_msgAlertClient></div>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="frm_newClient" name="frm_newClient" role="form" action="saveClient.php" method="post" enctype="multipart/form-data">
          <div>
            <label>Tipo de persona:       
              <input type="radio" name="rad_persona" id="rad_fisica" value=1 checked> Física 
              <input type="radio" name="rad_persona" id="rad_moral" value=2> Moral
            </label>
          </div>
          <div>
            <label>RFC:
              <input type="text" id="txt_rfc" name="txt_rfc" class="form-control" placeholder="XXXX-000000-YYY" maxlength="16" size="35" required>
            </label>    
          </div>
          <div>
            <label id="txt_rfc_error" class="div-errorVal"/>
          </div>             
          <div>
            <label>Razón Social:
              <input type="text" id="txt_razonSoc" name="txt_razonSoc" class="form-control" placeholder="Razón Social" maxlength="80" size="40" required/>
            </label> 
            <span id="spn_txt_razonSoc"></span> 
          </div>
          <div>
            <label id="txt_razonSoc_error" class="div-errorVal"/>
          </div>                          
          <div>
            <label>Nombre Comercial:
              <input type="text" id="txt_nomCom" name="txt_nomCom" class="form-control" placeholder="Nombre Comercial" maxlength="80" size="40" required/>
            </label>
            <span id="spn_txt_nomCom"></span>
          </div>
          <div>
            <label id="txt_nomCom_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Acta Constitutiva:
              <input type="text" id="txt_actaCons" name="txt_actaCons" class="form-control" placeholder="Acta Constitutiva" maxlength="40" size="40" required>
            </label>  
          </div>
          <div>
            <label id="txt_actaCons_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Fecha de Acta:
              <input type="text" id="txt_fecActa" name="txt_fecActa" class="form-control" placeholder="Fecha de Acta" maxlength="40" size="40" required readonly>
            </label>  
          </div>
          <div>
            <label id="txt_actaCons_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Teléfono:
              <input type="tel" id="txt_tel" name="txt_tel" class="form-control" placeholder="10 dígitos" maxlength="10" size="10" required>
            </label>  
            <label>Teléfono Móvil:
              <input type="tel" id="txt_cell" name="txt_cell" class="form-control" placeholder="10 dígitos" maxlength="10" size="10" required>
            </label>
          </div>
          <div>
            <label id="txt_tel_error" class="div-errorVal"></label>
            <label id="txt_cell_error" class="div-errorVal"></label>
          </div>
          <div>
            <label>Domicilio Fiscal:
              <input type="text" id="txt_dom" name="txt_dom" class="form-control" placeholder="Domicilio" maxlength="40" size="40" required>
            </label>
          </div>
          <div>
            <label id="txt_dom_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Colonia Fiscal:
              <input type="text" id="txt_col" name="txt_col" class="form-control" placeholder="Colonia" maxlength="40" size="40" required>
            </label>
          </div>
          <div>
            <label id="txt_col_error" class="div-errorVal"/>
          </div>
          <div>
            <label>C.P. Fiscal:
              <input type="text" id="txt_cp" name="txt_cp" class="form-control" placeholder="C.P." maxlength="5" size="5" required>
            </label>
            <label>Municipio Fiscal:
              <input type="text" id="txt_mun" name="txt_mun" class="form-control" placeholder="Municipio" maxlength="30" size="30" required>
            </label>
          </div>
          <div>
            <label id="txt_cp_error" class="div-errorVal"></label>
            <label id="txt_mun_error" class="div-errorVal"></label>
          </div>
          <div>
            <label>Estado Fiscal:<span id="spn_edo"></span></label>
          </div>
          <div>
            <label id="sel_state_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Correo Electrónico:
              <input type="email" id="txt_email" name="txt_email" class="form-control" placeholder="Correo Electrónico" maxlength="60" size="20" required>
            </label>
          </div>
          <div>
            <label id="txt_email_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Correo Electrónico Alterno:
              <input type="email" id="txt_emailAlt" name="txt_emailAlt" class="form-control" placeholder="Correo Electrónico" maxlength="60" size="20" required>
            </label>
          </div>
          <div>
            <label id="txt_emailAlt_error" class="div-errorVal"/>
          </div>
               
          <label>Contactos:<span id="spn_contacto"></span></label>
          <div>
            <input type="button" class="btn btn-primary" id="btn_insertarContacto" value="Agregar Contacto" onClick="openNewContact();">
          </div>
          <div>
            <div class="table-responsive">
              <table id="tbl_selectedContact" class="table table-hover table-striped">
                <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Apellidos</th>
                    <th>Puesto</th>
                    <th>Teléfono</th>
                    <th>Teléfono Móvil</th>
                    <th>Correo Electŕonico</th>
                    <th>Acciones</th>
                  </tr>
                </thead>
                <tbody>              
                </tbody>
              </table>
            </div>
          </div>
          
           <div>
            <label id="hdn_contArr_error" class="div-errorVal"/>
          </div>
          
          <label>Representante legal:<span id="spn_representante"></span></label>
          <div>
            <input type="button" class="btn btn-primary" id="btn_insertarRepresentante" value="Agregar Representante" onClick="openNewManager();">
          </div>
          <div>
            <div class="table-responsive">
              <table id="tbl_selectedManager" class="table table-hover table-striped">
                <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Apellidos</th>
                    <th># Poder</th>     
              <!-- <th>Fecha Poder</th> 
                    <th>Domicilio</th>
                    <th>Colonia</th>
                    <th>C.P.</th>
                    <th>Municipio</th>
                    <th>Estado</th>-->
<!--                    <th>Teléfono</th>
                    <th>Teléfono Movil</th>
                    <th>Email</th>                       
                    <th>Acciones</th>
                  </tr>
                </thead>
                <tbody>              
                </tbody>
              </table>
            </div>
          </div>
          
          <div>
            <label id="hdn_manArr_error" class="div-errorVal"/>
          </div> 
          <div>
            <label>Origen de Cliente:<span id="spn_orgn"></span></label>
          </div>
          <div>
            <label id="sel_origin_error" class="div-errorVal"/>
          </div>
          <div id="div_other_origin" name="div_other_origin">
            <div>
              <label>Otro origen:
                <input type="text" id="txt_origin" name="txt_origin" class="form-control" placeholder="Especifique Origen" maxlength="255" size="40" required>
              </label>
            </div>
            <div>
              <label id="txt_origin_error" class="div-errorVal"/>
            </div>
          </div>                                    
          <div>
            <label>Descripción:
              <textarea id="txt_desc" name="txt_desc" class="form-control" placeholder="Agregue una breve descripción acerca del cliente." maxlength="255" cols="40" rows="5" required></textarea>
            </label>
            <div>
              <label id="txt_desc_error" class="div-errorVal"/>
            </div>
          </div>
          <input type="hidden" id="hdn_action" name="hdn_action" class="form-control" value=-1>
          <input type="hidden" id="hdn_cliId" name="hdn_cliId" class="form-control" value=-1> 
          <input type="hidden" id="hdn_contArr" name="hdn_contArr" class="form-control" value=""> 
          <input type="hidden" id="hdn_manArr" name="hdn_manArr" class="form-control" value="">                           
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
        <input type="submit" class="btn btn-primary" id="cargaCamp" value="Guardar Cambios" onClick="saveClientNew();">
      </div>
         </form>
    </div><!-- /.modal-content -->
<!--  </div><!-- /.modal-dialog -->
<!--</div><!-- /.modal -->  

<!--Cuadro de diálogo de captura de representante legal nuevo-->
<div id="mod_managerInsert" class="modal fade" data-backdrop="static" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Capturar Representante Legal</h4>
      </div>
      <div class="container-fluid">
        <div id=div_msgAlertManager></div>
      </div>
      <div class="modal-body">  
        <form class="form-horizontal" id="frm_newManager" name="frm_newManager" role="form" method="post" enctype="multipart/form-data">
          <div>
            <label>Nombre(s):
              <input type="text" id="txt_repNombre" name="txt_repNombre" class="form-control" placeholder="Nombre" maxlength="120" size="40" required>
            </label>  
          </div>
          <div>
            <label id="txt_repNombre_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Apellidos:
              <input type="text" id="txt_repApellidos" name="txt_repApellidos" class="form-control" placeholder="Apellidos" maxlength="120" size="40" required>
            </label>  
          </div>
          <div>
            <label id="txt_repApellidos_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Número de poder:
              <input type="text" id="txt_repNumPod" name="txt_repNumPod" class="form-control" placeholder="Número de poder" maxlength="40" size="40" required>
            </label>  
          </div>
          <div>
            <label id="txt_repNumPod_error" class="div-errorVal"/>
          </div>
          
          <div>
            <label>Domicilio:
              <input type="text" id="txt_repDom" name="txt_repDom" class="form-control" placeholder="Domicilio" maxlength="120" size="40" required>
            </label>
          </div>
          <div>
            <label id="txt_repDom_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Colonia:
              <input type="text" id="txt_repCol" name="txt_repCol" class="form-control" placeholder="Colonia" maxlength="120" size="40" required>
            </label>
          </div>
          <div>
            <label id="txt_repCol_error" class="div-errorVal"/>
          </div>
          <div>
            <label>C.P.:
              <input type="text" id="txt_repCp" name="txt_repCp" class="form-control" placeholder="C.P." maxlength="5" size="5" required>
            </label>
            <label>Municipio:
              <input type="text" id="txt_repMun" name="txt_repMun" class="form-control" placeholder="Municipio" maxlength="80" size="30" required>
            </label>
          </div>
          <div>
            <label id="txt_repCp_error" class="div-errorVal"/>
            <label id="txt_repMun_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Estado:<span id="spn_repEdo"></span></label>
          </div>
          <div>
            <label id="sel_repstate_error" class="div-errorVal"/>
          </div>              
          <div>
            <label>Fecha de poder:
              <input type="text" id="txt_repFecPod" name="txt_repFecPod" class="form-control" placeholder="Fecha de poder" maxlength="20" size="40" readonly required>
            </label>  
          </div>
          <div>
            <label id="txt_repFecPod_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Teléfono:
              <input type="tel" id="txt_repTel" name="txt_repTel" class="form-control" placeholder="10 dígitos" maxlength="10" size="10" required>
            </label>  
            <label>Teléfono Móvil:
              <input type="tel" id="txt_repCell" name="txt_repCell" class="form-control" placeholder="10 dígitos" maxlength="10" size="10" required>
            </label>
          </div>
          <div>
            <label id="txt_repTel_error" class="div-errorVal"/>
            <label id="txt_repCell_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Correo Electrónico:
              <input type="email" id="txt_repEmail" name="txt_repEmail" class="form-control" placeholder="Correo Electrónico" maxlength="80" size="20" required>
            </label>
          </div>
          <div>
            <label id="txt_repEmail_error" class="div-errorVal"/>
          </div>
          <input type="hidden" id="hdn_mrepActNewAction" name="hdn_manactNewAction" class="form-control" value=0>        
        </form>
      </div>
      <div class="modal-footer">         
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
        <input type="submit" class="btn btn-primary" id="btn_manInsert" value="Guardar" onClick="insertManagerOnForm(0,'');">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--_Cuadro de dialogo de detalles de cliente -->
<div id="mod_clientDetails" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Detalles de Cliente</h4>
      </div>
      <div class="modal-body row">  
        <div class="col container-fluid" id="div_clientDetails"></div> 
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 
