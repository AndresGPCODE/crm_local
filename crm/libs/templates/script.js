//var audio = new Audio('/crm/sound/bikeRingBell.mp3');
var loOpt = 3;
$(document).ready(function(){
  // getDateTime();
  // getUnreadMsgNum("notifMenuIcon",1,"btn-circle-notif");
  // getUnreadMsgNum("alertMenuIcon",0,"btn-circle-alert");
  // getUnreadMsgNum("warnMenuIcon",2,"btn-circle-warning");
  // verifySession(4);
});
function getDateTime(){
  $.ajax({
    type: "POST",
    url: "/crm/crm/libs/templates/ajax.php",
    data: {"action": "getDateTime"},
    success: function(msg){
      $("#div_dateTime").html(msg);
    }
  });
  setTimeout(function() { getDateTime(); }, 1000);
}
//checa cuantos mensajes hay sin leer para notificar en la barra de navegacion - reotrna el numero de mensajes que se presenta encima de los iconos
function getUnreadMsgNum(divId,option,colorClass){
  $.ajax({
    type: "POST",
    url: "/crm/crm/libs/templates/ajax.php",
    data: {"action": "getUnreadMsgNum", "option":option},
    success: function(msg){
      var resp = msg.trim().split("||");
      if("NOPE"!=resp[0]){
        $("#btn_"+divId).addClass(colorClass); 
      }
      else{
        $("#btn_"+divId).removeClass(colorClass);
      }
      $("#spn_"+divId).html(resp[1]); 
    }  
  });
  setTimeout(function() { getUnreadMsgNum(divId,option,colorClass); }, 10000);
}
function verifySession(opt){
  $.ajax({
    type: "POST",
    url: "/crm/crm/libs/templates/ajax.php",
    data: {action:"verifySession", opt:opt},
    success: function(msg){
      var resp = (msg).trim().split("||");
      if(0==resp[0]){
        //cuando la session expira regresa a el login
        window.open("../../modulos/login/logout.php?sub=timeout",'_self');
        //cuando la session expira envia a la pagina de error de session
        // location.href = "../../crm/error_session.php";
      }
      else if(1==resp[0]){
        $("#spn_remTime").html(resp[2]);   
        $("#mod_renewSession").modal("show").on("hide",function(){
          $("#mod_renewSession").modal("hide");
        });
      }
      else if(2==resp[0]){
        verifySession(2);
      }
      else if(3==resp[0]){
        $("#mod_renewSession").modal("hide");
        alert(resp[1]);
      }
    }
  });
  setTimeout(function() {verifySession(1) }, 1000);
}
//obtiene lista de mensajes
function getAlert(divId,option){
  $.ajax({
    type: "POST",
    url: "/crm/crm/libs/templates/ajax.php",
    data: {"action": "getAlert", "option":option},
    success: function(msg){
      $("#div_"+divId).html(msg);
    }  
  });
}