<?php
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

include_once "../db/common.php";

/*Son los cuadros de diálogo para la visualización y para la inserción y edición de de documentos para clientes, prospectos sitios etc. Funciona para cualquier página que lo invoque, siempre y cuando setee las variables ocultas adeucadamente*/
/*Si inserta o edita y si se trata de un prospecto, cliente etc están indicados en las variables ocultas:

hdn_docUploadAction: Si es editar o guardar nuevo
hdn_docId: Id del documento a editar
hdn_orgType: Indica si será 0= prospecto, 1= cliente, 2= sitio etc
*/
?>
<script type="text/javascript">
$(document).ready(function(){
  //preview de imagenes en el formulario de documentos
  $.uploadPreview({
    input_field: "#fl_docNew",   // Default: .image-upload
    preview_box: "#div_doc_prev",  // Default: .image-preview
    label_field: "#lbl_doc_label",    // Default: .image-label
    label_default: "Elija un archivo (tamaño máximo 5Mb)",
    label_selected: "Cambiar archivo",
    no_label: false                 // Default: false
  }); 
});
</script>

<!--_Cuadro de dialogo de documentos para cliente -->
<div id="mod_uploadedDocuments" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Documentos escaneados</h4>
      </div>
      <div class="container-fluid">    
        <div id="div_msgAlertDocuments"></div>
      </div>
      <div class="modal-body">  
<?php  if(hasPermission(1,'w')){ //valida permisos que tiene en clientes, por ahora?>  
        <div><button type="button" id="btn_documentNew" title="Agregar Documento" class="btn btn-default" onClick="openNewDocument();"><span class="glyphicon glyphicon-asterisk" data-target="#mod_uploadNewDocument"></span> Agregar Documento</button></div>
<?php } ?>
        <div class="col container-fluid" id="div_uploadedDocuments"></div> 
      </div>        
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->  

<!--_Cuadro de dialogo de documentos para cliente/sitio/prospecto -->
<div id="mod_uploadNewDocument" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Agregar/Editar Documento</h4>
      </div>
      <div class="container-fluid">    
        <div id="div_msgAlertDocumentNew"></div>
      </div>
      <form class="form-horizontal" id="frm_newDocument" name="frm_newDocument" role="form" method="post" enctype="multipart/form-data">
      <div class="modal-body">  
        <div id="div_doc_prev">
          <label for="fl_docNew" id="lbl_doc_label">Elija un archivo (tamaño máximo 5Mb)</label>
          <input type="file" name="fl_docNew" id="fl_docNew"/>
        </div>
        <div>
          <label id="fl_docNew_error" class="div-errorVal"/>
        </div>
        <div>
          <label>Tipo de documento:<span id="spn_docType"></span></label>
        </div>
        <div>
          <label id="sel_docType_error" class="div-errorVal"/>
        </div>
        <div>
          <label>Descripción:
            <textarea id="txt_docDesc" name="txt_docDesc" class="form-control" placeholder="Agregue una breve descripción acerca del Documento." maxlength="255" cols="40" rows="5" required></textarea>
          </label>
          <div>
            <label id="txt_docDesc_error" class="div-errorVal"/>
          </div>
        </div>  
        <input type="hidden" id="hdn_docId" name="hdn_docId" class="form-control" value="0">
        <input type="hidden" id="hdn_docUploadAction" name="hdn_docUploadAction" class="form-control" value=-1>           
      </div>
      <div class="modal-footer">         
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
        <input type="submit" class="btn btn-primary" id="btn_docInsert" value="Guardar" onClick="saveNewDocument();">
      </div> 
      </form> 
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

