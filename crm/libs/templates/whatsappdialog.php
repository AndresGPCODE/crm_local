<?php
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');
include_once "../db/common.php";

?>
<script type="text/javascript">
  
  function openWhatsMsg(tel){
    $("#div_whatCommHistory").html("");
    delWhatMsgBox();
    $("#hdn_phone").val(tel);
    $("#mod_whatsMsg").modal("show");
  }
  
  function readWhatsMsgs(orgId,orgType){
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: {"action": "getWhatsHistory", "orgId":orgId, "orgType":orgType},
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(rsp){//id||tipo(entrada/salida)||mensaje
        var resp = rsp.trim().split("||");
        if(0==resp[1])
          var msgtypeclass = "message-out bubble-send";
        else
          var msgtypeclass = "message-in bubble-receive";
        $("#div_whatCommHistory").append("<div class=\""+msgtypeclass+"\" align=\"right\">"+resp[2]+"</div><br>");
      }  
    });
    setTimeout(function() { readWhatsMsgs(orgId,orgType); }, 1000);
  }
  
  function sendWhatsMsg(tel){
    callMarcatron($("#hdn_phone").val(),"WHATSAPP"); 
    delWhatMsgBox();
  }
  
  function delWhatMsgBox(){
    $("#txt_whatsMsg").val("");
  }
  
</script>

<style type="text/css">
  #slr_eveDurDia label{
    position: absolute;
    width: 20px;
    margin-left: -10px;
    text-align: center;
    margin-top: 20px;
  }
  
  .bubble-send {
    padding: 6px 0 8px 0;
    padding-left: 9px;
    padding-right: 7px;
    min-width: 110px;
    background-color:#dcf8c6;
    text-align: right;
    word-wrap: break-word;
    box-sizing: border-box;
  }
  
  .bubble-receive {
    padding: 6px 0 8px 0;
    padding-left: 7px;
    padding-right: 9px;
    min-width: 110px;
    background-color:#ffffff;
    text-align: left;
    word-wrap: break-word;
    box-sizing: border-box;
  }
  
  .message-in, .message-out {
    border-radius: 7.5px;
    position: relative;
    box-shadow: 0 1px 0.5px rgba(0,0,0,0.13);
  }
  
  .whats-history-div{
    height:auto;
    max-height:700px;
    overflow:auto; 
  }
</style>

<!--_Cuadro de dialogo de visualización de mensajes -->
<div id="mod_whatsMsg" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">WhatsApp Mensajería</h4>
      </div>  
      <div class="modal-body"> 
        <div class="container-fluid">
          <div id=div_whatsMsgAlert"></div>
        </div>
        <div id="div_whatCommHistory" class="form-signing-round whats-history-div">        
        </div>    
        <div class="form-signing-round">
          <label>Mensaje:
            <textarea id="txt_whatsMsg" name="txt_whatsMsg" class="form-control" placeholder="Mensaje." maxlength="1024" cols="40" rows="5" required></textarea>
          </label>
          <div>
            <label id="txt_whatsMsg_error" class="div-errorVal"/>
          </div>
        </div>
        <input type="hidden" id="hdn_phone" name="hdn_phone" class="form-control" value=-1>
      </div>
      <div class="modal-footer">
        <button class="btn" lass="btn btn-default" onClick="delWhatMsgBox();">Borrar</button>
        <input type="submit" class="btn btn-success" id="cargaEventCamp" value="Enviar" onClick="sendWhatsMsg();">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 
