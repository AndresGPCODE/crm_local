<!--Cuadro de diálogo de captura de representante legal nuevo-->
<div id="mod_managerInsert" class="modal fade" data-backdrop="static" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Capturar Representante Legal</h4>
      </div>
      <div class="container-fluid">
        <div id="div_msgAlertManager"></div>
      </div>
      <div class="modal-body">  
        <form class="form-horizontal" id="frm_newManager" name="frm_newManager" role="form" method="post" enctype="multipart/form-data">
          <div>
            <label>Nombre(s)(*):
              <input type="text" id="txt_repNombre" name="txt_repNombre" class="form-control" placeholder="Nombre" maxlength="120" size="40" required>
            </label>  
          </div>
          <div>
            <label id="txt_repNombre_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Apellidos(*):
              <input type="text" id="txt_repApellidos" name="txt_repApellidos" class="form-control" placeholder="Apellidos" maxlength="120" size="40" required>
            </label>  
          </div>
          <div>
            <label id="txt_repApellidos_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Número de poder(*):
              <input type="text" id="txt_repNumPod" name="txt_repNumPod" class="form-control" placeholder="Número de poder" maxlength="40" size="40" required>
            </label>  
          </div>
          <div>
            <label id="txt_repNumPod_error" class="div-errorVal"/>
          </div>          
          <div>
            <label>Notario(*):
              <input type="text" id="txt_repNotario" name="txt_repNotario" class="form-control" placeholder="Nombre de Notario" maxlength="30" size="20" required>
            </label>  
            <label>Folio Mercantil(*):
              <input type="text" id="txt_repFolio" name="txt_repFolio" class="form-control" placeholder="Folio Mercantil" maxlength="30" size="20" required>
            </label>
          </div>
          <div>
            <label id="txt_repNotario_error" class="div-errorVal"/>
            <label id="txt_repFolio_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Domicilio:
              <input type="text" id="txt_repDom" name="txt_repDom" class="form-control" placeholder="Domicilio" maxlength="120" size="40">
            </label>
          </div>
          <div>
            <label id="txt_repDom_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Colonia:
              <input type="text" id="txt_repCol" name="txt_repCol" class="form-control" placeholder="Colonia" maxlength="120" size="40">
            </label>
          </div>
          <div>
            <label id="txt_repCol_error" class="div-errorVal"/>
          </div>
          <div>
            <label>C.P.:
              <input type="text" id="txt_repCp" name="txt_repCp" class="form-control" placeholder="C.P." maxlength="5" size="5" required>
            </label>
            <label>Municipio:
              <input type="text" id="txt_repMun" name="txt_repMun" class="form-control" placeholder="Municipio" maxlength="80" size="30">
            </label>
          </div>
          <div>
            <label id="txt_repCp_error" class="div-errorVal"/>
            <label id="txt_repMun_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Estado:<span id="spn_repEdo"></span></label>
          </div>
          <div>
            <label id="sel_repstate_error" class="div-errorVal"/>
          </div>              
          <div>
            <label>Fecha de poder(*):
              <input type="text" id="txt_repFecPod" name="txt_repFecPod" class="form-control" placeholder="Fecha de poder" maxlength="20" size="40" readonly required>
            </label>  
          </div>
          <div>
            <label id="txt_repFecPod_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Teléfono:
              <input type="tel" id="txt_repTel" name="txt_repTel" class="form-control" placeholder="10 dígitos" maxlength="10" size="10">
            </label>  
            <label>Teléfono Móvil:
              <input type="tel" id="txt_repCell" name="txt_repCell" class="form-control" placeholder="10 dígitos" maxlength="10" size="10">
            </label>
          </div>
          <div>
            <label id="txt_repTel_error" class="div-errorVal"/>
            <label id="txt_repCell_error" class="div-errorVal"/>
          </div>
          <div>
            <label>Correo Electrónico:
              <input type="email" id="txt_repEmail" name="txt_repEmail" class="form-control" placeholder="Correo Electrónico" maxlength="80" size="20">
            </label>
          </div>
          <div>
            <label id="txt_repEmail_error" class="div-errorVal"/>
          </div>
          <input type="hidden" id="hdn_mrepActNewAction" name="hdn_manactNewAction" class="form-control" value=0>        
        </form>
      </div>
      <div class="modal-footer">         
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>     
        <input type="submit" class="btn btn-primary" id="btn_manInsert" value="Guardar" onClick="insertManagerOnForm(0,'');">      
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 
