<?php
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

include_once "../db/common.php";
?>
<script type="text/javascript">
  $(document).ready(function(){
    $("#txt_contFechaCon").datetimepicker(datetimeOptions);
  });
</script>

<!--Cuadro de dialogo de visualización de contratos-->
<div id="mod_contracts" class="modal fade" data-backdrop="static" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Contratos</h4>
      </div>
      <div class="container-fluid">
        <div id="div_msgAlertContracts"></div>
      </div>
      <div class="modal-body row">  
        <div class="col container-fluid">
<?php   
  if(hasPermission(4,'w')){ ?>         
          <button type="button" id="btn_contracteNew" title="Nuevo Contrato" class="btn btn-default" onClick="openNewContract();"><span class="glyphicon glyphicon-asterisk"></span> Nuevo Contrato</button><br>
<?php } ?>          
        </div>
        <div class="col container-fluid" id="div_contracts"></div> 
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 

<!--_Cuadro de dialogo de nuevo contrato de cliente -->
<div id="mod_clientNewContract" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">NuevoContrato</h4>
      </div>          
      <div class="container-fluid">
        <div id="div_msgAlertClientContract"></div>
      </div>
      <div class="modal-body">   
        <div id="div_insReqOpt">
          <label>Tipo de Servicio: <br>                     
            <input type="radio" name="rad_contSer" id="rad_cam" value=0 checked> Cambio de Domicilio<br>
            <input type="radio" name="rad_contSer" id="rad_ins" value=1> Instalación<br>
          </label>
        </div>
        <label>Seleccionar contacto: <br> 
        </label> 
        <div id="div_contrConSel"></div>                                                          
        <input type="hidden" name="hdn_cotId" id="hdn_cotId" value=-1 />
        <div>
          <label>Notas:
            <textarea id="txt_contNota" name="txt_contNota" class="form-control" placeholder="Agregue algun aspecto mencionable con respecto al contrato." maxlength="255" cols="40" rows="5"></textarea>
          </label>
          <div>
            <label id="txt_contNota_error" class="div-errorVal"/>
          </div>
        </div>
        <div>
          <label>Fecha Estimada de Inicio de Contrato:
            <input type="text" id="txt_contFechaCon" name="txt_contFechaCon" class="form-control" placeholder="Fecha de Inicio" maxlength="20" size="40" readonly>
          </label>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
        <input type="button" class="btn btn-danger" id="btn_saveSitConSigned" value="Contrato Firmado" onClick="saveContract(2);">
        <input type="button" class="btn btn-success" id="btn_saveSitCon" value="Guardar Contrato" onClick="saveContract(0);">
        <input type="button" class="btn btn-default" id="btn_sendSitConEmail" value="Enviar por Correo" onClick="openContractEmail();">
        <input type="button" class="btn btn-primary" id="btn_prevSitCon" value="Previsualizar Contrato" onClick="getContract(0);">
      </div>          
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 

<!--_Cuadro de dialogo de nuevo contrato de cliente -->
<div id="mod_clientContractAtt" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Anexar a Contrato</h4>
      </div>          
      <div class="container-fluid">
        <div id="div_msgAlertContractAtt"></div>
      </div>
      <div class="modal-body">   
        <label>Seleccionar contrato: <br> 
        </label> 
        <div id="div_contrConSelAtt"></div>                                                          
        
        <input type="hidden" name="hdn_cotId" id="hdn_cotId" value=-1 />
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
      </div>          
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 

<!--_Cuadro de dialogo de envio de cotización por email-->
<div id="mod_sendContractPdf" class="modal fade" data-backdrop="static" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Enviar cotización por correo</h4>
      </div><!-- /.modal-header -->
      <div class="container-fluid">
        <div id="div_msgAlertContractSendEmail"></div>
      </div>
      <div class="modal-body">  
        <form class="form-horizontal" id="frm_newContractEmail" name="frm_newContractEmail" role="form" method="post" enctype="multipart/form-data">    
         <label>Seleccionar destinatarios:<span id="spn_contacto"></span></label>
          <div>
            <input type="button" class="btn btn-primary" id="btn_buscarMailContactosContrato" value="Agregar Contacto" onClick="getContactList(11);">
          </div><br>
          <!--Datos de contacto-->
          <div class="table-responsive">
            <table id="tbl_selectedMailContactContract" class="table table-hover table-striped">
              <thead>
                <tr>
                  <th>id</th>
                  <th>Nombre</th>
                  <th>Apellidos</th>
                  <th>Puesto</th>
                  <th>Teléfono</th>
                  <th>Teléfono Móvil</th>
                  <th>Correo Electŕonico</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>                
              </tbody>
            </table>
          </div><hr>         
          <div>Asunto: 
            <input type="text" id="txt_contractMsgSubject" name="txt_contractMsgSubject" class="form-control" placeholder="" maxlength="80" size="60" required>
          </div>
          <div>
            <label id="txt_contractMsgSubject_error" class="div-errorVal"/>
          </div>
          <br> 
          Contenido del mensaje:<br>          
          <!--Editor de texto-->     
          <div id="div_contractMsgContent"></div> 
          
          <input type="hidden" id="hdn_contHtmlMsg" name="hdn_contHtmlMsg" class="form-control" value="">
          <input type="hidden" id="hdn_contPlainMsg" name="hdn_contPlainMsg" class="form-control" value="">
          <input type="hidden" id="hdn_cotIdCont" name="hdn_cotIdCont" class="form-control" value=-1>
          <input type="hidden" id="hdn_contIdCont" name="hdn_contIdCont" class="form-control" value=-1>
          <input type="hidden" id="hdn_orgTypeMailCont" name="hdn_orgTypeMailCont" class="form-control" value="">  
          <input type="hidden" id="hdn_contMailOpt" name="hdn_contMailOpt" class="form-control" value=-1>                               
         
      </div><!-- /.modal-body -->
      <div class="modal-footer">         
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
        <input type="button" class="btn btn-primary" id="btn_mandaConMail" value="Enviar Contrato" onClick="sendContractEmail();">
      </div><!-- /.modal-footer -->
        </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->  

<div class="container" id="contactContractDialog"></div><!--cuadro de dialogo para gestión de contactos--> 
