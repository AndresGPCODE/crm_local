<?php
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');
if (!isset($_SESSION)) {
  session_start();
}
$usuarioId  = isset($_SESSION['usrId']) ? $_SESSION['usrId'] : "";
include_once "../db/common.php";

/*Son los cuadros de diálogo para la visualización y para la inserción y edición de eventos nuevos al calendario desde la pagina de prospectos, clientes sitios etc. Funciona para cualquier página que lo invoque, siemrpe y cuando setee las variables ocultas adeucadamente*/
/*Si inserta o edita y si se trata de un prospecto, cliente etc están indicados en las variables ocultas:*/

?>

<script type="text/javascript">
  //borra el contenido del campo fecha fin en formulario de tareas si el campo fecha inicio está vacío
  
  $("#txt_tareaFechaIni").on("blur",function(){
    if(""==$("#txt_tareaFechaIni").val())
      $("#txt_tareaFechaFin").val("");
  });
  
  function getWorkOrders(relCotPro){
		$("#hdn_relProCon").val(relCotPro);
		$.ajax({
			type: "POST",
			url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
			data: {"action": "getWorkOrderList", "relCotPro":relCotPro},	
			beforeSend: function(){
				setLoadDialog(0,"Consultando ordenes de trabajo");
			},
			complete: function(){
				setLoadDialog(1,"");
			},			
			success: function(msg){
				$("#div_worOrderList").html(msg); 
				$("#hdn_relCotPro").val(relCotPro);
				$("#mod_workOrders").modal("show");
				setModalResponsive("mod_workOrders");
			}  
		});	
	}	
	
	//obtiene los estatus de eventos
	/*function getTaskStatList(id){
		$.ajax({
			type: "POST",
			url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
			data: {"action": "getEventStatList", "id":id},
			success: function(msg){
				$("#spn_tareaEstat").html(msg); 
			}  
		});
	}*/
	
	//obtiene las adecuaciones para las ordenes de trabajo
	function getAdecList(id){
		$.ajax({
			type: "POST",
			url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
			data: {"action": "getAdecList", "id":id, "ordenId":$("#hdn_worOrdId").val()},
			success: function(msg){
				$("#spn_worOrdStatAdecList").html(msg); 
			}  
		});
	}
	
	function getWorkOrderTypeList(id,option){
		$.ajax({
			type: "POST",
			url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
			data: {"action": "getWorkOrderTypeList", "id":id, "option":option},
			success: function(msg){
				$("#div_worOrdRad").html(msg); 
			}  
		});
	}
	
	//obtiene los estatus de eventos
	function getWorkOrderStatList(id){
		$.ajax({
			type: "POST",
			url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
			data: {"action": "getWorkOrderStatList", "id":id},
			success: function(msg){
				$("#spn_worOrdStat").html(msg); 
				if(3!=id && 5!=id){
					$("#div_worOrdFact").hide();
					$("#div_worOrdStatAdecList").hide();
				}
				else{
					$("#div_worOrdFact").show();
					$("#div_worOrdStatAdecList").show();
				}
				$("#sel_worOrdEstatus").on("change",function(){
					if(3==$("#sel_worOrdEstatus option:selected").val())
						$("#div_worOrdFact").show();
					else{
						$("#div_worOrdFact").hide();
						$("#tbl_selectedAdec").find("tr:gt(0)").remove();
						adecArray.length = 0;
					}
				}); 
				
				$('input[name="rad_workOrdFact"]').on("change",function(){
					$(this).prop("checked",true);
					if(3==$('input[name="rad_workOrdFact"]:checked').val()){
						$("#div_worOrdStatAdecList").show();
						getAdecList(id);
					}
					else{
						adecArray.length = 0;
						$("#tbl_selectedAdec").find("tr:gt(0)").remove();
						$("#div_worOrdStatAdecList").hide();
					}
				});
			}
		});
  }
  
  //funcion que abre el cuadro de dialogo para insertar una tarea nueva
function openWorkOrderTaskNew(){
	$("#txt_tareaTitulo").val(""); 
	$("#txt_tareaDesc").val("");
  $("#txt_tareaFechaIni").val("");
  $("#txt_tareaFechaFin").val("");
  $("#txt_tareaFechaIni").datetimepicker(datetimeOptions);
  $("#txt_tareaFechaFin").datetimepicker(datetimeOptions);
  $("#txt_taresDurHor").datetimepicker(timeOptions);
  $("#hdn_tareaAction").val(0);
  $("#mod_newWorkOrderTask").modal("show");
  $("#txt_tareaFechaFin").prop('readonly',true);
  $("#txt_tareaFechaIni").prop('disabled',false);
  $("#txt_tareaDurHor").prop('disabled',false);
  $("#sel_tareaDur").prop('disabled',false);
  $("#txt_tareaDurHor").prop('readonly',true);
  $("#btn_tareaEstHor").prop('disabled',false);
  $("#btn_tareaAddGuest").prop('disabled',true);
  $("#tbl_tareaSelectedGuest").find("tr:gt(0)").remove();
  $("#tbl_tareaSelectedEquip").find("tr:gt(0)").remove();
  $("#spn_tareaInv").html("");
  $("#div_msgAlertTask").html("");
  $("#txt_tareaTitulo_error").html("");
  $("#txt_tareaFechaIni_error").html("");
  $("#txt_tareaFechaFin_error").html("");
  $("#sel_tareaEstat_error").html("");
  $("#txt_tareaDesc_error").html("");
  getEventStatList(0,2);
  getEquipmentList(0);
  invitadoArray.length = 0;
  equipoArray.length = 0;
  setModalResponsive("mod_newWorkOrderTask");
}

//función que valida la inegridad de lso datos introducidos en el formulario devuelve true si todos son correctos
/*function validateWorkOrderTaskData(){ 
  addValidationRules(3);
  
  return $("#frm_newWorkOrderTask").validate({
    ignore:':disabled:not(#txt_tareaFechaFin)',
    rules: {
      txt_worOrdTitulo:
      {
        required: true
      },
      txt_worOrdDesc:
      {
        required: true
      },
      txt_tareaFechaIni:
      {
        isDateTime: true,
        isLaterThanToday: true
      },
      txt_tareaFechaFin:
      {
        isDateTime: true,
        isLaterThanDate: 'txt_tareaFechaIni'
      },
      sel_eveTipo:
      {
        isSomethingSelected: true
      },
      sel_eveEstat:
      {
        isSomethingSelected: true
      }
    },
    errorPlacement: function(error, element){
      error.appendTo($('#' + element.attr("id") + '_error'));
    }
  }).form(); 
}*/

</script>

<style type="text/css">
  #slr_tareaDurDia label{
    position: absolute;
    width: 20px;
    margin-left: -10px;
    text-align: center;
    margin-top: 20px;
  }
</style>

<!--_Cuadro de dialogo de visualización de ordenes de trabajo -->
<div id="mod_workOrders" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Ordenes de trabajo</h4>
      </div>
      <div class="container-fluid">
        <div id="div_msgAlertWorkOrderList"></div>
      </div>
      <div class="modal-body row">  
        <div class="col container-fluid" id="div_worOrderList"></div> 
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 

<!--_Cuadro de dialogo de visualización de detalles de orden -->
<div id="mod_workOrderDetails" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Orden de Trabajo</h4>
      </div>
      <div class="container-fluid">
        <div id="div_msgAlertWorkOrderDetails"></div>
      </div>
      <div class="modal-body row"> 	 
        <div>
					<div id="div_worOrderDetails"></div>
				</div>
      </div>
      <div class="container" id="commentWorkOrderDialog"></div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 

<!--_Cuadro de dialogo de insertar orden-->
<div id="mod_newWorkOrder" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Nueva Orden de Trabajo</h4>
      </div>
      <div class="container-fluid">
        <div id="div_msgAlertWorkOrder"></div>
      </div>
      <div class="modal-body"> 
        <form class="form-horizontal" id="frm_newWorkOrder" name="frm_newWorkOrder" role="form" method="post" enctype="multipart/form-data">
				<div>
					<label>Tipo de Trabajo<label><br>
				</div>
				<div id="div_worOrdRad"></div>	
				<div>
					<label>Título:
						<input type="text" id="txt_worOrdTitulo" name="txt_worOrdTitulo" class="form-control" placeholder="Título o nombre" maxlength="80" size="40" required>
					</label>  
				</div>
        <div>
          <label id="txt_worOrdTitulo_error" class="div-errorVal"/>
        </div>
        <div>
					<label>Descripción:
						<textarea id="txt_worOrdDesc" name="txt_worOrdDesc" class="form-control" placeholder="Agregue una breve descripción acerca de la orden." maxlength="255" cols="40" rows="5" required></textarea>
					</label>
					<div>
						<label id="txt_worOrdDesc_error" class="div-errorVal"/>
					</div>
				</div>
<?php   
  if(hasPermission(9,'w') || $usuarioId == 6){ ?>
          <div>
            <div>
              <label>Estatus:<span id="spn_worOrdStat"></span></label>
            </div>
            <div>
              <label id="sel_workOrdStat_error" class="div-errorVal"/>
            </div>
          </div>
					<div id="div_worOrdFact">
						<label>¿Fue factible?<label>
						<input type="radio" name="rad_workOrdFact" value="1" checked>Sí</input>
						<input type="radio" name="rad_workOrdFact" value="2">No</input>
						<input type="radio" name="rad_workOrdFact" value="3">Sí, solo con adecuaciones</input>
					</div>
          
					<div id="div_worOrdStatAdecList">
						<label>Cantidad:
              <input type="text" id="txt_adecCant" name="txt_adecCant" class="form-control" placeholder="" maxlength="4" size="3">
            </label>
						<label>Adecuaciones:<span id="spn_worOrdStatAdecList"></span></label>
						<input type="button" class="btn btn-primary" id="btn_addAdec" value="Agregar a la lista" onClick="addAdecToOrder(0,'');">
						<table id="tbl_selectedAdec" class="table table-hover table-striped">
							<thead>
								<tr>
									<td>Cantidad</td>
									<td>Descripcion</td>
									<td>Acciones</td>
								</tr>
							</thead>
							<tbody>                 
							</tbody>
            </table> 
					</div>	
          		
			<!--	</div>   --> 
<?php }?>				
				  
				<input type="hidden" id="hdn_worOrdAction" name="hdn_worOrdAction" class="form-control" value=-1>
				<input type="hidden" id="hdn_worOrdId" name="hdn_worOrdId" class="form-control" value=-1>
        <input type="hidden" id="hdn_worOrdPricId" name="hdn_worOrdPricId" class="form-control" value=-1>
			</div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
        <input type="submit" class="btn btn-primary" id="cargaEventCamp" value="Guardar Cambios" onClick="saveWorkOrderNew();">
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 

<!--_Cuadro de dialogo de insertar tarea-->
<div id="mod_newWorkOrderTask" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Nueva Tarea</h4>
      </div>
      <div class="container-fluid">
        <div id="div_msgAlertTask"></div>
      </div>
      <div class="modal-body"> 
        <form class="form-horizontal" id="frm_newWorkOrderTask" name="frm_newWorkOrderTask" role="form" method="post" enctype="multipart/form-data">
        <div class="row">
          <div class="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <h3>Datos Generales</h3>
            <div>
              <label>Título:
                <input type="text" id="txt_tareaTitulo" name="txt_tareaTitulo" class="form-control" placeholder="Título o nombre" maxlength="80" size="40" required>
              </label>  
            </div>
            <div>
              <label id="txt_tareaTitulo_error" class="div-errorVal"/>
            </div>
            <div>
              <label>Fecha de Inicio:
                <input type="text" id="txt_tareaFechaIni" name="txt_tareaFechaIni" class="form-control" placeholder="Fecha de inicio" maxlength="20" size="40" readonly required>
              </label>  
            </div>
            <div>
              <label id="txt_tareaFechaIni_error" class="div-errorVal"/>
            </div>          
            <br>
            <div>
              <label>Fecha de Fin:
                <input type="text" id="txt_tareaFechaFin" name="txt_tareaFechaFin" class="form-control" placeholder="Fecha de fin" maxlength="20" size="40" readonly required>
              </label>  
            </div>
            <div>
              <label id="txt_tareaFechaFin_error" class="div-errorVal"/>
            </div>                             
            <div>
              <label>Estatus:<span id="spn_tareaEstat"></span></label>
            </div>
            <div>
              <label id="sel_tareaEstat_error" class="div-errorVal"/>
            </div>
            <div>
              <label id="sel_tareaTipo_error" class="div-errorVal"/>
            </div>
            <div>
              <label>Descripción:
                <textarea id="txt_tareaDesc" name="txt_tareaDesc" class="form-control" placeholder="Agregue una descripción acerca de la tarea." maxlength="255" cols="40" rows="5" required></textarea>
              </label>
              <div>
                <label id="txt_tareaDesc_error" class="div-errorVal"/>
              </div>
            </div>      
          </div>
            <div class="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <h3>Personal y Equipo</h3>
              <div>
                <div><label>Usuarios Disponibles:</div>
                <div><span id="spn_tareaInv"></span></label><br>
                  <input type="button" id="btn_tareaAddGuest" class="btn btn-primary" onClick="addGuest('',0);" value="Agregar Invitado"></input>
                </div>
              </div>
              <div>
                <div class="table-responsive">
                  <table id="tbl_tareaSelectedGuest" class="table table-hover table-striped tbl_selectedContact">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Acciones</th>
                      </tr>
                    </thead>
                    <tbody>              
                    </tbody>
                  </table>
                </div>
              </div>
              <div>
                <label id="sel_tareaInv_error" class="div-errorVal"/>
              </div>
              <div>
                <div><label>Apartar Equipo:</label></div>
                <div>
                  <label>Categoría:<span id="spn_tareaEquiCat"></span><br>
                  <span id="spn_tareaEqu"></span>
                  <input type="button" id="btn_tareaAddEquip" class="btn btn-primary" onClick="addEquipment('',0);" value="Reservar Equipo"></input>
                </div>
              </div>
              <div>
                <div class="table-responsive">
                  <table id="tbl_tareaSelectedEquip" class="table table-hover table-striped tbl_selectedContact">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Equipo</th>
                        <th>Num. Serie</th>
                        <th>Acciones</th>
                      </tr>
                    </thead>
                    <tbody>              
                    </tbody>
                  </table>
                </div>
              </div>
              <div>
                <label id="sel_tareaEqui_error" class="div-errorVal"/>
              </div>
          </div>
        </div>
          <input type="hidden" id="hdn_tareaAction" name="hdn_tareaAction" class="form-control" value=-1>
          <input type="hidden" id="hdn_tareaId" name="hdn_tareaId" class="form-control" value=-1>         
      </div>    
      
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
        <input type="submit" class="btn btn-primary" id="cargaTaskCamp" value="Guardar Cambios" onClick="saveWorkOrderTaskNew();">
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 

<div class="container" id="commentEventDialog"></div><!--cuadro de dialogo para gestión de comentarios--> 
