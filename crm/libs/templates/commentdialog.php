<?php
  header('Content-Type: text/html; charset=UTF-8');
  date_default_timezone_set('America/Mexico_City');


  /*Estos son los cuadros de diálogo que despliegan los comentarios y permite la inseción de comentarios nuevos en cualquier modulo que lo necesite debe de setearse los valores de los campos hidden:
  
hdn_commentOriginId = id del cliente/prospecto/sitio etc
hdn_commentOriginType  = tipo de origen (0=prospecto, 1=cliente, 2=sitio)
  */
?>

<script type="text/javascript">
  $(document).ready(function(){
    $('#mod_comments').on('hide.bs.modal', function(){
      $("#commentProspectDialog").html("");
      $("#commentClientDialog").html("");
      $("#commentSiteDialog").html("");
      $("#commentEventDialog").html("");
      $("#commentConnDialog").html("");
      $("#commentTaskDialog").html("");
    });
  });  
</script>

<!--_Cuadro de dialogo de comentarios de prospecto -->
<div id="mod_comments" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Comentarios</h4>
      </div>
      <div class="container-fluid">
        <div id="div_msgAlertComment"></div>
      </div>
      <div class="panel panel-info small">  
      </div>
      <div class="modal-body" id="div_comments"></div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->  
