<?php
session_start();
set_time_limit(0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

include_once "../../libs/db/dbcommon.php";
include_once "../../libs/db/common.php";
include_once "../../libs/db/encrypt.php";
include_once "../../libs/db/session.php";

$gruposArr  = isset($_SESSION['grupos'])?$_SESSION['grupos']:"";
$usuarioId  = isset($_SESSION['usrId'])?$_SESSION['usrId']:"";
$usuario    = isset($_SESSION['usuario'])?$_SESSION['usuario']:"";
$nombre     = isset($_SESSION['usrNombre'])?$_SESSION['usrNombre']:"";
$usrUbica   = isset($_SESSION['usrUbica'])?$_SESSION['usrUbica']:"";
$nIDUsr     = isset($_SESSION['nIDUsuario'])?$_SESSION['nIDUsuario']:"";

$finalcount = date("i:s",30);
$fechainicio = $_SESSION['loginFecha'];

$semana = array("","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado","Domingo");
$meses  = array("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

$option = isset($_POST['option'])?$_POST['option']:-1;
$modId = isset($_POST['orgType'])?$_POST['orgType']:-1;

if("getDateTime"==$_POST['action']){
  $dia    = $semana[date('N')].", ".date('d')." de ".$meses[date('n')]." del ".date('Y');
  $hora   = date("H:i:s");
  $salida = "<p class=\"datetimedisplay\">".$dia."&nbsp;&nbsp;".$hora."</p>";
  echo $salida;
}

if("verifySession"==$_POST['action']){
  // logoutTimeout();
  $opt = isset($_POST['opt'])?$_POST['opt']:-1;
  $fechaactual  = date("Y-m-d H:i:s");
  
  if(!verifySession($_SESSION)){
    logoutTimeout();
    $salida = "0||La sesión ha expirado||";
  }
  else{
    if(1==$opt){
      
      $fechafin = strtotime("+".$timeoutTime." second",strtotime($fechainicio));
      if($fechaactual>=$fechafin){
        logoutTimeout();
        $salida = "0||La sesión ha expirado";
      }
      else{
        $remtime = date("i:s",$fechafin-strtotime($fechaactual));
        if($finalcount>=$remtime){
          $salida = "1||La sesión está a punto de expirar||".$remtime;
        } 
        else{
          $salida = "4||La sesión sigue vigente||". $remtime;
        }     
      }
    }
    elseif(2==$opt){
      $_SESSION['loginFecha'] = $fechaactual;
      $salida = "3||Su sesión ha sido renovada";
    } 
    elseif(4==$opt){
      $salida = "4||La sesión sigue vigente||";
    }
  }
  echo $salida;
}

/*if("verifySession"==$_POST['action']){
  $fecha  = date("Y-m-d H:i:s");
  $status = false;
  
  if(null===session_id()||!isset($session['usrId'])){
    $result = false;
    log_write("ERROR: LOGIN: No existen variables de sesión",5);
  }
  else{
    $antes = strtotime("-".$timeoutTime." second",strtotime($fecha)); 
    $fecLog = strtotime($session['loginFecha']);
    if($fecLog<$antes){
      log_write("DEBUG: LOGIN: Se excedió el tiempo de timeout se va a desloguear",5);
      $status = false;
    }
    else{
      $status = true;
    }
  }
  return $status;
}*/








//btiene las minilistas con mensajes nuevos que aparecen con los botones de alertas
if("getAlert"==$_POST['action']){
  $msgsPrev = "";
  $db_modulos   = condb_modulos();
  
  switch($option){ 
    case "0": //alertas
      $msgTypeTxt = "nuevas alertas";
    break;
    case "1": // mensajes
      $msgTypeTxt = "nuevos mensajes";
    break;
    case "2": //advertencias
      $msgTypeTxt = "nuevas advertencias";
    break;   
    default:
    break;
  }
  
  $query = "SELECT mensaje_id, asunto, mensaje, fecha_hora, leido FROM mensajes WHERE destinatario_id=".$usuarioId." AND tipo_mensaje=".$option." ORDER BY fecha_hora  DESC LIMIT 5";
  
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $msgsPrev = "Error al obtener los msgs";
  }
  else{
    if(0>=mysqli_num_rows($result))
      $msgsPrev = "<i>No existen ".$msgTypeTxt."</i>";
    else{
      while($row = mysqli_fetch_assoc($result)){
        $msgsPrev .= "<li><a href=\"#\" style=\"text-decoration:none\"> "; 
        if(0==$row['leido']){              
          $msgsPrev .= "  <div class=\"small notification-item-unread\"><span class=\"glyphicon glyphicon-exclamation-sign\"></span> ";
        }
        else{
          $msgsPrev .= "  <div class=\"small notification-item\"> ";
        }
        $msgsPrev .= "    <span onClick=\"readMsg('".encrypt($row['mensaje_id'])."');\"><i>".$row['fecha_hora']."</i><br><b>".substr($row['asunto'],0,30)." :</b> <i>".substr($row['mensaje'],0,20)."...</i><br><br></span>".
                     "  </div>".   
                     "</a></li>";
      }
    }
  }
  mysqli_close($db_modulos);
  echo $msgsPrev;
}







//función que obtiene le número de mensajes sin leer para su despliegue en encabezado y otros lados  
if("getUnreadMsgNum"==$_POST['action']){
  $db_modulos   = condb_modulos();
  
  $query = "SELECT COUNT(mensaje_id) FROM mensajes WHERE destinatario_id=".$usuarioId." AND tipo_mensaje=".$option." AND leido=0";
  
  $result = mysqli_query($db_modulos,$query);
  
  if(!$result){
    $numMsgs = "<i><b>Error:</b>Ocurrió un problema al cargarse los mensajes no leídos.</i>";
  }
  else{
    $row = mysqli_fetch_row($result);
    if(0<$row[0]){
      $numMsgs = "YEP||<span class=\"label-notification label-danger blink\">".$row[0]."</span>";  
      
      /*if($_SESSION['numUnreadMsgs']<$row[0]){
        $numMsgs = "YEP||".$numMsgs;
      }
      else{
        $numMsgs = "NOPE||".$numMsgs;
      } */  
    } 
    else{
      $numMsgs = "NOPE||";
    }
    //$_SESSION['numUnreadMsgs']=$row[0];
  }
  mysqli_close($db_modulos);
  echo $numMsgs;
}

/*if("getWhatsHistory"==$_POST['action']){
  $db_omni = condb_onmidb();
  
  $orgType = isset($_POST['orgType'])?$_POST['orgType']:-1;
  $orgId = isset($_POST['orgId'])?$_POST['orgId']:-1;
  
  $query = "SELECT nidcliente FROM tbl_nidclientes WHERE tipo_origen=".$orgType." AND origen_id=".$orgId;
  
  $result = 
    
  mysqli_close($db_omni);
  echo $salida;
}

if("sendWhatsMsg"==$_POST['action']){
  $db_omni = condb_onmidb();
    
  $query = "INSERT INTO tbl_chat_conversacion (nIDUsuario,nIDCliente,nIDProcesar,Fecha,Hora,Origen,".
                                              "Mensaje,FechaModificacion,FechaCreacion,Observaciones,bEstado) ".
           "VALUES (".$nIDUsr.","..")";
  mysqli_close($db_omni);
  echo $salida;
}*/

?>
