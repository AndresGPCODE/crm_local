<?php
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

include_once "../db/common.php";

?>
<div id="mod_siteConnectionInsert" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Nuevo Enlace</h4>
      </div>
      <div class="container-fluid">
        <div id=div_msgAlertSiteConnection></div>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="frm_newSiteConnection" name="frm_newSiteConnection" role="form" method="post" enctype="multipart/form-data">                                      
          <div>
            <label>Nodo:
              <input type="text" id="txt_nodSitCon" name="txt_nodSitCon" class="form-control" placeholder="Nodo" maxlength="60" size="40" required>
            </label>
          </div>
          <div>
            <label id="txt_nodSitCon_error" class="div-errorVal"/>
          </div>
                    
          <div>
            <label>BW Download:
              <input type="tel" id="txt_downSitCon" name="txt_downSitCon" class="form-control" placeholder="BW Download" maxlength="10" size="10" required>
            </label>  
          </div>
          <div>
            <label id="txt_downSitCon_error" class="div-errorVal"></label>
          </div>
          <div>
            <label>BW Upload:
              <input type="tel" id="txt_upSitCon" name="txt_upSitCon" class="form-control" placeholder="BW Upload" maxlength="10" size="10" required>
            </label>  
          </div>
          <div>
            <label id="txt_upSitCon_error" class="div-errorVal"></label>
          </div>
          
          <div>
            <label>Topología:<span id="spn_conTop"></span></label>
          </div>
          <div>
            <label id="sel_conTop_error" class="div-errorVal"/>
          </div> 
          
          <div>
            <label>Fecha Instalación:
              <input type="text" id="txt_fecInsSitCon" name="txt_fecInsSitCon" class="form-control" placeholder="Fecha instalación" maxlength="40" size="40" required readonly>
            </label>
          </div>
          <div>
            <label id="txt_fecInsSitCon_error" class="div-errorVal"/>
          </div>
          
          <div>
            <label>Activo:<span id="spn_conAct"></span></label>
          </div>
          <div>
            <label id="sel_conAct_error" class="div-errorVal"/>
          </div> 
          
          <div>
            <label>DBM:
              <input type="text" id="txt_dbmSitCon" name="txt_dbmSitCon" class="form-control" placeholder="DBM" maxlength="40" size="40" required>
            </label>
          </div>
          <div>
            <label id="txt_dbmSitCon_error" class="div-errorVal"/>
          </div>
          
          <div>
            <label>Radiobase:
              <input type="text" id="txt_radSitCon" name="txt_radSitCon" class="form-control" placeholder="Radiobase" maxlength="40" size="40" required>
            </label>
          </div>
          <div>
            <label id="txt_radSitCon_error" class="div-errorVal"/>
          </div>
          
          <div>
            <label>Modelo Antena 1:
              <input type="text" id="txt_modAnt1SitCon" name="txt_modAnt1SitCon" class="form-control" placeholder="Modelo Antena 1" maxlength="50" size="40" required>
            </label>
            <label id="txt_modAnt1SitCon_error" class="div-errorVal"></label>
          </div>
          <div>            
            <label>Serie Antena 1:
              <input type="text" id="txt_serAnt1SitCon" name="txt_serAnt1SitCon" class="form-control" placeholder="Serie Antena 1" maxlength="50" size="40" required>
            </label>
            <label id="txt_serAnt1SitCon_error" class="div-errorVal"></label>
          </div> 
          
          <div>
            <label>Modelo Antena 2:
              <input type="text" id="txt_modAnt2SitCon" name="txt_modAnt2SitCon" class="form-control" placeholder="Modelo Antena 1" maxlength="50" size="40" required>
            </label>
            <label id="txt_modAnt2SitCon_error" class="div-errorVal"></label>
          </div>
          <div>    
            <label>Serie Antena 2:
              <input type="text" id="txt_serAnt2SitCon" name="txt_serAnt2SitCon" class="form-control" placeholder="Serie Antena 1" maxlength="50" size="40" required>
            </label>
            <label id="txt_serAnt2SitCon_error" class="div-errorVal"></label>
          </div>
          
          <div>
            <label>Modelo Router:
              <input type="text" id="txt_modRouSitCon" name="txt_modRouSitCon" class="form-control" placeholder="Modelo Router" maxlength="50" size="40" required>
            </label>
            <label id="txt_modRouSitCon_error" class="div-errorVal"></label>
          </div>
          <div>           
            <label>Serie Router:
              <input type="text" id="txt_serRouSitCon" name="txt_serRouSitCon" class="form-control" placeholder="Serie Router" maxlength="50" size="40" required>
            </label>
            <label id="txt_serRouSitCon_error" class="div-errorVal"></label>
          </div>  
                 
          <div>
            <label>Fecha Desinstalación:
              <input type="text" id="txt_fecDesSitCon" name="txt_fecDesSitCon" class="form-control" placeholder="Fecha Desinstalación" maxlength="40" size="40" required readonly>
            </label>
             <label><input type="checkbox" id="chk_fecDesSitCon" name="chk_fecDesSitCon" value="0">Agregar</label>
          </div>
          <div>
            <label id="txt_fecDesSitCon_error" class="div-errorVal"/>
          </div>                                                 
          <div>
            <label>Comentarios:
              <textarea id="txt_sitConDesc" name="txt_sitConDesc" class="form-control" placeholder="Agregue una breve descripción acerca del enlace." maxlength="255" cols="40" rows="5" required></textarea>
            </label>
            <div>
              <label id="txt_sitDesc_error" class="div-errorVal"/>
            </div>
          </div>
          <input type="hidden" id="hdn_siteConnOption" name="hdn_siteConnOption" class="form-control" value=-1>
          <input type="hidden" id="hdn_siteConId" name="hdn_siteConId" class="form-control" value=-1>  
          <!--<input type="hidden" id="hdn_contArrSitCon" name="hdn_contArrSitCon" class="form-control" value="">-->                         
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
        <input type="button" class="btn btn-primary" id="saveSitCon" value="Guardar Cambios" onClick="saveSiteConnectionNew();">
      </div>
         </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--_Cuadro de dialogo de lista de sitios asociados a una cotización (originalmente a sitio)-->
<div id="mod_priceConnList" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Enlaces</h4>
      </div>
      <div class="container-fluid">
        <div id=div_msgAlertPriceConnList></div>
      </div>
      <div class="modal-body">   
        <label>Seleccionar contrato: <br> 
        </label> 
        <div id="div_priceConnList"></div>                                                          
        
        <input type="hidden" name="hdn_cotId" id="hdn_cotId" value=-1 />
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
      </div>          
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 

<div class="container" id="contactConnDialog"></div><!--cuadro de dialogo para gestión de comentarios-->   
