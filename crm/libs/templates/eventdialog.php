<?php
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

/*Son los cuadros de diálogo para la visualización y para la inserción y edición de eventos nuevos al calendario desde la pagina de prospectos, clientes sitios etc. Funciona para cualquier página que lo invoque, siemrpe y cuando setee las variables ocultas adeucadamente*/
/*Si inserta o edita y si se trata de un prospecto, cliente etc están indicados en las variables ocultas:

hdn_eveAction: Si es editar o guardar nuevo
hdn_eveId: Id del evento para editar
hdn_orgType: Indica si será 0= prospecto, 1= cliente, 2= sitio etc
*/

?>
<script type="text/javascript">
  //borra el contenido del campo fecha fin en formulario de eventos si el campo fecha inicio está vacío
  $("#txt_fechaIni").on("blur",function(){
    if(""==$("#txt_fechaIni").val())
      $("#txt_fechaFin").val("");
  });
</script>

<style type="text/css">
  #slr_eveDurDia label{
    position: absolute;
    width: 20px;
    margin-left: -10px;
    text-align: center;
    margin-top: 20px;
  }
</style>

<!--_Cuadro de dialogo de visualización de eventos -->
<div id="mod_Events" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Eventos programados</h4>
      </div>
      <div class="container-fluid">
        <div id=div_msgAlertEvents></div>
      </div>
      <div class="modal-body row">  
        <div class="col container-fluid" id="div_Events"></div> 
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 

<!--_Cuadro de dialogo de insertar evento-->
<div id="mod_newEvent" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Nuevo Evento</h4>
      </div>
      <div class="container-fluid">
        <div id="div_msgAlertEvent"></div>
      </div>
      <div class="modal-body"> 
        <form class="form-horizontal" id="frm_newEvent" name="frm_newEvent" role="form" method="post" enctype="multipart/form-data">
        <div class="row">
          <div class="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <h3>Datos Generales</h3>
            <div>
              <label>Título:
                <input type="text" id="txt_eveTitulo" name="txt_eveTitulo" class="form-control" placeholder="Título o nombre" maxlength="80" size="40" required>
              </label>  
            </div>
            <div>
              <label id="txt_eveTitulo_error" class="div-errorVal"/>
            </div>
            <div>
              <label>Fecha de Inicio:
                <input type="text" id="txt_fechaIni" name="txt_fechaIni" class="form-control" placeholder="Fecha de inicio" maxlength="20" size="40" readonly required>
              </label>  
            </div>
            <div>
              <label id="txt_fechaIni_error" class="div-errorVal"/>
            </div>          
            <br>
            <div>
              <label>Fecha de Fin:
                <input type="text" id="txt_fechaFin" name="txt_fechaFin" class="form-control" placeholder="Fecha de fin" maxlength="20" size="40" readonly required>
              </label>  
            </div>
            <div>
              <label id="txt_fechaFin_error" class="div-errorVal"/>
            </div>                             
            <div>
              <label>Estatus:<span id="spn_eveEstat"></span></label>
            </div>
            <div>
              <label id="sel_eveEstat_error" class="div-errorVal"/>
            </div>
            <div>
              <label>Tipo de evento:<span id="spn_eveTipo"></span></label>
            </div>
            <div>
              <label id="sel_eveTipo_error" class="div-errorVal"/>
            </div>
            <div id="div_eveRadFact">
              <label>¿Fue factible este estudio?<label>
              <input type="radio" name="rad_eveFact" value=1 checked>Sí</input>
              <input type="radio" name="rad_eveFact" value=2>No</input>
            </div>
            <div>
              <label>Descripción:
                <textarea id="txt_eveDesc" name="txt_eveDesc" class="form-control" placeholder="Agregue una breve descripción acerca del evento." maxlength="255" cols="40" rows="5" required></textarea>
              </label>
              <div>
                <label id="txt_eveDesc_error" class="div-errorVal"/>
              </div>
            </div>         
          </div>
          
          <!--<div class="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<h3>Personal y Equipo</h3>
						<div>
							<div><label>Usuarios Disponibles:</div>
							<div><span id="spn_eveInv"></span></label><br>
								<input type="button" id="btn_eveAddGuest" class="btn btn-primary" onClick="addGuest('',0);" value="Agregar Invitado"></input>
							</div>
						</div>
						<div>
							<div class="table-responsive">
								<table id="tbl_eveSelectedGuest" class="table table-hover table-striped tbl_selectedContact">
									<thead>
										<tr>
											<th>ID</th>
											<th>Nombre</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>              
									</tbody>
								</table>
							</div>
						</div>
						<div>
							<label id="sel_eveInv_error" class="div-errorVal"/>
						</div>
						<div>
							<div><label>Apartar Equipo:</label></div>
							<div>
								<label>Categoría:<span id="spn_eveEquiCat"></span><br>
								<span id="spn_eveEqu"></span>
								<input type="button" id="btn_eveAddEquip" class="btn btn-primary" onClick="addEquipment('',0);" value="Reservar Equipo"></input>
							</div>
						</div>
						<div>
							<div class="table-responsive">
								<table id="tbl_eveSelectedEquip" class="table table-hover table-striped tbl_selectedContact">
									<thead>
										<tr>
											<th>ID</th>
											<th>Equipo</th>
											<th>Num. Serie</th>
											<th>Acciones</th>
										</tr>
									</thead>
									<tbody>              
									</tbody>
								</table>
							</div>
						</div>
						<div>
							<label id="sel_eveEqui_error" class="div-errorVal"/>
						</div>        
          </div> --> 
        </div>
          <input type="hidden" id="hdn_eveAction" name="hdn_eveAction" class="form-control" value=-1>
          <input type="hidden" id="hdn_eveId" name="hdn_eveId" class="form-control" value=-1>         
      </div>
      
      
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
        <input type="submit" class="btn btn-primary" id="cargaEventCamp" value="Guardar Cambios" onClick="saveEventNew();">
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 

<div class="container" id="commentEventDialog"></div><!--cuadro de dialogo para gestión de comentarios-->   
