<?php
// header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

$usuarioId = $_SESSION['usrId'];

$picFile = glob("../../img/profilepics/72.*");

$picExt = pathinfo("../../img/profilepics/" . $picFile[0], PATHINFO_EXTENSION);

if (!file_exists("../../img/profilepics/72." . $picExt)) {
  $picUrl = "../../img/profilepics/default.jpg";
} else {
  $picUrl = "../../img/profilepics/72." . $picExt;
}

?>

<style type="text/css">
  body {
    padding-top: 100px;
    margin-bottom: 45px;
    background: -webkit-linear-gradient(#ebebeb, #ffffff, #ebebeb);
    /* Safari */
    background: -o-linear-gradient(#ebebeb, #ffffff, #ebebeb);
    /* Opera */
    background: -moz-linear-gradient(#ebebeb, #ffffff, #ebebeb);
    /* Firefox */
    background: linear-gradient(#ebebeb, #ffffff, #ebebeb);
    /* Chrome IE */
  }

  .navcoe1 {
    color: #000000;
    padding-right: 20px;
    background: -webkit-linear-gradient(#d1d2d7, #ffffff);
    /* Safari */
    background: -o-linear-gradient(#d1d2d7, #ffffff);
    /* Opera */
    background: -moz-linear-gradient(#d1d2d7, #ffffff);
    /* Firefox */
    background: linear-gradient(#d1d2d7, #ffffff);
    /* Chrome IE */
  }

  .nav>li.active>a,
  .navbar-inverse .navbar-nav>.open>a,
  .navbar-nav>.open>a:focus,
  .navbar-nav>.open>a:hover,
  .nav-tabs>li.active>a,
  .nav-tabs>li.active>a:focus,
  .nav-tabs>li.active>a:hover {
    color: #ffffff;
    background: -webkit-linear-gradient(#7d1317, #b10511);
    /* Safari */
    background: -o-linear-gradient(#7d1317, #b10511);
    /* Opera */
    background: -moz-linear-gradient(#7d1317, #b10511);
    /* Firefox */
    background: linear-gradient(#7d1317, #b10511);
    /* Chrome IE */
  }

  .nav .open>a,
  .nav .open>a:focus,
  .nav .open>a:hover {
    background-color: #eee;
    color: #ffffff;
    border-color: #a2a2a2;
  }

  .navbar-inverse .navbar-nav>li>a,
  .nav-tabs>li>a {
    color: #666;
  }

  .nav-tabs>li,
  .nav-pills>li {
    float: none;
    display: inline-block;
    *display: inline;
    /* ie7 fix */
    zoom: 1;
    /* hasLayout ie7 trigger */
  }

  .nav-tabs,
  .nav-pills {
    text-align: center;
  }

  /*.dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 0;
   }*/

  .nav-link,
    {
    background: -webkit-linear-gradient(#d1d2d7, #ffffff);
    /* Safari */
    background: -o-linear-gradient(#d1d2d7, #ffffff);
    /* Opera */
    background: -moz-linear-gradient(#d1d2d7, #ffffff);
    /* Firefox */
    background: linear-gradient(#d1d2d7, #ffffff);
    /* Chrome IE */
  }

  .navcoe2 {
    color: #eee;
    background: -webkit-linear-gradient(#ebebeb, #d1d2d7);
    /* Safari */
    background: -o-linear-gradient(#ebebeb, #d1d2d7);
    /* Opera */
    background: -moz-linear-gradient(#ebebeb, #d1d2d7);
    /* Firefox */
    background: linear-gradient(#ebebeb, #d1d2d7);
    /* Chrome IE */
  }

  .page-header {
    color: #7d1317;
    border-bottom: 1px solid #7d1317;
  }

  html {
    position: relative;
    min-height: 100%;
  }

  @media(min-width: 768px) {
    .main {
      padding-right: 40px;
      padding-left: 40px;
    }
  }

  @media(max-width: 768px) {
    .main {
      padding-right: 10px;
      padding-left: 10px;
    }

    .btn-responsive {
      padding: 4px 9px;
      font-size: 90%;
      line-height: 1.2;
    }

    .chk_filtersTick,
    .chk_filters,
    .chk_filtersUser {
      width: 17px;
      height: 17px;
      font-size: 90%;
      line-width: 1.2;
    }

    .rad_responsive {
      width: 17px;
      height: 17px;
      font-size: 90%;
      line-width: 1.2;
    }

    div.ui-datepicker {
      font-size: 16px;
    }

    .ui-slider .ui-slider-handle {
      width: 1.8em;
      height: 2em;
      margin-left: -0.9em;
    }

    .ui-slider-horizontal {
      height: 1.7em;
    }
  }

  .main .page-header {
    margin-top: 0;
  }

  .navbar-fixed-top {
    border: 0;
  }

  .modal-header {
    background: #9e9e9e;
    color: #666;
    border-top-left-radius: 15px;
    border-top-right-radius: 15px;
    background: -webkit-linear-gradient(#ebebeb, #d1d2d7);
    /* Safari */
    background: -o-linear-gradient(#ebebeb, #d1d2d7);
    /* Opera */
    background: -moz-linear-gradient(#ebebeb, #d1d2d7);
    /* Firefox */
    background: linear-gradient(#ebebeb, #d1d2d7);
    /* Chrome IE */
  }

  .footer {
    padding-top: 10px;
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 45px;
    background-color: #f5f5f5;
  }

  textarea {
    resize: none;
  }

  .div-errorVal {
    color: #ff0000;
  }

  .header-text {
    color: #666;
    padding-top: 20px;
  }

  .modal.modal-wide .modal-dialog {
    width: auto;
  }

  .label-notification {
    position: absolute;
    top: -9px;
    font-weight: bold;
    text-align: center;
    font-size: 12px;
    padding: 2px;
    color: #ffffff;
  }

  .td-blue {
    background: #bce8f1;
    color: #31708f;
  }

  .modal {
    overflow-y: auto;
  }

  .modal-open {
    overflow: auto;
  }

  .div_filters {
    background-color: #ffffff;
    border-style: solid;
    border: 1px solid #999;
    border-radius: 3px;
    /*background-color:#78E389;  */
  }

  .img-responsive {
    display: block;
    height: auto;
    max-width: 100%;
  }

  #div_doc_prev {
    height: 200px;
    position: relative;
    overflow: hidden;
  }

  .white-cont {
    background: #ffffff;
    padding-right: 40px;
    padding-left: 40px;
    padding-top: 20px;
    border-radius: 3px;
  }

  .sidebar-profile1 {
    background: #c17d7d;
    color: #ffffff;
    min-height: 150px;
    max-height: 800px;
    height: auto;
    border-color: #b10511;
    border-style: solid;
    border-width: 2px;
    overflow-y: auto;
    overflow-x: hidden;
    border-radius: 5px;
  }

  .nodatatablerowdark {
    background: #eee;
    border-color: #101010;
    word-break: break-word;
    vertical-align: top;
  }

  .nodatatablerowlight {
    word-break: break-word;
    vertical-align: top;
  }

  .nopadding {
    padding: 0 !important;
    margin: 0 !important;
  }

  .notifications {
    border-color: #9d9d9d;
    /*background: -webkit-linear-gradient(#eee,#ffffff);*/
    min-width: 200px;
    max-width: 400px;
    overflow-y: auto;
    overflow-x: hidden;
  }

  .notification-item {
    color: #000;
    border-color: #101010;
    background: #ffffff;
  }

  .notification-item-unread {
    color: #000;
    border-color: #101010;
    background: #eee;
  }

  .btn_primary {
    color: #ffffff;
    background: -webkit-linear-gradient(#ebebeb, #d1d2d7);
    /* Safari */
    background: -o-linear-gradient(#ebebeb, #d1d2d7);
    /* Opera */
    background: -moz-linear-gradient(#ebebeb, #d1d2d7);
    /* Firefox */
    background: linear-gradient(#ebebeb, #d1d2d7);
    /* Chrome IE */
  }

  .btn-circle-default {
    border: 3px solid #a2a2a2;
    padding: 2px 0;
    border-radius: 35px;
    color: #000;
    background: -webkit-linear-gradient(#c6c1c5, #9e9e9e);
    /* Safari */
    background: -o-linear-gradient(#c6c1c5, #9e9e9e);
    /* Opera */
    background: -moz-linear-gradient(#c6c1c5, #9e9e9e);
    /* Firefox */
    background: linear-gradient(#c6c1c5, #9e9e9e);
    /* Chrome IE */
  }

  .btn-circle-alert {
    border: 3px solid #660000;
    padding: 2px 0;
    border-radius: 35px;
    color: #ffffff;
    background: -webkit-linear-gradient(#b30000, #ff4d4d);
    /* Safari */
    background: -o-linear-gradient(#b30000, #ff4d4d);
    /* Opera */
    background: -moz-linear-gradient(#b30000, #ff4d4d);
    /* Firefox */
    background: linear-gradient(#b30000, #ff4d4d);
    /* Chrome IE */
  }

  .btn-circle-warning {
    border: 3px solid #b36b00;
    padding: 2px 0;
    border-radius: 35px;
    color: #ffffff;
    background: -webkit-linear-gradient(#cc7a00, #ffad33);
    /* Safari */
    background: -o-linear-gradient(#cc7a00, #ffad33);
    /* Opera */
    background: -moz-linear-gradient(#cc7a00, #ffad33);
    /* Firefox */
    background: linear-gradient(#cc7a00, #ffad33);
    /* Chrome IE */
  }

  .btn-circle-notif {
    border: 3px solid #113d6e;
    padding: 2px 0;
    border-radius: 35px;
    color: #ffffff;
    background: -webkit-linear-gradient(#2379db, #65a1e6);
    /* Safari */
    background: -o-linear-gradient(#2379db, #65a1e6);
    /* Opera */
    background: -moz-linear-gradient(#2379db, #65a1e6);
    /* Firefox */
    background: linear-gradient(#2379db, #65a1e6);
    /* Chrome IE */
  }

  .ul-custom1>li:before {
    font-family: 'FontAwesome';
    color: #7d1317;
    content: '\f069';
    margin: 0 5px 0 -15px;
  }

  .form-signing-round {
    /*#ebebeb,#d1d2d7*/
    border: 1px solid #d1d2d7;
    border-radius: 30px;
    padding: 30px;
    padding-left: 30px;
    padding-right: 30px;
    background: -webkit-linear-gradient(#ffffff, #eee);
    /* Safari */
    background: -o-linear-gradient(#ffffff, #eee);
    /* Opera */
    background: -moz-linear-gradient(#ffffff, #eee);
    /* Firefox */
    background: linear-gradient(#ffffff, #eee);
    /* Chrome IE */
  }

  .panelCoe1 {
    /*#ebebeb,#d1d2d7*/
    border: 1px solid #d1d2d7;
    border-radius: 10px;
    padding-left: 30px;
    padding-right: 30px;
    padding-bottom: 30px;
    word-wrap: break-word;
    background: -webkit-linear-gradient(#ffffff, #eee);
    /* Safari */
    background: -o-linear-gradient(#ffffff, #eee);
    /* Opera */
    background: -moz-linear-gradient(#ffffff, #eee);
    /* Firefox */
    background: linear-gradient(#ffffff, #eee);
    /* Chrome IE */
  }

  .row.row-eq-height {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
  }

  .tab {
    display: inline-block;
    padding: 1em;
    color: #ffffff;
    margin-bottom: 0px;
    background: -webkit-linear-gradient(#c6c1c5, #9e9e9e);
    /* Safari */
    background: -o-linear-gradient(#c6c1c5, #9e9e9e);
    /* Opera */
    background: -moz-linear-gradient(#c6c1c5, #9e9e9e);
    /* Firefox */
    background: linear-gradient(#c6c1c5, #9e9e9e);
    /* Chrome IE */
    /*transform: perspective(8px) rotateX(3deg);*/
    border-radius: 10px 10px 0 0;
  }

  .stat-table-div {
    min-height: 200px;
    overflow-y: auto;
    overflow-x: hidden;
    border: 1px solid #d1d2d7;
    margin: 0px;
    background: -webkit-linear-gradient(#ffffff, #eee);
    /* Safari */
    background: -o-linear-gradient(#ffffff, #eee);
    /* Opera */
    background: -moz-linear-gradient(#ffffff, #eee);
    /* Firefox */
    background: linear-gradient(#ffffff, #eee);
    /* Chrome IE */
  }

  .col-no-padding {
    padding-left: 0 !important;
    padding-right: 0 !important;
  }

  .panel-comm-info {
    display: inline-block;
    overflow-wrap: break-word;
    word-break: break-all;
    word-break: break-word;
    word-wrap: break-word;
    height: 380px;
    overflow: auto;
    hyphens: auto;
  }

  .table>thead>tr>th,
  .table>thead:first-child>tr:first-child>th {
    vertical-align: bottom;
    border-bottom: 2px solid #ddd;
    color: #ffffff;
    background: -webkit-linear-gradient(#c6c1c5, #9e9e9e);
    /* Safari */
    background: -o-linear-gradient(#c6c1c5, #9e9e9e);
    /* Opera */
    background: -moz-linear-gradient(#c6c1c5, #9e9e9e);
    /* Firefox */
    background: linear-gradient(#c6c1c5, #9e9e9e);
    /* Chrome IE */
  }

  .blink {
    -webkit-animation: blink .75s linear infinite;
    -moz-animation: blink .75s linear infinite;
    -ms-animation: blink .75s linear infinite;
    -o-animation: blink .75s linear infinite;
    animation: blink .75s linear infinite;
  }

  .map {
    height: 400px;
    width: 100%;
  }

  @-webkit-keyframes blink {
    0% {
      opacity: 1;
    }

    50% {
      opacity: 1;
    }

    50.01% {
      opacity: 0;
    }

    100% {
      opacity: 0;
    }
  }

  @-moz-keyframes blink {
    0% {
      opacity: 1;
    }

    50% {
      opacity: 1;
    }

    50.01% {
      opacity: 0;
    }

    100% {
      opacity: 0;
    }
  }

  @-ms-keyframes blink {
    0% {
      opacity: 1;
    }

    50% {
      opacity: 1;
    }

    50.01% {
      opacity: 0;
    }

    100% {
      opacity: 0;
    }
  }

  @-o-keyframes blink {
    0% {
      opacity: 1;
    }

    50% {
      opacity: 1;
    }

    50.01% {
      opacity: 0;
    }

    100% {
      opacity: 0;
    }
  }

  @keyframes blink {
    0% {
      opacity: 1;
    }

    50% {
      opacity: 1;
    }

    50.01% {
      opacity: 0;
    }

    100% {
      opacity: 0;
    }
  }

  /*table.dataTable tbody td {
    word-break: break-word;
    vertical-align: top;
  }*/
</style>
<script src="/crm/crm/libs/templates/script.js" type="text/javascript"></script>
<nav class=" navbar-inverse navbar-fixed-top navcoe1" id="nav_mainNavigation">

  <div class="container-fluid row navcoe1">
    <div class="navbar-header col col-2 col-md-2 col-lg-2">
      <!--Logo-->
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#title_navbarOptions" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Cambiar Navegación</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand hidden-sm hidden-xs" href="/crm/crm/modulos/main/"><img alt="Coeficiente" width="200" src="/crm/crm/img/logo_coe.png"></a>
    </div>
    <div id="title_navbarOptions" class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-left">
        <li id="li_navBar_home" class=""><a href="/crm/crm/modulos/main/"><span class="glyphicon glyphicon-home"></span> Principal</a></li>
        <li id="li_navBar_dashboard"><a href="/crm/crm/modulos/dashboard/"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>
        <?php if (hasPermission(3, 'r') || hasPermission(3, 'l')) { ?>
          <li id="li_navBar_calendario"><a href="/crm/crm/modulos/calendario/"><span class="glyphicon glyphicon-calendar"></span> Calendario</a></li>
        <?php } ?>
        <?php if (hasPermission(0, 'r') || hasPermission(0, 'l')) { ?>
          <li id="li_navBar_prospectos"><a href="/crm/crm/modulos/prospectos/"><span class="glyphicon glyphicon-list"></span> Prospectos</a></li>
        <?php } ?>
        <?php if (hasPermission(1, 'r') || hasPermission(1, 'l')) { ?>
          <li id="li_navBar_clientes"><a href="/crm/crm/modulos/clientes/"><span class="glyphicon glyphicon-list-alt"></span> Clientes</a></li>
        <?php } ?>
        <?php if (hasPermission(2, 'r') || hasPermission(2, 'l')) { ?>
          <li id="li_navBar_sitios"><a href="/crm/crm/modulos/sitios/"><span class="glyphicon glyphicon-map-marker"></span> Sitios</a></li>
        <?php } ?>

        <!-- menu desplegable de soporte aqui iran las vistas de los correos respondidos por los clientes -->
        <!--  hasPermission(numero, 'letra') esta funcion determinasi los perfiles del usuario tienen los parmisos de acceso necesarios -->
        <!--  -->
        <?php if (hasPermission(5, 'r') || hasPermission(5, 'l') || hasPermission(6, 'r') || hasPermission(6, 'l') || hasPermission(9, 'r') || hasPermission(9, 'l')) { ?>
          <li id="li_navBar_soporte" class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> Soporte<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <?php if (hasPermission(6, 'r') || hasPermission(6, 'l')) { ?>
                <li><a href="/crm/crm/modulos/tickets/"><span class="glyphicon glyphicon-alert"> Tickets</a></li>
              <?php } ?>
              <!-- modulo de prospectos atrasados -->
              <?php if (hasPermission(6, 'r') || hasPermission(6, 'l')) { ?>
                <li><a href="/crm/crm/modulos/prospectosatr/"><span class="glyphicon glyphicon-list-alt"> Reportes</a></li>
              <?php } ?>
              <!-- fin de modulo de prospectos atrasados -->
              <?php if (hasPermission(5, 'r') || hasPermission(5, 'l')) { ?>
                <li><a href="/crm/crm/modulos/servicios/"><span class="glyphicon glyphicon-briefcase"> Servicios</a></li>
              <?php } ?>
              <?php if (hasPermission(9, 'r') || hasPermission(9, 'l')) { ?>
                <li><a href="/crm/crm/modulos/instalaciones/"><span class="glyphicon glyphicon-tasks"> Instalaciones</a></li>
              <?php } ?>
              <?php if (hasPermission(9, 'r') || hasPermission(9, 'l')) { ?>
                <li><a href="/crm/crm/modulos/ordenestrabajo/"><span class="glyphicon glyphicon-tasks"> Órdenes de Trabajo</a></li>
              <?php } ?>
            </ul>
          </li>
        <?php } ?>
        <?php if (hasPermission(7, 'r') || hasPermission(7, 'l')) { ?>
          <li id="li_navBar_facturacion"><a href="/crm/crm/modulos/facturacion/"><span class="glyphicon glyphicon-briefcase"></span> Facturación</a></li>
        <?php } ?>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a id="btn_warnMenuIcon" href="#" class="btn btn-circle-default" data-toggle="dropdown" role="button" aria-expanded="false" onclick="getAlert('warnList','2');">
            <span id="spn_warnMenuIcon" class="glyphicon glyphicon-bell"></span>
          </a>
          <ul class="dropdown-menu" role="menu" style="padding:20px">
            <li><a href="#" disabled><b>Advertencias</b></a></li>
            <li class="divider"></li>
            <div id="div_warnList">
            </div>
            <li><a href="/crm/modulos/perfil/inbox.php?msgType=<?php echo encrypt(2); ?>">
                <span class="menu-title">Ver todas las advertencias <i class="glyphicon glyphicon-circle-arrow-right"></i></span>
              </a></li>
          </ul>
        </li>

        <li class="dropdown">
          <a id="btn_alertMenuIcon" href="#" class="btn btn-circle-default" data-toggle="dropdown" role="button" aria-expanded="false" onclick="getAlert('alertList','0');">
            <span id="spn_alertMenuIcon" class="glyphicon glyphicon-alert"></span>
          </a>
          <ul class="dropdown-menu" role="menu" style="padding:20px">
            <li><a href="#" disabled><b>Alertas</b></a></li>
            <li class="divider"></li>
            <div id="div_alertList">
            </div>
            <li><a href="/crm/modulos/perfil/inbox.php?msgType=<?php echo encrypt(0); ?>">
                <span class="menu-title">Ver todas las alertas <i class="glyphicon glyphicon-circle-arrow-right"></i></span>
              </a></li>
          </ul>
        </li>

        <li class="dropdown">
          <a id="btn_notifMenuIcon" href="#" class="btn btn-circle-default" data-toggle="dropdown" role="button" aria-expanded="false" onclick="getAlert('notifList','1');">
            <span id="spn_notifMenuIcon" class="glyphicon glyphicon-envelope"></span>
          </a>
          <ul class="dropdown-menu" role="menu" style="padding:20px">
            <li><a href="#" disabled><b>Mensajes</b></a></li>
            <li class="divider"></li>
            <div id="div_notifList">
            </div>
            <li><a href="/crm/modulos/perfil/inbox.php?msgType=<?php echo encrypt(1); ?>">
                <span class="menu-title">Ver todos los mensajes <i class="glyphicon glyphicon-circle-arrow-right"></i></span>
              </a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>

  <div class="container-fluid row navcoe2">
    <div class="hidden-xs col-2 col-sm-2 col-lg-3 header-text" align="left" id="div_dateTime"></div>
    <div class="hidden-xs col-2 col-sm-2 col-lg-6 header-text" align="center"><b><span style="color:#4c84b3">Bienvenido/a:</span> <?php echo $nombre; ?></b></div>
    <div class="col-8 col-sm-8 col-lg-3" align="right">
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown" id="li_navBar_perfil">
          <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            <img src="<?php echo $picUrl; ?>" alt="<?php echo $usuario ?>" height="25" width="25" class="img-circle">
            <?php echo $usuario ?>
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/crm/crm/modulos/perfil/index.php?id='<?php echo encrypt($usuarioId) ?>'"><span class="glyphicon glyphicon-user"> Perfil</a></li>
            <?php if ((array_key_exists(4, $gruposArr)) || (array_key_exists(1, $gruposArr)) || (array_key_exists(2, $gruposArr)) || (array_key_exists(11, $gruposArr))) { ?>
              <li><a href="/crm/crm/modulos/herramientas/"><span class="glyphicon glyphicon-wrench"> Herramientas</a></li>
            <?php } ?>
            <li class="divider"></li>
            <!--<li class="dropdown-header">Nav header</li>-->
            <li><a href="/crm/crm/modulos/login/logout.php?sub=logout"><span class="glyphicon glyphicon-log-out"> Salir</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>

  <body>
    <div id="mod_msgDetails" class="modal fade" data-backdrop="static" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Mensaje</h4>
          </div>
          <div class="modal-body">
            <div class="col container-fluid" id="div_msgBody"></div>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div id="mod_renewSession" class="modal" data-backdrop="static" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Sesión a punto de expirar</h4>
          </div>
          <div class="modal-body">
            <div class="col container-fluid" id="div_msgBodyrenewSession">Su sesión está a punto de expirar. Se perderá todo trabajo no guardado. Puede prolongar el tiempo de sesión haciendo click en el botón "Renovar Sesión"<br>
              Tiempo para terminar la sesión:<span id="spn_remTime"></span>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            <input type="submit" class="btn btn-primary" id="btn_renewSession" value="Renovar Sesión" onClick="verifySession(2);">
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  </body>

</nav>