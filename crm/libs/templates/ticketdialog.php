<?php
  include_once "../db/common.php";
?>

<!--_Cuadro de dialogo de insertar/editar ticket -->
<div id="mod_ticketInsert" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Nuevo Ticket</h4>
      </div>
      <div class="container-fluid">
        <div id=div_msgAlertTicket></div>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="frm_newTicket" name="frm_newTicket" role="form" method="post" enctype="multipart/form-data">
        <div>
          <label>Origen:<span id="spn_tickOrg"></span></label><br>
        </div>  
        <div>
          <label id="sel_ticketOrg_error" class="div-errorVal"/>
        </div>
        <div id="div_tickOrgOther">
          <div>
            <label>Otro Origen:
              <input type="text" id="txt_tickOrgOther" name="txt_tickOrgOther" class="form-control" placeholder="Otro origen" maxlength="255" size="80" required>
            </label>
          </div>
          <div>
            <label id="txt_tickOrgOther_error" class="div-errorVal"/>
          </div>
        </div>
        <div>
          <label>Topic:<span id="spn_tickTopic"></span></label><br>
        </div>
        <div>
          <label id="sel_ticketTopic_error" class="div-errorVal"/>
        </div>
        <div id="div_tickTopicOther">
          <div>
            <label>Otro Topic:
              <input type="text" id="txt_tickTopicOther" name="txt_tickTopicOther" class="form-control" placeholder="Otro topic" maxlength="255" size="80" required>
            </label>
          </div>
          <div>
            <label id="txt_tickTitle_error" class="div-errorVal"/>
          </div>
        </div>
        <div>
          <label>Departamento:<span id="spn_tickDepartment"></span></label><br>
        </div>
        <div>
          <label id="sel_ticketDepartment_error" class="div-errorVal"/>
        </div>
        <div>
          <label>SLA:<span id="spn_tickSla"></span></label><br>
        </div>
        <div>
          <label id="sel_ticketSla_error" class="div-errorVal"/>
        </div>
          
<?php if(hasPermission(6,'t')){?> 
        <div>         
          <label>Asignar a:<span id="spn_tickAssign"></span></label>
          <label id="lbl_ticketReassign"><input type="checkbox" id="chk_ticketReassign" name="chk_ticketReassign" title="Active si requiere que el ticket se asigne a otra persona o departamento"/>&nbsp;&nbsp;Reasignar</label>
        </div>
        <div>
          <label id="sel_tickAssign_error" class="div-errorVal"/>
        </div>
<?php
}
?>         
        <div>
          <label>Título:
            <input type="text" id="txt_tickTitle" name="txt_tickTitle" class="form-control" placeholder="Título" maxlength="255" size="80" required>
          </label>
        </div>
        <div>
          <label id="txt_tickTitle_error" class="div-errorVal"/>
        </div>
        <div id="div_tickDetailsDiv">  
          <label>Detalles del Inconveniente:
            <div id="div_tickDetails"></div>
          </label>
        </div>
        <div>
          <label id="div_tickDetails_error" class="div-errorVal"/>
        </div>
        <div>
          <label>Prioridad:<span id="spn_tickPriority"></span></label>
        </div>
        <div>
          <label id="sel_ticketPrior_error" class="div-errorVal"/>
        </div>
        <div>
          <input type="checkbox" id="chk_ticketClientNotify" name="chk_ticketClientNotify"/>&nbsp;&nbsp;<label>Enviar Notificación (insertar asociado)</label>
        </div>
      </div>
      <input type="hidden" id="hdn_tickId" name="hdn_tickId" class="form-control" value="0">
      <input type="hidden" id="hdn_siteId" name="hdn_siteId" class="form-control" value=-1>
      <input type="hidden" id="hdn_ticketInsertAction" name="hdn_ticketInsertAction" class="form-control" value=-1>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
        <input type="submit" class="btn btn-primary" id="cargaTicket" value="Guardar Cambios" onClick="saveTicketNew();">
      </div>
        </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 

<!--Cuadro de dialogo para inserción/edición de nota interna-->
<div id="mod_ticketNote" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Insertar Nota Interna</h4>
      </div>
      <div class="container-fluid">
        <div id=div_msgAlertTicketNote></div>
      </div>
      <div class="modal-body">
        <div>  
          <label>Nota:
            <div id="div_tickNote"></div>
          </label>   
        </div>
        <input type="hidden" id="hdn_tickNoteAction" value=0></input>
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
        <input type="submit" class="btn btn-primary" id="publicaNota" value="Guardar Cambios" onClick="saveTickNoteNew();">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--Cuadro de dialogo para inserción y envío de respuesta-->
<div id="mod_ticketReply" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Enviar respuesta</h4>
      </div>
      <div class="container-fluid">
        <div id=div_msgAlertTicketReply></div>
      </div>
      <div class="modal-body">
        <div>  
            <div id="div_tickAssoc"></div>
          </label>   
        </div>
        <div>  
          <label>Nota:
            <div id="div_tickReply"></div>
          </label>   
        </div>
        <div><br>
          <!--<input type="file" name="fl_tickReplyAttach" id="fl_tickReplyAttach" multiple='true' />
          <button type="button" id="btn_ticketIncrAtt" name="btn_ticketIncrAtt" title="Agregar más datos adjuntos">
            <span class="glyphicon glyphicon-plus"></span>
          </button>-->
        </div>       
        <input type="hidden" id="hdn_tickReplyAction" value=0></input>
      </div>  
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
        <input type="submit" class="btn btn-primary" id="publicaRespuesta" value="Guardar Cambios" onClick="saveTickReplyNew();">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--Cuadro de dialogo para cierre de ticket-->
<div id="mod_ticketClose" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="h4_ticketCloseTitle">Cerrar Ticket</h4>
      </div>
      <div class="container-fluid">
        <div id=div_msgAlertTicketClose></div>
      </div>
      <div class="modal-body">
        <div>  
          <label>Comentarios o motivo:
            <div id="div_tickCloseComments"></div>
          </label>
        </div>
        <input type="hidden" id="hdn_tickCloseAction" value="0"></input>
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
        <input type="submit" class="btn btn-primary" id="cancelaTicket" value="Guardar Cambios" onClick="ticketClose();">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 

<!--Cuadro de dialogo de inserción/edición de asociado-->
<div id="mod_assocInsert" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Nuevo Asociado</h4>
      </div>
      <div class="container-fluid">
        <div id=div_msgAlertTicketAssoc></div>
      </div>
      <div class="modal-body">
        <div>
          <label>Correo Electrónico de Notificación:
            <input type="text" id="txt_tickAssocEmail" name="txt_tickAssocEmail" class="form-control" placeholder="ejemplo@email.com" maxlength="255" size="80" required>
          </label>
        </div>
        <div>
          <label id="txt_tickAssocEmail_error" class="div-errorVal"/>
        </div>
        <div>  
          <label>Nombre del Notificado:
            <input type="text" id="txt_tickAssocName" name="txt_tickAssocName" class="form-control" placeholder="nombre" maxlength="255" size="80" required>
          </label>
        </div> 
        <div>
          <label id="txt_tickAssocName_error" class="div-errorVal"/>
        </div>
        <input type="hidden" id="hdn_tickAssocId" value=-1></input>
      </div>  
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
        <input type="submit" class="btn btn-primary" id="guardaAsociado" value="Guardar Asociado" onClick="saveAssocNew();">
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
