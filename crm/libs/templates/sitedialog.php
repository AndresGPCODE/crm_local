<?php
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

include_once "../db/common.php";

?>
<script type="text/javascript">
  $(document).ready(function(){
    $('#mod_sites').on('hide.bs.modal', function(){
      $("#div_sites").html("");
    });
  });  
</script>

<div id="mod_sites" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Sitios Asociados</h4>
      </div>
      <div class="container-fluid">
        <div id=div_msgAlertsSites></div>
      </div>
      <div class="modal-body row"> 
        <div class="col container-fluid" id="div_sites"></div> 
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 

<!--_Cuadro de dialogo de detalles de sitio -->
<div id="mod_siteDetails" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Detalles de Sitio</h4>
      </div>
      <div class="modal-body row">  
        <div class="col container-fluid" id="div_siteDetails"></div> 
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 

<?php   
  if(hasPermission(2,'w')){ ?>  
<!--_Cuadro de dialogo de insertar sitio -->
<div id="mod_siteInsert" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Nuevo Sitio</h4>
      </div>
      <div class="container-fluid">
        <div id="div_msgAlertSite"></div>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="frm_newSite" name="frm_newSite" role="form" method="post" enctype="multipart/form-data">
        <div class="row">
          <div class="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div>
              <label>Nombre (sucursal)(*):
                <input type="text" id="txt_nomSit" name="txt_nomSit" class="form-control" placeholder="Nombre (sucursal)" maxlength="60" size="40" required>
              </label>
              <input type="button" class="btn btn-primary" id="btn_clearSiteData" value="Limpiar Datos" onClick="clearSiteFields();">
            </div>
            <div>
              <label id="txt_nomSit_error" class="div-errorVal"/>
            </div>
            <div>
              <label>Teléfono(*):
                <input type="tel" id="txt_telSit" name="txt_telSit" class="form-control" placeholder="10 dígitos" maxlength="10" size="10" required>
              </label>  
            </div>
            <div>
              <label id="txt_telSit_error" class="div-errorVal"></label>
            </div>
            <div>
              <label>Domicilio(*):
                <input type="text" id="txt_domSit" name="txt_domSit" class="form-control" placeholder="Domicilio" maxlength="40" size="40" required>
              </label>
            </div>
            <div>
              <label id="txt_domSit_error" class="div-errorVal"/>
            </div>
            <div>
              <label>Colonia(*):
                <input type="text" id="txt_colSit" name="txt_colSit" class="form-control" placeholder="Colonia" maxlength="40" size="40" required>
              </label>
            </div>
            <div>
              <label id="txt_colSit_error" class="div-errorVal"/>
            </div>
            
            <div>
              <label>C.P.(*):
                <input type="text" id="txt_cpSit" name="txt_cpSit" class="form-control" placeholder="C.P." maxlength="5" size="5" required>
              </label>
              <label>Municipio(*):
                <input type="text" id="txt_munSit" name="txt_munSit" class="form-control" placeholder="Municipio" maxlength="30" size="30" required>
              </label>
            </div>
            <div>
              <label id="txt_cpSit_error" class="div-errorVal"></label>
              <label id="txt_munSit_error" class="div-errorVal"></label>
            </div>         
            <div>
              <div>
                <label>Estado(*):<span id="spn_edoSit"></span></label>
              </div>
              <div>
                <label id="sel_sitestate_error" class="div-errorVal"/>
              </div>
              <input type="button" class="btn btn-primary" id="btn_localizarMapaSitio" value="Localizar en mapa" onClick="getCodeAddress('txt_domSit','','sel_sitestate','txt_colSit','txt_munSit','','hdn_siteLatitud','hdn_siteLongitud');">
            </div>
            <div>
              <label>Entre Calle(*):
                <input type="text" id="txt_callSit1" name="txt_callSit1" class="form-control" placeholder="Calle 1" maxlength="60" size="40" required>
              </label>
            </div>
            <div>
              <label id="txt_callSit1_error" class="div-errorVal"/>
            </div>
            <div>
              <label>Y Calle(*):
                <input type="text" id="txt_callSit2" name="txt_callSit2" class="form-control" placeholder="Calle 2" maxlength="60" size="40" required>
              </label>
            </div>
            <div>
              <label id="txt_callSit2_error" class="div-errorVal"/>
            </div>
            <div>
              <label>Referencias(*):
                <textarea id="txt_refSit" name="txt_refSit" class="form-control" placeholder="Agregue una breve descripción de la ubicación del sitio." maxlength="1024" cols="40" rows="5" required></textarea>
              </label>
              <div>
                <label id="txt_refSit_error" class="div-errorVal"/>
              </div>
            </div> 
          </div>
          <div class="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <label>Contactos(*):<span id="spn_contacto"></span></label>
            <div>
              <input type="button" class="btn btn-primary" id="btn_insertarContactoSitio" value="Agregar Contacto" onClick="openNewContact(2);">
            </div>
            <div>
              <div class="table-responsive">
                <table id="tbl_selectedContact2" class="table table-hover table-striped tbl_selectedContact">
                  <thead>
                    <tr>
                      <th>Nombre</th>
                      <th>Apellidos</th>
                      <th>Puesto</th>
                      <th>Teléfono</th>
                      <th>Teléfono Móvil</th>
                      <th>Correo Electŕonico</th>
                      <th>Departamento</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>              
                  </tbody>
                </table>
              </div>
            </div>
            <div>
              <label id="hdn_contArrSit_error" class="div-errorVal"/>
            </div>                                    
            <div>
              <label>Descripción(*):
                <textarea id="txt_sitDesc" name="txt_sitDesc" class="form-control" placeholder="Agregue una breve descripción acerca del sitio." maxlength="255" cols="40" rows="5" required></textarea>
              </label>
              <div>
                <label id="txt_sitDesc_error" class="div-errorVal"/>
              </div>
            </div>
            <div>
              <div id="div_siteMap" class="map"></div>
            </div>
          </div>
        </div>  
        <input type="hidden" id="hdn_siteLatitud"  name="hdn_siteLatitud"  class="form-control" value=-1>
        <input type="hidden" id="hdn_siteLongitud" name="hdn_siteLongitud" class="form-control" value=-1>
        <input type="hidden" id="hdn_siteAction" name="hdn_siteAction" class="form-control" value=-1>
        <input type="hidden" id="hdn_siteId"     name="hdn_siteId"     class="form-control" value=-1>  
        <input type="hidden" id="hdn_contArrSit" name="hdn_contArrSit" class="form-control" value="">                            
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
        <input type="submit" class="btn btn-primary" id="guardaSitio" value="Guardar Cambios" onClick="saveSiteNew();">
      </div>
         </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="container" id="contactSiteDialog"></div><!--cuadro de dialogo para gestión de contactos--> 

<div class="container" id="commentSiteDialog"></div>

<?php } ?>

