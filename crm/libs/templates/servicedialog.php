<?php
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

include_once "../db/common.php";

?>

<!--Cuadro de dialogo de detalles de servicio-->
<div id="mod_serviceDetails" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Detalles de Servicio</h4>
      </div>
      <div class="modal-body row">  
        <div class="col container-fluid" id="div_serviceDetails"></div> 
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--Cuadro de dialogo de equipos asociados al servicio-->
<div id="mod_serviceEquipmentList" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Equipos asociados al servicio</h4>
      </div>
      <div class="container-fluid">
        <div id="div_msgAlertServiceEquipmentList"></div>
      </div>
      <div class="modal-body row">  
        <div class="col container-fluid" id="div_serviceEquipmentList"></div> 
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--_Cuadro de dialogo de insertar equipo-->
<div id="mod_newServiceEquipment" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Nuevo Equipo</h4>
      </div>
      <div class="container-fluid">
        <div id="div_msgAlertServiceEquipment"></div>
      </div>
      <div class="modal-body"> 
        <form class="form-horizontal" id="frm_newServiceEquipment" name="frm_newServiceEquipment" role="form" enctype="multipart/form-data">           
          <div>
            <label>Número de serie:
              <input type="text" id="txt_serEquipNumSerie" name="txt_serEquipNumSerie" class="form-control" placeholder="Número de serie" maxlength="40" size="40" required>
            </label>  
          </div>
          <div>
            <label id="txt_serEquipNumSerie_error" class="div-errorVal"/>
          </div>
                        
          <div>
            <label>Equipo:<span id="spn_serEquip"></span></label>
          </div>
          <div>
            <label id="sel_serEquip_error" class="div-errorVal"/>
          </div>
          
          <input type="hidden" id="hdn_ServiceEquipAction" name="hdn_ServiceEquipAction" class="form-control" value=-1>
          <input type="hidden" id="hdn_ServiceEquipId" name="hdn_ServiceEquipId" class="form-control" value=-1>         
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
        <input type="button" class="btn btn-primary" id="cargaEventCamp" value="Guardar Cambios" onClick="saveServiceEquipmentNew();">
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 

<!--_Cuadro de dialogo de insertar servicio-->
<div id="mod_newService" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Nuevo Servicio</h4>
      </div>
      <div class="container-fluid">
        <div id=div_msgAlertService></div>
      </div>
      <div class="modal-body"> 
        <form class="form-horizontal" id="frm_newService" name="frm_newService" role="form" method="post">            
          <div id="div_serTipo">
            <div>
              <label>Tipos de servicio:<span id="spn_serTipo"></span></label>
            </div>
            <div>
              <label id="sel_serTipo_error" class="div-errorVal"/>
            </div>
          </div>
        
          <div id="div_serDownload">
            <div>
              <label>BW Download:
                <input type="text" id="txt_serDownload" name="txt_serDownload" class="form-control" placeholder="BW Download" maxlength="10" size="10" required>
              </label>  
            </div>             
            <div>
              <label id="txt_serDownload_error" class="div-errorVal"/>
            </div>
          </div>              
          
          <div id="div_serUpload">
            <div>
              <label>BW Upload:
                <input type="text" id="txt_serUpload" name="txt_serUpload" class="form-control" placeholder="BW Upload" maxlength="10" size="10" required>
              </label>  
            </div>
            <div>
              <label id="txt_serUpload_error" class="div-errorVal"/>
            </div>
          </div>
          
          <div id="div_serCantIp">
            <div>
              <label>Cantidad IP Públicas:
                <input type="text" id="txt_serCantIp" name="txt_serCantIp" class="form-control" placeholder="" maxlength="10" size="10" required>
              </label>  
            </div>
            <div>
              <label id="txt_serCantIp_error" class="div-errorVal"/>
            </div>
          </div>
          
          <div id="div_serLinAna">
            <div>
              <label>Línea analógica:
                <input type="text" id="txt_serLinAna" name="txt_serLinAna" class="form-control" placeholder="" maxlength="20" size="20" required>
              </label>  
            </div>
            <div>
              <label id="txt_serLinAna_error" class="div-errorVal"/>
            </div>  
          </div>
           
          <div id="div_serCantDid">
            <div>
              <label>Cantidad Dids:
                <input type="text" id="txt_serCantDid" name="txt_serCantDid" class="form-control" placeholder="" maxlength="10" size="10" required>
              </label>  
            </div>
            <div>
              <label id="txt_serCantDid_error" class="div-errorVal"/>
            </div>
          </div>
          
          <div id="div_serCantCan">
            <div>
              <label>Cantidad de canales:
                <input type="text" id="txt_serCantCan" name="txt_serCantCan" class="form-control" placeholder="" maxlength="10" size="10" required>
              </label>  
            </div>
            <div>
              <label id="txt_serCantCan_error" class="div-errorVal"/>
            </div>
          </div>
          
          <div id="div_serCantExt">
            <div>
              <label>Cantidad de extensiones:
                <input type="text" id="txt_serCantExt" name="txt_serCantExt" class="form-control" placeholder="" maxlength="10" size="10" required>
              </label>  
            </div>
            <div>
              <label id="txt_serCantExt_error" class="div-errorVal"/>
            </div>
          </div>
          
          <div id="div_serDomVpbx">
            <div>
              <label>Dominio VPBX:
                <input type="text" id="txt_serDomVpbx" name="txt_serDomVpbx" class="form-control" placeholder="" maxlength="40" size="40" required>
              </label>  
            </div>
            <div>
              <label id="txt_serDomVpbx_error" class="div-errorVal"/>
            </div>
          </div>
          
          <div id="div_serIpVpbx">
            <div>
              <label>IP VPBX:
                <input type="text" id="txt_serIpVpbx" name="txt_serIpVpbx" class="form-control" placeholder="" maxlength="15" size="15" required>
              </label>  
            </div>
            <div>
              <label id="txt_serIpVpbx_error" class="div-errorVal"/>
            </div>  
          </div>  
                     
          <div id="div_serCosto">
            <div>
              <label>Costo:
                <input type="text" id="txt_serCosto" name="txt_serCosto" class="form-control" placeholder="" maxlength="15" size="15" required>
              </label>  
            </div>
            <div>
              <label id="txt_serCosto_error" class="div-errorVal"/>
            </div>                       
          </div>
          <input type="hidden" id="hdn_serviceAction" name="hdn_serviceAction" class="form-control" value=-1>
          <input type="hidden" id="hdn_serviceId" name="hdn_serviceId" class="form-control" value=-1>         
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
        <input type="button" class="btn btn-primary" id="saveService" value="Guardar Cambios" onClick="saveServiceNew();">
      </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 
