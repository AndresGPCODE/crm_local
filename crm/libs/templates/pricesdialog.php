<?php
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Mexico_City');

include_once "../db/common.php";

/*Son los cuadros de diálogo para la visualización y para la inserción y edición de cotizaciones desde la pagina de prospectos, clientes sitios etc. Funciona para cualquier página que lo invoque, siemrpe y cuando setee las variables ocultas adeucadamente*/
/*Si inserta o edita y si se trata de un prospecto, cliente etc están indicados en las variables ocultas:

hdn_priceAction: Si es editar o guardar nuevo
hdn_priceId: Id de la cotización para editar
hdn_orgType: Indica si será 0= prospecto, 1= cliente, 2= sitio etc
*/
?>

<!--Cuadro de dialogo de visualización de cotizaciones-->
<div id="mod_prices" class="modal fade" data-backdrop="static" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Cotizaciones</h4>
      </div>
      <div class="container-fluid">
        <div id="div_msgAlertPrices"></div>
      </div>
      <div class="modal-body row">  
        <div class="col container-fluid" id="div_prices"></div> 
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 

<!--_Cuadro de dialogo de visualización de los detalles de una cotización-->
<div id="mod_priceDetails"class="modal fade" data-backdrop="static" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Detalles de Cotización</h4>
      </div>
      <div class="modal-body">         
        <div class="col container-fluid" id="div_priceDetails"></div> 
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 

<?php   
  if(hasPermission(4,'w')){ ?> 
<!--_Cuadro de dialogo de insertar cotizacion -->	
	<div id="mod_priceInsert" class="modal fade" data-backdrop="static" tabindex="-1">
		 <div class="modal-dialog">
			 <div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Nueva Cotización</h4>
				</div>
				<div class="container-fluid">
					<div id="div_msgAlertPrice"></div>
				</div>
				<form class="form-horizontal" id="frm_newPrice" name="frm_newPrice" role="form" method="post" enctype="multipart/form-data">
					<div class="modal-body">
						<label>Contacto:<span id="spn_contacto"></span></label>
						<div>
							<input type="button" class="btn btn-primary" id="btn_buscarContactos" value="Añadir Contacto" onClick="getContactList(1);">
						</div><br>
						<div>
						  <div class="table-responsive">
							  <table id="tbl_selectedContact4" class="table table-hover table-striped">
								  <thead>
				  				  <tr>
											<th>id</th>
										  <th>Nombre</th>
										  <th>Apellidos</th>
								 		  <th>Puesto</th>
										  <th>Teléfono</th>
										  <th>Teléfono Móvil</th>
										  <th>Correo Electrónico</th>
										  <th>Acciones</th>
									  </tr>
								  </thead>
								  <tbody> 
								    <tr id="tr_selectedContact4">
										</tr>                
								  </tbody>
							  </table>
						  </div>
						</div>
						<div>
						  <label id="hdn_contactId_error" class="div-errorVal"/>
						</div>
<?php   
  if(hasPermission(0,'a')){ ?>                           
              <div>
                <label>Estatus:<span id="spn_cotEstat"></span></label>
              </div>
              <div>
                <label id="sel_cotEstat_error" class="div-errorVal"/>
              </div>
<?php } ?> 
						<div>
              <label>Vigencia:<span id="spn_vigencia"></span></label><div id="div_restValTag"></div>
            </div>
            <div>
              <label>Categoría:<span id="spn_prodCategoria"></span></label>
            </div>
            <div>
              <label>Región:<span id="spn_region"></span></label>
            </div>
            <div>
              <label>Cantidad:
                <input type="text" id="txt_prodCant" name="txt_prodCant" class="form-control" placeholder="" maxlength="4" size="3" required>
              </label>
              <label>Tipo de Producto:<span id="spn_prodTipo"></span></label>
              <label>Ó Promoción:<span id="spn_productoPromo"></span></label> 
              <input type="button" class="btn btn-primary" id="btn_cargaEnlace" value="Agregar a la lista" onClick="addProdToPrice(0,0);">
            </div>
            
            <!--<div>
							<span><b>Ó Promoción:</b></span><br>
							<label><span id="spn_productoPromo"></span></label> 
						 </div>
						 <div>
							<input type="button" class="btn btn-primary" id="btn_cargaEnlace" value="Agregar a la lista" onClick="addProdToPrice(0,0);">
            </div>-->
            <div>
              <div class="table-responsive">
                <table id="tbl_selectedProducts" class="table table-hover table-striped">
                  <thead>
                    <tr>
                      <td>Cantidad</td>
                      <td>Código de Producto</td>
                      <td>Descripción</td>
                      <td>Precio Unitario</td>
                      <td></td>
                      <td>Total</td>
                      <td>Acciones</td>
                    </tr>
                  </thead>
                  <tbody>                 
                  </tbody>
                </table> 
                <table class="table table-hover table-striped">
                  <tbody>
                    <tr id="tr_prodSubtotal">
                      <td colspan="3"></td>
                      <td><b>Subtotal:</b></td>
                      <td id="td_prodSubtotal"></td>
                    </tr>
                    <tr id="tr_prodImpuesto">
                      <td colspan="3"></td>
                      <td><b>Impuesto:</b></td>
                      <td id="td_prodImpuesto"></td>
                    </tr>
                    <tr id="tr_prodTotal">
                      <td colspan="3"></td>
                      <td><b>Total:</b></td>
                      <td id="td_prodTotal"></td>
                    </tr>
                  </tbody>
                </table>           
              </div>
            </div>
						
						
						<div>
              <label>Notas:
                <textarea id="txt_cotNota" name="txt_cotNota" class="form-control" placeholder="Agregue algun aspecto mencionable con respecto a la cotización." cols="40" rows="5" required></textarea>
              </label>
              <div>
                <label id="txt_cotNota_error" class="div-errorVal"/>
              </div>
            </div>
						<input type="hidden" id="hdn_contactId" name="hdn_contactId" class="form-control" value=-1>
            <input type="hidden" id="hdn_priceAction" name="hdn_priceAction" class="form-control" value=-1>
            <input type="hidden" id="hdn_pricId" name="hdn_pricId" class="form-control" value=-1>  
            <input type="hidden" id="hdn_pricAuthId" name="hdn_pricAuthId" class="form-control" value=-1> 
            <input type="hidden" id="hdn_relCotPro" name="hdn_relCotPro" class="form-control" value=-1>  
					</div><!--/.modal-body-->
					<div class="modal-footer">
						<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
						<input type="button" class="btn btn-primary" id="guardaCotizacion" value="Guardar Cambios" onClick="savePriceNew();">
					</div>
				</form>
			 </div><!-- /.modal-content -->
		 </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->	
<?php } ?> 

<!--_Cuadro de dialogo de envio de cotización por email-->
<div id="mod_sendPricePdf" class="modal fade" data-backdrop="static" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Enviar cotización por correo</h4>
      </div><!-- /.modal-header -->
      <div class="container-fluid">
        <div id="div_msgAlertPriceSendEmail"></div>
      </div>
      <div class="modal-body">  
        <form class="form-horizontal" id="frm_newPriceEmail" name="frm_newPriceEmail" role="form" method="post" enctype="multipart/form-data">    
         <label>Seleccionar destinatarios:<span id="spn_contacto"></span></label>
          <div>
            <input type="button" class="btn btn-primary" id="btn_buscarMailContactos" value="Agregar Contacto" onClick="getContactList(3);">
          </div><br>
          <!--Datos de contacto-->
          <div class="table-responsive">
            <table id="tbl_selectedMailContact" class="table table-hover table-striped">
              <thead>
                <tr>
                  <th>id</th>
                  <th>Nombre</th>
                  <th>Apellidos</th>
                  <th>Puesto</th>
                  <th>Teléfono</th>
                  <th>Teléfono Móvil</th>
                  <th>Correo Electŕonico</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>                
              </tbody>
            </table>
          </div><hr>         
          <div>Asunto: 
            <input type="text" id="txt_priceMsgSubject" name="txt_priceMsgSubject" class="form-control" placeholder="" maxlength="80" size="60" required>
          </div>
          <div>
            <label id="txt_priceMsgSubject_error" class="div-errorVal"/>
          </div>
          <br> 
          Contenido del mensaje:<br>          
          <!--Editor de texto-->     
          <div id="div_priceMsgContent"></div> 
          
          <input type="hidden" id="hdn_pricMailCont" name="hdn_pricMailCont" class="form-control" value=""> 
          <input type="hidden" id="hdn_htmlMsg" name="hdn_htmlMsg" class="form-control" value="">
          <input type="hidden" id="hdn_plainMsg" name="hdn_plainMsg" class="form-control" value="">
          <input type="hidden" id="hdn_cotId" name="hdn_cotId" class="form-control" value="">
          <input type="hidden" id="hdn_orgTypeMail" name="hdn_orgTypeMail" class="form-control" value="">  
          <input type="hidden" id="hdn_pricMailOpt" name="hdn_pricMailOpt" class="form-control" value=-1>                               
         
      </div><!-- /.modal-body -->
      <div class="modal-footer">         
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
        <input type="button" class="btn btn-primary" id="btn_mandaCotMail" value="Enviar Cotización" onClick="sendPriceEmail();">
      </div><!-- /.modal-footer -->
        </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->  

<!--_Cuadro de dialogo de autorización-->
<div id="mod_authorization" class="modal fade" data-backdrop="static" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Autorización de operación</h4>
      </div>
      <div class="container-fluid">
        <div id="div_msgAlertAuth"></div>
      </div>
      <div class="modal-body row"> 
        <div class="col container-fluid">
          <div>
            Esta acción requiere de la autorización de un gerente u otro usuario con permisos.
            <label>Usuario:
              <input type="text" id="txt_authUserName" name="txt_authUserName" class="form-control" placeholder="" maxlength="30" size="20" required>
            </label>          
          </div>
          <div>
            <label>Contraseña:
              <input type="password" id="txt_authPass" name="txt_authPass" class="form-control" placeholder="" maxlength="30" size="20" required>
            </label>          
          </div>
        </div>       
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
        <input type="submit" class="btn btn-primary" id="sendAuthRequest" value="Enviar" onClick="sendAuthReq();">
        <input type="hidden" id="hdn_authOpt" name="hdn_authOpt" class="form-control" value=-1>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 

<div class="container" id="contactPriceDialog"></div><!--cuadro de dialogo para gestión de contactos--> 


