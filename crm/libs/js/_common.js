  var subtotal = 0;
  var total = 0;
  var impuesto = 0;
  var perm = -1;
  var contTempId = 0;
  var map="";
  var geocoder="";
  var latlng = "";
  var pricAuthId = -1;
  var markerArr = new Array();
  
  /*arreglos globales que gestionan el guardado de series de datos dinámicos que se mandan por ajax (por ejemplo para agregar una serie do contactos nuevos a un prospecto, cuando se agregan productos o tarifas a una cotización etc)*/
  var productArray = new Array();
  var enlaceArray = new Array();
  var telefoniaArray = new Array();
  var contactoArray = new Array();
  var invitadoArray = new Array();
  var equipoArray = new Array();
  var adecArray = new Array();
  var contactoContrArray = new Array();

$(document).ready(function(){  
  /*$("#header").load("../../libs/templates/header.php"); //dirección relativa del modulo que invoca esta librería carga el encabezado
  $("#footer").load("../../libs/templates/footer.php");*/ //dirección relativa del modulo que invoca esta librería carga el footer
  
  //traduce los mensajes que despliega el validador de jQuery al español
  jQuery.extend(jQuery.validator.messages, {
    required: "Este campo es requerido.",
    remote: "Por favor, arregle este campo.",
    email: "El formato de de correo electrónico es incorrecto.",
    url: "Introduzca una url válida.",
    date: "El formato de la fecha es incorrecto.",
    dateISO: "El formato de fecha (ISO) es incorrecto.",
    number: "El campo debe ser numérico.",
    digits: "El campo sólo debe contener dígitos.",
    creditcard: "Por favor introduzca un número válido de tarjeta de crédito.",
    equalTo: "Por favor introduzca el mismo valor de nuevo.",
    accept: "Introduzca un valor con unaextensión válida.",
    maxlength: jQuery.validator.format("Este campo no admite más de {0} caracteres."),
    minlength: jQuery.validator.format("Introduzca mínimo {0} caracteres."),
    rangelength: jQuery.validator.format("Introduzca un valor entre {0} y {1} caracteres de longitud."),
    range: jQuery.validator.format("Introduzca un valor entre {0} y {1}."),
    max: jQuery.validator.format("Introduzca un valor menor o igual a {0}."),
    min: jQuery.validator.format("Introduzca un valor mayor o igual a {0}.")
  }); 
  
  //traducción del texto de las datatables
  datatableespaniol = {
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "<i class='icon-search asphalt'> Buscar:</i>",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "orderCellsTop": true,
    "oPaginate": {
      "sFirst":    "Primero",
      "sLast":     "Último",
      "sNext":     "Siguiente",
      "sPrevious": "Anterior"
    },
    "oAria": {
      "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }   
  }
  
  //se usa en el cuadro de dialogo de insertar/editar enlaces de sitios, se usa en caso de que se desee insertar una fecha de desinstalación o no
  $(document.body).on('change','#chk_fecDesSitCon',function(){
    if($(this).is(':checked')){
      $(this).val(1);
      $("#txt_fecDesSitCon").val("");
      $("#txt_fecDesSitCon").prop("disabled",false);
    } 
    else{
      $(this).val(0);
      $("#txt_fecDesSitCon").val("");
      $("#txt_fecDesSitCon").prop("disabled",true);     
    }    
  });
  
  $(document.body).on('change','#chk_ticketReassign, #chk_removeLogo',function(){
    if($(this).is(':checked')){
      $(this).val(1);
    } 
    else{
      $(this).val(0);
    }
  });
  
  $(document.body).on('change','#sel_serTipo',function(){
    var option = parseInt($(this).val());
    setSerFieldsByType(option);
  });
  
  $(document.body).on('change','#sel_ticketTopic',function(){
    if((5==$(this).val())||(6==$(this).val())||(14==$(this).val())||(17==$(this).val())){
      $("#div_tickTopicOther").show();
    }
    else{
      $("#txt_tickTopicOther").val("");
      $("#div_tickTopicOther").hide();
    }
  }); 
  
  $(document.body).on('change','#sel_ticketOrg',function(){
    if(3==$(this).val()){
      $("#div_tickOrgOther").show();
    }
    else{
      $("#txt_tickOrgOther").val("");
      $("#div_tickOrgOther").hide();
    }
  });
  
  $(document.body).on('change','#chk_ticketClientNotify',function(){
    if($(this).is(':checked')){
      $(this).val(1);
      $("#div_assocData").show();
      $("#txt_tickAssocEmail").val("");
      $("#txt_tickAssocEmail").prop("disabled",false);
      $("#txt_tickAssocName").val("");
      $("#txt_tickAssocName").prop("disabled",false);
    } 
    else{
      $(this).val(0);
      $("#div_assocData").hide();
      $("#txt_tickAssocEmail").val("");
      $("#txt_tickAssocEmail").prop("disabled",true); 
      $("#txt_tickAssocName").val("");
      $("#txt_tickAssocName").prop("disabled",true);    
    }    
  }); 
  
  $(document.body).on('change','#sel_validity',function(){
    /*productArray.length = 0;
    $("#tbl_selectedProducts").find("tr:gt(0)").remove();*/
  });  
  
  $(document.body).on('click','#txt_enlSub,#txt_telCant,#txt_prodCant',function(){
    $(this).select();
  });
  
  $(document.body).on('change','#txt_razonSoc,#txt_nomCom,#txt_rfc',function(){
    var id = $(this).attr("id");
    var fieldName = id.split("_");
    var razonSoc = $("#txt_razonSoc").val();
    var nomCom = $("#txt_nomCom").val();
    var rfc = $("#txt_rfc").val();
    
    if(0==$("#"+id).val().length)
      $("#spn_"+id).html("");
    else
      $.ajax({
        type: "POST",
        url: '../../libs/db/common.php',
        data: {"action":"clientAutocomplete","razonSoc":razonSoc,"nomCom":nomCom,"rfc":rfc,"orgType":$("#hdn_orgType").val(),"fieldName":fieldName[1]},
        success: function(msg){
          var resp = msg.trim();
          $("#spn_"+id).html(resp);      
        }
      });
  });
  
  $(document.body).on('change','#txt_TareaFechaIni,#txt_tareaFechaFin',function(){
    getAvailablePeople(0);
    getEquipmentList(0);
  }); 
  
  $(document.body).on('change','#sel_eveEstat,#sel_eveTipo',function(){
    if(3==$("#sel_eveEstat").val()&&5==$("#sel_eveTipo").val()){
      $("#div_eveRadFact").show();
    } 
    else{
      $("#div_eveRadFact").hide();
    }    
  });
  
  $(document.body).on('change','.chk_contactoContrato',function(){
    if($(this).siblings(':checked').length>1) {
      this.checked = false;
    }
    else{
      var value = $(this).val();
      if($(this).is(':checked')){
        contactoContrArray.push(String(value));
      } 
      else{
        unsetFromArray(contactoContrArray,value);
      }
    }
  });
});

function random_string(){
 var random = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15); 
 return random; 
}

function fn_Whatsapp(){
  window.open("https://web.whatsapp.com/","EventoWhatsapp","status=no,toolbar=no,menubar=no,scrollbars=yes,location=yes,width=1024, height=600");   
}

//inicializa un mapa. Debe especificarse las coordenadas y en qué div está contenido
function initMap(lat,lng,div,latDiv,lngDiv) {
  markerArr.length=0;
  latlng = new google.maps.LatLng(lat,lng);
  map = new google.maps.Map(document.getElementById(div), { // Mapa
    //center: new google.maps.LatLng(20.683, -103.378),   // Posicion Inicial
    center: latlng,   // Posicion Inicial
    zoom: 16
  }); 
  var marker = new google.maps.Marker({
      position: latlng,
      map: map,
      draggable:true
      //animation: google.maps.Animation.DROP
  });
  markerArr.push(marker);
   
  google.maps.event.addListener(marker, 'dragend', function(evt){
    $("#"+latDiv).val(evt.latLng.lat());
    $("#"+lngDiv).val(evt.latLng.lng());
    //alert('Latitude: '+evt.latLng.lat()+ ' & Longitude'+evt.latLng.lng());
  });
  
  //map.setCenter(latlng);
}

//agrega un puntero extra al mapa, para esto recibe la latitus y la longitud en un arreglo
function addMapPointer(locData) {
  var coord = new google.maps.LatLng(locData[1],locData[2]);
  var infoWindow = new google.maps.InfoWindow();  // ventana emergente de marcador
  
  var marker = new google.maps.Marker({
      position: coord,
      map: map
  }); 
  marker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png');  
  
  markerArr.push(marker);
  
  google.maps.event.addListener(marker, 'click', (function(marker) {
    return function() {
      var info = '<div id="contenedor">' + '<div class="titulo">' + locData[0] + '</div></div>';
      infoWindow.setContent(info);
      infoWindow.open(map, marker);
    }
  })(marker));
}

//ubica en el mapa y obtiene las coordenadas de una dirección insertada como texto en el formulario. Toma en cuenta el campo address (dirección) num (num exterior) estado y pais
function getCodeAddress(addressTxt,numTxt,edoSel,colTxt,munTxt,paisSel,latDiv,lngDiv) {
  if((0>=$("#"+addressTxt).val().length)||(0==$("#"+edoSel).val()))
   alert("No se ha agregado una dirección completa para localizar");
  else{
    geocoder = new google.maps.Geocoder();  // Inicia un geocodificador

    //Calle Ignacio Herrera y Cairo 1959, Ladrón de Guevara, Ladron De Guevara, Guadalajara, Jalisco, Mexico
    
    var address = $("#"+addressTxt).val();
    var addressSta = $("#"+edoSel+" option:selected").text();
    var addressCol = $("#"+colTxt).val();
    var addressMun = $("#"+munTxt).val();
    
    if(null!=$("#"+numTxt).val()&&undefined!=$("#"+numTxt).val())
      var addressNum = $("#"+numTxt).val()+", "; 
    else
      var addressNum = ", ";  
    if(null!=$("#"+paisSel).val()&&undefined!=$("#"+paisSel).val())
      var addressCou = $("#"+paisSel+" option:selected").text();
    else
      var addressCou = "";
    
    $.each(markerArr,function(key,value){//arreglo de invitados
      value.setMap(null);
    });
    
    var dirCompleta = address+' '+addressNum+', '+addressCol+', '+addressMun+', '+addressSta+' '+addressCou;
    
    markerArr.length=0;
    
    geocoder.geocode( { 'address': dirCompleta}, function(results, status) {
      if (status == 'OK') {
        $("#"+latDiv).val(results[0].geometry.location.lat());   // Obtiene la latitud 
        $("#"+lngDiv).val(results[0].geometry.location.lng());  // Obtiene la longitud
        map.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
            map: map,
            draggable:true,
            position: results[0].geometry.location
        });
        markerArr.push(marker); 
      } 
      else {
        alert('No se pudo localizar la dirección Err Code: ['+status+'] '+dirCompleta);
      }
    });
  }
}

function updateIndexActive(id){
  $("#"+id).addClass("active");
}

function readMsg(msgId){
  $.ajax({
    type: "POST",
    url:  "../../libs/db/common.php",
    data: {"action": "readMsg", "msgId":msgId},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo Mensaje");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){    
      var resp = (msg).trim().split("||");
      if("OK" == resp[0]){
        $("#div_msgBody").html(resp[1]);
        $("#mod_msgDetails").modal("show");
        $("#spn_glyphReadSign_"+msgId).hide();
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);                   
      }       
    }  
  });
}

function setSerFieldsByType(option){
  switch(option){
  case 0:
    serFieldSet("serDownload",0);
    serFieldSet("serUpload",0);
    serFieldSet("serCantIp",0);
    serFieldSet("serLinAna",0);
    serFieldSet("serCantDid",0);
    serFieldSet("serCantCan",0);
    serFieldSet("serCantExt",0);
    serFieldSet("serDomVpbx",0);
    serFieldSet("serIpVpbx",0);
    serFieldSet("serCosto",0);        
  break;
  case 1://internet
    serFieldSet("serDownload",1);
    serFieldSet("serUpload",1);
    serFieldSet("serCantIp",0);
    serFieldSet("serLinAna",0);
    serFieldSet("serCantDid",0);
    serFieldSet("serCantCan",0);
    serFieldSet("serCantExt",0);
    serFieldSet("serDomVpbx",0);
    serFieldSet("serIpVpbx",0);
    serFieldSet("serCosto",1);
  break;            
  case 2://RPV
    serFieldSet("serDownload",1);
    serFieldSet("serUpload",1);
    serFieldSet("serCantIp",0);
    serFieldSet("serLinAna",0);
    serFieldSet("serCantDid",0);
    serFieldSet("serCantCan",0);
    serFieldSet("serCantExt",0);
    serFieldSet("serDomVpbx",0);
    serFieldSet("serIpVpbx",0);
    serFieldSet("serCosto",1);
  break;
  case 3://ip publica
    serFieldSet("serDownload",0);
    serFieldSet("serUpload",0);
    serFieldSet("serCantIp",1);
    serFieldSet("serLinAna",0);
    serFieldSet("serCantDid",0);
    serFieldSet("serCantCan",0);
    serFieldSet("serCantExt",0);
    serFieldSet("serDomVpbx",0);
    serFieldSet("serIpVpbx",0);
    serFieldSet("serCosto",1);
  break;
  case 4://línea analógica
    serFieldSet("serDownload",0);
    serFieldSet("serUpload",0);
    serFieldSet("serCantIp",0);
    serFieldSet("serLinAna",1);
    serFieldSet("serCantDid",0);
    serFieldSet("serCantCan",0);
    serFieldSet("serCantExt",0);
    serFieldSet("serDomVpbx",0);
    serFieldSet("serIpVpbx",0);
    serFieldSet("serCosto",1);
  break;
  case 5://VPBX
    serFieldSet("serDownload",0);
    serFieldSet("serUpload",0);
    serFieldSet("serCantIp",0);
    serFieldSet("serLinAna",0);
    serFieldSet("serCantDid",1);
    serFieldSet("serCantCan",1);
    serFieldSet("serCantExt",1);
    serFieldSet("serDomVpbx",1);
    serFieldSet("serIpVpbx",1);
    serFieldSet("serCosto",1);
  break;   
  }  
}

//oculta o muestra el campo especificado en el parametro field dependiendo del valor del parametro action (show, hide)
function serFieldSet(field,action){
  $("#txt_"+field).val("");
  $("#txt_"+field+"_error").html("");
  
  if(0 == action){
    $("#div_"+field).hide();
    $("#txt_"+field).prop('disabled',true);
  }  
  if(1 == action){
    $("#div_"+field).show();
    $("#txt_"+field).prop('disabled',false);
  }
}

//carga tabs con respecto a status para los módulos que lo requieran (ahora prospectos y clientes)
function getStatusTabs(orgType){
  switch(orgType){
    case 0:
      var divTabId ="prospectTabs";
    break;
    case 1:
      var divTabId ="clientTabs";
    break;
    default:
      var divTabId ="";
    break;
  }
  $.ajax({
    type: "POST",
    url:  "../../libs/db/common.php",
    data: {"action": "getStatusTabs"},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo tabs de estatus para este módulo");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#div_"+divTabId).html(resp[1]);  
              
        $("#div_"+divTabId+" li").each(function(){          
          var statId = $(this).attr("id").split("_"); 
          if(0==orgType)        
            getProspectRegs(statId[2],statId[1]);        
        });
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);       
      }   
    }  
  });
}

//añade reglas de validación usadas en varias funciones de validación de datos para diferentes formularios
function addValidationRules(orgType){

  switch(orgType){
    case 0:
      var tipPerSel = $('#mod_prospectInsert input:radio:checked').val();
    break;
    case 1:
      var tipPerSel = $('#mod_clientInsert input:radio:checked').val();
    break;
  }
 
  jQuery.validator.addMethod("rfc", function(value, element){  
    if(1==tipPerSel && 0!=value.length){
      return /^([A-Z\s]{4})-\d{6}-([A-Z0-9\w]{3})$/.test(value);
    }
    else if(2==tipPerSel && 0!=value.length){
      return /^([A-Z\s]{3})-\d{6}-([A-Z0-9\w]{3})$/.test(value); 
    }
    else{
      return true;
    }
  }, "El formato de RFC no es correcto");
  
  jQuery.validator.addMethod("isSomethingSelected", function(value, element){    
    if(0!=value)
      return true;
    else
      return false;  
  }, "Debe seleccionar una opción");

  jQuery.validator.addMethod("isPhoneNumber", function(value, element){    
    if((10!=value.length||(value%1!=0))&&(0<value.length))
      return false;
    else
      return true;  
  }, "Debe introducir diez dígitos");
  
  jQuery.validator.addMethod("hasContacts", function(value, element){    
    if(0>=contactoArray.length)
      return false;
    else
      return true;  
  }, "Debe agregar al menos a un contacto");
  
  jQuery.validator.addMethod("integer", function(value, element){    
    if(value%1!=0)
      return false;
    else
      return true;  
  }, "Debe agregar al menos a un contacto");
  
  jQuery.validator.addMethod("isDateTime", function(value, element){
    if(1<=value.length){
      if(/^\d{4}\-\d{2}\-\d{2} \d{2}\:\d{2}\:\d{2}$/i.test(value)==true){
          var splitData=value.split(" ");
          var date_split = splitData[0].split("-");
          if((31>=date_split[2])&&(12>=date_split[1])){
            return true;
          }
          else{
            return false;
          }
        }
      else{
        return false;
      } 
    }
    else
      return true;      
  }, "El formato de fecha/hora no es válido");
  
  //valida si la fecha de fin es primero que la fecha de inicio
  jQuery.validator.addMethod("isLaterThanDate", function(value, element, arg) {
    var fecha1 = parseDatetime(value);
    var fecha2 = parseDatetime($("#"+arg).val());

    if(fecha1.getTime() <= fecha2.getTime()) {
      return false;     
    }
    else {
      return true;    
    }
  }, "La fecha y hora de fin son anteriores a la fecha y hora de inicio");
  
  //verifica que el formato de dirección ip sea el correcto
  jQuery.validator.addMethod("isIpAddress", function(value, element){  
    if(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/i.test(value)==true){
      return true;
    }
    else{
      return false;
    }  
  }, "El formato de ip no es válido");
  
  //valida si la fecha selecionada es menor que la actual
  jQuery.validator.addMethod("isLaterThanToday", function(value, element){ 
    var date = new Date();    
    var datetime = parseDatetime(value);
    
    if(Date.parse(datetime) < Date.parse(date)) {
      return false;
    } 
    else {
      return true;
    }
  }, "La fecha y hora introducidos son anteriores a la fecha y hora actual"); 
}
  
/*********Apartado de funciones misceláneas de formato, configuración y parseo de datos*********/

//convierte un cuadro de diálogo en responsivo, para que cambie su tamaño de acuerdo al tamaño de la pantalla se pone como parámetro el nombre del cuadro de diálogo sin #
function setModalResponsive(modalName){
  $('#'+modalName).modal({
    backdrop: 'static',
    keyboard: false
  });
  $('#'+modalName).on('shown.bs.modal', function(){
    $(this).find('.modal-dialog').css({width:'auto',
                                       height:'auto', 
                                      'max-height':'100%'});
    if(undefined!==map&&""!=map)  {                                
      google.maps.event.trigger(map, "resize");
      map.setCenter(latlng);
    }
  });
}

//imprime el un mensaje de error en el div especificado en el parámetro target se usa por ejemplo cuando no se puede insertar un prospecto/cliente/sitio etc o cuando un formulario no pasa la validación
function printErrorMsg(target,msg){
  if(undefined == msg){
    msg = "Ocurrió un error desconocido, consulte con su administrador";
  }
  $("#"+target).html("<div class=\"alert alert-danger alert-dismissable\">"+
                     "  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>"+
                     "  <strong>Error:</strong> "+msg+"."+
                     "</div>");
  $("html, body").animate({ scrollTop: 0 }, "slow");
  $(".modal").animate({ scrollTop: 0 }, "slow");
}

//imprime el un mensaje de éxito en el div especificado en el parámetro target se usa por ejemplo cuando se guardó un prospecto/cliente/sitio etc
function printSuccessMsg(target,msg){
  $("#"+target).html("<div class=\"alert alert-success alert-dismissable\">"+
                     "  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>"+
                     "  <strong>OK:</strong> "+msg+"."+
                     "</div>");
}

//función que abre el cuadro de diálogo de cargando cuando una operación o llamada a ajax se ejecuta
function setLoadDialog(opt,text){
  var waitDial = "<div id=\"mod_pleaseWaitDialog\" class=\"modal\" data-backdrop=\"static\" data-keyboard=\"false\">"+
                 "  <div class=\"modal-dialog\">"+
                 "    <div class=\"modal-content\">"+
                 "      <div class=\"modal-header\" id=\"div_pleaseWaitMsg\">"+text+". Por favor espere</div>"+
                 "      <div class=\"modal-body\">"+
                 "        <div class=\"progress progress-striped active\">"+
                 "          <div class=\"progress-bar progress-bar-info progress-bar-striped\" role=\"progressbar\" aria-valuemax=\"100\" style=\"width:100%\"></div>"+
                 "        </div>"+
                 "      </div>"+
                 "    </div>"+
                 "  </div>"+
                 "</div>";

  if(0==opt){
    $('body').append(waitDial);
    $("#mod_pleaseWaitDialog").modal("show");
  }
  if(1==opt){
    $("#mod_pleaseWaitDialog").modal("hide").remove();
  } 
}

//Da formato al RFC dependiendo si se ha seleccionado persona física (1) o moral (2)
function formatRfc(input,form){  
  var tipPerSel = $("#"+form+" input:radio:checked").val();  
  var rfc1= $("#"+input).val();
  var rfc2 = ""; 
  
  rfc2 = rfc1.replace(/\-/g,"");

  if(1==tipPerSel && ""!=rfc1)
    rfc2 = rfc2.substring(0,4)+"-"+rfc2.substring(4,10)+"-"+rfc2.substring(10,13);
  else if(2==tipPerSel && ""!=rfc1)
    rfc2 = rfc2.substring(0,3)+"-"+rfc2.substring(3,9)+"-"+rfc2.substring(9,12);  
  else
    $("#"+input).val("");
  rfc2 = rfc2.toUpperCase();
  $("#"+input).val(rfc2);
}

//le quita formato al RFC para su edición
function deformatRfc(input){
  var rfc1= $("#"+input).val();
  var rfc2 = ""; 
   
  rfc2 = rfc1.replace(/\-/g,"");
  rfc2 = rfc2.toLowerCase();
  $("#"+input).val(rfc2);
}

//convierte el formato de las fechas de las cajas de texto (aaaa-mm-dd hh:mm:ss) al formato de javascript (mm/dd/aaaa hh:mm:ss)
function parseDatetime(value){
  var dtSplit = value.split(" ");
  var fecha = dtSplit[0].split("-");
  var hora = dtSplit[1].split(":");
  var datetime = new Date(fecha[1]+"/"+fecha[2]+"/"+fecha[0]+" "+hora[0]+":"+hora[1]+":"+hora[2]);
  return datetime;
}

//variable con parámetros de configuración del picker de fecha y hora
var datetimeOptions = {
  yearRange: "-100:+0",
  changeMonth: true,
  changeYear: true,
  timeText: 'Tiempo',
  hourText: 'Hora',
  minuteText: 'Minuto',
  secondText: 'Segundos',
  millisecText: 'Milisegundos',
  timezoneText: 'Zona Horaria', 
  stepMinute: 10, 
  showSecond: false,
  showMillisec: false,
  showMicrosec: false,
  showTimezone: false,
  dateFormat: "yy-mm-dd",
  timeFormat: "HH:mm:ss",
  timeOnly: false,
  onClose: function() {
    $( this ).focus().blur();
  }
}

//variable con parámetros de configuración del picker de fecha y hora
var timeOptions = {
  timeText: 'Tiempo',
  hourText: 'Hora',
  minuteText: 'Minuto',
  secondText: 'Segundos',
  millisecText: 'Milisegundos',
  timezoneText: 'Zona Horaria', 
  stepMinute: 10, 
  showSecond: false,
  showMillisec: false,
  showMicrosec: false,
  showTimezone: false,
  dateFormat: "yy-mm-dd",
  timeFormat: "HH:mm:ss",
  timeOnly: true
}

//redondea a los decimales que se le pide
function redondeo(value, decimals) {
    return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

//retira el subarreglo del arreglo miltidimensional
function unsetFromArray(arr,id){
  if(undefined!==arr){
    for(var x=0; x<arr.length; x++){
      if(($.isArray(arr[x]))&&(1<arr[x].length)){
        if(arr[x][0] == id)
          arr.splice(x,1);
        unsetFromArray(arr[x]);  
      }
      else{
        if(arr[x] == id)
          arr.splice(x,1);    
      }
    }
  }
  else{
    //nada
  } 
}

//extrae un arreglo de un arreglo multidimensional
function extractArrayFromArray(arr,id){
  var newArray = Array();
  if(undefined!==arr){
    for(var x=0; x<arr.length; x++){
      if(1<arr[x].length){    
        if(arr[x][0] == id){
          newArray.push(arr[x]);
          extractArrayFromArray(arr[x],arr[x][0]);
        }        
      }
      else{
        if(arr[x] == id){         
          for(y=0; y<1<arr[x].length; y++){
            newArray.push(arr[x]);
          }
        }
      }
    }
  }
  else{
    //nada
  } 
  return newArray;
}


//toma una imagen de un editor de texto (summernote) y la manda a subir a una ubicación en el servidor. Ésto con la intención de que no se guarde como dato base64 sino como una imagen normal con url de referencia
function sendFile(file,editor,alertDiv){
  data = new FormData();
  data.append("file",file);
  data.append("action","uploadimage");
  $.ajax({
    data: data,
    type: "POST",
    url: '../../libs/db/uploadsummernoteimg.php',
    cache: false,
    contentType: false,
    processData: false,
    success: function(url){
      var resp = url.trim().split("||");
      if("OK" == resp[0]){
        editor.summernote('editor.insertImage', resp[1]); 
      }
      else{
        printErrorMsg(alertDiv,resp[1]);        
      }      
    }
  });
}

function removeFile(target,editor,alertDiv){
  data = new FormData();
  data.append("src",target.src);
  data.append("action","removeimage");
  $.ajax({
    data: data,
    type: "POST",
    url: '../../libs/db/uploadsummernoteimg.php',
    cache: false,
    contentType: false,
    processData: false,
    success: function(msg){
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        printSuccessMsg(alertDiv,resp[1]);
      }
      else{
        printErrorMsg(alertDiv,resp[1]);
      }
    }
  });
}

function callMarcatron(number,type){
  var message = "";
  if("WHATSAPP"==type){
    message = $("#txt_whatsMsg").val();
  }
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "callMarcatron", "number":number, "type": type, "msg": message},
    success: function(msg){
      resp = msg.trim();
      if(resp=="TRUE"){
        if("TELEFONO"==type)
          alert("Marcando");
        else if("WHATSAPP"==type)
          $("#div_whatCommHistory").append("<div class=\"message-out bubble-send\" align=\"right\">"+message+"</div><br>");
      } 
      else{
        alert("No es posible realizar la comunicación");
      }
    }  
  });
}

/****Fin de apartado****/


/*********llamadas de ajax que llenan las listas desplegables para los formularios***********/

//obtiene la lista de estados id corresponde al valor de la opción que se desea seleccionar por default, spnDest es el id del span donce se va a agregar la lista, pref es un prefijo que se usará para diferenciarlo entre otras listas de estado
function getStatesList(id,spnDest,pref){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getStatesList", "id":id, "pref":pref},
    success: function(msg){
      $("#"+spnDest).html(msg); 
    }  
  });
}

//obtiene la lista de paises id corresponde al valor de la opción que se desea seleccionar por default, spnDest es el id del span donde se va a agregar la lista, pref es un prefijo que se usará para diferenciarlo entre otras listas de país
function getCountryList(id,spnDest,pref){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getCountryList", "id":id, "pref":pref},
    success: function(msg){
      $("#"+spnDest).html(msg); 
    }  
  });
}

//obtiene la lista de origenes de ventas
function getOriginList(id){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getOriginList", "id":id},
    success: function(msg){
      $("#spn_orgn").html(msg); 
      $("#sel_origin").change(function(){
        if("8"==$(this).val()){         
          $("#div_other_origin").show();
        }
        else{
          $("#txt_origin").val("");
          $("#div_other_origin").hide();
        }     
      });
    }  
  });
}

//obtiene la lista de estatus de ventas
function getStatusList(id){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getStatusList", "id":id},
    success: function(msg){
      $("#spn_estat").html(msg); 
    }  
  });
}

//obtiene la lista de asignar a:
function getAssigntoList(id){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getAssigntoList", "id":id},
    success: function(msg){
      $("#spn_asign").html(msg); 
    }  
  });
}

//obtiene los estatus de eventos
function getEventStatList(id,option){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getEventStatList", "id":id, "opt":option},
    success: function(msg){
      if(1==option)
        $("#spn_eveEstat").html(msg); 
      else
        $("#spn_tareaEstat").html(msg); 
    }  
  });
}

//obtiene los tipos de eventos
function getEventTypeList(id){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getEventTypeList", "id":id},
    success: function(msg){
      $("#spn_eveTipo").html(msg); 
    }  
  });
}

//obtiene los intervalos de tiempo para los eventos
function getEventTimeInterval(id){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getEventTimeInterval", "id":id},
    success: function(msg){
      $("#spn_eveDur").html(msg); 
    }  
  });
}

//obtiene los tipos de documentos escaneados
function getUploadedDocType(id){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getDocTypeList", "id":id},
    success: function(msg){
      $("#spn_docType").html(msg); 
    }  
  });
}

//obtiene las topologías de un enlace de un sitio
function getConnectionTopology(id){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getConnectionTopologyList", "id":id},
    success: function(msg){
      $("#spn_conTop").html(msg); 
    }  
  });
}

//obtiene los estatus de conexión de un enlace de un sitio
function getConnectionActive(id){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getConnectionActiveList", "id":id},
    success: function(msg){
      $("#spn_conAct").html(msg); 
    }  
  });
}

//obtiene los tipos de servicios
function getService(id){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getServiceList", "id":id},
    success: function(msg){
      $("#spn_serTipo").html(msg); 
    }  
  });
}

//obtiene los equipos a seleccionar para los servicios
function getServiceEquipment(id){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getServiceEquipmentList", "id":id},
    success: function(msg){
      $("#spn_serEquip").html(msg); 
    }  
  });
}

//obtiene los usuarios o grupos a los cuales asignar el ticket
function getTicketAssignToList(id){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getTicketAssignToList", "id":id},
    success: function(msg){
      $("#spn_tickAssign").html(msg); 
    }  
  });
}

//obtiene los tipos de origen de un ticket
function getTicketOriginList(id){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getTicketOriginList", "id":id},
    success: function(msg){
      $("#spn_tickOrg").html(msg); 
    }  
  });
}

//obtiene los tipos de topics de tickets
function getTicketTopicList(id){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getTicketTopicList", "id":id},
    success: function(msg){
      $("#spn_tickTopic").html(msg); 
    }  
  });
}

//obtiene los tipos de sla de tickets
function getTicketSlaList(id){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getTicketSlaList", "id":id},
    success: function(msg){
      $("#spn_tickSla").html(msg); 
    }  
  });
}

//obtiene los tipos de departamentos de tickets
function getTicketDepartmentList(id){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getTicketDepartmentList", "id":id},
    success: function(msg){
      $("#spn_tickDepartment").html(msg); 
    }  
  });
}

//obtiene los tipos de prioridad de tickets
function getTicketPriorityList(id){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getTicketPriorityList", "id":id},
    success: function(msg){
      $("#spn_tickPriority").html(msg); 
    }  
  });
}

//obtiene las regiones (guadalajara, manzanillo etc)
function getRegionList(id){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getRegionList", "id":id},
    success: function(msg){
      $("#spn_region").html(msg);  
      getProductCategoryList(0);    
      $(document.body).on('change','#sel_region',function(){
        getProductCategoryList(0);     
      });
    }  
  });
}

//obtiene los titulos de persona (Ing. Lic. Sr. ect)
function getPersonTitletList(id){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getPersonTitleList", "id":id},
    success: function(msg){
      $("#spn_conTit").html(msg); 
    }  
  });
}

//obtiene los departamentos (contabilidad, direccion etc)
function getDepartmentList(id){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getDepartmentList", "id":id},
    success: function(msg){
      $("#spn_conDepa").html(msg); 
    }  
  });
}

//obtiene los giros comerciales (telecomunicaciones, automotriz etc)
function getBusinessList(id){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getBusinessList", "id":id},
    success: function(msg){
      $("#spn_giroCom").html(msg); 
    }  
  });
}

//obtiene los puestos (gerente, ing en sistemas etc) 
function getPositionList(id){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getPositionList", "id":id},
    success: function(msg){
      $("#spn_conPuesto").html(msg); 
      $("#sel_position").change(function(){
        if("6"==$(this).val()){         
          $("#div_contPuesto_otro").show();
        }
        else{
          $("#txt_contPuesto").val("");
          $("#div_contPuesto_otro").hide();
        }     
      });
    }  
  });
}

//obtiene los giros comerciales (telecomunicaciones, automotriz etc)
function getEquipmentList(id){
  $.ajax({
    type: "POST",
    async: false,
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getEquipmentCategoryList", "id":id},
    success: function(msg){
      $("#spn_tareaEquiCat").html(msg);  

      getAvailableEquipment(0);   

      $(document.body).on('change','#sel_equipCat',function(){
        getAvailableEquipment(0);       
      }); 
    }  
  });
}

//obtiene los equipos a seleccionar para los servicios
function getValidityList(id,opt){
  var resp = "";
  $.ajax({
    type: "POST",
    async: false,
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getValidityList", "id":id},
    success: function(msg){
      resp = msg.trim();
    }  
  });
  
  if(0==opt){
    $("#spn_vigencia").html(resp);
  }
  else{
    return resp; 
  }
}

//obtiene las categorias de productos (telecomunicaciones, automotriz etc)
function getProductCategoryList(id){
  $.ajax({
    type: "POST",
    async: false,
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getProductCategoryList", "id":id},
    success: function(msg){
      $("#spn_prodCategoria").html(msg);  
      getProductTypeList(0,1);  
      getProductPromoList(0,$('#sel_prodCategoria').val());       
      $(document.body).on('change','#sel_prodCategoria',function(){
        getProductTypeList(0,$(this).val()); 
        getProductPromoList(0,$(this).val());      
      }); 
    }  
  });
}

//obtiene los giros comerciales (telecomunicaciones, automotriz etc)
function getProductTypeList(id,cat){
  var region = $("#sel_region").val();
  $.ajax({
    type: "POST",
    async: false,
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getProductTypeList", "id":id, "categoria":cat, "region":region},
    success: function(msg){
      $("#spn_prodTipo").html(msg);  
      $(document.body).on('change','#sel_prodTipo',function(){
        $("#sel_productsPromo").prop("selectedIndex",0);
      }); 
    }  
  });
}

//obtiene los enlaces en promocion vigentes
function getProductPromoList(id,cat){
  var region = $("#sel_region").val();
  var vigencia = $("#sel_validity").val();
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getProductPromoList", "id":id, "categoria":cat, "region":region, "vigencia":vigencia},
    success: function(msg){
      $("#spn_productoPromo").html(msg);       
      $(document.body).on('change','#sel_productsPromo',function(){
        $("#sel_prodTipo").prop("selectedIndex",0);
      }); 
    }  
  });
}

//obtiene el estatus de cotización
function getPriceStatusList(id){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getPriceStatList", "id":id},
    success: function(msg){
      $("#spn_cotEstat").html(msg);       
    }  
  });
}

/****Fin de apartado****/

/************Apartado de gestión de comentarios*************/

//manda a guardar el comentario nuevo para un prospecto/cliente/sitio etc. en específico
function saveCommentNew(orgType){ 
  var orgType = $("#hdn_commentOriginType").val();
  if(false==validateCommentData(orgType)){
    printErrorMsg("div_msgAlertComment_"+orgType,"Existen datos no válidos, favor de corregirlos");    
  }
  else{
    var orgId = $("#hdn_commentOriginId").val();
    var comment = $("#txt_comment_"+orgType).val();
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: {"action": "saveCommentNew", "orgId":orgId, "orgType":orgType, "comment":comment},
      beforeSend: function(){
        setLoadDialog(0,"Guardando comentario");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){
        var resp = msg.trim().split("||");
        if("OK" == resp[0]){
          getComments(orgId,orgType);
        }
        else{
          if(9!=orgType)
            printErrorMsg("div_msgAlertComment",resp[1]);
          else
            printErrorMsg("div_msgAlertWorkOrderDetails",resp[1]);
        }   
      } 
    });
  }
}

//consigue los comentarios existentes para un prospecto/cliente/sitio y abre el cuadro de diálogo
function getComments(orgId,orgType){
  switch(orgType){
    
    case 0:
      var commNameDiv = "commentProspectDialog";
    break;
    case 1:
      var commNameDiv = "commentClientDialog";
    break;
    case 2:
      var commNameDiv = "commentSiteDialog";    
    break;
    case 3:
      var commNameDiv = "commentEventDialog";
    break;
    case 12:
      var commNameDiv = "commentTaskDialog";
    break;
    case 9:
      var commNameDiv = "commentWorkOrderDialog";
    break;
  }
  $("#"+commNameDiv).load("../../libs/templates/commentdialog.php");

  $("#div_msgAlertComment").html("");
  $("#txt_comment_"+orgType+"_error").html("");
  $("#txt_comment_"+orgType).val("");
  $("#hdn_commentOriginId").val(-1);
  $("#hdn_commentOriginType").val(-1);
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getComments", "orgId":orgId, "orgType":orgType},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo comentarios");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      if(9!=orgType){
        $("#div_comments").html(msg); 
        $("#mod_comments").modal("show"); 
        $("#hdn_commentOriginId").val(orgId);
        $("#hdn_commentOriginType").val(orgType);       
        setModalResponsive("mod_comments");
      }
      else{
        $("#commentWorkOrderDialog").html(msg); 
      }
    }  
  });
}

//valida la integridad del campo de comentario
function validateCommentData(orgType){
  return $("#frm_newComment_"+orgType).validate({
   rules: {
      /*'txt_comment_'+orgType:
      {
        required: true,
      }*/
    },
    errorPlacement: function(error, element) {
        error.appendTo($('#' + element.attr("id") + '_error'));
    }
  }).form();  
}

//elimina un comentario
function deleteComment(id){
  if(confirm("¿Está seguro/a de eliminar este comentario? Esta acción no se podrá deshacer.")) {
    $.ajax({
      type: "POST",
      url: "../../libs/db/common.php",
      data: {"action": "deleteComment", "commId":id, "orgType":$("#hdn_commentOriginType").val()},
      beforeSend: function(){
        setLoadDialog(0,"Eliminando comentario");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){
        var resp = msg.trim().split("||");
        if("OK" == resp[0]){
          getComments(resp[2],$("#hdn_commentOriginType").val());
        }
        else{
          printErrorMsg("div_msgAlertComment",resp[1]);
        }   
      } 
    });
  }
}

/*********Fin de apartado***********/

/********Apartado de funciones globales para gestión de eventos**********/

//funcion que consigue las listas de eventos de cierto origen para su despliegue
function getEvents(orgId,orgType){
  $("#div_msgAlertEvents").html("");
  $("#hdn_orgId").val(orgId);
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getEvents", "orgId":orgId, "orgType":orgType},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo eventos");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){    
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#div_Events").html(resp[1]);
        
        $("#tbl_Events").DataTable({
            language: datatableespaniol,
        }); 
        $("#mod_Events").modal("show");
        
        setModalResponsive("mod_Events");
      }
      else{
        printErrorMsg("div_msgAlertEvents",resp[1]);
      }
    }  
  });
}

//funcion que abre el cuadro de dialogo para insertar un evento nuevo
function openNewEvent(){
  clearEventFields();  
  $("#txt_fechaIni").datetimepicker(datetimeOptions);
  $("#txt_fechaFin").datetimepicker(datetimeOptions);
  $("#txt_eveDurHor").datetimepicker(timeOptions);
  $("#hdn_eveAction").val(0);
  $("#mod_newEvent").modal("show");
  $("#txt_fechaFin").prop('readonly',true);
  $("#txt_fechaIni").prop('disabled',false);
  $("#txt_eveDurHor").prop('disabled',false);
  $("#sel_eveDur").prop('disabled',false);
  $("#txt_eveDurHor").prop('readonly',true);
  $("#btn_eveEstHor").prop('disabled',false);
  $("#btn_eveAddGuest").prop('disabled',true);
  $("#tbl_eveSelectedGuest").find("tr:gt(0)").remove();
  $("#tbl_eveSelectedEquip").find("tr:gt(0)").remove();
  $("#spn_eveInv").html("");
  $("#div_eveRadFact").hide();
}

//funcion que abre el cuadro de diálogo para editar el evento y manda a llenar los campos con sus valores actuales
function editEvent(id){
  clearEventFields();  
  $("#txt_fechaIni").datetimepicker(datetimeOptions);
  $("#txt_fechaFin").datetimepicker(datetimeOptions);
  $("#mod_newEvent").modal("show");
  fillEventFields(id);
}

//funcion que obtiene los campos del evento para su edición en los campos del cuadro de diálogo
function fillEventFields(id){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getEventFields", "eventId":id, "eveCat":0}, //0 que es evento normal
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo datos de evento");
    },
    success: function(msg){
      setLoadDialog(1,"");
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        getEventStatList(resp[8],1);
        getEventTypeList(resp[6]);
        getEventTimeInterval(0);
        
        $("#txt_eveTitulo").val(resp[1]);        
        $("#txt_eveDesc").val(resp[5]);
        $("#hdn_eveAction").val(1);
        $("#hdn_eveId").val(id);
        
        if(5<=resp[6]){          
          $("#btn_eveEstHor").prop('disabled',false);
          $("#txt_eveDurHor").prop('disabled',false); 
          $("#sel_eveDur").prop('disabled',false);        
        }
        else{
          $("#btn_eveEstHor").prop('disabled',true);
          $("#txt_eveDurHor").prop('disabled',true);
          $("#sel_eveDur").prop('disabled',true); 
        }
        
        $("#txt_fechaIni").val(resp[2]);
        $("#txt_fechaFin").val(resp[3]);
        getAvailablePeople(0);

        if(0!=resp[10]&&3==resp[8]){
          $("#div_eveRadFact").show();
          $('input[name="rad_eveFact"][value="'+resp[10]+'"]').prop('checked', true);
        }
        else{
          $("#div_eveRadFact").hide();
        }
        
        var invArr = JSON.parse(resp[11]);
        $.each(invArr,function(key,value){//arreglo de invitados
          addGuest(value,1);
        });
        
        var equArr = JSON.parse(resp[12]);
        $.each(equArr,function(key,value){//arreglo de equipo
          addEquipment(value,1);
        });
      }
      else{
        printErrorMsg("div_msgAlertEvent",resp[1]);       
      }
    }  
  });
}

//función que borra los datos del evento con el id seleccionado
function deleteEvent(id){
  if(confirm("¿Está seguro/a de eliminar este evento? Esta acción no se podrá deshacer.")) {    
    var orgType = $("#hdn_orgType").val();
    var orgId = $("#hdn_orgId").val();     
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: {"action": "deleteEvent", "eventId":id},
      beforeSend: function(){
        setLoadDialog(0,"Eliminando evento");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){
        var resp = msg.trim().split("||");
        if("OK" == resp[0]){
          printSuccessMsg("div_msgAlertEvents",resp[1]);
          if(0>=orgType)
            getEvents(id,orgType);
          else
            location.reload();
          $("#mod_eventDetails").modal("hide");
          getEventFilters();
        }
        else{
          printErrorMsg("div_msgAlertEvents",resp[1]);          
        } 
      }   
    }); 
  }
}

function getAvailablePeople(id){ 
  var fecIni = $("#txt_tareaFechaIni").val();
  var fecFin = $("#txt_tareaFechaFin").val();
  $("#tbl_tareaSelectedGuest").find("tr:gt(0)").remove();
  
  invitadoArray.length = 0;

  if((0<fecIni.length&&0<fecFin.length)&&(parseDatetime(fecIni).getTime() <= parseDatetime(fecFin).getTime())){ 
    $.ajax({
      type: "POST",
      async:false,
      url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
      data: {"action": "getAvailablePeople", "id":id, "fecIni":fecIni, "fecFin":fecFin},
      success: function(msg){
        $("#spn_tareaInv").html(msg);         
         
        if(!$("#sel_invitados").length)
          $("#btn_tareaAddGuest").prop('disabled',true);
        else          
          $("#btn_tareaAddGuest").prop('disabled',false);
      }  
    });    
  }
  else{
    $("#btn_tareaAddGuest").prop('disabled',true);
    $("#spn_tareaInv").html(""); 
    $("#tbl_tareaSelectedGuest").find("tr:gt(0)").remove();
    invitadoArray.length = 0;   
  }
}

function addGuest(invArr,opt){
  if(0==opt){
    var id = $("#sel_invitados").val();
    var name = $("#sel_invitados option:selected").text();
  }
  else if(1==opt){
    var id = parseInt(invArr[0].toString().replace("!",""));
    var name = invArr[1]+" "+invArr[2];
  }
  
  invitadoArray.push(id);
                                                       
  var toAppend = "      <tr id=\"tr_eveGuest_"+id+"\">"+
                 "      <td id=\"td_eveGuestId_"+id+"\">"+id+"</td>"+
                 "      <td id=\"td_eveGuestName_"+id+"\">"+name+"</td>"+
                 "      <td id=\"td_eveGuestAction_"+id+"\"><input type=\"button\" class=\"btn btn-danger btn-xs\" id=\"btn_remueveInvitado\" value=\"Quitar\" onClick=\"removeGuest("+id+");\"></td>"+                   
                 "      <td>"+                 
                 "    </tr>";
                 
    $("#sel_invitados option[value='"+id+"']").remove();            
        
    $('#tbl_tareaSelectedGuest tr:last').after(toAppend);                                                        
                                                   
}

function removeGuest(id){  
  $("#sel_invitados").append("<option value=\""+id+"\">"+$("#td_eveGuestName_"+id).text()+"</option>");  
  $("#tr_eveGuest_"+id).remove();
  
  unsetFromArray(invitadoArray,id); 
  
  var opciones = $("#sel_invitados option");

  opciones.sort(function(a,b) {
      if (a.text > b.text) return 1;
      if (a.text < b.text) return -1;
      return 0
  })
  $("#sel_invitados").empty().append(opciones);
}

function getAvailableEquipment(id){
  
  var fecIni = $("#txt_tareaFechaIni").val();
  var fecFin = $("#txt_tareaFechaFin").val();
  
  var categoria = $("#sel_equipCat").val();

  if((0<fecIni.length&&0<fecFin.length)&&(parseDatetime(fecIni).getTime() <= parseDatetime(fecFin).getTime())){ 
    $.ajax({
      type: "POST",
      async:false,
      url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
      data: {"action": "getAvailableEquipment", "id":id, "fecIni":fecIni, "fecFin":fecFin, "categoria":categoria},
      success: function(msg){
        msg = msg.trim(); 
        $("#spn_tareaEqu").html(msg); 
        
        if(!$("#sel_eveEquipo option").length)             
          $("#btn_tareaAddEquip").prop('disabled',true);
        else
          $("#btn_tareaAddEquip").prop('disabled',false);
          
        $.each(equipoArray,function(key,value){
          $("#sel_tareaEquipo option[value='"+value+"']").remove(); 

        });
      }  
    });    
  }
  else{
    $("#btn_tareaAddEquip").prop('disabled',true);
    $("#spn_tareaEqu").html(""); 
    $("#tbl_tareaSelectedEquip").find("tr:gt(0)").remove();
    equipoArray.length = 0;   
  }
}

function addEquipment(equArr,opt){
  if(0==opt){
    var id = $("#sel_eveEquipo").val();
    var name = $("[id='sel_eveEquipo'] option:selected").text();
    var cat = $("[id='sel_equipCat'] option:selected").val();
  }
  if(1==opt){
    var id = equArr[0];
    var name = equArr[2]+" ("+equArr[3]+")";
    var cat = equArr[1];
  }
  
  equipoArray.push(id);
                                                       
  var toAppend = "      <tr id=\"tr_eveEquip_"+id+"\">"+
                 "      <td id=\"td_eveEqupId_"+id+"\">"+id+"</td>"+
                 "      <td id=\"td_eveEquipName_"+id+"\">"+name+"</td>"+
                 "      <td id=\"td_eveEquipSn_"+id+"\">"+name+"</td>"+
                 "      <td id=\"td_eveEquipAction_"+id+"\"><input type=\"button\" class=\"btn btn-danger btn-xs\" id=\"btn_remueveEquipo\" value=\"Quitar\" onClick=\"removeEquipment("+id+","+cat+");\"></td>"+                   
                 "      <td>"+                 
                 "    </tr>";
                 
    $('#tbl_tareaSelectedEquip tr:last').after(toAppend);              
             
    $("#sel_eveEquipo option[value='"+id+"']").remove();            
    
    if(0>=$("#sel_eveEquipo option").length){   
      $("#sel_equipCat option:selected").prop('disabled',true); 
      $("#btn_eveAddEquip").prop('disabled',true);
      $("#sel_equipCat").val(cat);
      getAvailableEquipment(0);
    }                                          
}

function removeEquipment(id,cat){  
  $("#sel_equipCat option[value='"+cat+"']").prop('disabled',false);
  $("#sel_equipCat").val(cat); 
  
  unsetFromArray(equipoArray,id);
  $("#tr_eveEquip_"+id).remove();
  
  getAvailableEquipment(id);
}

//función que guarda un evento nuevo o guarda un evento editado (dependiendo del valor en el hidden eveAction)
function saveEventNew(){
  if(false==validateEventData()){
    printErrorMsg("div_msgAlertEvent","Existen datos no válidos, favor de corregirlos");
  }
  else{
    var titulo = $("#txt_eveTitulo").val();
    var fechaIni = $("#txt_fechaIni").val();
    var fechaFin = $("#txt_fechaFin").val();
    var eveEstat = $("#sel_eveEstat").val();
    var eveTipo = $("#sel_eveTipo").val();
    var eveDesc = $("#txt_eveDesc").val();
    var eveAction = $("#hdn_eveAction").val();
    var eveId = $("#hdn_eveId").val();
    var orgType = $("#hdn_orgType").val();
    var id = $("#hdn_orgId").val();
    var factible = $('input[name="rad_eveFact"]:checked').val();
    var jsonInvitadoArray = JSON.stringify(invitadoArray);
    var jsonEquipoArray = JSON.stringify(equipoArray);
    
    if(undefined!=$("#hdn_orgSiteType").val()){
      var orgSiteType = $("#hdn_orgSiteType").val(); 
    }
    
    $.ajax({
      type: "POST",
      url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
      data: {"action": "saveEvent", "titulo":titulo, "fechaIni":fechaIni, "fechaFin":fechaFin, "eveEstat":eveEstat, "eveTipo":eveTipo, "eveDesc":eveDesc, "eveAction":eveAction, "eveId":eveId, "orgType":orgType, "id":id, "factible":factible, "invitadoArray":jsonInvitadoArray, "equipoArray":jsonEquipoArray, "orgSiteType":orgSiteType},
      beforeSend: function(){
        setLoadDialog(0,"Guardando evento");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(response){
        var resp = (response).trim().split('||');
        var errCod = resp[0];
        var errMsg = resp[1];
        if("OK"== errCod){
          printSuccessMsg("div_msgAlertEvent",errMsg);
          if(1>=orgType)
            getEvents(id,orgType); 
          else
            location.reload();                             
          $("#mod_newEvent").modal("hide");
          $("#mod_eventDetails").modal("hide");
        }
        else{
          printErrorMsg("div_msgAlertEvent",errMsg);                                                      
        }
      },
      error: function(){
        printErrorMsg("div_msgAlertEvent","Ocurrió un error al enviarse los datos para su guardado"); 
      }    
    });
  }
}

//funcion que limpia los campos del formulario de evento
function clearEventFields(){
  $("#div_msgAlertEvent").html("");
  $("#txt_eveTitulo_error").html("");
  $("#txt_fechaIni_error").html("");
  $("#txt_fechaFin_error").html("");
  $("#sel_eveEstat_error").html("");
  $("#sel_eveTipo_error").html("");
  $("#txt_eveDesc_error").html("");
  
  $("#txt_eveTitulo").val("");
  $("#txt_fechaIni").val("");
  $("#txt_fechaFin").val("");
  
  $("#txt_eveDurHor").val("00:00:00");
  $("#sel_eveDur").val(0);
  
  $("#sel_eveEstat").val("");
  $("#txt_eveDesc").val("");
  $("#hdn_eveAction").val(-1);
  $("#hdn_eveId").val(-1);
  
  $("#lbl_eveDurDia").html("0");
  $("#lbl_eveDurDiaText").html("&nbsp;día");  
  getEventStatList(0,1);
  getEventTypeList(0);
  getEventTimeInterval(0);
  getEquipmentList(0);
  
  invitadoArray.length = 0;
  equipoArray.length = 0;
    
  $("#slr_eveDurDia").slider({
    animate: false,
    range: "min",
    value: 0,
    min: 0,
    max: 31,
    sliderVal: '1 day',
    step: 1,
    slide: function (event, ui){
      value = ui.value;
      //days = value % 31
      days = value;

      if (days > 1 && days != 0){
        dayString = '&nbsp;dias';
      } 
      else{
        dayString = '&nbsp;dia';
      }
      $("#lbl_eveDurDia").html(days);
      $("#lbl_eveDurDiaText").html(dayString);
    }
  }).each(function(){
    var slmax = $(this).slider("option", "max");
    var slmin = $(this).slider("option", "min");
    var vals = slmax - slmin;
    for (var i=slmin; i<=vals; i++){      
      var el = $('<label>|</label>').css('left',(i/vals*100)+'%');    
      $( "#slr_eveDurDia" ).append(el);      
    }    
  });
}

//setea la hora de fin de evento con los valores en el slider de dias y las horas en el timepicker
function eventAddHour(){
  if(0>=($("#txt_fechaIni").val()).length){
    alert("Debe especificar una fecha de inicio");
  }
  else if((/^\d{2}\:\d{2}\:\d{2}$/i.test($("#txt_eveDurHor").val())==false)||(0>=($("#txt_eveDurHor").val()).length)){
    alert("El campo de horas a agregar no es válido"); 
  }
  else{
    var canTiempo   = $("#txt_eveDurHor").val().split(":");
    var canDias     = $("#lbl_eveDurDia").text();
    //var interTiempo = $("#sel_intervalo_tiempo").val(); 
    var fecHor = ($("#txt_fechaIni").val()).split(" "); 
   
    var fecIni      = fecHor[0].split("-"); 
    var horIni      = fecHor[1].split(":"); 
    var fechaIni    = new Date(fecIni[0],fecIni[1]-1,fecIni[2],horIni[0],horIni[1],horIni[2]);
    var fechaFin    = new Date(fecIni[0],fecIni[1]-1,fecIni[2],horIni[0],horIni[1],horIni[2]);
    
    fechaFin.setHours(fechaIni.getHours()+parseInt(canTiempo[0])); 
    fechaFin.setMinutes(fechaIni.getMinutes()+parseInt(canTiempo[1]));
    fechaFin.setDate(fechaIni.getDate()+parseInt(canDias));
    
    var fecFin = fechaFin.getFullYear()+"-"+("0" +(fechaFin.getMonth()+1)).substr(-2)+"-"+("0" +fechaFin.getDate()).substr(-2)+" "+("0" +fechaFin.getHours()).substr(-2)+":"+("0" +fechaFin.getMinutes()).substr(-2)+":"+("0" +fechaFin.getSeconds()).substr(-2);

    $("#txt_fechaFin").val(fecFin);
    $("#txt_eveDurHor").val("00:00:00");  
  }  
}

//función que valida la inegridad de lso datos introducidos en el formulario devuelve true si todos son correctos
function validateEventData(){ 
  addValidationRules(3);
  
  return $("#frm_newEvent").validate({
    ignore:':disabled:not(#txt_fechaFin, #txt_fechaIni)',
    rules: {
      txt_fechaIni:
      {
        isDateTime: true,
        isLaterThanToday: 
        {
          depends: function(){
            if(3<=$("#sel_eveEstat").val())
              return false;
            else
              return true;
          }
        }
      },
      txt_fechaFin:
      {
        isDateTime: true,
        isLaterThanDate: 'txt_fechaIni'
      },
      sel_eveTipo:
      {
        isSomethingSelected: true
      },
      sel_eveEstat:
      {
        isSomethingSelected: true
      }
    },
    errorPlacement: function(error, element){
      error.appendTo($('#' + element.attr("id") + '_error'));
    }
  }).form(); 
}

function goToCalendarDate(date){
  location.href = '../calendario/index.php?date='+date;
}

//carga funciones extras por el evento que se especifique, como la apertura de ventana de detalles o los botones de borrado y edición
function eventRender(event,element){
  element.attr('title', event.tip);
  element.find('.fc-title').after("<br/><i>\"" + (event.description).substring(0,25) + "...\"</i>");
  element.find('.fc-title').after("<br/><b>Agregado por:</b>" + event.user);
  element.click(function(){

    if(1==event.modify){
      var buttonEditAppend = "<button type=\"button\" id=\"btn_eventEdit_"+event.id+"\" title=\"Editar Evento\" class=\"btn btn-xs btn-success\" onClick=\"editEvent("+event.id+");\"><span class=\"glyphicon glyphicon-edit\"> Editar</span></button>&nbsp;";
    }
    else{
      var buttonEditAppend = "";
    }
    if(1==event.delete){
      buttonEditAppend += "<button type=\"button\" id=\"btn_eventDelete_"+event.id+"\" title=\"Eliminar Evento\" class=\"btn btn-xs btn-danger\" onClick=\"deleteEvent("+event.id+");\"><span class=\"glyphicon glyphicon-remove\"> Eliminar</span></button>&nbsp;";
    }   
    buttonEditAppend += "<button type=\"button\" id=\"btn_eventComments_"+event.id+"\" title=\"Ver Comentarios\" class=\"btn btn-xs btn-info\" onClick=\"getComments("+event.id+",3);\"><span class=\"glyphicon glyphicon-comment\"> Comentarios</span></button>&nbsp;";
    
    $("#div_eventDetails").html("<h4><b>"+event.title+"</b></h4>"+buttonEditAppend+"<br>"+
                                "<b>Agregado Por: </b>"+event.user+"<br>"+
                                "<b>Fecha y hora de inicio: </b>"+moment(event.start).format('MMMM DD h:mm A')+"<br>"+
                                "<b>Fecha y hora de fin: </b>"+moment(event.end).format('MMMM DD h:mm A')+"<br>"+
                                "<b>Descripción: </b>"+event.description+"<br>"+
                                "<b>Estatus: </b>"+event.status+"<br>"+
                                "<b>Invitados Confirmados:</b><br>"+event.guests+
                                "<b>Invitados Sin Confirmar:</b><br>"+event.inv+
                                "<b>Invitados Cancelados:</b><br>"+event.cancel+
                                "<b>Equipo:</b><br>"+event.equipment);//here
    $("#mod_eventDetails").modal("show"); 
  });
}

function confirmEveAssist(eveId,assistOpt){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",
    data: {"action": "confirmEveAssist", "eventId":eveId, "assistOption":assistOpt},
    beforeSend: function(){
      setLoadDialog(0,"Confirmando Asistencia");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = (msg).trim().split('||');
      alert(resp[1]);
    }  
  });
}

/****Fin de apartado****/


/********Apartado de funciones globales para gestión de cotizaciones**********/

//quita el texto en las cajas de texto para que el usuario pueda escribir
$("#txt_prodCant").click(function(){
  $("#txt_prodCant").val("");
});

$("#txt_prodCant").blur(function(){
  if(""==$("#txt_prodCant").val())
    $("#txt_prodCant").val("0");
});

$("#txt_enlSub").click(function(){
  $("#txt_enlSub").val("");
});

$("#txt_enlSub").blur(function(){
  if(""==$("#txt_prodCant").val())
    $("#txt_enlSub").val("0");
});

//funcion que abre el cuadro de dialogo para insertar una cotización nueva
function openNewPrice(){
  subtotal = 0;
  total = 0;
  impuesto = 0;
  enlaceArray.length = 0;
  productArray.length = 0;
  telefoniaArray.length = 0;
  pricAuthId = -1;
  
  $("#contactPriceDialog").load("../../libs/templates/contactdialog.php");//carga cuadro de dialogo en el modulo
  
  $("#hdn_priceAction").val(0);
  $("#hdn_contactId").val(-1);
  $("#hdn_contactId_error").html("");
  $("#txt_cotNota_error").html("");
  $("#tbl_selectedProducts").find("tr:gt(0)").remove();
  $("#td_prodSubtotal").html(""); 
  $("#td_prodImpuesto").html("");
  $("#td_prodTotal").html("");
  //clearpricefields
  $("#txt_prodCant").val(0);
  $("#sel_products").val(0);
  $("#txt_enlCant").val(0);
  $("#txt_enlSub").val(0);
  $("#sel_connections").val(0);
  $("#txt_telCant").val(0);
  $("#sel_telefonia").val(0);
  $("#txt_cotNota").val("");
  $("#div_msgAlertPrice").html(""); 
  //clearPriceFields();  
  $("#sel_connections").prop('disabled',false);
  $("#txt_enlSub").prop('disabled',false);
  $("#btn_cargaEnlace").prop('disabled',false);
  $("#btn_cargaTelefonia").prop('disabled',false);
  $("#btn_buscarContactos").prop('disabled',false);
  $("#tr_selectedContact4").html("");
  $("#div_msgAlertPrice").html("");
  $("#div_restValTag").html("");
  getRegionList(0); 
  getValidityList(0,0);
  
  $("#mod_priceInsert").modal("show");
}

//obtiene la lista de cotizaciones para un prospecto/cliente/etc
function getPrices(orgId,orgType,option){
  $("#hdn_orgId").val(orgId);
  $("#hdn_orgType").val(orgType);
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",
    data: {"action": "getPrices", "orgId":orgId, "orgType":orgType,"orgSiteType":$("#hdn_orgSiteType").val(),"option":option},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo cotizaciones");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      
      if(2==orgType)
        $("#div_sitePrices").html(msg);
      else{
        $("#div_prices").html(msg);
        $("#mod_prices").modal("show");
        setModalResponsive("mod_prices");
      }
      $("#tbl_prices").DataTable({
        language: datatableespaniol,
      });
    }  
  });
}

//obtiene los detalles de la cotización
function getPriceDetails(priceId){
  $.ajax({
    type: "POST",
    //async:false,
    url: "../../libs/db/common.php",
    data: {"action": "getPriceDetails", "priceId":priceId, "orgType":$("#hdn_orgType").val(), "option":0},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo detalles de la cotización");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      $("#div_priceDetails").html(msg);     
      $("#mod_priceDetails").modal("show"); 
           
      setModalResponsive("mod_priceDetails");
    }  
  });
}

//funcion que manda a llamar los datos de una cotización existente y los agrega en el formulario de captura de cotizaciones
function editPrice(priceId){  
  openNewPrice();  
  $("#hdn_pricId").val(priceId);
  $("#hdn_priceAction").val(1);
  var contactoId = 0; 
  var prodArray = "";

  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",
    data: {"action": "getPriceDetails", "priceId":priceId, "orgType":$("#hdn_orgType").val(), "option":1},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo datos de la cotización");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = (msg).trim().split('||');
      if("ERROR"== resp[0]){
        printErrorMsg("div_msgAlertPrice",resp[1]);
      }
      else{
        $("#txt_cotNota").val(resp[2]);
        pricAuthId = resp[7];
        getRegionList(resp[5]);
        getValidityList(resp[3],0);
        
        if(0<pricAuthId){
          $("#div_restValTag").html("<span class=\"glyphicon glyphicon-alert\">Autorizado por "+resp[8]+"</span>");
        }
        else{
          $("#div_restValTag").html("");
        }
        
        contactoId = parseInt(resp[0]); 
        getContactList(2);
        addContact(contactoId,4); //el 4 indica que se está añadiendo a un formulario de cotizacion 
        
        if(undefined!==resp[1]&&0<resp[1].length){
          prodArray = JSON.parse(resp[1]);
          if(0<prodArray.length){
            for(var a=0;a<prodArray.length;a++){
              addProdToPrice(1,prodArray[a]);
            }
          } 
        }
        else{
          printErrorMsg("div_msgAlertPrice","No existen productos para esta cotización"); 
        }       
      }                     
    }
  });
}

function addProdToPrice(mode,prodArr){
  var prodTipo;
  var prodCant; 
  var prodEsPri;
  var prodEsAut;
  var prodPromo;
  var prodTipId;
  var prodVig;
  var relId;
  var promoTxt;
  
  if(0==mode){
    relId = -1;
    prodTipo = $("#sel_prodTipo").val();
    prodCant = $("#txt_prodCant").val();
    prodEsPri = -1;
    prodEsAut = 0;
    prodPromo = $("#sel_productsPromo").val();
    prodVig = $("#sel_validity").val();
    prodCat = $("#sel_prodCategoria").val();
  }
  else if(1==mode){
    relId = prodArr[0];
    prodTipo = prodArr[1];
    prodCant = prodArr[2];
    prodEsPri = prodArr[3];
    prodEsAut = prodArr[4];
    prodPromo = prodArr[1];
    prodVig = prodArr[5];
    prodCat = prodArr[6];
  } 
  
  if(!$.isNumeric(prodCant)||(prodCant%1!=0)){
    alert("La cantidad debe ser un valor numérico entero");
  }
  else{
    var tempArr = new Array(); 
  
    if(0>=prodCant||0>=prodCant.length){
      alert("Debe introducir una cantidad de producto");
      return;
    }
    if(0>=prodTipo&&0>=prodPromo){
      alert("Debe seleccionar un servicio o promoción");
      return; 
    }
    if(0<prodPromo){
      prodTipId = prodPromo;
      promoTxt = "(Promoción)";
    }
    else{
      prodTipId = prodTipo;
      promoTxt = "";
    }
    
    $.ajax({
      async: false,
      type: "POST",
      url: "../../libs/db/common.php",
      data: {"action": "getProductTypeData", "prodTipo":prodTipId, "prodCant":prodCant, "relId":relId, "prodVig":prodVig, "prodEsPri": prodEsPri},
      beforeSend: function(){
        setLoadDialog(0,"Obteniendo datos del producto para agregar");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){ 
        var resp = msg.trim().split("||"); 
              
        var relid;
        var id;
        var prodNom;
        var tipSer;
        var precUn;
        var precTot; 
        var removeButton; 
              
        if("OK" == resp[0]){
              
              //OK||78||2||INTCORWIFI10ASIM-10KM||Internet Corporativo WiFi 10x2 Mbps hasta 10 Km||1487.50||1487.50||1
              
          relid = resp[1];
          id = resp[2];
          prodNom = resp[3];
          tipSer = resp[4];
          precUn = resp[5];
          precTot = resp[6];
          
          if(5==resp[7]){
            removeButton = "";
          }
          else{
            removeButton = "     <button type=\"button\" title=\"Remover\" class=\"btn btn-xs btn-danger\" "+
                                 "onClick=\"removeProduct('"+relid+"');\">"+
                                 "<span class=\"glyphicon glyphicon-remove\"></span>"+
                           "     </button>";
          }
                  
          tempArray = [relid,id,prodCant,prodEsPri,prodEsAut,prodVig,prodCat]; //prodEsPri -1 es que es el precio del sistema
          productArray.push(tempArray);
                          
          $("#sel_validity").prop('disabled', true);
          if(0<prodVig)
            var vigName = "a "+$("#sel_validity option[value='"+prodVig+"']").text();
          else
            var vigName = "pago único";
          //$("#sel_prodTipo option[value='"+id+"']").prop('disabled',true);
          
          if(-1!=prodEsPri)
            var glyphWar = "<span class=\"glyphicon glyphicon-alert\" style='color:red' title='Precio Especial'></span></td>"; 
          else
            var glyphWar = "";
              
          $('#tbl_selectedProducts tr:last').after("  <tr id=\"tr_selProd_"+relid+"\">"+
                                                   "    <td id=\"td_prodCant_"+relid+"\">"+prodCant+"</td>"+
                                                   "    <td id=\"td_tipSer_"+relid+"\">"+prodNom+" ("+vigName+")</td>"+
                                                   "    <td id=\"td_tipSer_"+relid+"\">"+tipSer+"</td>"+
                                                   "    <td id=\"td_precUn_"+relid+"\">"+precUn+
                                                   "      <button type=\"button\" title=\"Editar\" class=\"btn btn-xs btn-default\" onClick=\"editUnitPrice('"+relid+"');\">"+
                                                   "<span class=\"glyphicon glyphicon-edit\"></span>"+
                                                   "     </button></td>"+
                                                   "    <td id=\"td_espPricIc_"+relid+"\">"+glyphWar+"</td>"+
                                                   "    <td id=\"td_precTot"+relid+"\">"+precTot+"</td>"+
                                                   "   <td>"+removeButton+"</td>"+
                                                   "  </tr>");
                                                        
          subtotal = subtotal+parseFloat(precTot);                                              
          impuesto = redondeo((subtotal*0.16),2);
          total    = subtotal+impuesto;
                                                   
          $("#td_prodSubtotal").html("$"+subtotal.toFixed(2)); 
          $("#td_prodImpuesto").html("$"+impuesto.toFixed(2));
          $("#td_prodTotal").html("$"+total.toFixed(2));
          
          $("#txt_prodCant").val("0");
          $("#sel_prodTipo").val("0");
        }
        else{
          printErrorMsg("div_msgAlertPrice",resp[1]);
        }         
      }  
    });
  }
}

//quita un producto o enlace de la lista
function removeProduct(Id){
  //var valor = ($("#td_prodTotalCant_"+prodId).text()).replace("$","");
  //subtotal = subtotal-parseFloat(valor);
  var valor = 0;
  var prodcnt = 0;
  
  valor = parseFloat($("#td_precTot"+Id).text());
  $("#sel_products option[value='"+Id+"']").prop('disabled',false);
  unsetFromArray(productArray,Id);
  $("#tr_selProd_"+Id).remove();
  
  $.each(productArray,function(key,value){//arreglo de invitados
    if(0<value[5])
      prodcnt++;
  });

  if(0>=prodcnt++)
    $("#sel_validity").prop('disabled', false);
  else
    $("#sel_validity").prop('disabled', true);
       
  subtotal = subtotal-valor;
  impuesto = redondeo((subtotal*0.16),2);
  total    = subtotal+impuesto;
  
  $("#td_prodSubtotal").html("$"+subtotal.toFixed(2)); 
  $("#td_prodImpuesto").html("$"+impuesto.toFixed(2));
  $("#td_prodTotal").html("$"+total.toFixed(2));
}

//invoca la opción de editar un precio unitario a uno personalizado
function editUnitPrice(id){
    
  var initValue = $("#"+"td_precUn_"+id).text();
  $("#"+"td_precUn_"+id).html("<input type=\"text\" id=\"txt_espPrice_"+id+"\" name=\"txt_espPrice_"+id+"\" class=\"form-control\" placeholder=\"\" maxlength=\"7\" size=\"4\" required>"+
                          "     <button type=\"button\" title=\"Editar\" class=\"btn btn-xs btn-success\" onClick=\"setUnitPrice('"+id+"');\">"+
                          "<span class=\"glyphicon glyphicon-ok\"></span>"+
                          "     <button type=\"button\" title=\"Cancelar\" class=\"btn btn-xs btn-danger\" onClick=\"resetUnitPrice('"+id+"');\">"+
                          "<span class=\"glyphicon glyphicon-remove\"></span>"+
                          "     </button></td>"+
                          "<input type=\"hidden\" id=\"hdn_unitId_"+id+"\" name=\"hdn_unitId_"+id+"\" class=\"form-control\" value="+initValue+">");
   $("#txt_espPrice_"+id).focus(); 
}

//cancela la modificación del precio unitario de un producto
function resetUnitPrice(id){

  var initValue = parseFloat($("#hdn_unitId_"+id).val());
  $("#"+"td_precUn_"+id).html(initValue.toFixed(2)+"     <button type=\"button\" title=\"Editar\" class=\"btn btn-xs btn-default\" onClick=\"editUnitPrice('"+id+"');\">"+
                                                   "<span class=\"glyphicon glyphicon-edit\"></span>"+
                                                   "     </button>"); 
                                    
  /*if(1==modFlag){
    $("#"+tdUnitId+id).append("<span class=\"glyphicon glyphicon-alert\"></span>");
  }
  else if(2==modFlag){
    $("#"+tdUnitId+id).append("<span class=\"glyphicon glyphicon-alert\" style=\"color:red\"></span>");  
  }
  else{
    $("#"+tdUnitId+id).html("");
  }*/
}

//asigna el precio unitario personalizado al producto o enlace            
function setUnitPrice(id){
  //todo agregar funciones que asignan el precio especial capturado en la celda de la tabla y hacer los calculos  
  var newUnitVal = parseFloat($("#txt_espPrice_"+id).val());
  var initValue = parseFloat($("#hdn_unitId_"+id).val());
  var cant = 0;
  var subida = 0;
  var parcialInit = 0;
  var parcialNew = 0;
  var tempArray = new Array();
  
  if(isNaN(newUnitVal)){
    resetUnitPrice(id);
    return;
  }
  
  cant = parseInt($("#td_prodCant_"+id).text()); 
  parcialNew = cant*newUnitVal;    
  parcialInit = cant*initValue;  
  
  $("#td_espPricIc_"+id).html("<span class=\"glyphicon glyphicon-alert\" style='color:red' title='Precio Especial'></span></td>");
   
  $("#td_precUn_"+id).html(newUnitVal.toFixed(2)+"     <button type=\"button\" title=\"Editar\" class=\"btn btn-xs btn-default\" onClick=\"editUnitPrice('"+id+"');\">"+
                                                "<span class=\"glyphicon glyphicon-edit\"></span>"+                                  
                                                "     </button>");
                                      
  tempArray = extractArrayFromArray(productArray,id);
  unsetFromArray(productArray,id);
  
  tempArray[0][3]= newUnitVal.toFixed(2); //[relid,id,cant,newUnitVal.toFixed(2),-1,1];
  tempArray[0][4]= -1;
  productArray.push(tempArray[0]);
                          
  $("#td_precTot"+id).html(parcialNew.toFixed(2));
  
  subtotal = subtotal-parcialInit;                                  
                                    
  subtotal = subtotal+parcialNew;
  impuesto = redondeo((subtotal*0.16),2);
  total    = subtotal+impuesto;  

  $("#td_prodSubtotal").html("$"+subtotal.toFixed(2)); 
  $("#td_prodImpuesto").html("$"+impuesto.toFixed(2));
  $("#td_prodTotal").html("$"+total.toFixed(2));
  
}


function priceAuth(cotId,prodId,opt,precAct){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",
    data: {"action": "priceAuth", "pricId":cotId, "prodId": prodId, "option": opt, "precAct": precAct},
    beforeSend: function(){
      setLoadDialog(0,"Autorizando Precio");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){        
      alert(msg);     
    }  
  });
}

//Guarda la cotización. nueva si #hdn_priceaction=0 y actualiza existente si #hdn_priceaction=1
function savePriceNew(){
  if(false!=validatePriceData()){
    var jsonProductArray = JSON.stringify(productArray);
    $.ajax({
      type: "POST",
      url: "../../libs/db/common.php",
      data: {"action": "savePrice", "option": $("#hdn_priceAction").val(),"contactId":$("#hdn_contactId").val(), 
             "pricId":$("#hdn_pricId").val(), "notas": $("#txt_cotNota").val(), "vigencia":$("#sel_validity").val(), 
             "productArr":jsonProductArray, "orgId":$("#hdn_orgId").val(), "orgType":$("#hdn_orgType").val(), "region":$("#sel_region").val()},
      
      beforeSend: function(){
        setLoadDialog(0,"Guardando cotización");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){
        var resp = msg.trim().split("||");
        var errCod = resp[0];
        if("OK" == errCod){
          printSuccessMsg("div_msgAlertPrice",resp[1]);                          
          $("#mod_priceInsert").modal("hide"); 
          getPrices($("#hdn_orgId").val(),$("#hdn_orgType").val(),0);
          /*if(0==$("#hdn_orgType").val())
            getProspectRegs(2,'cotizacionLi');*/
        }
        else{
          printErrorMsg("div_msgAlertPrice",resp[1]);
          return false;       
        }
      }  
    }); 
  }
}

//elimina una cotización
function deletePrice(priceId){
  if(confirm("¿Está seguro/a de eliminar esta cotización? Si no existen otras cotizaciones para este prospecto, éste volverá a su fase de prospección. Esta acción no se podrá deshacer.")) {
    $.ajax({
      type: "POST",
      url: "../../libs/db/common.php",
      data: {"action": "deletePrice", "priceId":priceId, "orgType":$("#hdn_orgType").val(), "orgId":$("#hdn_orgId").val()},
      beforeSend: function(){
        setLoadDialog(0,"Eliminando cotización");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){        
        var resp = msg.trim().split("||");
        var errCod = resp[0];
        if("OK" == errCod){
          printSuccessMsg("div_msgAlertPrices",resp[1]);
          getPrices($("#hdn_orgId").val(),$("#hdn_orgType").val(),0);
        }
        else{
          printErrorMsg("div_msgAlertPrices",resp[1]);                   
        }        
      }  
    });
  }
}

//valida la integridad de los datos de captura de una cotización
function validatePriceData(){
  jQuery.validator.addMethod("isContactSelected", function(value, element){  
    if(-1<value){
      return true;
    }
    else{
      return false;
    }  
  }, "Debe seleccionar un contacto");
  
  if(0>=productArray.length){
    printErrorMsg("div_msgAlertPrice","Debe incluir al menos un producto");
    return false;                             
  }
  
  return $("#frm_newPrice").validate({
    ignore: ':hidden:not(#hdn_contactId)',
    rules: {
      hdn_contactId:
      {
        isContactSelected: true
      }
    },
    errorPlacement: function(error, element) {
        error.appendTo($('#' + element.attr("id") + '_error'));
    }
  }).form();
}

//abre el cuadro de diálogo para envío de correo electrónico de cotización o lo visualiza
function printPrice(id,option){
  $("#contactPriceDialog").load("../../libs/templates/contactdialog.php");//carga cuadro de dialogo en el modulo
  if(0==option){//si lo va a visualizar en el explorador
    //window.location.href = "../../libs/db/pricepdf.php?id='"+id+"'&orgType="+$("#hdn_orgType").val()+"&option="+option;
    window.open("../../libs/db/pricepdf.php?id='"+id+"'&orgType="+$("#hdn_orgType").val()+"&option="+option,'_blank');
    $("#hdn_pricMailOpt").val(0);
  }
  if(1==option){//si lo va a enviar por correo
    contactoArray.length = 0;
    $("#tbl_selectedMailContact").find("tr:gt(0)").remove();
    $("#div_priceMsgContent").code("");
    $("#txt_priceMsgSubject").val("");
    $("#hdn_pricMailOpt").val(1);
    $("#hdn_pricMailCont").val("");
    $("#mod_sendPricePdf").modal("show");
    $("#hdn_cotId").val(id);
    $("#hdn_orgTypeMail").val($("#hdn_orgType").val());
    $("#div_msgAlertPriceSendEmail").html("");
    
    $("#div_priceMsgContent").summernote({
      lang: 'es-ES',
      height: 300, 
      minHeight: 100,
      maxHeight: 600 
    }); 
    $('#mod_sendPricePdf').on('hide.bs.modal', function(){
      $("#contactPriceDialog").html("");
    });
    $('#mod_priceDetails').on('hide.bs.modal', function(){
      $("#contactPriceDialog").html("");
    });
  }
}

//valida los datos del contacto antes de agregarse al formulario de nuevo/editar contacto, prospecto, sitio etc.
function validatePriceEmailData(){
  
  if((0==contactoArray.length)){
    printErrorMsg("div_msgAlertPriceSendEmail","Debe incluir al menos un contacto/destinatario");
    return false;                             
  }  
  if(0>=$("#div_priceMsgContent").code().length){
    printErrorMsg("div_msgAlertPriceSendEmail","Por favor incluya un mensaje");
    return false; 
  }  
  if(0>=$("#txt_priceMsgSubject").val().length){
    printErrorMsg("div_msgAlertPriceSendEmail","Por favor incluya un asunto");
    return false; 
  }
}

//envia por correo la cotización junto con el mensaje personalizado
function sendPriceEmail(){
  if(false==validatePriceEmailData()){   
    alert("Por favor verifique los datos");
    return;
  }
  else{    
    var msgHtmlContent = $("#div_priceMsgContent").code();
    var msgPlainContent = $("#div_priceMsgContent").code().replace(/<\/p>/gi, "\n").replace(/<br\/?>/gi, "\n").replace(/<\/?[^>]+(>|$)/g, "");
    var msgSubject = $("#txt_priceMsgSubject").val();
    var pricMailContact = JSON.stringify(contactoArray);
    //alert(pricMailContact);
    var procMailOpt = $("#hdn_pricMailOpt").val();
    var cotId = $("#hdn_cotId").val();
    var orgType = $("#hdn_orgType").val();
    
    $.ajax({
      type: "POST",
      url: "../../libs/db/pricepdf.php",
      async: false,
      data: {"action": "pricePDFGenerate", "msgHtmlContent":msgHtmlContent, "msgPlainContent":msgPlainContent, "pricMailContact":pricMailContact, "procMailOpt":procMailOpt, "cotId":cotId, "orgType":orgType, "msgSubject":msgSubject},
      
      beforeSend: function(){
        setLoadDialog(0,"Enviando cotización por correo");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){
        var resp = msg.trim().split('||');
        var errCod = resp[0];
        if("OK"== errCod){
          printSuccessMsg("div_msgAlertPrices",resp[1]);
          $("#mod_sendPricePdf").modal("hide");
        }
        else{
          printErrorMsg("div_msgAlertPriceSendEmail",resp[1]);                                                    
        }      
      },
      error: function() {
        printErrorMsg("div_msgAlertPriceSendEmail","Ocurrió un error al enviarse los datos");   
      }  
    });
  }
}

//Abre ventana de diálogo para añadir una cotización como anexo a un contrato
function openAddAttachContract(cotId){ 
  $("#hdn_cotId").val(cotId);  
  $.ajax({
      type: "POST",
      url: "../../libs/db/common.php",
      data: {"action": "getContractsPerClient", "orgId":$("#hdn_orgId").val(), "orgType":$("#hdn_orgType").val()},
      beforeSend: function(){
        setLoadDialog(0,"Obteniendo cotizaciones");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){      
        setModalResponsive('mod_clientContractAtt');  
        $("#div_contrConSelAtt").html(msg);
      }  
    });
  $("#mod_clientContractAtt").modal("show");
}


function attachToContract(conId){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",
    data: {"action": "attachToContract", "cotId": $("#hdn_cotId").val(), "conId":conId},
    beforeSend: function(){
      setLoadDialog(0,"Anexando cotización a contrato");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){      
      var resp = msg.trim().split('||');
      var errCod = resp[0];
      if("OK"== errCod){
        printSuccessMsg("div_msgAlertPrices",resp[1]);
        $("#mod_clientContractAtt").modal("hide");
        getPrices($("#hdn_orgId").val(),$("#hdn_orgType").val(),0);
      }
      else{
        printErrorMsg("div_msgAlertContractAtt",resp[1]);                                                    
      }   
    }  
  });
}

//funcion que consigue las listas de enlaces de una cotización
function getPriceConn(cotId){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getPriceConnections", "cotId":cotId, "orgType":$("#hdn_orgType").val()},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo enlaces de cotización");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      $("#tbl_priceConnList").html(msg);
      
      $("#tbl_priceConnList").DataTable({
          language: datatableespaniol,
      });
      setModalResponsive('mod_priceConnList');
      $("#mod_priceConnList").modal("show"); 
    }  
  });
}

/****Fin de apartado****/


/****Apartado de Funciones de gestión de contratos ****/
//función que obtiene la lista de contratos de un cliente opt es para definir si el contrato ya está guardado o no
function openNewContract(cotId,opt){
  contactoArray.length = 0;
  contactoContrArray.length = 0;
  var orgType = $("#hdn_orgType").val();
  $("#div_msgAlertClientContract").html("");
  $("#hdn_cotId").val(cotId);
  $("#txt_contNota").val("");
  $("#txt_contFechaCon").val("");
  
  getContactList(10);
  getContractRepList(1);  
  if(0<opt){
    getContract(opt);
  }
  else{
    //$("#btn_saveSitCon").prop('disabled',false); 
    setModalResponsive('mod_clientNewContract');
    $("#mod_clientNewContract").modal("show"); 
    $("#btn_saveSitCon").removeAttr("onClick");
    $("#btn_saveSitCon").off("click");
    $("#btn_saveSitCon").on("click", function(){
      saveContract(0);
    });
    $("#btn_prevSitCon").removeAttr("onClick");
    $("#btn_prevSitCon").off("click");
    $("#btn_prevSitCon").on("click", function(){
      getContract(0);
    });
    $("#btn_sendSitConEmail").removeAttr("onClick");
    $("#btn_sendSitConEmail").off("click");
    $("#btn_sendSitConEmail").on("click", function(){
      openContractEmail(opt);
    });
  }
}

function getContractRepList(id){
  $.ajax({
    type:"POST",
    url:"ajax.php",
    data:{"action":"getContractRepList"},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo datos de contrato");
    },
    complete:function(){
      setLoadDialog(1,"");
    },
    success:function(msg){
      $("#div_contrRepSel").html(msg);
      $("input[name='rad_contratoRep'][value="+id+"]").prop('checked',true);
      $("#tbl_contractReps").DataTable({
        language: datatableespaniol,
      });
    }
  });
}

//Genera el formato del contrato para su previsualización
function getContract(mode){
  var opt = $("input[type='radio'][name='rad_contSer']:checked").val();
  var rep = $("input[type='radio'][name='rad_contratoRep']:checked").val();
  
  if(0==mode){ //solo previsualizar. No datos guardados
    var con = JSON.stringify(contactoContrArray);
    if(0>=contactoContrArray.length)
      alert("Debes seleccionar al menos un contacto");
    else
      window.open("../../libs/db/contractpdf4.php?cotId='"+$("#hdn_cotId").val()+"&conSer="+opt+
                  "&conCon="+con+"&conMode="+mode+"&tipIns="+$("#rad_contSer").val()+"&orgType="+$("#hdn_orgType").val()+"&option="+mode+"&repLeg="+rep,'_blank');
  }
  else if(1==mode||2==mode||3==mode){ //1 con datos guardados y firmado no se puede editar 2 es un anexo 3 aceptado por cte
    window.open("../../libs/db/contractpdf4.php?cotId='"+$("#hdn_cotId").val()+"&tipIns="+$("#rad_contSer").val()+"&orgType="+$("#hdn_orgType").val()+"&option="+mode,'_blank');
  }
  else if(4==mode){ //guardado. Libre para editar 
    $("#btn_sendSitConEmail").removeAttr("onClick");
    $("#btn_sendSitConEmail").off("click");
    $("#btn_sendSitConEmail").on("click", function(){
      openContractEmail(mode);
    });
    
    $.ajax({
      type:"POST",
      url:"ajax.php",
      data:{"action":"getContractData","cotId":$("#hdn_cotId").val()},
      beforeSend: function(){
        setLoadDialog(0,"Obteniendo datos de contrato");
      },
      complete:function(){
        setLoadDialog(1,"");
      },
      success:function(msg){
        var resp = msg.trim().split("||");
        if("OK" == resp[0]){
          $("input[name='rad_contSer'][value="+resp[1]+"]").prop('checked',true);  
          $("#txt_contNota").val(resp[2]);
          var contArr = JSON.parse(resp[3]);          
          $.each(contArr,function(key,value){
            $("input[name='chk_contactoContrato'][value='"+value+"']").prop('checked',true);
            getContractRepList(resp[5]);
            contactoContrArray.push(String(value));
          });
          setModalResponsive('mod_clientNewContract');
          $("#mod_clientNewContract").modal("show"); 
          $("#btn_saveSitCon").removeAttr("onClick");
          $("#btn_saveSitCon").off("click");
          $("#btn_saveSitCon").on("click", function(){
            saveContract(1);
          });
          $("#btn_prevSitCon").removeAttr("onClick");
          $("#btn_prevSitCon").off("click");
          $("#btn_prevSitCon").on("click", function(){
            getContract(1);
          });
          $("#txt_contFechaCon").val(resp[4]);
        }
        else{
          printErrorMsg("div_msgAlertPrices",resp[1]);        
        } 
      }
    });
  }
}

//guarda un contrato
function saveContract(opt){
  if(2==opt){
    var confirmar = confirm("Está a punto de marcar éste contrato como firmado. Tenga en cuenta que éste, ni la cotización de la cual se generó, podrán editarse más adelante. ¿Está seguro de hacerlo?");
  }
  else{
    var confirmar = true;
  }
  if(confirmar) {
    var ser = $("input[type='radio'][name='rad_contSer']:checked").val();
    var rep = $("input[type='radio'][name='rad_contratoRep']:checked").val();
    var con = JSON.stringify(contactoContrArray); 
    
    $.ajax({
      type:"POST",
      url:"ajax.php",
      data:{"action":"saveContract","cotId":$("#hdn_cotId").val(),"conSer":ser,"conCon":con,"orgId":$("#hdn_orgId").val(),"orgType":$("#hdn_orgType").val(), "conNot":$("#txt_contNota").val(), "conOpt":opt, "fecAct":$("#txt_contFechaCon").val(),"repId":rep},
      beforeSend: function(){
        setLoadDialog(0,"Guardando datos de contrato");
      },
      complete:function(){
        setLoadDialog(1,"");
      },
      success:function(msg){
        var resp = msg.trim().split("||");
        if("OK" == resp[0]){
          $("#mod_clientNewContract").modal("hide"); 
          printSuccessMsg("div_msgAlertPrices",resp[1]);
          getPrices($("#hdn_orgId").val(),$("#hdn_orgType").val(),0);
        }
        else{
          printErrorMsg("div_msgAlertClientContract",resp[1]);        
        } 
      }
    });
  }
}

function openContractEmail(opt){
  if(0>=contactoContrArray.length)
    alert("Debes seleccionar al menos un contacto");
  else{
    $("#contactContractDialog").load("../../libs/templates/contactdialog.php");
    contactoArray.length = 0;
    $("#tbl_selectedMailContactContract").find("tr:gt(0)").remove();
    $("#div_contractMsgContent").code("");
    $("#txt_contractMsgSubject").val("");
    $("#hdn_orgTypeMailCont").val($("#hdn_orgType").val());
    $("#div_msgAlertContractSendEmail").html("");
    $("#mod_sendContractPdf").modal("show");
    
    $("#btn_mandaConMail").removeAttr("onClick");
    $("#btn_mandaConMail").off("click");
    $("#btn_mandaConMail").on("click", function(){
      sendContractEmail(opt);
    });
    
    $("#div_contractMsgContent").summernote({
      lang: 'es-ES',
      height: 300, 
      minHeight: 100,
      maxHeight: 600 
    }); 
    $('#mod_clientNewContract').on('hide.bs.modal', function(){
      $("#contactContractDialog").html("");
    });
  }
}

//envia por correo el contrato junto con el mensaje personalizado
function sendContractEmail(opt){
  if(false==validateContractEmailData()){   
    alert("Por favor verifique los datos");
    return;
  }
  else{    
    var msgHtmlContent = $("#div_contractMsgContent").code();
    var msgPlainContent = $("#div_contractMsgContent").code().replace(/<\/p>/gi, "\n").replace(/<br\/?>/gi, "\n").replace(/<\/?[^>]+(>|$)/g, "");
    var msgSubject = $("#txt_contractMsgSubject").val();
    var contMailContact = JSON.stringify(contactoArray);
    var contContract = JSON.stringify(contactoContrArray);
    var cotId = $("#hdn_cotId").val();
    var orgType = $("#hdn_orgTypeMailCont").val();
    
    $.ajax({
      type: "POST",
      url: "../../libs/db/contractpdf3.php",
      async: false,
      data: {"action": "contractPDFGenerate", "contractMsgHtmlContent":msgHtmlContent, "contractMsgPlainContent":msgPlainContent, "contMailContact":contMailContact, "contContractArr":contContract, "option":opt, "isemail":1, "cotId":cotId, "orgType":orgType, "contractMsgSubject":msgSubject},
      
      beforeSend: function(){
        setLoadDialog(0,"Enviando cotización por correo");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){
        var resp = msg.trim().split('||');
        var errCod = resp[0];
        if("OK"== errCod){
          printSuccessMsg("div_msgAlertClientContract",resp[1]);
          $("#mod_sendContractPdf").modal("hide");
        }
        else{
          printErrorMsg("div_msgAlertContractSendEmail",resp[1]);                                                    
        }      
      },
      error: function() {
        printErrorMsg("div_msgAlertContractSendEmail","Ocurrió un error al enviarse los datos");   
      }  
    });
  }
}

//valida los datos del contacto antes de agregarse al formulario de nuevo/editar contacto, prospecto, sitio etc.
function validateContractEmailData(){
  
  if((0==contactoArray.length)){
    printErrorMsg("div_msgAlertPriceSendEmail","Debe incluir al menos un contacto/destinatario");
    return false;                             
  }  
  if(0>=$("#div_contractMsgContent").code().length){
    printErrorMsg("div_msgAlertContractSendEmail","Por favor incluya un mensaje");
    return false; 
  }  
  if(0>=$("#txt_contractMsgSubject").val().length){
    printErrorMsg("div_msgAlertContractSendEmail","Por favor incluya un asunto");
    return false; 
  }
}
/****Fin de apartado****/

/*********Apartado de funciones de gestión de contactos*********/

//obtiene lista de contactos ya dados de alta para agregarse a cualquier formulario que lo requiera
function getContactList(opt){
  var orgId = $("#hdn_orgId").val();
  var orgType = $("#hdn_orgType").val();
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",
    async: false,
    data: {"action": "getContactList", "option":opt, "orgType":orgType, "orgId":orgId},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo lista de contactos");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      if(9==opt){
        $("#div_insReqConSel").html(msg);
      }
      else if(10==opt){
        $("#div_contrConSel").html(msg); 
      }
      else{ 
        $("#div_contactList").html(msg); 
      }  
      if(1==opt||3==opt||11==opt){      
        $("#mod_contactList").modal("show"); 
        setModalResponsive("mod_contactList");
      }
      $("#tbl_contacts_"+opt).DataTable({
        language: datatableespaniol,
      });
    }  
  });
}

//agrega el contacto seleccionado a cualquier formulario que lo requiera (solo un contacto a la vez) asigna el valor a un hidden
//orgType indica a qué tipo de formulario se le está poniendo, 0=prospecto,1=cliente,2=sitio,4=cotización
function addContact(id,orgType){
  $("#hdn_contactId").val(id);
  $("#tr_selectedContact"+orgType).append("<td>"+$("#td_contactId_"+id).text()+"</td>"+
                                  "<td>"+$("#td_contactName_"+id).text()+"</td>"+
                                  "<td>"+$("#td_contactLastName_"+id).text()+"</td>"+
                                  "<td>"+$("#td_contactPosition_"+id).text()+"</td>"+
                                  "<td>"+$("#td_contactPhone_"+id).text()+"</td>"+
                                  "<td>"+$("#td_contactCell_"+id).text()+"</td>"+
                                  "<td>"+$("#td_contactEmail_"+id).text()+"</td>"+
                                  "<td><input type=\"button\" class=\"btn btn-danger btn-xs\" id=\"btn_remueveContacto\" value=\"Quitar\" onClick=\"removeContact();\"></td>");
  $("#btn_buscarContactos").prop('disabled',true);
  $("#mod_contactList").modal("hide");  
}

//agrega uno o más contactos como destinatario para envío de email
function addMailTo(id,opt){
  var contNombre   = $("#td_contactName_"+id).text();
  var contApellido = $("#td_contactLastName_"+id).text();
  var contPuesto   = $("#td_contactPosition_"+id).text();
  var contTel      = $("#td_contactPhone_"+id).text();
  var contCel      = $("#td_contactCell_"+id).text();
  var contEmail    = $("#td_contactEmail_"+id).text();
  var term = "";
  
  var tempArray = [id,contNombre+" "+contApellido,contEmail];
  contactoArray.push(tempArray); 
  if(11==opt)
    term = "Contract";
  $('#tbl_selectedMailContact'+term+' tr:last').after("<tr id=\"tr_mailContactId_"+id+"\">"+
                                      "  <td>"+id+"</td>"+
                                      "  <td>"+contNombre+"</td>"+
                                      "  <td>"+contApellido+"</td>"+
                                      "  <td>"+contPuesto+"</td>"+
                                      "  <td>"+contTel+"</td>"+
                                      "  <td>"+contCel+"</td>"+
                                      "  <td>"+contEmail+"</td>"+
                                      "  <td><input type=\"button\" class=\"btn btn-danger btn-xs\" id=\"btn_remueveMailDest_"+id+"\" value=\"Quitar\" onClick=\"removeMailTo(this);\"></td>"+
                                      "</tr>");
  $("#mod_contactList").modal("hide");
}

//quita el contacto del arreglo de destinatarios de email
function removeMailTo(elem){
  var contSplit = $(elem).attr('id').split("_");
  unsetFromArray(contactoArray,contSplit[2]);
  $("#tr_mailContactId_"+contSplit[2]).remove();
}

//quita contacto seleccionado
function removeContact(){
  $("#hdn_contactId").val(-1);
  $("#tr_selectedContact4").html("");
  $("#btn_buscarContactos").prop('disabled',false);
}

//abre ventana para crear un contacto nuevo orgtype 0=prospectos, 1=clientes, 2= sitios etc
function openNewContact(orgType){
  $("#txt_contNombre").val("");
  $("#txt_contApellidos").val("");
  $("#txt_contPuesto").val("");
  $("#txt_contTel").val("");
  $("#txt_contCell").val("");
  $("#txt_contEmail").val("");
  $("#txt_contNombre_error").html("");
  $("#txt_contApellidos_error").html("");
  $("#txt_contPuesto_error").html("");
  $("#txt_contTel_error").html("");
  $("#txt_contCell_error").html("");
  $("#txt_contEmail_error").html("");
  $("#sel_department_error").html("");
  $("#sel_position_error").html("");
  $("#div_msgAlertContact").html("");
  $("#mod_contactInsert").modal("show"); 
  $("#hdn_contactNewAction").val(0);
  $("#btn_contInsert").attr('onclick',"insertContactOnForm(0,'',"+orgType+");"); 
  $("#div_contPuesto_otro").hide();
  getDepartmentList(0);
  getPositionList(0);
  getPersonTitletList(0);
}

//abre el cuadro de dialogo de contacto y lo llena con los datos del contacto seleccionado para su edición
function editContact(elem){
  var contSplit = $(elem).attr('id').split("_");
  $("#hdn_contactNewAction").val(1); 
  
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",
    async: false,
    data: {"action": "getContactDetails", "contId":contSplit[2]},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo datos del contacto");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#txt_contNombre").val(resp[2]);
        $("#txt_contApellidos").val(resp[3]);
        $("#txt_contTel").val(resp[5]);
        $("#txt_contCell").val(resp[6]);
        $("#txt_contEmail").val(resp[7]);
        getPositionList(resp[8]);
        getDepartmentList(resp[9]);
        getPersonTitletList(resp[12]);
                
        if("6"==resp[8]){         
          $("#div_contPuesto_otro").show();         
          $("#txt_contPuesto").val(resp[4]);
        }
        else{
          $("#div_contPuesto_otro").hide();
        }
        
        $("#btn_contInsert").attr('onclick',"alterContact("+contSplit[2]+");")
        $("#mod_contactInsert").modal("show");  
      }       
      else{
        printErrorMsg("div_msgAlertProspect",resp[1]);       
      }
    }  
  });
}

//setea los nuevos valores para los campos del contacto a editar y los guarda en la db
function alterContact(id){
  if(false==validateContactData()){ 
    printErrorMsg("div_msgAlertContact","Existen datos no válidos, favor de corregirlos");  
  }
  else{
    for(var x=0; x<contactoArray.length; x++){
      if(contactoArray[x][0] == id){
        contactoArray[x][1] = $("#txt_contNombre").val();
        contactoArray[x][2] = $("#txt_contApellidos").val();
        contactoArray[x][3] = $("#txt_contPuesto").val();
        contactoArray[x][4] = $("#txt_contTel").val();
        contactoArray[x][5] = $("#txt_contCell").val();
        contactoArray[x][6] = $("#txt_contEmail").val();  
        contactoArray[x][7] = $("#sel_position").val();
        contactoArray[x][8] = $("#sel_department").val(); 
        contactoArray[x][9] = $("#sel_personTitle").val(); 
        
        var tempArray = [id,contactoArray[x][1],contactoArray[x][2],contactoArray[x][3],contactoArray[x][4],contactoArray[x][5],contactoArray[x][6],contactoArray[x][7],contactoArray[x][8],contactoArray[x][9]]; 
        break;
      }
    } 
    $.ajax({
      type: "POST",
      url: "../../libs/db/common.php",
      async: false,
      data: {"action": "updateContact", "contId":id, "contArr":JSON.stringify(tempArray)},
      beforeSend: function(){
        setLoadDialog(0,"Guardando cambios en el contacto");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){
        var resp = msg.trim().split("||");
        if("OK" == resp[0]){
          var orgType = $("#hdn_orgType").val();     
          
          if(6==contactoArray[x][7]){//si es "otro"
            var contPuestoDis = $("#txt_contPuesto").val();
          }
          else{
            var contPuestoDis = $("[name='sel_position'] option:selected").text();
          }
          
          $("#td_newContactName_"+id).html($("#txt_contNombre").val());
          $("#td_newContactLname_"+id).html($("#txt_contApellidos").val());
          $("#td_newContactPos_"+id).html(contPuestoDis);
          $("#td_newContactTel_"+id).html($("#txt_contTel").val());
          $("#td_newContactCel_"+id).html($("#txt_contCell").val());
          $("#td_newContactEmail_"+id).html($("#txt_contEmail").val());
          $("#td_newContactDepart_"+id).html($("[name='sel_department'] option:selected").text());
          
          $("#btn_contInsert").attr('onclick',"insertContactOnForm(0,'',"+orgType+");");
          $("#mod_contactInsert").modal("hide");
        }       
        else{ 
          printErrorMsg("div_msgAlertContact",resp[1]);               
        }
      }
    });
  }
}

//añade los datos del contacto en el formulario de nuevo prospecto, cliente, sitio etc
function insertContactOnForm(opt,contData,orgType){
 
    if(0==opt){//para inserción de contacto nuevo
      if(false==validateContactData()){ 
        printErrorMsg("div_msgAlertContact","Existen datos no válidos, favor de corregirlos"); 
        return; 
      }
      else{      
        var contNombre = $("#txt_contNombre").val();
        var contApellido = $("#txt_contApellidos").val();
        var contPuestoOtro = $("#txt_contPuesto").val();
        var contTel = $("#txt_contTel").val();
        var contCel = $("#txt_contCell").val();
        var contEmail = $("#txt_contEmail").val();
        var contPuesto = $("#sel_position").val();        
        var contDepart = $("#sel_department").val();
        var contPuestoDis = $("[name='sel_position'] option:selected").text();
        var contDepartDis = $("[name='sel_department'] option:selected").text(); 
        var contTituloDis = $("[name='sel_personTitle'] option:selected").text(); 
        var contTitulo = $("#sel_personTitle").val();
        var id = contTempId;
      } 
    }
    if(1==opt){//para cuando se edita el prospecto/cliente/etc que ya aparezcan en el formulario
      var contNombre = contData[1];
      var contApellido = contData[2];
      var contPuestoOtro = contData[3];
      var contTel = contData[4];
      var contCel = contData[5];
      var contEmail = contData[6];
      var contPuesto = contData[7];
      var contDepart = contData[8];
      var contPuestoDis = contData[9];
      var contDepartDis = contData[10];
      var id = contData[0];  
      var contTitulo = contData[11];
    }
        
    var tempArray = [id,contNombre,contApellido,contPuestoOtro,contTel,contCel,contEmail,contPuesto,contDepart,contTitulo];
    contactoArray.push(tempArray); 
    
    if(6==contPuesto){//si es "otro"
      var contPuestoDis = contPuestoOtro;
    }
    
    contactoArray.sort(function(a, b) {
      return a[0] - b[0];
    });
    var lastContactoElement = $(contactoArray).get(-1)[0];
    contTempId = parseInt(lastContactoElement)+1;

    var toAppend = "    <tr id=\"tr_newContact_"+id+"\">"+
                   "      <td id=\"td_newContactName_"+id+"\">"+contNombre+"</td>"+
                   "      <td id=\"td_newContactLname_"+id+"\">"+contApellido+"</td>"+
                   "      <td id=\"td_newContactPos_"+id+"\">"+contPuestoDis+"</td>"+
                   "      <td id=\"td_newContactTel_"+id+"\">"+contTel+"</td>"+
                   "      <td id=\"td_newContactCel_"+id+"\">"+contCel+"</td>"+
                   "      <td id=\"td_newContactEmail_"+id+"\">"+contEmail+"</td>"+
                   "      <td id=\"td_newContactDepart_"+id+"\">"+contDepartDis+"</td>"+
                   "      <td><input type=\"button\" class=\"btn btn-danger btn-xs\" id=\"btn_remueveContactoNuevo_"+id+"\" value=\"Quitar\" onClick=\"removeContactFromForm(this);\">&nbsp;";
                   
    if(1==opt){
      toAppend += "<input type=\"button\" class=\"btn btn-success btn-xs\" id=\"btn_editaContactoExistente_"+id+"\" value=\"Editar\" onClick=\"editContact(this);\">";
    }
    toAppend += "      <td>"+                 
                "    </tr>";
    
    $('#tbl_selectedContact'+orgType+' tr:last').after(toAppend);//se define en qué tabla se concatenarán los datos 0=prospecto,1=cliente,2=sitio,4=cotización
                                            
    //contTempId++;                                        
    $("#mod_contactInsert").modal("hide");
}

//valida los datos del contacto antes de agregarse al/modificarse en al formulario de nuevo/editar contacto, prospecto, sitio etc.
function validateContactData(){
  addValidationRules(0);
  jQuery.validator.addMethod("isPhoneNumber", function(value, element){    
    if(0<value.length&&(10!=value.length||(value%1!=0)))
      return false;
    else
      return true;  
  }, "Debe introducir diez dígitos");
  
  jQuery.validator.addMethod("isOtherPositionSpecified", function(value, element){    
    if((0>=(value).length)&&("6"==$("#sel_origin").val())) //cambiar 6 de acuerdo a catalogo
      return false;
    else
      return true;  
  }, "Debe especificar otro tipo de puesto");
  
  return $("#frm_newContact").validate({
    rules: {
      txt_contTel:
      {
        number:true,
        isPhoneNumber: true
      },
      txt_contCell:
      {
        number:true,
        isPhoneNumber: true
      },
      sel_position:
      {
        isSomethingSelected:true
      },
      sel_department:
      {
        isSomethingSelected:true
      },
      txt_contPuesto:
      {
        isOtherPositionSpecified:true
      }
    },
    errorPlacement: function(error, element) {
        error.appendTo($('#' + element.attr("id") + '_error'));
    }
  }).form();
}

//quita el contacto del arreglo de contactos a insertar
function removeContactFromForm(elem){
  var contSplit = $(elem).attr('id').split("_");
  unsetFromArray(contactoArray,contSplit[2]);
  $("#tr_newContact_"+contSplit[2]).remove();
}
/****Fin de apartado****/

/************Funciones de gestión de documentos*************/

//abre cuadro de dialogo de captura de documento.
function openNewDocument(){
  $("#fl_docNew").replaceWith($("#fl_docNew").val("").clone(true));
  getUploadedDocType(0);
  $("#txt_docDesc").val("");
  $("#txt_docDesc_error").html("");
  $("#fl_docNew_error").html("");
  $("#sel_docType_error").html("");
  $("#lbl_doc_label").html("Elija un archivo (tamaño máximo 15Mb)");
  $("#div_doc_prev").css("background-image","none");
  $("#hdn_docUploadAction").val(0);
  $("#hdn_docId").val(-1);
  $("#div_msgAlertDocumentNew").html("");
  $("#mod_uploadNewDocument").modal("show");
}

//abre el cuadro de dialogo para modificar los datos del documento y llena los campos
function editDocument(docId){
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",
    data: {"action": "getDocumentDetails", "docId":docId},
    beforeSend: function(){
      openNewDocument();
      setLoadDialog(0,"Obteniendo datos del documento");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = msg.trim().split('||');
      if("OK" == resp[0]){
        $("#hdn_docUploadAction").val(1);
        $("#txt_docDesc").val(resp[2]);
        $("#hdn_docId").val(docId);
        getUploadedDocType(resp[1]);
      }
      else{
        printErrorMsg("div_msgAlertDocuments",resp[1]);        
      }
    }  
  });
}

//guarda el documento y sus datos
function saveNewDocument(){ 
  if(false==validateDocumentData()){ 
    printErrorMsg("div_msgAlertDocumentNew","Existen datos no válidos, favor de corregirlos");  
  }
  else{
    var orgId   = $("#hdn_docOrgId").val();
    var orgType = $("#hdn_docOrgType").val();
    var docId   = $("#hdn_docId").val();
    
    var options = {
      contentType: 'multipart/form-data',
      url: "../../libs/db/common.php", //direccion relativa del modulo que invoca a la libreria common
      data: {"action": "uploadNewDocument", "orgId":orgId, "orgType":orgType, "docDesc":$("#txt_docDesc").val(), "docType":$("#sel_docType").val(), "docId":$("#hdn_docId").val(), "option":$("#hdn_docUploadAction").val()},
      beforeSend: function(){
        setLoadDialog(0,"Guardando documento");
      },
      complete: function(response){
        setLoadDialog(1,"");
        var resp = (response.responseText).trim().split('||');
        if("OK" == resp[0]){
          printSuccessMsg("div_msgAlertDocuments",resp[1]);
          displayDocuments(orgId,orgType); 
          $("#mod_uploadNewDocument").modal("hide");
        }
        else{  
          printErrorMsg("div_msgAlertDocumentNew",resp[1]);                
        }          
      },
      error: function(){
        printErrorMsg("div_msgAlertDocumentNew","Ocurrió un error al enviarse los datos de documento para su guardado");  
      }      
    };   
    $("#frm_newDocument").ajaxForm(options);
  } 
}

//elimina un documento y sus datos
function deleteDocument(docId){
  var orgId   = $("#hdn_orgId").val();
  var orgType = $("#hdn_orgType").val();
  var docOrgId   = $("#hdn_docOrgId").val();
  var docOrgType = $("#hdn_docOrgType").val();
  
  if(confirm("¿Está seguro/a de eliminar este documento? Esta acción no se podrá deshacer.")) {
    $.ajax({
      type: "POST",
      url: "../../libs/db/common.php", //direccion relativa del modulo que invoca a la libreria common
      data: {"action": "deleteDocument", "docId":docId, "orgId":docOrgId, "orgType":docOrgType},
      beforeSend: function(){
        setLoadDialog(0,"Eliminando documento");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){
        var resp = msg.trim().split("||");
        if("OK" == resp[0]){
          printSuccessMsg("div_msgAlertDocuments",resp[1]);
          displayDocuments(docOrgId,docOrgType);
        }
        else{
          printErrorMsg("div_msgAlertDocuments",resp[1]);         
        }   
      }  
    });  
  }  
}

//valida los datos del documento antes de guardarlo y asociarlo al cliente/prospecto/sitio etc.
function validateDocumentData(){  
  jQuery.validator.addMethod("isSomethingSelected", function(value, element){    
      if(0!=value)
        return true;
      else
        return false;  
  }, "Debe seleccionar una opción");
  
  jQuery.validator.addMethod("isFileSelected", function(value, element){  
      if(1!=$("#hdn_docUploadAction").val()){
        if(0!=value)
          return true;
        else
          return false;       
      }
      else{
        return true;
      }          
  }, "Debe seleccionar agregar un archivo");
  
  return $("#frm_newDocument").validate({
    rules: {
      fl_docNew:{
        isFileSelected:true
      },
      sel_docType:
      {
        isSomethingSelected:true
      }     
    },
    errorPlacement: function(error, element) {
        error.appendTo($('#' + element.attr("id") + '_error'));
    }
  }).form();
}

//función que despliega los documentos asociados a un origen (cliente/prospecto/sitio etc).
function displayDocuments(orgId,orgType){
  $("#div_msgAlertDocuments").html("");  
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getUploadedDocs", "orgId":orgId, "orgType":orgType},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo documentos");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){        
        $("#div_uploadedDocuments").html(resp[1]);  
        /*$("#tbl_uploadedDocuments").DataTable({
          language: datatableespaniol,
        });*/
        $("#mod_uploadedDocuments").modal("show");
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);
      }   
    }  
  });  
}

/*********Fin de apartado**********/

/*********Apartado de funciones de gestión de clientes (usado en modulo clientes y facturación)*********/

//función que obtiene los datos completos de un cliente seleccionado
function getClientDetails(id){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getClientDetails", "cliId":id, "option":"0"},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo datos del cliente");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      $("#div_clientDetails").html(msg);
      setModalResponsive('mod_clientDetails'); 
      $("#mod_clientDetails").modal("show"); 
    }  
  });
}

/*********Fin de apartado**********/

/*********Apartado de funciones de gestión de representante legal (usado en modulo prospectos, clientes y facturación)*********/

//abre ventana para crear un representante legal nuevo
function openNewManager(){
  $("#txt_repNombre").val("");
  $("#txt_repApellidos").val("");
  $("#txt_repNumPod").val("");
  $("#txt_repFecPod").val("");
  $("#txt_repDom").val("");
  $("#txt_repCol").val("");
  $("#txt_repCp").val("");
  $("#txt_repMun").val("");  
  $("#txt_repTel").val("");
  $("#txt_repCell").val("");
  $("#txt_repEmail").val("");
  $("#txt_repFolio").val("");
  $("#txt_repNotario").val("");
  $("#mod_managerInsert").modal("show");   
  $("#hdn_repActNewAction").val(0);
  getStatesList(0,"spn_repEdo","rep");
  
  $("#txt_repNombre_error").html("");
  $("#txt_repApellidos_error").html("");
  $("#txt_repNumPod_error").html("");
  $("#txt_repFecPod_error").html("");
  $("#txt_repDom_error").html("");
  $("#txt_repCol_error").html("");
  $("#txt_repCp_error").html("");
  $("#txt_repMun_error").html("");  
  $("#txt_repTel_error").html("");
  $("#txt_repCell_error").html("");
  $("#txt_repEmail_error").html("");
  $("#sel_repstate_error").html("");
  $("#txt_repFolio_error").val("");
  $("#txt_repNotario_error").val("");
  $("#div_msgAlertManager").html("");
  $("#btn_manInsert").attr('onclick',"saveManager(0,'');");
  //$("#btn_manInsert").attr('onclick',"insertManagerOnForm(0,'');");
  $("#txt_repFecPod").datetimepicker(datetimeOptions);
}

function getManagerDetails(){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    async: false,
    data: {"action": "getManagerDetails", "manId":manSplit[2], "option":0},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo datos del representante");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = msg.trim().split("||");
    }  
  });
}

//abre el cuadro de dialogo de contacto y lo llena con los datos del contacto seleccionado para su edición
function editManager(id){//elem originalmente como parametro
  //var manSplit = $(elem).attr('id').split("_");
  $("#hdn_repActNewAction").val(1); 
  
  $.ajax({
    type: "POST",
    url: "ajax.php",
    async: false,
    data: {"action": "getManagerDetails", /*"manId":manSplit[2],*/ "manId":id, "option":1},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo datos del representante");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#txt_repNombre").val(resp[1]);
        $("#txt_repApellidos").val(resp[2]);
        $("#txt_repNumPod").val(resp[3]);
        $("#txt_repFecPod").val(resp[4]);
        $("#txt_repDom").val(resp[5]);
        $("#txt_repCol").val(resp[6]);
        $("#txt_repCp").val(resp[7]);
        $("#txt_repMun").val(resp[8]);  
        $("#txt_repTel").val(resp[10]);
        $("#txt_repCell").val(resp[11]);
        $("#txt_repEmail").val(resp[12]);
        $("#txt_repNotario").val(resp[13]);
        $("#txt_repFolio").val(resp[14]);
        
        getStatesList(9,"spn_repEdo","rep");
        $("#txt_repFecPod").datetimepicker(datetimeOptions);
               
        $("#btn_manInsert").attr('onclick',"saveManager(1,'"+id+"');")
        $("#mod_managerInsert").modal("show");
      }       
      else{
        printErrorMsg("div_msgAlertManager",resp[1]);        
      }
    }  
  });
}

//setea los nuevos valores para los campos del representante a editar y los guarda en la db
function saveManager(opt,id){
  if(false==validateManagerData()){ 
    printErrorMsg("div_msgAlertManager","Existen datos no válidos, favor de corregirlos");
  }
  else{
    var repId     = id;
    var repNom    = $("#txt_repNombre").val();
    var repApe    = $("#txt_repApellidos").val();
    var repNumPod = $("#txt_repNumPod").val();
    var repFecPod = $("#txt_repFecPod").val();
    var repDom    = $("#txt_repDom").val();
    var repCol    = $("#txt_repCol").val();
    var repCp     = $("#txt_repCp").val();
    var repMun    = $("#txt_repMun").val();
    var repEdo    = $("#sel_repstate").val();  
    var repTel    = $("#txt_repTel").val();
    var repCel    = $("#txt_repCell").val();
    var repEmail  = $("#txt_repEmail").val();
    var repNot    = $("#txt_repNotario").val();
    var repFolio  = $("#txt_repFolio").val();

    var tempArray = [repId,repNom,repApe,repNumPod,repFecPod,repDom,repCol,repCp,repMun,repEdo,repTel,repCel,repEmail,repNot,repFolio];
    
    $.ajax({
      type: "POST",
      url: "ajax.php",
      async: false,
      data: {"action": "updateManager", "option":opt, "manArr":JSON.stringify(tempArray), "orgId":$("#hdn_orgId").val(), "orgType":$("#hdn_orgType").val()},
      beforeSend: function(){
        setLoadDialog(0,"Guardando cambios en el representante");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){
        var resp = msg.trim().split("||");
        if("OK" == resp[0]){
          /*if(1==opt){
            $("#td_newManagerName_"+id).html($("#txt_repNombre").val());
            $("#td_newManagerLname_"+id).html($("#txt_repApellidos").val());
            $("#td_newManagerPowNum_"+id).html($("#txt_repNumPod").val());
            /*$("#td_newManagerPowDat_"+id).html($("#txt_repFecPod").val());
            $("#td_newManagerAddr_"+id).html($("#txt_repDom").val());
            $("#td_newManagerCol_"+id).html($("#txt_repCol").val());
            $("#td_newManagerMun_"+id).html($("#txt_repMun").val());
            $("#td_newManagerSta_"+id).html($("#").val());*/
          /*  $("#td_newManagerTel_"+id).html($("#txt_repTel").val());
            $("#td_newManagerCel_"+id).html($("#txt_repCell").val());
            $("#td_newManagerEmail_"+id).html($("#txt_repEmail").val());
            
            $("#btn_manInsert").attr('onclick',"insertManagerOnForm(0,'');");
            $("#mod_managerInsert").modal("hide");
          }
          if(0==opt){
            getProspectDetails($("#hdn_orgId").val());
          }*/
          getProspectDetails($("#hdn_orgId").val());
          $("#mod_managerInsert").modal("hide");
        }       
        else{ 
          printErrorMsg("div_msgAlertManager",resp[1]);                                
        }
      }
    });
  }
}

//añade los datos del contacto en el formulario de nuevo prospecto, cliente, sitio etc
function insertManagerOnForm(opt,manData){
  if(false==validateManagerData()){   
    printErrorMsg("div_msgAlertManager","Existen datos no válidos, favor de corregirlos");
  }
  else{
    if(0==opt){//para inserción de contacto nuevo
    
      var manNombre = $("#txt_repNombre").val();
      var manApellido = $("#txt_repApellidos").val();
      var manNumPod = $("#txt_repNumPod").val();
      var manFecPod = $("#txt_repFecPod").val();
      var manDom = $("#txt_repDom").val();
      var manCol = $("#txt_repCol").val();
      var manCp = $("#txt_repCp").val();
      var manMun = $("#txt_repMun").val();
      var manEdo = $("#sel_repstate").val();  
      var manTel = $("#txt_repTel").val();
      var manCell = $("#txt_repCell").val();
      var manEmail = $("#txt_repEmail").val();
      var manNot = $("#txt_repNotario").val();
      var manFolio = $("#txt_repFolio").val();
      var id = 0;
    }
    if(1==opt){//para cuando se edita el prospecto/cliente/etc que ya aparezcan en el formulario
      var manNombre = manData[1];
      var manApellido = manData[2];
      var manNumPod = manData[3];
      var manFecPod = manData[4];
      var manDom = manData[5];
      var manCol = manData[6];
      var manCp = manData[7];
      var manMun = manData[8]; 
      var manEdo = manData[9]; 
      var manTel = manData[10];
      var manCell = manData[11];
      var manEmail = manData[12];
      var manNot = manData[13];
      var manFolio = manData[14];
      var id = manData[0];  
    }
        
    representanteArray = [id,manNombre,manApellido,manNumPod,manFecPod,manDom,manCol,manCp,manMun,manEdo,manTel,manCell,manEmail,manNot,manFolio];
    
    var toAppend = "    <tr id=\"tr_newManager_"+id+"\">"+
                   "      <td id=\"td_newManagerName_"+id+"\">"+manNombre+"</td>"+
                   "      <td id=\"td_newManagerLname_"+id+"\">"+manApellido+"</td>"+
                   "      <td id=\"td_newManagerPowNum_"+id+"\">"+manNumPod+"</td>"+
                   /*"      <td id=\"td_newManagerPowDat_"+id+"\">"+manFecPod+"</td>"+
                   "      <td id=\"td_newManagerAddr_"+id+"\">"+manDom+"</td>"+
                   "      <td id=\"td_newManagerCol_"+id+"\">"+manCol+"</td>"+
                   "      <td id=\"td_newManagerZip_"+id+"\">"+manCp+"</td>"+
                   "      <td id=\"td_newManagerMun_"+id+"\">"+manMun+"</td>"+
                   "      <td id=\"td_newManagerSta_"+id+"\">"+manEdo+"</td>"+*/
                   "      <td id=\"td_newManagerTel_"+id+"\">"+manTel+"</td>"+
                   "      <td id=\"td_newManagerCel_"+id+"\">"+manCell+"</td>"+
                   "      <td id=\"td_newManagerEmail_"+id+"\">"+manEmail+"</td>"+
                   "      <td><input type=\"button\" class=\"btn btn-danger btn-xs\" id=\"btn_remueveRepresentanteNuevo_"+id+"\" value=\"Quitar\" onClick=\"removeManagerFromForm(this);\">&nbsp;";
                   
    if(1==opt){
      toAppend += "<input type=\"button\" class=\"btn btn-success btn-xs\" id=\"btn_editaRepresentanteExistente_"+id+"\" value=\"Editar\" onClick=\"editManager(this);\">";
    }
    toAppend += "      <td>"+                 
                "    </tr>";
    
    $('#tbl_selectedManager tr:last').after(toAppend);
    $("#btn_insertarRepresentante").prop('disabled',true);                           
    $("#mod_managerInsert").modal("hide");
  } 
}

//quita el contacto del arreglo de contactos a insertar
function removeManagerFromForm(elem){
  var manSplit = $(elem).attr('id').split("_");
  unsetFromArray(representanteArray,manSplit[2]);
  $("#tr_newManager_"+manSplit[2]).remove();
  $("#btn_insertarRepresentante").prop('disabled',false); 
}

//valida los datos del contacto antes de agregarse al formulario de nuevo/editar contacto, prospecto, sitio etc.
function validateManagerData(){
  addValidationRules(1);
  	
	return $("#frm_newManager").validate({
    rules: {
      txt_repFecPod:
      {
        isDateTime: true
      },
      txt_repTel:
      {
        number:true,
        isPhoneNumber: true
      },
      txt_repCell:
      {
        number:true,
        isPhoneNumber: true
      },
      /*sel_repstate:
      {
        isSomethingSelected:true
      },*/
      txt_repCp:
      {
        number: true,
        integer: true
      }, 
      txt_repFolio:
      {
        number: true
      }       
    },
    errorPlacement: function(error, element) {
       	error.appendTo($('#' + element.attr("id") + '_error'));
    }
  }).form();
}

/*********Fin de apartado**********/

/*********Apartado de funciones de gestión de sitios**********/

//obtiene todos los datos detallados de un sitio
function goToSiteDetails(id,orgType){ 
  window.location.href = "../sitios/sitedetails.php?id="+id+"&orgType="+orgType+"&det=1";
  /*$.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getSiteDetails", "siteId":id, "option":"0"},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo datos del sitio");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      $("#div_siteDetails").html(msg);
      setModalResponsive('mod_siteDetails'); 
      $("#mod_siteDetails").modal("show"); 
    }  
  });*/
}

//abre la vantana de diálogo para agregar o editar un sitio a o de un cliente
function openNewSite(){ 
  clearSiteFields();
  $("#contactSiteDialog").load("../../libs/templates/contactdialog.php");//carga cuadro de dialogo de insertar contacto en el modulo
  //$("#hdn_siteAction").val(0);
  $("#hdn_siteId").val(-1); 
  setModalResponsive("mod_siteInsert");
  //$("#mod_siteInsert").modal("show"); 
  $("#hdn_selConnection").val(-1);
  
  var orgId = $("#hdn_orgId").val();
  var action = "";
  var orgVarName = "";
  var data = {};
  
  if(0==$("#hdn_orgType").val()){
    action = "getProspectDetails";
    orgVarName = "prosId";
  }
  else if(1==$("#hdn_orgType").val()){
    action = "getClientDetails";
    orgVarName = "cliId";
  }
  
  data["action"] = action;
  data[orgVarName] = orgId;
  data["option"] = "2";
    
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: data,
    beforeSend: function(){
      setLoadDialog(0,"Importando datos de registro para sitio");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = (msg).trim().split('||');
      if("OK" == resp[0]){     
      
        $("#hdn_siteAction").val(0);
                
        $("#txt_nomSit").val(resp[1]);
        $("#txt_telSit").val(resp[2]);
        $("#txt_domSit").val(resp[3]);
        $("#txt_colSit").val(resp[4]);
        $("#txt_cpSit").val(resp[5]);
        $("#txt_munSit").val(resp[6]);
        $("#txt_callSit1").val(resp[9]);
        $("#txt_callSit2").val(resp[10]);
        $("#txt_refSit").val("");
        $("#txt_sitDesc").val(resp[8]);  
        
        getStatesList(resp[7],"spn_edoSit","site");
        
        initMap(resp[11],resp[12],'div_siteMap','hdn_siteLatitud','hdn_siteLongitud');
         
        $("#hdn_siteLatitud").val(resp[11]);
        $("#hdn_siteLongitud").val(resp[12]);
        
        var contArr = JSON.parse(resp[13]);
        
        for(var x=0;x<contArr.length;x++){
          insertContactOnForm(1,contArr[x],2);
        }  
        setModalResponsive("mod_siteInsert");      
        $("#mod_siteInsert").modal("show");  
      }
      else{
        printErrorMsg("div_msgAlertSite",resp[1]);                                              
      }  
    }  
  });
    
}

//limpia los campos del formulario de sitios
function clearSiteFields(){
  $("#txt_nomSit").val("");
  $("#txt_telSit").val("");
  $("#txt_domSit").val("");
  $("#txt_colSit").val("");
  $("#txt_cpSit").val("");
  $("#txt_munSit").val("");
  $("#txt_callSit1").val("");
  $("#txt_callSit2").val("");
  $("#txt_refSit").val("");
  $("#txt_sitDesc").val("");  
  
  $("#tbl_selectedContact2").find("tr:gt(0)").remove();
  
  contactoArray.length = 0;
  getStatesList(0,"spn_edoSit","site");
  
  $("#hdn_contArrSit_error").html("");
  $("#div_msgAlertSite").html("");
  $("#sel_sitestate_error").html("");
  $("#txt_nomSit_error").html("");
  $("#txt_telSit_error").html("");
  $("#txt_domSit_error").html("");
  $("#txt_colSit_error").html("");
  $("#txt_cpSit_error").html("");
  $("#txt_munSit_error").html("");
  $("#txt_callSit1_error").html("");
  $("#txt_callSit2_error").html("");
  $("#txt_refSit_error").html("");
  $("#txt_sitDesc_error").html("");
  initMap(20.683, -103.378,'div_siteMap','hdn_siteLatitud','hdn_siteLongitud'); 
}

//llena los campos de formulario de sitios y abre su ventana para la edición
function editSite(siteId){
  $("#contactSiteDialog").load("../../libs/templates/contactdialog.php");//carga cuadro de dialogo de contactos en el modulo
  var orgType = $("#hdn_orgSiteType").val();
  clearSiteFields(); 
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getSiteDetails", "orgId":siteId, "orgType":orgType, "option":"1"},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo datos del sitio para su edición");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = (msg).trim().split('||');
      if("OK" == resp[0]){
        $("#hdn_siteAction").val(1);
        $("#hdn_siteId").val(siteId);         
        $("#hdn_orgId").val(resp[2]);
                
        $("#txt_nomSit").val(resp[3]);
        $("#txt_telSit").val(resp[4]);
        $("#txt_domSit").val(resp[5]);
        $("#txt_colSit").val(resp[6]);
        $("#txt_cpSit").val(resp[7]);
        $("#txt_munSit").val(resp[8]);
        $("#txt_callSit1").val(resp[10]);
        $("#txt_callSit2").val(resp[11]);
        $("#txt_refSit").val(resp[12]);
        $("#txt_sitDesc").val(resp[15]);  
        
        getStatesList(resp[9],"spn_edoSit","site");
        
        initMap(resp[17],resp[18],'div_siteMap','hdn_siteLatitud','hdn_siteLongitud');
         
        $("#hdn_siteLatitud").val(resp[17]);
        $("#hdn_siteLongitud").val(resp[18]);
        
        var contArr = JSON.parse(resp[19]);
        
        for(var x=0;x<contArr.length;x++){
          insertContactOnForm(1,contArr[x],2);
        }  
        setModalResponsive("mod_siteInsert");      
        $("#mod_siteInsert").modal("show");  
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);                                              
      }  
    }  
  });  
}

//abre una ventana que despliega todos los sitios asociados al un origen
function displayOrgSites(orgId,orgType,option){
  var connId = $("#hdn_selConnection").val();
  $("#hdn_orgId").val(orgId);
  
  $("#div_msgAlertsSites").html("");
  $.ajax({
    type: "POST",
    url: "ajax.php", //dirección relativa al ajax.php del modulo (clientes o prospecto)
    data: {"action": "getOrgSites", "orgId":orgId, "orgType":orgType, "option":option, "connId":connId},
    beforeSend: function(){
      setLoadDialog(0,"Consultando datos de Sitios");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){     
      if(0==option||2==option){//despliega lista de sitios en prospectos o clientes
        $("#div_sites").html(msg);
        $("#mod_sites").modal("show");
      }
      else{//despliega en ventana de generar contrato
        $("#div_contrSiteSel").html(msg);
      }
        
      $("#tbl_sites_"+option).DataTable({
          language: datatableespaniol,
      });              
      setModalResponsive("mod_sites");
    }
  });
}

//guarda un sitio nuevo o guarda los cambios en un sitio existente esto se especifica en el campo hidden hdn_siteAction donde 0=guardar nuevo 1=guardar existente(editar)
function saveSiteNew(){
  if(false==validateSiteData()){
    printErrorMsg("div_msgAlertSite","Existen datos no válidos, favor de corregirlos");    
  }
  else{
    var det = $("#hdn_det").val();
    
    var sitId   = $("#hdn_siteId").val();
    var orgId   = $("#hdn_orgId").val();
    var orgType = $("#hdn_orgType").val();
    var sitNom  = $("#txt_nomSit").val();
    var sitTel  = $("#txt_telSit").val();
    var sitDom  = $("#txt_domSit").val();
    var sitCol  = $("#txt_colSit").val();
    var sitCp   = $("#txt_cpSit").val();
    var sitMun  = $("#txt_munSit").val();
    var sitCa1  = $("#txt_callSit1").val();
    var sitCa2  = $("#txt_callSit2").val();
    var sitRef  = $("#txt_refSit").val();
    var sitDesc = $("#txt_sitDesc").val();
    var sitEdo  = $("#sel_sitestate").val();
    var sitOption = $("#hdn_siteAction").val();
    var sitLat  = $("#hdn_siteLatitud").val();
    var sitLon  = $("#hdn_siteLongitud").val();
    
    var sitContactoArr = JSON.stringify(contactoArray);
    
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: {"action": "saveSiteNew", "sitOption":sitOption, "sitId":sitId, "orgId":orgId, "orgType":orgType, "sitNom":sitNom, "sitTel":sitTel, "sitDom":sitDom, "sitCol":sitCol, "sitCp":sitCp, "sitMun":sitMun, "sitCa1":sitCa1, "sitCa2":sitCa2, "sitRef":sitRef, "sitDesc":sitDesc, "sitEdo":sitEdo, "sitContactoArr":sitContactoArr, "sitLatitud":sitLat, "sitLongitud":sitLon},
      beforeSend: function(){
        setLoadDialog(0,"Guardando Sitio. Por favor espere");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){
        var resp = (msg).trim().split('||');
        if("OK" == resp[0]){
          if(det === undefined){
            printSuccessMsg("div_msgAlertsSites",resp[1]);
            displayOrgSites(orgId,orgType,0);
            getStatusTabs(orgType);
          }
          else{
            printSuccessMsg("div_msgAlert",resp[1]);
            //getSiteRegs();
            $("#hdn_orgId").val($("#hdn_siteId").val());
            getSiteDetails(); 
          }
          if(-1<$("#hdn_selConnection").val()){
            displayOrgSites(orgId,orgType,2); 
            $("#hdn_selConnection").val("");
          }
          $("#mod_siteInsert").modal("hide");
        }
        else{
          printErrorMsg("div_msgAlertSite",resp[1]);                             
        }   
      } 
    });
  }  
}

//elimina un sitio
function deleteSite(siteId){
  var orgId  = $("#hdn_orgId").val();
  var orgType  = $("#hdn_orgType").val();
  
  if(confirm("¿Está seguro/a de eliminar este sitio? Esto eliminará también sus tickets, eventos y enlaces. Esta acción no se podrá deshacer.")) {
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: {"action": "deleteSite", "sitId":siteId, "orgId":orgId, "orgType":orgType},
      beforeSend: function(){
        setLoadDialog(0,"Eliminando sitio");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){
        var resp = msg.trim().split("||");
        if("OK" == resp[0]){
          if(typeof cliId !== undefined){
            printSuccessMsg("div_msgAlertsSites",resp[1]);
            displayOrgSites(orgId,orgType,0);
          }
          else{
            printSuccessMsg("div_msgAlertsSites",resp[1]);
            getSiteRegs(); 
          }
        }
        else{
          printErrorMsg("div_msgAlertSites",resp[1]);           
        }   
      } 
    });
  }
}

//valida la integridad de los datos antes de guardarse el sitio
function validateSiteData(){  
  //valida que se haya seleccionado una opción en la lista desplegable
  addValidationRules(2);
  
  return $("#frm_newSite").validate({
    ignore: ':hidden:not(#hdn_contArrSit)',
    rules: {
      txt_telSit:
      {
        isPhoneNumber:true
      },
      txt_cpSit:
      {
        number: true,
        integer: true
      },
      sel_sitestate:
      {
        isSomethingSelected:true
      },
       hdn_contArrSit:
      {
        hasContacts:true
      }     
    },
    errorPlacement: function(error, element) {
        error.appendTo($('#' + element.attr("id") + '_error'));
    }
  }).form();
}

//abre cuadro de dialogo para asignar sitio a un enlace en una cotización
function openSiteToConnection(orgId,orgType,connId){
  $("#contactSiteDialog").load("../../libs/templates/contactdialog.php");//carga cuadro de dialogo de insertar contacto en el modulo
  $("#hdn_selConnection").val(connId);
  displayOrgSites(orgId,orgType,2);
}

//asigna sitio a un enlace en una cotización
function assignSiteToConn(sitId,relCotPro){
  $.ajax({
    type: "POST",
    url: "ajax.php", //dirección relativa al ajax.php del modulo (clientes o prospecto)
    data: {"action": "assignSiteToConn", "siteId":sitId, "relCotPro":relCotPro},
    beforeSend: function(){
      setLoadDialog(0,"Asignando Sitios");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){ 
      var resp = msg.trim().split("||");    
      if("OK" == resp[0]){
        printSuccessMsg("div_msgAlertsSites",resp[1]); 
        $("#mod_sites").modal("hide");
        getPriceDetails($("#hdn_priceIdDet").val());
        $("#hdn_relCotPro").val(relCotPro);
        openNewWorkOrder(-1);//-1 = se está invocando desde asignación de sitio solo estará disponible la opcion de estudio de fact
      }
      else{
        printErrorMsg("div_msgAlertsSites",resp[1]); 
      }
    }
  }); 
}

/*********Fin de apartado**********/

/*********Apartado de gestión de órdenes de trabajo para enlaces de cotizaciones y sus sitios***********/

function getWorkOrders(relCotPro){
  $("#hdn_relProCon").val(relCotPro);
  var orgType = $("#hdn_orgType").val();
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getWorkOrderList", "relCotPro":relCotPro, "orgType":orgType},  
    beforeSend: function(){
      setLoadDialog(0,"Consultando órdenes de trabajo");
    },
    complete: function(){
      setLoadDialog(1,"");
    },      
    success: function(msg){
      $("#div_worOrderList").html(msg); 
      $("#hdn_relCotPro").val(relCotPro);
      $("#mod_workOrders").modal("show");
      setModalResponsive("mod_workOrders");
    }  
  }); 
} 

function getWorkOrderDetails(worOrdId,option){
  adecArray.length = 0;
  $.ajax({
    type: "POST",
    async: false,
    url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
    data: {"action": "getWorkOrderDetails", "worOrdId":worOrdId, "option":option, "orgType":$("#hdn_orgType").val()},  
    beforeSend: function(){
        setLoadDialog(0,"Consultando órden de trabajo");
    },
    complete: function(){
        setLoadDialog(1,"");
    },      
    success: function(msg){
      if(0==option){
        $("#div_worOrderDetails").html(msg); 
        $("#mod_workOrderDetails").modal("show");
        setModalResponsive("mod_workOrderDetails");
        getComments(worOrdId,9);
      }
      else if(1==option){
        var resp = msg.trim().split("||");
          
        if("OK"==resp[0]){
          $("#mod_newWorkOrder").modal("show");     
          $("#div_worOrdFact").hide();
          $("#div_worOrdStatAdecList").hide(); 
                       
          $("#txt_worOrdDesc").val(resp[3]);        
          $("#txt_worOrdTitulo").val(resp[2]);    
          $("#hdn_worOrdAction").val(1);
          $("#hdn_worOrdId").val(worOrdId);
          getWorkOrderStatList(resp[4]);
          getWorkOrderTypeList(resp[1],1);   
          $("#div_msgWorkOrderDetails").html(""); 
          
          if(3==resp[4]){ //si es estatus terminado o cancelado
            //$("#div_worOrdStatLot").show();
            $("#div_worOrdFact").show();
            $("input[name='rad_workOrdFact'][value="+resp[5]+"]").prop('checked',true);
            
            if(3==resp[5]){ //factible con adecuaciones   
              $("#div_worOrdStatAdecList").show(); 
              $("#tbl_selectedAdec").find("tr:gt(0)").remove();
              getAdecList(0);
              var adecArr = JSON.parse(resp[6]);
              
              if(0<adecArr.length && undefined!==adecArr){
                for(var a=0;a<adecArr.length;a++){
                  addAdecToOrder(1,adecArr[a]);   
                }               
              }     
            }
          }
          else{
            $("#div_worOrdFact").hide();
            $("#div_worOrdStatAdecList").hide();
            //$("#div_worOrdStatLot").hide();
          } 
        }
        else{
          printErrorMsg("div_msgWorkOrderDetails",resp[1]);
        }       
      }
    }  
  }); 
}

function newDimWorkOrder(cotId){
   $("#hdn_worOrdPricId").val(cotId);
   openNewWorkOrder(-2);
   getWorkOrderTypeList(5,-2);
}
  
function openNewWorkOrder(action){
  $("#txt_worOrdDesc").val("");
  $("#txt_worOrdTitulo").val("");
  $("#hdn_worOrdAction").val(0);
  $("#hdn_worOrdId").val(-1);
  getWorkOrderStatList(1);
  getWorkOrderTypeList(1,action);
  adecArray.length = 0;
  $("#tbl_selectedAdec").find("tr:gt(0)").remove();
  $("input[type='radio'][name='rad_workOrdFact'][value=1]").prop('checked', true);
  $("#div_msgAlertWorkOrderList").html("");
  $("#div_worOrdFact").hide();
  $("#div_worOrdStatAdecList").hide();
  if(-1==action||0==action||-2==action){
    //$("#div_worOrdStatLot").hide();
    $("#div_worOrdFact").hide();
    $("#div_worOrdStatAdecList").hide();
  }
  else{
    //$("#div_worOrdStatLot").show();
    $("#div_worOrdFact").hide();
    $("#div_worOrdStatAdecList").hide();
  }
  $("#mod_newWorkOrder").modal("show");
  $("#txt_worOrdTitulo_error").html("");
  $("#txt_worOrdDesc_error").html("");
  $("#div_msgAlertWorkOrder").html("");
}
  
//funcion que obtiene los campos del evento para su edición en los campos del cuadro de diálogo
function editWorkOrderTask(id){
  openWorkOrderTaskNew();
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getEventFields", "eventId":id, "eveCat":1}, //1 que es una tarea y no un evento
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo datos de evento");
    },
    success: function(msg){
      setLoadDialog(1,"");
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){
        $("#hdn_tareaAction").val(1);
        
        getWorkOrderStatList(resp[4])
        getWorkOrderTypeList(resp[1]);
        
        getEventStatList(resp[7],2);
        
        $("#txt_tareaTitulo").val(resp[2]);        
        $("#txt_tareaAction").val(1);
        $("#hdn_tareaId").val(id);
        
        if('0000-00-00 00:00:00'==resp[3]||'0000-00-00 00:00:00'==resp[4]){
          $("#txt_tareaFechaIni").val("");
          $("#txt_tareaFechaFin").val("");
        }
        else{
          $("#txt_tareaFechaIni").val(resp[3]);
          $("#txt_tareaFechaFin").val(resp[4]);
          getAvailablePeople(0);
        }

        if(0!=resp[1]&&3==resp[8]){
          $("#div_tareaRadFact").show();
          $('input[name="rad_tareaFact"][value="'+resp[10]+'"]').prop('checked', true);
        }
        else{
          $("#div_eveRadFact").hide();
        }
        
        $("#txt_tareaDesc").val(resp[6]);
        
        var invArr = JSON.parse(resp[10]);
        $.each(invArr,function(key,value){//arreglo de invitados
          addGuest(value,1);
        });
        
        var equArr = JSON.parse(resp[11]);
        $.each(equArr,function(key,value){//arreglo de equipo
          addEquipment(value,1);
        });
      }
      else{
        printErrorMsg("div_msgWorkOrderDetails",resp[1]);       
      }
      
      $("#txt_tareaFechaIni").datetimepicker(datetimeOptions);
      $("#txt_tareaFechaFin").datetimepicker(datetimeOptions);
      $("#txt_tareaDurHor").datetimepicker(timeOptions); 
      setModalResponsive('mod_newWorkOrderTask'); 
      $("#mod_newWorkOrderTask").modal("show");
    }  
  });
}
  
  function addAdecToOrder(mode,adecArr){
    var tempArray= new Array();
    if(0==mode){
      var cantidad = $("#txt_adecCant").val();
      var nombre = $("#sel_adecuaciones option:selected").text();
      var adecId = random_string();      
      var adecSerId = $("#sel_adecuaciones option:selected").val(); 
    }
    else if(1==mode){
      var cantidad = adecArr[2];
      var nombre = adecArr[3];
      var adecId = adecArr[0]; 
      var adecSerId = adecArr[1] 
    }
    
    if(undefined===cantidad||0>=cantidad){
      alert("Debe especificarse una cantidad a agregar");
    }
    else{
      $('#tbl_selectedAdec tr:last').after("  <tr id=\"tr_selAdec_"+adecId+"\">"+
                                          "    <td id=\"td_prodCant_"+adecId+"\">"+cantidad+"</td>"+
                                          "    <td id=\"td_tipSer_"+adecId+"\">"+nombre+"</td>"+
                                          "    <td><button type=\"button\" title=\"Remover\" class=\"btn btn-xs btn-danger\" onClick=\"removeAdec('"+adecId+"');\">"+
                                          "<span class=\"glyphicon glyphicon-remove\"></span>"+
                                          "     </button>"+
                                          "   </td>"+
                                          "  </tr>"); 
      
      tempArray = [adecId,adecSerId,cantidad]; 
      adecArray.push(tempArray);
      $("#sel_adecuaciones option[value='"+adecId+"']").prop('disabled',true);
      
      $("#txt_adecCant").val("");
    }
  }
  
  function removeAdec(adecId){
    $("#tr_selAdec_"+adecId).remove();
    $("#sel_adecuaciones option[value='"+adecId+"']").prop('disabled',false);
    unsetFromArray(adecArray,adecId);
  }
  
  function saveWorkOrderNew(){
    if(false==validateWorkOrderData()){
      printErrorMsg("div_msgAlertWorkOrder","Existen datos no válidos, favor de corregirlos");
    }
    else{
      var factible = $('input[name="rad_workOrdFact"]:checked').val();
      if((undefined==adecArray||0>=adecArray.length)&&3==factible){
        printErrorMsg("div_msgAlertWorkOrder","No existen adecuaciones para guardar");
      }
      else{
        var worOrdType = $('input[name="rad_workOrder"]:checked').val();
        var desc = $("#txt_worOrdDesc").val();
        var titulo = $("#txt_worOrdTitulo").val();
        var option = $("#hdn_worOrdAction").val();
        var relCotPro = $("#hdn_relCotPro").val();
        var worOrdId = $("#hdn_worOrdId").val();
        var worOrdStat = $("#sel_worOrdEstatus option:selected").val();
        var worOrdPricId = $("#hdn_worOrdPricId").val();
        var jsonAdecArr = JSON.stringify(adecArray);
            
        $.ajax({
          type: "POST",
          url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
          data: {"action": "saveWorkOrderNew", "worOrdId":worOrdId, "worOrdType":worOrdType, "titulo":titulo, "desc":desc, "relCotPro":relCotPro, "worOrdStat":worOrdStat, "option":option, "factible":factible, "adecArray":jsonAdecArr, "worOrdPricId":worOrdPricId},  
          beforeSend: function(){
            setLoadDialog(0,"Guardando órden de trabajo");
          },
          complete: function(){
            setLoadDialog(1,"");
          },      
          success: function(msg){
            var resp = msg.trim().split("||");
            if("OK" == resp[0]){
              if(5!=worOrdType){
                if(0==option){
                  getWorkOrders(relCotPro);
                }
                else if(1==option){
                  getWorkOrderDetails(worOrdId,0);
                }
              }
              else{
                $("#mod_prices").modal("hide"); 
              }
              printSuccessMsg("div_msgWorkOrderDetails",resp[1]);
              $("#mod_newWorkOrder").modal("hide");   
            }
            else{
              printErrorMsg("div_msgAlertWorkOrder",resp[1]);
            }  
          }  
        });
      }
    }
  }

  //función que valida la inegridad de lso datos introducidos en el formulario devuelve true si todos son correctos
function validateWorkOrderData(){  
  return $("#frm_newWorkOrder").validate({
    rules: {
      txt_worOrdTitulo:
      {
        required: true
      },
      txt_worOrdDesc:
      {
        required: true
      }
    },
    errorPlacement: function(error, element){
      error.appendTo($('#' + element.attr("id") + '_error'));
    }
  }).form(); 
} 
  
  //función que valida la inegridad de lso datos introducidos en el formulario devuelve true si todos son correctos
function validateWorkOrderTaskData(){ 
  addValidationRules(3);
  
  return $("#frm_newWorkOrderTask").validate({
    rules: {
      txt_tareaFechaIni:
      {
        isDateTime: true,
        isLaterThanToday: 
        {
          depends: function(){
            if(1<=$("#sel_tareaEstat").val())
              return false;
            else
              return true;
          }
        }
      },
      txt_tareaFechaFin:
      {
        isDateTime: true,
        isLaterThanDate: 'txt_tareaFechaIni'
      },
      sel_tareaEstat:
      {
        isSomethingSelected: true
      }
    },
    errorPlacement: function(error, element){
      error.appendTo($('#' + element.attr("id") + '_error'));
    }
  }).form(); 
} 


  
  //función que guarda un evento nuevo o guarda un evento editado (dependiendo del valor en el hidden eveAction)
function saveWorkOrderTaskNew(){
  if(false==validateWorkOrderTaskData()){
    printErrorMsg("div_msgAlertTask","Existen datos no válidos, favor de corregirlos");
  }
  else{
    var titulo = $("#txt_tareaTitulo").val();
    var fechaIni = $("#txt_tareaFechaIni").val();
    var fechaFin = $("#txt_tareaFechaFin").val();
    var eveEstat = $("#sel_tareaEstat").val();
    var eveDesc = $("#txt_tareaDesc").val();
    var eveAction = $("#hdn_tareaAction").val();
    var eveId = $("#hdn_tareaId").val();
    var workOrdId = $("#hdn_workOrdId").val();
    var jsonInvitadoArray = JSON.stringify(invitadoArray);
    var jsonEquipoArray = JSON.stringify(equipoArray);
    
    $.ajax({
      type: "POST",
      url: "../../libs/db/common.php",//direccion relativa del modulo que invoca a la libreria common
      data: {"action": "saveWorkOrderTask", "titulo":titulo, "fechaIni":fechaIni, "fechaFin":fechaFin, 
        "eveEstat":eveEstat,"eveDesc":eveDesc, "eveAction":eveAction, "eveId":eveId, 
        "workOrdId":workOrdId, "invitadoArray":jsonInvitadoArray, "equipoArray":jsonEquipoArray},
      beforeSend: function(){
        setLoadDialog(0,"Guardando tarea");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(response){
        var resp = (response).trim().split('||');
        var errCod = resp[0];
        var errMsg = resp[1];
        if("OK"== errCod){
          printSuccessMsg("div_msgAlertTask",errMsg);
            getWorkOrderDetails(workOrdId,0);                          
          $("#mod_newWorkOrderTask").modal("hide");
        }
        else{
          printErrorMsg("div_msgAlertTask",errMsg);                                                      
        }
      },
      error: function(){
        printErrorMsg("div_msgAlertTask","Ocurrió un error al enviarse los datos para su guardado"); 
      }    
    });
  }
}


/*********Fin de apartado**********/

/*********Apartado de funciones de gestión de enlaces de sitio**********/

//limpia los campos y mensajes de error en el formulario de enlace de sitio
function clearSiteConnectionFields(){
  $("#contactConnDialog").load("../../libs/templates/contactdialog.php");//carga cuadro de dialogo de insertar contacto en el modulo
  $("#div_msgAlertSiteConnection").html("");
  $("#txt_nodSitCon").val("");
  $("#txt_downSitCon").val("");
  $("#txt_upSitCon").val("");
  $("#txt_fecInsSitCon").val("");
  $("#txt_dbmSitCon").val("");
  $("#txt_radSitCon").val("");
  $("#txt_modAnt1SitCon").val("");
  $("#txt_serAnt1SitCon").val("");
  $("#txt_modAnt2SitCon").val("");
  $("#txt_serAnt2SitCon").val("");
  $("#txt_modRouSitCon").val("");
  $("#txt_serRouSitCon").val("");
  $("#txt_fecDesSitCon").val("");
  $("#txt_sitConDesc").val("");
  $("#tbl_selectedContact8").find("tr:gt(0)").remove();  
  
  $("#sel_conTop_error").html("");
  $("#sel_conAct_error").html("");
  $("#txt_nodSitCon_error").html("");
  $("#txt_downSitCon_error").html("");
  $("#txt_upSitCon_error").html("");
  $("#txt_fecInsSitCon_error").html("");
  $("#txt_dbmSitCon_error").html("");
  $("#txt_radSitCon_error").html("");
  $("#txt_modAnt1SitCon_error").html("");
  $("#txt_serAnt1SitCon_error").html("");
  $("#txt_modAnt2SitCon_error").html("");
  $("#txt_serAnt2SitCon_error").html("");
  $("#txt_modRouSitCon_error").html("");
  $("#txt_serRouSitCon_error").html("");
  $("#txt_fecDesSitCon_error").html("");
  $("#txt_sitConDesc_error").html(""); 
}

//función que prepara los campos para editar un enlace de sitio
function siteConnectionEdit(id){
  clearSiteConnectionFields();
  $("#hdn_siteConId").val(id); 
  $("#hdn_siteConnOption").val(1);
   
  $("#txt_fecInsSitCon").datetimepicker(datetimeOptions);
  $("#txt_fecDesSitCon").datetimepicker(datetimeOptions);
  contactoArray.length = 0; 
  fillSiteConnFields(id);
}

//llena los campos del formulario de enlace de sitio y abre el cuadro de diálogo
function fillSiteConnFields(id){  
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getSiteConnectionDetails", "siteConId":id, "option":1},
    success: function(msg){
      var resp = msg.trim().split("||");
      if("OK" == resp[0]){       
        $("#hdn_siteConId").val(id);
        $("#txt_nodSitCon").val(resp[2]);
        $("#txt_downSitCon").val(resp[4]);
        $("#txt_upSitCon").val(resp[5]);
        $("#txt_fecInsSitCon").val(resp[7]);
        $("#txt_dbmSitCon").val(resp[9]);
        $("#txt_radSitCon").val(resp[10]);
        $("#txt_modAnt1SitCon").val(resp[13]);
        $("#txt_serAnt1SitCon").val(resp[14]);
        $("#txt_modAnt2SitCon").val(resp[15]);
        $("#txt_serAnt2SitCon").val(resp[16]);
        $("#txt_modRouSitCon").val(resp[11]);
        $("#txt_serRouSitCon").val(resp[12]);
        //$("#txt_fecDesSitCon").val(resp[18]);
        $("#txt_sitConDesc").val(resp[17]);        
        
        if((0>=resp[18].length)||("0000-00-00 00:00:00"==resp[18])){
          $("#chk_fecDesSitCon").prop("checked",false);
          $("#chk_fecDesSitCon").val(0);
          $("#txt_fecDesSitCon").val("");
          $("#txt_fecDesSitCon").prop("disabled",true);
        }
        else if(0<resp[18].length){
          $("#chk_fecDesSitCon").prop("checked",true);
          $("#chk_fecDesSitCon").val(1);
          $("#txt_fecDesSitCon").val(resp[18]);
          $("#txt_fecDesSitCon").prop("disabled",false);       
        }
        
        getConnectionTopology(resp[6]);
        getConnectionActive(resp[8]);
        
        var contArr = JSON.parse(resp[19]);
        
        for(var x=0;x<contArr.length;x++){
          insertContactOnForm(1,contArr[x],8);
        }        
        $("#mod_siteConnectionInsert").modal("show");
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);
      }      
    }  
  });
}

//elimina un enlace de sitio
function deleteSiteConnection(id){
  if(confirm("¿Está seguro/a de eliminar esta enlace de sitio? Se eliminarán también sus servicios y equipos asociados. Esta acción no se podrá deshacer")) {
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: {"action": "deleteSiteConnection", "siteConId":id},
      beforeSend: function(){
        setLoadDialog(0,"Eliminando enlace de sitio");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){
        var resp = msg.trim().split("||");
        var errCod = resp[0];
        if("OK" == errCod){
          printSuccessMsg("div_msgAlert",resp[1]);
          getSiteConnections(); 
        }
        else{
          printErrorMsg("div_msgAlert",resp[1]);           
        }
      }  
    });
  }
}

//función que manda a guardar los datos del formulario de enlaces de sitio en la db donde "hdn_siteConnOption" indica si se trata de un registro nuevo o uno existente 0=nuevo 1= existente
function saveSiteConnectionNew(){
  if(false==validateSiteConnectionData()){ 
    printErrorMsg("div_msgAlertSiteConnection","Existen datos no válidos, favor de corregirlos"); 
  }
  else{    
    var sitConnContact = JSON.stringify(contactoArray);
    
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: {"action": "saveSiteConnections", "siteId": $("#hdn_orgId").val(), "nodSitCon": $("#txt_nodSitCon").val(), "downSitCon": $("#txt_downSitCon").val(), "upSitCon": $("#txt_upSitCon").val(), "fecInsSitCon": $("#txt_fecInsSitCon").val(), "dbmSitCon": $("#txt_dbmSitCon").val(), "radSitCon": $("#txt_radSitCon").val(), "modAnt1SitCon": $("#txt_modAnt1SitCon").val(), "serAnt1SitCon": $("#txt_serAnt1SitCon").val(), "modAnt2SitCon": $("#txt_modAnt2SitCon").val(), "serAnt2SitCon": $("#txt_serAnt2SitCon").val(), "modRouSitCon": $("#txt_modRouSitCon").val(), "serRouSitCon": $("#txt_serRouSitCon").val(), "fecDesSitCon": $("#txt_fecDesSitCon").val(), "sitConDesc": $("#txt_sitConDesc").val(), "sitConnContact": sitConnContact, "siteConnOption": $("#hdn_siteConnOption").val(), "siteConId": $("#hdn_siteConId").val(), "selConTop": $("#sel_conTop").val(), "selConAct": $("#sel_conAct").val()},
      beforeSend: function(){
        setLoadDialog(0,"Guardando enlace de sitio");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){        
        var resp = msg.trim().split("||");
        if("OK" == resp[0]){
          printSuccessMsg("div_msgAlert",resp[1]);
          $("#mod_siteConnectionInsert").modal("hide");
          
          if(typeof getSiteConnectionDetails == 'function')         
            getSiteConnectionDetails();
          if(typeof getSiteConnections == 'function')
            getSiteConnections();
        }
        else{ 
          printErrorMsg("div_msgAlertSiteConnection",resp[1]);         
        }   
      } 
    });
  }
}

//función que valida la integridad de los datos del formulario de enlaces de sitio
function validateSiteConnectionData(){
  addValidationRules(8);
  
  return $("#frm_newSiteConnection").validate({
    ignore: ':hidden:not(#hdn_contArrSitCon)',
    rules: {
      txt_downSitCon:
      {
        number: true
      },
      txt_upSitCon:
      {
        number: true
      },
      txt_fecInsSitCon:
      {
        isDateTime: true
      },  
      txt_fecDesSitCon:
      {
        isDateTime: true,
        isLaterThanDate: 'txt_fecInsSitCon'
      }, 
      txt_dbmSitCon:
      {
        number: true
      },    
      sel_conTop:
      {
        isSomethingSelected: true
      },
      sel_conAct:
      {
        isSomethingSelected: true
      },
      hdn_contArrSitCon:
      {
        hasContacts:true
      }
    },
    errorPlacement: function(error, element) {
        error.appendTo($('#' + element.attr("id") + '_error'));
    }
  }).form();
}

/*********Fin de apartado**********/

/*********Apartado de funciones de gestión de servicios**********/


//obtiene los detalles de un servicio en específico
function getServiceDetails(serviceId){
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getServiceDetails", "serviceId":serviceId, "option":"0"},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo datos del servicio");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      $("#div_serviceDetails").html(msg);
      //setModalResponsive('mod_serviceDetails'); 
      $("#mod_serviceDetails").modal("show"); 
    }  
  });
}

//borra un servicio
function deleteService(serviceId){
  if(confirm("¿Está seguro/a de eliminar este servicio? Se eliminarán también sus equipos asociados. Esta acción no se podrá deshacer.")) {
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: {"action": "deleteService", "serviceId":serviceId},
      beforeSend: function(){
        setLoadDialog(0,"Eliminando servicio");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){
        var resp = (msg).trim().split("||");
        if("OK" == resp[0]){
          printSuccessMsg("div_msgAlert",resp[1]);          
          if(typeof getServiceList == 'function')
            getServiceList();
          if(typeof getServicesRegs == 'function')  
            getServicesRegs();
        }
        else{ 
          printErrorMsg("div_msgAlert",resp[1]);       
        } 
      }  
    });
  }
}

//obtiene la lista de equipos asociados a un servicio
function getServiceEquipmentList(serviceId){
  $("#div_msgAlertServiceEquipmentList").html("");
  $("#hdn_serviceId").val(serviceId);
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getServiceEquipment", "serviceId":serviceId},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo datos de los equipos");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      $("#div_serviceEquipmentList").html(msg);
      $("#tbl_serviceEquipmentDetails").DataTable({
        language: datatableespaniol,
      });
      $("#mod_serviceEquipmentList").modal("show"); 
    }  
  });
}

//abre el cuadro de dialogo para editar un servicio y llena los campos con los valores actuales
function serviceEdit(serviceId){
  $("#hdn_serviceAction").val(1);
  $("#hdn_serviceId").val(serviceId); 
  
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getServiceDetails", "serviceId":serviceId, "option":"1"},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo datos del servicio");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = (msg).trim().split("||");
      if("OK" == resp[0]){
        setSerFieldsByType(parseInt(resp[2]));
        getService(parseInt(resp[2]));       
        
        $("#txt_serDownload").val(resp[3]); 
        $("#txt_serUpload").val(resp[7]);
        $("#txt_serCantIp").val(resp[4]);
        $("#txt_serLinAna").val(resp[5]);
        $("#txt_serCantDid").val(resp[6]);
        $("#txt_serCantCan").val(resp[8]);
        $("#txt_serCantExt").val(resp[9]);
        $("#txt_serDomVpbx").val(resp[10]);
        $("#txt_serIpVpbx").val(resp[11]);
        $("#txt_serCosto").val(resp[12]);
        
        $("#div_msgAlertService").html("");
        $("#mod_newService").modal("show");
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);        
      }      
    } 
  });
}

//función que valida la integridad de los datos introducidos en el formulario devuelve true si todos son correctos
function validateServiceData(){ 
  addValidationRules(5);
  
  return $("#frm_newService").validate({
    rules: {
      txt_serDownload:
      {
        number:true
      },
      txt_serUpload:
      {
        number:true
      },
      txt_serCantDid:
      {
        number:true
      },
      txt_serCantCan:
      {
        number:true
      },
      txt_serCantExt:
      {
        number:true
      },
      txt_serCantIp:
      {
        number:true       
      },  
      txt_serIpVpbx:
      {
        isIpAddress:true
      },
      sel_serTipo:
      {
        isSomethingSelected: true
      }
    },
    errorPlacement: function(error, element) {
        error.appendTo($('#' + element.attr("id") + '_error'));
    }
  }).form(); 
}

//guarda los datos de un servicio ya sea nuevo o uno existente
function saveServiceNew(){
  if(false==validateServiceData()){ 
    printErrorMsg("div_msgAlertService","Existen datos no válidos, favor de corregirlos");  
  }
  else{
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: {"action": "saveServiceNew", "option":$("#hdn_serviceAction").val(), "serviceId":$("#hdn_serviceId").val(), "serDownload":$("#txt_serDownload").val(), "siteConId":$("#hdn_siteConId").val(), "serType":$("#sel_serTipo").val(), "serUpload":$("#txt_serUpload").val(), "serCantIp":$("#txt_serCantIp").val(), "serLinAna":$("#txt_serLinAna").val(), "serCantDid":$("#txt_serCantDid").val(), "serCantCan":$("#txt_serCantCan").val(), "serCantExt":$("#txt_serCantExt").val(), "serDomVpbx":$("#txt_serDomVpbx").val(), "serIpVpbx":$("#txt_serIpVpbx").val(), "serCosto":$("#txt_serCosto").val()},
      
      beforeSend: function(){
        setLoadDialog(0,"Guardando servicio");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){
        var resp = (msg).trim().split("||");
        if("OK" == resp[0]){
          $("#mod_newService").modal("hide");
          printSuccessMsg("div_msgAlert",resp[1]);
          if(typeof getServiceList == 'function')
            getServiceList();
          if(typeof getServicesRegs == 'function')  
            getServicesRegs();
        }
        else{
          printErrorMsg("div_msgAlertService",resp[1]);          
        }  
      }  
    });   
  } 
}

//abre el cuadro de diálogo para agregar un equipo y asociarlo a un servicio
function openNewEquipment(serviceId){
  $("#hdn_ServiceEquipAction").val(0);
  $("#txt_serEquipNumSerie").val("");
  getServiceEquipment(0);
  $("#mod_newServiceEquipment").modal("show"); 
  $("#hdn_serviceId").val(serviceId);
}

//abre el formulario para editar un equipo y llena los campos con los valores existentes
function serviceEquipmentEdit(serEquipId){
  $("#hdn_ServiceEquipAction").val(1);
  $("#hdn_ServiceEquipId").val(serEquipId);
  
  $.ajax({
    type: "POST",
    url: "ajax.php",
    data: {"action": "getServiceEquipmentDetails", "serviceEquipId":serEquipId},
    beforeSend: function(){
      setLoadDialog(0,"Obteniendo datos de los equipos");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){    
      var resp = (msg).trim().split("||");
      if("OK" == resp[0]){
        $("#txt_serEquipNumSerie").val(resp[3]);
        getServiceEquipment(resp[2]);
        $("#mod_newServiceEquipment").modal("show");
        $("#hdn_serviceId").val(resp[1]);
      }
      else{
        printErrorMsg("div_msgAlert",resp[1]);                   
      }       
    }  
  });
}

//valida la integridad de los campos en el formulario de equipos antes de guardar
function validateServiceEquipmentData(){
  addValidationRules(5);
  
  return $("#frm_newServiceEquipment").validate({
    rules: {
      txt_serEquipNumSerie:
      {
        required:true
      },
      sel_serEquip:
      {
        isSomethingSelected: true
      }
    },
    errorPlacement: function(error, element) {
        error.appendTo($('#' + element.attr("id") + '_error'));
    }
  }).form(); 
}

//guarda los datos de un equipo asociado a un servicio
function saveServiceEquipmentNew(){
  if(false==validateServiceEquipmentData()){ 
    printErrorMsg("div_msgAlertServiceEquipment","Existen datos no válidos, favor de corregirlos");  
  }  
  else{
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: {"action": "saveServiceEquipmentNew", "option":$("#hdn_ServiceEquipAction").val(), "serviceId":$("#hdn_serviceId").val(), "serviceEquipId":$("#hdn_ServiceEquipId").val(), "serEquip":$("#sel_serEquip").val(), "serEquipNumSerie":$("#txt_serEquipNumSerie").val()},
      
      beforeSend: function(){
        setLoadDialog(0,"Guardando equipo");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){
        var resp = (msg).trim().split("||");
        if("OK" == resp[0]){
          $("#mod_newServiceEquipment").modal("hide");         
          printSuccessMsg("div_msgAlertServiceEquipmentList",resp[1]);
          getServiceEquipmentList($("#hdn_serviceId").val());
        }
        else{
          printErrorMsg("div_msgAlertServiceEquipment",resp[1]);          
        }  
      }  
    });   
  } 
}

//elimina un equipo asociado a un servicio
function deleteServiceEquipment(serEquipId){
  if(confirm("¿Está seguro/a de eliminar este equipo? Esta acción no se podrá deshacer.")) {
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: {"action": "deleteServiceEquipment", "serEquipId":serEquipId},
      beforeSend: function(){
        setLoadDialog(0,"Eliminando equipo");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){
        var resp = (msg).trim().split("||");
        if("OK" == resp[0]){
          printSuccessMsg("div_msgAlertServiceEquipmentList",resp[1]);
          getServiceEquipmentList($("#hdn_serviceId").val());
        }
        else{  
          printErrorMsg("div_msgAlertServiceEquipment",resp[1]);               
        } 
      }  
    });
  }
}
/*********Fin de apartado**********/

/*********Apartado de gestión de tickets*********/

function goToTicketDetails(id){
  window.location.href = "../tickets/index.php?id="+id;
}

//valida la integridad del campo de comentario
function validateTicketData(){
  addValidationRules(6);
  return $("#frm_newTicket").validate({
    rules: {
      txt_tickTitle:
      {
        required: true,
      },
      txt_tickAssocEmail:
      {
        required: true,
      },
      txt_tickAssocName:
      {
        required: true,
      },
      sel_ticketOrg:
      {
        isSomethingSelected: true
      },
      sel_ticketTopic:
      {
        isSomethingSelected: true
      },
      sel_ticketDepartment:
      {
        isSomethingSelected: true
      },
      sel_ticketSla:
      {
        isSomethingSelected: true
      },
      sel_tickAssign:
      {
        isSomethingSelected: true
      },
      sel_ticketPrior:
      {
        isSomethingSelected: true
      }
    },
    errorPlacement: function(error, element) {
        error.appendTo($('#' + element.attr("id") + '_error'));
    }
  }).form();  
}

//guarda un ticket nuevo
function saveTicketNew(){
  var assocArray = JSON.stringify(Array(
                                    Array($("#txt_tickAssocName").val(),$("#txt_tickAssocEmail").val(),1)
                                  ));
  if(false==validateTicketData()){ 
    printErrorMsg("div_msgAlertTicket","Existen datos no válidos, favor de corregirlos");  
  }  
  else{    
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: {"action": "saveTicketNew", "option":$("#hdn_ticketInsertAction").val(), "ticketOrg":$("#sel_ticketOrg").val(), "ticketTopic":$("#sel_ticketTopic").val(), "ticketDepartment":$("#sel_ticketDepartment").val(), "ticketSla":$("#sel_ticketSla").val(), "tickAssign":$("#sel_tickAssign").val(), "tickTitle":$("#txt_tickTitle").val(), "tickDetails":$("#div_tickDetails").code(), "ticketPrior":$("#sel_ticketPrior").val(), "siteId":$("#hdn_siteId").val(), "cliId":$("#hdn_cliId").val(), "tickId":$("#hdn_tickId").val(), "tickTopicOther":$("#txt_tickTopicOther").val(), "tickOrgOther":$("#txt_tickOrgOther").val(), "ticketReassign":$("#chk_ticketReassign").val(), "tickAssocEmail":$("#txt_tickAssocEmail").val(), "tickAssocName":$("#txt_tickAssocName").val(), "ticketClientNotify":$("#chk_ticketClientNotify").val(), "ticketAssocArray":$("#ticketAssocArray").val()},      
      beforeSend: function(){
        setLoadDialog(0,"Guardando ticket");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){
        var resp = (msg).trim().split("||");
        if("OK" == resp[0]){
          $("#mod_ticketInsert").modal("hide"); 
          
          if(0==$("#hdn_ticketInsertAction").val())
            getSiteTickets($("#hdn_siteId").val(),$("#hdn_cliId").val());
          else if(1==$("#hdn_ticketInsertAction").val())
            goToTicketDetails($("#hdn_tickId").val());
        }
        else{
          printErrorMsg("div_msgAlertTicket",resp[1]);          
        }  
      }  
    });  
  }
}

//Elimina un ticket
function deleteTicket(tiId,option){
  if(1==option||2==option||6==option) 
    var ticketId = tiId; //si es prospecto o cliente o tabla tickets
  else
    var ticketId = $("#hdn_tickId").val();//si se elimina directo desde tickets

  if(confirm("¿Desea borrar este ticket? También se borrarán sus posts, sus archivos remotos y podría afectar en las estadísticas totales de tickets. Esta acción no se podrá deshacer")){
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: {"action": "deleteTicket", "ticketId": ticketId},
      beforeSend: function(){
        setLoadDialog(0,"Obteniendo datos del ticket");
      },
      complete: function(){
        setLoadDialog(1,"");
      },
      success: function(msg){ 
        var resp = msg.trim().split("||");
        if("OK" == resp[0]){        
          if(1==option)
            getClientTickets($("#hdn_cliId").val());            
          else if(2==option)
            getSiteTickets($("#hdn_siteId").val(),$("#hdn_cliId").val());
          else if ((6==option)&&(""==$("#hdn_tickId").val())) 
            $("#tr_tick_"+tiId).remove(); 
          else
            window.location.href = "../tickets";
        }
        else{
          if(1==option)
            printErrorMsg("div_msgAlertClientTickets",resp[1]);
          else if(2==option)
            printErrorMsg("div_msgAlertSiteTickets",resp[1]);            
          else if (6==option)          
            printErrorMsg("div_msgAlert",resp[1]);
        }                
      }  
    });
  }
}

/*********Fin de apartado**********/

//envia datos de gerente para la autorización de cualquier acción que lo requiera
function sendAuthReq(){
  var option = $("#hdn_authOpt").val();
  $.ajax({
    type: "POST",
    url: "../../libs/db/common.php",
    data: {"action":"sendAuthReq", "usrName":$("#txt_authUserName").val(), "usrPass":$("#txt_authPass").val(), "option":option},
    beforeSend: function(){
      setLoadDialog(0,"Solicitando autorización");
    },
    complete: function(){
      setLoadDialog(1,"");
    },
    success: function(msg){
      var resp = msg.trim().split("||");
      
      if("OK" == resp[0]){
        if(4==option){ //valor que indica que es una cotización
          pricAuthId = resp[2];
          savePriceNew();
        }
        $("#txt_authUserName").val("");
        $("#txt_authPass").val("");
        
        $("#mod_authorization").modal("hide");
        return true;
      }
      else{
        printErrorMsg("div_msgAlertAuth",resp[1]);   
        return false;
      }
    }  
  });
}
