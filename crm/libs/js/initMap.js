var datos;
var codigo = 4; // Codigo para buscar en direccion_detalles
var lat = [];
var lng = [];
var address = [];

// Peticion para obtener todas las coordenadas asociadas    
$.ajax({
    type: "POST",
    url: "../Mapa/getDatos.php",
    data: {"codigo": codigo}, 
    success: function (resp) {
        datos = resp.trim().split("||");
        for (var i = 0; i < datos.length - 1; i +=6) {
            lat.push(datos[i]);
        }
        for (var i = 1; i < datos.length - 1; i +=6) {
            lng.push(datos[i]);
        }
        for (var i = 2; i < datos.length - 1; i +=6) {
            address.push( "<h3>" + datos[i] + "</h3>" + datos[i + 1] + "<br>Num. " + datos[i + 3]);
        }
    }
});

function initMap() {
    var bounds = new google.maps.LatLngBounds();    // Limitador de mapa
    var infoWindow = new google.maps.InfoWindow();  // ventana emergente de marcador

    var map = new google.maps.Map(document.getElementById('div_prospectMap'), { // Mapa
        center: new google.maps.LatLng(20.683, -103.378),   // Posicion Inicial
        zoom: 12
    });

    for (var i = 0; i < lat.length; i++) {  // ciclo para agregar marcadores
        var coord = new google.maps.LatLng(lat[i], lng[i]);
        var marker = new google.maps.Marker({
            position: coord,
            map: map
        });

        //agrega datos al marcador, pendiente de actualizacion
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            var info = '<div id="contenedor">' + '<div class="titulo">' + address[i] + '</div></div>';
            infoWindow.setContent(info);
            infoWindow.open(map, marker);
          }
        })(marker, i));
        bounds.extend(coord);   // añade marcadores a los limites
    }
    map.fitBounds(bounds);  // Ajusta el zoom al limite
}
