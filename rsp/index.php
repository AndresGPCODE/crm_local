<?php
// date_default_timezone_set('America/Mexico_City');
// $dbhost = '192.168.64.3';
// $dbname = 'coecrm_catalogos';
// $dbuser = 'user';
// $dbpass = '';
// echo system($command,$output);
// variables
$dbhost = 'example.org';
$dbname = 'nombre-base-datos';
$dbuser = 'usuario-base-datos';
$dbpass = 'contraseña-base-datos';
$backup_file = $dbname . date("Y-m-d-H-i-s") . '.gz';
// comandos a ejecutar
$command = "mysqldump --opt -h $dbhost -u $dbuser -p$dbpass $dbname > $backup_file";
// ejecución y salida de éxito o errores
system($command, $output);
echo $command;
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <br />
    <b>Notice</b>: Undefined index: estatus_id in <b>/opt/lampp/htdocs/crm/crm/modulos/prospectos/ajax.php</b> on line <b>398</b><br />
    <div class="row" id="div_prospectDetails">
        <div class="col-lg-6 col-xs-12 col-sm-6">
            <table id="tbl_prospectdetails" class="table table-striped table-condensed">
                <tbody>
                    <tr>
                        <td>ID</td>
                        <td>4</td>
                    </tr>
                    <tr>
                        <td><b>Razón Social</b></td>
                        <td>OSS</td>
                    </tr>
                    <tr>
                        <td>Nombre Comercial</td>
                        <td>OSS</td>
                    </tr>
                    <tr>
                        <td>RFC</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Giro Comercial</td>
                        <td>Servicios de Diseño de sistemas de cómputo y servicios relacionados</td>
                    </tr>
                    <tr>
                        <td>Teléfono</td>
                        <td><a href='#' onclick="callPhone('3322316732')">3322316732</a></td>
                    </tr>
                    <tr>
                        <td>Extensión</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Teléfono2</td>
                        <td><a href='#' onclick="callPhone('')">&nbsp;</a></td>
                    </tr>
                    <tr>
                        <td>Teléfono3</td>
                        <td><a href='#' onclick="callPhone('3336169881')">3336169881&nbsp;</a></td>
                    </tr>
                    <tr>
                        <td>Calle</td>
                        <td>Ave. Niños Héroes</td>
                    </tr>
                    <tr>
                        <td>Num Exterior</td>
                        <td>2320</td>
                    </tr>
                    <tr>
                        <td>Num Interior</td>
                        <td>D</td>
                    </tr>
                    <tr>
                        <td>Colonia</td>
                        <td>Jardines del bosque</td>
                    </tr>
                    <tr>
                        <td>CP</td>
                        <td>44520</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <div style="height:40px;width:130%;background-color:#000;left:-130px;position: relative;"></div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-lg-6 col-xs-12 col-sm-6">
            <table id="tbl_prospectdetails" class="table table-striped table-condensed">
                <tbody>
                    <tr>
                        <td>Municipio</td>
                        <td>Guadalajara</td>
                    </tr>
                    <tr>
                        <td>Estado</td>
                        <td>Jalisco</td>
                    </tr>
                    <tr>
                        <td>País</td>
                        <td>México</td>
                    </tr>
                    <tr>
                        <td>Sitio Web</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Estatus</td>
                        <td>Negociación (90%)</td>
                    </tr>
                    <tr>
                        <td>Asignado a:</td>
                        <td><a href=/crm/modulos/perfil/index.php?id='WjJMZ0dVY1FnVlhENHJ2bDI2Qks2QT09'>Andrea Zamora</a> </td> </tr> <tr>
                        <td>Fecha de Alta</td>
                        <td>2019-03-04 11:40:34</td>
                    </tr>
                    <tr>
                        <td>Usuario que lo Agregó</td>
                        <td><a href=/crm/modulos/perfil/index.php?id='TXJYdmhqNStHM0V5VElsRC8wWTAyZz09'>Griselda Mercado</a> </td> </tr> <tr>
                        <td>Descripción</td>
                        <td>Cliente que ofrece mismos servicios que Teknios</td>
                    </tr>
                    <tr>
                        <td>Facebook</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Twitter</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Instagram</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <b>Logo</b>
        <img src='../../docs/scan/prospectdocs/4/4_logo.png' alt='logo' title='OSS' width='200' class='img-responsive' /><br>

        <h4><b>Contactos:<b></h4>
        <div class="table-responsive">
            <table id="tbl_prospectContacts" class="table table-striped">
                <thead>
                    <tr>
                        <th class="hidden-xs">id</th>
                        <th>Nombre</th>
                        <th>Apellidos</th>
                        <th>Puesto</th>
                        <th>Teléfono</th>
                        <th>Teléfono Móvil</th>
                        <th>Correo Electŕonico</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="hidden-xs">4</td>
                        <td>Gonzalo</td>
                        <td>Lopez</td>
                        <td>Supervisor</td>
                        <td>3322316732</td>
                        <td>3316020280</td>
                        <td>gonzalo@1ss.mx</td>
                    </tr>
                </tbody>
            </table>
        </div>
</body>

</html>